﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"

template <typename R, typename T1>
struct VirtFuncInvoker1
{
	typedef R (*Func)(void*, T1, const RuntimeMethod*);

	static inline R Invoke (Il2CppMethodSlot slot, RuntimeObject* obj, T1 p1)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		return ((Func)invokeData.methodPtr)(obj, p1, invokeData.method);
	}
};
template <typename R>
struct VirtFuncInvoker0
{
	typedef R (*Func)(void*, const RuntimeMethod*);

	static inline R Invoke (Il2CppMethodSlot slot, RuntimeObject* obj)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		return ((Func)invokeData.methodPtr)(obj, invokeData.method);
	}
};
struct VirtActionInvoker0
{
	typedef void (*Action)(void*, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeObject* obj)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		((Action)invokeData.methodPtr)(obj, invokeData.method);
	}
};
template <typename R, typename T1, typename T2>
struct InterfaceFuncInvoker2
{
	typedef R (*Func)(void*, T1, T2, const RuntimeMethod*);

	static inline R Invoke (Il2CppMethodSlot slot, RuntimeClass* declaringInterface, RuntimeObject* obj, T1 p1, T2 p2)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_interface_invoke_data(slot, obj, declaringInterface);
		return ((Func)invokeData.methodPtr)(obj, p1, p2, invokeData.method);
	}
};
template <typename R>
struct InterfaceFuncInvoker0
{
	typedef R (*Func)(void*, const RuntimeMethod*);

	static inline R Invoke (Il2CppMethodSlot slot, RuntimeClass* declaringInterface, RuntimeObject* obj)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_interface_invoke_data(slot, obj, declaringInterface);
		return ((Func)invokeData.methodPtr)(obj, invokeData.method);
	}
};
template <typename T1>
struct InterfaceActionInvoker1
{
	typedef void (*Action)(void*, T1, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeClass* declaringInterface, RuntimeObject* obj, T1 p1)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_interface_invoke_data(slot, obj, declaringInterface);
		((Action)invokeData.methodPtr)(obj, p1, invokeData.method);
	}
};
template <typename R, typename T1, typename T2, typename T3>
struct InterfaceFuncInvoker3
{
	typedef R (*Func)(void*, T1, T2, T3, const RuntimeMethod*);

	static inline R Invoke (Il2CppMethodSlot slot, RuntimeClass* declaringInterface, RuntimeObject* obj, T1 p1, T2 p2, T3 p3)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_interface_invoke_data(slot, obj, declaringInterface);
		return ((Func)invokeData.methodPtr)(obj, p1, p2, p3, invokeData.method);
	}
};
struct InterfaceActionInvoker0
{
	typedef void (*Action)(void*, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeClass* declaringInterface, RuntimeObject* obj)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_interface_invoke_data(slot, obj, declaringInterface);
		((Action)invokeData.methodPtr)(obj, invokeData.method);
	}
};
template <typename R, typename T1>
struct InterfaceFuncInvoker1
{
	typedef R (*Func)(void*, T1, const RuntimeMethod*);

	static inline R Invoke (Il2CppMethodSlot slot, RuntimeClass* declaringInterface, RuntimeObject* obj, T1 p1)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_interface_invoke_data(slot, obj, declaringInterface);
		return ((Func)invokeData.methodPtr)(obj, p1, invokeData.method);
	}
};

// NativeGallery/MediaPickCallback
struct MediaPickCallback_t2F83A1D5E2DFEE08EECAE29F980B5BEDD1EEE77D;
// System.AsyncCallback
struct AsyncCallback_t3F3DA3BEDAEE81DD1D24125DF8EB30E85EE14DA4;
// System.Byte[]
struct ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821;
// System.Char[]
struct CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2;
// System.Collections.Generic.Dictionary`2<System.Int32,TouchScript.Pointers.Pointer>
struct Dictionary_2_tEA1D7FAE81C09FE63AC30DE7B11D50FB1FCCAC36;
// System.Collections.Generic.HashSet`1<System.Int32>
struct HashSet_1_tD16423F193A61077DD5FE7C8517877716AAFF11E;
// System.Collections.Generic.HashSet`1<TouchScript.Pointers.Pointer>
struct HashSet_1_t2EAB50A1455881DCD85499E2AF7A81A57399A1AB;
// System.Collections.Generic.IEnumerable`1<System.Boolean>
struct IEnumerable_1_tC6EB38083371E9C00CC441AE4ADB4A3100BEB721;
// System.Collections.Generic.IList`1<TouchScript.Pointers.Pointer>
struct IList_1_t3C3D2AB1202DCF5C5B777ABDD60717732726F73F;
// System.Collections.Generic.List`1<System.Object>
struct List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D;
// System.Collections.Generic.List`1<TouchScript.Hit.HitTest>
struct List_1_t2B70D9DC0409629A5438BC064BE6DD3A68B5CD1A;
// System.Collections.Generic.List`1<TouchScript.InputSources.IInputSource>
struct List_1_t3BE25EF6F647A730F976BA4F7D8D94EBB43DF746;
// System.Collections.Generic.List`1<TouchScript.Layers.TouchLayer>
struct List_1_tACFDA2C40746A89629E9D8D22CD4726B64D7C856;
// System.Collections.Generic.List`1<TouchScript.Pointers.Pointer>
struct List_1_tA07951FCC7998CA46E72419B7C06C4D1DA1446C2;
// System.Collections.Generic.List`1<UnityEngine.Transform>
struct List_1_t4DBFD85DCFB946888856DBE52AC08C2AF69C4DBE;
// System.Collections.IDictionary
struct IDictionary_t1BD5C1546718A374EA8122FBD6C6EE45331E8CE7;
// System.DelegateData
struct DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE;
// System.Delegate[]
struct DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86;
// System.Diagnostics.StackTrace[]
struct StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196;
// System.EventArgs
struct EventArgs_t8E6CA180BE0E56674C6407011A94BAF7C757352E;
// System.EventHandler
struct EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C;
// System.EventHandler`1<System.Object>
struct EventHandler_1_t10245A26B14DDE8DDFD5B263BDE0641F17DCFDC3;
// System.EventHandler`1<TouchScript.Layers.TouchLayerEventArgs>
struct EventHandler_1_tE6641FE0F11DA09432F018258F89A1B5C3715220;
// System.EventHandler`1<TouchScript.PointerEventArgs>
struct EventHandler_1_t0363FEC12482AFCCEA69CEA150D1710057B94330;
// System.Exception
struct Exception_t;
// System.Func`2<TouchScript.Layers.TouchLayer,System.Boolean>
struct Func_2_t403D42E78D662E69EC347FD9CE1A3F2E0E1F6DFA;
// System.IAsyncResult
struct IAsyncResult_t8E194308510B375B42432981AE5E7488C458D598;
// System.IntPtr[]
struct IntPtrU5BU5D_t4DC01DCB9A6DF6C9792A6513595D7A11E637DCDD;
// System.Object[]
struct ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.Runtime.Serialization.SafeSerializationManager
struct SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770;
// System.String
struct String_t;
// System.Text.StringBuilder
struct StringBuilder_t;
// System.Version
struct Version_tDBE6876C59B6F56D4F8CAA03851177ABC6FE0DFD;
// System.Void
struct Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017;
// TouchScript.Core.DebuggableMonoBehaviour
struct DebuggableMonoBehaviour_t9ECFEBA1E6079CED2A7AF5B46939A05FBDD0C667;
// TouchScript.Core.LayerManagerInstance
struct LayerManagerInstance_t88CDE54CE87FC7A715E326B47F38C3B0054D0578;
// TouchScript.Core.TouchManagerInstance
struct TouchManagerInstance_t2FC1653461034CC60588BF499439FF10FCC702CB;
// TouchScript.Devices.Display.IDisplayDevice
struct IDisplayDevice_t578977AAA9E7B4E744CECA232744DCB614E534F6;
// TouchScript.ILayerManager
struct ILayerManager_tE1F41773696005EFB2A5D97608D9598795083E74;
// TouchScript.ITouchManager
struct ITouchManager_t1E2C9BFCD55643DC5C5D4A005A64EA0BEEC2E843;
// TouchScript.InputSources.IInputSource
struct IInputSource_tC6FC9D2053D9966D4CB70B7AA2F13CD729EA432C;
// TouchScript.Layers.ILayerDelegate
struct ILayerDelegate_t13D48A803237FFC87EACE0505074C4A84D6E0628;
// TouchScript.Layers.ProjectionParams
struct ProjectionParams_t2B7BDC4A52427186D2923201E1479C2E7641E6B8;
// TouchScript.Layers.TouchLayer
struct TouchLayer_t484C3424D422E6073B3FE4DD21EFE05686F5AC54;
// TouchScript.Layers.TouchLayer[]
struct TouchLayerU5BU5D_tB6537B920589D57C5AF596FBC9308DEFCB888D80;
// TouchScript.Layers.WorldSpaceCanvasProjectionParams
struct WorldSpaceCanvasProjectionParams_t221503CDBFCD2E7D18AB639A41EFCB1F4BF05A3E;
// TouchScript.PointerEventArgs
struct PointerEventArgs_t60C9291910F961213770A1302F3CE7CC5A580106;
// TouchScript.Pointers.FakePointer
struct FakePointer_t8C1AEEBB63CF9BCC8170772F4E4601CD4298BC47;
// TouchScript.Pointers.IPointer
struct IPointer_t769E910CC2226CA89B07F95CCC6C526380C4BE13;
// TouchScript.Pointers.MousePointer
struct MousePointer_tEFB01FDD2340977461DEB8153349F62DFDE0E100;
// TouchScript.Pointers.ObjectPointer
struct ObjectPointer_tD190FCA2463DDBBD72FBA988696DFC51EFF46C36;
// TouchScript.Pointers.PenPointer
struct PenPointer_tAF65E3C674E3160B823CFBB7D82046A3FFD60340;
// TouchScript.Pointers.Pointer
struct Pointer_tBF6873B314D4CC8534B6CD8A8BA54B92D8CFDE98;
// TouchScript.Pointers.TouchPointer
struct TouchPointer_t35FC788E12AC007D8F90D6D28131910B61316942;
// TouchScript.TouchManager
struct TouchManager_tE3A4C5ABD05B3B6C984CDBB1FA80479E1ACBE92B;
// TouchScript.TouchManager/FrameEvent
struct FrameEvent_tEBA65ABA56D36C692FB1A49D6929A473CA7A3817;
// TouchScript.TouchManager/PointerEvent
struct PointerEvent_t46D6262BF503769AA7E077C49BB94DA4B83061DA;
// TouchScript.Utils.Attributes.NullToggleAttribute
struct NullToggleAttribute_tB95470BD28EF41A9B6EF4CC642D5D293DA28A5DF;
// TouchScript.Utils.Attributes.ToggleLeftAttribute
struct ToggleLeftAttribute_tBDFA12F6A17E62E0928F69045C9267C34DD8AAC1;
// TouchScript.Utils.ObjectPool`1<System.Collections.Generic.List`1<System.Int32>>
struct ObjectPool_1_t115A93ADDCF6A8278DEC06EB3277071E8545C393;
// TouchScript.Utils.ObjectPool`1<System.Collections.Generic.List`1<TouchScript.Pointers.Pointer>>
struct ObjectPool_1_tA955CD3AB80392B4522509C5221FC03670FCD2EA;
// UnityEngine.Camera
struct Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34;
// UnityEngine.Camera/CameraCallback
struct CameraCallback_t8BBB42AA08D7498DFC11F4128117055BC7F0B9D0;
// UnityEngine.Canvas
struct Canvas_tBC28BF1DD8D8499A89B5781505833D3728CF8591;
// UnityEngine.Canvas/WillRenderCanvases
struct WillRenderCanvases_tBD5AD090B5938021DEAA679A5AEEA790F60A8BEE;
// UnityEngine.Component
struct Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621;
// UnityEngine.EventSystems.BaseRaycaster
struct BaseRaycaster_tC7F6105A89F54A38FBFC2659901855FDBB0E3966;
// UnityEngine.Events.InvokableCallList
struct InvokableCallList_t18AA4F473C7B295216B7D4B9723B4F3DFCCC9A3F;
// UnityEngine.Events.PersistentCallGroup
struct PersistentCallGroup_t6E5DF2EBDA42794B5FE0C6DAA97DF65F0BFF571F;
// UnityEngine.Events.UnityEvent
struct UnityEvent_t5C6DDC2FCDF7F5C1808F1DDFBAD27A383F5FE65F;
// UnityEngine.Events.UnityEvent`1<System.Collections.Generic.IList`1<TouchScript.Pointers.Pointer>>
struct UnityEvent_1_tF49B9E69A03353E280A3371637BD7D7B4B691112;
// UnityEngine.Events.UnityEvent`1<System.Object>
struct UnityEvent_1_t9E897A46A46C78F7104A831E63BB081546EFFF0D;
// UnityEngine.GameObject
struct GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F;
// UnityEngine.Material
struct Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598;
// UnityEngine.MeshRenderer
struct MeshRenderer_t9D67CA54E83315F743623BDE8EADCD5074659EED;
// UnityEngine.MonoBehaviour
struct MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429;
// UnityEngine.Object
struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0;
// UnityEngine.Profiling.CustomSampler
struct CustomSampler_tD50B25148FC97E173885F9C379C8F89F067343C8;
// UnityEngine.PropertyAttribute
struct PropertyAttribute_t25BFFC093C9C96E3CCF4EAB36F5DC6F937B1FA54;
// UnityEngine.Renderer
struct Renderer_t0556D67DD582620D1F495627EDE30D03284151F4;
// UnityEngine.Shader
struct Shader_tE2731FF351B74AB4186897484FB01E000C1160CA;
// UnityEngine.Texture
struct Texture_t387FE83BB848001FD06B14707AEA6D5A0F6A95F4;
// UnityEngine.Texture2D
struct Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C;
// UnityEngine.Transform
struct Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA;
// UnityEngine.UI.Graphic
struct Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8;
// imageBank
struct imageBank_t5EEBC6CE9144A46186F0114E6CE8B23ACDC4CCE3;
// test
struct test_tBCA4AC3C1D45BA0DD176A21EC1F45D70CE6F1246;
// test_nativegallery
struct test_nativegallery_tAC993D2146BA62036BC7ADD8AFF36FC4CD783627;
// test_nativegallery/<>c__DisplayClass2_0
struct U3CU3Ec__DisplayClass2_0_t6812C288DD4EA8357CAFD0805FDB4C76A5DC6CB3;

IL2CPP_EXTERN_C RuntimeClass* ClusterUtils_t2CD9CE77DEAE30CFAFE820A605093A2A27A86696_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Debug_t7B5FCB117E2FD63B6838BC52821B252E2BFB61C4_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* EventArgs_t8E6CA180BE0E56674C6407011A94BAF7C757352E_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* EventHandler_1_t0363FEC12482AFCCEA69CEA150D1710057B94330_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Exception_t_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* FrameEvent_tEBA65ABA56D36C692FB1A49D6929A473CA7A3817_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* ICollection_1_tF3AE5A349C10D1F7E8F454D6EE9D5E63A06DD52C_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* IDisplayDevice_t578977AAA9E7B4E744CECA232744DCB614E534F6_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* IDisposable_t7218B22548186B208D65EA5B7870503810A2D15A_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* IEnumerable_1_tC6EB38083371E9C00CC441AE4ADB4A3100BEB721_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* IEnumerator_1_tE205FF055203E22F3B44CD6ABFFED46565288600_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* IEnumerator_t8789118187258CC88B77AFAC6315B5AF87D3E18A_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* ILayerManager_tE1F41773696005EFB2A5D97608D9598795083E74_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* IList_1_t3C3D2AB1202DCF5C5B777ABDD60717732726F73F_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* IPointer_t769E910CC2226CA89B07F95CCC6C526380C4BE13_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* ITouchManager_t1E2C9BFCD55643DC5C5D4A005A64EA0BEEC2E843_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* LayerManagerInstance_t88CDE54CE87FC7A715E326B47F38C3B0054D0578_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* List_1_tACFDA2C40746A89629E9D8D22CD4726B64D7C856_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Mathf_tFBDE6467D269BFE410605C7D806FD9991D4A89CB_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* MediaPickCallback_t2F83A1D5E2DFEE08EECAE29F980B5BEDD1EEE77D_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* MessageName_t450BA987F2CEE58334768B847602C2465422A4BE_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* MousePointer_tEFB01FDD2340977461DEB8153349F62DFDE0E100_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* NativeGallery_tD21DE6A0C6A5FF1B267058E3B2C00F0007F4C84C_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* ObjectPointer_tD190FCA2463DDBBD72FBA988696DFC51EFF46C36_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* PenPointer_tAF65E3C674E3160B823CFBB7D82046A3FFD60340_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Permission_t578711508834B1BD13C99D50DAD7CD563AAADEC5_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* PointerEventArgs_t60C9291910F961213770A1302F3CE7CC5A580106_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* PointerEvent_t46D6262BF503769AA7E077C49BB94DA4B83061DA_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* PointerType_t3FA6771F37343BF7F3F1070EB36821D6A1C30F74_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* PointerUtils_tEB27F0FDEFA565D24AC180439CAE78E49CF1D235_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Pointer_tBF6873B314D4CC8534B6CD8A8BA54B92D8CFDE98_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* StringBuilder_t_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* TouchManagerInstance_t2FC1653461034CC60588BF499439FF10FCC702CB_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* TouchManager_tE3A4C5ABD05B3B6C984CDBB1FA80479E1ACBE92B_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* TouchPointer_t35FC788E12AC007D8F90D6D28131910B61316942_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* TransformUtils_tEC6DF78714A4FEB9B1F27B298B574CF82DE1C680_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* U3CU3Ec__DisplayClass2_0_t6812C288DD4EA8357CAFD0805FDB4C76A5DC6CB3_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Version_tDBE6876C59B6F56D4F8CAA03851177ABC6FE0DFD_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C String_t* _stringLiteral04258F681B5AEA7A6BE8615854D7A0F58C3F32B8;
IL2CPP_EXTERN_C String_t* _stringLiteral1B6453892473A467D07372D45EB05ABC2031647A;
IL2CPP_EXTERN_C String_t* _stringLiteral1C4040CDF99C7F061E8046B8048A2E7E291BFB63;
IL2CPP_EXTERN_C String_t* _stringLiteral339F5868588CFC8FDA4A1EC94EAD8F737C5FCFC0;
IL2CPP_EXTERN_C String_t* _stringLiteral34D71611DCE9C30419B3C3EA42DC9426E931CDDE;
IL2CPP_EXTERN_C String_t* _stringLiteral356A192B7913B04C54574D18C28D46E6395428AB;
IL2CPP_EXTERN_C String_t* _stringLiteral35D5CC13D15FFCD6CBD0E77EA9F29C9C0A77CC64;
IL2CPP_EXTERN_C String_t* _stringLiteral3AE32277A8C4E1D11AF871E4E8CEF4961CE0C046;
IL2CPP_EXTERN_C String_t* _stringLiteral3C0D5845A80675A696F495CA84A88CFD41556CBA;
IL2CPP_EXTERN_C String_t* _stringLiteral42099B4AF021E53FD8FD4E056C2568D7C2E3FFA8;
IL2CPP_EXTERN_C String_t* _stringLiteral424F5635CBDACD59C5F967E319D15FC0CFCB0637;
IL2CPP_EXTERN_C String_t* _stringLiteral53A0ACFAD59379B3E050338BF9F23CFC172EE787;
IL2CPP_EXTERN_C String_t* _stringLiteral53DE552696C19F8EF3C3CB30E2C3F162ED1483CB;
IL2CPP_EXTERN_C String_t* _stringLiteral57F5F5EFBC5990F5230AA95359042338B856707B;
IL2CPP_EXTERN_C String_t* _stringLiteral5E6F80A34A9798CAFC6A5DB96CC57BA4C4DB59C2;
IL2CPP_EXTERN_C String_t* _stringLiteral6FB6DCA24A95A61457676DB99D1A9C5D11E0B5B8;
IL2CPP_EXTERN_C String_t* _stringLiteral760D542CE497A54CB36ACFB2A671DD229132CF03;
IL2CPP_EXTERN_C String_t* _stringLiteral77DE68DAECD823BABBB58EDB1C8E14D7106E83BB;
IL2CPP_EXTERN_C String_t* _stringLiteral7A38D8CBD20D9932BA948EFAA364BB62651D5AD4;
IL2CPP_EXTERN_C String_t* _stringLiteral7D56E9C94386F0AEE3567E79F1F6C6E37D7DB208;
IL2CPP_EXTERN_C String_t* _stringLiteral9036D06C54315FC50FA8E31B45FAFA8B898DA2F3;
IL2CPP_EXTERN_C String_t* _stringLiteralAC3478D69A3C81FA62E60F5C3696165A4E5E6AC4;
IL2CPP_EXTERN_C String_t* _stringLiteralB8D68D9750E37D43B20BDCF6675828CDB80E7B77;
IL2CPP_EXTERN_C String_t* _stringLiteralC439CE99A2A7165851EEA3A677E1703E301A17E0;
IL2CPP_EXTERN_C String_t* _stringLiteralC973956BBC57FD0B45330CCC233A90EB5DFDEA13;
IL2CPP_EXTERN_C String_t* _stringLiteralD08F88DF745FA7950B104E4A707A31CFCE7B5841;
IL2CPP_EXTERN_C String_t* _stringLiteralDA39A3EE5E6B4B0D3255BFEF95601890AFD80709;
IL2CPP_EXTERN_C String_t* _stringLiteralDA4B9237BACCCDF19C0760CAB7AEC4A8359010B0;
IL2CPP_EXTERN_C String_t* _stringLiteralE7064F0B80F61DBC65915311032D27BAA569AE2A;
IL2CPP_EXTERN_C const RuntimeMethod* EventHandler_1__ctor_mDD490FECE01D6EF94516D572915E5E6291E1B7D1_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* GameObject_GetComponent_TisMeshRenderer_t9D67CA54E83315F743623BDE8EADCD5074659EED_m91C1D9637A332988C72E62D52DFCFE89A6DDAB72_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* GameObject_GetComponent_TisRenderer_t0556D67DD582620D1F495627EDE30D03284151F4_mD65E2552CCFED4D0EC506EE90DE51215D90AEF85_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1__ctor_mA49956FD2D00F5266DDF54D252195E6D951B3B56_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_get_Count_mC85447D3C8462EC8C81F17B1EE9584D1DB2E804C_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_get_Item_m9CC34A7BCCF27B11010DF6AD1031D7B256727CCB_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* TouchManager_frameFinishedSendMessageHandler_m06643F8EE650407C385EF6648920F3A211E0D5E6_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* TouchManager_frameFinishedUnityEventsHandler_mBFE2FF2587FEE191DB70D58242692DC0690191ED_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* TouchManager_frameStartedSendMessageHandler_mAE3F3B2767D5FA1521A68F0275B31A7A9B3B2E04_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* TouchManager_frameStartedUnityEventsHandler_m89C3B5C487CEB8878A3B71B698F8BB814713ECF0_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* TouchManager_pointersAddedSendMessageHandler_m63C06829F976D339666BFF3FAB8530E101355FDD_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* TouchManager_pointersAddedUnityEventsHandler_m05BDEF56AC60141C69CFECD9324EAF9DCCBA3859_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* TouchManager_pointersCancelledSendMessageHandler_m4317C08AB3D25607D0B1544FBB0AEBE063597BFE_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* TouchManager_pointersCancelledUnityEventsHandler_mFEB2D2BA0CA023BC04A9ADA27B2CFED543EA6A26_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* TouchManager_pointersPressedSendMessageHandler_m93132F18416FB55153153A1F9F4D53C46255CF99_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* TouchManager_pointersPressedUnityEventsHandler_m82F19BBA6DFDA4EA2767F2F51EA100083F5C2F84_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* TouchManager_pointersReleasedSendMessageHandler_mBA8E495E7939B9A1F66F81C7179514F1F9EAE3FF_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* TouchManager_pointersReleasedUnityEventsHandler_m79811722367588FAA1AE65FB9B6F5250BCC92382_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* TouchManager_pointersRemovedSendMessageHandler_m57EA76B182D2BAD64AA62E1141EBC21CD80AE940_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* TouchManager_pointersRemovedUnityEventsHandler_mD8CB51C6894922EBAED67BD56F9920B78DA208FB_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* TouchManager_pointersUpdatedSendMessageHandler_m58381274AF569B5230025CFB6ABCDE92318C4606_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* TouchManager_pointersUpdatedUnityEventsHandler_mDD4D2BBAA16EB45E6473E25CAB802E6B1C02C719_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* U3CU3Ec__DisplayClass2_0_U3CPickImageU3Eb__0_m9A5755815823DE9D168D4EB0EA78CE86C35B1ED3_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* UnityEvent_1_Invoke_mF7D14A026E6DC7B30CDB2E16A114A79B946C7869_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* UnityEvent_1__ctor_m3C716A57D64E39CC8286EFD89A14C9361624E1FA_RuntimeMethod_var;
IL2CPP_EXTERN_C const uint32_t BinaryUtils_ToBinaryMask_mB486631A929B8755B239F3BD07EBD208F3F7E8AE_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t BinaryUtils_ToBinaryString_m841313E6549570892EDB2B9C9421BB3C54F64F03_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t ClusterUtils_Get2DCenterPosition_m85D3BC7F9ABF6B24256FC4F7EEC887B8EDC25293_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t ClusterUtils_GetPointsHash_m31E06AECDB1ACD7975B9B98CA151E538E1EFA823_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t ClusterUtils_GetPrevious2DCenterPosition_mE261E1BF0B86549ED5FE9DA1945C874585EF60BB_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t ClusterUtils__cctor_m2E71E8524F593F6D8BABC472814BC6A3BC5176A6_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t EventHandlerExtensions_InvokeHandleExceptions_m8E6D5FEFA89D62212811606B5639EA40526C8509_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t FakePointer_GetOverData_mAF9904063454D323B0D8E6758558C7F892680D31_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t MousePointer_CopyFrom_m9568581ECA2A9BB14241FB2E292E4FE15591EA2F_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t ObjectPointer_CopyFrom_mC615B4489D31D9AF0AEC3C4992CC93C7A562A14A_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t PointerEventArgs_GetCachedEventArgs_m32D6BD4E2C26738D2BB82F24904DBBB59587ED5F_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t PointerEventArgs__ctor_m2184ED9B05EDC50EC544304A7B0EEE2BE31DA2F0_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t PointerEvent__ctor_m63D4E009DE7DACDA202FD81E36A40E7D04F387C2_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t PointerFactory_Create_m7D6B0A6806D107FFCF0D3BA8B4C3EB31B4E6055F_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t PointerUtils_ButtonsToString_m90AE750BDB47BF3343173B2DC737E10AE1BB0C21_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t PointerUtils_ButtonsToString_mEC2274B4CDCC3BC17F12957876CA4760A47815B7_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t PointerUtils_IsPointerOnTarget_mFA6B5C217481FFAC6208A08D8564B02C464C2DBF_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t PointerUtils_PressedButtonsToString_m57F1173F8613DA8D378DB09AA0FDB41285C639A5_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t PointerUtils_PressedButtonsToString_mAF4CD9F932292EE068D39024DDD1E70F7DC26D9F_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t PointerUtils_initStringBuilder_mEB6698C4E28B8F6D3E4E40FE5CA452C8415C179C_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t Pointer_Equals_mB22D2809944C62A89A29FB04C2FC45DA69F38D9A_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t Pointer_INTERNAL_Reset_m851FB3F6C82DD723EB5A78235BC0B45865FE8897_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t Pointer_ToString_mFFA8218E27F99EC02496ABECBB46E8D4A5AE37B6_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t Pointer__ctor_mE7828F1485349BF2A610843ABB9A7944396F2D6C_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t Pointer_get_ProjectionParams_mECA4594E2279D92F8731317FA2BA2F3CD7904C38_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t ProjectionUtils_CameraToPlaneProjection_m49C90C2A65AB392F106E1B043BB38EC1D603376A_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t ProjectionUtils_ScreenToPlaneProjection_m5ED3FA8D18B088F44849EC32C475DE400C7B63D5_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t TouchManager_Awake_m9417FD558FC15583C25CEB2135A9A8F9053A0773_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t TouchManager_IsInvalidPosition_m80842AEBF1770FB52A67BC73957A1B1956D2716F_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t TouchManager__cctor_mA1F542857C7735E0DC46820A360BF8B2A9E48366_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t TouchManager__ctor_m54F25692CE236EEBEEA41BD3E445AB6042BA35E9_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t TouchManager_frameFinishedSendMessageHandler_m06643F8EE650407C385EF6648920F3A211E0D5E6_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t TouchManager_frameStartedSendMessageHandler_mAE3F3B2767D5FA1521A68F0275B31A7A9B3B2E04_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t TouchManager_get_DisplayDevice_m342110501378ACD88AFE29BCE3D2F1DB58D41D00_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t TouchManager_get_Instance_m355FC1AAF7F91B86F174A4429E8A44ABE454E8E3_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t TouchManager_pointersAddedSendMessageHandler_m63C06829F976D339666BFF3FAB8530E101355FDD_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t TouchManager_pointersAddedUnityEventsHandler_m05BDEF56AC60141C69CFECD9324EAF9DCCBA3859_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t TouchManager_pointersCancelledSendMessageHandler_m4317C08AB3D25607D0B1544FBB0AEBE063597BFE_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t TouchManager_pointersCancelledUnityEventsHandler_mFEB2D2BA0CA023BC04A9ADA27B2CFED543EA6A26_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t TouchManager_pointersPressedSendMessageHandler_m93132F18416FB55153153A1F9F4D53C46255CF99_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t TouchManager_pointersPressedUnityEventsHandler_m82F19BBA6DFDA4EA2767F2F51EA100083F5C2F84_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t TouchManager_pointersReleasedSendMessageHandler_mBA8E495E7939B9A1F66F81C7179514F1F9EAE3FF_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t TouchManager_pointersReleasedUnityEventsHandler_m79811722367588FAA1AE65FB9B6F5250BCC92382_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t TouchManager_pointersRemovedSendMessageHandler_m57EA76B182D2BAD64AA62E1141EBC21CD80AE940_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t TouchManager_pointersRemovedUnityEventsHandler_mD8CB51C6894922EBAED67BD56F9920B78DA208FB_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t TouchManager_pointersUpdatedSendMessageHandler_m58381274AF569B5230025CFB6ABCDE92318C4606_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t TouchManager_pointersUpdatedUnityEventsHandler_mDD4D2BBAA16EB45E6473E25CAB802E6B1C02C719_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t TouchManager_removeSendMessageSubscriptions_mDA19EA010E735C99BF9CEC546274DAC4694CF57C_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t TouchManager_removeUnityEventsSubscriptions_m16B73F9CF5C8D19E6FB2776C07439A1F6A93F3FA_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t TouchManager_set_DisplayDevice_m473676E71E3A26D1B447AC97A228687CA1FE9C71_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t TouchManager_set_SendMessageTarget_m887C577DCA1779ABB748FF27E07892D9FDBAA1BA_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t TouchManager_updateSendMessageSubscription_mAA916B61823E409F702F20238AFA50536FBABADA_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t TouchManager_updateUnityEventsSubscription_m6A5CA7A27D10F83B2A5CA578C3446A140E2FF3B4_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t TransformUtils_GetHeirarchyPath_m0AEF337A0A5E5D3DD2686702E34749D3AC63B0E5_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t TransformUtils_GlobalToLocalDirection_m51021C8A547001E01BEFD522426645789F7F7C76_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t TransformUtils_GlobalToLocalPosition_m83D494F04656DBAD08C6443DEDE9A014171743FD_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t TransformUtils_GlobalToLocalVector_m57DE02E94AFA43DD446DD9C16B46140FADCF1003_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t TransformUtils_initStringBuilder_m6846E6CB8306F423AC924826D4D12667ED674C99_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t TwoD_PointToLineDistance2_mA904184ADD13C33E7951C56BB2B8A16C167686F9_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t TwoD_PointToLineDistance_m423B16A5A7384D020178775D8019DE019E7C52B0_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t TwoD_Rotate_m5077CC8359AAB1CE9AFD09F8F8A10E6FB2E6BF42_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t U3CU3Ec__DisplayClass2_0_U3CPickImageU3Eb__0_m9A5755815823DE9D168D4EB0EA78CE86C35B1ED3_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t WorldSpaceCanvasProjectionParams_ProjectFrom_mBD552841734A8E42D1978277AF584B317C84A321_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t imageBank_Save_mD64E958D323A334B2ED50F5261B037EDAA1B6F9E_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t imageBank_Start_m0FA24C2C5260EBC0A56D06CD47E23E7EC25E8764_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t test_nativegallery_PickImage_m5836EF2B5F137638F5B23C1186F18096E8A8AF7E_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t test_nativegallery_TryPickImage_mD9DA2AAE5585AFE7E98CB7BF7498366BCE3DED39_MetadataUsageId;
struct Delegate_t_marshaled_com;
struct Delegate_t_marshaled_pinvoke;
struct Exception_t_marshaled_com;
struct Exception_t_marshaled_pinvoke;

struct ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821;
struct ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A;

IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

struct Il2CppArrayBounds;

// System.Array


// System.Attribute
struct  Attribute_tF048C13FB3C8CFCC53F82290E4A3F621089F9A74  : public RuntimeObject
{
public:

public:
};


// System.Collections.Generic.List`1<System.Object>
struct  List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;
	// System.Object System.Collections.Generic.List`1::_syncRoot
	RuntimeObject * ____syncRoot_4;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D, ____items_1)); }
	inline ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* get__items_1() const { return ____items_1; }
	inline ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____items_1), (void*)value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}

	inline static int32_t get_offset_of__syncRoot_4() { return static_cast<int32_t>(offsetof(List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D, ____syncRoot_4)); }
	inline RuntimeObject * get__syncRoot_4() const { return ____syncRoot_4; }
	inline RuntimeObject ** get_address_of__syncRoot_4() { return &____syncRoot_4; }
	inline void set__syncRoot_4(RuntimeObject * value)
	{
		____syncRoot_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____syncRoot_4), (void*)value);
	}
};

struct List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::_emptyArray
	ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* ____emptyArray_5;

public:
	inline static int32_t get_offset_of__emptyArray_5() { return static_cast<int32_t>(offsetof(List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D_StaticFields, ____emptyArray_5)); }
	inline ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* get__emptyArray_5() const { return ____emptyArray_5; }
	inline ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A** get_address_of__emptyArray_5() { return &____emptyArray_5; }
	inline void set__emptyArray_5(ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* value)
	{
		____emptyArray_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____emptyArray_5), (void*)value);
	}
};


// System.Collections.Generic.List`1<TouchScript.Layers.TouchLayer>
struct  List_1_tACFDA2C40746A89629E9D8D22CD4726B64D7C856  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	TouchLayerU5BU5D_tB6537B920589D57C5AF596FBC9308DEFCB888D80* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;
	// System.Object System.Collections.Generic.List`1::_syncRoot
	RuntimeObject * ____syncRoot_4;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_tACFDA2C40746A89629E9D8D22CD4726B64D7C856, ____items_1)); }
	inline TouchLayerU5BU5D_tB6537B920589D57C5AF596FBC9308DEFCB888D80* get__items_1() const { return ____items_1; }
	inline TouchLayerU5BU5D_tB6537B920589D57C5AF596FBC9308DEFCB888D80** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(TouchLayerU5BU5D_tB6537B920589D57C5AF596FBC9308DEFCB888D80* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____items_1), (void*)value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_tACFDA2C40746A89629E9D8D22CD4726B64D7C856, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_tACFDA2C40746A89629E9D8D22CD4726B64D7C856, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}

	inline static int32_t get_offset_of__syncRoot_4() { return static_cast<int32_t>(offsetof(List_1_tACFDA2C40746A89629E9D8D22CD4726B64D7C856, ____syncRoot_4)); }
	inline RuntimeObject * get__syncRoot_4() const { return ____syncRoot_4; }
	inline RuntimeObject ** get_address_of__syncRoot_4() { return &____syncRoot_4; }
	inline void set__syncRoot_4(RuntimeObject * value)
	{
		____syncRoot_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____syncRoot_4), (void*)value);
	}
};

struct List_1_tACFDA2C40746A89629E9D8D22CD4726B64D7C856_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::_emptyArray
	TouchLayerU5BU5D_tB6537B920589D57C5AF596FBC9308DEFCB888D80* ____emptyArray_5;

public:
	inline static int32_t get_offset_of__emptyArray_5() { return static_cast<int32_t>(offsetof(List_1_tACFDA2C40746A89629E9D8D22CD4726B64D7C856_StaticFields, ____emptyArray_5)); }
	inline TouchLayerU5BU5D_tB6537B920589D57C5AF596FBC9308DEFCB888D80* get__emptyArray_5() const { return ____emptyArray_5; }
	inline TouchLayerU5BU5D_tB6537B920589D57C5AF596FBC9308DEFCB888D80** get_address_of__emptyArray_5() { return &____emptyArray_5; }
	inline void set__emptyArray_5(TouchLayerU5BU5D_tB6537B920589D57C5AF596FBC9308DEFCB888D80* value)
	{
		____emptyArray_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____emptyArray_5), (void*)value);
	}
};


// System.EventArgs
struct  EventArgs_t8E6CA180BE0E56674C6407011A94BAF7C757352E  : public RuntimeObject
{
public:

public:
};

struct EventArgs_t8E6CA180BE0E56674C6407011A94BAF7C757352E_StaticFields
{
public:
	// System.EventArgs System.EventArgs::Empty
	EventArgs_t8E6CA180BE0E56674C6407011A94BAF7C757352E * ___Empty_0;

public:
	inline static int32_t get_offset_of_Empty_0() { return static_cast<int32_t>(offsetof(EventArgs_t8E6CA180BE0E56674C6407011A94BAF7C757352E_StaticFields, ___Empty_0)); }
	inline EventArgs_t8E6CA180BE0E56674C6407011A94BAF7C757352E * get_Empty_0() const { return ___Empty_0; }
	inline EventArgs_t8E6CA180BE0E56674C6407011A94BAF7C757352E ** get_address_of_Empty_0() { return &___Empty_0; }
	inline void set_Empty_0(EventArgs_t8E6CA180BE0E56674C6407011A94BAF7C757352E * value)
	{
		___Empty_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Empty_0), (void*)value);
	}
};


// System.String
struct  String_t  : public RuntimeObject
{
public:
	// System.Int32 System.String::m_stringLength
	int32_t ___m_stringLength_0;
	// System.Char System.String::m_firstChar
	Il2CppChar ___m_firstChar_1;

public:
	inline static int32_t get_offset_of_m_stringLength_0() { return static_cast<int32_t>(offsetof(String_t, ___m_stringLength_0)); }
	inline int32_t get_m_stringLength_0() const { return ___m_stringLength_0; }
	inline int32_t* get_address_of_m_stringLength_0() { return &___m_stringLength_0; }
	inline void set_m_stringLength_0(int32_t value)
	{
		___m_stringLength_0 = value;
	}

	inline static int32_t get_offset_of_m_firstChar_1() { return static_cast<int32_t>(offsetof(String_t, ___m_firstChar_1)); }
	inline Il2CppChar get_m_firstChar_1() const { return ___m_firstChar_1; }
	inline Il2CppChar* get_address_of_m_firstChar_1() { return &___m_firstChar_1; }
	inline void set_m_firstChar_1(Il2CppChar value)
	{
		___m_firstChar_1 = value;
	}
};

struct String_t_StaticFields
{
public:
	// System.String System.String::Empty
	String_t* ___Empty_5;

public:
	inline static int32_t get_offset_of_Empty_5() { return static_cast<int32_t>(offsetof(String_t_StaticFields, ___Empty_5)); }
	inline String_t* get_Empty_5() const { return ___Empty_5; }
	inline String_t** get_address_of_Empty_5() { return &___Empty_5; }
	inline void set_Empty_5(String_t* value)
	{
		___Empty_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Empty_5), (void*)value);
	}
};


// System.Text.StringBuilder
struct  StringBuilder_t  : public RuntimeObject
{
public:
	// System.Char[] System.Text.StringBuilder::m_ChunkChars
	CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* ___m_ChunkChars_0;
	// System.Text.StringBuilder System.Text.StringBuilder::m_ChunkPrevious
	StringBuilder_t * ___m_ChunkPrevious_1;
	// System.Int32 System.Text.StringBuilder::m_ChunkLength
	int32_t ___m_ChunkLength_2;
	// System.Int32 System.Text.StringBuilder::m_ChunkOffset
	int32_t ___m_ChunkOffset_3;
	// System.Int32 System.Text.StringBuilder::m_MaxCapacity
	int32_t ___m_MaxCapacity_4;

public:
	inline static int32_t get_offset_of_m_ChunkChars_0() { return static_cast<int32_t>(offsetof(StringBuilder_t, ___m_ChunkChars_0)); }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* get_m_ChunkChars_0() const { return ___m_ChunkChars_0; }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2** get_address_of_m_ChunkChars_0() { return &___m_ChunkChars_0; }
	inline void set_m_ChunkChars_0(CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* value)
	{
		___m_ChunkChars_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_ChunkChars_0), (void*)value);
	}

	inline static int32_t get_offset_of_m_ChunkPrevious_1() { return static_cast<int32_t>(offsetof(StringBuilder_t, ___m_ChunkPrevious_1)); }
	inline StringBuilder_t * get_m_ChunkPrevious_1() const { return ___m_ChunkPrevious_1; }
	inline StringBuilder_t ** get_address_of_m_ChunkPrevious_1() { return &___m_ChunkPrevious_1; }
	inline void set_m_ChunkPrevious_1(StringBuilder_t * value)
	{
		___m_ChunkPrevious_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_ChunkPrevious_1), (void*)value);
	}

	inline static int32_t get_offset_of_m_ChunkLength_2() { return static_cast<int32_t>(offsetof(StringBuilder_t, ___m_ChunkLength_2)); }
	inline int32_t get_m_ChunkLength_2() const { return ___m_ChunkLength_2; }
	inline int32_t* get_address_of_m_ChunkLength_2() { return &___m_ChunkLength_2; }
	inline void set_m_ChunkLength_2(int32_t value)
	{
		___m_ChunkLength_2 = value;
	}

	inline static int32_t get_offset_of_m_ChunkOffset_3() { return static_cast<int32_t>(offsetof(StringBuilder_t, ___m_ChunkOffset_3)); }
	inline int32_t get_m_ChunkOffset_3() const { return ___m_ChunkOffset_3; }
	inline int32_t* get_address_of_m_ChunkOffset_3() { return &___m_ChunkOffset_3; }
	inline void set_m_ChunkOffset_3(int32_t value)
	{
		___m_ChunkOffset_3 = value;
	}

	inline static int32_t get_offset_of_m_MaxCapacity_4() { return static_cast<int32_t>(offsetof(StringBuilder_t, ___m_MaxCapacity_4)); }
	inline int32_t get_m_MaxCapacity_4() const { return ___m_MaxCapacity_4; }
	inline int32_t* get_address_of_m_MaxCapacity_4() { return &___m_MaxCapacity_4; }
	inline void set_m_MaxCapacity_4(int32_t value)
	{
		___m_MaxCapacity_4 = value;
	}
};


// System.ValueType
struct  ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF  : public RuntimeObject
{
public:

public:
};

// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_com
{
};

// System.Version
struct  Version_tDBE6876C59B6F56D4F8CAA03851177ABC6FE0DFD  : public RuntimeObject
{
public:
	// System.Int32 System.Version::_Major
	int32_t ____Major_0;
	// System.Int32 System.Version::_Minor
	int32_t ____Minor_1;
	// System.Int32 System.Version::_Build
	int32_t ____Build_2;
	// System.Int32 System.Version::_Revision
	int32_t ____Revision_3;

public:
	inline static int32_t get_offset_of__Major_0() { return static_cast<int32_t>(offsetof(Version_tDBE6876C59B6F56D4F8CAA03851177ABC6FE0DFD, ____Major_0)); }
	inline int32_t get__Major_0() const { return ____Major_0; }
	inline int32_t* get_address_of__Major_0() { return &____Major_0; }
	inline void set__Major_0(int32_t value)
	{
		____Major_0 = value;
	}

	inline static int32_t get_offset_of__Minor_1() { return static_cast<int32_t>(offsetof(Version_tDBE6876C59B6F56D4F8CAA03851177ABC6FE0DFD, ____Minor_1)); }
	inline int32_t get__Minor_1() const { return ____Minor_1; }
	inline int32_t* get_address_of__Minor_1() { return &____Minor_1; }
	inline void set__Minor_1(int32_t value)
	{
		____Minor_1 = value;
	}

	inline static int32_t get_offset_of__Build_2() { return static_cast<int32_t>(offsetof(Version_tDBE6876C59B6F56D4F8CAA03851177ABC6FE0DFD, ____Build_2)); }
	inline int32_t get__Build_2() const { return ____Build_2; }
	inline int32_t* get_address_of__Build_2() { return &____Build_2; }
	inline void set__Build_2(int32_t value)
	{
		____Build_2 = value;
	}

	inline static int32_t get_offset_of__Revision_3() { return static_cast<int32_t>(offsetof(Version_tDBE6876C59B6F56D4F8CAA03851177ABC6FE0DFD, ____Revision_3)); }
	inline int32_t get__Revision_3() const { return ____Revision_3; }
	inline int32_t* get_address_of__Revision_3() { return &____Revision_3; }
	inline void set__Revision_3(int32_t value)
	{
		____Revision_3 = value;
	}
};

struct Version_tDBE6876C59B6F56D4F8CAA03851177ABC6FE0DFD_StaticFields
{
public:
	// System.Char[] System.Version::SeparatorsArray
	CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* ___SeparatorsArray_4;

public:
	inline static int32_t get_offset_of_SeparatorsArray_4() { return static_cast<int32_t>(offsetof(Version_tDBE6876C59B6F56D4F8CAA03851177ABC6FE0DFD_StaticFields, ___SeparatorsArray_4)); }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* get_SeparatorsArray_4() const { return ___SeparatorsArray_4; }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2** get_address_of_SeparatorsArray_4() { return &___SeparatorsArray_4; }
	inline void set_SeparatorsArray_4(CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* value)
	{
		___SeparatorsArray_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___SeparatorsArray_4), (void*)value);
	}
};


// TouchScript.Layers.ProjectionParams
struct  ProjectionParams_t2B7BDC4A52427186D2923201E1479C2E7641E6B8  : public RuntimeObject
{
public:

public:
};


// TouchScript.Pointers.PointerFactory
struct  PointerFactory_tFBC335F89C9737BF9CF92CF6326E49588ECABEC5  : public RuntimeObject
{
public:

public:
};


// TouchScript.Utils.BinaryUtils
struct  BinaryUtils_tEA9B5BD38F38D319604F0250A3121B0FAE50CE7D  : public RuntimeObject
{
public:

public:
};


// TouchScript.Utils.ClusterUtils
struct  ClusterUtils_t2CD9CE77DEAE30CFAFE820A605093A2A27A86696  : public RuntimeObject
{
public:

public:
};

struct ClusterUtils_t2CD9CE77DEAE30CFAFE820A605093A2A27A86696_StaticFields
{
public:
	// System.Text.StringBuilder TouchScript.Utils.ClusterUtils::hashString
	StringBuilder_t * ___hashString_0;

public:
	inline static int32_t get_offset_of_hashString_0() { return static_cast<int32_t>(offsetof(ClusterUtils_t2CD9CE77DEAE30CFAFE820A605093A2A27A86696_StaticFields, ___hashString_0)); }
	inline StringBuilder_t * get_hashString_0() const { return ___hashString_0; }
	inline StringBuilder_t ** get_address_of_hashString_0() { return &___hashString_0; }
	inline void set_hashString_0(StringBuilder_t * value)
	{
		___hashString_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___hashString_0), (void*)value);
	}
};


// TouchScript.Utils.EventHandlerExtensions
struct  EventHandlerExtensions_tADB873EECCD8C2FD18AA899E92E0599479E7049C  : public RuntimeObject
{
public:

public:
};


// TouchScript.Utils.Geom.ProjectionUtils
struct  ProjectionUtils_t0213D2B849610CADC8B5AA3005D7EA009B909C5A  : public RuntimeObject
{
public:

public:
};


// TouchScript.Utils.Geom.TwoD
struct  TwoD_t98D95FF722916581CF9AD354572AC30EFFB844D4  : public RuntimeObject
{
public:

public:
};


// TouchScript.Utils.PointerUtils
struct  PointerUtils_tEB27F0FDEFA565D24AC180439CAE78E49CF1D235  : public RuntimeObject
{
public:

public:
};

struct PointerUtils_tEB27F0FDEFA565D24AC180439CAE78E49CF1D235_StaticFields
{
public:
	// System.Text.StringBuilder TouchScript.Utils.PointerUtils::sb
	StringBuilder_t * ___sb_0;

public:
	inline static int32_t get_offset_of_sb_0() { return static_cast<int32_t>(offsetof(PointerUtils_tEB27F0FDEFA565D24AC180439CAE78E49CF1D235_StaticFields, ___sb_0)); }
	inline StringBuilder_t * get_sb_0() const { return ___sb_0; }
	inline StringBuilder_t ** get_address_of_sb_0() { return &___sb_0; }
	inline void set_sb_0(StringBuilder_t * value)
	{
		___sb_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___sb_0), (void*)value);
	}
};


// TouchScript.Utils.TransformUtils
struct  TransformUtils_tEC6DF78714A4FEB9B1F27B298B574CF82DE1C680  : public RuntimeObject
{
public:

public:
};

struct TransformUtils_tEC6DF78714A4FEB9B1F27B298B574CF82DE1C680_StaticFields
{
public:
	// System.Text.StringBuilder TouchScript.Utils.TransformUtils::sb
	StringBuilder_t * ___sb_0;

public:
	inline static int32_t get_offset_of_sb_0() { return static_cast<int32_t>(offsetof(TransformUtils_tEC6DF78714A4FEB9B1F27B298B574CF82DE1C680_StaticFields, ___sb_0)); }
	inline StringBuilder_t * get_sb_0() const { return ___sb_0; }
	inline StringBuilder_t ** get_address_of_sb_0() { return &___sb_0; }
	inline void set_sb_0(StringBuilder_t * value)
	{
		___sb_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___sb_0), (void*)value);
	}
};


// UnityEngine.Events.UnityEventBase
struct  UnityEventBase_t6E0F7823762EE94BB8489B5AE41C7802A266D3D5  : public RuntimeObject
{
public:
	// UnityEngine.Events.InvokableCallList UnityEngine.Events.UnityEventBase::m_Calls
	InvokableCallList_t18AA4F473C7B295216B7D4B9723B4F3DFCCC9A3F * ___m_Calls_0;
	// UnityEngine.Events.PersistentCallGroup UnityEngine.Events.UnityEventBase::m_PersistentCalls
	PersistentCallGroup_t6E5DF2EBDA42794B5FE0C6DAA97DF65F0BFF571F * ___m_PersistentCalls_1;
	// System.Boolean UnityEngine.Events.UnityEventBase::m_CallsDirty
	bool ___m_CallsDirty_2;

public:
	inline static int32_t get_offset_of_m_Calls_0() { return static_cast<int32_t>(offsetof(UnityEventBase_t6E0F7823762EE94BB8489B5AE41C7802A266D3D5, ___m_Calls_0)); }
	inline InvokableCallList_t18AA4F473C7B295216B7D4B9723B4F3DFCCC9A3F * get_m_Calls_0() const { return ___m_Calls_0; }
	inline InvokableCallList_t18AA4F473C7B295216B7D4B9723B4F3DFCCC9A3F ** get_address_of_m_Calls_0() { return &___m_Calls_0; }
	inline void set_m_Calls_0(InvokableCallList_t18AA4F473C7B295216B7D4B9723B4F3DFCCC9A3F * value)
	{
		___m_Calls_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Calls_0), (void*)value);
	}

	inline static int32_t get_offset_of_m_PersistentCalls_1() { return static_cast<int32_t>(offsetof(UnityEventBase_t6E0F7823762EE94BB8489B5AE41C7802A266D3D5, ___m_PersistentCalls_1)); }
	inline PersistentCallGroup_t6E5DF2EBDA42794B5FE0C6DAA97DF65F0BFF571F * get_m_PersistentCalls_1() const { return ___m_PersistentCalls_1; }
	inline PersistentCallGroup_t6E5DF2EBDA42794B5FE0C6DAA97DF65F0BFF571F ** get_address_of_m_PersistentCalls_1() { return &___m_PersistentCalls_1; }
	inline void set_m_PersistentCalls_1(PersistentCallGroup_t6E5DF2EBDA42794B5FE0C6DAA97DF65F0BFF571F * value)
	{
		___m_PersistentCalls_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_PersistentCalls_1), (void*)value);
	}

	inline static int32_t get_offset_of_m_CallsDirty_2() { return static_cast<int32_t>(offsetof(UnityEventBase_t6E0F7823762EE94BB8489B5AE41C7802A266D3D5, ___m_CallsDirty_2)); }
	inline bool get_m_CallsDirty_2() const { return ___m_CallsDirty_2; }
	inline bool* get_address_of_m_CallsDirty_2() { return &___m_CallsDirty_2; }
	inline void set_m_CallsDirty_2(bool value)
	{
		___m_CallsDirty_2 = value;
	}
};


// test_nativegallery_<>c__DisplayClass2_0
struct  U3CU3Ec__DisplayClass2_0_t6812C288DD4EA8357CAFD0805FDB4C76A5DC6CB3  : public RuntimeObject
{
public:
	// UnityEngine.Texture2D test_nativegallery_<>c__DisplayClass2_0::texture
	Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * ___texture_0;
	// System.Int32 test_nativegallery_<>c__DisplayClass2_0::maxSize
	int32_t ___maxSize_1;
	// test_nativegallery test_nativegallery_<>c__DisplayClass2_0::<>4__this
	test_nativegallery_tAC993D2146BA62036BC7ADD8AFF36FC4CD783627 * ___U3CU3E4__this_2;

public:
	inline static int32_t get_offset_of_texture_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass2_0_t6812C288DD4EA8357CAFD0805FDB4C76A5DC6CB3, ___texture_0)); }
	inline Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * get_texture_0() const { return ___texture_0; }
	inline Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C ** get_address_of_texture_0() { return &___texture_0; }
	inline void set_texture_0(Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * value)
	{
		___texture_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___texture_0), (void*)value);
	}

	inline static int32_t get_offset_of_maxSize_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass2_0_t6812C288DD4EA8357CAFD0805FDB4C76A5DC6CB3, ___maxSize_1)); }
	inline int32_t get_maxSize_1() const { return ___maxSize_1; }
	inline int32_t* get_address_of_maxSize_1() { return &___maxSize_1; }
	inline void set_maxSize_1(int32_t value)
	{
		___maxSize_1 = value;
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass2_0_t6812C288DD4EA8357CAFD0805FDB4C76A5DC6CB3, ___U3CU3E4__this_2)); }
	inline test_nativegallery_tAC993D2146BA62036BC7ADD8AFF36FC4CD783627 * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline test_nativegallery_tAC993D2146BA62036BC7ADD8AFF36FC4CD783627 ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(test_nativegallery_tAC993D2146BA62036BC7ADD8AFF36FC4CD783627 * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E4__this_2), (void*)value);
	}
};


// System.Boolean
struct  Boolean_tB53F6830F670160873277339AA58F15CAED4399C 
{
public:
	// System.Boolean System.Boolean::m_value
	bool ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Boolean_tB53F6830F670160873277339AA58F15CAED4399C, ___m_value_0)); }
	inline bool get_m_value_0() const { return ___m_value_0; }
	inline bool* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(bool value)
	{
		___m_value_0 = value;
	}
};

struct Boolean_tB53F6830F670160873277339AA58F15CAED4399C_StaticFields
{
public:
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_5;
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_6;

public:
	inline static int32_t get_offset_of_TrueString_5() { return static_cast<int32_t>(offsetof(Boolean_tB53F6830F670160873277339AA58F15CAED4399C_StaticFields, ___TrueString_5)); }
	inline String_t* get_TrueString_5() const { return ___TrueString_5; }
	inline String_t** get_address_of_TrueString_5() { return &___TrueString_5; }
	inline void set_TrueString_5(String_t* value)
	{
		___TrueString_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___TrueString_5), (void*)value);
	}

	inline static int32_t get_offset_of_FalseString_6() { return static_cast<int32_t>(offsetof(Boolean_tB53F6830F670160873277339AA58F15CAED4399C_StaticFields, ___FalseString_6)); }
	inline String_t* get_FalseString_6() const { return ___FalseString_6; }
	inline String_t** get_address_of_FalseString_6() { return &___FalseString_6; }
	inline void set_FalseString_6(String_t* value)
	{
		___FalseString_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FalseString_6), (void*)value);
	}
};


// System.Byte
struct  Byte_tF87C579059BD4633E6840EBBBEEF899C6E33EF07 
{
public:
	// System.Byte System.Byte::m_value
	uint8_t ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Byte_tF87C579059BD4633E6840EBBBEEF899C6E33EF07, ___m_value_0)); }
	inline uint8_t get_m_value_0() const { return ___m_value_0; }
	inline uint8_t* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(uint8_t value)
	{
		___m_value_0 = value;
	}
};


// System.Enum
struct  Enum_t2AF27C02B8653AE29442467390005ABC74D8F521  : public ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF
{
public:

public:
};

struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___enumSeperatorCharArray_0), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_com
{
};

// System.Int32
struct  Int32_t585191389E07734F19F3156FF88FB3EF4800D102 
{
public:
	// System.Int32 System.Int32::m_value
	int32_t ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Int32_t585191389E07734F19F3156FF88FB3EF4800D102, ___m_value_0)); }
	inline int32_t get_m_value_0() const { return ___m_value_0; }
	inline int32_t* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(int32_t value)
	{
		___m_value_0 = value;
	}
};


// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};


// System.Single
struct  Single_tDDDA9169C4E4E308AC6D7A824F9B28DC82204AE1 
{
public:
	// System.Single System.Single::m_value
	float ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Single_tDDDA9169C4E4E308AC6D7A824F9B28DC82204AE1, ___m_value_0)); }
	inline float get_m_value_0() const { return ___m_value_0; }
	inline float* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(float value)
	{
		___m_value_0 = value;
	}
};


// System.UInt32
struct  UInt32_t4980FA09003AFAAB5A6E361BA2748EA9A005709B 
{
public:
	// System.UInt32 System.UInt32::m_value
	uint32_t ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(UInt32_t4980FA09003AFAAB5A6E361BA2748EA9A005709B, ___m_value_0)); }
	inline uint32_t get_m_value_0() const { return ___m_value_0; }
	inline uint32_t* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(uint32_t value)
	{
		___m_value_0 = value;
	}
};


// System.Void
struct  Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017 
{
public:
	union
	{
		struct
		{
		};
		uint8_t Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017__padding[1];
	};

public:
};


// TouchScript.PointerEventArgs
struct  PointerEventArgs_t60C9291910F961213770A1302F3CE7CC5A580106  : public EventArgs_t8E6CA180BE0E56674C6407011A94BAF7C757352E
{
public:
	// System.Collections.Generic.IList`1<TouchScript.Pointers.Pointer> TouchScript.PointerEventArgs::<Pointers>k__BackingField
	RuntimeObject* ___U3CPointersU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CPointersU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(PointerEventArgs_t60C9291910F961213770A1302F3CE7CC5A580106, ___U3CPointersU3Ek__BackingField_1)); }
	inline RuntimeObject* get_U3CPointersU3Ek__BackingField_1() const { return ___U3CPointersU3Ek__BackingField_1; }
	inline RuntimeObject** get_address_of_U3CPointersU3Ek__BackingField_1() { return &___U3CPointersU3Ek__BackingField_1; }
	inline void set_U3CPointersU3Ek__BackingField_1(RuntimeObject* value)
	{
		___U3CPointersU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CPointersU3Ek__BackingField_1), (void*)value);
	}
};

struct PointerEventArgs_t60C9291910F961213770A1302F3CE7CC5A580106_StaticFields
{
public:
	// TouchScript.PointerEventArgs TouchScript.PointerEventArgs::instance
	PointerEventArgs_t60C9291910F961213770A1302F3CE7CC5A580106 * ___instance_2;

public:
	inline static int32_t get_offset_of_instance_2() { return static_cast<int32_t>(offsetof(PointerEventArgs_t60C9291910F961213770A1302F3CE7CC5A580106_StaticFields, ___instance_2)); }
	inline PointerEventArgs_t60C9291910F961213770A1302F3CE7CC5A580106 * get_instance_2() const { return ___instance_2; }
	inline PointerEventArgs_t60C9291910F961213770A1302F3CE7CC5A580106 ** get_address_of_instance_2() { return &___instance_2; }
	inline void set_instance_2(PointerEventArgs_t60C9291910F961213770A1302F3CE7CC5A580106 * value)
	{
		___instance_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___instance_2), (void*)value);
	}
};


// UnityEngine.Events.UnityEvent
struct  UnityEvent_t5C6DDC2FCDF7F5C1808F1DDFBAD27A383F5FE65F  : public UnityEventBase_t6E0F7823762EE94BB8489B5AE41C7802A266D3D5
{
public:
	// System.Object[] UnityEngine.Events.UnityEvent::m_InvokeArray
	ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* ___m_InvokeArray_3;

public:
	inline static int32_t get_offset_of_m_InvokeArray_3() { return static_cast<int32_t>(offsetof(UnityEvent_t5C6DDC2FCDF7F5C1808F1DDFBAD27A383F5FE65F, ___m_InvokeArray_3)); }
	inline ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* get_m_InvokeArray_3() const { return ___m_InvokeArray_3; }
	inline ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A** get_address_of_m_InvokeArray_3() { return &___m_InvokeArray_3; }
	inline void set_m_InvokeArray_3(ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* value)
	{
		___m_InvokeArray_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_InvokeArray_3), (void*)value);
	}
};


// UnityEngine.Events.UnityEvent`1<System.Collections.Generic.IList`1<TouchScript.Pointers.Pointer>>
struct  UnityEvent_1_tF49B9E69A03353E280A3371637BD7D7B4B691112  : public UnityEventBase_t6E0F7823762EE94BB8489B5AE41C7802A266D3D5
{
public:
	// System.Object[] UnityEngine.Events.UnityEvent`1::m_InvokeArray
	ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* ___m_InvokeArray_3;

public:
	inline static int32_t get_offset_of_m_InvokeArray_3() { return static_cast<int32_t>(offsetof(UnityEvent_1_tF49B9E69A03353E280A3371637BD7D7B4B691112, ___m_InvokeArray_3)); }
	inline ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* get_m_InvokeArray_3() const { return ___m_InvokeArray_3; }
	inline ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A** get_address_of_m_InvokeArray_3() { return &___m_InvokeArray_3; }
	inline void set_m_InvokeArray_3(ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* value)
	{
		___m_InvokeArray_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_InvokeArray_3), (void*)value);
	}
};


// UnityEngine.PropertyAttribute
struct  PropertyAttribute_t25BFFC093C9C96E3CCF4EAB36F5DC6F937B1FA54  : public Attribute_tF048C13FB3C8CFCC53F82290E4A3F621089F9A74
{
public:

public:
};


// UnityEngine.Quaternion
struct  Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357 
{
public:
	// System.Single UnityEngine.Quaternion::x
	float ___x_0;
	// System.Single UnityEngine.Quaternion::y
	float ___y_1;
	// System.Single UnityEngine.Quaternion::z
	float ___z_2;
	// System.Single UnityEngine.Quaternion::w
	float ___w_3;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}

	inline static int32_t get_offset_of_z_2() { return static_cast<int32_t>(offsetof(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357, ___z_2)); }
	inline float get_z_2() const { return ___z_2; }
	inline float* get_address_of_z_2() { return &___z_2; }
	inline void set_z_2(float value)
	{
		___z_2 = value;
	}

	inline static int32_t get_offset_of_w_3() { return static_cast<int32_t>(offsetof(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357, ___w_3)); }
	inline float get_w_3() const { return ___w_3; }
	inline float* get_address_of_w_3() { return &___w_3; }
	inline void set_w_3(float value)
	{
		___w_3 = value;
	}
};

struct Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357_StaticFields
{
public:
	// UnityEngine.Quaternion UnityEngine.Quaternion::identityQuaternion
	Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  ___identityQuaternion_4;

public:
	inline static int32_t get_offset_of_identityQuaternion_4() { return static_cast<int32_t>(offsetof(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357_StaticFields, ___identityQuaternion_4)); }
	inline Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  get_identityQuaternion_4() const { return ___identityQuaternion_4; }
	inline Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357 * get_address_of_identityQuaternion_4() { return &___identityQuaternion_4; }
	inline void set_identityQuaternion_4(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  value)
	{
		___identityQuaternion_4 = value;
	}
};


// UnityEngine.Vector2
struct  Vector2_tA85D2DD88578276CA8A8796756458277E72D073D 
{
public:
	// System.Single UnityEngine.Vector2::x
	float ___x_0;
	// System.Single UnityEngine.Vector2::y
	float ___y_1;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}
};

struct Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields
{
public:
	// UnityEngine.Vector2 UnityEngine.Vector2::zeroVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___zeroVector_2;
	// UnityEngine.Vector2 UnityEngine.Vector2::oneVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___oneVector_3;
	// UnityEngine.Vector2 UnityEngine.Vector2::upVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___upVector_4;
	// UnityEngine.Vector2 UnityEngine.Vector2::downVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___downVector_5;
	// UnityEngine.Vector2 UnityEngine.Vector2::leftVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___leftVector_6;
	// UnityEngine.Vector2 UnityEngine.Vector2::rightVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___rightVector_7;
	// UnityEngine.Vector2 UnityEngine.Vector2::positiveInfinityVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___positiveInfinityVector_8;
	// UnityEngine.Vector2 UnityEngine.Vector2::negativeInfinityVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___negativeInfinityVector_9;

public:
	inline static int32_t get_offset_of_zeroVector_2() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___zeroVector_2)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_zeroVector_2() const { return ___zeroVector_2; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_zeroVector_2() { return &___zeroVector_2; }
	inline void set_zeroVector_2(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___zeroVector_2 = value;
	}

	inline static int32_t get_offset_of_oneVector_3() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___oneVector_3)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_oneVector_3() const { return ___oneVector_3; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_oneVector_3() { return &___oneVector_3; }
	inline void set_oneVector_3(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___oneVector_3 = value;
	}

	inline static int32_t get_offset_of_upVector_4() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___upVector_4)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_upVector_4() const { return ___upVector_4; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_upVector_4() { return &___upVector_4; }
	inline void set_upVector_4(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___upVector_4 = value;
	}

	inline static int32_t get_offset_of_downVector_5() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___downVector_5)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_downVector_5() const { return ___downVector_5; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_downVector_5() { return &___downVector_5; }
	inline void set_downVector_5(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___downVector_5 = value;
	}

	inline static int32_t get_offset_of_leftVector_6() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___leftVector_6)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_leftVector_6() const { return ___leftVector_6; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_leftVector_6() { return &___leftVector_6; }
	inline void set_leftVector_6(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___leftVector_6 = value;
	}

	inline static int32_t get_offset_of_rightVector_7() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___rightVector_7)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_rightVector_7() const { return ___rightVector_7; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_rightVector_7() { return &___rightVector_7; }
	inline void set_rightVector_7(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___rightVector_7 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___positiveInfinityVector_8)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_positiveInfinityVector_8() const { return ___positiveInfinityVector_8; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_positiveInfinityVector_8() { return &___positiveInfinityVector_8; }
	inline void set_positiveInfinityVector_8(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___positiveInfinityVector_8 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_9() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___negativeInfinityVector_9)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_negativeInfinityVector_9() const { return ___negativeInfinityVector_9; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_negativeInfinityVector_9() { return &___negativeInfinityVector_9; }
	inline void set_negativeInfinityVector_9(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___negativeInfinityVector_9 = value;
	}
};


// UnityEngine.Vector3
struct  Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 
{
public:
	// System.Single UnityEngine.Vector3::x
	float ___x_2;
	// System.Single UnityEngine.Vector3::y
	float ___y_3;
	// System.Single UnityEngine.Vector3::z
	float ___z_4;

public:
	inline static int32_t get_offset_of_x_2() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720, ___x_2)); }
	inline float get_x_2() const { return ___x_2; }
	inline float* get_address_of_x_2() { return &___x_2; }
	inline void set_x_2(float value)
	{
		___x_2 = value;
	}

	inline static int32_t get_offset_of_y_3() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720, ___y_3)); }
	inline float get_y_3() const { return ___y_3; }
	inline float* get_address_of_y_3() { return &___y_3; }
	inline void set_y_3(float value)
	{
		___y_3 = value;
	}

	inline static int32_t get_offset_of_z_4() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720, ___z_4)); }
	inline float get_z_4() const { return ___z_4; }
	inline float* get_address_of_z_4() { return &___z_4; }
	inline void set_z_4(float value)
	{
		___z_4 = value;
	}
};

struct Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields
{
public:
	// UnityEngine.Vector3 UnityEngine.Vector3::zeroVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___zeroVector_5;
	// UnityEngine.Vector3 UnityEngine.Vector3::oneVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___oneVector_6;
	// UnityEngine.Vector3 UnityEngine.Vector3::upVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___upVector_7;
	// UnityEngine.Vector3 UnityEngine.Vector3::downVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___downVector_8;
	// UnityEngine.Vector3 UnityEngine.Vector3::leftVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___leftVector_9;
	// UnityEngine.Vector3 UnityEngine.Vector3::rightVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___rightVector_10;
	// UnityEngine.Vector3 UnityEngine.Vector3::forwardVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___forwardVector_11;
	// UnityEngine.Vector3 UnityEngine.Vector3::backVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___backVector_12;
	// UnityEngine.Vector3 UnityEngine.Vector3::positiveInfinityVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___positiveInfinityVector_13;
	// UnityEngine.Vector3 UnityEngine.Vector3::negativeInfinityVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___negativeInfinityVector_14;

public:
	inline static int32_t get_offset_of_zeroVector_5() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___zeroVector_5)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_zeroVector_5() const { return ___zeroVector_5; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_zeroVector_5() { return &___zeroVector_5; }
	inline void set_zeroVector_5(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___zeroVector_5 = value;
	}

	inline static int32_t get_offset_of_oneVector_6() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___oneVector_6)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_oneVector_6() const { return ___oneVector_6; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_oneVector_6() { return &___oneVector_6; }
	inline void set_oneVector_6(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___oneVector_6 = value;
	}

	inline static int32_t get_offset_of_upVector_7() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___upVector_7)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_upVector_7() const { return ___upVector_7; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_upVector_7() { return &___upVector_7; }
	inline void set_upVector_7(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___upVector_7 = value;
	}

	inline static int32_t get_offset_of_downVector_8() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___downVector_8)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_downVector_8() const { return ___downVector_8; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_downVector_8() { return &___downVector_8; }
	inline void set_downVector_8(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___downVector_8 = value;
	}

	inline static int32_t get_offset_of_leftVector_9() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___leftVector_9)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_leftVector_9() const { return ___leftVector_9; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_leftVector_9() { return &___leftVector_9; }
	inline void set_leftVector_9(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___leftVector_9 = value;
	}

	inline static int32_t get_offset_of_rightVector_10() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___rightVector_10)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_rightVector_10() const { return ___rightVector_10; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_rightVector_10() { return &___rightVector_10; }
	inline void set_rightVector_10(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___rightVector_10 = value;
	}

	inline static int32_t get_offset_of_forwardVector_11() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___forwardVector_11)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_forwardVector_11() const { return ___forwardVector_11; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_forwardVector_11() { return &___forwardVector_11; }
	inline void set_forwardVector_11(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___forwardVector_11 = value;
	}

	inline static int32_t get_offset_of_backVector_12() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___backVector_12)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_backVector_12() const { return ___backVector_12; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_backVector_12() { return &___backVector_12; }
	inline void set_backVector_12(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___backVector_12 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_13() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___positiveInfinityVector_13)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_positiveInfinityVector_13() const { return ___positiveInfinityVector_13; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_positiveInfinityVector_13() { return &___positiveInfinityVector_13; }
	inline void set_positiveInfinityVector_13(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___positiveInfinityVector_13 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_14() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___negativeInfinityVector_14)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_negativeInfinityVector_14() const { return ___negativeInfinityVector_14; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_negativeInfinityVector_14() { return &___negativeInfinityVector_14; }
	inline void set_negativeInfinityVector_14(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___negativeInfinityVector_14 = value;
	}
};


// NativeGallery_Permission
struct  Permission_t578711508834B1BD13C99D50DAD7CD563AAADEC5 
{
public:
	// System.Int32 NativeGallery_Permission::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Permission_t578711508834B1BD13C99D50DAD7CD563AAADEC5, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// System.Delegate
struct  Delegate_t  : public RuntimeObject
{
public:
	// System.IntPtr System.Delegate::method_ptr
	Il2CppMethodPointer ___method_ptr_0;
	// System.IntPtr System.Delegate::invoke_impl
	intptr_t ___invoke_impl_1;
	// System.Object System.Delegate::m_target
	RuntimeObject * ___m_target_2;
	// System.IntPtr System.Delegate::method
	intptr_t ___method_3;
	// System.IntPtr System.Delegate::delegate_trampoline
	intptr_t ___delegate_trampoline_4;
	// System.IntPtr System.Delegate::extra_arg
	intptr_t ___extra_arg_5;
	// System.IntPtr System.Delegate::method_code
	intptr_t ___method_code_6;
	// System.Reflection.MethodInfo System.Delegate::method_info
	MethodInfo_t * ___method_info_7;
	// System.Reflection.MethodInfo System.Delegate::original_method_info
	MethodInfo_t * ___original_method_info_8;
	// System.DelegateData System.Delegate::data
	DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * ___data_9;
	// System.Boolean System.Delegate::method_is_virtual
	bool ___method_is_virtual_10;

public:
	inline static int32_t get_offset_of_method_ptr_0() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_ptr_0)); }
	inline Il2CppMethodPointer get_method_ptr_0() const { return ___method_ptr_0; }
	inline Il2CppMethodPointer* get_address_of_method_ptr_0() { return &___method_ptr_0; }
	inline void set_method_ptr_0(Il2CppMethodPointer value)
	{
		___method_ptr_0 = value;
	}

	inline static int32_t get_offset_of_invoke_impl_1() { return static_cast<int32_t>(offsetof(Delegate_t, ___invoke_impl_1)); }
	inline intptr_t get_invoke_impl_1() const { return ___invoke_impl_1; }
	inline intptr_t* get_address_of_invoke_impl_1() { return &___invoke_impl_1; }
	inline void set_invoke_impl_1(intptr_t value)
	{
		___invoke_impl_1 = value;
	}

	inline static int32_t get_offset_of_m_target_2() { return static_cast<int32_t>(offsetof(Delegate_t, ___m_target_2)); }
	inline RuntimeObject * get_m_target_2() const { return ___m_target_2; }
	inline RuntimeObject ** get_address_of_m_target_2() { return &___m_target_2; }
	inline void set_m_target_2(RuntimeObject * value)
	{
		___m_target_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_target_2), (void*)value);
	}

	inline static int32_t get_offset_of_method_3() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_3)); }
	inline intptr_t get_method_3() const { return ___method_3; }
	inline intptr_t* get_address_of_method_3() { return &___method_3; }
	inline void set_method_3(intptr_t value)
	{
		___method_3 = value;
	}

	inline static int32_t get_offset_of_delegate_trampoline_4() { return static_cast<int32_t>(offsetof(Delegate_t, ___delegate_trampoline_4)); }
	inline intptr_t get_delegate_trampoline_4() const { return ___delegate_trampoline_4; }
	inline intptr_t* get_address_of_delegate_trampoline_4() { return &___delegate_trampoline_4; }
	inline void set_delegate_trampoline_4(intptr_t value)
	{
		___delegate_trampoline_4 = value;
	}

	inline static int32_t get_offset_of_extra_arg_5() { return static_cast<int32_t>(offsetof(Delegate_t, ___extra_arg_5)); }
	inline intptr_t get_extra_arg_5() const { return ___extra_arg_5; }
	inline intptr_t* get_address_of_extra_arg_5() { return &___extra_arg_5; }
	inline void set_extra_arg_5(intptr_t value)
	{
		___extra_arg_5 = value;
	}

	inline static int32_t get_offset_of_method_code_6() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_code_6)); }
	inline intptr_t get_method_code_6() const { return ___method_code_6; }
	inline intptr_t* get_address_of_method_code_6() { return &___method_code_6; }
	inline void set_method_code_6(intptr_t value)
	{
		___method_code_6 = value;
	}

	inline static int32_t get_offset_of_method_info_7() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_info_7)); }
	inline MethodInfo_t * get_method_info_7() const { return ___method_info_7; }
	inline MethodInfo_t ** get_address_of_method_info_7() { return &___method_info_7; }
	inline void set_method_info_7(MethodInfo_t * value)
	{
		___method_info_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___method_info_7), (void*)value);
	}

	inline static int32_t get_offset_of_original_method_info_8() { return static_cast<int32_t>(offsetof(Delegate_t, ___original_method_info_8)); }
	inline MethodInfo_t * get_original_method_info_8() const { return ___original_method_info_8; }
	inline MethodInfo_t ** get_address_of_original_method_info_8() { return &___original_method_info_8; }
	inline void set_original_method_info_8(MethodInfo_t * value)
	{
		___original_method_info_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___original_method_info_8), (void*)value);
	}

	inline static int32_t get_offset_of_data_9() { return static_cast<int32_t>(offsetof(Delegate_t, ___data_9)); }
	inline DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * get_data_9() const { return ___data_9; }
	inline DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE ** get_address_of_data_9() { return &___data_9; }
	inline void set_data_9(DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * value)
	{
		___data_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___data_9), (void*)value);
	}

	inline static int32_t get_offset_of_method_is_virtual_10() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_is_virtual_10)); }
	inline bool get_method_is_virtual_10() const { return ___method_is_virtual_10; }
	inline bool* get_address_of_method_is_virtual_10() { return &___method_is_virtual_10; }
	inline void set_method_is_virtual_10(bool value)
	{
		___method_is_virtual_10 = value;
	}
};

// Native definition for P/Invoke marshalling of System.Delegate
struct Delegate_t_marshaled_pinvoke
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	MethodInfo_t * ___method_info_7;
	MethodInfo_t * ___original_method_info_8;
	DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * ___data_9;
	int32_t ___method_is_virtual_10;
};
// Native definition for COM marshalling of System.Delegate
struct Delegate_t_marshaled_com
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	MethodInfo_t * ___method_info_7;
	MethodInfo_t * ___original_method_info_8;
	DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * ___data_9;
	int32_t ___method_is_virtual_10;
};

// System.Exception
struct  Exception_t  : public RuntimeObject
{
public:
	// System.String System.Exception::_className
	String_t* ____className_1;
	// System.String System.Exception::_message
	String_t* ____message_2;
	// System.Collections.IDictionary System.Exception::_data
	RuntimeObject* ____data_3;
	// System.Exception System.Exception::_innerException
	Exception_t * ____innerException_4;
	// System.String System.Exception::_helpURL
	String_t* ____helpURL_5;
	// System.Object System.Exception::_stackTrace
	RuntimeObject * ____stackTrace_6;
	// System.String System.Exception::_stackTraceString
	String_t* ____stackTraceString_7;
	// System.String System.Exception::_remoteStackTraceString
	String_t* ____remoteStackTraceString_8;
	// System.Int32 System.Exception::_remoteStackIndex
	int32_t ____remoteStackIndex_9;
	// System.Object System.Exception::_dynamicMethods
	RuntimeObject * ____dynamicMethods_10;
	// System.Int32 System.Exception::_HResult
	int32_t ____HResult_11;
	// System.String System.Exception::_source
	String_t* ____source_12;
	// System.Runtime.Serialization.SafeSerializationManager System.Exception::_safeSerializationManager
	SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 * ____safeSerializationManager_13;
	// System.Diagnostics.StackTrace[] System.Exception::captured_traces
	StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196* ___captured_traces_14;
	// System.IntPtr[] System.Exception::native_trace_ips
	IntPtrU5BU5D_t4DC01DCB9A6DF6C9792A6513595D7A11E637DCDD* ___native_trace_ips_15;

public:
	inline static int32_t get_offset_of__className_1() { return static_cast<int32_t>(offsetof(Exception_t, ____className_1)); }
	inline String_t* get__className_1() const { return ____className_1; }
	inline String_t** get_address_of__className_1() { return &____className_1; }
	inline void set__className_1(String_t* value)
	{
		____className_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____className_1), (void*)value);
	}

	inline static int32_t get_offset_of__message_2() { return static_cast<int32_t>(offsetof(Exception_t, ____message_2)); }
	inline String_t* get__message_2() const { return ____message_2; }
	inline String_t** get_address_of__message_2() { return &____message_2; }
	inline void set__message_2(String_t* value)
	{
		____message_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____message_2), (void*)value);
	}

	inline static int32_t get_offset_of__data_3() { return static_cast<int32_t>(offsetof(Exception_t, ____data_3)); }
	inline RuntimeObject* get__data_3() const { return ____data_3; }
	inline RuntimeObject** get_address_of__data_3() { return &____data_3; }
	inline void set__data_3(RuntimeObject* value)
	{
		____data_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____data_3), (void*)value);
	}

	inline static int32_t get_offset_of__innerException_4() { return static_cast<int32_t>(offsetof(Exception_t, ____innerException_4)); }
	inline Exception_t * get__innerException_4() const { return ____innerException_4; }
	inline Exception_t ** get_address_of__innerException_4() { return &____innerException_4; }
	inline void set__innerException_4(Exception_t * value)
	{
		____innerException_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____innerException_4), (void*)value);
	}

	inline static int32_t get_offset_of__helpURL_5() { return static_cast<int32_t>(offsetof(Exception_t, ____helpURL_5)); }
	inline String_t* get__helpURL_5() const { return ____helpURL_5; }
	inline String_t** get_address_of__helpURL_5() { return &____helpURL_5; }
	inline void set__helpURL_5(String_t* value)
	{
		____helpURL_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____helpURL_5), (void*)value);
	}

	inline static int32_t get_offset_of__stackTrace_6() { return static_cast<int32_t>(offsetof(Exception_t, ____stackTrace_6)); }
	inline RuntimeObject * get__stackTrace_6() const { return ____stackTrace_6; }
	inline RuntimeObject ** get_address_of__stackTrace_6() { return &____stackTrace_6; }
	inline void set__stackTrace_6(RuntimeObject * value)
	{
		____stackTrace_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____stackTrace_6), (void*)value);
	}

	inline static int32_t get_offset_of__stackTraceString_7() { return static_cast<int32_t>(offsetof(Exception_t, ____stackTraceString_7)); }
	inline String_t* get__stackTraceString_7() const { return ____stackTraceString_7; }
	inline String_t** get_address_of__stackTraceString_7() { return &____stackTraceString_7; }
	inline void set__stackTraceString_7(String_t* value)
	{
		____stackTraceString_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____stackTraceString_7), (void*)value);
	}

	inline static int32_t get_offset_of__remoteStackTraceString_8() { return static_cast<int32_t>(offsetof(Exception_t, ____remoteStackTraceString_8)); }
	inline String_t* get__remoteStackTraceString_8() const { return ____remoteStackTraceString_8; }
	inline String_t** get_address_of__remoteStackTraceString_8() { return &____remoteStackTraceString_8; }
	inline void set__remoteStackTraceString_8(String_t* value)
	{
		____remoteStackTraceString_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____remoteStackTraceString_8), (void*)value);
	}

	inline static int32_t get_offset_of__remoteStackIndex_9() { return static_cast<int32_t>(offsetof(Exception_t, ____remoteStackIndex_9)); }
	inline int32_t get__remoteStackIndex_9() const { return ____remoteStackIndex_9; }
	inline int32_t* get_address_of__remoteStackIndex_9() { return &____remoteStackIndex_9; }
	inline void set__remoteStackIndex_9(int32_t value)
	{
		____remoteStackIndex_9 = value;
	}

	inline static int32_t get_offset_of__dynamicMethods_10() { return static_cast<int32_t>(offsetof(Exception_t, ____dynamicMethods_10)); }
	inline RuntimeObject * get__dynamicMethods_10() const { return ____dynamicMethods_10; }
	inline RuntimeObject ** get_address_of__dynamicMethods_10() { return &____dynamicMethods_10; }
	inline void set__dynamicMethods_10(RuntimeObject * value)
	{
		____dynamicMethods_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____dynamicMethods_10), (void*)value);
	}

	inline static int32_t get_offset_of__HResult_11() { return static_cast<int32_t>(offsetof(Exception_t, ____HResult_11)); }
	inline int32_t get__HResult_11() const { return ____HResult_11; }
	inline int32_t* get_address_of__HResult_11() { return &____HResult_11; }
	inline void set__HResult_11(int32_t value)
	{
		____HResult_11 = value;
	}

	inline static int32_t get_offset_of__source_12() { return static_cast<int32_t>(offsetof(Exception_t, ____source_12)); }
	inline String_t* get__source_12() const { return ____source_12; }
	inline String_t** get_address_of__source_12() { return &____source_12; }
	inline void set__source_12(String_t* value)
	{
		____source_12 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____source_12), (void*)value);
	}

	inline static int32_t get_offset_of__safeSerializationManager_13() { return static_cast<int32_t>(offsetof(Exception_t, ____safeSerializationManager_13)); }
	inline SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 * get__safeSerializationManager_13() const { return ____safeSerializationManager_13; }
	inline SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 ** get_address_of__safeSerializationManager_13() { return &____safeSerializationManager_13; }
	inline void set__safeSerializationManager_13(SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 * value)
	{
		____safeSerializationManager_13 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____safeSerializationManager_13), (void*)value);
	}

	inline static int32_t get_offset_of_captured_traces_14() { return static_cast<int32_t>(offsetof(Exception_t, ___captured_traces_14)); }
	inline StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196* get_captured_traces_14() const { return ___captured_traces_14; }
	inline StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196** get_address_of_captured_traces_14() { return &___captured_traces_14; }
	inline void set_captured_traces_14(StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196* value)
	{
		___captured_traces_14 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___captured_traces_14), (void*)value);
	}

	inline static int32_t get_offset_of_native_trace_ips_15() { return static_cast<int32_t>(offsetof(Exception_t, ___native_trace_ips_15)); }
	inline IntPtrU5BU5D_t4DC01DCB9A6DF6C9792A6513595D7A11E637DCDD* get_native_trace_ips_15() const { return ___native_trace_ips_15; }
	inline IntPtrU5BU5D_t4DC01DCB9A6DF6C9792A6513595D7A11E637DCDD** get_address_of_native_trace_ips_15() { return &___native_trace_ips_15; }
	inline void set_native_trace_ips_15(IntPtrU5BU5D_t4DC01DCB9A6DF6C9792A6513595D7A11E637DCDD* value)
	{
		___native_trace_ips_15 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___native_trace_ips_15), (void*)value);
	}
};

struct Exception_t_StaticFields
{
public:
	// System.Object System.Exception::s_EDILock
	RuntimeObject * ___s_EDILock_0;

public:
	inline static int32_t get_offset_of_s_EDILock_0() { return static_cast<int32_t>(offsetof(Exception_t_StaticFields, ___s_EDILock_0)); }
	inline RuntimeObject * get_s_EDILock_0() const { return ___s_EDILock_0; }
	inline RuntimeObject ** get_address_of_s_EDILock_0() { return &___s_EDILock_0; }
	inline void set_s_EDILock_0(RuntimeObject * value)
	{
		___s_EDILock_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_EDILock_0), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of System.Exception
struct Exception_t_marshaled_pinvoke
{
	char* ____className_1;
	char* ____message_2;
	RuntimeObject* ____data_3;
	Exception_t_marshaled_pinvoke* ____innerException_4;
	char* ____helpURL_5;
	Il2CppIUnknown* ____stackTrace_6;
	char* ____stackTraceString_7;
	char* ____remoteStackTraceString_8;
	int32_t ____remoteStackIndex_9;
	Il2CppIUnknown* ____dynamicMethods_10;
	int32_t ____HResult_11;
	char* ____source_12;
	SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 * ____safeSerializationManager_13;
	StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196* ___captured_traces_14;
	Il2CppSafeArray/*NONE*/* ___native_trace_ips_15;
};
// Native definition for COM marshalling of System.Exception
struct Exception_t_marshaled_com
{
	Il2CppChar* ____className_1;
	Il2CppChar* ____message_2;
	RuntimeObject* ____data_3;
	Exception_t_marshaled_com* ____innerException_4;
	Il2CppChar* ____helpURL_5;
	Il2CppIUnknown* ____stackTrace_6;
	Il2CppChar* ____stackTraceString_7;
	Il2CppChar* ____remoteStackTraceString_8;
	int32_t ____remoteStackIndex_9;
	Il2CppIUnknown* ____dynamicMethods_10;
	int32_t ____HResult_11;
	Il2CppChar* ____source_12;
	SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 * ____safeSerializationManager_13;
	StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196* ___captured_traces_14;
	Il2CppSafeArray/*NONE*/* ___native_trace_ips_15;
};

// TouchScript.Hit.HitData_HitType
struct  HitType_t7AF5C2C9EC9445575078769EAD92214E545F86EB 
{
public:
	// System.Int32 TouchScript.Hit.HitData_HitType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(HitType_t7AF5C2C9EC9445575078769EAD92214E545F86EB, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// TouchScript.Hit.RaycastHitUI
struct  RaycastHitUI_tDB98E903E0E03D92E486668834F04300C1723874 
{
public:
	// UnityEngine.Transform TouchScript.Hit.RaycastHitUI::Target
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ___Target_0;
	// UnityEngine.EventSystems.BaseRaycaster TouchScript.Hit.RaycastHitUI::Raycaster
	BaseRaycaster_tC7F6105A89F54A38FBFC2659901855FDBB0E3966 * ___Raycaster_1;
	// System.Int32 TouchScript.Hit.RaycastHitUI::GraphicIndex
	int32_t ___GraphicIndex_2;
	// System.Int32 TouchScript.Hit.RaycastHitUI::Depth
	int32_t ___Depth_3;
	// System.Int32 TouchScript.Hit.RaycastHitUI::SortingLayer
	int32_t ___SortingLayer_4;
	// System.Int32 TouchScript.Hit.RaycastHitUI::SortingOrder
	int32_t ___SortingOrder_5;
	// UnityEngine.UI.Graphic TouchScript.Hit.RaycastHitUI::Graphic
	Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8 * ___Graphic_6;
	// UnityEngine.Vector3 TouchScript.Hit.RaycastHitUI::WorldPosition
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___WorldPosition_7;
	// UnityEngine.Vector3 TouchScript.Hit.RaycastHitUI::WorldNormal
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___WorldNormal_8;
	// System.Single TouchScript.Hit.RaycastHitUI::Distance
	float ___Distance_9;

public:
	inline static int32_t get_offset_of_Target_0() { return static_cast<int32_t>(offsetof(RaycastHitUI_tDB98E903E0E03D92E486668834F04300C1723874, ___Target_0)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get_Target_0() const { return ___Target_0; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of_Target_0() { return &___Target_0; }
	inline void set_Target_0(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		___Target_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Target_0), (void*)value);
	}

	inline static int32_t get_offset_of_Raycaster_1() { return static_cast<int32_t>(offsetof(RaycastHitUI_tDB98E903E0E03D92E486668834F04300C1723874, ___Raycaster_1)); }
	inline BaseRaycaster_tC7F6105A89F54A38FBFC2659901855FDBB0E3966 * get_Raycaster_1() const { return ___Raycaster_1; }
	inline BaseRaycaster_tC7F6105A89F54A38FBFC2659901855FDBB0E3966 ** get_address_of_Raycaster_1() { return &___Raycaster_1; }
	inline void set_Raycaster_1(BaseRaycaster_tC7F6105A89F54A38FBFC2659901855FDBB0E3966 * value)
	{
		___Raycaster_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Raycaster_1), (void*)value);
	}

	inline static int32_t get_offset_of_GraphicIndex_2() { return static_cast<int32_t>(offsetof(RaycastHitUI_tDB98E903E0E03D92E486668834F04300C1723874, ___GraphicIndex_2)); }
	inline int32_t get_GraphicIndex_2() const { return ___GraphicIndex_2; }
	inline int32_t* get_address_of_GraphicIndex_2() { return &___GraphicIndex_2; }
	inline void set_GraphicIndex_2(int32_t value)
	{
		___GraphicIndex_2 = value;
	}

	inline static int32_t get_offset_of_Depth_3() { return static_cast<int32_t>(offsetof(RaycastHitUI_tDB98E903E0E03D92E486668834F04300C1723874, ___Depth_3)); }
	inline int32_t get_Depth_3() const { return ___Depth_3; }
	inline int32_t* get_address_of_Depth_3() { return &___Depth_3; }
	inline void set_Depth_3(int32_t value)
	{
		___Depth_3 = value;
	}

	inline static int32_t get_offset_of_SortingLayer_4() { return static_cast<int32_t>(offsetof(RaycastHitUI_tDB98E903E0E03D92E486668834F04300C1723874, ___SortingLayer_4)); }
	inline int32_t get_SortingLayer_4() const { return ___SortingLayer_4; }
	inline int32_t* get_address_of_SortingLayer_4() { return &___SortingLayer_4; }
	inline void set_SortingLayer_4(int32_t value)
	{
		___SortingLayer_4 = value;
	}

	inline static int32_t get_offset_of_SortingOrder_5() { return static_cast<int32_t>(offsetof(RaycastHitUI_tDB98E903E0E03D92E486668834F04300C1723874, ___SortingOrder_5)); }
	inline int32_t get_SortingOrder_5() const { return ___SortingOrder_5; }
	inline int32_t* get_address_of_SortingOrder_5() { return &___SortingOrder_5; }
	inline void set_SortingOrder_5(int32_t value)
	{
		___SortingOrder_5 = value;
	}

	inline static int32_t get_offset_of_Graphic_6() { return static_cast<int32_t>(offsetof(RaycastHitUI_tDB98E903E0E03D92E486668834F04300C1723874, ___Graphic_6)); }
	inline Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8 * get_Graphic_6() const { return ___Graphic_6; }
	inline Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8 ** get_address_of_Graphic_6() { return &___Graphic_6; }
	inline void set_Graphic_6(Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8 * value)
	{
		___Graphic_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Graphic_6), (void*)value);
	}

	inline static int32_t get_offset_of_WorldPosition_7() { return static_cast<int32_t>(offsetof(RaycastHitUI_tDB98E903E0E03D92E486668834F04300C1723874, ___WorldPosition_7)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_WorldPosition_7() const { return ___WorldPosition_7; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_WorldPosition_7() { return &___WorldPosition_7; }
	inline void set_WorldPosition_7(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___WorldPosition_7 = value;
	}

	inline static int32_t get_offset_of_WorldNormal_8() { return static_cast<int32_t>(offsetof(RaycastHitUI_tDB98E903E0E03D92E486668834F04300C1723874, ___WorldNormal_8)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_WorldNormal_8() const { return ___WorldNormal_8; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_WorldNormal_8() { return &___WorldNormal_8; }
	inline void set_WorldNormal_8(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___WorldNormal_8 = value;
	}

	inline static int32_t get_offset_of_Distance_9() { return static_cast<int32_t>(offsetof(RaycastHitUI_tDB98E903E0E03D92E486668834F04300C1723874, ___Distance_9)); }
	inline float get_Distance_9() const { return ___Distance_9; }
	inline float* get_address_of_Distance_9() { return &___Distance_9; }
	inline void set_Distance_9(float value)
	{
		___Distance_9 = value;
	}
};

// Native definition for P/Invoke marshalling of TouchScript.Hit.RaycastHitUI
struct RaycastHitUI_tDB98E903E0E03D92E486668834F04300C1723874_marshaled_pinvoke
{
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ___Target_0;
	BaseRaycaster_tC7F6105A89F54A38FBFC2659901855FDBB0E3966 * ___Raycaster_1;
	int32_t ___GraphicIndex_2;
	int32_t ___Depth_3;
	int32_t ___SortingLayer_4;
	int32_t ___SortingOrder_5;
	Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8 * ___Graphic_6;
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___WorldPosition_7;
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___WorldNormal_8;
	float ___Distance_9;
};
// Native definition for COM marshalling of TouchScript.Hit.RaycastHitUI
struct RaycastHitUI_tDB98E903E0E03D92E486668834F04300C1723874_marshaled_com
{
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ___Target_0;
	BaseRaycaster_tC7F6105A89F54A38FBFC2659901855FDBB0E3966 * ___Raycaster_1;
	int32_t ___GraphicIndex_2;
	int32_t ___Depth_3;
	int32_t ___SortingLayer_4;
	int32_t ___SortingOrder_5;
	Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8 * ___Graphic_6;
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___WorldPosition_7;
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___WorldNormal_8;
	float ___Distance_9;
};

// TouchScript.Pointers.Pointer_PointerButtonState
struct  PointerButtonState_t2E590C457096B0F15ABFC17CC62AEDCAA1F68E31 
{
public:
	// System.Int32 TouchScript.Pointers.Pointer_PointerButtonState::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(PointerButtonState_t2E590C457096B0F15ABFC17CC62AEDCAA1F68E31, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// TouchScript.Pointers.Pointer_PointerType
struct  PointerType_t3FA6771F37343BF7F3F1070EB36821D6A1C30F74 
{
public:
	// System.Int32 TouchScript.Pointers.Pointer_PointerType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(PointerType_t3FA6771F37343BF7F3F1070EB36821D6A1C30F74, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// TouchScript.TouchManager_FrameEvent
struct  FrameEvent_tEBA65ABA56D36C692FB1A49D6929A473CA7A3817  : public UnityEvent_t5C6DDC2FCDF7F5C1808F1DDFBAD27A383F5FE65F
{
public:

public:
};


// TouchScript.TouchManager_MessageName
struct  MessageName_t450BA987F2CEE58334768B847602C2465422A4BE 
{
public:
	// System.Int32 TouchScript.TouchManager_MessageName::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(MessageName_t450BA987F2CEE58334768B847602C2465422A4BE, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// TouchScript.TouchManager_MessageType
struct  MessageType_t5F220637E3DAE30C10F6A06260240E7F168F080D 
{
public:
	// System.Int32 TouchScript.TouchManager_MessageType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(MessageType_t5F220637E3DAE30C10F6A06260240E7F168F080D, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// TouchScript.TouchManager_PointerEvent
struct  PointerEvent_t46D6262BF503769AA7E077C49BB94DA4B83061DA  : public UnityEvent_1_tF49B9E69A03353E280A3371637BD7D7B4B691112
{
public:

public:
};


// TouchScript.Utils.Attributes.NullToggleAttribute
struct  NullToggleAttribute_tB95470BD28EF41A9B6EF4CC642D5D293DA28A5DF  : public PropertyAttribute_t25BFFC093C9C96E3CCF4EAB36F5DC6F937B1FA54
{
public:
	// System.Int32 TouchScript.Utils.Attributes.NullToggleAttribute::NullIntValue
	int32_t ___NullIntValue_0;
	// System.Single TouchScript.Utils.Attributes.NullToggleAttribute::NullFloatValue
	float ___NullFloatValue_1;
	// UnityEngine.Object TouchScript.Utils.Attributes.NullToggleAttribute::NullObjectValue
	Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 * ___NullObjectValue_2;

public:
	inline static int32_t get_offset_of_NullIntValue_0() { return static_cast<int32_t>(offsetof(NullToggleAttribute_tB95470BD28EF41A9B6EF4CC642D5D293DA28A5DF, ___NullIntValue_0)); }
	inline int32_t get_NullIntValue_0() const { return ___NullIntValue_0; }
	inline int32_t* get_address_of_NullIntValue_0() { return &___NullIntValue_0; }
	inline void set_NullIntValue_0(int32_t value)
	{
		___NullIntValue_0 = value;
	}

	inline static int32_t get_offset_of_NullFloatValue_1() { return static_cast<int32_t>(offsetof(NullToggleAttribute_tB95470BD28EF41A9B6EF4CC642D5D293DA28A5DF, ___NullFloatValue_1)); }
	inline float get_NullFloatValue_1() const { return ___NullFloatValue_1; }
	inline float* get_address_of_NullFloatValue_1() { return &___NullFloatValue_1; }
	inline void set_NullFloatValue_1(float value)
	{
		___NullFloatValue_1 = value;
	}

	inline static int32_t get_offset_of_NullObjectValue_2() { return static_cast<int32_t>(offsetof(NullToggleAttribute_tB95470BD28EF41A9B6EF4CC642D5D293DA28A5DF, ___NullObjectValue_2)); }
	inline Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 * get_NullObjectValue_2() const { return ___NullObjectValue_2; }
	inline Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 ** get_address_of_NullObjectValue_2() { return &___NullObjectValue_2; }
	inline void set_NullObjectValue_2(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 * value)
	{
		___NullObjectValue_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___NullObjectValue_2), (void*)value);
	}
};


// TouchScript.Utils.Attributes.ToggleLeftAttribute
struct  ToggleLeftAttribute_tBDFA12F6A17E62E0928F69045C9267C34DD8AAC1  : public PropertyAttribute_t25BFFC093C9C96E3CCF4EAB36F5DC6F937B1FA54
{
public:

public:
};


// UnityEngine.Object
struct  Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};

// UnityEngine.Plane
struct  Plane_t0903921088DEEDE1BCDEA5BF279EDBCFC9679AED 
{
public:
	// UnityEngine.Vector3 UnityEngine.Plane::m_Normal
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___m_Normal_1;
	// System.Single UnityEngine.Plane::m_Distance
	float ___m_Distance_2;

public:
	inline static int32_t get_offset_of_m_Normal_1() { return static_cast<int32_t>(offsetof(Plane_t0903921088DEEDE1BCDEA5BF279EDBCFC9679AED, ___m_Normal_1)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_m_Normal_1() const { return ___m_Normal_1; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_m_Normal_1() { return &___m_Normal_1; }
	inline void set_m_Normal_1(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___m_Normal_1 = value;
	}

	inline static int32_t get_offset_of_m_Distance_2() { return static_cast<int32_t>(offsetof(Plane_t0903921088DEEDE1BCDEA5BF279EDBCFC9679AED, ___m_Distance_2)); }
	inline float get_m_Distance_2() const { return ___m_Distance_2; }
	inline float* get_address_of_m_Distance_2() { return &___m_Distance_2; }
	inline void set_m_Distance_2(float value)
	{
		___m_Distance_2 = value;
	}
};


// UnityEngine.Ray
struct  Ray_tE2163D4CB3E6B267E29F8ABE41684490E4A614B2 
{
public:
	// UnityEngine.Vector3 UnityEngine.Ray::m_Origin
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___m_Origin_0;
	// UnityEngine.Vector3 UnityEngine.Ray::m_Direction
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___m_Direction_1;

public:
	inline static int32_t get_offset_of_m_Origin_0() { return static_cast<int32_t>(offsetof(Ray_tE2163D4CB3E6B267E29F8ABE41684490E4A614B2, ___m_Origin_0)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_m_Origin_0() const { return ___m_Origin_0; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_m_Origin_0() { return &___m_Origin_0; }
	inline void set_m_Origin_0(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___m_Origin_0 = value;
	}

	inline static int32_t get_offset_of_m_Direction_1() { return static_cast<int32_t>(offsetof(Ray_tE2163D4CB3E6B267E29F8ABE41684490E4A614B2, ___m_Direction_1)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_m_Direction_1() const { return ___m_Direction_1; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_m_Direction_1() { return &___m_Direction_1; }
	inline void set_m_Direction_1(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___m_Direction_1 = value;
	}
};


// UnityEngine.RaycastHit
struct  RaycastHit_t19695F18F9265FE5425062BBA6A4D330480538C3 
{
public:
	// UnityEngine.Vector3 UnityEngine.RaycastHit::m_Point
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___m_Point_0;
	// UnityEngine.Vector3 UnityEngine.RaycastHit::m_Normal
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___m_Normal_1;
	// System.UInt32 UnityEngine.RaycastHit::m_FaceID
	uint32_t ___m_FaceID_2;
	// System.Single UnityEngine.RaycastHit::m_Distance
	float ___m_Distance_3;
	// UnityEngine.Vector2 UnityEngine.RaycastHit::m_UV
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___m_UV_4;
	// System.Int32 UnityEngine.RaycastHit::m_Collider
	int32_t ___m_Collider_5;

public:
	inline static int32_t get_offset_of_m_Point_0() { return static_cast<int32_t>(offsetof(RaycastHit_t19695F18F9265FE5425062BBA6A4D330480538C3, ___m_Point_0)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_m_Point_0() const { return ___m_Point_0; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_m_Point_0() { return &___m_Point_0; }
	inline void set_m_Point_0(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___m_Point_0 = value;
	}

	inline static int32_t get_offset_of_m_Normal_1() { return static_cast<int32_t>(offsetof(RaycastHit_t19695F18F9265FE5425062BBA6A4D330480538C3, ___m_Normal_1)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_m_Normal_1() const { return ___m_Normal_1; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_m_Normal_1() { return &___m_Normal_1; }
	inline void set_m_Normal_1(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___m_Normal_1 = value;
	}

	inline static int32_t get_offset_of_m_FaceID_2() { return static_cast<int32_t>(offsetof(RaycastHit_t19695F18F9265FE5425062BBA6A4D330480538C3, ___m_FaceID_2)); }
	inline uint32_t get_m_FaceID_2() const { return ___m_FaceID_2; }
	inline uint32_t* get_address_of_m_FaceID_2() { return &___m_FaceID_2; }
	inline void set_m_FaceID_2(uint32_t value)
	{
		___m_FaceID_2 = value;
	}

	inline static int32_t get_offset_of_m_Distance_3() { return static_cast<int32_t>(offsetof(RaycastHit_t19695F18F9265FE5425062BBA6A4D330480538C3, ___m_Distance_3)); }
	inline float get_m_Distance_3() const { return ___m_Distance_3; }
	inline float* get_address_of_m_Distance_3() { return &___m_Distance_3; }
	inline void set_m_Distance_3(float value)
	{
		___m_Distance_3 = value;
	}

	inline static int32_t get_offset_of_m_UV_4() { return static_cast<int32_t>(offsetof(RaycastHit_t19695F18F9265FE5425062BBA6A4D330480538C3, ___m_UV_4)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_m_UV_4() const { return ___m_UV_4; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_m_UV_4() { return &___m_UV_4; }
	inline void set_m_UV_4(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___m_UV_4 = value;
	}

	inline static int32_t get_offset_of_m_Collider_5() { return static_cast<int32_t>(offsetof(RaycastHit_t19695F18F9265FE5425062BBA6A4D330480538C3, ___m_Collider_5)); }
	inline int32_t get_m_Collider_5() const { return ___m_Collider_5; }
	inline int32_t* get_address_of_m_Collider_5() { return &___m_Collider_5; }
	inline void set_m_Collider_5(int32_t value)
	{
		___m_Collider_5 = value;
	}
};


// UnityEngine.RaycastHit2D
struct  RaycastHit2D_t5E8A7F96317BAF2033362FC780F4D72DC72764BE 
{
public:
	// UnityEngine.Vector2 UnityEngine.RaycastHit2D::m_Centroid
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___m_Centroid_0;
	// UnityEngine.Vector2 UnityEngine.RaycastHit2D::m_Point
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___m_Point_1;
	// UnityEngine.Vector2 UnityEngine.RaycastHit2D::m_Normal
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___m_Normal_2;
	// System.Single UnityEngine.RaycastHit2D::m_Distance
	float ___m_Distance_3;
	// System.Single UnityEngine.RaycastHit2D::m_Fraction
	float ___m_Fraction_4;
	// System.Int32 UnityEngine.RaycastHit2D::m_Collider
	int32_t ___m_Collider_5;

public:
	inline static int32_t get_offset_of_m_Centroid_0() { return static_cast<int32_t>(offsetof(RaycastHit2D_t5E8A7F96317BAF2033362FC780F4D72DC72764BE, ___m_Centroid_0)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_m_Centroid_0() const { return ___m_Centroid_0; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_m_Centroid_0() { return &___m_Centroid_0; }
	inline void set_m_Centroid_0(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___m_Centroid_0 = value;
	}

	inline static int32_t get_offset_of_m_Point_1() { return static_cast<int32_t>(offsetof(RaycastHit2D_t5E8A7F96317BAF2033362FC780F4D72DC72764BE, ___m_Point_1)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_m_Point_1() const { return ___m_Point_1; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_m_Point_1() { return &___m_Point_1; }
	inline void set_m_Point_1(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___m_Point_1 = value;
	}

	inline static int32_t get_offset_of_m_Normal_2() { return static_cast<int32_t>(offsetof(RaycastHit2D_t5E8A7F96317BAF2033362FC780F4D72DC72764BE, ___m_Normal_2)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_m_Normal_2() const { return ___m_Normal_2; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_m_Normal_2() { return &___m_Normal_2; }
	inline void set_m_Normal_2(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___m_Normal_2 = value;
	}

	inline static int32_t get_offset_of_m_Distance_3() { return static_cast<int32_t>(offsetof(RaycastHit2D_t5E8A7F96317BAF2033362FC780F4D72DC72764BE, ___m_Distance_3)); }
	inline float get_m_Distance_3() const { return ___m_Distance_3; }
	inline float* get_address_of_m_Distance_3() { return &___m_Distance_3; }
	inline void set_m_Distance_3(float value)
	{
		___m_Distance_3 = value;
	}

	inline static int32_t get_offset_of_m_Fraction_4() { return static_cast<int32_t>(offsetof(RaycastHit2D_t5E8A7F96317BAF2033362FC780F4D72DC72764BE, ___m_Fraction_4)); }
	inline float get_m_Fraction_4() const { return ___m_Fraction_4; }
	inline float* get_address_of_m_Fraction_4() { return &___m_Fraction_4; }
	inline void set_m_Fraction_4(float value)
	{
		___m_Fraction_4 = value;
	}

	inline static int32_t get_offset_of_m_Collider_5() { return static_cast<int32_t>(offsetof(RaycastHit2D_t5E8A7F96317BAF2033362FC780F4D72DC72764BE, ___m_Collider_5)); }
	inline int32_t get_m_Collider_5() const { return ___m_Collider_5; }
	inline int32_t* get_address_of_m_Collider_5() { return &___m_Collider_5; }
	inline void set_m_Collider_5(int32_t value)
	{
		___m_Collider_5 = value;
	}
};


// UnityEngine.RenderMode
struct  RenderMode_tB54632E74CDC4A990E815EB8C3CC515D3A9E2F60 
{
public:
	// System.Int32 UnityEngine.RenderMode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(RenderMode_tB54632E74CDC4A990E815EB8C3CC515D3A9E2F60, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.SendMessageOptions
struct  SendMessageOptions_t4EA4645A7D0C4E0186BD7A984CDF4EE2C8F26250 
{
public:
	// System.Int32 UnityEngine.SendMessageOptions::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(SendMessageOptions_t4EA4645A7D0C4E0186BD7A984CDF4EE2C8F26250, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// System.MulticastDelegate
struct  MulticastDelegate_t  : public Delegate_t
{
public:
	// System.Delegate[] System.MulticastDelegate::delegates
	DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* ___delegates_11;

public:
	inline static int32_t get_offset_of_delegates_11() { return static_cast<int32_t>(offsetof(MulticastDelegate_t, ___delegates_11)); }
	inline DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* get_delegates_11() const { return ___delegates_11; }
	inline DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86** get_address_of_delegates_11() { return &___delegates_11; }
	inline void set_delegates_11(DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* value)
	{
		___delegates_11 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___delegates_11), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of System.MulticastDelegate
struct MulticastDelegate_t_marshaled_pinvoke : public Delegate_t_marshaled_pinvoke
{
	Delegate_t_marshaled_pinvoke** ___delegates_11;
};
// Native definition for COM marshalling of System.MulticastDelegate
struct MulticastDelegate_t_marshaled_com : public Delegate_t_marshaled_com
{
	Delegate_t_marshaled_com** ___delegates_11;
};

// TouchScript.Hit.HitData
struct  HitData_t3022C0DF180FDE3CF86F713B78A2056D02E78980 
{
public:
	// TouchScript.Hit.HitData_HitType TouchScript.Hit.HitData::type
	int32_t ___type_0;
	// UnityEngine.Transform TouchScript.Hit.HitData::target
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ___target_1;
	// System.Boolean TouchScript.Hit.HitData::screenSpace
	bool ___screenSpace_2;
	// TouchScript.Layers.TouchLayer TouchScript.Hit.HitData::layer
	TouchLayer_t484C3424D422E6073B3FE4DD21EFE05686F5AC54 * ___layer_3;
	// UnityEngine.RaycastHit TouchScript.Hit.HitData::raycastHit
	RaycastHit_t19695F18F9265FE5425062BBA6A4D330480538C3  ___raycastHit_4;
	// UnityEngine.RaycastHit2D TouchScript.Hit.HitData::raycastHit2D
	RaycastHit2D_t5E8A7F96317BAF2033362FC780F4D72DC72764BE  ___raycastHit2D_5;
	// TouchScript.Hit.RaycastHitUI TouchScript.Hit.HitData::raycastHitUI
	RaycastHitUI_tDB98E903E0E03D92E486668834F04300C1723874  ___raycastHitUI_6;
	// System.Int32 TouchScript.Hit.HitData::sortingLayer
	int32_t ___sortingLayer_7;
	// System.Int32 TouchScript.Hit.HitData::sortingOrder
	int32_t ___sortingOrder_8;

public:
	inline static int32_t get_offset_of_type_0() { return static_cast<int32_t>(offsetof(HitData_t3022C0DF180FDE3CF86F713B78A2056D02E78980, ___type_0)); }
	inline int32_t get_type_0() const { return ___type_0; }
	inline int32_t* get_address_of_type_0() { return &___type_0; }
	inline void set_type_0(int32_t value)
	{
		___type_0 = value;
	}

	inline static int32_t get_offset_of_target_1() { return static_cast<int32_t>(offsetof(HitData_t3022C0DF180FDE3CF86F713B78A2056D02E78980, ___target_1)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get_target_1() const { return ___target_1; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of_target_1() { return &___target_1; }
	inline void set_target_1(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		___target_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___target_1), (void*)value);
	}

	inline static int32_t get_offset_of_screenSpace_2() { return static_cast<int32_t>(offsetof(HitData_t3022C0DF180FDE3CF86F713B78A2056D02E78980, ___screenSpace_2)); }
	inline bool get_screenSpace_2() const { return ___screenSpace_2; }
	inline bool* get_address_of_screenSpace_2() { return &___screenSpace_2; }
	inline void set_screenSpace_2(bool value)
	{
		___screenSpace_2 = value;
	}

	inline static int32_t get_offset_of_layer_3() { return static_cast<int32_t>(offsetof(HitData_t3022C0DF180FDE3CF86F713B78A2056D02E78980, ___layer_3)); }
	inline TouchLayer_t484C3424D422E6073B3FE4DD21EFE05686F5AC54 * get_layer_3() const { return ___layer_3; }
	inline TouchLayer_t484C3424D422E6073B3FE4DD21EFE05686F5AC54 ** get_address_of_layer_3() { return &___layer_3; }
	inline void set_layer_3(TouchLayer_t484C3424D422E6073B3FE4DD21EFE05686F5AC54 * value)
	{
		___layer_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___layer_3), (void*)value);
	}

	inline static int32_t get_offset_of_raycastHit_4() { return static_cast<int32_t>(offsetof(HitData_t3022C0DF180FDE3CF86F713B78A2056D02E78980, ___raycastHit_4)); }
	inline RaycastHit_t19695F18F9265FE5425062BBA6A4D330480538C3  get_raycastHit_4() const { return ___raycastHit_4; }
	inline RaycastHit_t19695F18F9265FE5425062BBA6A4D330480538C3 * get_address_of_raycastHit_4() { return &___raycastHit_4; }
	inline void set_raycastHit_4(RaycastHit_t19695F18F9265FE5425062BBA6A4D330480538C3  value)
	{
		___raycastHit_4 = value;
	}

	inline static int32_t get_offset_of_raycastHit2D_5() { return static_cast<int32_t>(offsetof(HitData_t3022C0DF180FDE3CF86F713B78A2056D02E78980, ___raycastHit2D_5)); }
	inline RaycastHit2D_t5E8A7F96317BAF2033362FC780F4D72DC72764BE  get_raycastHit2D_5() const { return ___raycastHit2D_5; }
	inline RaycastHit2D_t5E8A7F96317BAF2033362FC780F4D72DC72764BE * get_address_of_raycastHit2D_5() { return &___raycastHit2D_5; }
	inline void set_raycastHit2D_5(RaycastHit2D_t5E8A7F96317BAF2033362FC780F4D72DC72764BE  value)
	{
		___raycastHit2D_5 = value;
	}

	inline static int32_t get_offset_of_raycastHitUI_6() { return static_cast<int32_t>(offsetof(HitData_t3022C0DF180FDE3CF86F713B78A2056D02E78980, ___raycastHitUI_6)); }
	inline RaycastHitUI_tDB98E903E0E03D92E486668834F04300C1723874  get_raycastHitUI_6() const { return ___raycastHitUI_6; }
	inline RaycastHitUI_tDB98E903E0E03D92E486668834F04300C1723874 * get_address_of_raycastHitUI_6() { return &___raycastHitUI_6; }
	inline void set_raycastHitUI_6(RaycastHitUI_tDB98E903E0E03D92E486668834F04300C1723874  value)
	{
		___raycastHitUI_6 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___raycastHitUI_6))->___Target_0), (void*)NULL);
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&(((&___raycastHitUI_6))->___Raycaster_1), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&(((&___raycastHitUI_6))->___Graphic_6), (void*)NULL);
		#endif
	}

	inline static int32_t get_offset_of_sortingLayer_7() { return static_cast<int32_t>(offsetof(HitData_t3022C0DF180FDE3CF86F713B78A2056D02E78980, ___sortingLayer_7)); }
	inline int32_t get_sortingLayer_7() const { return ___sortingLayer_7; }
	inline int32_t* get_address_of_sortingLayer_7() { return &___sortingLayer_7; }
	inline void set_sortingLayer_7(int32_t value)
	{
		___sortingLayer_7 = value;
	}

	inline static int32_t get_offset_of_sortingOrder_8() { return static_cast<int32_t>(offsetof(HitData_t3022C0DF180FDE3CF86F713B78A2056D02E78980, ___sortingOrder_8)); }
	inline int32_t get_sortingOrder_8() const { return ___sortingOrder_8; }
	inline int32_t* get_address_of_sortingOrder_8() { return &___sortingOrder_8; }
	inline void set_sortingOrder_8(int32_t value)
	{
		___sortingOrder_8 = value;
	}
};

// Native definition for P/Invoke marshalling of TouchScript.Hit.HitData
struct HitData_t3022C0DF180FDE3CF86F713B78A2056D02E78980_marshaled_pinvoke
{
	int32_t ___type_0;
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ___target_1;
	int32_t ___screenSpace_2;
	TouchLayer_t484C3424D422E6073B3FE4DD21EFE05686F5AC54 * ___layer_3;
	RaycastHit_t19695F18F9265FE5425062BBA6A4D330480538C3  ___raycastHit_4;
	RaycastHit2D_t5E8A7F96317BAF2033362FC780F4D72DC72764BE  ___raycastHit2D_5;
	RaycastHitUI_tDB98E903E0E03D92E486668834F04300C1723874_marshaled_pinvoke ___raycastHitUI_6;
	int32_t ___sortingLayer_7;
	int32_t ___sortingOrder_8;
};
// Native definition for COM marshalling of TouchScript.Hit.HitData
struct HitData_t3022C0DF180FDE3CF86F713B78A2056D02E78980_marshaled_com
{
	int32_t ___type_0;
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ___target_1;
	int32_t ___screenSpace_2;
	TouchLayer_t484C3424D422E6073B3FE4DD21EFE05686F5AC54 * ___layer_3;
	RaycastHit_t19695F18F9265FE5425062BBA6A4D330480538C3  ___raycastHit_4;
	RaycastHit2D_t5E8A7F96317BAF2033362FC780F4D72DC72764BE  ___raycastHit2D_5;
	RaycastHitUI_tDB98E903E0E03D92E486668834F04300C1723874_marshaled_com ___raycastHitUI_6;
	int32_t ___sortingLayer_7;
	int32_t ___sortingOrder_8;
};

// TouchScript.Layers.WorldSpaceCanvasProjectionParams
struct  WorldSpaceCanvasProjectionParams_t221503CDBFCD2E7D18AB639A41EFCB1F4BF05A3E  : public ProjectionParams_t2B7BDC4A52427186D2923201E1479C2E7641E6B8
{
public:
	// UnityEngine.Canvas TouchScript.Layers.WorldSpaceCanvasProjectionParams::canvas
	Canvas_tBC28BF1DD8D8499A89B5781505833D3728CF8591 * ___canvas_0;
	// UnityEngine.RenderMode TouchScript.Layers.WorldSpaceCanvasProjectionParams::mode
	int32_t ___mode_1;
	// UnityEngine.Camera TouchScript.Layers.WorldSpaceCanvasProjectionParams::camera
	Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * ___camera_2;

public:
	inline static int32_t get_offset_of_canvas_0() { return static_cast<int32_t>(offsetof(WorldSpaceCanvasProjectionParams_t221503CDBFCD2E7D18AB639A41EFCB1F4BF05A3E, ___canvas_0)); }
	inline Canvas_tBC28BF1DD8D8499A89B5781505833D3728CF8591 * get_canvas_0() const { return ___canvas_0; }
	inline Canvas_tBC28BF1DD8D8499A89B5781505833D3728CF8591 ** get_address_of_canvas_0() { return &___canvas_0; }
	inline void set_canvas_0(Canvas_tBC28BF1DD8D8499A89B5781505833D3728CF8591 * value)
	{
		___canvas_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___canvas_0), (void*)value);
	}

	inline static int32_t get_offset_of_mode_1() { return static_cast<int32_t>(offsetof(WorldSpaceCanvasProjectionParams_t221503CDBFCD2E7D18AB639A41EFCB1F4BF05A3E, ___mode_1)); }
	inline int32_t get_mode_1() const { return ___mode_1; }
	inline int32_t* get_address_of_mode_1() { return &___mode_1; }
	inline void set_mode_1(int32_t value)
	{
		___mode_1 = value;
	}

	inline static int32_t get_offset_of_camera_2() { return static_cast<int32_t>(offsetof(WorldSpaceCanvasProjectionParams_t221503CDBFCD2E7D18AB639A41EFCB1F4BF05A3E, ___camera_2)); }
	inline Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * get_camera_2() const { return ___camera_2; }
	inline Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 ** get_address_of_camera_2() { return &___camera_2; }
	inline void set_camera_2(Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * value)
	{
		___camera_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___camera_2), (void*)value);
	}
};


// TouchScript.Pointers.FakePointer
struct  FakePointer_t8C1AEEBB63CF9BCC8170772F4E4601CD4298BC47  : public RuntimeObject
{
public:
	// System.Int32 TouchScript.Pointers.FakePointer::<Id>k__BackingField
	int32_t ___U3CIdU3Ek__BackingField_0;
	// TouchScript.Pointers.Pointer_PointerType TouchScript.Pointers.FakePointer::<Type>k__BackingField
	int32_t ___U3CTypeU3Ek__BackingField_1;
	// TouchScript.InputSources.IInputSource TouchScript.Pointers.FakePointer::<InputSource>k__BackingField
	RuntimeObject* ___U3CInputSourceU3Ek__BackingField_2;
	// UnityEngine.Vector2 TouchScript.Pointers.FakePointer::<Position>k__BackingField
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___U3CPositionU3Ek__BackingField_3;
	// System.UInt32 TouchScript.Pointers.FakePointer::<Flags>k__BackingField
	uint32_t ___U3CFlagsU3Ek__BackingField_4;
	// TouchScript.Pointers.Pointer_PointerButtonState TouchScript.Pointers.FakePointer::<Buttons>k__BackingField
	int32_t ___U3CButtonsU3Ek__BackingField_5;
	// UnityEngine.Vector2 TouchScript.Pointers.FakePointer::<PreviousPosition>k__BackingField
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___U3CPreviousPositionU3Ek__BackingField_6;

public:
	inline static int32_t get_offset_of_U3CIdU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(FakePointer_t8C1AEEBB63CF9BCC8170772F4E4601CD4298BC47, ___U3CIdU3Ek__BackingField_0)); }
	inline int32_t get_U3CIdU3Ek__BackingField_0() const { return ___U3CIdU3Ek__BackingField_0; }
	inline int32_t* get_address_of_U3CIdU3Ek__BackingField_0() { return &___U3CIdU3Ek__BackingField_0; }
	inline void set_U3CIdU3Ek__BackingField_0(int32_t value)
	{
		___U3CIdU3Ek__BackingField_0 = value;
	}

	inline static int32_t get_offset_of_U3CTypeU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(FakePointer_t8C1AEEBB63CF9BCC8170772F4E4601CD4298BC47, ___U3CTypeU3Ek__BackingField_1)); }
	inline int32_t get_U3CTypeU3Ek__BackingField_1() const { return ___U3CTypeU3Ek__BackingField_1; }
	inline int32_t* get_address_of_U3CTypeU3Ek__BackingField_1() { return &___U3CTypeU3Ek__BackingField_1; }
	inline void set_U3CTypeU3Ek__BackingField_1(int32_t value)
	{
		___U3CTypeU3Ek__BackingField_1 = value;
	}

	inline static int32_t get_offset_of_U3CInputSourceU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(FakePointer_t8C1AEEBB63CF9BCC8170772F4E4601CD4298BC47, ___U3CInputSourceU3Ek__BackingField_2)); }
	inline RuntimeObject* get_U3CInputSourceU3Ek__BackingField_2() const { return ___U3CInputSourceU3Ek__BackingField_2; }
	inline RuntimeObject** get_address_of_U3CInputSourceU3Ek__BackingField_2() { return &___U3CInputSourceU3Ek__BackingField_2; }
	inline void set_U3CInputSourceU3Ek__BackingField_2(RuntimeObject* value)
	{
		___U3CInputSourceU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CInputSourceU3Ek__BackingField_2), (void*)value);
	}

	inline static int32_t get_offset_of_U3CPositionU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(FakePointer_t8C1AEEBB63CF9BCC8170772F4E4601CD4298BC47, ___U3CPositionU3Ek__BackingField_3)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_U3CPositionU3Ek__BackingField_3() const { return ___U3CPositionU3Ek__BackingField_3; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_U3CPositionU3Ek__BackingField_3() { return &___U3CPositionU3Ek__BackingField_3; }
	inline void set_U3CPositionU3Ek__BackingField_3(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___U3CPositionU3Ek__BackingField_3 = value;
	}

	inline static int32_t get_offset_of_U3CFlagsU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(FakePointer_t8C1AEEBB63CF9BCC8170772F4E4601CD4298BC47, ___U3CFlagsU3Ek__BackingField_4)); }
	inline uint32_t get_U3CFlagsU3Ek__BackingField_4() const { return ___U3CFlagsU3Ek__BackingField_4; }
	inline uint32_t* get_address_of_U3CFlagsU3Ek__BackingField_4() { return &___U3CFlagsU3Ek__BackingField_4; }
	inline void set_U3CFlagsU3Ek__BackingField_4(uint32_t value)
	{
		___U3CFlagsU3Ek__BackingField_4 = value;
	}

	inline static int32_t get_offset_of_U3CButtonsU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(FakePointer_t8C1AEEBB63CF9BCC8170772F4E4601CD4298BC47, ___U3CButtonsU3Ek__BackingField_5)); }
	inline int32_t get_U3CButtonsU3Ek__BackingField_5() const { return ___U3CButtonsU3Ek__BackingField_5; }
	inline int32_t* get_address_of_U3CButtonsU3Ek__BackingField_5() { return &___U3CButtonsU3Ek__BackingField_5; }
	inline void set_U3CButtonsU3Ek__BackingField_5(int32_t value)
	{
		___U3CButtonsU3Ek__BackingField_5 = value;
	}

	inline static int32_t get_offset_of_U3CPreviousPositionU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(FakePointer_t8C1AEEBB63CF9BCC8170772F4E4601CD4298BC47, ___U3CPreviousPositionU3Ek__BackingField_6)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_U3CPreviousPositionU3Ek__BackingField_6() const { return ___U3CPreviousPositionU3Ek__BackingField_6; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_U3CPreviousPositionU3Ek__BackingField_6() { return &___U3CPreviousPositionU3Ek__BackingField_6; }
	inline void set_U3CPreviousPositionU3Ek__BackingField_6(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___U3CPreviousPositionU3Ek__BackingField_6 = value;
	}
};


// UnityEngine.Component
struct  Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621  : public Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0
{
public:

public:
};


// UnityEngine.GameObject
struct  GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F  : public Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0
{
public:

public:
};


// UnityEngine.Material
struct  Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598  : public Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0
{
public:

public:
};


// UnityEngine.Shader
struct  Shader_tE2731FF351B74AB4186897484FB01E000C1160CA  : public Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0
{
public:

public:
};


// UnityEngine.Texture
struct  Texture_t387FE83BB848001FD06B14707AEA6D5A0F6A95F4  : public Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0
{
public:

public:
};

struct Texture_t387FE83BB848001FD06B14707AEA6D5A0F6A95F4_StaticFields
{
public:
	// System.Int32 UnityEngine.Texture::GenerateAllMips
	int32_t ___GenerateAllMips_4;

public:
	inline static int32_t get_offset_of_GenerateAllMips_4() { return static_cast<int32_t>(offsetof(Texture_t387FE83BB848001FD06B14707AEA6D5A0F6A95F4_StaticFields, ___GenerateAllMips_4)); }
	inline int32_t get_GenerateAllMips_4() const { return ___GenerateAllMips_4; }
	inline int32_t* get_address_of_GenerateAllMips_4() { return &___GenerateAllMips_4; }
	inline void set_GenerateAllMips_4(int32_t value)
	{
		___GenerateAllMips_4 = value;
	}
};


// NativeGallery_MediaPickCallback
struct  MediaPickCallback_t2F83A1D5E2DFEE08EECAE29F980B5BEDD1EEE77D  : public MulticastDelegate_t
{
public:

public:
};


// System.EventHandler
struct  EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C  : public MulticastDelegate_t
{
public:

public:
};


// System.EventHandler`1<TouchScript.PointerEventArgs>
struct  EventHandler_1_t0363FEC12482AFCCEA69CEA150D1710057B94330  : public MulticastDelegate_t
{
public:

public:
};


// TouchScript.Pointers.Pointer
struct  Pointer_tBF6873B314D4CC8534B6CD8A8BA54B92D8CFDE98  : public RuntimeObject
{
public:
	// System.Int32 TouchScript.Pointers.Pointer::<Id>k__BackingField
	int32_t ___U3CIdU3Ek__BackingField_4;
	// TouchScript.Pointers.Pointer_PointerType TouchScript.Pointers.Pointer::<Type>k__BackingField
	int32_t ___U3CTypeU3Ek__BackingField_5;
	// TouchScript.Pointers.Pointer_PointerButtonState TouchScript.Pointers.Pointer::<Buttons>k__BackingField
	int32_t ___U3CButtonsU3Ek__BackingField_6;
	// TouchScript.InputSources.IInputSource TouchScript.Pointers.Pointer::<InputSource>k__BackingField
	RuntimeObject* ___U3CInputSourceU3Ek__BackingField_7;
	// UnityEngine.Vector2 TouchScript.Pointers.Pointer::<PreviousPosition>k__BackingField
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___U3CPreviousPositionU3Ek__BackingField_8;
	// System.UInt32 TouchScript.Pointers.Pointer::<Flags>k__BackingField
	uint32_t ___U3CFlagsU3Ek__BackingField_9;
	// TouchScript.Core.LayerManagerInstance TouchScript.Pointers.Pointer::layerManager
	LayerManagerInstance_t88CDE54CE87FC7A715E326B47F38C3B0054D0578 * ___layerManager_11;
	// System.Int32 TouchScript.Pointers.Pointer::refCount
	int32_t ___refCount_12;
	// UnityEngine.Vector2 TouchScript.Pointers.Pointer::position
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___position_13;
	// UnityEngine.Vector2 TouchScript.Pointers.Pointer::newPosition
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___newPosition_14;
	// TouchScript.Hit.HitData TouchScript.Pointers.Pointer::pressData
	HitData_t3022C0DF180FDE3CF86F713B78A2056D02E78980  ___pressData_15;
	// TouchScript.Hit.HitData TouchScript.Pointers.Pointer::overData
	HitData_t3022C0DF180FDE3CF86F713B78A2056D02E78980  ___overData_16;
	// System.Boolean TouchScript.Pointers.Pointer::overDataIsDirty
	bool ___overDataIsDirty_17;

public:
	inline static int32_t get_offset_of_U3CIdU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(Pointer_tBF6873B314D4CC8534B6CD8A8BA54B92D8CFDE98, ___U3CIdU3Ek__BackingField_4)); }
	inline int32_t get_U3CIdU3Ek__BackingField_4() const { return ___U3CIdU3Ek__BackingField_4; }
	inline int32_t* get_address_of_U3CIdU3Ek__BackingField_4() { return &___U3CIdU3Ek__BackingField_4; }
	inline void set_U3CIdU3Ek__BackingField_4(int32_t value)
	{
		___U3CIdU3Ek__BackingField_4 = value;
	}

	inline static int32_t get_offset_of_U3CTypeU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(Pointer_tBF6873B314D4CC8534B6CD8A8BA54B92D8CFDE98, ___U3CTypeU3Ek__BackingField_5)); }
	inline int32_t get_U3CTypeU3Ek__BackingField_5() const { return ___U3CTypeU3Ek__BackingField_5; }
	inline int32_t* get_address_of_U3CTypeU3Ek__BackingField_5() { return &___U3CTypeU3Ek__BackingField_5; }
	inline void set_U3CTypeU3Ek__BackingField_5(int32_t value)
	{
		___U3CTypeU3Ek__BackingField_5 = value;
	}

	inline static int32_t get_offset_of_U3CButtonsU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(Pointer_tBF6873B314D4CC8534B6CD8A8BA54B92D8CFDE98, ___U3CButtonsU3Ek__BackingField_6)); }
	inline int32_t get_U3CButtonsU3Ek__BackingField_6() const { return ___U3CButtonsU3Ek__BackingField_6; }
	inline int32_t* get_address_of_U3CButtonsU3Ek__BackingField_6() { return &___U3CButtonsU3Ek__BackingField_6; }
	inline void set_U3CButtonsU3Ek__BackingField_6(int32_t value)
	{
		___U3CButtonsU3Ek__BackingField_6 = value;
	}

	inline static int32_t get_offset_of_U3CInputSourceU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(Pointer_tBF6873B314D4CC8534B6CD8A8BA54B92D8CFDE98, ___U3CInputSourceU3Ek__BackingField_7)); }
	inline RuntimeObject* get_U3CInputSourceU3Ek__BackingField_7() const { return ___U3CInputSourceU3Ek__BackingField_7; }
	inline RuntimeObject** get_address_of_U3CInputSourceU3Ek__BackingField_7() { return &___U3CInputSourceU3Ek__BackingField_7; }
	inline void set_U3CInputSourceU3Ek__BackingField_7(RuntimeObject* value)
	{
		___U3CInputSourceU3Ek__BackingField_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CInputSourceU3Ek__BackingField_7), (void*)value);
	}

	inline static int32_t get_offset_of_U3CPreviousPositionU3Ek__BackingField_8() { return static_cast<int32_t>(offsetof(Pointer_tBF6873B314D4CC8534B6CD8A8BA54B92D8CFDE98, ___U3CPreviousPositionU3Ek__BackingField_8)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_U3CPreviousPositionU3Ek__BackingField_8() const { return ___U3CPreviousPositionU3Ek__BackingField_8; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_U3CPreviousPositionU3Ek__BackingField_8() { return &___U3CPreviousPositionU3Ek__BackingField_8; }
	inline void set_U3CPreviousPositionU3Ek__BackingField_8(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___U3CPreviousPositionU3Ek__BackingField_8 = value;
	}

	inline static int32_t get_offset_of_U3CFlagsU3Ek__BackingField_9() { return static_cast<int32_t>(offsetof(Pointer_tBF6873B314D4CC8534B6CD8A8BA54B92D8CFDE98, ___U3CFlagsU3Ek__BackingField_9)); }
	inline uint32_t get_U3CFlagsU3Ek__BackingField_9() const { return ___U3CFlagsU3Ek__BackingField_9; }
	inline uint32_t* get_address_of_U3CFlagsU3Ek__BackingField_9() { return &___U3CFlagsU3Ek__BackingField_9; }
	inline void set_U3CFlagsU3Ek__BackingField_9(uint32_t value)
	{
		___U3CFlagsU3Ek__BackingField_9 = value;
	}

	inline static int32_t get_offset_of_layerManager_11() { return static_cast<int32_t>(offsetof(Pointer_tBF6873B314D4CC8534B6CD8A8BA54B92D8CFDE98, ___layerManager_11)); }
	inline LayerManagerInstance_t88CDE54CE87FC7A715E326B47F38C3B0054D0578 * get_layerManager_11() const { return ___layerManager_11; }
	inline LayerManagerInstance_t88CDE54CE87FC7A715E326B47F38C3B0054D0578 ** get_address_of_layerManager_11() { return &___layerManager_11; }
	inline void set_layerManager_11(LayerManagerInstance_t88CDE54CE87FC7A715E326B47F38C3B0054D0578 * value)
	{
		___layerManager_11 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___layerManager_11), (void*)value);
	}

	inline static int32_t get_offset_of_refCount_12() { return static_cast<int32_t>(offsetof(Pointer_tBF6873B314D4CC8534B6CD8A8BA54B92D8CFDE98, ___refCount_12)); }
	inline int32_t get_refCount_12() const { return ___refCount_12; }
	inline int32_t* get_address_of_refCount_12() { return &___refCount_12; }
	inline void set_refCount_12(int32_t value)
	{
		___refCount_12 = value;
	}

	inline static int32_t get_offset_of_position_13() { return static_cast<int32_t>(offsetof(Pointer_tBF6873B314D4CC8534B6CD8A8BA54B92D8CFDE98, ___position_13)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_position_13() const { return ___position_13; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_position_13() { return &___position_13; }
	inline void set_position_13(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___position_13 = value;
	}

	inline static int32_t get_offset_of_newPosition_14() { return static_cast<int32_t>(offsetof(Pointer_tBF6873B314D4CC8534B6CD8A8BA54B92D8CFDE98, ___newPosition_14)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_newPosition_14() const { return ___newPosition_14; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_newPosition_14() { return &___newPosition_14; }
	inline void set_newPosition_14(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___newPosition_14 = value;
	}

	inline static int32_t get_offset_of_pressData_15() { return static_cast<int32_t>(offsetof(Pointer_tBF6873B314D4CC8534B6CD8A8BA54B92D8CFDE98, ___pressData_15)); }
	inline HitData_t3022C0DF180FDE3CF86F713B78A2056D02E78980  get_pressData_15() const { return ___pressData_15; }
	inline HitData_t3022C0DF180FDE3CF86F713B78A2056D02E78980 * get_address_of_pressData_15() { return &___pressData_15; }
	inline void set_pressData_15(HitData_t3022C0DF180FDE3CF86F713B78A2056D02E78980  value)
	{
		___pressData_15 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___pressData_15))->___target_1), (void*)NULL);
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&(((&___pressData_15))->___layer_3), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___pressData_15))->___raycastHitUI_6))->___Target_0), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___pressData_15))->___raycastHitUI_6))->___Raycaster_1), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___pressData_15))->___raycastHitUI_6))->___Graphic_6), (void*)NULL);
		#endif
	}

	inline static int32_t get_offset_of_overData_16() { return static_cast<int32_t>(offsetof(Pointer_tBF6873B314D4CC8534B6CD8A8BA54B92D8CFDE98, ___overData_16)); }
	inline HitData_t3022C0DF180FDE3CF86F713B78A2056D02E78980  get_overData_16() const { return ___overData_16; }
	inline HitData_t3022C0DF180FDE3CF86F713B78A2056D02E78980 * get_address_of_overData_16() { return &___overData_16; }
	inline void set_overData_16(HitData_t3022C0DF180FDE3CF86F713B78A2056D02E78980  value)
	{
		___overData_16 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___overData_16))->___target_1), (void*)NULL);
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&(((&___overData_16))->___layer_3), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___overData_16))->___raycastHitUI_6))->___Target_0), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___overData_16))->___raycastHitUI_6))->___Raycaster_1), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___overData_16))->___raycastHitUI_6))->___Graphic_6), (void*)NULL);
		#endif
	}

	inline static int32_t get_offset_of_overDataIsDirty_17() { return static_cast<int32_t>(offsetof(Pointer_tBF6873B314D4CC8534B6CD8A8BA54B92D8CFDE98, ___overDataIsDirty_17)); }
	inline bool get_overDataIsDirty_17() const { return ___overDataIsDirty_17; }
	inline bool* get_address_of_overDataIsDirty_17() { return &___overDataIsDirty_17; }
	inline void set_overDataIsDirty_17(bool value)
	{
		___overDataIsDirty_17 = value;
	}
};

struct Pointer_tBF6873B314D4CC8534B6CD8A8BA54B92D8CFDE98_StaticFields
{
public:
	// System.Text.StringBuilder TouchScript.Pointers.Pointer::builder
	StringBuilder_t * ___builder_10;

public:
	inline static int32_t get_offset_of_builder_10() { return static_cast<int32_t>(offsetof(Pointer_tBF6873B314D4CC8534B6CD8A8BA54B92D8CFDE98_StaticFields, ___builder_10)); }
	inline StringBuilder_t * get_builder_10() const { return ___builder_10; }
	inline StringBuilder_t ** get_address_of_builder_10() { return &___builder_10; }
	inline void set_builder_10(StringBuilder_t * value)
	{
		___builder_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___builder_10), (void*)value);
	}
};


// UnityEngine.Behaviour
struct  Behaviour_tBDC7E9C3C898AD8348891B82D3E345801D920CA8  : public Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621
{
public:

public:
};


// UnityEngine.Renderer
struct  Renderer_t0556D67DD582620D1F495627EDE30D03284151F4  : public Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621
{
public:

public:
};


// UnityEngine.Texture2D
struct  Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C  : public Texture_t387FE83BB848001FD06B14707AEA6D5A0F6A95F4
{
public:

public:
};


// UnityEngine.Transform
struct  Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA  : public Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621
{
public:

public:
};


// TouchScript.Pointers.MousePointer
struct  MousePointer_tEFB01FDD2340977461DEB8153349F62DFDE0E100  : public Pointer_tBF6873B314D4CC8534B6CD8A8BA54B92D8CFDE98
{
public:
	// UnityEngine.Vector2 TouchScript.Pointers.MousePointer::<ScrollDelta>k__BackingField
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___U3CScrollDeltaU3Ek__BackingField_18;

public:
	inline static int32_t get_offset_of_U3CScrollDeltaU3Ek__BackingField_18() { return static_cast<int32_t>(offsetof(MousePointer_tEFB01FDD2340977461DEB8153349F62DFDE0E100, ___U3CScrollDeltaU3Ek__BackingField_18)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_U3CScrollDeltaU3Ek__BackingField_18() const { return ___U3CScrollDeltaU3Ek__BackingField_18; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_U3CScrollDeltaU3Ek__BackingField_18() { return &___U3CScrollDeltaU3Ek__BackingField_18; }
	inline void set_U3CScrollDeltaU3Ek__BackingField_18(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___U3CScrollDeltaU3Ek__BackingField_18 = value;
	}
};


// TouchScript.Pointers.ObjectPointer
struct  ObjectPointer_tD190FCA2463DDBBD72FBA988696DFC51EFF46C36  : public Pointer_tBF6873B314D4CC8534B6CD8A8BA54B92D8CFDE98
{
public:
	// System.Int32 TouchScript.Pointers.ObjectPointer::<ObjectId>k__BackingField
	int32_t ___U3CObjectIdU3Ek__BackingField_22;
	// System.Single TouchScript.Pointers.ObjectPointer::<Width>k__BackingField
	float ___U3CWidthU3Ek__BackingField_23;
	// System.Single TouchScript.Pointers.ObjectPointer::<Height>k__BackingField
	float ___U3CHeightU3Ek__BackingField_24;
	// System.Single TouchScript.Pointers.ObjectPointer::<Angle>k__BackingField
	float ___U3CAngleU3Ek__BackingField_25;

public:
	inline static int32_t get_offset_of_U3CObjectIdU3Ek__BackingField_22() { return static_cast<int32_t>(offsetof(ObjectPointer_tD190FCA2463DDBBD72FBA988696DFC51EFF46C36, ___U3CObjectIdU3Ek__BackingField_22)); }
	inline int32_t get_U3CObjectIdU3Ek__BackingField_22() const { return ___U3CObjectIdU3Ek__BackingField_22; }
	inline int32_t* get_address_of_U3CObjectIdU3Ek__BackingField_22() { return &___U3CObjectIdU3Ek__BackingField_22; }
	inline void set_U3CObjectIdU3Ek__BackingField_22(int32_t value)
	{
		___U3CObjectIdU3Ek__BackingField_22 = value;
	}

	inline static int32_t get_offset_of_U3CWidthU3Ek__BackingField_23() { return static_cast<int32_t>(offsetof(ObjectPointer_tD190FCA2463DDBBD72FBA988696DFC51EFF46C36, ___U3CWidthU3Ek__BackingField_23)); }
	inline float get_U3CWidthU3Ek__BackingField_23() const { return ___U3CWidthU3Ek__BackingField_23; }
	inline float* get_address_of_U3CWidthU3Ek__BackingField_23() { return &___U3CWidthU3Ek__BackingField_23; }
	inline void set_U3CWidthU3Ek__BackingField_23(float value)
	{
		___U3CWidthU3Ek__BackingField_23 = value;
	}

	inline static int32_t get_offset_of_U3CHeightU3Ek__BackingField_24() { return static_cast<int32_t>(offsetof(ObjectPointer_tD190FCA2463DDBBD72FBA988696DFC51EFF46C36, ___U3CHeightU3Ek__BackingField_24)); }
	inline float get_U3CHeightU3Ek__BackingField_24() const { return ___U3CHeightU3Ek__BackingField_24; }
	inline float* get_address_of_U3CHeightU3Ek__BackingField_24() { return &___U3CHeightU3Ek__BackingField_24; }
	inline void set_U3CHeightU3Ek__BackingField_24(float value)
	{
		___U3CHeightU3Ek__BackingField_24 = value;
	}

	inline static int32_t get_offset_of_U3CAngleU3Ek__BackingField_25() { return static_cast<int32_t>(offsetof(ObjectPointer_tD190FCA2463DDBBD72FBA988696DFC51EFF46C36, ___U3CAngleU3Ek__BackingField_25)); }
	inline float get_U3CAngleU3Ek__BackingField_25() const { return ___U3CAngleU3Ek__BackingField_25; }
	inline float* get_address_of_U3CAngleU3Ek__BackingField_25() { return &___U3CAngleU3Ek__BackingField_25; }
	inline void set_U3CAngleU3Ek__BackingField_25(float value)
	{
		___U3CAngleU3Ek__BackingField_25 = value;
	}
};


// TouchScript.Pointers.PenPointer
struct  PenPointer_tAF65E3C674E3160B823CFBB7D82046A3FFD60340  : public Pointer_tBF6873B314D4CC8534B6CD8A8BA54B92D8CFDE98
{
public:
	// System.Single TouchScript.Pointers.PenPointer::<Rotation>k__BackingField
	float ___U3CRotationU3Ek__BackingField_20;
	// System.Single TouchScript.Pointers.PenPointer::<Pressure>k__BackingField
	float ___U3CPressureU3Ek__BackingField_21;

public:
	inline static int32_t get_offset_of_U3CRotationU3Ek__BackingField_20() { return static_cast<int32_t>(offsetof(PenPointer_tAF65E3C674E3160B823CFBB7D82046A3FFD60340, ___U3CRotationU3Ek__BackingField_20)); }
	inline float get_U3CRotationU3Ek__BackingField_20() const { return ___U3CRotationU3Ek__BackingField_20; }
	inline float* get_address_of_U3CRotationU3Ek__BackingField_20() { return &___U3CRotationU3Ek__BackingField_20; }
	inline void set_U3CRotationU3Ek__BackingField_20(float value)
	{
		___U3CRotationU3Ek__BackingField_20 = value;
	}

	inline static int32_t get_offset_of_U3CPressureU3Ek__BackingField_21() { return static_cast<int32_t>(offsetof(PenPointer_tAF65E3C674E3160B823CFBB7D82046A3FFD60340, ___U3CPressureU3Ek__BackingField_21)); }
	inline float get_U3CPressureU3Ek__BackingField_21() const { return ___U3CPressureU3Ek__BackingField_21; }
	inline float* get_address_of_U3CPressureU3Ek__BackingField_21() { return &___U3CPressureU3Ek__BackingField_21; }
	inline void set_U3CPressureU3Ek__BackingField_21(float value)
	{
		___U3CPressureU3Ek__BackingField_21 = value;
	}
};


// TouchScript.Pointers.TouchPointer
struct  TouchPointer_t35FC788E12AC007D8F90D6D28131910B61316942  : public Pointer_tBF6873B314D4CC8534B6CD8A8BA54B92D8CFDE98
{
public:
	// System.Single TouchScript.Pointers.TouchPointer::<Rotation>k__BackingField
	float ___U3CRotationU3Ek__BackingField_20;
	// System.Single TouchScript.Pointers.TouchPointer::<Pressure>k__BackingField
	float ___U3CPressureU3Ek__BackingField_21;

public:
	inline static int32_t get_offset_of_U3CRotationU3Ek__BackingField_20() { return static_cast<int32_t>(offsetof(TouchPointer_t35FC788E12AC007D8F90D6D28131910B61316942, ___U3CRotationU3Ek__BackingField_20)); }
	inline float get_U3CRotationU3Ek__BackingField_20() const { return ___U3CRotationU3Ek__BackingField_20; }
	inline float* get_address_of_U3CRotationU3Ek__BackingField_20() { return &___U3CRotationU3Ek__BackingField_20; }
	inline void set_U3CRotationU3Ek__BackingField_20(float value)
	{
		___U3CRotationU3Ek__BackingField_20 = value;
	}

	inline static int32_t get_offset_of_U3CPressureU3Ek__BackingField_21() { return static_cast<int32_t>(offsetof(TouchPointer_t35FC788E12AC007D8F90D6D28131910B61316942, ___U3CPressureU3Ek__BackingField_21)); }
	inline float get_U3CPressureU3Ek__BackingField_21() const { return ___U3CPressureU3Ek__BackingField_21; }
	inline float* get_address_of_U3CPressureU3Ek__BackingField_21() { return &___U3CPressureU3Ek__BackingField_21; }
	inline void set_U3CPressureU3Ek__BackingField_21(float value)
	{
		___U3CPressureU3Ek__BackingField_21 = value;
	}
};


// UnityEngine.Camera
struct  Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34  : public Behaviour_tBDC7E9C3C898AD8348891B82D3E345801D920CA8
{
public:

public:
};

struct Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34_StaticFields
{
public:
	// UnityEngine.Camera_CameraCallback UnityEngine.Camera::onPreCull
	CameraCallback_t8BBB42AA08D7498DFC11F4128117055BC7F0B9D0 * ___onPreCull_4;
	// UnityEngine.Camera_CameraCallback UnityEngine.Camera::onPreRender
	CameraCallback_t8BBB42AA08D7498DFC11F4128117055BC7F0B9D0 * ___onPreRender_5;
	// UnityEngine.Camera_CameraCallback UnityEngine.Camera::onPostRender
	CameraCallback_t8BBB42AA08D7498DFC11F4128117055BC7F0B9D0 * ___onPostRender_6;

public:
	inline static int32_t get_offset_of_onPreCull_4() { return static_cast<int32_t>(offsetof(Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34_StaticFields, ___onPreCull_4)); }
	inline CameraCallback_t8BBB42AA08D7498DFC11F4128117055BC7F0B9D0 * get_onPreCull_4() const { return ___onPreCull_4; }
	inline CameraCallback_t8BBB42AA08D7498DFC11F4128117055BC7F0B9D0 ** get_address_of_onPreCull_4() { return &___onPreCull_4; }
	inline void set_onPreCull_4(CameraCallback_t8BBB42AA08D7498DFC11F4128117055BC7F0B9D0 * value)
	{
		___onPreCull_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___onPreCull_4), (void*)value);
	}

	inline static int32_t get_offset_of_onPreRender_5() { return static_cast<int32_t>(offsetof(Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34_StaticFields, ___onPreRender_5)); }
	inline CameraCallback_t8BBB42AA08D7498DFC11F4128117055BC7F0B9D0 * get_onPreRender_5() const { return ___onPreRender_5; }
	inline CameraCallback_t8BBB42AA08D7498DFC11F4128117055BC7F0B9D0 ** get_address_of_onPreRender_5() { return &___onPreRender_5; }
	inline void set_onPreRender_5(CameraCallback_t8BBB42AA08D7498DFC11F4128117055BC7F0B9D0 * value)
	{
		___onPreRender_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___onPreRender_5), (void*)value);
	}

	inline static int32_t get_offset_of_onPostRender_6() { return static_cast<int32_t>(offsetof(Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34_StaticFields, ___onPostRender_6)); }
	inline CameraCallback_t8BBB42AA08D7498DFC11F4128117055BC7F0B9D0 * get_onPostRender_6() const { return ___onPostRender_6; }
	inline CameraCallback_t8BBB42AA08D7498DFC11F4128117055BC7F0B9D0 ** get_address_of_onPostRender_6() { return &___onPostRender_6; }
	inline void set_onPostRender_6(CameraCallback_t8BBB42AA08D7498DFC11F4128117055BC7F0B9D0 * value)
	{
		___onPostRender_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___onPostRender_6), (void*)value);
	}
};


// UnityEngine.Canvas
struct  Canvas_tBC28BF1DD8D8499A89B5781505833D3728CF8591  : public Behaviour_tBDC7E9C3C898AD8348891B82D3E345801D920CA8
{
public:

public:
};

struct Canvas_tBC28BF1DD8D8499A89B5781505833D3728CF8591_StaticFields
{
public:
	// UnityEngine.Canvas_WillRenderCanvases UnityEngine.Canvas::willRenderCanvases
	WillRenderCanvases_tBD5AD090B5938021DEAA679A5AEEA790F60A8BEE * ___willRenderCanvases_4;

public:
	inline static int32_t get_offset_of_willRenderCanvases_4() { return static_cast<int32_t>(offsetof(Canvas_tBC28BF1DD8D8499A89B5781505833D3728CF8591_StaticFields, ___willRenderCanvases_4)); }
	inline WillRenderCanvases_tBD5AD090B5938021DEAA679A5AEEA790F60A8BEE * get_willRenderCanvases_4() const { return ___willRenderCanvases_4; }
	inline WillRenderCanvases_tBD5AD090B5938021DEAA679A5AEEA790F60A8BEE ** get_address_of_willRenderCanvases_4() { return &___willRenderCanvases_4; }
	inline void set_willRenderCanvases_4(WillRenderCanvases_tBD5AD090B5938021DEAA679A5AEEA790F60A8BEE * value)
	{
		___willRenderCanvases_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___willRenderCanvases_4), (void*)value);
	}
};


// UnityEngine.MeshRenderer
struct  MeshRenderer_t9D67CA54E83315F743623BDE8EADCD5074659EED  : public Renderer_t0556D67DD582620D1F495627EDE30D03284151F4
{
public:

public:
};


// UnityEngine.MonoBehaviour
struct  MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429  : public Behaviour_tBDC7E9C3C898AD8348891B82D3E345801D920CA8
{
public:

public:
};


// TouchScript.Core.DebuggableMonoBehaviour
struct  DebuggableMonoBehaviour_t9ECFEBA1E6079CED2A7AF5B46939A05FBDD0C667  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:

public:
};


// TouchScript.Core.LayerManagerInstance
struct  LayerManagerInstance_t88CDE54CE87FC7A715E326B47F38C3B0054D0578  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// TouchScript.ITouchManager TouchScript.Core.LayerManagerInstance::manager
	RuntimeObject* ___manager_6;
	// System.Collections.Generic.List`1<TouchScript.Layers.TouchLayer> TouchScript.Core.LayerManagerInstance::layers
	List_1_tACFDA2C40746A89629E9D8D22CD4726B64D7C856 * ___layers_7;
	// System.Int32 TouchScript.Core.LayerManagerInstance::layerCount
	int32_t ___layerCount_8;
	// System.Collections.Generic.HashSet`1<System.Int32> TouchScript.Core.LayerManagerInstance::exclusive
	HashSet_1_tD16423F193A61077DD5FE7C8517877716AAFF11E * ___exclusive_9;
	// System.Int32 TouchScript.Core.LayerManagerInstance::exclusiveCount
	int32_t ___exclusiveCount_10;
	// System.Int32 TouchScript.Core.LayerManagerInstance::clearExclusiveDelay
	int32_t ___clearExclusiveDelay_11;
	// System.Collections.Generic.List`1<UnityEngine.Transform> TouchScript.Core.LayerManagerInstance::tmpList
	List_1_t4DBFD85DCFB946888856DBE52AC08C2AF69C4DBE * ___tmpList_12;

public:
	inline static int32_t get_offset_of_manager_6() { return static_cast<int32_t>(offsetof(LayerManagerInstance_t88CDE54CE87FC7A715E326B47F38C3B0054D0578, ___manager_6)); }
	inline RuntimeObject* get_manager_6() const { return ___manager_6; }
	inline RuntimeObject** get_address_of_manager_6() { return &___manager_6; }
	inline void set_manager_6(RuntimeObject* value)
	{
		___manager_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___manager_6), (void*)value);
	}

	inline static int32_t get_offset_of_layers_7() { return static_cast<int32_t>(offsetof(LayerManagerInstance_t88CDE54CE87FC7A715E326B47F38C3B0054D0578, ___layers_7)); }
	inline List_1_tACFDA2C40746A89629E9D8D22CD4726B64D7C856 * get_layers_7() const { return ___layers_7; }
	inline List_1_tACFDA2C40746A89629E9D8D22CD4726B64D7C856 ** get_address_of_layers_7() { return &___layers_7; }
	inline void set_layers_7(List_1_tACFDA2C40746A89629E9D8D22CD4726B64D7C856 * value)
	{
		___layers_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___layers_7), (void*)value);
	}

	inline static int32_t get_offset_of_layerCount_8() { return static_cast<int32_t>(offsetof(LayerManagerInstance_t88CDE54CE87FC7A715E326B47F38C3B0054D0578, ___layerCount_8)); }
	inline int32_t get_layerCount_8() const { return ___layerCount_8; }
	inline int32_t* get_address_of_layerCount_8() { return &___layerCount_8; }
	inline void set_layerCount_8(int32_t value)
	{
		___layerCount_8 = value;
	}

	inline static int32_t get_offset_of_exclusive_9() { return static_cast<int32_t>(offsetof(LayerManagerInstance_t88CDE54CE87FC7A715E326B47F38C3B0054D0578, ___exclusive_9)); }
	inline HashSet_1_tD16423F193A61077DD5FE7C8517877716AAFF11E * get_exclusive_9() const { return ___exclusive_9; }
	inline HashSet_1_tD16423F193A61077DD5FE7C8517877716AAFF11E ** get_address_of_exclusive_9() { return &___exclusive_9; }
	inline void set_exclusive_9(HashSet_1_tD16423F193A61077DD5FE7C8517877716AAFF11E * value)
	{
		___exclusive_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___exclusive_9), (void*)value);
	}

	inline static int32_t get_offset_of_exclusiveCount_10() { return static_cast<int32_t>(offsetof(LayerManagerInstance_t88CDE54CE87FC7A715E326B47F38C3B0054D0578, ___exclusiveCount_10)); }
	inline int32_t get_exclusiveCount_10() const { return ___exclusiveCount_10; }
	inline int32_t* get_address_of_exclusiveCount_10() { return &___exclusiveCount_10; }
	inline void set_exclusiveCount_10(int32_t value)
	{
		___exclusiveCount_10 = value;
	}

	inline static int32_t get_offset_of_clearExclusiveDelay_11() { return static_cast<int32_t>(offsetof(LayerManagerInstance_t88CDE54CE87FC7A715E326B47F38C3B0054D0578, ___clearExclusiveDelay_11)); }
	inline int32_t get_clearExclusiveDelay_11() const { return ___clearExclusiveDelay_11; }
	inline int32_t* get_address_of_clearExclusiveDelay_11() { return &___clearExclusiveDelay_11; }
	inline void set_clearExclusiveDelay_11(int32_t value)
	{
		___clearExclusiveDelay_11 = value;
	}

	inline static int32_t get_offset_of_tmpList_12() { return static_cast<int32_t>(offsetof(LayerManagerInstance_t88CDE54CE87FC7A715E326B47F38C3B0054D0578, ___tmpList_12)); }
	inline List_1_t4DBFD85DCFB946888856DBE52AC08C2AF69C4DBE * get_tmpList_12() const { return ___tmpList_12; }
	inline List_1_t4DBFD85DCFB946888856DBE52AC08C2AF69C4DBE ** get_address_of_tmpList_12() { return &___tmpList_12; }
	inline void set_tmpList_12(List_1_t4DBFD85DCFB946888856DBE52AC08C2AF69C4DBE * value)
	{
		___tmpList_12 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___tmpList_12), (void*)value);
	}
};

struct LayerManagerInstance_t88CDE54CE87FC7A715E326B47F38C3B0054D0578_StaticFields
{
public:
	// TouchScript.Core.LayerManagerInstance TouchScript.Core.LayerManagerInstance::instance
	LayerManagerInstance_t88CDE54CE87FC7A715E326B47F38C3B0054D0578 * ___instance_4;
	// System.Boolean TouchScript.Core.LayerManagerInstance::shuttingDown
	bool ___shuttingDown_5;

public:
	inline static int32_t get_offset_of_instance_4() { return static_cast<int32_t>(offsetof(LayerManagerInstance_t88CDE54CE87FC7A715E326B47F38C3B0054D0578_StaticFields, ___instance_4)); }
	inline LayerManagerInstance_t88CDE54CE87FC7A715E326B47F38C3B0054D0578 * get_instance_4() const { return ___instance_4; }
	inline LayerManagerInstance_t88CDE54CE87FC7A715E326B47F38C3B0054D0578 ** get_address_of_instance_4() { return &___instance_4; }
	inline void set_instance_4(LayerManagerInstance_t88CDE54CE87FC7A715E326B47F38C3B0054D0578 * value)
	{
		___instance_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___instance_4), (void*)value);
	}

	inline static int32_t get_offset_of_shuttingDown_5() { return static_cast<int32_t>(offsetof(LayerManagerInstance_t88CDE54CE87FC7A715E326B47F38C3B0054D0578_StaticFields, ___shuttingDown_5)); }
	inline bool get_shuttingDown_5() const { return ___shuttingDown_5; }
	inline bool* get_address_of_shuttingDown_5() { return &___shuttingDown_5; }
	inline void set_shuttingDown_5(bool value)
	{
		___shuttingDown_5 = value;
	}
};


// TouchScript.Layers.TouchLayer
struct  TouchLayer_t484C3424D422E6073B3FE4DD21EFE05686F5AC54  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.EventHandler`1<TouchScript.Layers.TouchLayerEventArgs> TouchScript.Layers.TouchLayer::pointerPressInvoker
	EventHandler_1_tE6641FE0F11DA09432F018258F89A1B5C3715220 * ___pointerPressInvoker_4;
	// System.String TouchScript.Layers.TouchLayer::Name
	String_t* ___Name_5;
	// TouchScript.Layers.ILayerDelegate TouchScript.Layers.TouchLayer::<Delegate>k__BackingField
	RuntimeObject* ___U3CDelegateU3Ek__BackingField_6;
	// TouchScript.Layers.ProjectionParams TouchScript.Layers.TouchLayer::layerProjectionParams
	ProjectionParams_t2B7BDC4A52427186D2923201E1479C2E7641E6B8 * ___layerProjectionParams_7;
	// TouchScript.ILayerManager TouchScript.Layers.TouchLayer::manager
	RuntimeObject* ___manager_8;
	// System.Collections.Generic.List`1<TouchScript.Hit.HitTest> TouchScript.Layers.TouchLayer::tmpHitTestList
	List_1_t2B70D9DC0409629A5438BC064BE6DD3A68B5CD1A * ___tmpHitTestList_9;

public:
	inline static int32_t get_offset_of_pointerPressInvoker_4() { return static_cast<int32_t>(offsetof(TouchLayer_t484C3424D422E6073B3FE4DD21EFE05686F5AC54, ___pointerPressInvoker_4)); }
	inline EventHandler_1_tE6641FE0F11DA09432F018258F89A1B5C3715220 * get_pointerPressInvoker_4() const { return ___pointerPressInvoker_4; }
	inline EventHandler_1_tE6641FE0F11DA09432F018258F89A1B5C3715220 ** get_address_of_pointerPressInvoker_4() { return &___pointerPressInvoker_4; }
	inline void set_pointerPressInvoker_4(EventHandler_1_tE6641FE0F11DA09432F018258F89A1B5C3715220 * value)
	{
		___pointerPressInvoker_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___pointerPressInvoker_4), (void*)value);
	}

	inline static int32_t get_offset_of_Name_5() { return static_cast<int32_t>(offsetof(TouchLayer_t484C3424D422E6073B3FE4DD21EFE05686F5AC54, ___Name_5)); }
	inline String_t* get_Name_5() const { return ___Name_5; }
	inline String_t** get_address_of_Name_5() { return &___Name_5; }
	inline void set_Name_5(String_t* value)
	{
		___Name_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Name_5), (void*)value);
	}

	inline static int32_t get_offset_of_U3CDelegateU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(TouchLayer_t484C3424D422E6073B3FE4DD21EFE05686F5AC54, ___U3CDelegateU3Ek__BackingField_6)); }
	inline RuntimeObject* get_U3CDelegateU3Ek__BackingField_6() const { return ___U3CDelegateU3Ek__BackingField_6; }
	inline RuntimeObject** get_address_of_U3CDelegateU3Ek__BackingField_6() { return &___U3CDelegateU3Ek__BackingField_6; }
	inline void set_U3CDelegateU3Ek__BackingField_6(RuntimeObject* value)
	{
		___U3CDelegateU3Ek__BackingField_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CDelegateU3Ek__BackingField_6), (void*)value);
	}

	inline static int32_t get_offset_of_layerProjectionParams_7() { return static_cast<int32_t>(offsetof(TouchLayer_t484C3424D422E6073B3FE4DD21EFE05686F5AC54, ___layerProjectionParams_7)); }
	inline ProjectionParams_t2B7BDC4A52427186D2923201E1479C2E7641E6B8 * get_layerProjectionParams_7() const { return ___layerProjectionParams_7; }
	inline ProjectionParams_t2B7BDC4A52427186D2923201E1479C2E7641E6B8 ** get_address_of_layerProjectionParams_7() { return &___layerProjectionParams_7; }
	inline void set_layerProjectionParams_7(ProjectionParams_t2B7BDC4A52427186D2923201E1479C2E7641E6B8 * value)
	{
		___layerProjectionParams_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___layerProjectionParams_7), (void*)value);
	}

	inline static int32_t get_offset_of_manager_8() { return static_cast<int32_t>(offsetof(TouchLayer_t484C3424D422E6073B3FE4DD21EFE05686F5AC54, ___manager_8)); }
	inline RuntimeObject* get_manager_8() const { return ___manager_8; }
	inline RuntimeObject** get_address_of_manager_8() { return &___manager_8; }
	inline void set_manager_8(RuntimeObject* value)
	{
		___manager_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___manager_8), (void*)value);
	}

	inline static int32_t get_offset_of_tmpHitTestList_9() { return static_cast<int32_t>(offsetof(TouchLayer_t484C3424D422E6073B3FE4DD21EFE05686F5AC54, ___tmpHitTestList_9)); }
	inline List_1_t2B70D9DC0409629A5438BC064BE6DD3A68B5CD1A * get_tmpHitTestList_9() const { return ___tmpHitTestList_9; }
	inline List_1_t2B70D9DC0409629A5438BC064BE6DD3A68B5CD1A ** get_address_of_tmpHitTestList_9() { return &___tmpHitTestList_9; }
	inline void set_tmpHitTestList_9(List_1_t2B70D9DC0409629A5438BC064BE6DD3A68B5CD1A * value)
	{
		___tmpHitTestList_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___tmpHitTestList_9), (void*)value);
	}
};


// imageBank
struct  imageBank_t5EEBC6CE9144A46186F0114E6CE8B23ACDC4CCE3  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.GameObject imageBank::oldQuad
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___oldQuad_4;

public:
	inline static int32_t get_offset_of_oldQuad_4() { return static_cast<int32_t>(offsetof(imageBank_t5EEBC6CE9144A46186F0114E6CE8B23ACDC4CCE3, ___oldQuad_4)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_oldQuad_4() const { return ___oldQuad_4; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_oldQuad_4() { return &___oldQuad_4; }
	inline void set_oldQuad_4(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___oldQuad_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___oldQuad_4), (void*)value);
	}
};


// test
struct  test_tBCA4AC3C1D45BA0DD176A21EC1F45D70CE6F1246  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:

public:
};


// test_nativegallery
struct  test_nativegallery_tAC993D2146BA62036BC7ADD8AFF36FC4CD783627  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.GameObject test_nativegallery::quad
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___quad_4;

public:
	inline static int32_t get_offset_of_quad_4() { return static_cast<int32_t>(offsetof(test_nativegallery_tAC993D2146BA62036BC7ADD8AFF36FC4CD783627, ___quad_4)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_quad_4() const { return ___quad_4; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_quad_4() { return &___quad_4; }
	inline void set_quad_4(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___quad_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___quad_4), (void*)value);
	}
};


// TouchScript.Core.TouchManagerInstance
struct  TouchManagerInstance_t2FC1653461034CC60588BF499439FF10FCC702CB  : public DebuggableMonoBehaviour_t9ECFEBA1E6079CED2A7AF5B46939A05FBDD0C667
{
public:
	// System.EventHandler`1<TouchScript.PointerEventArgs> TouchScript.Core.TouchManagerInstance::pointersAddedInvoker
	EventHandler_1_t0363FEC12482AFCCEA69CEA150D1710057B94330 * ___pointersAddedInvoker_4;
	// System.EventHandler`1<TouchScript.PointerEventArgs> TouchScript.Core.TouchManagerInstance::pointersUpdatedInvoker
	EventHandler_1_t0363FEC12482AFCCEA69CEA150D1710057B94330 * ___pointersUpdatedInvoker_5;
	// System.EventHandler`1<TouchScript.PointerEventArgs> TouchScript.Core.TouchManagerInstance::pointersPressedInvoker
	EventHandler_1_t0363FEC12482AFCCEA69CEA150D1710057B94330 * ___pointersPressedInvoker_6;
	// System.EventHandler`1<TouchScript.PointerEventArgs> TouchScript.Core.TouchManagerInstance::pointersReleasedInvoker
	EventHandler_1_t0363FEC12482AFCCEA69CEA150D1710057B94330 * ___pointersReleasedInvoker_7;
	// System.EventHandler`1<TouchScript.PointerEventArgs> TouchScript.Core.TouchManagerInstance::pointersRemovedInvoker
	EventHandler_1_t0363FEC12482AFCCEA69CEA150D1710057B94330 * ___pointersRemovedInvoker_8;
	// System.EventHandler`1<TouchScript.PointerEventArgs> TouchScript.Core.TouchManagerInstance::pointersCancelledInvoker
	EventHandler_1_t0363FEC12482AFCCEA69CEA150D1710057B94330 * ___pointersCancelledInvoker_9;
	// System.EventHandler TouchScript.Core.TouchManagerInstance::frameStartedInvoker
	EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C * ___frameStartedInvoker_10;
	// System.EventHandler TouchScript.Core.TouchManagerInstance::frameFinishedInvoker
	EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C * ___frameFinishedInvoker_11;
	// System.Boolean TouchScript.Core.TouchManagerInstance::<IsInsidePointerFrame>k__BackingField
	bool ___U3CIsInsidePointerFrameU3Ek__BackingField_12;
	// System.Boolean TouchScript.Core.TouchManagerInstance::shouldCreateCameraLayer
	bool ___shouldCreateCameraLayer_15;
	// System.Boolean TouchScript.Core.TouchManagerInstance::shouldCreateStandardInput
	bool ___shouldCreateStandardInput_16;
	// TouchScript.Devices.Display.IDisplayDevice TouchScript.Core.TouchManagerInstance::displayDevice
	RuntimeObject* ___displayDevice_17;
	// System.Single TouchScript.Core.TouchManagerInstance::dpi
	float ___dpi_18;
	// System.Single TouchScript.Core.TouchManagerInstance::dotsPerCentimeter
	float ___dotsPerCentimeter_19;
	// TouchScript.ILayerManager TouchScript.Core.TouchManagerInstance::layerManager
	RuntimeObject* ___layerManager_20;
	// System.Collections.Generic.List`1<TouchScript.InputSources.IInputSource> TouchScript.Core.TouchManagerInstance::inputs
	List_1_t3BE25EF6F647A730F976BA4F7D8D94EBB43DF746 * ___inputs_21;
	// System.Int32 TouchScript.Core.TouchManagerInstance::inputCount
	int32_t ___inputCount_22;
	// System.Collections.Generic.List`1<TouchScript.Pointers.Pointer> TouchScript.Core.TouchManagerInstance::pointers
	List_1_tA07951FCC7998CA46E72419B7C06C4D1DA1446C2 * ___pointers_23;
	// System.Collections.Generic.HashSet`1<TouchScript.Pointers.Pointer> TouchScript.Core.TouchManagerInstance::pressedPointers
	HashSet_1_t2EAB50A1455881DCD85499E2AF7A81A57399A1AB * ___pressedPointers_24;
	// System.Collections.Generic.Dictionary`2<System.Int32,TouchScript.Pointers.Pointer> TouchScript.Core.TouchManagerInstance::idToPointer
	Dictionary_2_tEA1D7FAE81C09FE63AC30DE7B11D50FB1FCCAC36 * ___idToPointer_25;
	// System.Collections.Generic.List`1<TouchScript.Pointers.Pointer> TouchScript.Core.TouchManagerInstance::pointersAdded
	List_1_tA07951FCC7998CA46E72419B7C06C4D1DA1446C2 * ___pointersAdded_26;
	// System.Collections.Generic.HashSet`1<System.Int32> TouchScript.Core.TouchManagerInstance::pointersUpdated
	HashSet_1_tD16423F193A61077DD5FE7C8517877716AAFF11E * ___pointersUpdated_27;
	// System.Collections.Generic.HashSet`1<System.Int32> TouchScript.Core.TouchManagerInstance::pointersPressed
	HashSet_1_tD16423F193A61077DD5FE7C8517877716AAFF11E * ___pointersPressed_28;
	// System.Collections.Generic.HashSet`1<System.Int32> TouchScript.Core.TouchManagerInstance::pointersReleased
	HashSet_1_tD16423F193A61077DD5FE7C8517877716AAFF11E * ___pointersReleased_29;
	// System.Collections.Generic.HashSet`1<System.Int32> TouchScript.Core.TouchManagerInstance::pointersRemoved
	HashSet_1_tD16423F193A61077DD5FE7C8517877716AAFF11E * ___pointersRemoved_30;
	// System.Collections.Generic.HashSet`1<System.Int32> TouchScript.Core.TouchManagerInstance::pointersCancelled
	HashSet_1_tD16423F193A61077DD5FE7C8517877716AAFF11E * ___pointersCancelled_31;
	// System.Int32 TouchScript.Core.TouchManagerInstance::nextPointerId
	int32_t ___nextPointerId_34;
	// System.Object TouchScript.Core.TouchManagerInstance::pointerLock
	RuntimeObject * ___pointerLock_35;
	// System.Func`2<TouchScript.Layers.TouchLayer,System.Boolean> TouchScript.Core.TouchManagerInstance::_layerAddPointer
	Func_2_t403D42E78D662E69EC347FD9CE1A3F2E0E1F6DFA * ____layerAddPointer_36;
	// System.Func`2<TouchScript.Layers.TouchLayer,System.Boolean> TouchScript.Core.TouchManagerInstance::_layerUpdatePointer
	Func_2_t403D42E78D662E69EC347FD9CE1A3F2E0E1F6DFA * ____layerUpdatePointer_37;
	// System.Func`2<TouchScript.Layers.TouchLayer,System.Boolean> TouchScript.Core.TouchManagerInstance::_layerRemovePointer
	Func_2_t403D42E78D662E69EC347FD9CE1A3F2E0E1F6DFA * ____layerRemovePointer_38;
	// System.Func`2<TouchScript.Layers.TouchLayer,System.Boolean> TouchScript.Core.TouchManagerInstance::_layerCancelPointer
	Func_2_t403D42E78D662E69EC347FD9CE1A3F2E0E1F6DFA * ____layerCancelPointer_39;
	// TouchScript.Pointers.Pointer TouchScript.Core.TouchManagerInstance::tmpPointer
	Pointer_tBF6873B314D4CC8534B6CD8A8BA54B92D8CFDE98 * ___tmpPointer_40;
	// UnityEngine.Profiling.CustomSampler TouchScript.Core.TouchManagerInstance::samplerUpdateInputs
	CustomSampler_tD50B25148FC97E173885F9C379C8F89F067343C8 * ___samplerUpdateInputs_41;
	// UnityEngine.Profiling.CustomSampler TouchScript.Core.TouchManagerInstance::samplerUpdateAdded
	CustomSampler_tD50B25148FC97E173885F9C379C8F89F067343C8 * ___samplerUpdateAdded_42;
	// UnityEngine.Profiling.CustomSampler TouchScript.Core.TouchManagerInstance::samplerUpdatePressed
	CustomSampler_tD50B25148FC97E173885F9C379C8F89F067343C8 * ___samplerUpdatePressed_43;
	// UnityEngine.Profiling.CustomSampler TouchScript.Core.TouchManagerInstance::samplerUpdateUpdated
	CustomSampler_tD50B25148FC97E173885F9C379C8F89F067343C8 * ___samplerUpdateUpdated_44;
	// UnityEngine.Profiling.CustomSampler TouchScript.Core.TouchManagerInstance::samplerUpdateReleased
	CustomSampler_tD50B25148FC97E173885F9C379C8F89F067343C8 * ___samplerUpdateReleased_45;
	// UnityEngine.Profiling.CustomSampler TouchScript.Core.TouchManagerInstance::samplerUpdateRemoved
	CustomSampler_tD50B25148FC97E173885F9C379C8F89F067343C8 * ___samplerUpdateRemoved_46;
	// UnityEngine.Profiling.CustomSampler TouchScript.Core.TouchManagerInstance::samplerUpdateCancelled
	CustomSampler_tD50B25148FC97E173885F9C379C8F89F067343C8 * ___samplerUpdateCancelled_47;

public:
	inline static int32_t get_offset_of_pointersAddedInvoker_4() { return static_cast<int32_t>(offsetof(TouchManagerInstance_t2FC1653461034CC60588BF499439FF10FCC702CB, ___pointersAddedInvoker_4)); }
	inline EventHandler_1_t0363FEC12482AFCCEA69CEA150D1710057B94330 * get_pointersAddedInvoker_4() const { return ___pointersAddedInvoker_4; }
	inline EventHandler_1_t0363FEC12482AFCCEA69CEA150D1710057B94330 ** get_address_of_pointersAddedInvoker_4() { return &___pointersAddedInvoker_4; }
	inline void set_pointersAddedInvoker_4(EventHandler_1_t0363FEC12482AFCCEA69CEA150D1710057B94330 * value)
	{
		___pointersAddedInvoker_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___pointersAddedInvoker_4), (void*)value);
	}

	inline static int32_t get_offset_of_pointersUpdatedInvoker_5() { return static_cast<int32_t>(offsetof(TouchManagerInstance_t2FC1653461034CC60588BF499439FF10FCC702CB, ___pointersUpdatedInvoker_5)); }
	inline EventHandler_1_t0363FEC12482AFCCEA69CEA150D1710057B94330 * get_pointersUpdatedInvoker_5() const { return ___pointersUpdatedInvoker_5; }
	inline EventHandler_1_t0363FEC12482AFCCEA69CEA150D1710057B94330 ** get_address_of_pointersUpdatedInvoker_5() { return &___pointersUpdatedInvoker_5; }
	inline void set_pointersUpdatedInvoker_5(EventHandler_1_t0363FEC12482AFCCEA69CEA150D1710057B94330 * value)
	{
		___pointersUpdatedInvoker_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___pointersUpdatedInvoker_5), (void*)value);
	}

	inline static int32_t get_offset_of_pointersPressedInvoker_6() { return static_cast<int32_t>(offsetof(TouchManagerInstance_t2FC1653461034CC60588BF499439FF10FCC702CB, ___pointersPressedInvoker_6)); }
	inline EventHandler_1_t0363FEC12482AFCCEA69CEA150D1710057B94330 * get_pointersPressedInvoker_6() const { return ___pointersPressedInvoker_6; }
	inline EventHandler_1_t0363FEC12482AFCCEA69CEA150D1710057B94330 ** get_address_of_pointersPressedInvoker_6() { return &___pointersPressedInvoker_6; }
	inline void set_pointersPressedInvoker_6(EventHandler_1_t0363FEC12482AFCCEA69CEA150D1710057B94330 * value)
	{
		___pointersPressedInvoker_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___pointersPressedInvoker_6), (void*)value);
	}

	inline static int32_t get_offset_of_pointersReleasedInvoker_7() { return static_cast<int32_t>(offsetof(TouchManagerInstance_t2FC1653461034CC60588BF499439FF10FCC702CB, ___pointersReleasedInvoker_7)); }
	inline EventHandler_1_t0363FEC12482AFCCEA69CEA150D1710057B94330 * get_pointersReleasedInvoker_7() const { return ___pointersReleasedInvoker_7; }
	inline EventHandler_1_t0363FEC12482AFCCEA69CEA150D1710057B94330 ** get_address_of_pointersReleasedInvoker_7() { return &___pointersReleasedInvoker_7; }
	inline void set_pointersReleasedInvoker_7(EventHandler_1_t0363FEC12482AFCCEA69CEA150D1710057B94330 * value)
	{
		___pointersReleasedInvoker_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___pointersReleasedInvoker_7), (void*)value);
	}

	inline static int32_t get_offset_of_pointersRemovedInvoker_8() { return static_cast<int32_t>(offsetof(TouchManagerInstance_t2FC1653461034CC60588BF499439FF10FCC702CB, ___pointersRemovedInvoker_8)); }
	inline EventHandler_1_t0363FEC12482AFCCEA69CEA150D1710057B94330 * get_pointersRemovedInvoker_8() const { return ___pointersRemovedInvoker_8; }
	inline EventHandler_1_t0363FEC12482AFCCEA69CEA150D1710057B94330 ** get_address_of_pointersRemovedInvoker_8() { return &___pointersRemovedInvoker_8; }
	inline void set_pointersRemovedInvoker_8(EventHandler_1_t0363FEC12482AFCCEA69CEA150D1710057B94330 * value)
	{
		___pointersRemovedInvoker_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___pointersRemovedInvoker_8), (void*)value);
	}

	inline static int32_t get_offset_of_pointersCancelledInvoker_9() { return static_cast<int32_t>(offsetof(TouchManagerInstance_t2FC1653461034CC60588BF499439FF10FCC702CB, ___pointersCancelledInvoker_9)); }
	inline EventHandler_1_t0363FEC12482AFCCEA69CEA150D1710057B94330 * get_pointersCancelledInvoker_9() const { return ___pointersCancelledInvoker_9; }
	inline EventHandler_1_t0363FEC12482AFCCEA69CEA150D1710057B94330 ** get_address_of_pointersCancelledInvoker_9() { return &___pointersCancelledInvoker_9; }
	inline void set_pointersCancelledInvoker_9(EventHandler_1_t0363FEC12482AFCCEA69CEA150D1710057B94330 * value)
	{
		___pointersCancelledInvoker_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___pointersCancelledInvoker_9), (void*)value);
	}

	inline static int32_t get_offset_of_frameStartedInvoker_10() { return static_cast<int32_t>(offsetof(TouchManagerInstance_t2FC1653461034CC60588BF499439FF10FCC702CB, ___frameStartedInvoker_10)); }
	inline EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C * get_frameStartedInvoker_10() const { return ___frameStartedInvoker_10; }
	inline EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C ** get_address_of_frameStartedInvoker_10() { return &___frameStartedInvoker_10; }
	inline void set_frameStartedInvoker_10(EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C * value)
	{
		___frameStartedInvoker_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___frameStartedInvoker_10), (void*)value);
	}

	inline static int32_t get_offset_of_frameFinishedInvoker_11() { return static_cast<int32_t>(offsetof(TouchManagerInstance_t2FC1653461034CC60588BF499439FF10FCC702CB, ___frameFinishedInvoker_11)); }
	inline EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C * get_frameFinishedInvoker_11() const { return ___frameFinishedInvoker_11; }
	inline EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C ** get_address_of_frameFinishedInvoker_11() { return &___frameFinishedInvoker_11; }
	inline void set_frameFinishedInvoker_11(EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C * value)
	{
		___frameFinishedInvoker_11 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___frameFinishedInvoker_11), (void*)value);
	}

	inline static int32_t get_offset_of_U3CIsInsidePointerFrameU3Ek__BackingField_12() { return static_cast<int32_t>(offsetof(TouchManagerInstance_t2FC1653461034CC60588BF499439FF10FCC702CB, ___U3CIsInsidePointerFrameU3Ek__BackingField_12)); }
	inline bool get_U3CIsInsidePointerFrameU3Ek__BackingField_12() const { return ___U3CIsInsidePointerFrameU3Ek__BackingField_12; }
	inline bool* get_address_of_U3CIsInsidePointerFrameU3Ek__BackingField_12() { return &___U3CIsInsidePointerFrameU3Ek__BackingField_12; }
	inline void set_U3CIsInsidePointerFrameU3Ek__BackingField_12(bool value)
	{
		___U3CIsInsidePointerFrameU3Ek__BackingField_12 = value;
	}

	inline static int32_t get_offset_of_shouldCreateCameraLayer_15() { return static_cast<int32_t>(offsetof(TouchManagerInstance_t2FC1653461034CC60588BF499439FF10FCC702CB, ___shouldCreateCameraLayer_15)); }
	inline bool get_shouldCreateCameraLayer_15() const { return ___shouldCreateCameraLayer_15; }
	inline bool* get_address_of_shouldCreateCameraLayer_15() { return &___shouldCreateCameraLayer_15; }
	inline void set_shouldCreateCameraLayer_15(bool value)
	{
		___shouldCreateCameraLayer_15 = value;
	}

	inline static int32_t get_offset_of_shouldCreateStandardInput_16() { return static_cast<int32_t>(offsetof(TouchManagerInstance_t2FC1653461034CC60588BF499439FF10FCC702CB, ___shouldCreateStandardInput_16)); }
	inline bool get_shouldCreateStandardInput_16() const { return ___shouldCreateStandardInput_16; }
	inline bool* get_address_of_shouldCreateStandardInput_16() { return &___shouldCreateStandardInput_16; }
	inline void set_shouldCreateStandardInput_16(bool value)
	{
		___shouldCreateStandardInput_16 = value;
	}

	inline static int32_t get_offset_of_displayDevice_17() { return static_cast<int32_t>(offsetof(TouchManagerInstance_t2FC1653461034CC60588BF499439FF10FCC702CB, ___displayDevice_17)); }
	inline RuntimeObject* get_displayDevice_17() const { return ___displayDevice_17; }
	inline RuntimeObject** get_address_of_displayDevice_17() { return &___displayDevice_17; }
	inline void set_displayDevice_17(RuntimeObject* value)
	{
		___displayDevice_17 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___displayDevice_17), (void*)value);
	}

	inline static int32_t get_offset_of_dpi_18() { return static_cast<int32_t>(offsetof(TouchManagerInstance_t2FC1653461034CC60588BF499439FF10FCC702CB, ___dpi_18)); }
	inline float get_dpi_18() const { return ___dpi_18; }
	inline float* get_address_of_dpi_18() { return &___dpi_18; }
	inline void set_dpi_18(float value)
	{
		___dpi_18 = value;
	}

	inline static int32_t get_offset_of_dotsPerCentimeter_19() { return static_cast<int32_t>(offsetof(TouchManagerInstance_t2FC1653461034CC60588BF499439FF10FCC702CB, ___dotsPerCentimeter_19)); }
	inline float get_dotsPerCentimeter_19() const { return ___dotsPerCentimeter_19; }
	inline float* get_address_of_dotsPerCentimeter_19() { return &___dotsPerCentimeter_19; }
	inline void set_dotsPerCentimeter_19(float value)
	{
		___dotsPerCentimeter_19 = value;
	}

	inline static int32_t get_offset_of_layerManager_20() { return static_cast<int32_t>(offsetof(TouchManagerInstance_t2FC1653461034CC60588BF499439FF10FCC702CB, ___layerManager_20)); }
	inline RuntimeObject* get_layerManager_20() const { return ___layerManager_20; }
	inline RuntimeObject** get_address_of_layerManager_20() { return &___layerManager_20; }
	inline void set_layerManager_20(RuntimeObject* value)
	{
		___layerManager_20 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___layerManager_20), (void*)value);
	}

	inline static int32_t get_offset_of_inputs_21() { return static_cast<int32_t>(offsetof(TouchManagerInstance_t2FC1653461034CC60588BF499439FF10FCC702CB, ___inputs_21)); }
	inline List_1_t3BE25EF6F647A730F976BA4F7D8D94EBB43DF746 * get_inputs_21() const { return ___inputs_21; }
	inline List_1_t3BE25EF6F647A730F976BA4F7D8D94EBB43DF746 ** get_address_of_inputs_21() { return &___inputs_21; }
	inline void set_inputs_21(List_1_t3BE25EF6F647A730F976BA4F7D8D94EBB43DF746 * value)
	{
		___inputs_21 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___inputs_21), (void*)value);
	}

	inline static int32_t get_offset_of_inputCount_22() { return static_cast<int32_t>(offsetof(TouchManagerInstance_t2FC1653461034CC60588BF499439FF10FCC702CB, ___inputCount_22)); }
	inline int32_t get_inputCount_22() const { return ___inputCount_22; }
	inline int32_t* get_address_of_inputCount_22() { return &___inputCount_22; }
	inline void set_inputCount_22(int32_t value)
	{
		___inputCount_22 = value;
	}

	inline static int32_t get_offset_of_pointers_23() { return static_cast<int32_t>(offsetof(TouchManagerInstance_t2FC1653461034CC60588BF499439FF10FCC702CB, ___pointers_23)); }
	inline List_1_tA07951FCC7998CA46E72419B7C06C4D1DA1446C2 * get_pointers_23() const { return ___pointers_23; }
	inline List_1_tA07951FCC7998CA46E72419B7C06C4D1DA1446C2 ** get_address_of_pointers_23() { return &___pointers_23; }
	inline void set_pointers_23(List_1_tA07951FCC7998CA46E72419B7C06C4D1DA1446C2 * value)
	{
		___pointers_23 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___pointers_23), (void*)value);
	}

	inline static int32_t get_offset_of_pressedPointers_24() { return static_cast<int32_t>(offsetof(TouchManagerInstance_t2FC1653461034CC60588BF499439FF10FCC702CB, ___pressedPointers_24)); }
	inline HashSet_1_t2EAB50A1455881DCD85499E2AF7A81A57399A1AB * get_pressedPointers_24() const { return ___pressedPointers_24; }
	inline HashSet_1_t2EAB50A1455881DCD85499E2AF7A81A57399A1AB ** get_address_of_pressedPointers_24() { return &___pressedPointers_24; }
	inline void set_pressedPointers_24(HashSet_1_t2EAB50A1455881DCD85499E2AF7A81A57399A1AB * value)
	{
		___pressedPointers_24 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___pressedPointers_24), (void*)value);
	}

	inline static int32_t get_offset_of_idToPointer_25() { return static_cast<int32_t>(offsetof(TouchManagerInstance_t2FC1653461034CC60588BF499439FF10FCC702CB, ___idToPointer_25)); }
	inline Dictionary_2_tEA1D7FAE81C09FE63AC30DE7B11D50FB1FCCAC36 * get_idToPointer_25() const { return ___idToPointer_25; }
	inline Dictionary_2_tEA1D7FAE81C09FE63AC30DE7B11D50FB1FCCAC36 ** get_address_of_idToPointer_25() { return &___idToPointer_25; }
	inline void set_idToPointer_25(Dictionary_2_tEA1D7FAE81C09FE63AC30DE7B11D50FB1FCCAC36 * value)
	{
		___idToPointer_25 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___idToPointer_25), (void*)value);
	}

	inline static int32_t get_offset_of_pointersAdded_26() { return static_cast<int32_t>(offsetof(TouchManagerInstance_t2FC1653461034CC60588BF499439FF10FCC702CB, ___pointersAdded_26)); }
	inline List_1_tA07951FCC7998CA46E72419B7C06C4D1DA1446C2 * get_pointersAdded_26() const { return ___pointersAdded_26; }
	inline List_1_tA07951FCC7998CA46E72419B7C06C4D1DA1446C2 ** get_address_of_pointersAdded_26() { return &___pointersAdded_26; }
	inline void set_pointersAdded_26(List_1_tA07951FCC7998CA46E72419B7C06C4D1DA1446C2 * value)
	{
		___pointersAdded_26 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___pointersAdded_26), (void*)value);
	}

	inline static int32_t get_offset_of_pointersUpdated_27() { return static_cast<int32_t>(offsetof(TouchManagerInstance_t2FC1653461034CC60588BF499439FF10FCC702CB, ___pointersUpdated_27)); }
	inline HashSet_1_tD16423F193A61077DD5FE7C8517877716AAFF11E * get_pointersUpdated_27() const { return ___pointersUpdated_27; }
	inline HashSet_1_tD16423F193A61077DD5FE7C8517877716AAFF11E ** get_address_of_pointersUpdated_27() { return &___pointersUpdated_27; }
	inline void set_pointersUpdated_27(HashSet_1_tD16423F193A61077DD5FE7C8517877716AAFF11E * value)
	{
		___pointersUpdated_27 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___pointersUpdated_27), (void*)value);
	}

	inline static int32_t get_offset_of_pointersPressed_28() { return static_cast<int32_t>(offsetof(TouchManagerInstance_t2FC1653461034CC60588BF499439FF10FCC702CB, ___pointersPressed_28)); }
	inline HashSet_1_tD16423F193A61077DD5FE7C8517877716AAFF11E * get_pointersPressed_28() const { return ___pointersPressed_28; }
	inline HashSet_1_tD16423F193A61077DD5FE7C8517877716AAFF11E ** get_address_of_pointersPressed_28() { return &___pointersPressed_28; }
	inline void set_pointersPressed_28(HashSet_1_tD16423F193A61077DD5FE7C8517877716AAFF11E * value)
	{
		___pointersPressed_28 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___pointersPressed_28), (void*)value);
	}

	inline static int32_t get_offset_of_pointersReleased_29() { return static_cast<int32_t>(offsetof(TouchManagerInstance_t2FC1653461034CC60588BF499439FF10FCC702CB, ___pointersReleased_29)); }
	inline HashSet_1_tD16423F193A61077DD5FE7C8517877716AAFF11E * get_pointersReleased_29() const { return ___pointersReleased_29; }
	inline HashSet_1_tD16423F193A61077DD5FE7C8517877716AAFF11E ** get_address_of_pointersReleased_29() { return &___pointersReleased_29; }
	inline void set_pointersReleased_29(HashSet_1_tD16423F193A61077DD5FE7C8517877716AAFF11E * value)
	{
		___pointersReleased_29 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___pointersReleased_29), (void*)value);
	}

	inline static int32_t get_offset_of_pointersRemoved_30() { return static_cast<int32_t>(offsetof(TouchManagerInstance_t2FC1653461034CC60588BF499439FF10FCC702CB, ___pointersRemoved_30)); }
	inline HashSet_1_tD16423F193A61077DD5FE7C8517877716AAFF11E * get_pointersRemoved_30() const { return ___pointersRemoved_30; }
	inline HashSet_1_tD16423F193A61077DD5FE7C8517877716AAFF11E ** get_address_of_pointersRemoved_30() { return &___pointersRemoved_30; }
	inline void set_pointersRemoved_30(HashSet_1_tD16423F193A61077DD5FE7C8517877716AAFF11E * value)
	{
		___pointersRemoved_30 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___pointersRemoved_30), (void*)value);
	}

	inline static int32_t get_offset_of_pointersCancelled_31() { return static_cast<int32_t>(offsetof(TouchManagerInstance_t2FC1653461034CC60588BF499439FF10FCC702CB, ___pointersCancelled_31)); }
	inline HashSet_1_tD16423F193A61077DD5FE7C8517877716AAFF11E * get_pointersCancelled_31() const { return ___pointersCancelled_31; }
	inline HashSet_1_tD16423F193A61077DD5FE7C8517877716AAFF11E ** get_address_of_pointersCancelled_31() { return &___pointersCancelled_31; }
	inline void set_pointersCancelled_31(HashSet_1_tD16423F193A61077DD5FE7C8517877716AAFF11E * value)
	{
		___pointersCancelled_31 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___pointersCancelled_31), (void*)value);
	}

	inline static int32_t get_offset_of_nextPointerId_34() { return static_cast<int32_t>(offsetof(TouchManagerInstance_t2FC1653461034CC60588BF499439FF10FCC702CB, ___nextPointerId_34)); }
	inline int32_t get_nextPointerId_34() const { return ___nextPointerId_34; }
	inline int32_t* get_address_of_nextPointerId_34() { return &___nextPointerId_34; }
	inline void set_nextPointerId_34(int32_t value)
	{
		___nextPointerId_34 = value;
	}

	inline static int32_t get_offset_of_pointerLock_35() { return static_cast<int32_t>(offsetof(TouchManagerInstance_t2FC1653461034CC60588BF499439FF10FCC702CB, ___pointerLock_35)); }
	inline RuntimeObject * get_pointerLock_35() const { return ___pointerLock_35; }
	inline RuntimeObject ** get_address_of_pointerLock_35() { return &___pointerLock_35; }
	inline void set_pointerLock_35(RuntimeObject * value)
	{
		___pointerLock_35 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___pointerLock_35), (void*)value);
	}

	inline static int32_t get_offset_of__layerAddPointer_36() { return static_cast<int32_t>(offsetof(TouchManagerInstance_t2FC1653461034CC60588BF499439FF10FCC702CB, ____layerAddPointer_36)); }
	inline Func_2_t403D42E78D662E69EC347FD9CE1A3F2E0E1F6DFA * get__layerAddPointer_36() const { return ____layerAddPointer_36; }
	inline Func_2_t403D42E78D662E69EC347FD9CE1A3F2E0E1F6DFA ** get_address_of__layerAddPointer_36() { return &____layerAddPointer_36; }
	inline void set__layerAddPointer_36(Func_2_t403D42E78D662E69EC347FD9CE1A3F2E0E1F6DFA * value)
	{
		____layerAddPointer_36 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____layerAddPointer_36), (void*)value);
	}

	inline static int32_t get_offset_of__layerUpdatePointer_37() { return static_cast<int32_t>(offsetof(TouchManagerInstance_t2FC1653461034CC60588BF499439FF10FCC702CB, ____layerUpdatePointer_37)); }
	inline Func_2_t403D42E78D662E69EC347FD9CE1A3F2E0E1F6DFA * get__layerUpdatePointer_37() const { return ____layerUpdatePointer_37; }
	inline Func_2_t403D42E78D662E69EC347FD9CE1A3F2E0E1F6DFA ** get_address_of__layerUpdatePointer_37() { return &____layerUpdatePointer_37; }
	inline void set__layerUpdatePointer_37(Func_2_t403D42E78D662E69EC347FD9CE1A3F2E0E1F6DFA * value)
	{
		____layerUpdatePointer_37 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____layerUpdatePointer_37), (void*)value);
	}

	inline static int32_t get_offset_of__layerRemovePointer_38() { return static_cast<int32_t>(offsetof(TouchManagerInstance_t2FC1653461034CC60588BF499439FF10FCC702CB, ____layerRemovePointer_38)); }
	inline Func_2_t403D42E78D662E69EC347FD9CE1A3F2E0E1F6DFA * get__layerRemovePointer_38() const { return ____layerRemovePointer_38; }
	inline Func_2_t403D42E78D662E69EC347FD9CE1A3F2E0E1F6DFA ** get_address_of__layerRemovePointer_38() { return &____layerRemovePointer_38; }
	inline void set__layerRemovePointer_38(Func_2_t403D42E78D662E69EC347FD9CE1A3F2E0E1F6DFA * value)
	{
		____layerRemovePointer_38 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____layerRemovePointer_38), (void*)value);
	}

	inline static int32_t get_offset_of__layerCancelPointer_39() { return static_cast<int32_t>(offsetof(TouchManagerInstance_t2FC1653461034CC60588BF499439FF10FCC702CB, ____layerCancelPointer_39)); }
	inline Func_2_t403D42E78D662E69EC347FD9CE1A3F2E0E1F6DFA * get__layerCancelPointer_39() const { return ____layerCancelPointer_39; }
	inline Func_2_t403D42E78D662E69EC347FD9CE1A3F2E0E1F6DFA ** get_address_of__layerCancelPointer_39() { return &____layerCancelPointer_39; }
	inline void set__layerCancelPointer_39(Func_2_t403D42E78D662E69EC347FD9CE1A3F2E0E1F6DFA * value)
	{
		____layerCancelPointer_39 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____layerCancelPointer_39), (void*)value);
	}

	inline static int32_t get_offset_of_tmpPointer_40() { return static_cast<int32_t>(offsetof(TouchManagerInstance_t2FC1653461034CC60588BF499439FF10FCC702CB, ___tmpPointer_40)); }
	inline Pointer_tBF6873B314D4CC8534B6CD8A8BA54B92D8CFDE98 * get_tmpPointer_40() const { return ___tmpPointer_40; }
	inline Pointer_tBF6873B314D4CC8534B6CD8A8BA54B92D8CFDE98 ** get_address_of_tmpPointer_40() { return &___tmpPointer_40; }
	inline void set_tmpPointer_40(Pointer_tBF6873B314D4CC8534B6CD8A8BA54B92D8CFDE98 * value)
	{
		___tmpPointer_40 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___tmpPointer_40), (void*)value);
	}

	inline static int32_t get_offset_of_samplerUpdateInputs_41() { return static_cast<int32_t>(offsetof(TouchManagerInstance_t2FC1653461034CC60588BF499439FF10FCC702CB, ___samplerUpdateInputs_41)); }
	inline CustomSampler_tD50B25148FC97E173885F9C379C8F89F067343C8 * get_samplerUpdateInputs_41() const { return ___samplerUpdateInputs_41; }
	inline CustomSampler_tD50B25148FC97E173885F9C379C8F89F067343C8 ** get_address_of_samplerUpdateInputs_41() { return &___samplerUpdateInputs_41; }
	inline void set_samplerUpdateInputs_41(CustomSampler_tD50B25148FC97E173885F9C379C8F89F067343C8 * value)
	{
		___samplerUpdateInputs_41 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___samplerUpdateInputs_41), (void*)value);
	}

	inline static int32_t get_offset_of_samplerUpdateAdded_42() { return static_cast<int32_t>(offsetof(TouchManagerInstance_t2FC1653461034CC60588BF499439FF10FCC702CB, ___samplerUpdateAdded_42)); }
	inline CustomSampler_tD50B25148FC97E173885F9C379C8F89F067343C8 * get_samplerUpdateAdded_42() const { return ___samplerUpdateAdded_42; }
	inline CustomSampler_tD50B25148FC97E173885F9C379C8F89F067343C8 ** get_address_of_samplerUpdateAdded_42() { return &___samplerUpdateAdded_42; }
	inline void set_samplerUpdateAdded_42(CustomSampler_tD50B25148FC97E173885F9C379C8F89F067343C8 * value)
	{
		___samplerUpdateAdded_42 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___samplerUpdateAdded_42), (void*)value);
	}

	inline static int32_t get_offset_of_samplerUpdatePressed_43() { return static_cast<int32_t>(offsetof(TouchManagerInstance_t2FC1653461034CC60588BF499439FF10FCC702CB, ___samplerUpdatePressed_43)); }
	inline CustomSampler_tD50B25148FC97E173885F9C379C8F89F067343C8 * get_samplerUpdatePressed_43() const { return ___samplerUpdatePressed_43; }
	inline CustomSampler_tD50B25148FC97E173885F9C379C8F89F067343C8 ** get_address_of_samplerUpdatePressed_43() { return &___samplerUpdatePressed_43; }
	inline void set_samplerUpdatePressed_43(CustomSampler_tD50B25148FC97E173885F9C379C8F89F067343C8 * value)
	{
		___samplerUpdatePressed_43 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___samplerUpdatePressed_43), (void*)value);
	}

	inline static int32_t get_offset_of_samplerUpdateUpdated_44() { return static_cast<int32_t>(offsetof(TouchManagerInstance_t2FC1653461034CC60588BF499439FF10FCC702CB, ___samplerUpdateUpdated_44)); }
	inline CustomSampler_tD50B25148FC97E173885F9C379C8F89F067343C8 * get_samplerUpdateUpdated_44() const { return ___samplerUpdateUpdated_44; }
	inline CustomSampler_tD50B25148FC97E173885F9C379C8F89F067343C8 ** get_address_of_samplerUpdateUpdated_44() { return &___samplerUpdateUpdated_44; }
	inline void set_samplerUpdateUpdated_44(CustomSampler_tD50B25148FC97E173885F9C379C8F89F067343C8 * value)
	{
		___samplerUpdateUpdated_44 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___samplerUpdateUpdated_44), (void*)value);
	}

	inline static int32_t get_offset_of_samplerUpdateReleased_45() { return static_cast<int32_t>(offsetof(TouchManagerInstance_t2FC1653461034CC60588BF499439FF10FCC702CB, ___samplerUpdateReleased_45)); }
	inline CustomSampler_tD50B25148FC97E173885F9C379C8F89F067343C8 * get_samplerUpdateReleased_45() const { return ___samplerUpdateReleased_45; }
	inline CustomSampler_tD50B25148FC97E173885F9C379C8F89F067343C8 ** get_address_of_samplerUpdateReleased_45() { return &___samplerUpdateReleased_45; }
	inline void set_samplerUpdateReleased_45(CustomSampler_tD50B25148FC97E173885F9C379C8F89F067343C8 * value)
	{
		___samplerUpdateReleased_45 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___samplerUpdateReleased_45), (void*)value);
	}

	inline static int32_t get_offset_of_samplerUpdateRemoved_46() { return static_cast<int32_t>(offsetof(TouchManagerInstance_t2FC1653461034CC60588BF499439FF10FCC702CB, ___samplerUpdateRemoved_46)); }
	inline CustomSampler_tD50B25148FC97E173885F9C379C8F89F067343C8 * get_samplerUpdateRemoved_46() const { return ___samplerUpdateRemoved_46; }
	inline CustomSampler_tD50B25148FC97E173885F9C379C8F89F067343C8 ** get_address_of_samplerUpdateRemoved_46() { return &___samplerUpdateRemoved_46; }
	inline void set_samplerUpdateRemoved_46(CustomSampler_tD50B25148FC97E173885F9C379C8F89F067343C8 * value)
	{
		___samplerUpdateRemoved_46 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___samplerUpdateRemoved_46), (void*)value);
	}

	inline static int32_t get_offset_of_samplerUpdateCancelled_47() { return static_cast<int32_t>(offsetof(TouchManagerInstance_t2FC1653461034CC60588BF499439FF10FCC702CB, ___samplerUpdateCancelled_47)); }
	inline CustomSampler_tD50B25148FC97E173885F9C379C8F89F067343C8 * get_samplerUpdateCancelled_47() const { return ___samplerUpdateCancelled_47; }
	inline CustomSampler_tD50B25148FC97E173885F9C379C8F89F067343C8 ** get_address_of_samplerUpdateCancelled_47() { return &___samplerUpdateCancelled_47; }
	inline void set_samplerUpdateCancelled_47(CustomSampler_tD50B25148FC97E173885F9C379C8F89F067343C8 * value)
	{
		___samplerUpdateCancelled_47 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___samplerUpdateCancelled_47), (void*)value);
	}
};

struct TouchManagerInstance_t2FC1653461034CC60588BF499439FF10FCC702CB_StaticFields
{
public:
	// System.Boolean TouchScript.Core.TouchManagerInstance::shuttingDown
	bool ___shuttingDown_13;
	// TouchScript.Core.TouchManagerInstance TouchScript.Core.TouchManagerInstance::instance
	TouchManagerInstance_t2FC1653461034CC60588BF499439FF10FCC702CB * ___instance_14;
	// TouchScript.Utils.ObjectPool`1<System.Collections.Generic.List`1<TouchScript.Pointers.Pointer>> TouchScript.Core.TouchManagerInstance::pointerListPool
	ObjectPool_1_tA955CD3AB80392B4522509C5221FC03670FCD2EA * ___pointerListPool_32;
	// TouchScript.Utils.ObjectPool`1<System.Collections.Generic.List`1<System.Int32>> TouchScript.Core.TouchManagerInstance::intListPool
	ObjectPool_1_t115A93ADDCF6A8278DEC06EB3277071E8545C393 * ___intListPool_33;

public:
	inline static int32_t get_offset_of_shuttingDown_13() { return static_cast<int32_t>(offsetof(TouchManagerInstance_t2FC1653461034CC60588BF499439FF10FCC702CB_StaticFields, ___shuttingDown_13)); }
	inline bool get_shuttingDown_13() const { return ___shuttingDown_13; }
	inline bool* get_address_of_shuttingDown_13() { return &___shuttingDown_13; }
	inline void set_shuttingDown_13(bool value)
	{
		___shuttingDown_13 = value;
	}

	inline static int32_t get_offset_of_instance_14() { return static_cast<int32_t>(offsetof(TouchManagerInstance_t2FC1653461034CC60588BF499439FF10FCC702CB_StaticFields, ___instance_14)); }
	inline TouchManagerInstance_t2FC1653461034CC60588BF499439FF10FCC702CB * get_instance_14() const { return ___instance_14; }
	inline TouchManagerInstance_t2FC1653461034CC60588BF499439FF10FCC702CB ** get_address_of_instance_14() { return &___instance_14; }
	inline void set_instance_14(TouchManagerInstance_t2FC1653461034CC60588BF499439FF10FCC702CB * value)
	{
		___instance_14 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___instance_14), (void*)value);
	}

	inline static int32_t get_offset_of_pointerListPool_32() { return static_cast<int32_t>(offsetof(TouchManagerInstance_t2FC1653461034CC60588BF499439FF10FCC702CB_StaticFields, ___pointerListPool_32)); }
	inline ObjectPool_1_tA955CD3AB80392B4522509C5221FC03670FCD2EA * get_pointerListPool_32() const { return ___pointerListPool_32; }
	inline ObjectPool_1_tA955CD3AB80392B4522509C5221FC03670FCD2EA ** get_address_of_pointerListPool_32() { return &___pointerListPool_32; }
	inline void set_pointerListPool_32(ObjectPool_1_tA955CD3AB80392B4522509C5221FC03670FCD2EA * value)
	{
		___pointerListPool_32 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___pointerListPool_32), (void*)value);
	}

	inline static int32_t get_offset_of_intListPool_33() { return static_cast<int32_t>(offsetof(TouchManagerInstance_t2FC1653461034CC60588BF499439FF10FCC702CB_StaticFields, ___intListPool_33)); }
	inline ObjectPool_1_t115A93ADDCF6A8278DEC06EB3277071E8545C393 * get_intListPool_33() const { return ___intListPool_33; }
	inline ObjectPool_1_t115A93ADDCF6A8278DEC06EB3277071E8545C393 ** get_address_of_intListPool_33() { return &___intListPool_33; }
	inline void set_intListPool_33(ObjectPool_1_t115A93ADDCF6A8278DEC06EB3277071E8545C393 * value)
	{
		___intListPool_33 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___intListPool_33), (void*)value);
	}
};


// TouchScript.TouchManager
struct  TouchManager_tE3A4C5ABD05B3B6C984CDBB1FA80479E1ACBE92B  : public DebuggableMonoBehaviour_t9ECFEBA1E6079CED2A7AF5B46939A05FBDD0C667
{
public:
	// TouchScript.TouchManager_FrameEvent TouchScript.TouchManager::OnFrameStart
	FrameEvent_tEBA65ABA56D36C692FB1A49D6929A473CA7A3817 * ___OnFrameStart_9;
	// TouchScript.TouchManager_FrameEvent TouchScript.TouchManager::OnFrameFinish
	FrameEvent_tEBA65ABA56D36C692FB1A49D6929A473CA7A3817 * ___OnFrameFinish_10;
	// TouchScript.TouchManager_PointerEvent TouchScript.TouchManager::OnPointersAdd
	PointerEvent_t46D6262BF503769AA7E077C49BB94DA4B83061DA * ___OnPointersAdd_11;
	// TouchScript.TouchManager_PointerEvent TouchScript.TouchManager::OnPointersUpdate
	PointerEvent_t46D6262BF503769AA7E077C49BB94DA4B83061DA * ___OnPointersUpdate_12;
	// TouchScript.TouchManager_PointerEvent TouchScript.TouchManager::OnPointersPress
	PointerEvent_t46D6262BF503769AA7E077C49BB94DA4B83061DA * ___OnPointersPress_13;
	// TouchScript.TouchManager_PointerEvent TouchScript.TouchManager::OnPointersRelease
	PointerEvent_t46D6262BF503769AA7E077C49BB94DA4B83061DA * ___OnPointersRelease_14;
	// TouchScript.TouchManager_PointerEvent TouchScript.TouchManager::OnPointersRemove
	PointerEvent_t46D6262BF503769AA7E077C49BB94DA4B83061DA * ___OnPointersRemove_15;
	// TouchScript.TouchManager_PointerEvent TouchScript.TouchManager::OnPointersCancel
	PointerEvent_t46D6262BF503769AA7E077C49BB94DA4B83061DA * ___OnPointersCancel_16;
	// System.Boolean TouchScript.TouchManager::basicEditor
	bool ___basicEditor_17;
	// UnityEngine.Object TouchScript.TouchManager::displayDevice
	Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 * ___displayDevice_18;
	// System.Boolean TouchScript.TouchManager::shouldCreateCameraLayer
	bool ___shouldCreateCameraLayer_19;
	// System.Boolean TouchScript.TouchManager::shouldCreateStandardInput
	bool ___shouldCreateStandardInput_20;
	// System.Boolean TouchScript.TouchManager::useSendMessage
	bool ___useSendMessage_21;
	// TouchScript.TouchManager_MessageType TouchScript.TouchManager::sendMessageEvents
	int32_t ___sendMessageEvents_22;
	// UnityEngine.GameObject TouchScript.TouchManager::sendMessageTarget
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___sendMessageTarget_23;
	// System.Boolean TouchScript.TouchManager::useUnityEvents
	bool ___useUnityEvents_24;
	// System.Collections.Generic.List`1<TouchScript.Layers.TouchLayer> TouchScript.TouchManager::layers
	List_1_tACFDA2C40746A89629E9D8D22CD4726B64D7C856 * ___layers_25;

public:
	inline static int32_t get_offset_of_OnFrameStart_9() { return static_cast<int32_t>(offsetof(TouchManager_tE3A4C5ABD05B3B6C984CDBB1FA80479E1ACBE92B, ___OnFrameStart_9)); }
	inline FrameEvent_tEBA65ABA56D36C692FB1A49D6929A473CA7A3817 * get_OnFrameStart_9() const { return ___OnFrameStart_9; }
	inline FrameEvent_tEBA65ABA56D36C692FB1A49D6929A473CA7A3817 ** get_address_of_OnFrameStart_9() { return &___OnFrameStart_9; }
	inline void set_OnFrameStart_9(FrameEvent_tEBA65ABA56D36C692FB1A49D6929A473CA7A3817 * value)
	{
		___OnFrameStart_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___OnFrameStart_9), (void*)value);
	}

	inline static int32_t get_offset_of_OnFrameFinish_10() { return static_cast<int32_t>(offsetof(TouchManager_tE3A4C5ABD05B3B6C984CDBB1FA80479E1ACBE92B, ___OnFrameFinish_10)); }
	inline FrameEvent_tEBA65ABA56D36C692FB1A49D6929A473CA7A3817 * get_OnFrameFinish_10() const { return ___OnFrameFinish_10; }
	inline FrameEvent_tEBA65ABA56D36C692FB1A49D6929A473CA7A3817 ** get_address_of_OnFrameFinish_10() { return &___OnFrameFinish_10; }
	inline void set_OnFrameFinish_10(FrameEvent_tEBA65ABA56D36C692FB1A49D6929A473CA7A3817 * value)
	{
		___OnFrameFinish_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___OnFrameFinish_10), (void*)value);
	}

	inline static int32_t get_offset_of_OnPointersAdd_11() { return static_cast<int32_t>(offsetof(TouchManager_tE3A4C5ABD05B3B6C984CDBB1FA80479E1ACBE92B, ___OnPointersAdd_11)); }
	inline PointerEvent_t46D6262BF503769AA7E077C49BB94DA4B83061DA * get_OnPointersAdd_11() const { return ___OnPointersAdd_11; }
	inline PointerEvent_t46D6262BF503769AA7E077C49BB94DA4B83061DA ** get_address_of_OnPointersAdd_11() { return &___OnPointersAdd_11; }
	inline void set_OnPointersAdd_11(PointerEvent_t46D6262BF503769AA7E077C49BB94DA4B83061DA * value)
	{
		___OnPointersAdd_11 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___OnPointersAdd_11), (void*)value);
	}

	inline static int32_t get_offset_of_OnPointersUpdate_12() { return static_cast<int32_t>(offsetof(TouchManager_tE3A4C5ABD05B3B6C984CDBB1FA80479E1ACBE92B, ___OnPointersUpdate_12)); }
	inline PointerEvent_t46D6262BF503769AA7E077C49BB94DA4B83061DA * get_OnPointersUpdate_12() const { return ___OnPointersUpdate_12; }
	inline PointerEvent_t46D6262BF503769AA7E077C49BB94DA4B83061DA ** get_address_of_OnPointersUpdate_12() { return &___OnPointersUpdate_12; }
	inline void set_OnPointersUpdate_12(PointerEvent_t46D6262BF503769AA7E077C49BB94DA4B83061DA * value)
	{
		___OnPointersUpdate_12 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___OnPointersUpdate_12), (void*)value);
	}

	inline static int32_t get_offset_of_OnPointersPress_13() { return static_cast<int32_t>(offsetof(TouchManager_tE3A4C5ABD05B3B6C984CDBB1FA80479E1ACBE92B, ___OnPointersPress_13)); }
	inline PointerEvent_t46D6262BF503769AA7E077C49BB94DA4B83061DA * get_OnPointersPress_13() const { return ___OnPointersPress_13; }
	inline PointerEvent_t46D6262BF503769AA7E077C49BB94DA4B83061DA ** get_address_of_OnPointersPress_13() { return &___OnPointersPress_13; }
	inline void set_OnPointersPress_13(PointerEvent_t46D6262BF503769AA7E077C49BB94DA4B83061DA * value)
	{
		___OnPointersPress_13 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___OnPointersPress_13), (void*)value);
	}

	inline static int32_t get_offset_of_OnPointersRelease_14() { return static_cast<int32_t>(offsetof(TouchManager_tE3A4C5ABD05B3B6C984CDBB1FA80479E1ACBE92B, ___OnPointersRelease_14)); }
	inline PointerEvent_t46D6262BF503769AA7E077C49BB94DA4B83061DA * get_OnPointersRelease_14() const { return ___OnPointersRelease_14; }
	inline PointerEvent_t46D6262BF503769AA7E077C49BB94DA4B83061DA ** get_address_of_OnPointersRelease_14() { return &___OnPointersRelease_14; }
	inline void set_OnPointersRelease_14(PointerEvent_t46D6262BF503769AA7E077C49BB94DA4B83061DA * value)
	{
		___OnPointersRelease_14 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___OnPointersRelease_14), (void*)value);
	}

	inline static int32_t get_offset_of_OnPointersRemove_15() { return static_cast<int32_t>(offsetof(TouchManager_tE3A4C5ABD05B3B6C984CDBB1FA80479E1ACBE92B, ___OnPointersRemove_15)); }
	inline PointerEvent_t46D6262BF503769AA7E077C49BB94DA4B83061DA * get_OnPointersRemove_15() const { return ___OnPointersRemove_15; }
	inline PointerEvent_t46D6262BF503769AA7E077C49BB94DA4B83061DA ** get_address_of_OnPointersRemove_15() { return &___OnPointersRemove_15; }
	inline void set_OnPointersRemove_15(PointerEvent_t46D6262BF503769AA7E077C49BB94DA4B83061DA * value)
	{
		___OnPointersRemove_15 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___OnPointersRemove_15), (void*)value);
	}

	inline static int32_t get_offset_of_OnPointersCancel_16() { return static_cast<int32_t>(offsetof(TouchManager_tE3A4C5ABD05B3B6C984CDBB1FA80479E1ACBE92B, ___OnPointersCancel_16)); }
	inline PointerEvent_t46D6262BF503769AA7E077C49BB94DA4B83061DA * get_OnPointersCancel_16() const { return ___OnPointersCancel_16; }
	inline PointerEvent_t46D6262BF503769AA7E077C49BB94DA4B83061DA ** get_address_of_OnPointersCancel_16() { return &___OnPointersCancel_16; }
	inline void set_OnPointersCancel_16(PointerEvent_t46D6262BF503769AA7E077C49BB94DA4B83061DA * value)
	{
		___OnPointersCancel_16 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___OnPointersCancel_16), (void*)value);
	}

	inline static int32_t get_offset_of_basicEditor_17() { return static_cast<int32_t>(offsetof(TouchManager_tE3A4C5ABD05B3B6C984CDBB1FA80479E1ACBE92B, ___basicEditor_17)); }
	inline bool get_basicEditor_17() const { return ___basicEditor_17; }
	inline bool* get_address_of_basicEditor_17() { return &___basicEditor_17; }
	inline void set_basicEditor_17(bool value)
	{
		___basicEditor_17 = value;
	}

	inline static int32_t get_offset_of_displayDevice_18() { return static_cast<int32_t>(offsetof(TouchManager_tE3A4C5ABD05B3B6C984CDBB1FA80479E1ACBE92B, ___displayDevice_18)); }
	inline Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 * get_displayDevice_18() const { return ___displayDevice_18; }
	inline Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 ** get_address_of_displayDevice_18() { return &___displayDevice_18; }
	inline void set_displayDevice_18(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 * value)
	{
		___displayDevice_18 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___displayDevice_18), (void*)value);
	}

	inline static int32_t get_offset_of_shouldCreateCameraLayer_19() { return static_cast<int32_t>(offsetof(TouchManager_tE3A4C5ABD05B3B6C984CDBB1FA80479E1ACBE92B, ___shouldCreateCameraLayer_19)); }
	inline bool get_shouldCreateCameraLayer_19() const { return ___shouldCreateCameraLayer_19; }
	inline bool* get_address_of_shouldCreateCameraLayer_19() { return &___shouldCreateCameraLayer_19; }
	inline void set_shouldCreateCameraLayer_19(bool value)
	{
		___shouldCreateCameraLayer_19 = value;
	}

	inline static int32_t get_offset_of_shouldCreateStandardInput_20() { return static_cast<int32_t>(offsetof(TouchManager_tE3A4C5ABD05B3B6C984CDBB1FA80479E1ACBE92B, ___shouldCreateStandardInput_20)); }
	inline bool get_shouldCreateStandardInput_20() const { return ___shouldCreateStandardInput_20; }
	inline bool* get_address_of_shouldCreateStandardInput_20() { return &___shouldCreateStandardInput_20; }
	inline void set_shouldCreateStandardInput_20(bool value)
	{
		___shouldCreateStandardInput_20 = value;
	}

	inline static int32_t get_offset_of_useSendMessage_21() { return static_cast<int32_t>(offsetof(TouchManager_tE3A4C5ABD05B3B6C984CDBB1FA80479E1ACBE92B, ___useSendMessage_21)); }
	inline bool get_useSendMessage_21() const { return ___useSendMessage_21; }
	inline bool* get_address_of_useSendMessage_21() { return &___useSendMessage_21; }
	inline void set_useSendMessage_21(bool value)
	{
		___useSendMessage_21 = value;
	}

	inline static int32_t get_offset_of_sendMessageEvents_22() { return static_cast<int32_t>(offsetof(TouchManager_tE3A4C5ABD05B3B6C984CDBB1FA80479E1ACBE92B, ___sendMessageEvents_22)); }
	inline int32_t get_sendMessageEvents_22() const { return ___sendMessageEvents_22; }
	inline int32_t* get_address_of_sendMessageEvents_22() { return &___sendMessageEvents_22; }
	inline void set_sendMessageEvents_22(int32_t value)
	{
		___sendMessageEvents_22 = value;
	}

	inline static int32_t get_offset_of_sendMessageTarget_23() { return static_cast<int32_t>(offsetof(TouchManager_tE3A4C5ABD05B3B6C984CDBB1FA80479E1ACBE92B, ___sendMessageTarget_23)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_sendMessageTarget_23() const { return ___sendMessageTarget_23; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_sendMessageTarget_23() { return &___sendMessageTarget_23; }
	inline void set_sendMessageTarget_23(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___sendMessageTarget_23 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___sendMessageTarget_23), (void*)value);
	}

	inline static int32_t get_offset_of_useUnityEvents_24() { return static_cast<int32_t>(offsetof(TouchManager_tE3A4C5ABD05B3B6C984CDBB1FA80479E1ACBE92B, ___useUnityEvents_24)); }
	inline bool get_useUnityEvents_24() const { return ___useUnityEvents_24; }
	inline bool* get_address_of_useUnityEvents_24() { return &___useUnityEvents_24; }
	inline void set_useUnityEvents_24(bool value)
	{
		___useUnityEvents_24 = value;
	}

	inline static int32_t get_offset_of_layers_25() { return static_cast<int32_t>(offsetof(TouchManager_tE3A4C5ABD05B3B6C984CDBB1FA80479E1ACBE92B, ___layers_25)); }
	inline List_1_tACFDA2C40746A89629E9D8D22CD4726B64D7C856 * get_layers_25() const { return ___layers_25; }
	inline List_1_tACFDA2C40746A89629E9D8D22CD4726B64D7C856 ** get_address_of_layers_25() { return &___layers_25; }
	inline void set_layers_25(List_1_tACFDA2C40746A89629E9D8D22CD4726B64D7C856 * value)
	{
		___layers_25 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___layers_25), (void*)value);
	}
};

struct TouchManager_tE3A4C5ABD05B3B6C984CDBB1FA80479E1ACBE92B_StaticFields
{
public:
	// UnityEngine.Vector2 TouchScript.TouchManager::INVALID_POSITION
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___INVALID_POSITION_6;
	// System.Version TouchScript.TouchManager::VERSION
	Version_tDBE6876C59B6F56D4F8CAA03851177ABC6FE0DFD * ___VERSION_7;
	// System.String TouchScript.TouchManager::VERSION_SUFFIX
	String_t* ___VERSION_SUFFIX_8;

public:
	inline static int32_t get_offset_of_INVALID_POSITION_6() { return static_cast<int32_t>(offsetof(TouchManager_tE3A4C5ABD05B3B6C984CDBB1FA80479E1ACBE92B_StaticFields, ___INVALID_POSITION_6)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_INVALID_POSITION_6() const { return ___INVALID_POSITION_6; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_INVALID_POSITION_6() { return &___INVALID_POSITION_6; }
	inline void set_INVALID_POSITION_6(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___INVALID_POSITION_6 = value;
	}

	inline static int32_t get_offset_of_VERSION_7() { return static_cast<int32_t>(offsetof(TouchManager_tE3A4C5ABD05B3B6C984CDBB1FA80479E1ACBE92B_StaticFields, ___VERSION_7)); }
	inline Version_tDBE6876C59B6F56D4F8CAA03851177ABC6FE0DFD * get_VERSION_7() const { return ___VERSION_7; }
	inline Version_tDBE6876C59B6F56D4F8CAA03851177ABC6FE0DFD ** get_address_of_VERSION_7() { return &___VERSION_7; }
	inline void set_VERSION_7(Version_tDBE6876C59B6F56D4F8CAA03851177ABC6FE0DFD * value)
	{
		___VERSION_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___VERSION_7), (void*)value);
	}

	inline static int32_t get_offset_of_VERSION_SUFFIX_8() { return static_cast<int32_t>(offsetof(TouchManager_tE3A4C5ABD05B3B6C984CDBB1FA80479E1ACBE92B_StaticFields, ___VERSION_SUFFIX_8)); }
	inline String_t* get_VERSION_SUFFIX_8() const { return ___VERSION_SUFFIX_8; }
	inline String_t** get_address_of_VERSION_SUFFIX_8() { return &___VERSION_SUFFIX_8; }
	inline void set_VERSION_SUFFIX_8(String_t* value)
	{
		___VERSION_SUFFIX_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___VERSION_SUFFIX_8), (void*)value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// System.Byte[]
struct ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) uint8_t m_Items[1];

public:
	inline uint8_t GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline uint8_t* GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, uint8_t value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline uint8_t GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline uint8_t* GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, uint8_t value)
	{
		m_Items[index] = value;
	}
};
// System.Object[]
struct ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) RuntimeObject * m_Items[1];

public:
	inline RuntimeObject * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline RuntimeObject ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, RuntimeObject * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
	inline RuntimeObject * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline RuntimeObject ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, RuntimeObject * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
};


// !0 System.Collections.Generic.List`1<System.Object>::get_Item(System.Int32)
IL2CPP_EXTERN_C inline IL2CPP_METHOD_ATTR RuntimeObject * List_1_get_Item_mFDB8AD680C600072736579BBF5F38F7416396588_gshared_inline (List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * __this, int32_t ___index0, const RuntimeMethod* method);
// System.Int32 System.Collections.Generic.List`1<System.Object>::get_Count()
IL2CPP_EXTERN_C inline IL2CPP_METHOD_ATTR int32_t List_1_get_Count_m507C9149FF7F83AAC72C29091E745D557DA47D22_gshared_inline (List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * __this, const RuntimeMethod* method);
// System.Void System.EventHandler`1<System.Object>::.ctor(System.Object,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void EventHandler_1__ctor_m3D6B301FCEB48533990CFB2386DAAB091D357BAC_gshared (EventHandler_1_t10245A26B14DDE8DDFD5B263BDE0641F17DCFDC3 * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method);
// System.Void UnityEngine.Events.UnityEvent`1<System.Object>::Invoke(!0)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UnityEvent_1_Invoke_m027706B0C7150736F066D5C663304CB0B80ABBF0_gshared (UnityEvent_1_t9E897A46A46C78F7104A831E63BB081546EFFF0D * __this, RuntimeObject * ___arg00, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<System.Object>::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void List_1__ctor_mC832F1AC0F814BAEB19175F5D7972A7507508BC3_gshared (List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * __this, const RuntimeMethod* method);
// System.Void UnityEngine.Events.UnityEvent`1<System.Object>::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UnityEvent_1__ctor_m38CD236F782AA440F6DDB5D90B4C9DA24CDBA3A7_gshared (UnityEvent_1_t9E897A46A46C78F7104A831E63BB081546EFFF0D * __this, const RuntimeMethod* method);
// !!0 UnityEngine.GameObject::GetComponent<System.Object>()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * GameObject_GetComponent_TisRuntimeObject_mD4382B2843BA9A61A01A8F9D7B9813D060F9C9CA_gshared (GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * __this, const RuntimeMethod* method);

// System.Void TouchScript.Layers.ProjectionParams::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ProjectionParams__ctor_m6A4C0C8062A36F504724741380C7479CCD9AB348 (ProjectionParams_t2B7BDC4A52427186D2923201E1479C2E7641E6B8 * __this, const RuntimeMethod* method);
// UnityEngine.RenderMode UnityEngine.Canvas::get_renderMode()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t Canvas_get_renderMode_mAF68701B143F01C7D19B6C7D3033E3B34ECB2FC8 (Canvas_tBC28BF1DD8D8499A89B5781505833D3728CF8591 * __this, const RuntimeMethod* method);
// UnityEngine.Camera UnityEngine.Canvas::get_worldCamera()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * Canvas_get_worldCamera_m36F1A8DBFC4AB34278125DA017CACDC873F53409 (Canvas_tBC28BF1DD8D8499A89B5781505833D3728CF8591 * __this, const RuntimeMethod* method);
// UnityEngine.Camera UnityEngine.Camera::get_main()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * Camera_get_main_m9256A9F84F92D7ED73F3E6C4E2694030AD8B61FA (const RuntimeMethod* method);
// UnityEngine.Vector3 TouchScript.Utils.Geom.ProjectionUtils::CameraToPlaneProjection(UnityEngine.Vector2,UnityEngine.Camera,UnityEngine.Plane)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ProjectionUtils_CameraToPlaneProjection_m49C90C2A65AB392F106E1B043BB38EC1D603376A (Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___position0, Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * ___camera1, Plane_t0903921088DEEDE1BCDEA5BF279EDBCFC9679AED  ___projectionPlane2, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Camera::WorldToScreenPoint(UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  Camera_WorldToScreenPoint_m880F9611E4848C11F21FDF1A1D307B401C61B1BF (Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * __this, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___position0, const RuntimeMethod* method);
// UnityEngine.Vector2 UnityEngine.Vector2::op_Implicit(UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  Vector2_op_Implicit_mEA1F75961E3D368418BA8CEB9C40E55C25BA3C28 (Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___v0, const RuntimeMethod* method);
// System.Void System.EventArgs::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void EventArgs__ctor_m3551293259861C5A78CD47689D559F828ED29DF7 (EventArgs_t8E6CA180BE0E56674C6407011A94BAF7C757352E * __this, const RuntimeMethod* method);
// System.Void TouchScript.PointerEventArgs::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PointerEventArgs__ctor_m2184ED9B05EDC50EC544304A7B0EEE2BE31DA2F0 (PointerEventArgs_t60C9291910F961213770A1302F3CE7CC5A580106 * __this, const RuntimeMethod* method);
// System.Void TouchScript.PointerEventArgs::set_Pointers(System.Collections.Generic.IList`1<TouchScript.Pointers.Pointer>)
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR void PointerEventArgs_set_Pointers_m80547B23A481F55D60DEE830E6340F85555B550A_inline (PointerEventArgs_t60C9291910F961213770A1302F3CE7CC5A580106 * __this, RuntimeObject* ___value0, const RuntimeMethod* method);
// System.Void TouchScript.Pointers.FakePointer::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void FakePointer__ctor_mAB218D43DC15CD0732B9995C7F5EF5D08FFA2F3C (FakePointer_t8C1AEEBB63CF9BCC8170772F4E4601CD4298BC47 * __this, const RuntimeMethod* method);
// System.Void TouchScript.Pointers.FakePointer::set_Position(UnityEngine.Vector2)
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR void FakePointer_set_Position_mCA620D1E36778D9DD9DB87D24F6899D5EFEBBF34_inline (FakePointer_t8C1AEEBB63CF9BCC8170772F4E4601CD4298BC47 * __this, Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___value0, const RuntimeMethod* method);
// System.Void System.Object::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Object__ctor_m925ECA5E85CA100E3FB86A4F9E15C120E9A184C0 (RuntimeObject * __this, const RuntimeMethod* method);
// System.Void TouchScript.Pointers.FakePointer::set_Id(System.Int32)
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR void FakePointer_set_Id_mC0D41C4688A1B33B826E36B909C6A0E3983EFEB5_inline (FakePointer_t8C1AEEBB63CF9BCC8170772F4E4601CD4298BC47 * __this, int32_t ___value0, const RuntimeMethod* method);
// System.Void TouchScript.Pointers.FakePointer::set_Type(TouchScript.Pointers.Pointer/PointerType)
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR void FakePointer_set_Type_m79DF4E0AF1EEF7C1BD223A21042D27D9E12327EC_inline (FakePointer_t8C1AEEBB63CF9BCC8170772F4E4601CD4298BC47 * __this, int32_t ___value0, const RuntimeMethod* method);
// System.Void TouchScript.Pointers.FakePointer::set_Flags(System.UInt32)
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR void FakePointer_set_Flags_m64E3B23758D21054F7CB36BB6033AA7088169EB8_inline (FakePointer_t8C1AEEBB63CF9BCC8170772F4E4601CD4298BC47 * __this, uint32_t ___value0, const RuntimeMethod* method);
// TouchScript.ILayerManager TouchScript.LayerManager::get_Instance()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* LayerManager_get_Instance_mA50DB5F8ACCAF0DCDE4BEEA78843142A752CF9F7 (const RuntimeMethod* method);
// System.Void TouchScript.Pointers.Pointer::.ctor(TouchScript.InputSources.IInputSource)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Pointer__ctor_mE7828F1485349BF2A610843ABB9A7944396F2D6C (Pointer_tBF6873B314D4CC8534B6CD8A8BA54B92D8CFDE98 * __this, RuntimeObject* ___input0, const RuntimeMethod* method);
// System.Void TouchScript.Pointers.Pointer::set_Type(TouchScript.Pointers.Pointer/PointerType)
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR void Pointer_set_Type_mC0D513634112590ABB8EB877E8BA70416BDF2625_inline (Pointer_tBF6873B314D4CC8534B6CD8A8BA54B92D8CFDE98 * __this, int32_t ___value0, const RuntimeMethod* method);
// System.Void TouchScript.Pointers.Pointer::CopyFrom(TouchScript.Pointers.Pointer)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Pointer_CopyFrom_m323BE52F7920E028E626E6C42E1C5A230CAB1494 (Pointer_tBF6873B314D4CC8534B6CD8A8BA54B92D8CFDE98 * __this, Pointer_tBF6873B314D4CC8534B6CD8A8BA54B92D8CFDE98 * ___target0, const RuntimeMethod* method);
// UnityEngine.Vector2 TouchScript.Pointers.MousePointer::get_ScrollDelta()
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  MousePointer_get_ScrollDelta_mC8F97C22EFBD42A56861A7B1EF1907185F0746A3_inline (MousePointer_tEFB01FDD2340977461DEB8153349F62DFDE0E100 * __this, const RuntimeMethod* method);
// System.Void TouchScript.Pointers.MousePointer::set_ScrollDelta(UnityEngine.Vector2)
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR void MousePointer_set_ScrollDelta_m352D328338CF5FF4420BEA90891189D57C517C1F_inline (MousePointer_tEFB01FDD2340977461DEB8153349F62DFDE0E100 * __this, Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___value0, const RuntimeMethod* method);
// System.Int32 TouchScript.Pointers.ObjectPointer::get_ObjectId()
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR int32_t ObjectPointer_get_ObjectId_m19757D9C574D591D4DD3043EEFBDDCE1064B2581_inline (ObjectPointer_tD190FCA2463DDBBD72FBA988696DFC51EFF46C36 * __this, const RuntimeMethod* method);
// System.Void TouchScript.Pointers.ObjectPointer::set_ObjectId(System.Int32)
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR void ObjectPointer_set_ObjectId_m4951549B2096276F8533B2D025F744CE8E5D4FDD_inline (ObjectPointer_tD190FCA2463DDBBD72FBA988696DFC51EFF46C36 * __this, int32_t ___value0, const RuntimeMethod* method);
// System.Single TouchScript.Pointers.ObjectPointer::get_Width()
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR float ObjectPointer_get_Width_m92F855D6202D7ECC31204F7BFB1924B26DA5F007_inline (ObjectPointer_tD190FCA2463DDBBD72FBA988696DFC51EFF46C36 * __this, const RuntimeMethod* method);
// System.Void TouchScript.Pointers.ObjectPointer::set_Width(System.Single)
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR void ObjectPointer_set_Width_mFCDC006E0026F249E35C88362BD748EE5A14AFD7_inline (ObjectPointer_tD190FCA2463DDBBD72FBA988696DFC51EFF46C36 * __this, float ___value0, const RuntimeMethod* method);
// System.Single TouchScript.Pointers.ObjectPointer::get_Height()
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR float ObjectPointer_get_Height_m30D7BD97E5CD11924D9111F3011E0E453CAA6DA0_inline (ObjectPointer_tD190FCA2463DDBBD72FBA988696DFC51EFF46C36 * __this, const RuntimeMethod* method);
// System.Void TouchScript.Pointers.ObjectPointer::set_Height(System.Single)
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR void ObjectPointer_set_Height_m793FAF0083E3C0DC47E6825EE6ECCA7F342CE66A_inline (ObjectPointer_tD190FCA2463DDBBD72FBA988696DFC51EFF46C36 * __this, float ___value0, const RuntimeMethod* method);
// System.Single TouchScript.Pointers.ObjectPointer::get_Angle()
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR float ObjectPointer_get_Angle_mF6824CF5CA2316D34523B893EE50BEF01DF9D1E9_inline (ObjectPointer_tD190FCA2463DDBBD72FBA988696DFC51EFF46C36 * __this, const RuntimeMethod* method);
// System.Void TouchScript.Pointers.ObjectPointer::set_Angle(System.Single)
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR void ObjectPointer_set_Angle_m3710F36254EBE84ACBC00123055FFB1CE8D8D108_inline (ObjectPointer_tD190FCA2463DDBBD72FBA988696DFC51EFF46C36 * __this, float ___value0, const RuntimeMethod* method);
// System.Void TouchScript.Pointers.Pointer::INTERNAL_Reset()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Pointer_INTERNAL_Reset_m851FB3F6C82DD723EB5A78235BC0B45865FE8897 (Pointer_tBF6873B314D4CC8534B6CD8A8BA54B92D8CFDE98 * __this, const RuntimeMethod* method);
// System.Void TouchScript.Pointers.PenPointer::set_Rotation(System.Single)
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR void PenPointer_set_Rotation_m74FF90FEA33FF1621F9792CBDDE3A31569246E08_inline (PenPointer_tAF65E3C674E3160B823CFBB7D82046A3FFD60340 * __this, float ___value0, const RuntimeMethod* method);
// System.Void TouchScript.Pointers.PenPointer::set_Pressure(System.Single)
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR void PenPointer_set_Pressure_m33A397E626F343E987248BEC63FAB5F18621BD61_inline (PenPointer_tAF65E3C674E3160B823CFBB7D82046A3FFD60340 * __this, float ___value0, const RuntimeMethod* method);
// TouchScript.Layers.TouchLayer TouchScript.Hit.HitData::get_Layer()
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR TouchLayer_t484C3424D422E6073B3FE4DD21EFE05686F5AC54 * HitData_get_Layer_m80AD3383FA69E902C6C6E7B29769771F24EAEA51_inline (HitData_t3022C0DF180FDE3CF86F713B78A2056D02E78980 * __this, const RuntimeMethod* method);
// System.Boolean UnityEngine.Object::op_Equality(UnityEngine.Object,UnityEngine.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Object_op_Equality_mBC2401774F3BE33E8CF6F0A8148E66C95D6CFF1C (Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 * ___x0, Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 * ___y1, const RuntimeMethod* method);
// System.Boolean TouchScript.Core.LayerManagerInstance::GetHitTarget(TouchScript.Pointers.IPointer,TouchScript.Hit.HitData&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool LayerManagerInstance_GetHitTarget_m9B935210E7FF3A5562AE24FA17AB0936252E72D9 (LayerManagerInstance_t88CDE54CE87FC7A715E326B47F38C3B0054D0578 * __this, RuntimeObject* ___pointer0, HitData_t3022C0DF180FDE3CF86F713B78A2056D02E78980 * ___hit1, const RuntimeMethod* method);
// TouchScript.Pointers.Pointer/PointerType TouchScript.Pointers.Pointer::get_Type()
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR int32_t Pointer_get_Type_m8733FBC6C16FE2C6FBA301C50E8E26266720B9EB_inline (Pointer_tBF6873B314D4CC8534B6CD8A8BA54B92D8CFDE98 * __this, const RuntimeMethod* method);
// System.UInt32 TouchScript.Pointers.Pointer::get_Flags()
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR uint32_t Pointer_get_Flags_m941FB1843C3FD979278B353B040264E56F68A883_inline (Pointer_tBF6873B314D4CC8534B6CD8A8BA54B92D8CFDE98 * __this, const RuntimeMethod* method);
// System.Void TouchScript.Pointers.Pointer::set_Flags(System.UInt32)
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR void Pointer_set_Flags_mF6A08BE8419E9E7DFD4CD15AF3A4E811734E9293_inline (Pointer_tBF6873B314D4CC8534B6CD8A8BA54B92D8CFDE98 * __this, uint32_t ___value0, const RuntimeMethod* method);
// TouchScript.Pointers.Pointer/PointerButtonState TouchScript.Pointers.Pointer::get_Buttons()
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR int32_t Pointer_get_Buttons_mDD144F267D37B07E930E44E9B7D9357246D5839A_inline (Pointer_tBF6873B314D4CC8534B6CD8A8BA54B92D8CFDE98 * __this, const RuntimeMethod* method);
// System.Void TouchScript.Pointers.Pointer::set_Buttons(TouchScript.Pointers.Pointer/PointerButtonState)
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR void Pointer_set_Buttons_mAE901843226D346730CAD2CAD512793E4E4BEAC8_inline (Pointer_tBF6873B314D4CC8534B6CD8A8BA54B92D8CFDE98 * __this, int32_t ___value0, const RuntimeMethod* method);
// UnityEngine.Vector2 TouchScript.Pointers.Pointer::get_PreviousPosition()
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  Pointer_get_PreviousPosition_m18CBA71CDAE159A709A4F88D96F40B81B333118E_inline (Pointer_tBF6873B314D4CC8534B6CD8A8BA54B92D8CFDE98 * __this, const RuntimeMethod* method);
// System.Void TouchScript.Pointers.Pointer::set_PreviousPosition(UnityEngine.Vector2)
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR void Pointer_set_PreviousPosition_m9825F629338D0DF6160DCB6DAADF3488860FA703_inline (Pointer_tBF6873B314D4CC8534B6CD8A8BA54B92D8CFDE98 * __this, Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___value0, const RuntimeMethod* method);
// System.Boolean TouchScript.Pointers.Pointer::Equals(TouchScript.Pointers.Pointer)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Pointer_Equals_mBB1ABA52EFE71C48513257DDD931073295A2EE1C (Pointer_tBF6873B314D4CC8534B6CD8A8BA54B92D8CFDE98 * __this, Pointer_tBF6873B314D4CC8534B6CD8A8BA54B92D8CFDE98 * ___other0, const RuntimeMethod* method);
// System.Int32 TouchScript.Pointers.Pointer::get_Id()
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR int32_t Pointer_get_Id_mFF112114C234DC146CA93A7719C3F316E8DACCAD_inline (Pointer_tBF6873B314D4CC8534B6CD8A8BA54B92D8CFDE98 * __this, const RuntimeMethod* method);
// System.Void System.Text.StringBuilder::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void StringBuilder__ctor_mF928376F82E8C8FF3C11842C562DB8CF28B2735E (StringBuilder_t * __this, const RuntimeMethod* method);
// System.Void System.Text.StringBuilder::set_Length(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void StringBuilder_set_Length_m84AF318230AE5C3D0D48F1CE7C2170F6F5C19F5B (StringBuilder_t * __this, int32_t ___value0, const RuntimeMethod* method);
// System.Text.StringBuilder System.Text.StringBuilder::Append(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR StringBuilder_t * StringBuilder_Append_mDBB8CCBB7750C67BE2F2D92F47E6C0FA42793260 (StringBuilder_t * __this, String_t* ___value0, const RuntimeMethod* method);
// System.Text.StringBuilder System.Text.StringBuilder::Append(System.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR StringBuilder_t * StringBuilder_Append_mA1A063A1388A21C8EA011DBA7FC98C24C3EE3D65 (StringBuilder_t * __this, RuntimeObject * ___value0, const RuntimeMethod* method);
// System.Text.StringBuilder System.Text.StringBuilder::Append(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR StringBuilder_t * StringBuilder_Append_m85874CFF9E4B152DB2A91936C6F2CA3E9B1EFF84 (StringBuilder_t * __this, int32_t ___value0, const RuntimeMethod* method);
// System.Void TouchScript.Utils.PointerUtils::PressedButtonsToString(TouchScript.Pointers.Pointer/PointerButtonState,System.Text.StringBuilder)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PointerUtils_PressedButtonsToString_mAF4CD9F932292EE068D39024DDD1E70F7DC26D9F (int32_t ___buttons0, StringBuilder_t * ___builder1, const RuntimeMethod* method);
// System.Void TouchScript.Utils.BinaryUtils::ToBinaryString(System.UInt32,System.Text.StringBuilder,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void BinaryUtils_ToBinaryString_mCB55E824E5042B4C35CD8EF55C3090C73D1B3FEC (uint32_t ___value0, StringBuilder_t * ___builder1, int32_t ___digits2, const RuntimeMethod* method);
// UnityEngine.Vector2 TouchScript.Pointers.Pointer::get_Position()
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  Pointer_get_Position_mCE0B33AF8B7D86A8BA6824E7817A3DFCB29BE4BC_inline (Pointer_tBF6873B314D4CC8534B6CD8A8BA54B92D8CFDE98 * __this, const RuntimeMethod* method);
// System.Void TouchScript.Pointers.Pointer::set_InputSource(TouchScript.InputSources.IInputSource)
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR void Pointer_set_InputSource_m646284A86B72DC008C111B607C12E38F4A4EA385_inline (Pointer_tBF6873B314D4CC8534B6CD8A8BA54B92D8CFDE98 * __this, RuntimeObject* ___value0, const RuntimeMethod* method);
// System.Void TouchScript.Pointers.Pointer::set_Id(System.Int32)
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR void Pointer_set_Id_mEB0B14E2905D84B287EABA142EEAFF7737D63886_inline (Pointer_tBF6873B314D4CC8534B6CD8A8BA54B92D8CFDE98 * __this, int32_t ___value0, const RuntimeMethod* method);
// System.Void TouchScript.Pointers.Pointer::INTERNAL_ClearPressData()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Pointer_INTERNAL_ClearPressData_mBF3AD7F3B5D26FC7D9D50A7753BFA63FB3BFFF39 (Pointer_tBF6873B314D4CC8534B6CD8A8BA54B92D8CFDE98 * __this, const RuntimeMethod* method);
// UnityEngine.Vector2 UnityEngine.Vector2::get_zero()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  Vector2_get_zero_mFE0C3213BB698130D6C5247AB4B887A59074D0A8 (const RuntimeMethod* method);
// System.Void TouchScript.Pointers.TouchPointer::.ctor(TouchScript.InputSources.IInputSource)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TouchPointer__ctor_m45AB2CE7C1661672F231AFA9D718A1F877FBF276 (TouchPointer_t35FC788E12AC007D8F90D6D28131910B61316942 * __this, RuntimeObject* ___input0, const RuntimeMethod* method);
// System.Void TouchScript.Pointers.MousePointer::.ctor(TouchScript.InputSources.IInputSource)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MousePointer__ctor_m406338CE653654229558A5BCC38F97692A452158 (MousePointer_tEFB01FDD2340977461DEB8153349F62DFDE0E100 * __this, RuntimeObject* ___input0, const RuntimeMethod* method);
// System.Void TouchScript.Pointers.PenPointer::.ctor(TouchScript.InputSources.IInputSource)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PenPointer__ctor_m6D8A7F282AAE1FA58ECC119BF00ECF3C534AE5E2 (PenPointer_tAF65E3C674E3160B823CFBB7D82046A3FFD60340 * __this, RuntimeObject* ___input0, const RuntimeMethod* method);
// System.Void TouchScript.Pointers.ObjectPointer::.ctor(TouchScript.InputSources.IInputSource)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ObjectPointer__ctor_m24E6EC51EA50C4F5BF815D88CF8F84A552D98A41 (ObjectPointer_tD190FCA2463DDBBD72FBA988696DFC51EFF46C36 * __this, RuntimeObject* ___input0, const RuntimeMethod* method);
// System.Void TouchScript.Pointers.TouchPointer::set_Rotation(System.Single)
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR void TouchPointer_set_Rotation_m87442B9A19EB126F7E42949A8E0FB1FC2CB88FF9_inline (TouchPointer_t35FC788E12AC007D8F90D6D28131910B61316942 * __this, float ___value0, const RuntimeMethod* method);
// System.Void TouchScript.Pointers.TouchPointer::set_Pressure(System.Single)
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR void TouchPointer_set_Pressure_m9DAD082334050E94FCD81D8887D56CAA8D548BD0_inline (TouchPointer_t35FC788E12AC007D8F90D6D28131910B61316942 * __this, float ___value0, const RuntimeMethod* method);
// TouchScript.Core.TouchManagerInstance TouchScript.Core.TouchManagerInstance::get_Instance()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR TouchManagerInstance_t2FC1653461034CC60588BF499439FF10FCC702CB * TouchManagerInstance_get_Instance_m78AE6F31FA1A28D96EEAA748AD95F344E1E219EA (const RuntimeMethod* method);
// TouchScript.ITouchManager TouchScript.TouchManager::get_Instance()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* TouchManager_get_Instance_m355FC1AAF7F91B86F174A4429E8A44ABE454E8E3 (const RuntimeMethod* method);
// System.Void TouchScript.TouchManager::updateSendMessageSubscription()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TouchManager_updateSendMessageSubscription_mAA916B61823E409F702F20238AFA50536FBABADA (TouchManager_tE3A4C5ABD05B3B6C984CDBB1FA80479E1ACBE92B * __this, const RuntimeMethod* method);
// UnityEngine.GameObject UnityEngine.Component::get_gameObject()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * Component_get_gameObject_m0B0570BA8DDD3CD78A9DB568EA18D7317686603C (Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621 * __this, const RuntimeMethod* method);
// System.Void TouchScript.TouchManager::updateUnityEventsSubscription()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TouchManager_updateUnityEventsSubscription_m6A5CA7A27D10F83B2A5CA578C3446A140E2FF3B4 (TouchManager_tE3A4C5ABD05B3B6C984CDBB1FA80479E1ACBE92B * __this, const RuntimeMethod* method);
// System.Boolean TouchScript.TouchManager::get_ShouldCreateCameraLayer()
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR bool TouchManager_get_ShouldCreateCameraLayer_m9555EEF395EA3D416B86CD071695F48749CEA641_inline (TouchManager_tE3A4C5ABD05B3B6C984CDBB1FA80479E1ACBE92B * __this, const RuntimeMethod* method);
// System.Boolean TouchScript.TouchManager::get_ShouldCreateStandardInput()
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR bool TouchManager_get_ShouldCreateStandardInput_mE33919A0FE9C7D46BA910362D807179381DFAAE1_inline (TouchManager_tE3A4C5ABD05B3B6C984CDBB1FA80479E1ACBE92B * __this, const RuntimeMethod* method);
// !0 System.Collections.Generic.List`1<TouchScript.Layers.TouchLayer>::get_Item(System.Int32)
inline TouchLayer_t484C3424D422E6073B3FE4DD21EFE05686F5AC54 * List_1_get_Item_m9CC34A7BCCF27B11010DF6AD1031D7B256727CCB_inline (List_1_tACFDA2C40746A89629E9D8D22CD4726B64D7C856 * __this, int32_t ___index0, const RuntimeMethod* method)
{
	return ((  TouchLayer_t484C3424D422E6073B3FE4DD21EFE05686F5AC54 * (*) (List_1_tACFDA2C40746A89629E9D8D22CD4726B64D7C856 *, int32_t, const RuntimeMethod*))List_1_get_Item_mFDB8AD680C600072736579BBF5F38F7416396588_gshared_inline)(__this, ___index0, method);
}
// System.Boolean UnityEngine.Object::op_Inequality(UnityEngine.Object,UnityEngine.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Object_op_Inequality_m31EF58E217E8F4BDD3E409DEF79E1AEE95874FC1 (Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 * ___x0, Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 * ___y1, const RuntimeMethod* method);
// System.Int32 System.Collections.Generic.List`1<TouchScript.Layers.TouchLayer>::get_Count()
inline int32_t List_1_get_Count_mC85447D3C8462EC8C81F17B1EE9584D1DB2E804C_inline (List_1_tACFDA2C40746A89629E9D8D22CD4726B64D7C856 * __this, const RuntimeMethod* method)
{
	return ((  int32_t (*) (List_1_tACFDA2C40746A89629E9D8D22CD4726B64D7C856 *, const RuntimeMethod*))List_1_get_Count_m507C9149FF7F83AAC72C29091E745D557DA47D22_gshared_inline)(__this, method);
}
// System.Void TouchScript.TouchManager::removeSendMessageSubscriptions()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TouchManager_removeSendMessageSubscriptions_mDA19EA010E735C99BF9CEC546274DAC4694CF57C (TouchManager_tE3A4C5ABD05B3B6C984CDBB1FA80479E1ACBE92B * __this, const RuntimeMethod* method);
// System.Void TouchScript.TouchManager::removeUnityEventsSubscriptions()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TouchManager_removeUnityEventsSubscriptions_m16B73F9CF5C8D19E6FB2776C07439A1F6A93F3FA (TouchManager_tE3A4C5ABD05B3B6C984CDBB1FA80479E1ACBE92B * __this, const RuntimeMethod* method);
// System.Boolean UnityEngine.Application::get_isPlaying()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Application_get_isPlaying_mF43B519662E7433DD90D883E5AE22EC3CFB65CA5 (const RuntimeMethod* method);
// TouchScript.TouchManager/MessageType TouchScript.TouchManager::get_SendMessageEvents()
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR int32_t TouchManager_get_SendMessageEvents_m807C4A591F1C10FB759D7470C7D1F54484C478E0_inline (TouchManager_tE3A4C5ABD05B3B6C984CDBB1FA80479E1ACBE92B * __this, const RuntimeMethod* method);
// System.Void System.EventHandler::.ctor(System.Object,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void EventHandler__ctor_m497A2BCD46DB769EAFFDE919303FCAE226906B6F (EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method);
// System.Void System.EventHandler`1<TouchScript.PointerEventArgs>::.ctor(System.Object,System.IntPtr)
inline void EventHandler_1__ctor_mDD490FECE01D6EF94516D572915E5E6291E1B7D1 (EventHandler_1_t0363FEC12482AFCCEA69CEA150D1710057B94330 * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	((  void (*) (EventHandler_1_t0363FEC12482AFCCEA69CEA150D1710057B94330 *, RuntimeObject *, intptr_t, const RuntimeMethod*))EventHandler_1__ctor_m3D6B301FCEB48533990CFB2386DAAB091D357BAC_gshared)(__this, ___object0, ___method1, method);
}
// System.Collections.Generic.IList`1<TouchScript.Pointers.Pointer> TouchScript.PointerEventArgs::get_Pointers()
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR RuntimeObject* PointerEventArgs_get_Pointers_m16B91F1D5BE47012CF6C4BC0BC3F53691525FC41_inline (PointerEventArgs_t60C9291910F961213770A1302F3CE7CC5A580106 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.GameObject::SendMessage(System.String,System.Object,UnityEngine.SendMessageOptions)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GameObject_SendMessage_mB9147E503F1F55C4F3BC2816C0BDA8C21EA22E95 (GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * __this, String_t* ___methodName0, RuntimeObject * ___value1, int32_t ___options2, const RuntimeMethod* method);
// System.Void UnityEngine.GameObject::SendMessage(System.String,UnityEngine.SendMessageOptions)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GameObject_SendMessage_m68EE6232C4171FB0767181C1C1DD95C1103A6A75 (GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * __this, String_t* ___methodName0, int32_t ___options1, const RuntimeMethod* method);
// System.Void UnityEngine.Events.UnityEvent`1<System.Collections.Generic.IList`1<TouchScript.Pointers.Pointer>>::Invoke(!0)
inline void UnityEvent_1_Invoke_mF7D14A026E6DC7B30CDB2E16A114A79B946C7869 (UnityEvent_1_tF49B9E69A03353E280A3371637BD7D7B4B691112 * __this, RuntimeObject* ___arg00, const RuntimeMethod* method)
{
	((  void (*) (UnityEvent_1_tF49B9E69A03353E280A3371637BD7D7B4B691112 *, RuntimeObject*, const RuntimeMethod*))UnityEvent_1_Invoke_m027706B0C7150736F066D5C663304CB0B80ABBF0_gshared)(__this, ___arg00, method);
}
// System.Void UnityEngine.Events.UnityEvent::Invoke()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UnityEvent_Invoke_mB2FA1C76256FE34D5E7F84ABE528AC61CE8A0325 (UnityEvent_t5C6DDC2FCDF7F5C1808F1DDFBAD27A383F5FE65F * __this, const RuntimeMethod* method);
// System.Void TouchScript.TouchManager/FrameEvent::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void FrameEvent__ctor_mC2B90B221D4FECA515E3401E5611A8E7510BF817 (FrameEvent_tEBA65ABA56D36C692FB1A49D6929A473CA7A3817 * __this, const RuntimeMethod* method);
// System.Void TouchScript.TouchManager/PointerEvent::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PointerEvent__ctor_m63D4E009DE7DACDA202FD81E36A40E7D04F387C2 (PointerEvent_t46D6262BF503769AA7E077C49BB94DA4B83061DA * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<TouchScript.Layers.TouchLayer>::.ctor()
inline void List_1__ctor_mA49956FD2D00F5266DDF54D252195E6D951B3B56 (List_1_tACFDA2C40746A89629E9D8D22CD4726B64D7C856 * __this, const RuntimeMethod* method)
{
	((  void (*) (List_1_tACFDA2C40746A89629E9D8D22CD4726B64D7C856 *, const RuntimeMethod*))List_1__ctor_mC832F1AC0F814BAEB19175F5D7972A7507508BC3_gshared)(__this, method);
}
// System.Void TouchScript.Core.DebuggableMonoBehaviour::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DebuggableMonoBehaviour__ctor_m2721B121281EA6FCDA46C369E88D5A282D6FD065 (DebuggableMonoBehaviour_t9ECFEBA1E6079CED2A7AF5B46939A05FBDD0C667 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.Vector2::.ctor(System.Single,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Vector2__ctor_mEE8FB117AB1F8DB746FB8B3EB4C0DA3BF2A230D0 (Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * __this, float ___x0, float ___y1, const RuntimeMethod* method);
// System.Void System.Version::.ctor(System.Int32,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Version__ctor_mC2880C190E158700B0C114D4CC921C0D240DAA9C (Version_tDBE6876C59B6F56D4F8CAA03851177ABC6FE0DFD * __this, int32_t ___major0, int32_t ___minor1, const RuntimeMethod* method);
// System.Void UnityEngine.Events.UnityEvent::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UnityEvent__ctor_m2F8C02F28E289CA65598FF4FA8EAB84D955FF028 (UnityEvent_t5C6DDC2FCDF7F5C1808F1DDFBAD27A383F5FE65F * __this, const RuntimeMethod* method);
// System.Void UnityEngine.Events.UnityEvent`1<System.Collections.Generic.IList`1<TouchScript.Pointers.Pointer>>::.ctor()
inline void UnityEvent_1__ctor_m3C716A57D64E39CC8286EFD89A14C9361624E1FA (UnityEvent_1_tF49B9E69A03353E280A3371637BD7D7B4B691112 * __this, const RuntimeMethod* method)
{
	((  void (*) (UnityEvent_1_tF49B9E69A03353E280A3371637BD7D7B4B691112 *, const RuntimeMethod*))UnityEvent_1__ctor_m38CD236F782AA440F6DDB5D90B4C9DA24CDBA3A7_gshared)(__this, method);
}
// System.Void UnityEngine.PropertyAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PropertyAttribute__ctor_m7F5C473F39D5601486C1127DA0D52F2DC293FC35 (PropertyAttribute_t25BFFC093C9C96E3CCF4EAB36F5DC6F937B1FA54 * __this, const RuntimeMethod* method);
// System.Void System.Text.StringBuilder::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void StringBuilder__ctor_m1C0F2D97B838537A2D0F64033AE4EF02D150A956 (StringBuilder_t * __this, int32_t ___capacity0, const RuntimeMethod* method);
// UnityEngine.Vector2 UnityEngine.Vector2::op_Addition(UnityEngine.Vector2,UnityEngine.Vector2)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  Vector2_op_Addition_m81A4D928B8E399DA3A4E3ACD8937EDFDCB014682 (Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___a0, Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___b1, const RuntimeMethod* method);
// UnityEngine.Vector2 UnityEngine.Vector2::op_Division(UnityEngine.Vector2,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  Vector2_op_Division_m0961A935168EE6701E098E2B37013DFFF46A5077 (Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___a0, float ___d1, const RuntimeMethod* method);
// System.Int32 System.Text.StringBuilder::get_Length()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t StringBuilder_get_Length_m44BCD2BF32D45E9376761FF33AA429BFBD902F07 (StringBuilder_t * __this, const RuntimeMethod* method);
// System.Text.StringBuilder System.Text.StringBuilder::Remove(System.Int32,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR StringBuilder_t * StringBuilder_Remove_m5DA9C1C4D056FA61B8923BE85E6BFF44B14A24F9 (StringBuilder_t * __this, int32_t ___startIndex0, int32_t ___length1, const RuntimeMethod* method);
// System.Void System.EventHandler::Invoke(System.Object,System.EventArgs)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void EventHandler_Invoke_mD23D5EFEA562A05C5EACDD3E91EEDD2BF6C22800 (EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C * __this, RuntimeObject * ___sender0, EventArgs_t8E6CA180BE0E56674C6407011A94BAF7C757352E * ___e1, const RuntimeMethod* method);
// System.Void UnityEngine.Debug::LogException(System.Exception)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Debug_LogException_mBAA6702C240E37B2A834AA74E4FDC15A3A5589A9 (Exception_t * ___exception0, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Vector2::op_Implicit(UnityEngine.Vector2)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  Vector2_op_Implicit_mD152B6A34B4DB7FFECC2844D74718568FE867D6F (Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___v0, const RuntimeMethod* method);
// UnityEngine.Ray UnityEngine.Camera::ScreenPointToRay(UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Ray_tE2163D4CB3E6B267E29F8ABE41684490E4A614B2  Camera_ScreenPointToRay_m27638E78502DB6D6D7113F81AF7C210773B828F3 (Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * __this, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___pos0, const RuntimeMethod* method);
// System.Boolean UnityEngine.Plane::Raycast(UnityEngine.Ray,System.Single&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Plane_Raycast_m04E61D7C78A5DA70F4F73F9805ABB54177B799A9 (Plane_t0903921088DEEDE1BCDEA5BF279EDBCFC9679AED * __this, Ray_tE2163D4CB3E6B267E29F8ABE41684490E4A614B2  ___ray0, float* ___enter1, const RuntimeMethod* method);
// System.Boolean UnityEngine.Mathf::Approximately(System.Single,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Mathf_Approximately_m91AF00403E0D2DEA1AAE68601AD218CFAD70DF7E (float ___a0, float ___b1, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Plane::get_normal()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  Plane_get_normal_m203D43F51C449990214D04F332E8261295162E84 (Plane_t0903921088DEEDE1BCDEA5BF279EDBCFC9679AED * __this, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Vector3::op_UnaryNegation(UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  Vector3_op_UnaryNegation_m2AFBBF22801F9BCA5A4EBE642A29F433FE1339C2 (Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___a0, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Vector3::get_zero()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  Vector3_get_zero_m3CDDCAE94581DF3BB16C4B40A100E28E9C6649C2 (const RuntimeMethod* method);
// System.Single UnityEngine.Plane::GetDistanceToPoint(UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float Plane_GetDistanceToPoint_mF59C65D80AEE969807A8D78AB6B5012B07BF113A (Plane_t0903921088DEEDE1BCDEA5BF279EDBCFC9679AED * __this, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___point0, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Vector3::op_Multiply(UnityEngine.Vector3,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  Vector3_op_Multiply_m1C5F07723615156ACF035D88A1280A9E8F35A04E (Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___a0, float ___d1, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Ray::get_origin()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  Ray_get_origin_m3773CA7B1E2F26F6F1447652B485D86C0BEC5187 (Ray_tE2163D4CB3E6B267E29F8ABE41684490E4A614B2 * __this, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Ray::get_direction()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  Ray_get_direction_m9E6468CD87844B437FC4B93491E63D388322F76E (Ray_tE2163D4CB3E6B267E29F8ABE41684490E4A614B2 * __this, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Vector3::op_Addition(UnityEngine.Vector3,UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  Vector3_op_Addition_m929F9C17E5D11B94D50B4AFF1D730B70CB59B50E (Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___a0, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___b1, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Vector3::get_forward()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  Vector3_get_forward_m3E2E192B3302130098738C308FA1EE1439449D0D (const RuntimeMethod* method);
// System.Void UnityEngine.Ray::.ctor(UnityEngine.Vector3,UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Ray__ctor_m695D219349B8AA4C82F96C55A27D384C07736F6B (Ray_tE2163D4CB3E6B267E29F8ABE41684490E4A614B2 * __this, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___origin0, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___direction1, const RuntimeMethod* method);
// System.Void UnityEngine.Vector3::.ctor(System.Single,System.Single,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Vector3__ctor_m08F61F548AA5836D8789843ACB4A81E4963D2EE1 (Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * __this, float ___x0, float ___y1, float ___z2, const RuntimeMethod* method);
// TouchScript.Hit.HitData TouchScript.Pointers.Pointer::GetPressData()
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR HitData_t3022C0DF180FDE3CF86F713B78A2056D02E78980  Pointer_GetPressData_m806849D3319D7AF0EB6469AC978F593B1AD6A3B4_inline (Pointer_tBF6873B314D4CC8534B6CD8A8BA54B92D8CFDE98 * __this, const RuntimeMethod* method);
// UnityEngine.Transform TouchScript.Hit.HitData::get_Target()
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * HitData_get_Target_mD8CF226FD6A5401997C2F6F554C2BA26155F7A05_inline (HitData_t3022C0DF180FDE3CF86F713B78A2056D02E78980 * __this, const RuntimeMethod* method);
// System.Boolean TouchScript.Utils.PointerUtils::IsPointerOnTarget(TouchScript.Pointers.IPointer,UnityEngine.Transform)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool PointerUtils_IsPointerOnTarget_m73191DDEDDAA92FC5B63E82BE8C94DB49948E576 (RuntimeObject* ___pointer0, Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ___target1, const RuntimeMethod* method);
// System.Boolean TouchScript.Utils.PointerUtils::IsPointerOnTarget(TouchScript.Pointers.IPointer,UnityEngine.Transform,TouchScript.Hit.HitData&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool PointerUtils_IsPointerOnTarget_mFA6B5C217481FFAC6208A08D8564B02C464C2DBF (RuntimeObject* ___pointer0, Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ___target1, HitData_t3022C0DF180FDE3CF86F713B78A2056D02E78980 * ___hit2, const RuntimeMethod* method);
// System.Boolean UnityEngine.Transform::IsChildOf(UnityEngine.Transform)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Transform_IsChildOf_mCB98BA14F7FB82B6AF6AE961E84C47AE1D99AA80 (Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * __this, Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ___parent0, const RuntimeMethod* method);
// System.Void TouchScript.Utils.PointerUtils::initStringBuilder()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PointerUtils_initStringBuilder_mEB6698C4E28B8F6D3E4E40FE5CA452C8415C179C (const RuntimeMethod* method);
// System.Void TouchScript.Utils.PointerUtils::ButtonsToString(TouchScript.Pointers.Pointer/PointerButtonState,System.Text.StringBuilder)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PointerUtils_ButtonsToString_m90AE750BDB47BF3343173B2DC737E10AE1BB0C21 (int32_t ___buttons0, StringBuilder_t * ___builder1, const RuntimeMethod* method);
// UnityEngine.Transform UnityEngine.Transform::get_parent()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * Transform_get_parent_m8FA24E38A1FA29D90CBF3CDC9F9F017C65BB3403 (Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * __this, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Transform::InverseTransformPoint(UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  Transform_InverseTransformPoint_mB6E3145F20B531B4A781C194BAC43A8255C96C47 (Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * __this, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___position0, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Transform::InverseTransformDirection(UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  Transform_InverseTransformDirection_m6F0513F2EC19C204F2077E3C68DD1D45317CB5F2 (Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * __this, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___direction0, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Transform::get_localScale()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  Transform_get_localScale_mD8F631021C2D62B7C341B1A17FA75491F64E13DA (Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * __this, const RuntimeMethod* method);
// UnityEngine.Vector3 TouchScript.Utils.TransformUtils::GlobalToLocalVector(UnityEngine.Transform,UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  TransformUtils_GlobalToLocalVector_m57DE02E94AFA43DD446DD9C16B46140FADCF1003 (Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ___transform0, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___global1, const RuntimeMethod* method);
// UnityEngine.Quaternion UnityEngine.Transform::get_localRotation()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  Transform_get_localRotation_mEDA319E1B42EF12A19A95AC0824345B6574863FE (Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * __this, const RuntimeMethod* method);
// UnityEngine.Quaternion UnityEngine.Quaternion::Inverse(UnityEngine.Quaternion)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  Quaternion_Inverse_mC3A78571A826F05CE179637E675BD25F8B203E0C (Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  ___rotation0, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Quaternion::op_Multiply(UnityEngine.Quaternion,UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  Quaternion_op_Multiply_mD5999DE317D808808B72E58E7A978C4C0995879C (Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  ___rotation0, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___point1, const RuntimeMethod* method);
// System.Void TouchScript.Utils.TransformUtils::initStringBuilder()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TransformUtils_initStringBuilder_m6846E6CB8306F423AC924826D4D12667ED674C99 (const RuntimeMethod* method);
// System.String UnityEngine.Object::get_name()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* Object_get_name_mA2D400141CB3C991C87A2556429781DE961A83CE (Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 * __this, const RuntimeMethod* method);
// System.Text.StringBuilder System.Text.StringBuilder::Insert(System.Int32,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR StringBuilder_t * StringBuilder_Insert_m38829D9C9FE52ACD6541ED735D4435FB2A831A2C (StringBuilder_t * __this, int32_t ___index0, String_t* ___value1, const RuntimeMethod* method);
// UnityEngine.GameObject UnityEngine.GameObject::FindGameObjectWithTag(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * GameObject_FindGameObjectWithTag_m9F2877F52346B973DE3023209D73852E96ECC10D (String_t* ___tag0, const RuntimeMethod* method);
// !!0 UnityEngine.GameObject::GetComponent<UnityEngine.MeshRenderer>()
inline MeshRenderer_t9D67CA54E83315F743623BDE8EADCD5074659EED * GameObject_GetComponent_TisMeshRenderer_t9D67CA54E83315F743623BDE8EADCD5074659EED_m91C1D9637A332988C72E62D52DFCFE89A6DDAB72 (GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * __this, const RuntimeMethod* method)
{
	return ((  MeshRenderer_t9D67CA54E83315F743623BDE8EADCD5074659EED * (*) (GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F *, const RuntimeMethod*))GameObject_GetComponent_TisRuntimeObject_mD4382B2843BA9A61A01A8F9D7B9813D060F9C9CA_gshared)(__this, method);
}
// UnityEngine.Material UnityEngine.Renderer::get_material()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * Renderer_get_material_m4434513446B652652CE9FD766B0E3D1D34C4A617 (Renderer_t0556D67DD582620D1F495627EDE30D03284151F4 * __this, const RuntimeMethod* method);
// UnityEngine.Texture UnityEngine.Material::get_mainTexture()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Texture_t387FE83BB848001FD06B14707AEA6D5A0F6A95F4 * Material_get_mainTexture_mE85CF647728AD145D7E03A172EFD5930773E514E (Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * __this, const RuntimeMethod* method);
// System.Boolean UnityEngine.Object::op_Implicit(UnityEngine.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Object_op_Implicit_m8B2A44B4B1406ED346D1AE6D962294FD58D0D534 (Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 * ___exists0, const RuntimeMethod* method);
// System.String UnityEngine.Application::get_persistentDataPath()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* Application_get_persistentDataPath_m82E34156D8BD0A55CAC258CDFE8317FAD6945F5B (const RuntimeMethod* method);
// System.String System.Int32::ToString()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* Int32_ToString_m1863896DE712BF97C031D55B12E1583F1982DC02 (int32_t* __this, const RuntimeMethod* method);
// System.String System.String::Concat(System.String,System.String,System.String,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* String_Concat_mDD2E38332DED3A8C088D38D78A0E0BEB5091DA64 (String_t* ___str00, String_t* ___str11, String_t* ___str22, String_t* ___str33, const RuntimeMethod* method);
// System.Boolean System.IO.File::Exists(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool File_Exists_m6B9BDD8EEB33D744EB0590DD27BC0152FAFBD1FB (String_t* ___path0, const RuntimeMethod* method);
// System.Byte[] UnityEngine.ImageConversion::EncodeToPNG(UnityEngine.Texture2D)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ImageConversion_EncodeToPNG_m8D67A36A7D81F436CDA108CC5293E15A9CFD5617 (Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * ___tex0, const RuntimeMethod* method);
// System.Void System.IO.File::WriteAllBytes(System.String,System.Byte[])
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void File_WriteAllBytes_m07F13C1CA0BD0960392C78AB99E0F19564F9B594 (String_t* ___path0, ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___bytes1, const RuntimeMethod* method);
// System.Void UnityEngine.Object::Destroy(UnityEngine.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Object_Destroy_m23B4562495BA35A74266D4372D45368F8C05109A (Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 * ___obj0, const RuntimeMethod* method);
// System.Void UnityEngine.Debug::Log(System.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Debug_Log_m4B7C70BAFD477C6BDB59C88A0934F0B018D03708 (RuntimeObject * ___message0, const RuntimeMethod* method);
// System.Void UnityEngine.SceneManagement.SceneManager::LoadScene(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SceneManager_LoadScene_mFC850AC783E5EA05D6154976385DFECC251CDFB9 (String_t* ___sceneName0, const RuntimeMethod* method);
// System.Void UnityEngine.MonoBehaviour::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MonoBehaviour__ctor_mEAEC84B222C60319D593E456D769B3311DFCEF97 (MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429 * __this, const RuntimeMethod* method);
// System.Boolean NativeGallery::IsMediaPickerBusy()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool NativeGallery_IsMediaPickerBusy_m1AB7029F286FAB7CD4FCE7A4145DBEEB2E9946AB (const RuntimeMethod* method);
// System.Void test_nativegallery::PickImage(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void test_nativegallery_PickImage_m5836EF2B5F137638F5B23C1186F18096E8A8AF7E (test_nativegallery_tAC993D2146BA62036BC7ADD8AFF36FC4CD783627 * __this, int32_t ___maxSize0, const RuntimeMethod* method);
// System.Void test_nativegallery/<>c__DisplayClass2_0::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__DisplayClass2_0__ctor_mF95464CCBB9EDFA86D0785A8482181854DEFF131 (U3CU3Ec__DisplayClass2_0_t6812C288DD4EA8357CAFD0805FDB4C76A5DC6CB3 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.Texture2D::.ctor(System.Int32,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Texture2D__ctor_m0C86A87871AA8075791EF98499D34DA95ACB0E35 (Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * __this, int32_t ___width0, int32_t ___height1, const RuntimeMethod* method);
// System.Void NativeGallery/MediaPickCallback::.ctor(System.Object,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MediaPickCallback__ctor_m82EF4DC0AEBE82B93D0132A4205B6A28DD958979 (MediaPickCallback_t2F83A1D5E2DFEE08EECAE29F980B5BEDD1EEE77D * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method);
// NativeGallery/Permission NativeGallery::GetImageFromGallery(NativeGallery/MediaPickCallback,System.String,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t NativeGallery_GetImageFromGallery_mD6D6BF52470E918BF06AB31BAB5EB0DD320DA7C8 (MediaPickCallback_t2F83A1D5E2DFEE08EECAE29F980B5BEDD1EEE77D * ___callback0, String_t* ___title1, String_t* ___mime2, const RuntimeMethod* method);
// System.String System.String::Concat(System.Object,System.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* String_Concat_mBB19C73816BDD1C3519F248E1ADC8E11A6FDB495 (RuntimeObject * ___arg00, RuntimeObject * ___arg11, const RuntimeMethod* method);
// System.String System.String::Concat(System.String,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* String_Concat_mB78D0094592718DA6D5DB6C712A9C225631666BE (String_t* ___str00, String_t* ___str11, const RuntimeMethod* method);
// UnityEngine.Texture2D NativeGallery::LoadImageAtPath(System.String,System.Int32,System.Boolean,System.Boolean,System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * NativeGallery_LoadImageAtPath_m7E2D04E8514667DB5A7BBC450D46FDC971F2F0C7 (String_t* ___imagePath0, int32_t ___maxSize1, bool ___markTextureNonReadable2, bool ___generateMipmaps3, bool ___linearColorSpace4, const RuntimeMethod* method);
// UnityEngine.Transform UnityEngine.GameObject::get_transform()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * GameObject_get_transform_mA5C38857137F137CB96C69FAA624199EB1C2FB2C (GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * __this, const RuntimeMethod* method);
// UnityEngine.Transform UnityEngine.Component::get_transform()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * Component_get_transform_m00F05BD782F920C301A7EBA480F3B7A904C07EC9 (Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621 * __this, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Transform::get_position()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  Transform_get_position_mF54C3A064F7C8E24F1C56EE128728B2E4485E294 (Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * __this, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Transform::get_forward()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  Transform_get_forward_m0BE1E88B86049ADA39391C3ACED2314A624BC67F (Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * __this, const RuntimeMethod* method);
// System.Void UnityEngine.Transform::set_position(UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Transform_set_position_mDA89E4893F14ECA5CBEEE7FB80A5BF7C1B8EA6DC (Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * __this, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___value0, const RuntimeMethod* method);
// System.Void UnityEngine.Transform::set_forward(UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Transform_set_forward_m02858E8B3313B27174B19E9113F24EF25FBCEC7F (Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * __this, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___value0, const RuntimeMethod* method);
// System.Void UnityEngine.Transform::set_localScale(UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Transform_set_localScale_m7ED1A6E5A87CD1D483515B99D6D3121FB92B0556 (Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * __this, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___value0, const RuntimeMethod* method);
// !!0 UnityEngine.GameObject::GetComponent<UnityEngine.Renderer>()
inline Renderer_t0556D67DD582620D1F495627EDE30D03284151F4 * GameObject_GetComponent_TisRenderer_t0556D67DD582620D1F495627EDE30D03284151F4_mD65E2552CCFED4D0EC506EE90DE51215D90AEF85 (GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * __this, const RuntimeMethod* method)
{
	return ((  Renderer_t0556D67DD582620D1F495627EDE30D03284151F4 * (*) (GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F *, const RuntimeMethod*))GameObject_GetComponent_TisRuntimeObject_mD4382B2843BA9A61A01A8F9D7B9813D060F9C9CA_gshared)(__this, method);
}
// UnityEngine.Shader UnityEngine.Material::get_shader()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Shader_tE2731FF351B74AB4186897484FB01E000C1160CA * Material_get_shader_m9CEDCA4D97D42588C6B827400E364E4A8EC55FF0 (Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * __this, const RuntimeMethod* method);
// System.Boolean UnityEngine.Shader::get_isSupported()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Shader_get_isSupported_m3660681289CDFE742D399C3030A6CF5C4D8B030D (Shader_tE2731FF351B74AB4186897484FB01E000C1160CA * __this, const RuntimeMethod* method);
// UnityEngine.Shader UnityEngine.Shader::Find(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Shader_tE2731FF351B74AB4186897484FB01E000C1160CA * Shader_Find_m755654AA68D1C663A3E20A10E00CDC10F96C962B (String_t* ___name0, const RuntimeMethod* method);
// System.Void UnityEngine.Material::set_shader(UnityEngine.Shader)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Material_set_shader_m689F997F888E3C2A0FF9E6F399AA5D8204B454B1 (Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * __this, Shader_tE2731FF351B74AB4186897484FB01E000C1160CA * ___value0, const RuntimeMethod* method);
// System.Void UnityEngine.Material::set_mainTexture(UnityEngine.Texture)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Material_set_mainTexture_m0742CFF768E9701618DA07C71F009239AB31EB41 (Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * __this, Texture_t387FE83BB848001FD06B14707AEA6D5A0F6A95F4 * ___value0, const RuntimeMethod* method);
// System.Void System.ThrowHelper::ThrowArgumentOutOfRangeException()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ThrowHelper_ThrowArgumentOutOfRangeException_mBA2AF20A35144E0C43CD721A22EAC9FCA15D6550 (const RuntimeMethod* method);
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void TouchScript.Layers.WorldSpaceCanvasProjectionParams::.ctor(UnityEngine.Canvas)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WorldSpaceCanvasProjectionParams__ctor_mDDE0E638C385DBFF93B8CB5802CF8D3D8B39BC4C (WorldSpaceCanvasProjectionParams_t221503CDBFCD2E7D18AB639A41EFCB1F4BF05A3E * __this, Canvas_tBC28BF1DD8D8499A89B5781505833D3728CF8591 * ___canvas0, const RuntimeMethod* method)
{
	Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * G_B2_0 = NULL;
	WorldSpaceCanvasProjectionParams_t221503CDBFCD2E7D18AB639A41EFCB1F4BF05A3E * G_B2_1 = NULL;
	Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * G_B1_0 = NULL;
	WorldSpaceCanvasProjectionParams_t221503CDBFCD2E7D18AB639A41EFCB1F4BF05A3E * G_B1_1 = NULL;
	{
		// public WorldSpaceCanvasProjectionParams(Canvas canvas)
		ProjectionParams__ctor_m6A4C0C8062A36F504724741380C7479CCD9AB348(__this, /*hidden argument*/NULL);
		// this.canvas = canvas;
		Canvas_tBC28BF1DD8D8499A89B5781505833D3728CF8591 * L_0 = ___canvas0;
		__this->set_canvas_0(L_0);
		// mode = canvas.renderMode;
		Canvas_tBC28BF1DD8D8499A89B5781505833D3728CF8591 * L_1 = ___canvas0;
		NullCheck(L_1);
		int32_t L_2 = Canvas_get_renderMode_mAF68701B143F01C7D19B6C7D3033E3B34ECB2FC8(L_1, /*hidden argument*/NULL);
		__this->set_mode_1(L_2);
		// camera = canvas.worldCamera ?? Camera.main;
		Canvas_tBC28BF1DD8D8499A89B5781505833D3728CF8591 * L_3 = ___canvas0;
		NullCheck(L_3);
		Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * L_4 = Canvas_get_worldCamera_m36F1A8DBFC4AB34278125DA017CACDC873F53409(L_3, /*hidden argument*/NULL);
		Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * L_5 = L_4;
		G_B1_0 = L_5;
		G_B1_1 = __this;
		if (L_5)
		{
			G_B2_0 = L_5;
			G_B2_1 = __this;
			goto IL_0029;
		}
	}
	{
		Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * L_6 = Camera_get_main_m9256A9F84F92D7ED73F3E6C4E2694030AD8B61FA(/*hidden argument*/NULL);
		G_B2_0 = L_6;
		G_B2_1 = G_B1_1;
	}

IL_0029:
	{
		NullCheck(G_B2_1);
		G_B2_1->set_camera_2(G_B2_0);
		// }
		return;
	}
}
// UnityEngine.Vector3 TouchScript.Layers.WorldSpaceCanvasProjectionParams::ProjectTo(UnityEngine.Vector2,UnityEngine.Plane)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  WorldSpaceCanvasProjectionParams_ProjectTo_m1D26196396B95BA4A98BAD0D3366D951DA12F030 (WorldSpaceCanvasProjectionParams_t221503CDBFCD2E7D18AB639A41EFCB1F4BF05A3E * __this, Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___screenPosition0, Plane_t0903921088DEEDE1BCDEA5BF279EDBCFC9679AED  ___projectionPlane1, const RuntimeMethod* method)
{
	{
		// return ProjectionUtils.CameraToPlaneProjection(screenPosition, camera, projectionPlane);
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_0 = ___screenPosition0;
		Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * L_1 = __this->get_camera_2();
		Plane_t0903921088DEEDE1BCDEA5BF279EDBCFC9679AED  L_2 = ___projectionPlane1;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_3 = ProjectionUtils_CameraToPlaneProjection_m49C90C2A65AB392F106E1B043BB38EC1D603376A(L_0, L_1, L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
// UnityEngine.Vector2 TouchScript.Layers.WorldSpaceCanvasProjectionParams::ProjectFrom(UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  WorldSpaceCanvasProjectionParams_ProjectFrom_mBD552841734A8E42D1978277AF584B317C84A321 (WorldSpaceCanvasProjectionParams_t221503CDBFCD2E7D18AB639A41EFCB1F4BF05A3E * __this, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___worldPosition0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WorldSpaceCanvasProjectionParams_ProjectFrom_mBD552841734A8E42D1978277AF584B317C84A321_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// return camera.WorldToScreenPoint(worldPosition);
		Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * L_0 = __this->get_camera_2();
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_1 = ___worldPosition0;
		NullCheck(L_0);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_2 = Camera_WorldToScreenPoint_m880F9611E4848C11F21FDF1A1D307B401C61B1BF(L_0, L_1, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_il2cpp_TypeInfo_var);
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_3 = Vector2_op_Implicit_mEA1F75961E3D368418BA8CEB9C40E55C25BA3C28(L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Collections.Generic.IList`1<TouchScript.Pointers.Pointer> TouchScript.PointerEventArgs::get_Pointers()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* PointerEventArgs_get_Pointers_m16B91F1D5BE47012CF6C4BC0BC3F53691525FC41 (PointerEventArgs_t60C9291910F961213770A1302F3CE7CC5A580106 * __this, const RuntimeMethod* method)
{
	{
		// public IList<Pointer> Pointers { get; private set; }
		RuntimeObject* L_0 = __this->get_U3CPointersU3Ek__BackingField_1();
		return L_0;
	}
}
// System.Void TouchScript.PointerEventArgs::set_Pointers(System.Collections.Generic.IList`1<TouchScript.Pointers.Pointer>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PointerEventArgs_set_Pointers_m80547B23A481F55D60DEE830E6340F85555B550A (PointerEventArgs_t60C9291910F961213770A1302F3CE7CC5A580106 * __this, RuntimeObject* ___value0, const RuntimeMethod* method)
{
	{
		// public IList<Pointer> Pointers { get; private set; }
		RuntimeObject* L_0 = ___value0;
		__this->set_U3CPointersU3Ek__BackingField_1(L_0);
		return;
	}
}
// System.Void TouchScript.PointerEventArgs::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PointerEventArgs__ctor_m2184ED9B05EDC50EC544304A7B0EEE2BE31DA2F0 (PointerEventArgs_t60C9291910F961213770A1302F3CE7CC5A580106 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PointerEventArgs__ctor_m2184ED9B05EDC50EC544304A7B0EEE2BE31DA2F0_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// private PointerEventArgs() {}
		IL2CPP_RUNTIME_CLASS_INIT(EventArgs_t8E6CA180BE0E56674C6407011A94BAF7C757352E_il2cpp_TypeInfo_var);
		EventArgs__ctor_m3551293259861C5A78CD47689D559F828ED29DF7(__this, /*hidden argument*/NULL);
		// private PointerEventArgs() {}
		return;
	}
}
// TouchScript.PointerEventArgs TouchScript.PointerEventArgs::GetCachedEventArgs(System.Collections.Generic.IList`1<TouchScript.Pointers.Pointer>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR PointerEventArgs_t60C9291910F961213770A1302F3CE7CC5A580106 * PointerEventArgs_GetCachedEventArgs_m32D6BD4E2C26738D2BB82F24904DBBB59587ED5F (RuntimeObject* ___pointers0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PointerEventArgs_GetCachedEventArgs_m32D6BD4E2C26738D2BB82F24904DBBB59587ED5F_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// if (instance == null) instance = new PointerEventArgs();
		PointerEventArgs_t60C9291910F961213770A1302F3CE7CC5A580106 * L_0 = ((PointerEventArgs_t60C9291910F961213770A1302F3CE7CC5A580106_StaticFields*)il2cpp_codegen_static_fields_for(PointerEventArgs_t60C9291910F961213770A1302F3CE7CC5A580106_il2cpp_TypeInfo_var))->get_instance_2();
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		// if (instance == null) instance = new PointerEventArgs();
		PointerEventArgs_t60C9291910F961213770A1302F3CE7CC5A580106 * L_1 = (PointerEventArgs_t60C9291910F961213770A1302F3CE7CC5A580106 *)il2cpp_codegen_object_new(PointerEventArgs_t60C9291910F961213770A1302F3CE7CC5A580106_il2cpp_TypeInfo_var);
		PointerEventArgs__ctor_m2184ED9B05EDC50EC544304A7B0EEE2BE31DA2F0(L_1, /*hidden argument*/NULL);
		((PointerEventArgs_t60C9291910F961213770A1302F3CE7CC5A580106_StaticFields*)il2cpp_codegen_static_fields_for(PointerEventArgs_t60C9291910F961213770A1302F3CE7CC5A580106_il2cpp_TypeInfo_var))->set_instance_2(L_1);
	}

IL_0011:
	{
		// instance.Pointers = pointers;
		PointerEventArgs_t60C9291910F961213770A1302F3CE7CC5A580106 * L_2 = ((PointerEventArgs_t60C9291910F961213770A1302F3CE7CC5A580106_StaticFields*)il2cpp_codegen_static_fields_for(PointerEventArgs_t60C9291910F961213770A1302F3CE7CC5A580106_il2cpp_TypeInfo_var))->get_instance_2();
		RuntimeObject* L_3 = ___pointers0;
		NullCheck(L_2);
		PointerEventArgs_set_Pointers_m80547B23A481F55D60DEE830E6340F85555B550A_inline(L_2, L_3, /*hidden argument*/NULL);
		// return instance;
		PointerEventArgs_t60C9291910F961213770A1302F3CE7CC5A580106 * L_4 = ((PointerEventArgs_t60C9291910F961213770A1302F3CE7CC5A580106_StaticFields*)il2cpp_codegen_static_fields_for(PointerEventArgs_t60C9291910F961213770A1302F3CE7CC5A580106_il2cpp_TypeInfo_var))->get_instance_2();
		return L_4;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Int32 TouchScript.Pointers.FakePointer::get_Id()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t FakePointer_get_Id_m7F37A5013C88FA454666B85294990FD3C5BE5EE3 (FakePointer_t8C1AEEBB63CF9BCC8170772F4E4601CD4298BC47 * __this, const RuntimeMethod* method)
{
	{
		// public int Id { get; private set; }
		int32_t L_0 = __this->get_U3CIdU3Ek__BackingField_0();
		return L_0;
	}
}
// System.Void TouchScript.Pointers.FakePointer::set_Id(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void FakePointer_set_Id_mC0D41C4688A1B33B826E36B909C6A0E3983EFEB5 (FakePointer_t8C1AEEBB63CF9BCC8170772F4E4601CD4298BC47 * __this, int32_t ___value0, const RuntimeMethod* method)
{
	{
		// public int Id { get; private set; }
		int32_t L_0 = ___value0;
		__this->set_U3CIdU3Ek__BackingField_0(L_0);
		return;
	}
}
// TouchScript.Pointers.Pointer_PointerType TouchScript.Pointers.FakePointer::get_Type()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t FakePointer_get_Type_m0273E1B87410A58AF1941B97AE4547FE7F22B588 (FakePointer_t8C1AEEBB63CF9BCC8170772F4E4601CD4298BC47 * __this, const RuntimeMethod* method)
{
	{
		// public Pointer.PointerType Type { get; private set; }
		int32_t L_0 = __this->get_U3CTypeU3Ek__BackingField_1();
		return L_0;
	}
}
// System.Void TouchScript.Pointers.FakePointer::set_Type(TouchScript.Pointers.Pointer_PointerType)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void FakePointer_set_Type_m79DF4E0AF1EEF7C1BD223A21042D27D9E12327EC (FakePointer_t8C1AEEBB63CF9BCC8170772F4E4601CD4298BC47 * __this, int32_t ___value0, const RuntimeMethod* method)
{
	{
		// public Pointer.PointerType Type { get; private set; }
		int32_t L_0 = ___value0;
		__this->set_U3CTypeU3Ek__BackingField_1(L_0);
		return;
	}
}
// TouchScript.InputSources.IInputSource TouchScript.Pointers.FakePointer::get_InputSource()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* FakePointer_get_InputSource_m43787A4A83F64C058A0E58DEA93B595FA05C0670 (FakePointer_t8C1AEEBB63CF9BCC8170772F4E4601CD4298BC47 * __this, const RuntimeMethod* method)
{
	{
		// public IInputSource InputSource { get; private set; }
		RuntimeObject* L_0 = __this->get_U3CInputSourceU3Ek__BackingField_2();
		return L_0;
	}
}
// System.Void TouchScript.Pointers.FakePointer::set_InputSource(TouchScript.InputSources.IInputSource)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void FakePointer_set_InputSource_mB5DEBE9F1174C7584540B72CD9DE5FF267967CB5 (FakePointer_t8C1AEEBB63CF9BCC8170772F4E4601CD4298BC47 * __this, RuntimeObject* ___value0, const RuntimeMethod* method)
{
	{
		// public IInputSource InputSource { get; private set; }
		RuntimeObject* L_0 = ___value0;
		__this->set_U3CInputSourceU3Ek__BackingField_2(L_0);
		return;
	}
}
// UnityEngine.Vector2 TouchScript.Pointers.FakePointer::get_Position()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  FakePointer_get_Position_m9FD582FD15FF725FF49EF261D11455ABAF2F6BB7 (FakePointer_t8C1AEEBB63CF9BCC8170772F4E4601CD4298BC47 * __this, const RuntimeMethod* method)
{
	{
		// public Vector2 Position { get; set; }
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_0 = __this->get_U3CPositionU3Ek__BackingField_3();
		return L_0;
	}
}
// System.Void TouchScript.Pointers.FakePointer::set_Position(UnityEngine.Vector2)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void FakePointer_set_Position_mCA620D1E36778D9DD9DB87D24F6899D5EFEBBF34 (FakePointer_t8C1AEEBB63CF9BCC8170772F4E4601CD4298BC47 * __this, Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___value0, const RuntimeMethod* method)
{
	{
		// public Vector2 Position { get; set; }
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_0 = ___value0;
		__this->set_U3CPositionU3Ek__BackingField_3(L_0);
		return;
	}
}
// System.UInt32 TouchScript.Pointers.FakePointer::get_Flags()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR uint32_t FakePointer_get_Flags_m85F8668433273916F920F9C20331964A5C0BB71A (FakePointer_t8C1AEEBB63CF9BCC8170772F4E4601CD4298BC47 * __this, const RuntimeMethod* method)
{
	{
		// public uint Flags { get; private set; }
		uint32_t L_0 = __this->get_U3CFlagsU3Ek__BackingField_4();
		return L_0;
	}
}
// System.Void TouchScript.Pointers.FakePointer::set_Flags(System.UInt32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void FakePointer_set_Flags_m64E3B23758D21054F7CB36BB6033AA7088169EB8 (FakePointer_t8C1AEEBB63CF9BCC8170772F4E4601CD4298BC47 * __this, uint32_t ___value0, const RuntimeMethod* method)
{
	{
		// public uint Flags { get; private set; }
		uint32_t L_0 = ___value0;
		__this->set_U3CFlagsU3Ek__BackingField_4(L_0);
		return;
	}
}
// TouchScript.Pointers.Pointer_PointerButtonState TouchScript.Pointers.FakePointer::get_Buttons()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t FakePointer_get_Buttons_mFB0294AF9118DD3A20720111DD9BB5349873D224 (FakePointer_t8C1AEEBB63CF9BCC8170772F4E4601CD4298BC47 * __this, const RuntimeMethod* method)
{
	{
		// public Pointer.PointerButtonState Buttons { get; private set; }
		int32_t L_0 = __this->get_U3CButtonsU3Ek__BackingField_5();
		return L_0;
	}
}
// System.Void TouchScript.Pointers.FakePointer::set_Buttons(TouchScript.Pointers.Pointer_PointerButtonState)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void FakePointer_set_Buttons_m85F4C76E07206AAFFB0F917283159DCDC4261280 (FakePointer_t8C1AEEBB63CF9BCC8170772F4E4601CD4298BC47 * __this, int32_t ___value0, const RuntimeMethod* method)
{
	{
		// public Pointer.PointerButtonState Buttons { get; private set; }
		int32_t L_0 = ___value0;
		__this->set_U3CButtonsU3Ek__BackingField_5(L_0);
		return;
	}
}
// UnityEngine.Vector2 TouchScript.Pointers.FakePointer::get_PreviousPosition()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  FakePointer_get_PreviousPosition_m2F7D67485F3BBA05DB7CC1E4A07AC85743450ECB (FakePointer_t8C1AEEBB63CF9BCC8170772F4E4601CD4298BC47 * __this, const RuntimeMethod* method)
{
	{
		// public Vector2 PreviousPosition { get; private set; }
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_0 = __this->get_U3CPreviousPositionU3Ek__BackingField_6();
		return L_0;
	}
}
// System.Void TouchScript.Pointers.FakePointer::set_PreviousPosition(UnityEngine.Vector2)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void FakePointer_set_PreviousPosition_mCA34B1330CCAD003F7A6033C7F3EB24C712C3608 (FakePointer_t8C1AEEBB63CF9BCC8170772F4E4601CD4298BC47 * __this, Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___value0, const RuntimeMethod* method)
{
	{
		// public Vector2 PreviousPosition { get; private set; }
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_0 = ___value0;
		__this->set_U3CPreviousPositionU3Ek__BackingField_6(L_0);
		return;
	}
}
// System.Void TouchScript.Pointers.FakePointer::.ctor(UnityEngine.Vector2)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void FakePointer__ctor_m296C5E4936AD664F9CA7065525B7AC8CEF1D5F0E (FakePointer_t8C1AEEBB63CF9BCC8170772F4E4601CD4298BC47 * __this, Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___position0, const RuntimeMethod* method)
{
	{
		// public FakePointer(Vector2 position) : this()
		FakePointer__ctor_mAB218D43DC15CD0732B9995C7F5EF5D08FFA2F3C(__this, /*hidden argument*/NULL);
		// Position = position;
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_0 = ___position0;
		FakePointer_set_Position_mCA620D1E36778D9DD9DB87D24F6899D5EFEBBF34_inline(__this, L_0, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void TouchScript.Pointers.FakePointer::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void FakePointer__ctor_mAB218D43DC15CD0732B9995C7F5EF5D08FFA2F3C (FakePointer_t8C1AEEBB63CF9BCC8170772F4E4601CD4298BC47 * __this, const RuntimeMethod* method)
{
	{
		// public FakePointer()
		Object__ctor_m925ECA5E85CA100E3FB86A4F9E15C120E9A184C0(__this, /*hidden argument*/NULL);
		// Id = Pointer.INVALID_POINTER;
		FakePointer_set_Id_mC0D41C4688A1B33B826E36B909C6A0E3983EFEB5_inline(__this, (-1), /*hidden argument*/NULL);
		// Type = Pointer.PointerType.Unknown;
		FakePointer_set_Type_m79DF4E0AF1EEF7C1BD223A21042D27D9E12327EC_inline(__this, 0, /*hidden argument*/NULL);
		// Flags = Pointer.FLAG_ARTIFICIAL;
		FakePointer_set_Flags_m64E3B23758D21054F7CB36BB6033AA7088169EB8_inline(__this, 1, /*hidden argument*/NULL);
		// }
		return;
	}
}
// TouchScript.Hit.HitData TouchScript.Pointers.FakePointer::GetOverData(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR HitData_t3022C0DF180FDE3CF86F713B78A2056D02E78980  FakePointer_GetOverData_mAF9904063454D323B0D8E6758558C7F892680D31 (FakePointer_t8C1AEEBB63CF9BCC8170772F4E4601CD4298BC47 * __this, bool ___forceRecalculate0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (FakePointer_GetOverData_mAF9904063454D323B0D8E6758558C7F892680D31_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	HitData_t3022C0DF180FDE3CF86F713B78A2056D02E78980  V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		// LayerManager.Instance.GetHitTarget(this, out overData);
		RuntimeObject* L_0 = LayerManager_get_Instance_mA50DB5F8ACCAF0DCDE4BEEA78843142A752CF9F7(/*hidden argument*/NULL);
		NullCheck(L_0);
		InterfaceFuncInvoker2< bool, RuntimeObject*, HitData_t3022C0DF180FDE3CF86F713B78A2056D02E78980 * >::Invoke(7 /* System.Boolean TouchScript.ILayerManager::GetHitTarget(TouchScript.Pointers.IPointer,TouchScript.Hit.HitData&) */, ILayerManager_tE1F41773696005EFB2A5D97608D9598795083E74_il2cpp_TypeInfo_var, L_0, __this, (HitData_t3022C0DF180FDE3CF86F713B78A2056D02E78980 *)(&V_0));
		// return overData;
		HitData_t3022C0DF180FDE3CF86F713B78A2056D02E78980  L_1 = V_0;
		return L_1;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// UnityEngine.Vector2 TouchScript.Pointers.MousePointer::get_ScrollDelta()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  MousePointer_get_ScrollDelta_mC8F97C22EFBD42A56861A7B1EF1907185F0746A3 (MousePointer_tEFB01FDD2340977461DEB8153349F62DFDE0E100 * __this, const RuntimeMethod* method)
{
	{
		// public Vector2 ScrollDelta { get; set; }
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_0 = __this->get_U3CScrollDeltaU3Ek__BackingField_18();
		return L_0;
	}
}
// System.Void TouchScript.Pointers.MousePointer::set_ScrollDelta(UnityEngine.Vector2)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MousePointer_set_ScrollDelta_m352D328338CF5FF4420BEA90891189D57C517C1F (MousePointer_tEFB01FDD2340977461DEB8153349F62DFDE0E100 * __this, Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___value0, const RuntimeMethod* method)
{
	{
		// public Vector2 ScrollDelta { get; set; }
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_0 = ___value0;
		__this->set_U3CScrollDeltaU3Ek__BackingField_18(L_0);
		return;
	}
}
// System.Void TouchScript.Pointers.MousePointer::.ctor(TouchScript.InputSources.IInputSource)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MousePointer__ctor_m406338CE653654229558A5BCC38F97692A452158 (MousePointer_tEFB01FDD2340977461DEB8153349F62DFDE0E100 * __this, RuntimeObject* ___input0, const RuntimeMethod* method)
{
	{
		// public MousePointer(IInputSource input) : base(input)
		RuntimeObject* L_0 = ___input0;
		Pointer__ctor_mE7828F1485349BF2A610843ABB9A7944396F2D6C(__this, L_0, /*hidden argument*/NULL);
		// Type = PointerType.Mouse;
		Pointer_set_Type_mC0D513634112590ABB8EB877E8BA70416BDF2625_inline(__this, 2, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void TouchScript.Pointers.MousePointer::CopyFrom(TouchScript.Pointers.Pointer)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MousePointer_CopyFrom_m9568581ECA2A9BB14241FB2E292E4FE15591EA2F (MousePointer_tEFB01FDD2340977461DEB8153349F62DFDE0E100 * __this, Pointer_tBF6873B314D4CC8534B6CD8A8BA54B92D8CFDE98 * ___target0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MousePointer_CopyFrom_m9568581ECA2A9BB14241FB2E292E4FE15591EA2F_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	MousePointer_tEFB01FDD2340977461DEB8153349F62DFDE0E100 * V_0 = NULL;
	{
		// base.CopyFrom(target);
		Pointer_tBF6873B314D4CC8534B6CD8A8BA54B92D8CFDE98 * L_0 = ___target0;
		Pointer_CopyFrom_m323BE52F7920E028E626E6C42E1C5A230CAB1494(__this, L_0, /*hidden argument*/NULL);
		// var mouseTarget = target as MousePointer;
		Pointer_tBF6873B314D4CC8534B6CD8A8BA54B92D8CFDE98 * L_1 = ___target0;
		V_0 = ((MousePointer_tEFB01FDD2340977461DEB8153349F62DFDE0E100 *)IsInstClass((RuntimeObject*)L_1, MousePointer_tEFB01FDD2340977461DEB8153349F62DFDE0E100_il2cpp_TypeInfo_var));
		// if (mouseTarget == null) return;
		MousePointer_tEFB01FDD2340977461DEB8153349F62DFDE0E100 * L_2 = V_0;
		if (L_2)
		{
			goto IL_0012;
		}
	}
	{
		// if (mouseTarget == null) return;
		return;
	}

IL_0012:
	{
		// ScrollDelta = mouseTarget.ScrollDelta;
		MousePointer_tEFB01FDD2340977461DEB8153349F62DFDE0E100 * L_3 = V_0;
		NullCheck(L_3);
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_4 = MousePointer_get_ScrollDelta_mC8F97C22EFBD42A56861A7B1EF1907185F0746A3_inline(L_3, /*hidden argument*/NULL);
		MousePointer_set_ScrollDelta_m352D328338CF5FF4420BEA90891189D57C517C1F_inline(__this, L_4, /*hidden argument*/NULL);
		// }
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Int32 TouchScript.Pointers.ObjectPointer::get_ObjectId()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t ObjectPointer_get_ObjectId_m19757D9C574D591D4DD3043EEFBDDCE1064B2581 (ObjectPointer_tD190FCA2463DDBBD72FBA988696DFC51EFF46C36 * __this, const RuntimeMethod* method)
{
	{
		// public int ObjectId { get; internal set; }
		int32_t L_0 = __this->get_U3CObjectIdU3Ek__BackingField_22();
		return L_0;
	}
}
// System.Void TouchScript.Pointers.ObjectPointer::set_ObjectId(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ObjectPointer_set_ObjectId_m4951549B2096276F8533B2D025F744CE8E5D4FDD (ObjectPointer_tD190FCA2463DDBBD72FBA988696DFC51EFF46C36 * __this, int32_t ___value0, const RuntimeMethod* method)
{
	{
		// public int ObjectId { get; internal set; }
		int32_t L_0 = ___value0;
		__this->set_U3CObjectIdU3Ek__BackingField_22(L_0);
		return;
	}
}
// System.Single TouchScript.Pointers.ObjectPointer::get_Width()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float ObjectPointer_get_Width_m92F855D6202D7ECC31204F7BFB1924B26DA5F007 (ObjectPointer_tD190FCA2463DDBBD72FBA988696DFC51EFF46C36 * __this, const RuntimeMethod* method)
{
	{
		// public float Width { get; internal set; }
		float L_0 = __this->get_U3CWidthU3Ek__BackingField_23();
		return L_0;
	}
}
// System.Void TouchScript.Pointers.ObjectPointer::set_Width(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ObjectPointer_set_Width_mFCDC006E0026F249E35C88362BD748EE5A14AFD7 (ObjectPointer_tD190FCA2463DDBBD72FBA988696DFC51EFF46C36 * __this, float ___value0, const RuntimeMethod* method)
{
	{
		// public float Width { get; internal set; }
		float L_0 = ___value0;
		__this->set_U3CWidthU3Ek__BackingField_23(L_0);
		return;
	}
}
// System.Single TouchScript.Pointers.ObjectPointer::get_Height()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float ObjectPointer_get_Height_m30D7BD97E5CD11924D9111F3011E0E453CAA6DA0 (ObjectPointer_tD190FCA2463DDBBD72FBA988696DFC51EFF46C36 * __this, const RuntimeMethod* method)
{
	{
		// public float Height { get; internal set; }
		float L_0 = __this->get_U3CHeightU3Ek__BackingField_24();
		return L_0;
	}
}
// System.Void TouchScript.Pointers.ObjectPointer::set_Height(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ObjectPointer_set_Height_m793FAF0083E3C0DC47E6825EE6ECCA7F342CE66A (ObjectPointer_tD190FCA2463DDBBD72FBA988696DFC51EFF46C36 * __this, float ___value0, const RuntimeMethod* method)
{
	{
		// public float Height { get; internal set; }
		float L_0 = ___value0;
		__this->set_U3CHeightU3Ek__BackingField_24(L_0);
		return;
	}
}
// System.Single TouchScript.Pointers.ObjectPointer::get_Angle()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float ObjectPointer_get_Angle_mF6824CF5CA2316D34523B893EE50BEF01DF9D1E9 (ObjectPointer_tD190FCA2463DDBBD72FBA988696DFC51EFF46C36 * __this, const RuntimeMethod* method)
{
	{
		// public float Angle { get; internal set; }
		float L_0 = __this->get_U3CAngleU3Ek__BackingField_25();
		return L_0;
	}
}
// System.Void TouchScript.Pointers.ObjectPointer::set_Angle(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ObjectPointer_set_Angle_m3710F36254EBE84ACBC00123055FFB1CE8D8D108 (ObjectPointer_tD190FCA2463DDBBD72FBA988696DFC51EFF46C36 * __this, float ___value0, const RuntimeMethod* method)
{
	{
		// public float Angle { get; internal set; }
		float L_0 = ___value0;
		__this->set_U3CAngleU3Ek__BackingField_25(L_0);
		return;
	}
}
// System.Void TouchScript.Pointers.ObjectPointer::.ctor(TouchScript.InputSources.IInputSource)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ObjectPointer__ctor_m24E6EC51EA50C4F5BF815D88CF8F84A552D98A41 (ObjectPointer_tD190FCA2463DDBBD72FBA988696DFC51EFF46C36 * __this, RuntimeObject* ___input0, const RuntimeMethod* method)
{
	{
		// public ObjectPointer(IInputSource input) : base(input)
		RuntimeObject* L_0 = ___input0;
		Pointer__ctor_mE7828F1485349BF2A610843ABB9A7944396F2D6C(__this, L_0, /*hidden argument*/NULL);
		// Type = PointerType.Object;
		Pointer_set_Type_mC0D513634112590ABB8EB877E8BA70416BDF2625_inline(__this, 4, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void TouchScript.Pointers.ObjectPointer::CopyFrom(TouchScript.Pointers.Pointer)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ObjectPointer_CopyFrom_mC615B4489D31D9AF0AEC3C4992CC93C7A562A14A (ObjectPointer_tD190FCA2463DDBBD72FBA988696DFC51EFF46C36 * __this, Pointer_tBF6873B314D4CC8534B6CD8A8BA54B92D8CFDE98 * ___target0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ObjectPointer_CopyFrom_mC615B4489D31D9AF0AEC3C4992CC93C7A562A14A_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	ObjectPointer_tD190FCA2463DDBBD72FBA988696DFC51EFF46C36 * V_0 = NULL;
	{
		// base.CopyFrom(target);
		Pointer_tBF6873B314D4CC8534B6CD8A8BA54B92D8CFDE98 * L_0 = ___target0;
		Pointer_CopyFrom_m323BE52F7920E028E626E6C42E1C5A230CAB1494(__this, L_0, /*hidden argument*/NULL);
		// var obj = target as ObjectPointer;
		Pointer_tBF6873B314D4CC8534B6CD8A8BA54B92D8CFDE98 * L_1 = ___target0;
		V_0 = ((ObjectPointer_tD190FCA2463DDBBD72FBA988696DFC51EFF46C36 *)IsInstClass((RuntimeObject*)L_1, ObjectPointer_tD190FCA2463DDBBD72FBA988696DFC51EFF46C36_il2cpp_TypeInfo_var));
		// if (obj == null) return;
		ObjectPointer_tD190FCA2463DDBBD72FBA988696DFC51EFF46C36 * L_2 = V_0;
		if (L_2)
		{
			goto IL_0012;
		}
	}
	{
		// if (obj == null) return;
		return;
	}

IL_0012:
	{
		// ObjectId = obj.ObjectId;
		ObjectPointer_tD190FCA2463DDBBD72FBA988696DFC51EFF46C36 * L_3 = V_0;
		NullCheck(L_3);
		int32_t L_4 = ObjectPointer_get_ObjectId_m19757D9C574D591D4DD3043EEFBDDCE1064B2581_inline(L_3, /*hidden argument*/NULL);
		ObjectPointer_set_ObjectId_m4951549B2096276F8533B2D025F744CE8E5D4FDD_inline(__this, L_4, /*hidden argument*/NULL);
		// Width = obj.Width;
		ObjectPointer_tD190FCA2463DDBBD72FBA988696DFC51EFF46C36 * L_5 = V_0;
		NullCheck(L_5);
		float L_6 = ObjectPointer_get_Width_m92F855D6202D7ECC31204F7BFB1924B26DA5F007_inline(L_5, /*hidden argument*/NULL);
		ObjectPointer_set_Width_mFCDC006E0026F249E35C88362BD748EE5A14AFD7_inline(__this, L_6, /*hidden argument*/NULL);
		// Height = obj.Height;
		ObjectPointer_tD190FCA2463DDBBD72FBA988696DFC51EFF46C36 * L_7 = V_0;
		NullCheck(L_7);
		float L_8 = ObjectPointer_get_Height_m30D7BD97E5CD11924D9111F3011E0E453CAA6DA0_inline(L_7, /*hidden argument*/NULL);
		ObjectPointer_set_Height_m793FAF0083E3C0DC47E6825EE6ECCA7F342CE66A_inline(__this, L_8, /*hidden argument*/NULL);
		// Angle = obj.Angle;
		ObjectPointer_tD190FCA2463DDBBD72FBA988696DFC51EFF46C36 * L_9 = V_0;
		NullCheck(L_9);
		float L_10 = ObjectPointer_get_Angle_mF6824CF5CA2316D34523B893EE50BEF01DF9D1E9_inline(L_9, /*hidden argument*/NULL);
		ObjectPointer_set_Angle_m3710F36254EBE84ACBC00123055FFB1CE8D8D108_inline(__this, L_10, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void TouchScript.Pointers.ObjectPointer::INTERNAL_Reset()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ObjectPointer_INTERNAL_Reset_mAFD389FA014F7C7EE0FEC7A4F81CDB9D5CE410A4 (ObjectPointer_tD190FCA2463DDBBD72FBA988696DFC51EFF46C36 * __this, const RuntimeMethod* method)
{
	{
		// base.INTERNAL_Reset();
		Pointer_INTERNAL_Reset_m851FB3F6C82DD723EB5A78235BC0B45865FE8897(__this, /*hidden argument*/NULL);
		// ObjectId = DEFAULT_OBJECT_ID;
		ObjectPointer_set_ObjectId_m4951549B2096276F8533B2D025F744CE8E5D4FDD_inline(__this, 0, /*hidden argument*/NULL);
		// Width = DEFAULT_WIDTH;
		ObjectPointer_set_Width_mFCDC006E0026F249E35C88362BD748EE5A14AFD7_inline(__this, (1.0f), /*hidden argument*/NULL);
		// Height = DEFAULT_HEIGHT;
		ObjectPointer_set_Height_m793FAF0083E3C0DC47E6825EE6ECCA7F342CE66A_inline(__this, (1.0f), /*hidden argument*/NULL);
		// Angle = DEFAULT_ANGLE;
		ObjectPointer_set_Angle_m3710F36254EBE84ACBC00123055FFB1CE8D8D108_inline(__this, (0.0f), /*hidden argument*/NULL);
		// }
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Single TouchScript.Pointers.PenPointer::get_Rotation()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float PenPointer_get_Rotation_m83B389E7974ED8C633C93AE5CC7811D3F8980BEA (PenPointer_tAF65E3C674E3160B823CFBB7D82046A3FFD60340 * __this, const RuntimeMethod* method)
{
	{
		// public float Rotation { get; set; }
		float L_0 = __this->get_U3CRotationU3Ek__BackingField_20();
		return L_0;
	}
}
// System.Void TouchScript.Pointers.PenPointer::set_Rotation(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PenPointer_set_Rotation_m74FF90FEA33FF1621F9792CBDDE3A31569246E08 (PenPointer_tAF65E3C674E3160B823CFBB7D82046A3FFD60340 * __this, float ___value0, const RuntimeMethod* method)
{
	{
		// public float Rotation { get; set; }
		float L_0 = ___value0;
		__this->set_U3CRotationU3Ek__BackingField_20(L_0);
		return;
	}
}
// System.Single TouchScript.Pointers.PenPointer::get_Pressure()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float PenPointer_get_Pressure_m7594304DCC404FB3D58E741002C56E92B44BFF30 (PenPointer_tAF65E3C674E3160B823CFBB7D82046A3FFD60340 * __this, const RuntimeMethod* method)
{
	{
		// public float Pressure { get; set; }
		float L_0 = __this->get_U3CPressureU3Ek__BackingField_21();
		return L_0;
	}
}
// System.Void TouchScript.Pointers.PenPointer::set_Pressure(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PenPointer_set_Pressure_m33A397E626F343E987248BEC63FAB5F18621BD61 (PenPointer_tAF65E3C674E3160B823CFBB7D82046A3FFD60340 * __this, float ___value0, const RuntimeMethod* method)
{
	{
		// public float Pressure { get; set; }
		float L_0 = ___value0;
		__this->set_U3CPressureU3Ek__BackingField_21(L_0);
		return;
	}
}
// System.Void TouchScript.Pointers.PenPointer::.ctor(TouchScript.InputSources.IInputSource)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PenPointer__ctor_m6D8A7F282AAE1FA58ECC119BF00ECF3C534AE5E2 (PenPointer_tAF65E3C674E3160B823CFBB7D82046A3FFD60340 * __this, RuntimeObject* ___input0, const RuntimeMethod* method)
{
	{
		// public PenPointer(IInputSource input) : base(input)
		RuntimeObject* L_0 = ___input0;
		Pointer__ctor_mE7828F1485349BF2A610843ABB9A7944396F2D6C(__this, L_0, /*hidden argument*/NULL);
		// Type = PointerType.Pen;
		Pointer_set_Type_mC0D513634112590ABB8EB877E8BA70416BDF2625_inline(__this, 3, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void TouchScript.Pointers.PenPointer::INTERNAL_Reset()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PenPointer_INTERNAL_Reset_mB10622BC68374DB2B92C6111E0D3BAA2BF38A718 (PenPointer_tAF65E3C674E3160B823CFBB7D82046A3FFD60340 * __this, const RuntimeMethod* method)
{
	{
		// base.INTERNAL_Reset();
		Pointer_INTERNAL_Reset_m851FB3F6C82DD723EB5A78235BC0B45865FE8897(__this, /*hidden argument*/NULL);
		// Rotation = DEFAULT_ROTATION;
		PenPointer_set_Rotation_m74FF90FEA33FF1621F9792CBDDE3A31569246E08_inline(__this, (0.0f), /*hidden argument*/NULL);
		// Pressure = DEFAULT_PRESSURE;
		PenPointer_set_Pressure_m33A397E626F343E987248BEC63FAB5F18621BD61_inline(__this, (0.5f), /*hidden argument*/NULL);
		// }
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Int32 TouchScript.Pointers.Pointer::get_Id()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t Pointer_get_Id_mFF112114C234DC146CA93A7719C3F316E8DACCAD (Pointer_tBF6873B314D4CC8534B6CD8A8BA54B92D8CFDE98 * __this, const RuntimeMethod* method)
{
	{
		// public int Id { get; private set; }
		int32_t L_0 = __this->get_U3CIdU3Ek__BackingField_4();
		return L_0;
	}
}
// System.Void TouchScript.Pointers.Pointer::set_Id(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Pointer_set_Id_mEB0B14E2905D84B287EABA142EEAFF7737D63886 (Pointer_tBF6873B314D4CC8534B6CD8A8BA54B92D8CFDE98 * __this, int32_t ___value0, const RuntimeMethod* method)
{
	{
		// public int Id { get; private set; }
		int32_t L_0 = ___value0;
		__this->set_U3CIdU3Ek__BackingField_4(L_0);
		return;
	}
}
// TouchScript.Pointers.Pointer_PointerType TouchScript.Pointers.Pointer::get_Type()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t Pointer_get_Type_m8733FBC6C16FE2C6FBA301C50E8E26266720B9EB (Pointer_tBF6873B314D4CC8534B6CD8A8BA54B92D8CFDE98 * __this, const RuntimeMethod* method)
{
	{
		// public PointerType Type { get; protected set; }
		int32_t L_0 = __this->get_U3CTypeU3Ek__BackingField_5();
		return L_0;
	}
}
// System.Void TouchScript.Pointers.Pointer::set_Type(TouchScript.Pointers.Pointer_PointerType)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Pointer_set_Type_mC0D513634112590ABB8EB877E8BA70416BDF2625 (Pointer_tBF6873B314D4CC8534B6CD8A8BA54B92D8CFDE98 * __this, int32_t ___value0, const RuntimeMethod* method)
{
	{
		// public PointerType Type { get; protected set; }
		int32_t L_0 = ___value0;
		__this->set_U3CTypeU3Ek__BackingField_5(L_0);
		return;
	}
}
// TouchScript.Pointers.Pointer_PointerButtonState TouchScript.Pointers.Pointer::get_Buttons()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t Pointer_get_Buttons_mDD144F267D37B07E930E44E9B7D9357246D5839A (Pointer_tBF6873B314D4CC8534B6CD8A8BA54B92D8CFDE98 * __this, const RuntimeMethod* method)
{
	{
		// public PointerButtonState Buttons { get; set; }
		int32_t L_0 = __this->get_U3CButtonsU3Ek__BackingField_6();
		return L_0;
	}
}
// System.Void TouchScript.Pointers.Pointer::set_Buttons(TouchScript.Pointers.Pointer_PointerButtonState)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Pointer_set_Buttons_mAE901843226D346730CAD2CAD512793E4E4BEAC8 (Pointer_tBF6873B314D4CC8534B6CD8A8BA54B92D8CFDE98 * __this, int32_t ___value0, const RuntimeMethod* method)
{
	{
		// public PointerButtonState Buttons { get; set; }
		int32_t L_0 = ___value0;
		__this->set_U3CButtonsU3Ek__BackingField_6(L_0);
		return;
	}
}
// TouchScript.InputSources.IInputSource TouchScript.Pointers.Pointer::get_InputSource()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* Pointer_get_InputSource_m39065F33E59F38803F3CB3DB02DF22DF17332149 (Pointer_tBF6873B314D4CC8534B6CD8A8BA54B92D8CFDE98 * __this, const RuntimeMethod* method)
{
	{
		// public IInputSource InputSource { get; private set; }
		RuntimeObject* L_0 = __this->get_U3CInputSourceU3Ek__BackingField_7();
		return L_0;
	}
}
// System.Void TouchScript.Pointers.Pointer::set_InputSource(TouchScript.InputSources.IInputSource)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Pointer_set_InputSource_m646284A86B72DC008C111B607C12E38F4A4EA385 (Pointer_tBF6873B314D4CC8534B6CD8A8BA54B92D8CFDE98 * __this, RuntimeObject* ___value0, const RuntimeMethod* method)
{
	{
		// public IInputSource InputSource { get; private set; }
		RuntimeObject* L_0 = ___value0;
		__this->set_U3CInputSourceU3Ek__BackingField_7(L_0);
		return;
	}
}
// UnityEngine.Vector2 TouchScript.Pointers.Pointer::get_Position()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  Pointer_get_Position_mCE0B33AF8B7D86A8BA6824E7817A3DFCB29BE4BC (Pointer_tBF6873B314D4CC8534B6CD8A8BA54B92D8CFDE98 * __this, const RuntimeMethod* method)
{
	{
		// get { return position; }
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_0 = __this->get_position_13();
		return L_0;
	}
}
// System.Void TouchScript.Pointers.Pointer::set_Position(UnityEngine.Vector2)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Pointer_set_Position_m00CDB115C2F9FE3EDDB493FCCF6016D09AD52C4A (Pointer_tBF6873B314D4CC8534B6CD8A8BA54B92D8CFDE98 * __this, Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___value0, const RuntimeMethod* method)
{
	{
		// set { newPosition = value; }
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_0 = ___value0;
		__this->set_newPosition_14(L_0);
		// set { newPosition = value; }
		return;
	}
}
// UnityEngine.Vector2 TouchScript.Pointers.Pointer::get_PreviousPosition()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  Pointer_get_PreviousPosition_m18CBA71CDAE159A709A4F88D96F40B81B333118E (Pointer_tBF6873B314D4CC8534B6CD8A8BA54B92D8CFDE98 * __this, const RuntimeMethod* method)
{
	{
		// public Vector2 PreviousPosition { get; private set; }
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_0 = __this->get_U3CPreviousPositionU3Ek__BackingField_8();
		return L_0;
	}
}
// System.Void TouchScript.Pointers.Pointer::set_PreviousPosition(UnityEngine.Vector2)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Pointer_set_PreviousPosition_m9825F629338D0DF6160DCB6DAADF3488860FA703 (Pointer_tBF6873B314D4CC8534B6CD8A8BA54B92D8CFDE98 * __this, Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___value0, const RuntimeMethod* method)
{
	{
		// public Vector2 PreviousPosition { get; private set; }
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_0 = ___value0;
		__this->set_U3CPreviousPositionU3Ek__BackingField_8(L_0);
		return;
	}
}
// System.UInt32 TouchScript.Pointers.Pointer::get_Flags()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR uint32_t Pointer_get_Flags_m941FB1843C3FD979278B353B040264E56F68A883 (Pointer_tBF6873B314D4CC8534B6CD8A8BA54B92D8CFDE98 * __this, const RuntimeMethod* method)
{
	{
		// public uint Flags { get; set; }
		uint32_t L_0 = __this->get_U3CFlagsU3Ek__BackingField_9();
		return L_0;
	}
}
// System.Void TouchScript.Pointers.Pointer::set_Flags(System.UInt32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Pointer_set_Flags_mF6A08BE8419E9E7DFD4CD15AF3A4E811734E9293 (Pointer_tBF6873B314D4CC8534B6CD8A8BA54B92D8CFDE98 * __this, uint32_t ___value0, const RuntimeMethod* method)
{
	{
		// public uint Flags { get; set; }
		uint32_t L_0 = ___value0;
		__this->set_U3CFlagsU3Ek__BackingField_9(L_0);
		return;
	}
}
// TouchScript.Layers.ProjectionParams TouchScript.Pointers.Pointer::get_ProjectionParams()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR ProjectionParams_t2B7BDC4A52427186D2923201E1479C2E7641E6B8 * Pointer_get_ProjectionParams_mECA4594E2279D92F8731317FA2BA2F3CD7904C38 (Pointer_tBF6873B314D4CC8534B6CD8A8BA54B92D8CFDE98 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Pointer_get_ProjectionParams_mECA4594E2279D92F8731317FA2BA2F3CD7904C38_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// if (pressData.Layer == null) return null;
		HitData_t3022C0DF180FDE3CF86F713B78A2056D02E78980 * L_0 = __this->get_address_of_pressData_15();
		TouchLayer_t484C3424D422E6073B3FE4DD21EFE05686F5AC54 * L_1 = HitData_get_Layer_m80AD3383FA69E902C6C6E7B29769771F24EAEA51_inline((HitData_t3022C0DF180FDE3CF86F713B78A2056D02E78980 *)L_0, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_il2cpp_TypeInfo_var);
		bool L_2 = Object_op_Equality_mBC2401774F3BE33E8CF6F0A8148E66C95D6CFF1C(L_1, (Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 *)NULL, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0015;
		}
	}
	{
		// if (pressData.Layer == null) return null;
		return (ProjectionParams_t2B7BDC4A52427186D2923201E1479C2E7641E6B8 *)NULL;
	}

IL_0015:
	{
		// return pressData.Layer.GetProjectionParams(this);
		HitData_t3022C0DF180FDE3CF86F713B78A2056D02E78980 * L_3 = __this->get_address_of_pressData_15();
		TouchLayer_t484C3424D422E6073B3FE4DD21EFE05686F5AC54 * L_4 = HitData_get_Layer_m80AD3383FA69E902C6C6E7B29769771F24EAEA51_inline((HitData_t3022C0DF180FDE3CF86F713B78A2056D02E78980 *)L_3, /*hidden argument*/NULL);
		NullCheck(L_4);
		ProjectionParams_t2B7BDC4A52427186D2923201E1479C2E7641E6B8 * L_5 = VirtFuncInvoker1< ProjectionParams_t2B7BDC4A52427186D2923201E1479C2E7641E6B8 *, Pointer_tBF6873B314D4CC8534B6CD8A8BA54B92D8CFDE98 * >::Invoke(5 /* TouchScript.Layers.ProjectionParams TouchScript.Layers.TouchLayer::GetProjectionParams(TouchScript.Pointers.Pointer) */, L_4, __this);
		return L_5;
	}
}
// TouchScript.Hit.HitData TouchScript.Pointers.Pointer::GetOverData(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR HitData_t3022C0DF180FDE3CF86F713B78A2056D02E78980  Pointer_GetOverData_m3E939E2B28388235F9797C61CBBDABC0905DD195 (Pointer_tBF6873B314D4CC8534B6CD8A8BA54B92D8CFDE98 * __this, bool ___forceRecalculate0, const RuntimeMethod* method)
{
	{
		// if (overDataIsDirty || forceRecalculate)
		bool L_0 = __this->get_overDataIsDirty_17();
		bool L_1 = ___forceRecalculate0;
		if (!((int32_t)((int32_t)L_0|(int32_t)L_1)))
		{
			goto IL_0024;
		}
	}
	{
		// layerManager.GetHitTarget(this, out overData);
		LayerManagerInstance_t88CDE54CE87FC7A715E326B47F38C3B0054D0578 * L_2 = __this->get_layerManager_11();
		HitData_t3022C0DF180FDE3CF86F713B78A2056D02E78980 * L_3 = __this->get_address_of_overData_16();
		NullCheck(L_2);
		LayerManagerInstance_GetHitTarget_m9B935210E7FF3A5562AE24FA17AB0936252E72D9(L_2, __this, (HitData_t3022C0DF180FDE3CF86F713B78A2056D02E78980 *)L_3, /*hidden argument*/NULL);
		// overDataIsDirty = false;
		__this->set_overDataIsDirty_17((bool)0);
	}

IL_0024:
	{
		// return overData;
		HitData_t3022C0DF180FDE3CF86F713B78A2056D02E78980  L_4 = __this->get_overData_16();
		return L_4;
	}
}
// TouchScript.Hit.HitData TouchScript.Pointers.Pointer::GetPressData()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR HitData_t3022C0DF180FDE3CF86F713B78A2056D02E78980  Pointer_GetPressData_m806849D3319D7AF0EB6469AC978F593B1AD6A3B4 (Pointer_tBF6873B314D4CC8534B6CD8A8BA54B92D8CFDE98 * __this, const RuntimeMethod* method)
{
	{
		// return pressData;
		HitData_t3022C0DF180FDE3CF86F713B78A2056D02E78980  L_0 = __this->get_pressData_15();
		return L_0;
	}
}
// System.Void TouchScript.Pointers.Pointer::CopyFrom(TouchScript.Pointers.Pointer)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Pointer_CopyFrom_m323BE52F7920E028E626E6C42E1C5A230CAB1494 (Pointer_tBF6873B314D4CC8534B6CD8A8BA54B92D8CFDE98 * __this, Pointer_tBF6873B314D4CC8534B6CD8A8BA54B92D8CFDE98 * ___target0, const RuntimeMethod* method)
{
	{
		// Type = target.Type;
		Pointer_tBF6873B314D4CC8534B6CD8A8BA54B92D8CFDE98 * L_0 = ___target0;
		NullCheck(L_0);
		int32_t L_1 = Pointer_get_Type_m8733FBC6C16FE2C6FBA301C50E8E26266720B9EB_inline(L_0, /*hidden argument*/NULL);
		Pointer_set_Type_mC0D513634112590ABB8EB877E8BA70416BDF2625_inline(__this, L_1, /*hidden argument*/NULL);
		// Flags = target.Flags;
		Pointer_tBF6873B314D4CC8534B6CD8A8BA54B92D8CFDE98 * L_2 = ___target0;
		NullCheck(L_2);
		uint32_t L_3 = Pointer_get_Flags_m941FB1843C3FD979278B353B040264E56F68A883_inline(L_2, /*hidden argument*/NULL);
		Pointer_set_Flags_mF6A08BE8419E9E7DFD4CD15AF3A4E811734E9293_inline(__this, L_3, /*hidden argument*/NULL);
		// Buttons = target.Buttons;
		Pointer_tBF6873B314D4CC8534B6CD8A8BA54B92D8CFDE98 * L_4 = ___target0;
		NullCheck(L_4);
		int32_t L_5 = Pointer_get_Buttons_mDD144F267D37B07E930E44E9B7D9357246D5839A_inline(L_4, /*hidden argument*/NULL);
		Pointer_set_Buttons_mAE901843226D346730CAD2CAD512793E4E4BEAC8_inline(__this, L_5, /*hidden argument*/NULL);
		// position = target.position;
		Pointer_tBF6873B314D4CC8534B6CD8A8BA54B92D8CFDE98 * L_6 = ___target0;
		NullCheck(L_6);
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_7 = L_6->get_position_13();
		__this->set_position_13(L_7);
		// newPosition = target.newPosition;
		Pointer_tBF6873B314D4CC8534B6CD8A8BA54B92D8CFDE98 * L_8 = ___target0;
		NullCheck(L_8);
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_9 = L_8->get_newPosition_14();
		__this->set_newPosition_14(L_9);
		// PreviousPosition = target.PreviousPosition;
		Pointer_tBF6873B314D4CC8534B6CD8A8BA54B92D8CFDE98 * L_10 = ___target0;
		NullCheck(L_10);
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_11 = Pointer_get_PreviousPosition_m18CBA71CDAE159A709A4F88D96F40B81B333118E_inline(L_10, /*hidden argument*/NULL);
		Pointer_set_PreviousPosition_m9825F629338D0DF6160DCB6DAADF3488860FA703_inline(__this, L_11, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Boolean TouchScript.Pointers.Pointer::Equals(System.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Pointer_Equals_mB22D2809944C62A89A29FB04C2FC45DA69F38D9A (Pointer_tBF6873B314D4CC8534B6CD8A8BA54B92D8CFDE98 * __this, RuntimeObject * ___other0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Pointer_Equals_mB22D2809944C62A89A29FB04C2FC45DA69F38D9A_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// return Equals(other as Pointer);
		RuntimeObject * L_0 = ___other0;
		bool L_1 = Pointer_Equals_mBB1ABA52EFE71C48513257DDD931073295A2EE1C(__this, ((Pointer_tBF6873B314D4CC8534B6CD8A8BA54B92D8CFDE98 *)IsInstClass((RuntimeObject*)L_0, Pointer_tBF6873B314D4CC8534B6CD8A8BA54B92D8CFDE98_il2cpp_TypeInfo_var)), /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Boolean TouchScript.Pointers.Pointer::Equals(TouchScript.Pointers.Pointer)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Pointer_Equals_mBB1ABA52EFE71C48513257DDD931073295A2EE1C (Pointer_tBF6873B314D4CC8534B6CD8A8BA54B92D8CFDE98 * __this, Pointer_tBF6873B314D4CC8534B6CD8A8BA54B92D8CFDE98 * ___other0, const RuntimeMethod* method)
{
	{
		// if (other == null)
		Pointer_tBF6873B314D4CC8534B6CD8A8BA54B92D8CFDE98 * L_0 = ___other0;
		if (L_0)
		{
			goto IL_0005;
		}
	}
	{
		// return false;
		return (bool)0;
	}

IL_0005:
	{
		// return Id == other.Id;
		int32_t L_1 = Pointer_get_Id_mFF112114C234DC146CA93A7719C3F316E8DACCAD_inline(__this, /*hidden argument*/NULL);
		Pointer_tBF6873B314D4CC8534B6CD8A8BA54B92D8CFDE98 * L_2 = ___other0;
		NullCheck(L_2);
		int32_t L_3 = Pointer_get_Id_mFF112114C234DC146CA93A7719C3F316E8DACCAD_inline(L_2, /*hidden argument*/NULL);
		return (bool)((((int32_t)L_1) == ((int32_t)L_3))? 1 : 0);
	}
}
// System.Int32 TouchScript.Pointers.Pointer::GetHashCode()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t Pointer_GetHashCode_m408BF0466B46E542E764170915D38FDB1EA60B5B (Pointer_tBF6873B314D4CC8534B6CD8A8BA54B92D8CFDE98 * __this, const RuntimeMethod* method)
{
	{
		// return Id;
		int32_t L_0 = Pointer_get_Id_mFF112114C234DC146CA93A7719C3F316E8DACCAD_inline(__this, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.String TouchScript.Pointers.Pointer::ToString()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* Pointer_ToString_mFFA8218E27F99EC02496ABECBB46E8D4A5AE37B6 (Pointer_tBF6873B314D4CC8534B6CD8A8BA54B92D8CFDE98 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Pointer_ToString_mFFA8218E27F99EC02496ABECBB46E8D4A5AE37B6_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// if (builder == null) builder = new StringBuilder();
		StringBuilder_t * L_0 = ((Pointer_tBF6873B314D4CC8534B6CD8A8BA54B92D8CFDE98_StaticFields*)il2cpp_codegen_static_fields_for(Pointer_tBF6873B314D4CC8534B6CD8A8BA54B92D8CFDE98_il2cpp_TypeInfo_var))->get_builder_10();
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		// if (builder == null) builder = new StringBuilder();
		StringBuilder_t * L_1 = (StringBuilder_t *)il2cpp_codegen_object_new(StringBuilder_t_il2cpp_TypeInfo_var);
		StringBuilder__ctor_mF928376F82E8C8FF3C11842C562DB8CF28B2735E(L_1, /*hidden argument*/NULL);
		((Pointer_tBF6873B314D4CC8534B6CD8A8BA54B92D8CFDE98_StaticFields*)il2cpp_codegen_static_fields_for(Pointer_tBF6873B314D4CC8534B6CD8A8BA54B92D8CFDE98_il2cpp_TypeInfo_var))->set_builder_10(L_1);
	}

IL_0011:
	{
		// builder.Length = 0;
		StringBuilder_t * L_2 = ((Pointer_tBF6873B314D4CC8534B6CD8A8BA54B92D8CFDE98_StaticFields*)il2cpp_codegen_static_fields_for(Pointer_tBF6873B314D4CC8534B6CD8A8BA54B92D8CFDE98_il2cpp_TypeInfo_var))->get_builder_10();
		NullCheck(L_2);
		StringBuilder_set_Length_m84AF318230AE5C3D0D48F1CE7C2170F6F5C19F5B(L_2, 0, /*hidden argument*/NULL);
		// builder.Append("(Pointer type: ");
		StringBuilder_t * L_3 = ((Pointer_tBF6873B314D4CC8534B6CD8A8BA54B92D8CFDE98_StaticFields*)il2cpp_codegen_static_fields_for(Pointer_tBF6873B314D4CC8534B6CD8A8BA54B92D8CFDE98_il2cpp_TypeInfo_var))->get_builder_10();
		NullCheck(L_3);
		StringBuilder_Append_mDBB8CCBB7750C67BE2F2D92F47E6C0FA42793260(L_3, _stringLiteral35D5CC13D15FFCD6CBD0E77EA9F29C9C0A77CC64, /*hidden argument*/NULL);
		// builder.Append(Type);
		StringBuilder_t * L_4 = ((Pointer_tBF6873B314D4CC8534B6CD8A8BA54B92D8CFDE98_StaticFields*)il2cpp_codegen_static_fields_for(Pointer_tBF6873B314D4CC8534B6CD8A8BA54B92D8CFDE98_il2cpp_TypeInfo_var))->get_builder_10();
		int32_t L_5 = Pointer_get_Type_m8733FBC6C16FE2C6FBA301C50E8E26266720B9EB_inline(__this, /*hidden argument*/NULL);
		int32_t L_6 = L_5;
		RuntimeObject * L_7 = Box(PointerType_t3FA6771F37343BF7F3F1070EB36821D6A1C30F74_il2cpp_TypeInfo_var, &L_6);
		NullCheck(L_4);
		StringBuilder_Append_mA1A063A1388A21C8EA011DBA7FC98C24C3EE3D65(L_4, L_7, /*hidden argument*/NULL);
		// builder.Append(", id: ");
		StringBuilder_t * L_8 = ((Pointer_tBF6873B314D4CC8534B6CD8A8BA54B92D8CFDE98_StaticFields*)il2cpp_codegen_static_fields_for(Pointer_tBF6873B314D4CC8534B6CD8A8BA54B92D8CFDE98_il2cpp_TypeInfo_var))->get_builder_10();
		NullCheck(L_8);
		StringBuilder_Append_mDBB8CCBB7750C67BE2F2D92F47E6C0FA42793260(L_8, _stringLiteral7D56E9C94386F0AEE3567E79F1F6C6E37D7DB208, /*hidden argument*/NULL);
		// builder.Append(Id);
		StringBuilder_t * L_9 = ((Pointer_tBF6873B314D4CC8534B6CD8A8BA54B92D8CFDE98_StaticFields*)il2cpp_codegen_static_fields_for(Pointer_tBF6873B314D4CC8534B6CD8A8BA54B92D8CFDE98_il2cpp_TypeInfo_var))->get_builder_10();
		int32_t L_10 = Pointer_get_Id_mFF112114C234DC146CA93A7719C3F316E8DACCAD_inline(__this, /*hidden argument*/NULL);
		NullCheck(L_9);
		StringBuilder_Append_m85874CFF9E4B152DB2A91936C6F2CA3E9B1EFF84(L_9, L_10, /*hidden argument*/NULL);
		// builder.Append(", buttons: ");
		StringBuilder_t * L_11 = ((Pointer_tBF6873B314D4CC8534B6CD8A8BA54B92D8CFDE98_StaticFields*)il2cpp_codegen_static_fields_for(Pointer_tBF6873B314D4CC8534B6CD8A8BA54B92D8CFDE98_il2cpp_TypeInfo_var))->get_builder_10();
		NullCheck(L_11);
		StringBuilder_Append_mDBB8CCBB7750C67BE2F2D92F47E6C0FA42793260(L_11, _stringLiteralC439CE99A2A7165851EEA3A677E1703E301A17E0, /*hidden argument*/NULL);
		// PointerUtils.PressedButtonsToString(Buttons, builder);
		int32_t L_12 = Pointer_get_Buttons_mDD144F267D37B07E930E44E9B7D9357246D5839A_inline(__this, /*hidden argument*/NULL);
		StringBuilder_t * L_13 = ((Pointer_tBF6873B314D4CC8534B6CD8A8BA54B92D8CFDE98_StaticFields*)il2cpp_codegen_static_fields_for(Pointer_tBF6873B314D4CC8534B6CD8A8BA54B92D8CFDE98_il2cpp_TypeInfo_var))->get_builder_10();
		PointerUtils_PressedButtonsToString_mAF4CD9F932292EE068D39024DDD1E70F7DC26D9F(L_12, L_13, /*hidden argument*/NULL);
		// builder.Append(", flags: ");
		StringBuilder_t * L_14 = ((Pointer_tBF6873B314D4CC8534B6CD8A8BA54B92D8CFDE98_StaticFields*)il2cpp_codegen_static_fields_for(Pointer_tBF6873B314D4CC8534B6CD8A8BA54B92D8CFDE98_il2cpp_TypeInfo_var))->get_builder_10();
		NullCheck(L_14);
		StringBuilder_Append_mDBB8CCBB7750C67BE2F2D92F47E6C0FA42793260(L_14, _stringLiteral04258F681B5AEA7A6BE8615854D7A0F58C3F32B8, /*hidden argument*/NULL);
		// BinaryUtils.ToBinaryString(Flags, builder, 8);
		uint32_t L_15 = Pointer_get_Flags_m941FB1843C3FD979278B353B040264E56F68A883_inline(__this, /*hidden argument*/NULL);
		StringBuilder_t * L_16 = ((Pointer_tBF6873B314D4CC8534B6CD8A8BA54B92D8CFDE98_StaticFields*)il2cpp_codegen_static_fields_for(Pointer_tBF6873B314D4CC8534B6CD8A8BA54B92D8CFDE98_il2cpp_TypeInfo_var))->get_builder_10();
		BinaryUtils_ToBinaryString_mCB55E824E5042B4C35CD8EF55C3090C73D1B3FEC(L_15, L_16, 8, /*hidden argument*/NULL);
		// builder.Append(", position: ");
		StringBuilder_t * L_17 = ((Pointer_tBF6873B314D4CC8534B6CD8A8BA54B92D8CFDE98_StaticFields*)il2cpp_codegen_static_fields_for(Pointer_tBF6873B314D4CC8534B6CD8A8BA54B92D8CFDE98_il2cpp_TypeInfo_var))->get_builder_10();
		NullCheck(L_17);
		StringBuilder_Append_mDBB8CCBB7750C67BE2F2D92F47E6C0FA42793260(L_17, _stringLiteral3AE32277A8C4E1D11AF871E4E8CEF4961CE0C046, /*hidden argument*/NULL);
		// builder.Append(Position);
		StringBuilder_t * L_18 = ((Pointer_tBF6873B314D4CC8534B6CD8A8BA54B92D8CFDE98_StaticFields*)il2cpp_codegen_static_fields_for(Pointer_tBF6873B314D4CC8534B6CD8A8BA54B92D8CFDE98_il2cpp_TypeInfo_var))->get_builder_10();
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_19 = Pointer_get_Position_mCE0B33AF8B7D86A8BA6824E7817A3DFCB29BE4BC_inline(__this, /*hidden argument*/NULL);
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_20 = L_19;
		RuntimeObject * L_21 = Box(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_il2cpp_TypeInfo_var, &L_20);
		NullCheck(L_18);
		StringBuilder_Append_mA1A063A1388A21C8EA011DBA7FC98C24C3EE3D65(L_18, L_21, /*hidden argument*/NULL);
		// builder.Append(")");
		StringBuilder_t * L_22 = ((Pointer_tBF6873B314D4CC8534B6CD8A8BA54B92D8CFDE98_StaticFields*)il2cpp_codegen_static_fields_for(Pointer_tBF6873B314D4CC8534B6CD8A8BA54B92D8CFDE98_il2cpp_TypeInfo_var))->get_builder_10();
		NullCheck(L_22);
		StringBuilder_Append_mDBB8CCBB7750C67BE2F2D92F47E6C0FA42793260(L_22, _stringLiteralE7064F0B80F61DBC65915311032D27BAA569AE2A, /*hidden argument*/NULL);
		// return builder.ToString();
		StringBuilder_t * L_23 = ((Pointer_tBF6873B314D4CC8534B6CD8A8BA54B92D8CFDE98_StaticFields*)il2cpp_codegen_static_fields_for(Pointer_tBF6873B314D4CC8534B6CD8A8BA54B92D8CFDE98_il2cpp_TypeInfo_var))->get_builder_10();
		NullCheck(L_23);
		String_t* L_24 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_23);
		return L_24;
	}
}
// System.Void TouchScript.Pointers.Pointer::.ctor(TouchScript.InputSources.IInputSource)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Pointer__ctor_mE7828F1485349BF2A610843ABB9A7944396F2D6C (Pointer_tBF6873B314D4CC8534B6CD8A8BA54B92D8CFDE98 * __this, RuntimeObject* ___input0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Pointer__ctor_mE7828F1485349BF2A610843ABB9A7944396F2D6C_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// private bool overDataIsDirty = true;
		__this->set_overDataIsDirty_17((bool)1);
		// public Pointer(IInputSource input)
		Object__ctor_m925ECA5E85CA100E3FB86A4F9E15C120E9A184C0(__this, /*hidden argument*/NULL);
		// layerManager = LayerManager.Instance as LayerManagerInstance;
		RuntimeObject* L_0 = LayerManager_get_Instance_mA50DB5F8ACCAF0DCDE4BEEA78843142A752CF9F7(/*hidden argument*/NULL);
		__this->set_layerManager_11(((LayerManagerInstance_t88CDE54CE87FC7A715E326B47F38C3B0054D0578 *)IsInstSealed((RuntimeObject*)L_0, LayerManagerInstance_t88CDE54CE87FC7A715E326B47F38C3B0054D0578_il2cpp_TypeInfo_var)));
		// Type = PointerType.Unknown;
		Pointer_set_Type_mC0D513634112590ABB8EB877E8BA70416BDF2625_inline(__this, 0, /*hidden argument*/NULL);
		// InputSource = input;
		RuntimeObject* L_1 = ___input0;
		Pointer_set_InputSource_m646284A86B72DC008C111B607C12E38F4A4EA385_inline(__this, L_1, /*hidden argument*/NULL);
		// INTERNAL_Reset();
		VirtActionInvoker0::Invoke(16 /* System.Void TouchScript.Pointers.Pointer::INTERNAL_Reset() */, __this);
		// }
		return;
	}
}
// System.Void TouchScript.Pointers.Pointer::INTERNAL_Init(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Pointer_INTERNAL_Init_m2E683FC93E6121AAC8BF03B4B10E2DE3274A2C5C (Pointer_tBF6873B314D4CC8534B6CD8A8BA54B92D8CFDE98 * __this, int32_t ___id0, const RuntimeMethod* method)
{
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		// Id = id;
		int32_t L_0 = ___id0;
		Pointer_set_Id_mEB0B14E2905D84B287EABA142EEAFF7737D63886_inline(__this, L_0, /*hidden argument*/NULL);
		// PreviousPosition = position = newPosition;
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_1 = __this->get_newPosition_14();
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_2 = L_1;
		V_0 = L_2;
		__this->set_position_13(L_2);
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_3 = V_0;
		Pointer_set_PreviousPosition_m9825F629338D0DF6160DCB6DAADF3488860FA703_inline(__this, L_3, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void TouchScript.Pointers.Pointer::INTERNAL_Reset()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Pointer_INTERNAL_Reset_m851FB3F6C82DD723EB5A78235BC0B45865FE8897 (Pointer_tBF6873B314D4CC8534B6CD8A8BA54B92D8CFDE98 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Pointer_INTERNAL_Reset_m851FB3F6C82DD723EB5A78235BC0B45865FE8897_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		// Id = INVALID_POINTER;
		Pointer_set_Id_mEB0B14E2905D84B287EABA142EEAFF7737D63886_inline(__this, (-1), /*hidden argument*/NULL);
		// INTERNAL_ClearPressData();
		Pointer_INTERNAL_ClearPressData_mBF3AD7F3B5D26FC7D9D50A7753BFA63FB3BFFF39(__this, /*hidden argument*/NULL);
		// position = newPosition = PreviousPosition = Vector2.zero;
		IL2CPP_RUNTIME_CLASS_INIT(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_il2cpp_TypeInfo_var);
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_0 = Vector2_get_zero_mFE0C3213BB698130D6C5247AB4B887A59074D0A8(/*hidden argument*/NULL);
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_1 = L_0;
		V_0 = L_1;
		Pointer_set_PreviousPosition_m9825F629338D0DF6160DCB6DAADF3488860FA703_inline(__this, L_1, /*hidden argument*/NULL);
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_2 = V_0;
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_3 = L_2;
		V_0 = L_3;
		__this->set_newPosition_14(L_3);
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_4 = V_0;
		__this->set_position_13(L_4);
		// Flags = 0;
		Pointer_set_Flags_mF6A08BE8419E9E7DFD4CD15AF3A4E811734E9293_inline(__this, 0, /*hidden argument*/NULL);
		// Buttons = PointerButtonState.Nothing;
		Pointer_set_Buttons_mAE901843226D346730CAD2CAD512793E4E4BEAC8_inline(__this, 0, /*hidden argument*/NULL);
		// overDataIsDirty = true;
		__this->set_overDataIsDirty_17((bool)1);
		// }
		return;
	}
}
// System.Void TouchScript.Pointers.Pointer::INTERNAL_FrameStarted()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Pointer_INTERNAL_FrameStarted_mC677279C07B7D49D25BFC21580F50C74D037FAA0 (Pointer_tBF6873B314D4CC8534B6CD8A8BA54B92D8CFDE98 * __this, const RuntimeMethod* method)
{
	{
		// Buttons &= ~(PointerButtonState.AnyButtonDown | PointerButtonState.AnyButtonUp);
		int32_t L_0 = Pointer_get_Buttons_mDD144F267D37B07E930E44E9B7D9357246D5839A_inline(__this, /*hidden argument*/NULL);
		Pointer_set_Buttons_mAE901843226D346730CAD2CAD512793E4E4BEAC8_inline(__this, ((int32_t)((int32_t)L_0&(int32_t)((int32_t)-2095105))), /*hidden argument*/NULL);
		// overDataIsDirty = true;
		__this->set_overDataIsDirty_17((bool)1);
		// }
		return;
	}
}
// System.Void TouchScript.Pointers.Pointer::INTERNAL_UpdatePosition()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Pointer_INTERNAL_UpdatePosition_mB4AE54E8D1CFC6EFDBD36FAA6A3DFD04E98E0716 (Pointer_tBF6873B314D4CC8534B6CD8A8BA54B92D8CFDE98 * __this, const RuntimeMethod* method)
{
	{
		// PreviousPosition = position;
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_0 = __this->get_position_13();
		Pointer_set_PreviousPosition_m9825F629338D0DF6160DCB6DAADF3488860FA703_inline(__this, L_0, /*hidden argument*/NULL);
		// position = newPosition;
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_1 = __this->get_newPosition_14();
		__this->set_position_13(L_1);
		// }
		return;
	}
}
// System.Void TouchScript.Pointers.Pointer::INTERNAL_Retain()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Pointer_INTERNAL_Retain_m4B131382F3A12FF21FB907EEE0769FEA6704A0BF (Pointer_tBF6873B314D4CC8534B6CD8A8BA54B92D8CFDE98 * __this, const RuntimeMethod* method)
{
	{
		// refCount++;
		int32_t L_0 = __this->get_refCount_12();
		__this->set_refCount_12(((int32_t)il2cpp_codegen_add((int32_t)L_0, (int32_t)1)));
		// }
		return;
	}
}
// System.Int32 TouchScript.Pointers.Pointer::INTERNAL_Release()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t Pointer_INTERNAL_Release_m15DC4D63F05CB4001750D188BC00502BF017927B (Pointer_tBF6873B314D4CC8534B6CD8A8BA54B92D8CFDE98 * __this, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	{
		// return --refCount;
		int32_t L_0 = __this->get_refCount_12();
		V_0 = ((int32_t)il2cpp_codegen_subtract((int32_t)L_0, (int32_t)1));
		int32_t L_1 = V_0;
		__this->set_refCount_12(L_1);
		int32_t L_2 = V_0;
		return L_2;
	}
}
// System.Void TouchScript.Pointers.Pointer::INTERNAL_SetPressData(TouchScript.Hit.HitData)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Pointer_INTERNAL_SetPressData_m922C64806B13717F734D42422561C0FE31DA8B26 (Pointer_tBF6873B314D4CC8534B6CD8A8BA54B92D8CFDE98 * __this, HitData_t3022C0DF180FDE3CF86F713B78A2056D02E78980  ___data0, const RuntimeMethod* method)
{
	{
		// pressData = data;
		HitData_t3022C0DF180FDE3CF86F713B78A2056D02E78980  L_0 = ___data0;
		__this->set_pressData_15(L_0);
		// overData = data;
		HitData_t3022C0DF180FDE3CF86F713B78A2056D02E78980  L_1 = ___data0;
		__this->set_overData_16(L_1);
		// overDataIsDirty = false;
		__this->set_overDataIsDirty_17((bool)0);
		// }
		return;
	}
}
// System.Void TouchScript.Pointers.Pointer::INTERNAL_ClearPressData()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Pointer_INTERNAL_ClearPressData_mBF3AD7F3B5D26FC7D9D50A7753BFA63FB3BFFF39 (Pointer_tBF6873B314D4CC8534B6CD8A8BA54B92D8CFDE98 * __this, const RuntimeMethod* method)
{
	{
		// pressData = default(HitData);
		HitData_t3022C0DF180FDE3CF86F713B78A2056D02E78980 * L_0 = __this->get_address_of_pressData_15();
		il2cpp_codegen_initobj(L_0, sizeof(HitData_t3022C0DF180FDE3CF86F713B78A2056D02E78980 ));
		// refCount = 0;
		__this->set_refCount_12(0);
		// }
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// TouchScript.Pointers.Pointer TouchScript.Pointers.PointerFactory::Create(TouchScript.Pointers.Pointer_PointerType,TouchScript.InputSources.IInputSource)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Pointer_tBF6873B314D4CC8534B6CD8A8BA54B92D8CFDE98 * PointerFactory_Create_m7D6B0A6806D107FFCF0D3BA8B4C3EB31B4E6055F (int32_t ___type0, RuntimeObject* ___input1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PointerFactory_Create_m7D6B0A6806D107FFCF0D3BA8B4C3EB31B4E6055F_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// switch (type)
		int32_t L_0 = ___type0;
		switch (((int32_t)il2cpp_codegen_subtract((int32_t)L_0, (int32_t)1)))
		{
			case 0:
			{
				goto IL_001a;
			}
			case 1:
			{
				goto IL_0021;
			}
			case 2:
			{
				goto IL_0028;
			}
			case 3:
			{
				goto IL_002f;
			}
		}
	}
	{
		goto IL_0036;
	}

IL_001a:
	{
		// return new TouchPointer(input);
		RuntimeObject* L_1 = ___input1;
		TouchPointer_t35FC788E12AC007D8F90D6D28131910B61316942 * L_2 = (TouchPointer_t35FC788E12AC007D8F90D6D28131910B61316942 *)il2cpp_codegen_object_new(TouchPointer_t35FC788E12AC007D8F90D6D28131910B61316942_il2cpp_TypeInfo_var);
		TouchPointer__ctor_m45AB2CE7C1661672F231AFA9D718A1F877FBF276(L_2, L_1, /*hidden argument*/NULL);
		return L_2;
	}

IL_0021:
	{
		// return new MousePointer(input);
		RuntimeObject* L_3 = ___input1;
		MousePointer_tEFB01FDD2340977461DEB8153349F62DFDE0E100 * L_4 = (MousePointer_tEFB01FDD2340977461DEB8153349F62DFDE0E100 *)il2cpp_codegen_object_new(MousePointer_tEFB01FDD2340977461DEB8153349F62DFDE0E100_il2cpp_TypeInfo_var);
		MousePointer__ctor_m406338CE653654229558A5BCC38F97692A452158(L_4, L_3, /*hidden argument*/NULL);
		return L_4;
	}

IL_0028:
	{
		// return new PenPointer(input);
		RuntimeObject* L_5 = ___input1;
		PenPointer_tAF65E3C674E3160B823CFBB7D82046A3FFD60340 * L_6 = (PenPointer_tAF65E3C674E3160B823CFBB7D82046A3FFD60340 *)il2cpp_codegen_object_new(PenPointer_tAF65E3C674E3160B823CFBB7D82046A3FFD60340_il2cpp_TypeInfo_var);
		PenPointer__ctor_m6D8A7F282AAE1FA58ECC119BF00ECF3C534AE5E2(L_6, L_5, /*hidden argument*/NULL);
		return L_6;
	}

IL_002f:
	{
		// return new ObjectPointer(input);
		RuntimeObject* L_7 = ___input1;
		ObjectPointer_tD190FCA2463DDBBD72FBA988696DFC51EFF46C36 * L_8 = (ObjectPointer_tD190FCA2463DDBBD72FBA988696DFC51EFF46C36 *)il2cpp_codegen_object_new(ObjectPointer_tD190FCA2463DDBBD72FBA988696DFC51EFF46C36_il2cpp_TypeInfo_var);
		ObjectPointer__ctor_m24E6EC51EA50C4F5BF815D88CF8F84A552D98A41(L_8, L_7, /*hidden argument*/NULL);
		return L_8;
	}

IL_0036:
	{
		// return null;
		return (Pointer_tBF6873B314D4CC8534B6CD8A8BA54B92D8CFDE98 *)NULL;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Single TouchScript.Pointers.TouchPointer::get_Rotation()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float TouchPointer_get_Rotation_m3ECB502F6BA93D13E74646A81460AAACF27B75AF (TouchPointer_t35FC788E12AC007D8F90D6D28131910B61316942 * __this, const RuntimeMethod* method)
{
	{
		// public float Rotation { get; set; }
		float L_0 = __this->get_U3CRotationU3Ek__BackingField_20();
		return L_0;
	}
}
// System.Void TouchScript.Pointers.TouchPointer::set_Rotation(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TouchPointer_set_Rotation_m87442B9A19EB126F7E42949A8E0FB1FC2CB88FF9 (TouchPointer_t35FC788E12AC007D8F90D6D28131910B61316942 * __this, float ___value0, const RuntimeMethod* method)
{
	{
		// public float Rotation { get; set; }
		float L_0 = ___value0;
		__this->set_U3CRotationU3Ek__BackingField_20(L_0);
		return;
	}
}
// System.Single TouchScript.Pointers.TouchPointer::get_Pressure()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float TouchPointer_get_Pressure_m894BA5476887A5547C328C11BE33BB03E2267462 (TouchPointer_t35FC788E12AC007D8F90D6D28131910B61316942 * __this, const RuntimeMethod* method)
{
	{
		// public float Pressure { get; set; }
		float L_0 = __this->get_U3CPressureU3Ek__BackingField_21();
		return L_0;
	}
}
// System.Void TouchScript.Pointers.TouchPointer::set_Pressure(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TouchPointer_set_Pressure_m9DAD082334050E94FCD81D8887D56CAA8D548BD0 (TouchPointer_t35FC788E12AC007D8F90D6D28131910B61316942 * __this, float ___value0, const RuntimeMethod* method)
{
	{
		// public float Pressure { get; set; }
		float L_0 = ___value0;
		__this->set_U3CPressureU3Ek__BackingField_21(L_0);
		return;
	}
}
// System.Void TouchScript.Pointers.TouchPointer::.ctor(TouchScript.InputSources.IInputSource)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TouchPointer__ctor_m45AB2CE7C1661672F231AFA9D718A1F877FBF276 (TouchPointer_t35FC788E12AC007D8F90D6D28131910B61316942 * __this, RuntimeObject* ___input0, const RuntimeMethod* method)
{
	{
		// public TouchPointer(IInputSource input) : base(input)
		RuntimeObject* L_0 = ___input0;
		Pointer__ctor_mE7828F1485349BF2A610843ABB9A7944396F2D6C(__this, L_0, /*hidden argument*/NULL);
		// Type = PointerType.Touch;
		Pointer_set_Type_mC0D513634112590ABB8EB877E8BA70416BDF2625_inline(__this, 1, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void TouchScript.Pointers.TouchPointer::INTERNAL_Reset()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TouchPointer_INTERNAL_Reset_mAA8BF6032219E321A1CC86AEF61EF74A4ADAF0A2 (TouchPointer_t35FC788E12AC007D8F90D6D28131910B61316942 * __this, const RuntimeMethod* method)
{
	{
		// base.INTERNAL_Reset();
		Pointer_INTERNAL_Reset_m851FB3F6C82DD723EB5A78235BC0B45865FE8897(__this, /*hidden argument*/NULL);
		// Rotation = DEFAULT_ROTATION;
		TouchPointer_set_Rotation_m87442B9A19EB126F7E42949A8E0FB1FC2CB88FF9_inline(__this, (0.0f), /*hidden argument*/NULL);
		// Pressure = DEFAULT_PRESSURE;
		TouchPointer_set_Pressure_m9DAD082334050E94FCD81D8887D56CAA8D548BD0_inline(__this, (0.5f), /*hidden argument*/NULL);
		// }
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// TouchScript.ITouchManager TouchScript.TouchManager::get_Instance()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* TouchManager_get_Instance_m355FC1AAF7F91B86F174A4429E8A44ABE454E8E3 (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TouchManager_get_Instance_m355FC1AAF7F91B86F174A4429E8A44ABE454E8E3_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// get { return TouchManagerInstance.Instance; }
		IL2CPP_RUNTIME_CLASS_INIT(TouchManagerInstance_t2FC1653461034CC60588BF499439FF10FCC702CB_il2cpp_TypeInfo_var);
		TouchManagerInstance_t2FC1653461034CC60588BF499439FF10FCC702CB * L_0 = TouchManagerInstance_get_Instance_m78AE6F31FA1A28D96EEAA748AD95F344E1E219EA(/*hidden argument*/NULL);
		return L_0;
	}
}
// TouchScript.Devices.Display.IDisplayDevice TouchScript.TouchManager::get_DisplayDevice()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* TouchManager_get_DisplayDevice_m342110501378ACD88AFE29BCE3D2F1DB58D41D00 (TouchManager_tE3A4C5ABD05B3B6C984CDBB1FA80479E1ACBE92B * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TouchManager_get_DisplayDevice_m342110501378ACD88AFE29BCE3D2F1DB58D41D00_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// if (Instance == null) return displayDevice as IDisplayDevice;
		IL2CPP_RUNTIME_CLASS_INIT(TouchManager_tE3A4C5ABD05B3B6C984CDBB1FA80479E1ACBE92B_il2cpp_TypeInfo_var);
		RuntimeObject* L_0 = TouchManager_get_Instance_m355FC1AAF7F91B86F174A4429E8A44ABE454E8E3(/*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_0013;
		}
	}
	{
		// if (Instance == null) return displayDevice as IDisplayDevice;
		Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 * L_1 = __this->get_displayDevice_18();
		return ((RuntimeObject*)IsInst((RuntimeObject*)L_1, IDisplayDevice_t578977AAA9E7B4E744CECA232744DCB614E534F6_il2cpp_TypeInfo_var));
	}

IL_0013:
	{
		// return Instance.DisplayDevice;
		IL2CPP_RUNTIME_CLASS_INIT(TouchManager_tE3A4C5ABD05B3B6C984CDBB1FA80479E1ACBE92B_il2cpp_TypeInfo_var);
		RuntimeObject* L_2 = TouchManager_get_Instance_m355FC1AAF7F91B86F174A4429E8A44ABE454E8E3(/*hidden argument*/NULL);
		NullCheck(L_2);
		RuntimeObject* L_3 = InterfaceFuncInvoker0< RuntimeObject* >::Invoke(16 /* TouchScript.Devices.Display.IDisplayDevice TouchScript.ITouchManager::get_DisplayDevice() */, ITouchManager_t1E2C9BFCD55643DC5C5D4A005A64EA0BEEC2E843_il2cpp_TypeInfo_var, L_2);
		return L_3;
	}
}
// System.Void TouchScript.TouchManager::set_DisplayDevice(TouchScript.Devices.Display.IDisplayDevice)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TouchManager_set_DisplayDevice_m473676E71E3A26D1B447AC97A228687CA1FE9C71 (TouchManager_tE3A4C5ABD05B3B6C984CDBB1FA80479E1ACBE92B * __this, RuntimeObject* ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TouchManager_set_DisplayDevice_m473676E71E3A26D1B447AC97A228687CA1FE9C71_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// if (Instance == null)
		IL2CPP_RUNTIME_CLASS_INIT(TouchManager_tE3A4C5ABD05B3B6C984CDBB1FA80479E1ACBE92B_il2cpp_TypeInfo_var);
		RuntimeObject* L_0 = TouchManager_get_Instance_m355FC1AAF7F91B86F174A4429E8A44ABE454E8E3(/*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_0014;
		}
	}
	{
		// displayDevice = value as Object;
		RuntimeObject* L_1 = ___value0;
		__this->set_displayDevice_18(((Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 *)IsInstClass((RuntimeObject*)L_1, Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_il2cpp_TypeInfo_var)));
		// return;
		return;
	}

IL_0014:
	{
		// Instance.DisplayDevice = value;
		IL2CPP_RUNTIME_CLASS_INIT(TouchManager_tE3A4C5ABD05B3B6C984CDBB1FA80479E1ACBE92B_il2cpp_TypeInfo_var);
		RuntimeObject* L_2 = TouchManager_get_Instance_m355FC1AAF7F91B86F174A4429E8A44ABE454E8E3(/*hidden argument*/NULL);
		RuntimeObject* L_3 = ___value0;
		NullCheck(L_2);
		InterfaceActionInvoker1< RuntimeObject* >::Invoke(17 /* System.Void TouchScript.ITouchManager::set_DisplayDevice(TouchScript.Devices.Display.IDisplayDevice) */, ITouchManager_t1E2C9BFCD55643DC5C5D4A005A64EA0BEEC2E843_il2cpp_TypeInfo_var, L_2, L_3);
		// }
		return;
	}
}
// System.Boolean TouchScript.TouchManager::get_ShouldCreateCameraLayer()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool TouchManager_get_ShouldCreateCameraLayer_m9555EEF395EA3D416B86CD071695F48749CEA641 (TouchManager_tE3A4C5ABD05B3B6C984CDBB1FA80479E1ACBE92B * __this, const RuntimeMethod* method)
{
	{
		// get { return shouldCreateCameraLayer; }
		bool L_0 = __this->get_shouldCreateCameraLayer_19();
		return L_0;
	}
}
// System.Void TouchScript.TouchManager::set_ShouldCreateCameraLayer(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TouchManager_set_ShouldCreateCameraLayer_m321C5C1830A0EF4AE5FEB0FB5A33A5E32B62D938 (TouchManager_tE3A4C5ABD05B3B6C984CDBB1FA80479E1ACBE92B * __this, bool ___value0, const RuntimeMethod* method)
{
	{
		// set { shouldCreateCameraLayer = value; }
		bool L_0 = ___value0;
		__this->set_shouldCreateCameraLayer_19(L_0);
		// set { shouldCreateCameraLayer = value; }
		return;
	}
}
// System.Boolean TouchScript.TouchManager::get_ShouldCreateStandardInput()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool TouchManager_get_ShouldCreateStandardInput_mE33919A0FE9C7D46BA910362D807179381DFAAE1 (TouchManager_tE3A4C5ABD05B3B6C984CDBB1FA80479E1ACBE92B * __this, const RuntimeMethod* method)
{
	{
		// get { return shouldCreateStandardInput; }
		bool L_0 = __this->get_shouldCreateStandardInput_20();
		return L_0;
	}
}
// System.Void TouchScript.TouchManager::set_ShouldCreateStandardInput(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TouchManager_set_ShouldCreateStandardInput_m06BB13C8B96C2A1040F4EB85BAEC51BDF4FAFEDA (TouchManager_tE3A4C5ABD05B3B6C984CDBB1FA80479E1ACBE92B * __this, bool ___value0, const RuntimeMethod* method)
{
	{
		// set { shouldCreateStandardInput = value; }
		bool L_0 = ___value0;
		__this->set_shouldCreateStandardInput_20(L_0);
		// set { shouldCreateStandardInput = value; }
		return;
	}
}
// System.Boolean TouchScript.TouchManager::get_UseSendMessage()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool TouchManager_get_UseSendMessage_m6174E37C90F89BBF01BDD07A57AA79D8153E85A9 (TouchManager_tE3A4C5ABD05B3B6C984CDBB1FA80479E1ACBE92B * __this, const RuntimeMethod* method)
{
	{
		// get { return useSendMessage; }
		bool L_0 = __this->get_useSendMessage_21();
		return L_0;
	}
}
// System.Void TouchScript.TouchManager::set_UseSendMessage(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TouchManager_set_UseSendMessage_m7885229E5BA8439B403C3BFFEEF0E1C7315D2009 (TouchManager_tE3A4C5ABD05B3B6C984CDBB1FA80479E1ACBE92B * __this, bool ___value0, const RuntimeMethod* method)
{
	{
		// if (value == useSendMessage) return;
		bool L_0 = ___value0;
		bool L_1 = __this->get_useSendMessage_21();
		if ((!(((uint32_t)L_0) == ((uint32_t)L_1))))
		{
			goto IL_000a;
		}
	}
	{
		// if (value == useSendMessage) return;
		return;
	}

IL_000a:
	{
		// useSendMessage = value;
		bool L_2 = ___value0;
		__this->set_useSendMessage_21(L_2);
		// updateSendMessageSubscription();
		TouchManager_updateSendMessageSubscription_mAA916B61823E409F702F20238AFA50536FBABADA(__this, /*hidden argument*/NULL);
		// }
		return;
	}
}
// TouchScript.TouchManager_MessageType TouchScript.TouchManager::get_SendMessageEvents()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t TouchManager_get_SendMessageEvents_m807C4A591F1C10FB759D7470C7D1F54484C478E0 (TouchManager_tE3A4C5ABD05B3B6C984CDBB1FA80479E1ACBE92B * __this, const RuntimeMethod* method)
{
	{
		// get { return sendMessageEvents; }
		int32_t L_0 = __this->get_sendMessageEvents_22();
		return L_0;
	}
}
// System.Void TouchScript.TouchManager::set_SendMessageEvents(TouchScript.TouchManager_MessageType)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TouchManager_set_SendMessageEvents_m83F740F8FC4AE4DC5FC8BC5349C2964F8E3325FA (TouchManager_tE3A4C5ABD05B3B6C984CDBB1FA80479E1ACBE92B * __this, int32_t ___value0, const RuntimeMethod* method)
{
	{
		// if (sendMessageEvents == value) return;
		int32_t L_0 = __this->get_sendMessageEvents_22();
		int32_t L_1 = ___value0;
		if ((!(((uint32_t)L_0) == ((uint32_t)L_1))))
		{
			goto IL_000a;
		}
	}
	{
		// if (sendMessageEvents == value) return;
		return;
	}

IL_000a:
	{
		// sendMessageEvents = value;
		int32_t L_2 = ___value0;
		__this->set_sendMessageEvents_22(L_2);
		// updateSendMessageSubscription();
		TouchManager_updateSendMessageSubscription_mAA916B61823E409F702F20238AFA50536FBABADA(__this, /*hidden argument*/NULL);
		// }
		return;
	}
}
// UnityEngine.GameObject TouchScript.TouchManager::get_SendMessageTarget()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * TouchManager_get_SendMessageTarget_m0410C0052F476BB4C6B06ACAF535FFCBDFD21863 (TouchManager_tE3A4C5ABD05B3B6C984CDBB1FA80479E1ACBE92B * __this, const RuntimeMethod* method)
{
	{
		// get { return sendMessageTarget; }
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_0 = __this->get_sendMessageTarget_23();
		return L_0;
	}
}
// System.Void TouchScript.TouchManager::set_SendMessageTarget(UnityEngine.GameObject)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TouchManager_set_SendMessageTarget_m887C577DCA1779ABB748FF27E07892D9FDBAA1BA (TouchManager_tE3A4C5ABD05B3B6C984CDBB1FA80479E1ACBE92B * __this, GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TouchManager_set_SendMessageTarget_m887C577DCA1779ABB748FF27E07892D9FDBAA1BA_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// sendMessageTarget = value;
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_0 = ___value0;
		__this->set_sendMessageTarget_23(L_0);
		// if (value == null) sendMessageTarget = gameObject;
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_1 = ___value0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_il2cpp_TypeInfo_var);
		bool L_2 = Object_op_Equality_mBC2401774F3BE33E8CF6F0A8148E66C95D6CFF1C(L_1, (Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 *)NULL, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_001c;
		}
	}
	{
		// if (value == null) sendMessageTarget = gameObject;
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_3 = Component_get_gameObject_m0B0570BA8DDD3CD78A9DB568EA18D7317686603C(__this, /*hidden argument*/NULL);
		__this->set_sendMessageTarget_23(L_3);
	}

IL_001c:
	{
		// }
		return;
	}
}
// System.Boolean TouchScript.TouchManager::get_UseUnityEvents()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool TouchManager_get_UseUnityEvents_m7AD3163F983E7429A268638E986C75EEC6620ECE (TouchManager_tE3A4C5ABD05B3B6C984CDBB1FA80479E1ACBE92B * __this, const RuntimeMethod* method)
{
	{
		// get { return useUnityEvents; }
		bool L_0 = __this->get_useUnityEvents_24();
		return L_0;
	}
}
// System.Void TouchScript.TouchManager::set_UseUnityEvents(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TouchManager_set_UseUnityEvents_m330D1F5971F4A6D8C5BEE0D2F36A6F721CEC56F7 (TouchManager_tE3A4C5ABD05B3B6C984CDBB1FA80479E1ACBE92B * __this, bool ___value0, const RuntimeMethod* method)
{
	{
		// if (useUnityEvents == value) return;
		bool L_0 = __this->get_useUnityEvents_24();
		bool L_1 = ___value0;
		if ((!(((uint32_t)L_0) == ((uint32_t)L_1))))
		{
			goto IL_000a;
		}
	}
	{
		// if (useUnityEvents == value) return;
		return;
	}

IL_000a:
	{
		// useUnityEvents = value;
		bool L_2 = ___value0;
		__this->set_useUnityEvents_24(L_2);
		// updateUnityEventsSubscription();
		TouchManager_updateUnityEventsSubscription_m6A5CA7A27D10F83B2A5CA578C3446A140E2FF3B4(__this, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Boolean TouchScript.TouchManager::IsInvalidPosition(UnityEngine.Vector2)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool TouchManager_IsInvalidPosition_m80842AEBF1770FB52A67BC73957A1B1956D2716F (Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___position0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TouchManager_IsInvalidPosition_m80842AEBF1770FB52A67BC73957A1B1956D2716F_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// return position.x == INVALID_POSITION.x && position.y == INVALID_POSITION.y;
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_0 = ___position0;
		float L_1 = L_0.get_x_0();
		IL2CPP_RUNTIME_CLASS_INIT(TouchManager_tE3A4C5ABD05B3B6C984CDBB1FA80479E1ACBE92B_il2cpp_TypeInfo_var);
		float L_2 = (((TouchManager_tE3A4C5ABD05B3B6C984CDBB1FA80479E1ACBE92B_StaticFields*)il2cpp_codegen_static_fields_for(TouchManager_tE3A4C5ABD05B3B6C984CDBB1FA80479E1ACBE92B_il2cpp_TypeInfo_var))->get_address_of_INVALID_POSITION_6())->get_x_0();
		if ((!(((float)L_1) == ((float)L_2))))
		{
			goto IL_0025;
		}
	}
	{
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_3 = ___position0;
		float L_4 = L_3.get_y_1();
		IL2CPP_RUNTIME_CLASS_INIT(TouchManager_tE3A4C5ABD05B3B6C984CDBB1FA80479E1ACBE92B_il2cpp_TypeInfo_var);
		float L_5 = (((TouchManager_tE3A4C5ABD05B3B6C984CDBB1FA80479E1ACBE92B_StaticFields*)il2cpp_codegen_static_fields_for(TouchManager_tE3A4C5ABD05B3B6C984CDBB1FA80479E1ACBE92B_il2cpp_TypeInfo_var))->get_address_of_INVALID_POSITION_6())->get_y_1();
		return (bool)((((float)L_4) == ((float)L_5))? 1 : 0);
	}

IL_0025:
	{
		return (bool)0;
	}
}
// System.Void TouchScript.TouchManager::Awake()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TouchManager_Awake_m9417FD558FC15583C25CEB2135A9A8F9053A0773 (TouchManager_tE3A4C5ABD05B3B6C984CDBB1FA80479E1ACBE92B * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TouchManager_Awake_m9417FD558FC15583C25CEB2135A9A8F9053A0773_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	TouchLayer_t484C3424D422E6073B3FE4DD21EFE05686F5AC54 * V_1 = NULL;
	{
		// if (Instance == null) return;
		IL2CPP_RUNTIME_CLASS_INIT(TouchManager_tE3A4C5ABD05B3B6C984CDBB1FA80479E1ACBE92B_il2cpp_TypeInfo_var);
		RuntimeObject* L_0 = TouchManager_get_Instance_m355FC1AAF7F91B86F174A4429E8A44ABE454E8E3(/*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_0008;
		}
	}
	{
		// if (Instance == null) return;
		return;
	}

IL_0008:
	{
		// Instance.DisplayDevice = displayDevice as IDisplayDevice;
		IL2CPP_RUNTIME_CLASS_INIT(TouchManager_tE3A4C5ABD05B3B6C984CDBB1FA80479E1ACBE92B_il2cpp_TypeInfo_var);
		RuntimeObject* L_1 = TouchManager_get_Instance_m355FC1AAF7F91B86F174A4429E8A44ABE454E8E3(/*hidden argument*/NULL);
		Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 * L_2 = __this->get_displayDevice_18();
		NullCheck(L_1);
		InterfaceActionInvoker1< RuntimeObject* >::Invoke(17 /* System.Void TouchScript.ITouchManager::set_DisplayDevice(TouchScript.Devices.Display.IDisplayDevice) */, ITouchManager_t1E2C9BFCD55643DC5C5D4A005A64EA0BEEC2E843_il2cpp_TypeInfo_var, L_1, ((RuntimeObject*)IsInst((RuntimeObject*)L_2, IDisplayDevice_t578977AAA9E7B4E744CECA232744DCB614E534F6_il2cpp_TypeInfo_var)));
		// Instance.ShouldCreateCameraLayer = ShouldCreateCameraLayer;
		RuntimeObject* L_3 = TouchManager_get_Instance_m355FC1AAF7F91B86F174A4429E8A44ABE454E8E3(/*hidden argument*/NULL);
		bool L_4 = TouchManager_get_ShouldCreateCameraLayer_m9555EEF395EA3D416B86CD071695F48749CEA641_inline(__this, /*hidden argument*/NULL);
		NullCheck(L_3);
		InterfaceActionInvoker1< bool >::Invoke(20 /* System.Void TouchScript.ITouchManager::set_ShouldCreateCameraLayer(System.Boolean) */, ITouchManager_t1E2C9BFCD55643DC5C5D4A005A64EA0BEEC2E843_il2cpp_TypeInfo_var, L_3, L_4);
		// Instance.ShouldCreateStandardInput = ShouldCreateStandardInput;
		RuntimeObject* L_5 = TouchManager_get_Instance_m355FC1AAF7F91B86F174A4429E8A44ABE454E8E3(/*hidden argument*/NULL);
		bool L_6 = TouchManager_get_ShouldCreateStandardInput_mE33919A0FE9C7D46BA910362D807179381DFAAE1_inline(__this, /*hidden argument*/NULL);
		NullCheck(L_5);
		InterfaceActionInvoker1< bool >::Invoke(22 /* System.Void TouchScript.ITouchManager::set_ShouldCreateStandardInput(System.Boolean) */, ITouchManager_t1E2C9BFCD55643DC5C5D4A005A64EA0BEEC2E843_il2cpp_TypeInfo_var, L_5, L_6);
		// for (var i = 0; i < layers.Count; i++)
		V_0 = 0;
		goto IL_0069;
	}

IL_0041:
	{
		// var layer = layers[i];
		List_1_tACFDA2C40746A89629E9D8D22CD4726B64D7C856 * L_7 = __this->get_layers_25();
		int32_t L_8 = V_0;
		NullCheck(L_7);
		TouchLayer_t484C3424D422E6073B3FE4DD21EFE05686F5AC54 * L_9 = List_1_get_Item_m9CC34A7BCCF27B11010DF6AD1031D7B256727CCB_inline(L_7, L_8, /*hidden argument*/List_1_get_Item_m9CC34A7BCCF27B11010DF6AD1031D7B256727CCB_RuntimeMethod_var);
		V_1 = L_9;
		// if (layer != null) LayerManager.Instance.AddLayer(layer, i);
		TouchLayer_t484C3424D422E6073B3FE4DD21EFE05686F5AC54 * L_10 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_il2cpp_TypeInfo_var);
		bool L_11 = Object_op_Inequality_m31EF58E217E8F4BDD3E409DEF79E1AEE95874FC1(L_10, (Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 *)NULL, /*hidden argument*/NULL);
		if (!L_11)
		{
			goto IL_0065;
		}
	}
	{
		// if (layer != null) LayerManager.Instance.AddLayer(layer, i);
		RuntimeObject* L_12 = LayerManager_get_Instance_mA50DB5F8ACCAF0DCDE4BEEA78843142A752CF9F7(/*hidden argument*/NULL);
		TouchLayer_t484C3424D422E6073B3FE4DD21EFE05686F5AC54 * L_13 = V_1;
		int32_t L_14 = V_0;
		NullCheck(L_12);
		InterfaceFuncInvoker3< bool, TouchLayer_t484C3424D422E6073B3FE4DD21EFE05686F5AC54 *, int32_t, bool >::Invoke(3 /* System.Boolean TouchScript.ILayerManager::AddLayer(TouchScript.Layers.TouchLayer,System.Int32,System.Boolean) */, ILayerManager_tE1F41773696005EFB2A5D97608D9598795083E74_il2cpp_TypeInfo_var, L_12, L_13, L_14, (bool)1);
	}

IL_0065:
	{
		// for (var i = 0; i < layers.Count; i++)
		int32_t L_15 = V_0;
		V_0 = ((int32_t)il2cpp_codegen_add((int32_t)L_15, (int32_t)1));
	}

IL_0069:
	{
		// for (var i = 0; i < layers.Count; i++)
		int32_t L_16 = V_0;
		List_1_tACFDA2C40746A89629E9D8D22CD4726B64D7C856 * L_17 = __this->get_layers_25();
		NullCheck(L_17);
		int32_t L_18 = List_1_get_Count_mC85447D3C8462EC8C81F17B1EE9584D1DB2E804C_inline(L_17, /*hidden argument*/List_1_get_Count_mC85447D3C8462EC8C81F17B1EE9584D1DB2E804C_RuntimeMethod_var);
		if ((((int32_t)L_16) < ((int32_t)L_18)))
		{
			goto IL_0041;
		}
	}
	{
		// }
		return;
	}
}
// System.Void TouchScript.TouchManager::OnEnable()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TouchManager_OnEnable_m9C0ECA70F64AACDA06C0B83E848F5F3030AC3549 (TouchManager_tE3A4C5ABD05B3B6C984CDBB1FA80479E1ACBE92B * __this, const RuntimeMethod* method)
{
	{
		// updateSendMessageSubscription();
		TouchManager_updateSendMessageSubscription_mAA916B61823E409F702F20238AFA50536FBABADA(__this, /*hidden argument*/NULL);
		// updateUnityEventsSubscription();
		TouchManager_updateUnityEventsSubscription_m6A5CA7A27D10F83B2A5CA578C3446A140E2FF3B4(__this, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void TouchScript.TouchManager::OnDisable()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TouchManager_OnDisable_m9039E96AFD2AFA352E2EE0D14007F3779E130C8D (TouchManager_tE3A4C5ABD05B3B6C984CDBB1FA80479E1ACBE92B * __this, const RuntimeMethod* method)
{
	{
		// removeSendMessageSubscriptions();
		TouchManager_removeSendMessageSubscriptions_mDA19EA010E735C99BF9CEC546274DAC4694CF57C(__this, /*hidden argument*/NULL);
		// removeUnityEventsSubscriptions();
		TouchManager_removeUnityEventsSubscriptions_m16B73F9CF5C8D19E6FB2776C07439A1F6A93F3FA(__this, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void TouchScript.TouchManager::switchToBasicEditor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TouchManager_switchToBasicEditor_m3A1C84E7DDBA54ED96FAC435729BC58C5F3B636E (TouchManager_tE3A4C5ABD05B3B6C984CDBB1FA80479E1ACBE92B * __this, const RuntimeMethod* method)
{
	{
		// basicEditor = true;
		__this->set_basicEditor_17((bool)1);
		// }
		return;
	}
}
// System.Void TouchScript.TouchManager::updateSendMessageSubscription()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TouchManager_updateSendMessageSubscription_mAA916B61823E409F702F20238AFA50536FBABADA (TouchManager_tE3A4C5ABD05B3B6C984CDBB1FA80479E1ACBE92B * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TouchManager_updateSendMessageSubscription_mAA916B61823E409F702F20238AFA50536FBABADA_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// if (!Application.isPlaying) return;
		bool L_0 = Application_get_isPlaying_mF43B519662E7433DD90D883E5AE22EC3CFB65CA5(/*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_0008;
		}
	}
	{
		// if (!Application.isPlaying) return;
		return;
	}

IL_0008:
	{
		// if (Instance == null) return;
		IL2CPP_RUNTIME_CLASS_INIT(TouchManager_tE3A4C5ABD05B3B6C984CDBB1FA80479E1ACBE92B_il2cpp_TypeInfo_var);
		RuntimeObject* L_1 = TouchManager_get_Instance_m355FC1AAF7F91B86F174A4429E8A44ABE454E8E3(/*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_0010;
		}
	}
	{
		// if (Instance == null) return;
		return;
	}

IL_0010:
	{
		// if (sendMessageTarget == null) sendMessageTarget = gameObject;
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_2 = __this->get_sendMessageTarget_23();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_il2cpp_TypeInfo_var);
		bool L_3 = Object_op_Equality_mBC2401774F3BE33E8CF6F0A8148E66C95D6CFF1C(L_2, (Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 *)NULL, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_002a;
		}
	}
	{
		// if (sendMessageTarget == null) sendMessageTarget = gameObject;
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_4 = Component_get_gameObject_m0B0570BA8DDD3CD78A9DB568EA18D7317686603C(__this, /*hidden argument*/NULL);
		__this->set_sendMessageTarget_23(L_4);
	}

IL_002a:
	{
		// removeSendMessageSubscriptions();
		TouchManager_removeSendMessageSubscriptions_mDA19EA010E735C99BF9CEC546274DAC4694CF57C(__this, /*hidden argument*/NULL);
		// if (!useSendMessage) return;
		bool L_5 = __this->get_useSendMessage_21();
		if (L_5)
		{
			goto IL_0039;
		}
	}
	{
		// if (!useSendMessage) return;
		return;
	}

IL_0039:
	{
		// if ((SendMessageEvents & MessageType.FrameStarted) != 0) Instance.FrameStarted += frameStartedSendMessageHandler;
		int32_t L_6 = TouchManager_get_SendMessageEvents_m807C4A591F1C10FB759D7470C7D1F54484C478E0_inline(__this, /*hidden argument*/NULL);
		if (!((int32_t)((int32_t)L_6&(int32_t)1)))
		{
			goto IL_0059;
		}
	}
	{
		// if ((SendMessageEvents & MessageType.FrameStarted) != 0) Instance.FrameStarted += frameStartedSendMessageHandler;
		IL2CPP_RUNTIME_CLASS_INIT(TouchManager_tE3A4C5ABD05B3B6C984CDBB1FA80479E1ACBE92B_il2cpp_TypeInfo_var);
		RuntimeObject* L_7 = TouchManager_get_Instance_m355FC1AAF7F91B86F174A4429E8A44ABE454E8E3(/*hidden argument*/NULL);
		EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C * L_8 = (EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C *)il2cpp_codegen_object_new(EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C_il2cpp_TypeInfo_var);
		EventHandler__ctor_m497A2BCD46DB769EAFFDE919303FCAE226906B6F(L_8, __this, (intptr_t)((intptr_t)TouchManager_frameStartedSendMessageHandler_mAE3F3B2767D5FA1521A68F0275B31A7A9B3B2E04_RuntimeMethod_var), /*hidden argument*/NULL);
		NullCheck(L_7);
		InterfaceActionInvoker1< EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C * >::Invoke(0 /* System.Void TouchScript.ITouchManager::add_FrameStarted(System.EventHandler) */, ITouchManager_t1E2C9BFCD55643DC5C5D4A005A64EA0BEEC2E843_il2cpp_TypeInfo_var, L_7, L_8);
	}

IL_0059:
	{
		// if ((SendMessageEvents & MessageType.FrameFinished) != 0) Instance.FrameFinished += frameFinishedSendMessageHandler;
		int32_t L_9 = TouchManager_get_SendMessageEvents_m807C4A591F1C10FB759D7470C7D1F54484C478E0_inline(__this, /*hidden argument*/NULL);
		if (!((int32_t)((int32_t)L_9&(int32_t)2)))
		{
			goto IL_0079;
		}
	}
	{
		// if ((SendMessageEvents & MessageType.FrameFinished) != 0) Instance.FrameFinished += frameFinishedSendMessageHandler;
		IL2CPP_RUNTIME_CLASS_INIT(TouchManager_tE3A4C5ABD05B3B6C984CDBB1FA80479E1ACBE92B_il2cpp_TypeInfo_var);
		RuntimeObject* L_10 = TouchManager_get_Instance_m355FC1AAF7F91B86F174A4429E8A44ABE454E8E3(/*hidden argument*/NULL);
		EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C * L_11 = (EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C *)il2cpp_codegen_object_new(EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C_il2cpp_TypeInfo_var);
		EventHandler__ctor_m497A2BCD46DB769EAFFDE919303FCAE226906B6F(L_11, __this, (intptr_t)((intptr_t)TouchManager_frameFinishedSendMessageHandler_m06643F8EE650407C385EF6648920F3A211E0D5E6_RuntimeMethod_var), /*hidden argument*/NULL);
		NullCheck(L_10);
		InterfaceActionInvoker1< EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C * >::Invoke(2 /* System.Void TouchScript.ITouchManager::add_FrameFinished(System.EventHandler) */, ITouchManager_t1E2C9BFCD55643DC5C5D4A005A64EA0BEEC2E843_il2cpp_TypeInfo_var, L_10, L_11);
	}

IL_0079:
	{
		// if ((SendMessageEvents & MessageType.PointersAdded) != 0) Instance.PointersAdded += pointersAddedSendMessageHandler;
		int32_t L_12 = TouchManager_get_SendMessageEvents_m807C4A591F1C10FB759D7470C7D1F54484C478E0_inline(__this, /*hidden argument*/NULL);
		if (!((int32_t)((int32_t)L_12&(int32_t)4)))
		{
			goto IL_0099;
		}
	}
	{
		// if ((SendMessageEvents & MessageType.PointersAdded) != 0) Instance.PointersAdded += pointersAddedSendMessageHandler;
		IL2CPP_RUNTIME_CLASS_INIT(TouchManager_tE3A4C5ABD05B3B6C984CDBB1FA80479E1ACBE92B_il2cpp_TypeInfo_var);
		RuntimeObject* L_13 = TouchManager_get_Instance_m355FC1AAF7F91B86F174A4429E8A44ABE454E8E3(/*hidden argument*/NULL);
		EventHandler_1_t0363FEC12482AFCCEA69CEA150D1710057B94330 * L_14 = (EventHandler_1_t0363FEC12482AFCCEA69CEA150D1710057B94330 *)il2cpp_codegen_object_new(EventHandler_1_t0363FEC12482AFCCEA69CEA150D1710057B94330_il2cpp_TypeInfo_var);
		EventHandler_1__ctor_mDD490FECE01D6EF94516D572915E5E6291E1B7D1(L_14, __this, (intptr_t)((intptr_t)TouchManager_pointersAddedSendMessageHandler_m63C06829F976D339666BFF3FAB8530E101355FDD_RuntimeMethod_var), /*hidden argument*/EventHandler_1__ctor_mDD490FECE01D6EF94516D572915E5E6291E1B7D1_RuntimeMethod_var);
		NullCheck(L_13);
		InterfaceActionInvoker1< EventHandler_1_t0363FEC12482AFCCEA69CEA150D1710057B94330 * >::Invoke(4 /* System.Void TouchScript.ITouchManager::add_PointersAdded(System.EventHandler`1<TouchScript.PointerEventArgs>) */, ITouchManager_t1E2C9BFCD55643DC5C5D4A005A64EA0BEEC2E843_il2cpp_TypeInfo_var, L_13, L_14);
	}

IL_0099:
	{
		// if ((SendMessageEvents & MessageType.PointersUpdated) != 0) Instance.PointersUpdated += pointersUpdatedSendMessageHandler;
		int32_t L_15 = TouchManager_get_SendMessageEvents_m807C4A591F1C10FB759D7470C7D1F54484C478E0_inline(__this, /*hidden argument*/NULL);
		if (!((int32_t)((int32_t)L_15&(int32_t)8)))
		{
			goto IL_00b9;
		}
	}
	{
		// if ((SendMessageEvents & MessageType.PointersUpdated) != 0) Instance.PointersUpdated += pointersUpdatedSendMessageHandler;
		IL2CPP_RUNTIME_CLASS_INIT(TouchManager_tE3A4C5ABD05B3B6C984CDBB1FA80479E1ACBE92B_il2cpp_TypeInfo_var);
		RuntimeObject* L_16 = TouchManager_get_Instance_m355FC1AAF7F91B86F174A4429E8A44ABE454E8E3(/*hidden argument*/NULL);
		EventHandler_1_t0363FEC12482AFCCEA69CEA150D1710057B94330 * L_17 = (EventHandler_1_t0363FEC12482AFCCEA69CEA150D1710057B94330 *)il2cpp_codegen_object_new(EventHandler_1_t0363FEC12482AFCCEA69CEA150D1710057B94330_il2cpp_TypeInfo_var);
		EventHandler_1__ctor_mDD490FECE01D6EF94516D572915E5E6291E1B7D1(L_17, __this, (intptr_t)((intptr_t)TouchManager_pointersUpdatedSendMessageHandler_m58381274AF569B5230025CFB6ABCDE92318C4606_RuntimeMethod_var), /*hidden argument*/EventHandler_1__ctor_mDD490FECE01D6EF94516D572915E5E6291E1B7D1_RuntimeMethod_var);
		NullCheck(L_16);
		InterfaceActionInvoker1< EventHandler_1_t0363FEC12482AFCCEA69CEA150D1710057B94330 * >::Invoke(6 /* System.Void TouchScript.ITouchManager::add_PointersUpdated(System.EventHandler`1<TouchScript.PointerEventArgs>) */, ITouchManager_t1E2C9BFCD55643DC5C5D4A005A64EA0BEEC2E843_il2cpp_TypeInfo_var, L_16, L_17);
	}

IL_00b9:
	{
		// if ((SendMessageEvents & MessageType.PointersPressed) != 0) Instance.PointersPressed += pointersPressedSendMessageHandler;
		int32_t L_18 = TouchManager_get_SendMessageEvents_m807C4A591F1C10FB759D7470C7D1F54484C478E0_inline(__this, /*hidden argument*/NULL);
		if (!((int32_t)((int32_t)L_18&(int32_t)((int32_t)16))))
		{
			goto IL_00da;
		}
	}
	{
		// if ((SendMessageEvents & MessageType.PointersPressed) != 0) Instance.PointersPressed += pointersPressedSendMessageHandler;
		IL2CPP_RUNTIME_CLASS_INIT(TouchManager_tE3A4C5ABD05B3B6C984CDBB1FA80479E1ACBE92B_il2cpp_TypeInfo_var);
		RuntimeObject* L_19 = TouchManager_get_Instance_m355FC1AAF7F91B86F174A4429E8A44ABE454E8E3(/*hidden argument*/NULL);
		EventHandler_1_t0363FEC12482AFCCEA69CEA150D1710057B94330 * L_20 = (EventHandler_1_t0363FEC12482AFCCEA69CEA150D1710057B94330 *)il2cpp_codegen_object_new(EventHandler_1_t0363FEC12482AFCCEA69CEA150D1710057B94330_il2cpp_TypeInfo_var);
		EventHandler_1__ctor_mDD490FECE01D6EF94516D572915E5E6291E1B7D1(L_20, __this, (intptr_t)((intptr_t)TouchManager_pointersPressedSendMessageHandler_m93132F18416FB55153153A1F9F4D53C46255CF99_RuntimeMethod_var), /*hidden argument*/EventHandler_1__ctor_mDD490FECE01D6EF94516D572915E5E6291E1B7D1_RuntimeMethod_var);
		NullCheck(L_19);
		InterfaceActionInvoker1< EventHandler_1_t0363FEC12482AFCCEA69CEA150D1710057B94330 * >::Invoke(8 /* System.Void TouchScript.ITouchManager::add_PointersPressed(System.EventHandler`1<TouchScript.PointerEventArgs>) */, ITouchManager_t1E2C9BFCD55643DC5C5D4A005A64EA0BEEC2E843_il2cpp_TypeInfo_var, L_19, L_20);
	}

IL_00da:
	{
		// if ((SendMessageEvents & MessageType.PointersReleased) != 0) Instance.PointersReleased += pointersReleasedSendMessageHandler;
		int32_t L_21 = TouchManager_get_SendMessageEvents_m807C4A591F1C10FB759D7470C7D1F54484C478E0_inline(__this, /*hidden argument*/NULL);
		if (!((int32_t)((int32_t)L_21&(int32_t)((int32_t)32))))
		{
			goto IL_00fb;
		}
	}
	{
		// if ((SendMessageEvents & MessageType.PointersReleased) != 0) Instance.PointersReleased += pointersReleasedSendMessageHandler;
		IL2CPP_RUNTIME_CLASS_INIT(TouchManager_tE3A4C5ABD05B3B6C984CDBB1FA80479E1ACBE92B_il2cpp_TypeInfo_var);
		RuntimeObject* L_22 = TouchManager_get_Instance_m355FC1AAF7F91B86F174A4429E8A44ABE454E8E3(/*hidden argument*/NULL);
		EventHandler_1_t0363FEC12482AFCCEA69CEA150D1710057B94330 * L_23 = (EventHandler_1_t0363FEC12482AFCCEA69CEA150D1710057B94330 *)il2cpp_codegen_object_new(EventHandler_1_t0363FEC12482AFCCEA69CEA150D1710057B94330_il2cpp_TypeInfo_var);
		EventHandler_1__ctor_mDD490FECE01D6EF94516D572915E5E6291E1B7D1(L_23, __this, (intptr_t)((intptr_t)TouchManager_pointersReleasedSendMessageHandler_mBA8E495E7939B9A1F66F81C7179514F1F9EAE3FF_RuntimeMethod_var), /*hidden argument*/EventHandler_1__ctor_mDD490FECE01D6EF94516D572915E5E6291E1B7D1_RuntimeMethod_var);
		NullCheck(L_22);
		InterfaceActionInvoker1< EventHandler_1_t0363FEC12482AFCCEA69CEA150D1710057B94330 * >::Invoke(10 /* System.Void TouchScript.ITouchManager::add_PointersReleased(System.EventHandler`1<TouchScript.PointerEventArgs>) */, ITouchManager_t1E2C9BFCD55643DC5C5D4A005A64EA0BEEC2E843_il2cpp_TypeInfo_var, L_22, L_23);
	}

IL_00fb:
	{
		// if ((SendMessageEvents & MessageType.PointersRemoved) != 0) Instance.PointersRemoved += pointersRemovedSendMessageHandler;
		int32_t L_24 = TouchManager_get_SendMessageEvents_m807C4A591F1C10FB759D7470C7D1F54484C478E0_inline(__this, /*hidden argument*/NULL);
		if (!((int32_t)((int32_t)L_24&(int32_t)((int32_t)64))))
		{
			goto IL_011c;
		}
	}
	{
		// if ((SendMessageEvents & MessageType.PointersRemoved) != 0) Instance.PointersRemoved += pointersRemovedSendMessageHandler;
		IL2CPP_RUNTIME_CLASS_INIT(TouchManager_tE3A4C5ABD05B3B6C984CDBB1FA80479E1ACBE92B_il2cpp_TypeInfo_var);
		RuntimeObject* L_25 = TouchManager_get_Instance_m355FC1AAF7F91B86F174A4429E8A44ABE454E8E3(/*hidden argument*/NULL);
		EventHandler_1_t0363FEC12482AFCCEA69CEA150D1710057B94330 * L_26 = (EventHandler_1_t0363FEC12482AFCCEA69CEA150D1710057B94330 *)il2cpp_codegen_object_new(EventHandler_1_t0363FEC12482AFCCEA69CEA150D1710057B94330_il2cpp_TypeInfo_var);
		EventHandler_1__ctor_mDD490FECE01D6EF94516D572915E5E6291E1B7D1(L_26, __this, (intptr_t)((intptr_t)TouchManager_pointersRemovedSendMessageHandler_m57EA76B182D2BAD64AA62E1141EBC21CD80AE940_RuntimeMethod_var), /*hidden argument*/EventHandler_1__ctor_mDD490FECE01D6EF94516D572915E5E6291E1B7D1_RuntimeMethod_var);
		NullCheck(L_25);
		InterfaceActionInvoker1< EventHandler_1_t0363FEC12482AFCCEA69CEA150D1710057B94330 * >::Invoke(12 /* System.Void TouchScript.ITouchManager::add_PointersRemoved(System.EventHandler`1<TouchScript.PointerEventArgs>) */, ITouchManager_t1E2C9BFCD55643DC5C5D4A005A64EA0BEEC2E843_il2cpp_TypeInfo_var, L_25, L_26);
	}

IL_011c:
	{
		// if ((SendMessageEvents & MessageType.PointersCancelled) != 0) Instance.PointersCancelled += pointersCancelledSendMessageHandler;
		int32_t L_27 = TouchManager_get_SendMessageEvents_m807C4A591F1C10FB759D7470C7D1F54484C478E0_inline(__this, /*hidden argument*/NULL);
		if (!((int32_t)((int32_t)L_27&(int32_t)((int32_t)128))))
		{
			goto IL_0140;
		}
	}
	{
		// if ((SendMessageEvents & MessageType.PointersCancelled) != 0) Instance.PointersCancelled += pointersCancelledSendMessageHandler;
		IL2CPP_RUNTIME_CLASS_INIT(TouchManager_tE3A4C5ABD05B3B6C984CDBB1FA80479E1ACBE92B_il2cpp_TypeInfo_var);
		RuntimeObject* L_28 = TouchManager_get_Instance_m355FC1AAF7F91B86F174A4429E8A44ABE454E8E3(/*hidden argument*/NULL);
		EventHandler_1_t0363FEC12482AFCCEA69CEA150D1710057B94330 * L_29 = (EventHandler_1_t0363FEC12482AFCCEA69CEA150D1710057B94330 *)il2cpp_codegen_object_new(EventHandler_1_t0363FEC12482AFCCEA69CEA150D1710057B94330_il2cpp_TypeInfo_var);
		EventHandler_1__ctor_mDD490FECE01D6EF94516D572915E5E6291E1B7D1(L_29, __this, (intptr_t)((intptr_t)TouchManager_pointersCancelledSendMessageHandler_m4317C08AB3D25607D0B1544FBB0AEBE063597BFE_RuntimeMethod_var), /*hidden argument*/EventHandler_1__ctor_mDD490FECE01D6EF94516D572915E5E6291E1B7D1_RuntimeMethod_var);
		NullCheck(L_28);
		InterfaceActionInvoker1< EventHandler_1_t0363FEC12482AFCCEA69CEA150D1710057B94330 * >::Invoke(14 /* System.Void TouchScript.ITouchManager::add_PointersCancelled(System.EventHandler`1<TouchScript.PointerEventArgs>) */, ITouchManager_t1E2C9BFCD55643DC5C5D4A005A64EA0BEEC2E843_il2cpp_TypeInfo_var, L_28, L_29);
	}

IL_0140:
	{
		// }
		return;
	}
}
// System.Void TouchScript.TouchManager::removeSendMessageSubscriptions()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TouchManager_removeSendMessageSubscriptions_mDA19EA010E735C99BF9CEC546274DAC4694CF57C (TouchManager_tE3A4C5ABD05B3B6C984CDBB1FA80479E1ACBE92B * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TouchManager_removeSendMessageSubscriptions_mDA19EA010E735C99BF9CEC546274DAC4694CF57C_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// if (!Application.isPlaying) return;
		bool L_0 = Application_get_isPlaying_mF43B519662E7433DD90D883E5AE22EC3CFB65CA5(/*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_0008;
		}
	}
	{
		// if (!Application.isPlaying) return;
		return;
	}

IL_0008:
	{
		// if (Instance == null) return;
		IL2CPP_RUNTIME_CLASS_INIT(TouchManager_tE3A4C5ABD05B3B6C984CDBB1FA80479E1ACBE92B_il2cpp_TypeInfo_var);
		RuntimeObject* L_1 = TouchManager_get_Instance_m355FC1AAF7F91B86F174A4429E8A44ABE454E8E3(/*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_0010;
		}
	}
	{
		// if (Instance == null) return;
		return;
	}

IL_0010:
	{
		// Instance.FrameStarted -= frameStartedSendMessageHandler;
		IL2CPP_RUNTIME_CLASS_INIT(TouchManager_tE3A4C5ABD05B3B6C984CDBB1FA80479E1ACBE92B_il2cpp_TypeInfo_var);
		RuntimeObject* L_2 = TouchManager_get_Instance_m355FC1AAF7F91B86F174A4429E8A44ABE454E8E3(/*hidden argument*/NULL);
		EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C * L_3 = (EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C *)il2cpp_codegen_object_new(EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C_il2cpp_TypeInfo_var);
		EventHandler__ctor_m497A2BCD46DB769EAFFDE919303FCAE226906B6F(L_3, __this, (intptr_t)((intptr_t)TouchManager_frameStartedSendMessageHandler_mAE3F3B2767D5FA1521A68F0275B31A7A9B3B2E04_RuntimeMethod_var), /*hidden argument*/NULL);
		NullCheck(L_2);
		InterfaceActionInvoker1< EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C * >::Invoke(1 /* System.Void TouchScript.ITouchManager::remove_FrameStarted(System.EventHandler) */, ITouchManager_t1E2C9BFCD55643DC5C5D4A005A64EA0BEEC2E843_il2cpp_TypeInfo_var, L_2, L_3);
		// Instance.FrameFinished -= frameFinishedSendMessageHandler;
		RuntimeObject* L_4 = TouchManager_get_Instance_m355FC1AAF7F91B86F174A4429E8A44ABE454E8E3(/*hidden argument*/NULL);
		EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C * L_5 = (EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C *)il2cpp_codegen_object_new(EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C_il2cpp_TypeInfo_var);
		EventHandler__ctor_m497A2BCD46DB769EAFFDE919303FCAE226906B6F(L_5, __this, (intptr_t)((intptr_t)TouchManager_frameFinishedSendMessageHandler_m06643F8EE650407C385EF6648920F3A211E0D5E6_RuntimeMethod_var), /*hidden argument*/NULL);
		NullCheck(L_4);
		InterfaceActionInvoker1< EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C * >::Invoke(3 /* System.Void TouchScript.ITouchManager::remove_FrameFinished(System.EventHandler) */, ITouchManager_t1E2C9BFCD55643DC5C5D4A005A64EA0BEEC2E843_il2cpp_TypeInfo_var, L_4, L_5);
		// Instance.PointersAdded -= pointersAddedSendMessageHandler;
		RuntimeObject* L_6 = TouchManager_get_Instance_m355FC1AAF7F91B86F174A4429E8A44ABE454E8E3(/*hidden argument*/NULL);
		EventHandler_1_t0363FEC12482AFCCEA69CEA150D1710057B94330 * L_7 = (EventHandler_1_t0363FEC12482AFCCEA69CEA150D1710057B94330 *)il2cpp_codegen_object_new(EventHandler_1_t0363FEC12482AFCCEA69CEA150D1710057B94330_il2cpp_TypeInfo_var);
		EventHandler_1__ctor_mDD490FECE01D6EF94516D572915E5E6291E1B7D1(L_7, __this, (intptr_t)((intptr_t)TouchManager_pointersAddedSendMessageHandler_m63C06829F976D339666BFF3FAB8530E101355FDD_RuntimeMethod_var), /*hidden argument*/EventHandler_1__ctor_mDD490FECE01D6EF94516D572915E5E6291E1B7D1_RuntimeMethod_var);
		NullCheck(L_6);
		InterfaceActionInvoker1< EventHandler_1_t0363FEC12482AFCCEA69CEA150D1710057B94330 * >::Invoke(5 /* System.Void TouchScript.ITouchManager::remove_PointersAdded(System.EventHandler`1<TouchScript.PointerEventArgs>) */, ITouchManager_t1E2C9BFCD55643DC5C5D4A005A64EA0BEEC2E843_il2cpp_TypeInfo_var, L_6, L_7);
		// Instance.PointersUpdated -= pointersUpdatedSendMessageHandler;
		RuntimeObject* L_8 = TouchManager_get_Instance_m355FC1AAF7F91B86F174A4429E8A44ABE454E8E3(/*hidden argument*/NULL);
		EventHandler_1_t0363FEC12482AFCCEA69CEA150D1710057B94330 * L_9 = (EventHandler_1_t0363FEC12482AFCCEA69CEA150D1710057B94330 *)il2cpp_codegen_object_new(EventHandler_1_t0363FEC12482AFCCEA69CEA150D1710057B94330_il2cpp_TypeInfo_var);
		EventHandler_1__ctor_mDD490FECE01D6EF94516D572915E5E6291E1B7D1(L_9, __this, (intptr_t)((intptr_t)TouchManager_pointersUpdatedSendMessageHandler_m58381274AF569B5230025CFB6ABCDE92318C4606_RuntimeMethod_var), /*hidden argument*/EventHandler_1__ctor_mDD490FECE01D6EF94516D572915E5E6291E1B7D1_RuntimeMethod_var);
		NullCheck(L_8);
		InterfaceActionInvoker1< EventHandler_1_t0363FEC12482AFCCEA69CEA150D1710057B94330 * >::Invoke(7 /* System.Void TouchScript.ITouchManager::remove_PointersUpdated(System.EventHandler`1<TouchScript.PointerEventArgs>) */, ITouchManager_t1E2C9BFCD55643DC5C5D4A005A64EA0BEEC2E843_il2cpp_TypeInfo_var, L_8, L_9);
		// Instance.PointersPressed -= pointersPressedSendMessageHandler;
		RuntimeObject* L_10 = TouchManager_get_Instance_m355FC1AAF7F91B86F174A4429E8A44ABE454E8E3(/*hidden argument*/NULL);
		EventHandler_1_t0363FEC12482AFCCEA69CEA150D1710057B94330 * L_11 = (EventHandler_1_t0363FEC12482AFCCEA69CEA150D1710057B94330 *)il2cpp_codegen_object_new(EventHandler_1_t0363FEC12482AFCCEA69CEA150D1710057B94330_il2cpp_TypeInfo_var);
		EventHandler_1__ctor_mDD490FECE01D6EF94516D572915E5E6291E1B7D1(L_11, __this, (intptr_t)((intptr_t)TouchManager_pointersPressedSendMessageHandler_m93132F18416FB55153153A1F9F4D53C46255CF99_RuntimeMethod_var), /*hidden argument*/EventHandler_1__ctor_mDD490FECE01D6EF94516D572915E5E6291E1B7D1_RuntimeMethod_var);
		NullCheck(L_10);
		InterfaceActionInvoker1< EventHandler_1_t0363FEC12482AFCCEA69CEA150D1710057B94330 * >::Invoke(9 /* System.Void TouchScript.ITouchManager::remove_PointersPressed(System.EventHandler`1<TouchScript.PointerEventArgs>) */, ITouchManager_t1E2C9BFCD55643DC5C5D4A005A64EA0BEEC2E843_il2cpp_TypeInfo_var, L_10, L_11);
		// Instance.PointersReleased -= pointersReleasedSendMessageHandler;
		RuntimeObject* L_12 = TouchManager_get_Instance_m355FC1AAF7F91B86F174A4429E8A44ABE454E8E3(/*hidden argument*/NULL);
		EventHandler_1_t0363FEC12482AFCCEA69CEA150D1710057B94330 * L_13 = (EventHandler_1_t0363FEC12482AFCCEA69CEA150D1710057B94330 *)il2cpp_codegen_object_new(EventHandler_1_t0363FEC12482AFCCEA69CEA150D1710057B94330_il2cpp_TypeInfo_var);
		EventHandler_1__ctor_mDD490FECE01D6EF94516D572915E5E6291E1B7D1(L_13, __this, (intptr_t)((intptr_t)TouchManager_pointersReleasedSendMessageHandler_mBA8E495E7939B9A1F66F81C7179514F1F9EAE3FF_RuntimeMethod_var), /*hidden argument*/EventHandler_1__ctor_mDD490FECE01D6EF94516D572915E5E6291E1B7D1_RuntimeMethod_var);
		NullCheck(L_12);
		InterfaceActionInvoker1< EventHandler_1_t0363FEC12482AFCCEA69CEA150D1710057B94330 * >::Invoke(11 /* System.Void TouchScript.ITouchManager::remove_PointersReleased(System.EventHandler`1<TouchScript.PointerEventArgs>) */, ITouchManager_t1E2C9BFCD55643DC5C5D4A005A64EA0BEEC2E843_il2cpp_TypeInfo_var, L_12, L_13);
		// Instance.PointersRemoved -= pointersRemovedSendMessageHandler;
		RuntimeObject* L_14 = TouchManager_get_Instance_m355FC1AAF7F91B86F174A4429E8A44ABE454E8E3(/*hidden argument*/NULL);
		EventHandler_1_t0363FEC12482AFCCEA69CEA150D1710057B94330 * L_15 = (EventHandler_1_t0363FEC12482AFCCEA69CEA150D1710057B94330 *)il2cpp_codegen_object_new(EventHandler_1_t0363FEC12482AFCCEA69CEA150D1710057B94330_il2cpp_TypeInfo_var);
		EventHandler_1__ctor_mDD490FECE01D6EF94516D572915E5E6291E1B7D1(L_15, __this, (intptr_t)((intptr_t)TouchManager_pointersRemovedSendMessageHandler_m57EA76B182D2BAD64AA62E1141EBC21CD80AE940_RuntimeMethod_var), /*hidden argument*/EventHandler_1__ctor_mDD490FECE01D6EF94516D572915E5E6291E1B7D1_RuntimeMethod_var);
		NullCheck(L_14);
		InterfaceActionInvoker1< EventHandler_1_t0363FEC12482AFCCEA69CEA150D1710057B94330 * >::Invoke(13 /* System.Void TouchScript.ITouchManager::remove_PointersRemoved(System.EventHandler`1<TouchScript.PointerEventArgs>) */, ITouchManager_t1E2C9BFCD55643DC5C5D4A005A64EA0BEEC2E843_il2cpp_TypeInfo_var, L_14, L_15);
		// Instance.PointersCancelled -= pointersCancelledSendMessageHandler;
		RuntimeObject* L_16 = TouchManager_get_Instance_m355FC1AAF7F91B86F174A4429E8A44ABE454E8E3(/*hidden argument*/NULL);
		EventHandler_1_t0363FEC12482AFCCEA69CEA150D1710057B94330 * L_17 = (EventHandler_1_t0363FEC12482AFCCEA69CEA150D1710057B94330 *)il2cpp_codegen_object_new(EventHandler_1_t0363FEC12482AFCCEA69CEA150D1710057B94330_il2cpp_TypeInfo_var);
		EventHandler_1__ctor_mDD490FECE01D6EF94516D572915E5E6291E1B7D1(L_17, __this, (intptr_t)((intptr_t)TouchManager_pointersCancelledSendMessageHandler_m4317C08AB3D25607D0B1544FBB0AEBE063597BFE_RuntimeMethod_var), /*hidden argument*/EventHandler_1__ctor_mDD490FECE01D6EF94516D572915E5E6291E1B7D1_RuntimeMethod_var);
		NullCheck(L_16);
		InterfaceActionInvoker1< EventHandler_1_t0363FEC12482AFCCEA69CEA150D1710057B94330 * >::Invoke(15 /* System.Void TouchScript.ITouchManager::remove_PointersCancelled(System.EventHandler`1<TouchScript.PointerEventArgs>) */, ITouchManager_t1E2C9BFCD55643DC5C5D4A005A64EA0BEEC2E843_il2cpp_TypeInfo_var, L_16, L_17);
		// }
		return;
	}
}
// System.Void TouchScript.TouchManager::pointersAddedSendMessageHandler(System.Object,TouchScript.PointerEventArgs)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TouchManager_pointersAddedSendMessageHandler_m63C06829F976D339666BFF3FAB8530E101355FDD (TouchManager_tE3A4C5ABD05B3B6C984CDBB1FA80479E1ACBE92B * __this, RuntimeObject * ___sender0, PointerEventArgs_t60C9291910F961213770A1302F3CE7CC5A580106 * ___e1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TouchManager_pointersAddedSendMessageHandler_m63C06829F976D339666BFF3FAB8530E101355FDD_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		// sendMessageTarget.SendMessage(MessageName.OnPointersAdd.ToString(), e.Pointers,
		//     SendMessageOptions.DontRequireReceiver);
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_0 = __this->get_sendMessageTarget_23();
		V_0 = 4;
		RuntimeObject * L_1 = Box(MessageName_t450BA987F2CEE58334768B847602C2465422A4BE_il2cpp_TypeInfo_var, (&V_0));
		NullCheck(L_1);
		String_t* L_2 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_1);
		V_0 = *(int32_t*)UnBox(L_1);
		PointerEventArgs_t60C9291910F961213770A1302F3CE7CC5A580106 * L_3 = ___e1;
		NullCheck(L_3);
		RuntimeObject* L_4 = PointerEventArgs_get_Pointers_m16B91F1D5BE47012CF6C4BC0BC3F53691525FC41_inline(L_3, /*hidden argument*/NULL);
		NullCheck(L_0);
		GameObject_SendMessage_mB9147E503F1F55C4F3BC2816C0BDA8C21EA22E95(L_0, L_2, L_4, 1, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void TouchScript.TouchManager::pointersUpdatedSendMessageHandler(System.Object,TouchScript.PointerEventArgs)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TouchManager_pointersUpdatedSendMessageHandler_m58381274AF569B5230025CFB6ABCDE92318C4606 (TouchManager_tE3A4C5ABD05B3B6C984CDBB1FA80479E1ACBE92B * __this, RuntimeObject * ___sender0, PointerEventArgs_t60C9291910F961213770A1302F3CE7CC5A580106 * ___e1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TouchManager_pointersUpdatedSendMessageHandler_m58381274AF569B5230025CFB6ABCDE92318C4606_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		// sendMessageTarget.SendMessage(MessageName.OnPointersUpdate.ToString(), e.Pointers,
		//     SendMessageOptions.DontRequireReceiver);
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_0 = __this->get_sendMessageTarget_23();
		V_0 = 8;
		RuntimeObject * L_1 = Box(MessageName_t450BA987F2CEE58334768B847602C2465422A4BE_il2cpp_TypeInfo_var, (&V_0));
		NullCheck(L_1);
		String_t* L_2 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_1);
		V_0 = *(int32_t*)UnBox(L_1);
		PointerEventArgs_t60C9291910F961213770A1302F3CE7CC5A580106 * L_3 = ___e1;
		NullCheck(L_3);
		RuntimeObject* L_4 = PointerEventArgs_get_Pointers_m16B91F1D5BE47012CF6C4BC0BC3F53691525FC41_inline(L_3, /*hidden argument*/NULL);
		NullCheck(L_0);
		GameObject_SendMessage_mB9147E503F1F55C4F3BC2816C0BDA8C21EA22E95(L_0, L_2, L_4, 1, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void TouchScript.TouchManager::pointersPressedSendMessageHandler(System.Object,TouchScript.PointerEventArgs)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TouchManager_pointersPressedSendMessageHandler_m93132F18416FB55153153A1F9F4D53C46255CF99 (TouchManager_tE3A4C5ABD05B3B6C984CDBB1FA80479E1ACBE92B * __this, RuntimeObject * ___sender0, PointerEventArgs_t60C9291910F961213770A1302F3CE7CC5A580106 * ___e1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TouchManager_pointersPressedSendMessageHandler_m93132F18416FB55153153A1F9F4D53C46255CF99_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		// sendMessageTarget.SendMessage(MessageName.OnPointersPress.ToString(), e.Pointers,
		//     SendMessageOptions.DontRequireReceiver);
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_0 = __this->get_sendMessageTarget_23();
		V_0 = ((int32_t)16);
		RuntimeObject * L_1 = Box(MessageName_t450BA987F2CEE58334768B847602C2465422A4BE_il2cpp_TypeInfo_var, (&V_0));
		NullCheck(L_1);
		String_t* L_2 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_1);
		V_0 = *(int32_t*)UnBox(L_1);
		PointerEventArgs_t60C9291910F961213770A1302F3CE7CC5A580106 * L_3 = ___e1;
		NullCheck(L_3);
		RuntimeObject* L_4 = PointerEventArgs_get_Pointers_m16B91F1D5BE47012CF6C4BC0BC3F53691525FC41_inline(L_3, /*hidden argument*/NULL);
		NullCheck(L_0);
		GameObject_SendMessage_mB9147E503F1F55C4F3BC2816C0BDA8C21EA22E95(L_0, L_2, L_4, 1, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void TouchScript.TouchManager::pointersReleasedSendMessageHandler(System.Object,TouchScript.PointerEventArgs)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TouchManager_pointersReleasedSendMessageHandler_mBA8E495E7939B9A1F66F81C7179514F1F9EAE3FF (TouchManager_tE3A4C5ABD05B3B6C984CDBB1FA80479E1ACBE92B * __this, RuntimeObject * ___sender0, PointerEventArgs_t60C9291910F961213770A1302F3CE7CC5A580106 * ___e1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TouchManager_pointersReleasedSendMessageHandler_mBA8E495E7939B9A1F66F81C7179514F1F9EAE3FF_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		// sendMessageTarget.SendMessage(MessageName.OnPointersRelease.ToString(), e.Pointers,
		//     SendMessageOptions.DontRequireReceiver);
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_0 = __this->get_sendMessageTarget_23();
		V_0 = ((int32_t)32);
		RuntimeObject * L_1 = Box(MessageName_t450BA987F2CEE58334768B847602C2465422A4BE_il2cpp_TypeInfo_var, (&V_0));
		NullCheck(L_1);
		String_t* L_2 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_1);
		V_0 = *(int32_t*)UnBox(L_1);
		PointerEventArgs_t60C9291910F961213770A1302F3CE7CC5A580106 * L_3 = ___e1;
		NullCheck(L_3);
		RuntimeObject* L_4 = PointerEventArgs_get_Pointers_m16B91F1D5BE47012CF6C4BC0BC3F53691525FC41_inline(L_3, /*hidden argument*/NULL);
		NullCheck(L_0);
		GameObject_SendMessage_mB9147E503F1F55C4F3BC2816C0BDA8C21EA22E95(L_0, L_2, L_4, 1, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void TouchScript.TouchManager::pointersRemovedSendMessageHandler(System.Object,TouchScript.PointerEventArgs)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TouchManager_pointersRemovedSendMessageHandler_m57EA76B182D2BAD64AA62E1141EBC21CD80AE940 (TouchManager_tE3A4C5ABD05B3B6C984CDBB1FA80479E1ACBE92B * __this, RuntimeObject * ___sender0, PointerEventArgs_t60C9291910F961213770A1302F3CE7CC5A580106 * ___e1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TouchManager_pointersRemovedSendMessageHandler_m57EA76B182D2BAD64AA62E1141EBC21CD80AE940_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		// sendMessageTarget.SendMessage(MessageName.OnPointersRemove.ToString(), e.Pointers,
		//     SendMessageOptions.DontRequireReceiver);
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_0 = __this->get_sendMessageTarget_23();
		V_0 = ((int32_t)64);
		RuntimeObject * L_1 = Box(MessageName_t450BA987F2CEE58334768B847602C2465422A4BE_il2cpp_TypeInfo_var, (&V_0));
		NullCheck(L_1);
		String_t* L_2 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_1);
		V_0 = *(int32_t*)UnBox(L_1);
		PointerEventArgs_t60C9291910F961213770A1302F3CE7CC5A580106 * L_3 = ___e1;
		NullCheck(L_3);
		RuntimeObject* L_4 = PointerEventArgs_get_Pointers_m16B91F1D5BE47012CF6C4BC0BC3F53691525FC41_inline(L_3, /*hidden argument*/NULL);
		NullCheck(L_0);
		GameObject_SendMessage_mB9147E503F1F55C4F3BC2816C0BDA8C21EA22E95(L_0, L_2, L_4, 1, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void TouchScript.TouchManager::pointersCancelledSendMessageHandler(System.Object,TouchScript.PointerEventArgs)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TouchManager_pointersCancelledSendMessageHandler_m4317C08AB3D25607D0B1544FBB0AEBE063597BFE (TouchManager_tE3A4C5ABD05B3B6C984CDBB1FA80479E1ACBE92B * __this, RuntimeObject * ___sender0, PointerEventArgs_t60C9291910F961213770A1302F3CE7CC5A580106 * ___e1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TouchManager_pointersCancelledSendMessageHandler_m4317C08AB3D25607D0B1544FBB0AEBE063597BFE_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		// sendMessageTarget.SendMessage(MessageName.OnPointersCancel.ToString(), e.Pointers,
		//     SendMessageOptions.DontRequireReceiver);
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_0 = __this->get_sendMessageTarget_23();
		V_0 = ((int32_t)128);
		RuntimeObject * L_1 = Box(MessageName_t450BA987F2CEE58334768B847602C2465422A4BE_il2cpp_TypeInfo_var, (&V_0));
		NullCheck(L_1);
		String_t* L_2 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_1);
		V_0 = *(int32_t*)UnBox(L_1);
		PointerEventArgs_t60C9291910F961213770A1302F3CE7CC5A580106 * L_3 = ___e1;
		NullCheck(L_3);
		RuntimeObject* L_4 = PointerEventArgs_get_Pointers_m16B91F1D5BE47012CF6C4BC0BC3F53691525FC41_inline(L_3, /*hidden argument*/NULL);
		NullCheck(L_0);
		GameObject_SendMessage_mB9147E503F1F55C4F3BC2816C0BDA8C21EA22E95(L_0, L_2, L_4, 1, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void TouchScript.TouchManager::frameStartedSendMessageHandler(System.Object,System.EventArgs)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TouchManager_frameStartedSendMessageHandler_mAE3F3B2767D5FA1521A68F0275B31A7A9B3B2E04 (TouchManager_tE3A4C5ABD05B3B6C984CDBB1FA80479E1ACBE92B * __this, RuntimeObject * ___sender0, EventArgs_t8E6CA180BE0E56674C6407011A94BAF7C757352E * ___e1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TouchManager_frameStartedSendMessageHandler_mAE3F3B2767D5FA1521A68F0275B31A7A9B3B2E04_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		// sendMessageTarget.SendMessage(MessageName.OnFrameStart.ToString(),
		//     SendMessageOptions.DontRequireReceiver);
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_0 = __this->get_sendMessageTarget_23();
		V_0 = 1;
		RuntimeObject * L_1 = Box(MessageName_t450BA987F2CEE58334768B847602C2465422A4BE_il2cpp_TypeInfo_var, (&V_0));
		NullCheck(L_1);
		String_t* L_2 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_1);
		V_0 = *(int32_t*)UnBox(L_1);
		NullCheck(L_0);
		GameObject_SendMessage_m68EE6232C4171FB0767181C1C1DD95C1103A6A75(L_0, L_2, 1, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void TouchScript.TouchManager::frameFinishedSendMessageHandler(System.Object,System.EventArgs)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TouchManager_frameFinishedSendMessageHandler_m06643F8EE650407C385EF6648920F3A211E0D5E6 (TouchManager_tE3A4C5ABD05B3B6C984CDBB1FA80479E1ACBE92B * __this, RuntimeObject * ___sender0, EventArgs_t8E6CA180BE0E56674C6407011A94BAF7C757352E * ___e1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TouchManager_frameFinishedSendMessageHandler_m06643F8EE650407C385EF6648920F3A211E0D5E6_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		// sendMessageTarget.SendMessage(MessageName.OnFrameFinish.ToString(),
		//     SendMessageOptions.DontRequireReceiver);
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_0 = __this->get_sendMessageTarget_23();
		V_0 = 2;
		RuntimeObject * L_1 = Box(MessageName_t450BA987F2CEE58334768B847602C2465422A4BE_il2cpp_TypeInfo_var, (&V_0));
		NullCheck(L_1);
		String_t* L_2 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_1);
		V_0 = *(int32_t*)UnBox(L_1);
		NullCheck(L_0);
		GameObject_SendMessage_m68EE6232C4171FB0767181C1C1DD95C1103A6A75(L_0, L_2, 1, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void TouchScript.TouchManager::updateUnityEventsSubscription()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TouchManager_updateUnityEventsSubscription_m6A5CA7A27D10F83B2A5CA578C3446A140E2FF3B4 (TouchManager_tE3A4C5ABD05B3B6C984CDBB1FA80479E1ACBE92B * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TouchManager_updateUnityEventsSubscription_m6A5CA7A27D10F83B2A5CA578C3446A140E2FF3B4_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// if (!Application.isPlaying) return;
		bool L_0 = Application_get_isPlaying_mF43B519662E7433DD90D883E5AE22EC3CFB65CA5(/*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_0008;
		}
	}
	{
		// if (!Application.isPlaying) return;
		return;
	}

IL_0008:
	{
		// if (Instance == null) return;
		IL2CPP_RUNTIME_CLASS_INIT(TouchManager_tE3A4C5ABD05B3B6C984CDBB1FA80479E1ACBE92B_il2cpp_TypeInfo_var);
		RuntimeObject* L_1 = TouchManager_get_Instance_m355FC1AAF7F91B86F174A4429E8A44ABE454E8E3(/*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_0010;
		}
	}
	{
		// if (Instance == null) return;
		return;
	}

IL_0010:
	{
		// removeUnityEventsSubscriptions();
		TouchManager_removeUnityEventsSubscriptions_m16B73F9CF5C8D19E6FB2776C07439A1F6A93F3FA(__this, /*hidden argument*/NULL);
		// if (!useUnityEvents) return;
		bool L_2 = __this->get_useUnityEvents_24();
		if (L_2)
		{
			goto IL_001f;
		}
	}
	{
		// if (!useUnityEvents) return;
		return;
	}

IL_001f:
	{
		// Instance.FrameStarted += frameStartedUnityEventsHandler;
		IL2CPP_RUNTIME_CLASS_INIT(TouchManager_tE3A4C5ABD05B3B6C984CDBB1FA80479E1ACBE92B_il2cpp_TypeInfo_var);
		RuntimeObject* L_3 = TouchManager_get_Instance_m355FC1AAF7F91B86F174A4429E8A44ABE454E8E3(/*hidden argument*/NULL);
		EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C * L_4 = (EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C *)il2cpp_codegen_object_new(EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C_il2cpp_TypeInfo_var);
		EventHandler__ctor_m497A2BCD46DB769EAFFDE919303FCAE226906B6F(L_4, __this, (intptr_t)((intptr_t)TouchManager_frameStartedUnityEventsHandler_m89C3B5C487CEB8878A3B71B698F8BB814713ECF0_RuntimeMethod_var), /*hidden argument*/NULL);
		NullCheck(L_3);
		InterfaceActionInvoker1< EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C * >::Invoke(0 /* System.Void TouchScript.ITouchManager::add_FrameStarted(System.EventHandler) */, ITouchManager_t1E2C9BFCD55643DC5C5D4A005A64EA0BEEC2E843_il2cpp_TypeInfo_var, L_3, L_4);
		// Instance.FrameFinished += frameFinishedUnityEventsHandler;
		RuntimeObject* L_5 = TouchManager_get_Instance_m355FC1AAF7F91B86F174A4429E8A44ABE454E8E3(/*hidden argument*/NULL);
		EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C * L_6 = (EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C *)il2cpp_codegen_object_new(EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C_il2cpp_TypeInfo_var);
		EventHandler__ctor_m497A2BCD46DB769EAFFDE919303FCAE226906B6F(L_6, __this, (intptr_t)((intptr_t)TouchManager_frameFinishedUnityEventsHandler_mBFE2FF2587FEE191DB70D58242692DC0690191ED_RuntimeMethod_var), /*hidden argument*/NULL);
		NullCheck(L_5);
		InterfaceActionInvoker1< EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C * >::Invoke(2 /* System.Void TouchScript.ITouchManager::add_FrameFinished(System.EventHandler) */, ITouchManager_t1E2C9BFCD55643DC5C5D4A005A64EA0BEEC2E843_il2cpp_TypeInfo_var, L_5, L_6);
		// Instance.PointersAdded += pointersAddedUnityEventsHandler;
		RuntimeObject* L_7 = TouchManager_get_Instance_m355FC1AAF7F91B86F174A4429E8A44ABE454E8E3(/*hidden argument*/NULL);
		EventHandler_1_t0363FEC12482AFCCEA69CEA150D1710057B94330 * L_8 = (EventHandler_1_t0363FEC12482AFCCEA69CEA150D1710057B94330 *)il2cpp_codegen_object_new(EventHandler_1_t0363FEC12482AFCCEA69CEA150D1710057B94330_il2cpp_TypeInfo_var);
		EventHandler_1__ctor_mDD490FECE01D6EF94516D572915E5E6291E1B7D1(L_8, __this, (intptr_t)((intptr_t)TouchManager_pointersAddedUnityEventsHandler_m05BDEF56AC60141C69CFECD9324EAF9DCCBA3859_RuntimeMethod_var), /*hidden argument*/EventHandler_1__ctor_mDD490FECE01D6EF94516D572915E5E6291E1B7D1_RuntimeMethod_var);
		NullCheck(L_7);
		InterfaceActionInvoker1< EventHandler_1_t0363FEC12482AFCCEA69CEA150D1710057B94330 * >::Invoke(4 /* System.Void TouchScript.ITouchManager::add_PointersAdded(System.EventHandler`1<TouchScript.PointerEventArgs>) */, ITouchManager_t1E2C9BFCD55643DC5C5D4A005A64EA0BEEC2E843_il2cpp_TypeInfo_var, L_7, L_8);
		// Instance.PointersUpdated += pointersUpdatedUnityEventsHandler;
		RuntimeObject* L_9 = TouchManager_get_Instance_m355FC1AAF7F91B86F174A4429E8A44ABE454E8E3(/*hidden argument*/NULL);
		EventHandler_1_t0363FEC12482AFCCEA69CEA150D1710057B94330 * L_10 = (EventHandler_1_t0363FEC12482AFCCEA69CEA150D1710057B94330 *)il2cpp_codegen_object_new(EventHandler_1_t0363FEC12482AFCCEA69CEA150D1710057B94330_il2cpp_TypeInfo_var);
		EventHandler_1__ctor_mDD490FECE01D6EF94516D572915E5E6291E1B7D1(L_10, __this, (intptr_t)((intptr_t)TouchManager_pointersUpdatedUnityEventsHandler_mDD4D2BBAA16EB45E6473E25CAB802E6B1C02C719_RuntimeMethod_var), /*hidden argument*/EventHandler_1__ctor_mDD490FECE01D6EF94516D572915E5E6291E1B7D1_RuntimeMethod_var);
		NullCheck(L_9);
		InterfaceActionInvoker1< EventHandler_1_t0363FEC12482AFCCEA69CEA150D1710057B94330 * >::Invoke(6 /* System.Void TouchScript.ITouchManager::add_PointersUpdated(System.EventHandler`1<TouchScript.PointerEventArgs>) */, ITouchManager_t1E2C9BFCD55643DC5C5D4A005A64EA0BEEC2E843_il2cpp_TypeInfo_var, L_9, L_10);
		// Instance.PointersPressed += pointersPressedUnityEventsHandler;
		RuntimeObject* L_11 = TouchManager_get_Instance_m355FC1AAF7F91B86F174A4429E8A44ABE454E8E3(/*hidden argument*/NULL);
		EventHandler_1_t0363FEC12482AFCCEA69CEA150D1710057B94330 * L_12 = (EventHandler_1_t0363FEC12482AFCCEA69CEA150D1710057B94330 *)il2cpp_codegen_object_new(EventHandler_1_t0363FEC12482AFCCEA69CEA150D1710057B94330_il2cpp_TypeInfo_var);
		EventHandler_1__ctor_mDD490FECE01D6EF94516D572915E5E6291E1B7D1(L_12, __this, (intptr_t)((intptr_t)TouchManager_pointersPressedUnityEventsHandler_m82F19BBA6DFDA4EA2767F2F51EA100083F5C2F84_RuntimeMethod_var), /*hidden argument*/EventHandler_1__ctor_mDD490FECE01D6EF94516D572915E5E6291E1B7D1_RuntimeMethod_var);
		NullCheck(L_11);
		InterfaceActionInvoker1< EventHandler_1_t0363FEC12482AFCCEA69CEA150D1710057B94330 * >::Invoke(8 /* System.Void TouchScript.ITouchManager::add_PointersPressed(System.EventHandler`1<TouchScript.PointerEventArgs>) */, ITouchManager_t1E2C9BFCD55643DC5C5D4A005A64EA0BEEC2E843_il2cpp_TypeInfo_var, L_11, L_12);
		// Instance.PointersReleased += pointersReleasedUnityEventsHandler;
		RuntimeObject* L_13 = TouchManager_get_Instance_m355FC1AAF7F91B86F174A4429E8A44ABE454E8E3(/*hidden argument*/NULL);
		EventHandler_1_t0363FEC12482AFCCEA69CEA150D1710057B94330 * L_14 = (EventHandler_1_t0363FEC12482AFCCEA69CEA150D1710057B94330 *)il2cpp_codegen_object_new(EventHandler_1_t0363FEC12482AFCCEA69CEA150D1710057B94330_il2cpp_TypeInfo_var);
		EventHandler_1__ctor_mDD490FECE01D6EF94516D572915E5E6291E1B7D1(L_14, __this, (intptr_t)((intptr_t)TouchManager_pointersReleasedUnityEventsHandler_m79811722367588FAA1AE65FB9B6F5250BCC92382_RuntimeMethod_var), /*hidden argument*/EventHandler_1__ctor_mDD490FECE01D6EF94516D572915E5E6291E1B7D1_RuntimeMethod_var);
		NullCheck(L_13);
		InterfaceActionInvoker1< EventHandler_1_t0363FEC12482AFCCEA69CEA150D1710057B94330 * >::Invoke(10 /* System.Void TouchScript.ITouchManager::add_PointersReleased(System.EventHandler`1<TouchScript.PointerEventArgs>) */, ITouchManager_t1E2C9BFCD55643DC5C5D4A005A64EA0BEEC2E843_il2cpp_TypeInfo_var, L_13, L_14);
		// Instance.PointersRemoved += pointersRemovedUnityEventsHandler;
		RuntimeObject* L_15 = TouchManager_get_Instance_m355FC1AAF7F91B86F174A4429E8A44ABE454E8E3(/*hidden argument*/NULL);
		EventHandler_1_t0363FEC12482AFCCEA69CEA150D1710057B94330 * L_16 = (EventHandler_1_t0363FEC12482AFCCEA69CEA150D1710057B94330 *)il2cpp_codegen_object_new(EventHandler_1_t0363FEC12482AFCCEA69CEA150D1710057B94330_il2cpp_TypeInfo_var);
		EventHandler_1__ctor_mDD490FECE01D6EF94516D572915E5E6291E1B7D1(L_16, __this, (intptr_t)((intptr_t)TouchManager_pointersRemovedUnityEventsHandler_mD8CB51C6894922EBAED67BD56F9920B78DA208FB_RuntimeMethod_var), /*hidden argument*/EventHandler_1__ctor_mDD490FECE01D6EF94516D572915E5E6291E1B7D1_RuntimeMethod_var);
		NullCheck(L_15);
		InterfaceActionInvoker1< EventHandler_1_t0363FEC12482AFCCEA69CEA150D1710057B94330 * >::Invoke(12 /* System.Void TouchScript.ITouchManager::add_PointersRemoved(System.EventHandler`1<TouchScript.PointerEventArgs>) */, ITouchManager_t1E2C9BFCD55643DC5C5D4A005A64EA0BEEC2E843_il2cpp_TypeInfo_var, L_15, L_16);
		// Instance.PointersCancelled += pointersCancelledUnityEventsHandler;
		RuntimeObject* L_17 = TouchManager_get_Instance_m355FC1AAF7F91B86F174A4429E8A44ABE454E8E3(/*hidden argument*/NULL);
		EventHandler_1_t0363FEC12482AFCCEA69CEA150D1710057B94330 * L_18 = (EventHandler_1_t0363FEC12482AFCCEA69CEA150D1710057B94330 *)il2cpp_codegen_object_new(EventHandler_1_t0363FEC12482AFCCEA69CEA150D1710057B94330_il2cpp_TypeInfo_var);
		EventHandler_1__ctor_mDD490FECE01D6EF94516D572915E5E6291E1B7D1(L_18, __this, (intptr_t)((intptr_t)TouchManager_pointersCancelledUnityEventsHandler_mFEB2D2BA0CA023BC04A9ADA27B2CFED543EA6A26_RuntimeMethod_var), /*hidden argument*/EventHandler_1__ctor_mDD490FECE01D6EF94516D572915E5E6291E1B7D1_RuntimeMethod_var);
		NullCheck(L_17);
		InterfaceActionInvoker1< EventHandler_1_t0363FEC12482AFCCEA69CEA150D1710057B94330 * >::Invoke(14 /* System.Void TouchScript.ITouchManager::add_PointersCancelled(System.EventHandler`1<TouchScript.PointerEventArgs>) */, ITouchManager_t1E2C9BFCD55643DC5C5D4A005A64EA0BEEC2E843_il2cpp_TypeInfo_var, L_17, L_18);
		// }
		return;
	}
}
// System.Void TouchScript.TouchManager::removeUnityEventsSubscriptions()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TouchManager_removeUnityEventsSubscriptions_m16B73F9CF5C8D19E6FB2776C07439A1F6A93F3FA (TouchManager_tE3A4C5ABD05B3B6C984CDBB1FA80479E1ACBE92B * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TouchManager_removeUnityEventsSubscriptions_m16B73F9CF5C8D19E6FB2776C07439A1F6A93F3FA_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// if (!Application.isPlaying) return;
		bool L_0 = Application_get_isPlaying_mF43B519662E7433DD90D883E5AE22EC3CFB65CA5(/*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_0008;
		}
	}
	{
		// if (!Application.isPlaying) return;
		return;
	}

IL_0008:
	{
		// if (Instance == null) return;
		IL2CPP_RUNTIME_CLASS_INIT(TouchManager_tE3A4C5ABD05B3B6C984CDBB1FA80479E1ACBE92B_il2cpp_TypeInfo_var);
		RuntimeObject* L_1 = TouchManager_get_Instance_m355FC1AAF7F91B86F174A4429E8A44ABE454E8E3(/*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_0010;
		}
	}
	{
		// if (Instance == null) return;
		return;
	}

IL_0010:
	{
		// Instance.FrameStarted -= frameStartedUnityEventsHandler;
		IL2CPP_RUNTIME_CLASS_INIT(TouchManager_tE3A4C5ABD05B3B6C984CDBB1FA80479E1ACBE92B_il2cpp_TypeInfo_var);
		RuntimeObject* L_2 = TouchManager_get_Instance_m355FC1AAF7F91B86F174A4429E8A44ABE454E8E3(/*hidden argument*/NULL);
		EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C * L_3 = (EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C *)il2cpp_codegen_object_new(EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C_il2cpp_TypeInfo_var);
		EventHandler__ctor_m497A2BCD46DB769EAFFDE919303FCAE226906B6F(L_3, __this, (intptr_t)((intptr_t)TouchManager_frameStartedUnityEventsHandler_m89C3B5C487CEB8878A3B71B698F8BB814713ECF0_RuntimeMethod_var), /*hidden argument*/NULL);
		NullCheck(L_2);
		InterfaceActionInvoker1< EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C * >::Invoke(1 /* System.Void TouchScript.ITouchManager::remove_FrameStarted(System.EventHandler) */, ITouchManager_t1E2C9BFCD55643DC5C5D4A005A64EA0BEEC2E843_il2cpp_TypeInfo_var, L_2, L_3);
		// Instance.FrameFinished -= frameFinishedUnityEventsHandler;
		RuntimeObject* L_4 = TouchManager_get_Instance_m355FC1AAF7F91B86F174A4429E8A44ABE454E8E3(/*hidden argument*/NULL);
		EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C * L_5 = (EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C *)il2cpp_codegen_object_new(EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C_il2cpp_TypeInfo_var);
		EventHandler__ctor_m497A2BCD46DB769EAFFDE919303FCAE226906B6F(L_5, __this, (intptr_t)((intptr_t)TouchManager_frameFinishedUnityEventsHandler_mBFE2FF2587FEE191DB70D58242692DC0690191ED_RuntimeMethod_var), /*hidden argument*/NULL);
		NullCheck(L_4);
		InterfaceActionInvoker1< EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C * >::Invoke(3 /* System.Void TouchScript.ITouchManager::remove_FrameFinished(System.EventHandler) */, ITouchManager_t1E2C9BFCD55643DC5C5D4A005A64EA0BEEC2E843_il2cpp_TypeInfo_var, L_4, L_5);
		// Instance.PointersAdded -= pointersAddedUnityEventsHandler;
		RuntimeObject* L_6 = TouchManager_get_Instance_m355FC1AAF7F91B86F174A4429E8A44ABE454E8E3(/*hidden argument*/NULL);
		EventHandler_1_t0363FEC12482AFCCEA69CEA150D1710057B94330 * L_7 = (EventHandler_1_t0363FEC12482AFCCEA69CEA150D1710057B94330 *)il2cpp_codegen_object_new(EventHandler_1_t0363FEC12482AFCCEA69CEA150D1710057B94330_il2cpp_TypeInfo_var);
		EventHandler_1__ctor_mDD490FECE01D6EF94516D572915E5E6291E1B7D1(L_7, __this, (intptr_t)((intptr_t)TouchManager_pointersAddedUnityEventsHandler_m05BDEF56AC60141C69CFECD9324EAF9DCCBA3859_RuntimeMethod_var), /*hidden argument*/EventHandler_1__ctor_mDD490FECE01D6EF94516D572915E5E6291E1B7D1_RuntimeMethod_var);
		NullCheck(L_6);
		InterfaceActionInvoker1< EventHandler_1_t0363FEC12482AFCCEA69CEA150D1710057B94330 * >::Invoke(5 /* System.Void TouchScript.ITouchManager::remove_PointersAdded(System.EventHandler`1<TouchScript.PointerEventArgs>) */, ITouchManager_t1E2C9BFCD55643DC5C5D4A005A64EA0BEEC2E843_il2cpp_TypeInfo_var, L_6, L_7);
		// Instance.PointersUpdated -= pointersUpdatedUnityEventsHandler;
		RuntimeObject* L_8 = TouchManager_get_Instance_m355FC1AAF7F91B86F174A4429E8A44ABE454E8E3(/*hidden argument*/NULL);
		EventHandler_1_t0363FEC12482AFCCEA69CEA150D1710057B94330 * L_9 = (EventHandler_1_t0363FEC12482AFCCEA69CEA150D1710057B94330 *)il2cpp_codegen_object_new(EventHandler_1_t0363FEC12482AFCCEA69CEA150D1710057B94330_il2cpp_TypeInfo_var);
		EventHandler_1__ctor_mDD490FECE01D6EF94516D572915E5E6291E1B7D1(L_9, __this, (intptr_t)((intptr_t)TouchManager_pointersUpdatedUnityEventsHandler_mDD4D2BBAA16EB45E6473E25CAB802E6B1C02C719_RuntimeMethod_var), /*hidden argument*/EventHandler_1__ctor_mDD490FECE01D6EF94516D572915E5E6291E1B7D1_RuntimeMethod_var);
		NullCheck(L_8);
		InterfaceActionInvoker1< EventHandler_1_t0363FEC12482AFCCEA69CEA150D1710057B94330 * >::Invoke(7 /* System.Void TouchScript.ITouchManager::remove_PointersUpdated(System.EventHandler`1<TouchScript.PointerEventArgs>) */, ITouchManager_t1E2C9BFCD55643DC5C5D4A005A64EA0BEEC2E843_il2cpp_TypeInfo_var, L_8, L_9);
		// Instance.PointersPressed -= pointersPressedUnityEventsHandler;
		RuntimeObject* L_10 = TouchManager_get_Instance_m355FC1AAF7F91B86F174A4429E8A44ABE454E8E3(/*hidden argument*/NULL);
		EventHandler_1_t0363FEC12482AFCCEA69CEA150D1710057B94330 * L_11 = (EventHandler_1_t0363FEC12482AFCCEA69CEA150D1710057B94330 *)il2cpp_codegen_object_new(EventHandler_1_t0363FEC12482AFCCEA69CEA150D1710057B94330_il2cpp_TypeInfo_var);
		EventHandler_1__ctor_mDD490FECE01D6EF94516D572915E5E6291E1B7D1(L_11, __this, (intptr_t)((intptr_t)TouchManager_pointersPressedUnityEventsHandler_m82F19BBA6DFDA4EA2767F2F51EA100083F5C2F84_RuntimeMethod_var), /*hidden argument*/EventHandler_1__ctor_mDD490FECE01D6EF94516D572915E5E6291E1B7D1_RuntimeMethod_var);
		NullCheck(L_10);
		InterfaceActionInvoker1< EventHandler_1_t0363FEC12482AFCCEA69CEA150D1710057B94330 * >::Invoke(9 /* System.Void TouchScript.ITouchManager::remove_PointersPressed(System.EventHandler`1<TouchScript.PointerEventArgs>) */, ITouchManager_t1E2C9BFCD55643DC5C5D4A005A64EA0BEEC2E843_il2cpp_TypeInfo_var, L_10, L_11);
		// Instance.PointersReleased -= pointersReleasedUnityEventsHandler;
		RuntimeObject* L_12 = TouchManager_get_Instance_m355FC1AAF7F91B86F174A4429E8A44ABE454E8E3(/*hidden argument*/NULL);
		EventHandler_1_t0363FEC12482AFCCEA69CEA150D1710057B94330 * L_13 = (EventHandler_1_t0363FEC12482AFCCEA69CEA150D1710057B94330 *)il2cpp_codegen_object_new(EventHandler_1_t0363FEC12482AFCCEA69CEA150D1710057B94330_il2cpp_TypeInfo_var);
		EventHandler_1__ctor_mDD490FECE01D6EF94516D572915E5E6291E1B7D1(L_13, __this, (intptr_t)((intptr_t)TouchManager_pointersReleasedUnityEventsHandler_m79811722367588FAA1AE65FB9B6F5250BCC92382_RuntimeMethod_var), /*hidden argument*/EventHandler_1__ctor_mDD490FECE01D6EF94516D572915E5E6291E1B7D1_RuntimeMethod_var);
		NullCheck(L_12);
		InterfaceActionInvoker1< EventHandler_1_t0363FEC12482AFCCEA69CEA150D1710057B94330 * >::Invoke(11 /* System.Void TouchScript.ITouchManager::remove_PointersReleased(System.EventHandler`1<TouchScript.PointerEventArgs>) */, ITouchManager_t1E2C9BFCD55643DC5C5D4A005A64EA0BEEC2E843_il2cpp_TypeInfo_var, L_12, L_13);
		// Instance.PointersRemoved -= pointersRemovedUnityEventsHandler;
		RuntimeObject* L_14 = TouchManager_get_Instance_m355FC1AAF7F91B86F174A4429E8A44ABE454E8E3(/*hidden argument*/NULL);
		EventHandler_1_t0363FEC12482AFCCEA69CEA150D1710057B94330 * L_15 = (EventHandler_1_t0363FEC12482AFCCEA69CEA150D1710057B94330 *)il2cpp_codegen_object_new(EventHandler_1_t0363FEC12482AFCCEA69CEA150D1710057B94330_il2cpp_TypeInfo_var);
		EventHandler_1__ctor_mDD490FECE01D6EF94516D572915E5E6291E1B7D1(L_15, __this, (intptr_t)((intptr_t)TouchManager_pointersRemovedUnityEventsHandler_mD8CB51C6894922EBAED67BD56F9920B78DA208FB_RuntimeMethod_var), /*hidden argument*/EventHandler_1__ctor_mDD490FECE01D6EF94516D572915E5E6291E1B7D1_RuntimeMethod_var);
		NullCheck(L_14);
		InterfaceActionInvoker1< EventHandler_1_t0363FEC12482AFCCEA69CEA150D1710057B94330 * >::Invoke(13 /* System.Void TouchScript.ITouchManager::remove_PointersRemoved(System.EventHandler`1<TouchScript.PointerEventArgs>) */, ITouchManager_t1E2C9BFCD55643DC5C5D4A005A64EA0BEEC2E843_il2cpp_TypeInfo_var, L_14, L_15);
		// Instance.PointersCancelled -= pointersCancelledUnityEventsHandler;
		RuntimeObject* L_16 = TouchManager_get_Instance_m355FC1AAF7F91B86F174A4429E8A44ABE454E8E3(/*hidden argument*/NULL);
		EventHandler_1_t0363FEC12482AFCCEA69CEA150D1710057B94330 * L_17 = (EventHandler_1_t0363FEC12482AFCCEA69CEA150D1710057B94330 *)il2cpp_codegen_object_new(EventHandler_1_t0363FEC12482AFCCEA69CEA150D1710057B94330_il2cpp_TypeInfo_var);
		EventHandler_1__ctor_mDD490FECE01D6EF94516D572915E5E6291E1B7D1(L_17, __this, (intptr_t)((intptr_t)TouchManager_pointersCancelledUnityEventsHandler_mFEB2D2BA0CA023BC04A9ADA27B2CFED543EA6A26_RuntimeMethod_var), /*hidden argument*/EventHandler_1__ctor_mDD490FECE01D6EF94516D572915E5E6291E1B7D1_RuntimeMethod_var);
		NullCheck(L_16);
		InterfaceActionInvoker1< EventHandler_1_t0363FEC12482AFCCEA69CEA150D1710057B94330 * >::Invoke(15 /* System.Void TouchScript.ITouchManager::remove_PointersCancelled(System.EventHandler`1<TouchScript.PointerEventArgs>) */, ITouchManager_t1E2C9BFCD55643DC5C5D4A005A64EA0BEEC2E843_il2cpp_TypeInfo_var, L_16, L_17);
		// }
		return;
	}
}
// System.Void TouchScript.TouchManager::pointersAddedUnityEventsHandler(System.Object,TouchScript.PointerEventArgs)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TouchManager_pointersAddedUnityEventsHandler_m05BDEF56AC60141C69CFECD9324EAF9DCCBA3859 (TouchManager_tE3A4C5ABD05B3B6C984CDBB1FA80479E1ACBE92B * __this, RuntimeObject * ___sender0, PointerEventArgs_t60C9291910F961213770A1302F3CE7CC5A580106 * ___e1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TouchManager_pointersAddedUnityEventsHandler_m05BDEF56AC60141C69CFECD9324EAF9DCCBA3859_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// OnPointersAdd.Invoke(e.Pointers);
		PointerEvent_t46D6262BF503769AA7E077C49BB94DA4B83061DA * L_0 = __this->get_OnPointersAdd_11();
		PointerEventArgs_t60C9291910F961213770A1302F3CE7CC5A580106 * L_1 = ___e1;
		NullCheck(L_1);
		RuntimeObject* L_2 = PointerEventArgs_get_Pointers_m16B91F1D5BE47012CF6C4BC0BC3F53691525FC41_inline(L_1, /*hidden argument*/NULL);
		NullCheck(L_0);
		UnityEvent_1_Invoke_mF7D14A026E6DC7B30CDB2E16A114A79B946C7869(L_0, L_2, /*hidden argument*/UnityEvent_1_Invoke_mF7D14A026E6DC7B30CDB2E16A114A79B946C7869_RuntimeMethod_var);
		// }
		return;
	}
}
// System.Void TouchScript.TouchManager::pointersUpdatedUnityEventsHandler(System.Object,TouchScript.PointerEventArgs)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TouchManager_pointersUpdatedUnityEventsHandler_mDD4D2BBAA16EB45E6473E25CAB802E6B1C02C719 (TouchManager_tE3A4C5ABD05B3B6C984CDBB1FA80479E1ACBE92B * __this, RuntimeObject * ___sender0, PointerEventArgs_t60C9291910F961213770A1302F3CE7CC5A580106 * ___e1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TouchManager_pointersUpdatedUnityEventsHandler_mDD4D2BBAA16EB45E6473E25CAB802E6B1C02C719_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// OnPointersUpdate.Invoke(e.Pointers);
		PointerEvent_t46D6262BF503769AA7E077C49BB94DA4B83061DA * L_0 = __this->get_OnPointersUpdate_12();
		PointerEventArgs_t60C9291910F961213770A1302F3CE7CC5A580106 * L_1 = ___e1;
		NullCheck(L_1);
		RuntimeObject* L_2 = PointerEventArgs_get_Pointers_m16B91F1D5BE47012CF6C4BC0BC3F53691525FC41_inline(L_1, /*hidden argument*/NULL);
		NullCheck(L_0);
		UnityEvent_1_Invoke_mF7D14A026E6DC7B30CDB2E16A114A79B946C7869(L_0, L_2, /*hidden argument*/UnityEvent_1_Invoke_mF7D14A026E6DC7B30CDB2E16A114A79B946C7869_RuntimeMethod_var);
		// }
		return;
	}
}
// System.Void TouchScript.TouchManager::pointersPressedUnityEventsHandler(System.Object,TouchScript.PointerEventArgs)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TouchManager_pointersPressedUnityEventsHandler_m82F19BBA6DFDA4EA2767F2F51EA100083F5C2F84 (TouchManager_tE3A4C5ABD05B3B6C984CDBB1FA80479E1ACBE92B * __this, RuntimeObject * ___sender0, PointerEventArgs_t60C9291910F961213770A1302F3CE7CC5A580106 * ___e1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TouchManager_pointersPressedUnityEventsHandler_m82F19BBA6DFDA4EA2767F2F51EA100083F5C2F84_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// OnPointersPress.Invoke(e.Pointers);
		PointerEvent_t46D6262BF503769AA7E077C49BB94DA4B83061DA * L_0 = __this->get_OnPointersPress_13();
		PointerEventArgs_t60C9291910F961213770A1302F3CE7CC5A580106 * L_1 = ___e1;
		NullCheck(L_1);
		RuntimeObject* L_2 = PointerEventArgs_get_Pointers_m16B91F1D5BE47012CF6C4BC0BC3F53691525FC41_inline(L_1, /*hidden argument*/NULL);
		NullCheck(L_0);
		UnityEvent_1_Invoke_mF7D14A026E6DC7B30CDB2E16A114A79B946C7869(L_0, L_2, /*hidden argument*/UnityEvent_1_Invoke_mF7D14A026E6DC7B30CDB2E16A114A79B946C7869_RuntimeMethod_var);
		// }
		return;
	}
}
// System.Void TouchScript.TouchManager::pointersReleasedUnityEventsHandler(System.Object,TouchScript.PointerEventArgs)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TouchManager_pointersReleasedUnityEventsHandler_m79811722367588FAA1AE65FB9B6F5250BCC92382 (TouchManager_tE3A4C5ABD05B3B6C984CDBB1FA80479E1ACBE92B * __this, RuntimeObject * ___sender0, PointerEventArgs_t60C9291910F961213770A1302F3CE7CC5A580106 * ___e1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TouchManager_pointersReleasedUnityEventsHandler_m79811722367588FAA1AE65FB9B6F5250BCC92382_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// OnPointersRelease.Invoke(e.Pointers);
		PointerEvent_t46D6262BF503769AA7E077C49BB94DA4B83061DA * L_0 = __this->get_OnPointersRelease_14();
		PointerEventArgs_t60C9291910F961213770A1302F3CE7CC5A580106 * L_1 = ___e1;
		NullCheck(L_1);
		RuntimeObject* L_2 = PointerEventArgs_get_Pointers_m16B91F1D5BE47012CF6C4BC0BC3F53691525FC41_inline(L_1, /*hidden argument*/NULL);
		NullCheck(L_0);
		UnityEvent_1_Invoke_mF7D14A026E6DC7B30CDB2E16A114A79B946C7869(L_0, L_2, /*hidden argument*/UnityEvent_1_Invoke_mF7D14A026E6DC7B30CDB2E16A114A79B946C7869_RuntimeMethod_var);
		// }
		return;
	}
}
// System.Void TouchScript.TouchManager::pointersRemovedUnityEventsHandler(System.Object,TouchScript.PointerEventArgs)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TouchManager_pointersRemovedUnityEventsHandler_mD8CB51C6894922EBAED67BD56F9920B78DA208FB (TouchManager_tE3A4C5ABD05B3B6C984CDBB1FA80479E1ACBE92B * __this, RuntimeObject * ___sender0, PointerEventArgs_t60C9291910F961213770A1302F3CE7CC5A580106 * ___e1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TouchManager_pointersRemovedUnityEventsHandler_mD8CB51C6894922EBAED67BD56F9920B78DA208FB_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// OnPointersRemove.Invoke(e.Pointers);
		PointerEvent_t46D6262BF503769AA7E077C49BB94DA4B83061DA * L_0 = __this->get_OnPointersRemove_15();
		PointerEventArgs_t60C9291910F961213770A1302F3CE7CC5A580106 * L_1 = ___e1;
		NullCheck(L_1);
		RuntimeObject* L_2 = PointerEventArgs_get_Pointers_m16B91F1D5BE47012CF6C4BC0BC3F53691525FC41_inline(L_1, /*hidden argument*/NULL);
		NullCheck(L_0);
		UnityEvent_1_Invoke_mF7D14A026E6DC7B30CDB2E16A114A79B946C7869(L_0, L_2, /*hidden argument*/UnityEvent_1_Invoke_mF7D14A026E6DC7B30CDB2E16A114A79B946C7869_RuntimeMethod_var);
		// }
		return;
	}
}
// System.Void TouchScript.TouchManager::pointersCancelledUnityEventsHandler(System.Object,TouchScript.PointerEventArgs)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TouchManager_pointersCancelledUnityEventsHandler_mFEB2D2BA0CA023BC04A9ADA27B2CFED543EA6A26 (TouchManager_tE3A4C5ABD05B3B6C984CDBB1FA80479E1ACBE92B * __this, RuntimeObject * ___sender0, PointerEventArgs_t60C9291910F961213770A1302F3CE7CC5A580106 * ___e1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TouchManager_pointersCancelledUnityEventsHandler_mFEB2D2BA0CA023BC04A9ADA27B2CFED543EA6A26_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// OnPointersCancel.Invoke(e.Pointers);
		PointerEvent_t46D6262BF503769AA7E077C49BB94DA4B83061DA * L_0 = __this->get_OnPointersCancel_16();
		PointerEventArgs_t60C9291910F961213770A1302F3CE7CC5A580106 * L_1 = ___e1;
		NullCheck(L_1);
		RuntimeObject* L_2 = PointerEventArgs_get_Pointers_m16B91F1D5BE47012CF6C4BC0BC3F53691525FC41_inline(L_1, /*hidden argument*/NULL);
		NullCheck(L_0);
		UnityEvent_1_Invoke_mF7D14A026E6DC7B30CDB2E16A114A79B946C7869(L_0, L_2, /*hidden argument*/UnityEvent_1_Invoke_mF7D14A026E6DC7B30CDB2E16A114A79B946C7869_RuntimeMethod_var);
		// }
		return;
	}
}
// System.Void TouchScript.TouchManager::frameStartedUnityEventsHandler(System.Object,System.EventArgs)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TouchManager_frameStartedUnityEventsHandler_m89C3B5C487CEB8878A3B71B698F8BB814713ECF0 (TouchManager_tE3A4C5ABD05B3B6C984CDBB1FA80479E1ACBE92B * __this, RuntimeObject * ___sender0, EventArgs_t8E6CA180BE0E56674C6407011A94BAF7C757352E * ___e1, const RuntimeMethod* method)
{
	{
		// OnFrameStart.Invoke();
		FrameEvent_tEBA65ABA56D36C692FB1A49D6929A473CA7A3817 * L_0 = __this->get_OnFrameStart_9();
		NullCheck(L_0);
		UnityEvent_Invoke_mB2FA1C76256FE34D5E7F84ABE528AC61CE8A0325(L_0, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void TouchScript.TouchManager::frameFinishedUnityEventsHandler(System.Object,System.EventArgs)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TouchManager_frameFinishedUnityEventsHandler_mBFE2FF2587FEE191DB70D58242692DC0690191ED (TouchManager_tE3A4C5ABD05B3B6C984CDBB1FA80479E1ACBE92B * __this, RuntimeObject * ___sender0, EventArgs_t8E6CA180BE0E56674C6407011A94BAF7C757352E * ___e1, const RuntimeMethod* method)
{
	{
		// OnFrameFinish.Invoke();
		FrameEvent_tEBA65ABA56D36C692FB1A49D6929A473CA7A3817 * L_0 = __this->get_OnFrameFinish_10();
		NullCheck(L_0);
		UnityEvent_Invoke_mB2FA1C76256FE34D5E7F84ABE528AC61CE8A0325(L_0, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void TouchScript.TouchManager::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TouchManager__ctor_m54F25692CE236EEBEEA41BD3E445AB6042BA35E9 (TouchManager_tE3A4C5ABD05B3B6C984CDBB1FA80479E1ACBE92B * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TouchManager__ctor_m54F25692CE236EEBEEA41BD3E445AB6042BA35E9_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// public FrameEvent OnFrameStart = new FrameEvent();
		FrameEvent_tEBA65ABA56D36C692FB1A49D6929A473CA7A3817 * L_0 = (FrameEvent_tEBA65ABA56D36C692FB1A49D6929A473CA7A3817 *)il2cpp_codegen_object_new(FrameEvent_tEBA65ABA56D36C692FB1A49D6929A473CA7A3817_il2cpp_TypeInfo_var);
		FrameEvent__ctor_mC2B90B221D4FECA515E3401E5611A8E7510BF817(L_0, /*hidden argument*/NULL);
		__this->set_OnFrameStart_9(L_0);
		// public FrameEvent OnFrameFinish = new FrameEvent();
		FrameEvent_tEBA65ABA56D36C692FB1A49D6929A473CA7A3817 * L_1 = (FrameEvent_tEBA65ABA56D36C692FB1A49D6929A473CA7A3817 *)il2cpp_codegen_object_new(FrameEvent_tEBA65ABA56D36C692FB1A49D6929A473CA7A3817_il2cpp_TypeInfo_var);
		FrameEvent__ctor_mC2B90B221D4FECA515E3401E5611A8E7510BF817(L_1, /*hidden argument*/NULL);
		__this->set_OnFrameFinish_10(L_1);
		// public PointerEvent OnPointersAdd = new PointerEvent();
		PointerEvent_t46D6262BF503769AA7E077C49BB94DA4B83061DA * L_2 = (PointerEvent_t46D6262BF503769AA7E077C49BB94DA4B83061DA *)il2cpp_codegen_object_new(PointerEvent_t46D6262BF503769AA7E077C49BB94DA4B83061DA_il2cpp_TypeInfo_var);
		PointerEvent__ctor_m63D4E009DE7DACDA202FD81E36A40E7D04F387C2(L_2, /*hidden argument*/NULL);
		__this->set_OnPointersAdd_11(L_2);
		// public PointerEvent OnPointersUpdate = new PointerEvent();
		PointerEvent_t46D6262BF503769AA7E077C49BB94DA4B83061DA * L_3 = (PointerEvent_t46D6262BF503769AA7E077C49BB94DA4B83061DA *)il2cpp_codegen_object_new(PointerEvent_t46D6262BF503769AA7E077C49BB94DA4B83061DA_il2cpp_TypeInfo_var);
		PointerEvent__ctor_m63D4E009DE7DACDA202FD81E36A40E7D04F387C2(L_3, /*hidden argument*/NULL);
		__this->set_OnPointersUpdate_12(L_3);
		// public PointerEvent OnPointersPress = new PointerEvent();
		PointerEvent_t46D6262BF503769AA7E077C49BB94DA4B83061DA * L_4 = (PointerEvent_t46D6262BF503769AA7E077C49BB94DA4B83061DA *)il2cpp_codegen_object_new(PointerEvent_t46D6262BF503769AA7E077C49BB94DA4B83061DA_il2cpp_TypeInfo_var);
		PointerEvent__ctor_m63D4E009DE7DACDA202FD81E36A40E7D04F387C2(L_4, /*hidden argument*/NULL);
		__this->set_OnPointersPress_13(L_4);
		// public PointerEvent OnPointersRelease = new PointerEvent();
		PointerEvent_t46D6262BF503769AA7E077C49BB94DA4B83061DA * L_5 = (PointerEvent_t46D6262BF503769AA7E077C49BB94DA4B83061DA *)il2cpp_codegen_object_new(PointerEvent_t46D6262BF503769AA7E077C49BB94DA4B83061DA_il2cpp_TypeInfo_var);
		PointerEvent__ctor_m63D4E009DE7DACDA202FD81E36A40E7D04F387C2(L_5, /*hidden argument*/NULL);
		__this->set_OnPointersRelease_14(L_5);
		// public PointerEvent OnPointersRemove = new PointerEvent();
		PointerEvent_t46D6262BF503769AA7E077C49BB94DA4B83061DA * L_6 = (PointerEvent_t46D6262BF503769AA7E077C49BB94DA4B83061DA *)il2cpp_codegen_object_new(PointerEvent_t46D6262BF503769AA7E077C49BB94DA4B83061DA_il2cpp_TypeInfo_var);
		PointerEvent__ctor_m63D4E009DE7DACDA202FD81E36A40E7D04F387C2(L_6, /*hidden argument*/NULL);
		__this->set_OnPointersRemove_15(L_6);
		// public PointerEvent OnPointersCancel = new PointerEvent();
		PointerEvent_t46D6262BF503769AA7E077C49BB94DA4B83061DA * L_7 = (PointerEvent_t46D6262BF503769AA7E077C49BB94DA4B83061DA *)il2cpp_codegen_object_new(PointerEvent_t46D6262BF503769AA7E077C49BB94DA4B83061DA_il2cpp_TypeInfo_var);
		PointerEvent__ctor_m63D4E009DE7DACDA202FD81E36A40E7D04F387C2(L_7, /*hidden argument*/NULL);
		__this->set_OnPointersCancel_16(L_7);
		// private bool basicEditor = true;
		__this->set_basicEditor_17((bool)1);
		// private bool shouldCreateCameraLayer = true;
		__this->set_shouldCreateCameraLayer_19((bool)1);
		// private bool shouldCreateStandardInput = true;
		__this->set_shouldCreateStandardInput_20((bool)1);
		// private MessageType sendMessageEvents = MessageType.PointersPressed | MessageType.PointersCancelled |
		//                                         MessageType.PointersReleased | MessageType.PointersUpdated |
		//                                         MessageType.PointersAdded | MessageType.PointersRemoved;
		__this->set_sendMessageEvents_22(((int32_t)252));
		// private List<TouchLayer> layers = new List<TouchLayer>();
		List_1_tACFDA2C40746A89629E9D8D22CD4726B64D7C856 * L_8 = (List_1_tACFDA2C40746A89629E9D8D22CD4726B64D7C856 *)il2cpp_codegen_object_new(List_1_tACFDA2C40746A89629E9D8D22CD4726B64D7C856_il2cpp_TypeInfo_var);
		List_1__ctor_mA49956FD2D00F5266DDF54D252195E6D951B3B56(L_8, /*hidden argument*/List_1__ctor_mA49956FD2D00F5266DDF54D252195E6D951B3B56_RuntimeMethod_var);
		__this->set_layers_25(L_8);
		DebuggableMonoBehaviour__ctor_m2721B121281EA6FCDA46C369E88D5A282D6FD065(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void TouchScript.TouchManager::.cctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TouchManager__cctor_mA1F542857C7735E0DC46820A360BF8B2A9E48366 (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TouchManager__cctor_mA1F542857C7735E0DC46820A360BF8B2A9E48366_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// public static readonly Vector2 INVALID_POSITION = new Vector2(float.NaN, float.NaN);
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_0;
		memset((&L_0), 0, sizeof(L_0));
		Vector2__ctor_mEE8FB117AB1F8DB746FB8B3EB4C0DA3BF2A230D0((&L_0), (std::numeric_limits<float>::quiet_NaN()), (std::numeric_limits<float>::quiet_NaN()), /*hidden argument*/NULL);
		((TouchManager_tE3A4C5ABD05B3B6C984CDBB1FA80479E1ACBE92B_StaticFields*)il2cpp_codegen_static_fields_for(TouchManager_tE3A4C5ABD05B3B6C984CDBB1FA80479E1ACBE92B_il2cpp_TypeInfo_var))->set_INVALID_POSITION_6(L_0);
		// public static readonly Version VERSION = new Version(9, 0);
		Version_tDBE6876C59B6F56D4F8CAA03851177ABC6FE0DFD * L_1 = (Version_tDBE6876C59B6F56D4F8CAA03851177ABC6FE0DFD *)il2cpp_codegen_object_new(Version_tDBE6876C59B6F56D4F8CAA03851177ABC6FE0DFD_il2cpp_TypeInfo_var);
		Version__ctor_mC2880C190E158700B0C114D4CC921C0D240DAA9C(L_1, ((int32_t)9), 0, /*hidden argument*/NULL);
		((TouchManager_tE3A4C5ABD05B3B6C984CDBB1FA80479E1ACBE92B_StaticFields*)il2cpp_codegen_static_fields_for(TouchManager_tE3A4C5ABD05B3B6C984CDBB1FA80479E1ACBE92B_il2cpp_TypeInfo_var))->set_VERSION_7(L_1);
		// public static readonly string VERSION_SUFFIX = "";
		((TouchManager_tE3A4C5ABD05B3B6C984CDBB1FA80479E1ACBE92B_StaticFields*)il2cpp_codegen_static_fields_for(TouchManager_tE3A4C5ABD05B3B6C984CDBB1FA80479E1ACBE92B_il2cpp_TypeInfo_var))->set_VERSION_SUFFIX_8(_stringLiteralDA39A3EE5E6B4B0D3255BFEF95601890AFD80709);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void TouchScript.TouchManager_FrameEvent::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void FrameEvent__ctor_mC2B90B221D4FECA515E3401E5611A8E7510BF817 (FrameEvent_tEBA65ABA56D36C692FB1A49D6929A473CA7A3817 * __this, const RuntimeMethod* method)
{
	{
		UnityEvent__ctor_m2F8C02F28E289CA65598FF4FA8EAB84D955FF028(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void TouchScript.TouchManager_PointerEvent::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PointerEvent__ctor_m63D4E009DE7DACDA202FD81E36A40E7D04F387C2 (PointerEvent_t46D6262BF503769AA7E077C49BB94DA4B83061DA * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PointerEvent__ctor_m63D4E009DE7DACDA202FD81E36A40E7D04F387C2_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		UnityEvent_1__ctor_m3C716A57D64E39CC8286EFD89A14C9361624E1FA(__this, /*hidden argument*/UnityEvent_1__ctor_m3C716A57D64E39CC8286EFD89A14C9361624E1FA_RuntimeMethod_var);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void TouchScript.Utils.Attributes.NullToggleAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void NullToggleAttribute__ctor_m35A491114899DA79F9BE73EA8B91B70FFC5041F0 (NullToggleAttribute_tB95470BD28EF41A9B6EF4CC642D5D293DA28A5DF * __this, const RuntimeMethod* method)
{
	{
		PropertyAttribute__ctor_m7F5C473F39D5601486C1127DA0D52F2DC293FC35(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void TouchScript.Utils.Attributes.ToggleLeftAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ToggleLeftAttribute__ctor_mF0FF749A04925D5C49276E1DEE37922A4087A4E3 (ToggleLeftAttribute_tBDFA12F6A17E62E0928F69045C9267C34DD8AAC1 * __this, const RuntimeMethod* method)
{
	{
		PropertyAttribute__ctor_m7F5C473F39D5601486C1127DA0D52F2DC293FC35(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void TouchScript.Utils.BinaryUtils::ToBinaryString(System.UInt32,System.Text.StringBuilder,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void BinaryUtils_ToBinaryString_mCB55E824E5042B4C35CD8EF55C3090C73D1B3FEC (uint32_t ___value0, StringBuilder_t * ___builder1, int32_t ___digits2, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	StringBuilder_t * G_B3_0 = NULL;
	StringBuilder_t * G_B2_0 = NULL;
	int32_t G_B4_0 = 0;
	StringBuilder_t * G_B4_1 = NULL;
	{
		// int i = digits - 1;
		int32_t L_0 = ___digits2;
		V_0 = ((int32_t)il2cpp_codegen_subtract((int32_t)L_0, (int32_t)1));
		goto IL_0021;
	}

IL_0006:
	{
		// builder.Append((value & (1 << i)) == 0 ? 0 : 1);
		StringBuilder_t * L_1 = ___builder1;
		uint32_t L_2 = ___value0;
		int32_t L_3 = V_0;
		G_B2_0 = L_1;
		if (!((int64_t)((int64_t)(((int64_t)((uint64_t)L_2)))&(int64_t)(((int64_t)((int64_t)((int32_t)((int32_t)1<<(int32_t)((int32_t)((int32_t)L_3&(int32_t)((int32_t)31)))))))))))
		{
			G_B3_0 = L_1;
			goto IL_0016;
		}
	}
	{
		G_B4_0 = 1;
		G_B4_1 = G_B2_0;
		goto IL_0017;
	}

IL_0016:
	{
		G_B4_0 = 0;
		G_B4_1 = G_B3_0;
	}

IL_0017:
	{
		NullCheck(G_B4_1);
		StringBuilder_Append_m85874CFF9E4B152DB2A91936C6F2CA3E9B1EFF84(G_B4_1, G_B4_0, /*hidden argument*/NULL);
		// i--;
		int32_t L_4 = V_0;
		V_0 = ((int32_t)il2cpp_codegen_subtract((int32_t)L_4, (int32_t)1));
	}

IL_0021:
	{
		// while (i >= 0)
		int32_t L_5 = V_0;
		if ((((int32_t)L_5) >= ((int32_t)0)))
		{
			goto IL_0006;
		}
	}
	{
		// }
		return;
	}
}
// System.String TouchScript.Utils.BinaryUtils::ToBinaryString(System.UInt32,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* BinaryUtils_ToBinaryString_m841313E6549570892EDB2B9C9421BB3C54F64F03 (uint32_t ___value0, int32_t ___digits1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (BinaryUtils_ToBinaryString_m841313E6549570892EDB2B9C9421BB3C54F64F03_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	StringBuilder_t * V_0 = NULL;
	{
		// var sb = new StringBuilder(digits);
		int32_t L_0 = ___digits1;
		StringBuilder_t * L_1 = (StringBuilder_t *)il2cpp_codegen_object_new(StringBuilder_t_il2cpp_TypeInfo_var);
		StringBuilder__ctor_m1C0F2D97B838537A2D0F64033AE4EF02D150A956(L_1, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		// ToBinaryString(value, sb, digits);
		uint32_t L_2 = ___value0;
		StringBuilder_t * L_3 = V_0;
		int32_t L_4 = ___digits1;
		BinaryUtils_ToBinaryString_mCB55E824E5042B4C35CD8EF55C3090C73D1B3FEC(L_2, L_3, L_4, /*hidden argument*/NULL);
		// return sb.ToString();
		StringBuilder_t * L_5 = V_0;
		NullCheck(L_5);
		String_t* L_6 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_5);
		return L_6;
	}
}
// System.UInt32 TouchScript.Utils.BinaryUtils::ToBinaryMask(System.Collections.Generic.IEnumerable`1<System.Boolean>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR uint32_t BinaryUtils_ToBinaryMask_mB486631A929B8755B239F3BD07EBD208F3F7E8AE (RuntimeObject* ___collection0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (BinaryUtils_ToBinaryMask_mB486631A929B8755B239F3BD07EBD208F3F7E8AE_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	uint32_t V_0 = 0;
	int32_t V_1 = 0;
	RuntimeObject* V_2 = NULL;
	Exception_t * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	void* __leave_targets_storage = alloca(sizeof(int32_t) * 2);
	il2cpp::utils::LeaveTargetStack __leave_targets(__leave_targets_storage);
	NO_UNUSED_WARNING (__leave_targets);
	{
		// uint mask = 0;
		V_0 = 0;
		// var count = 0;
		V_1 = 0;
		// foreach (bool value in collection)
		RuntimeObject* L_0 = ___collection0;
		NullCheck(L_0);
		RuntimeObject* L_1 = InterfaceFuncInvoker0< RuntimeObject* >::Invoke(0 /* System.Collections.Generic.IEnumerator`1<!0> System.Collections.Generic.IEnumerable`1<System.Boolean>::GetEnumerator() */, IEnumerable_1_tC6EB38083371E9C00CC441AE4ADB4A3100BEB721_il2cpp_TypeInfo_var, L_0);
		V_2 = L_1;
	}

IL_000b:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0029;
		}

IL_000d:
		{
			// foreach (bool value in collection)
			RuntimeObject* L_2 = V_2;
			NullCheck(L_2);
			bool L_3 = InterfaceFuncInvoker0< bool >::Invoke(0 /* !0 System.Collections.Generic.IEnumerator`1<System.Boolean>::get_Current() */, IEnumerator_1_tE205FF055203E22F3B44CD6ABFFED46565288600_il2cpp_TypeInfo_var, L_2);
			// if (value) mask |= (uint) (1 << count);
			if (!L_3)
			{
				goto IL_001e;
			}
		}

IL_0015:
		{
			// if (value) mask |= (uint) (1 << count);
			uint32_t L_4 = V_0;
			int32_t L_5 = V_1;
			V_0 = ((int32_t)((int32_t)L_4|(int32_t)((int32_t)((int32_t)1<<(int32_t)((int32_t)((int32_t)L_5&(int32_t)((int32_t)31)))))));
		}

IL_001e:
		{
			// if (++count >= 32) break;
			int32_t L_6 = V_1;
			int32_t L_7 = ((int32_t)il2cpp_codegen_add((int32_t)L_6, (int32_t)1));
			V_1 = L_7;
			if ((((int32_t)L_7) < ((int32_t)((int32_t)32))))
			{
				goto IL_0029;
			}
		}

IL_0027:
		{
			// if (++count >= 32) break;
			IL2CPP_LEAVE(0x3D, FINALLY_0033);
		}

IL_0029:
		{
			// foreach (bool value in collection)
			RuntimeObject* L_8 = V_2;
			NullCheck(L_8);
			bool L_9 = InterfaceFuncInvoker0< bool >::Invoke(0 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t8789118187258CC88B77AFAC6315B5AF87D3E18A_il2cpp_TypeInfo_var, L_8);
			if (L_9)
			{
				goto IL_000d;
			}
		}

IL_0031:
		{
			IL2CPP_LEAVE(0x3D, FINALLY_0033);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t *)e.ex;
		goto FINALLY_0033;
	}

FINALLY_0033:
	{ // begin finally (depth: 1)
		{
			RuntimeObject* L_10 = V_2;
			if (!L_10)
			{
				goto IL_003c;
			}
		}

IL_0036:
		{
			RuntimeObject* L_11 = V_2;
			NullCheck(L_11);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t7218B22548186B208D65EA5B7870503810A2D15A_il2cpp_TypeInfo_var, L_11);
		}

IL_003c:
		{
			IL2CPP_END_FINALLY(51)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(51)
	{
		IL2CPP_JUMP_TBL(0x3D, IL_003d)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
	}

IL_003d:
	{
		// return mask;
		uint32_t L_12 = V_0;
		return L_12;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// UnityEngine.Vector2 TouchScript.Utils.ClusterUtils::Get2DCenterPosition(System.Collections.Generic.IList`1<TouchScript.Pointers.Pointer>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ClusterUtils_Get2DCenterPosition_m85D3BC7F9ABF6B24256FC4F7EEC887B8EDC25293 (RuntimeObject* ___pointers0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ClusterUtils_Get2DCenterPosition_m85D3BC7F9ABF6B24256FC4F7EEC887B8EDC25293_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  V_1;
	memset((&V_1), 0, sizeof(V_1));
	int32_t V_2 = 0;
	{
		// var count = pointers.Count;
		RuntimeObject* L_0 = ___pointers0;
		NullCheck(L_0);
		int32_t L_1 = InterfaceFuncInvoker0< int32_t >::Invoke(0 /* System.Int32 System.Collections.Generic.ICollection`1<TouchScript.Pointers.Pointer>::get_Count() */, ICollection_1_tF3AE5A349C10D1F7E8F454D6EE9D5E63A06DD52C_il2cpp_TypeInfo_var, L_0);
		V_0 = L_1;
		// if (count == 0) return TouchManager.INVALID_POSITION;
		int32_t L_2 = V_0;
		if (L_2)
		{
			goto IL_0010;
		}
	}
	{
		// if (count == 0) return TouchManager.INVALID_POSITION;
		IL2CPP_RUNTIME_CLASS_INIT(TouchManager_tE3A4C5ABD05B3B6C984CDBB1FA80479E1ACBE92B_il2cpp_TypeInfo_var);
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_3 = ((TouchManager_tE3A4C5ABD05B3B6C984CDBB1FA80479E1ACBE92B_StaticFields*)il2cpp_codegen_static_fields_for(TouchManager_tE3A4C5ABD05B3B6C984CDBB1FA80479E1ACBE92B_il2cpp_TypeInfo_var))->get_INVALID_POSITION_6();
		return L_3;
	}

IL_0010:
	{
		// if (count == 1) return pointers[0].Position;
		int32_t L_4 = V_0;
		if ((!(((uint32_t)L_4) == ((uint32_t)1))))
		{
			goto IL_0021;
		}
	}
	{
		// if (count == 1) return pointers[0].Position;
		RuntimeObject* L_5 = ___pointers0;
		NullCheck(L_5);
		Pointer_tBF6873B314D4CC8534B6CD8A8BA54B92D8CFDE98 * L_6 = InterfaceFuncInvoker1< Pointer_tBF6873B314D4CC8534B6CD8A8BA54B92D8CFDE98 *, int32_t >::Invoke(0 /* !0 System.Collections.Generic.IList`1<TouchScript.Pointers.Pointer>::get_Item(System.Int32) */, IList_1_t3C3D2AB1202DCF5C5B777ABDD60717732726F73F_il2cpp_TypeInfo_var, L_5, 0);
		NullCheck(L_6);
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_7 = Pointer_get_Position_mCE0B33AF8B7D86A8BA6824E7817A3DFCB29BE4BC_inline(L_6, /*hidden argument*/NULL);
		return L_7;
	}

IL_0021:
	{
		// var position = new Vector2();
		il2cpp_codegen_initobj((&V_1), sizeof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D ));
		// for (var i = 0; i < count; i++) position += pointers[i].Position;
		V_2 = 0;
		goto IL_0044;
	}

IL_002d:
	{
		// for (var i = 0; i < count; i++) position += pointers[i].Position;
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_8 = V_1;
		RuntimeObject* L_9 = ___pointers0;
		int32_t L_10 = V_2;
		NullCheck(L_9);
		Pointer_tBF6873B314D4CC8534B6CD8A8BA54B92D8CFDE98 * L_11 = InterfaceFuncInvoker1< Pointer_tBF6873B314D4CC8534B6CD8A8BA54B92D8CFDE98 *, int32_t >::Invoke(0 /* !0 System.Collections.Generic.IList`1<TouchScript.Pointers.Pointer>::get_Item(System.Int32) */, IList_1_t3C3D2AB1202DCF5C5B777ABDD60717732726F73F_il2cpp_TypeInfo_var, L_9, L_10);
		NullCheck(L_11);
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_12 = Pointer_get_Position_mCE0B33AF8B7D86A8BA6824E7817A3DFCB29BE4BC_inline(L_11, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_il2cpp_TypeInfo_var);
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_13 = Vector2_op_Addition_m81A4D928B8E399DA3A4E3ACD8937EDFDCB014682(L_8, L_12, /*hidden argument*/NULL);
		V_1 = L_13;
		// for (var i = 0; i < count; i++) position += pointers[i].Position;
		int32_t L_14 = V_2;
		V_2 = ((int32_t)il2cpp_codegen_add((int32_t)L_14, (int32_t)1));
	}

IL_0044:
	{
		// for (var i = 0; i < count; i++) position += pointers[i].Position;
		int32_t L_15 = V_2;
		int32_t L_16 = V_0;
		if ((((int32_t)L_15) < ((int32_t)L_16)))
		{
			goto IL_002d;
		}
	}
	{
		// return position / count;
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_17 = V_1;
		int32_t L_18 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_il2cpp_TypeInfo_var);
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_19 = Vector2_op_Division_m0961A935168EE6701E098E2B37013DFFF46A5077(L_17, (((float)((float)L_18))), /*hidden argument*/NULL);
		return L_19;
	}
}
// UnityEngine.Vector2 TouchScript.Utils.ClusterUtils::GetPrevious2DCenterPosition(System.Collections.Generic.IList`1<TouchScript.Pointers.Pointer>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ClusterUtils_GetPrevious2DCenterPosition_mE261E1BF0B86549ED5FE9DA1945C874585EF60BB (RuntimeObject* ___pointers0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ClusterUtils_GetPrevious2DCenterPosition_mE261E1BF0B86549ED5FE9DA1945C874585EF60BB_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  V_1;
	memset((&V_1), 0, sizeof(V_1));
	int32_t V_2 = 0;
	{
		// var count = pointers.Count;
		RuntimeObject* L_0 = ___pointers0;
		NullCheck(L_0);
		int32_t L_1 = InterfaceFuncInvoker0< int32_t >::Invoke(0 /* System.Int32 System.Collections.Generic.ICollection`1<TouchScript.Pointers.Pointer>::get_Count() */, ICollection_1_tF3AE5A349C10D1F7E8F454D6EE9D5E63A06DD52C_il2cpp_TypeInfo_var, L_0);
		V_0 = L_1;
		// if (count == 0) return TouchManager.INVALID_POSITION;
		int32_t L_2 = V_0;
		if (L_2)
		{
			goto IL_0010;
		}
	}
	{
		// if (count == 0) return TouchManager.INVALID_POSITION;
		IL2CPP_RUNTIME_CLASS_INIT(TouchManager_tE3A4C5ABD05B3B6C984CDBB1FA80479E1ACBE92B_il2cpp_TypeInfo_var);
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_3 = ((TouchManager_tE3A4C5ABD05B3B6C984CDBB1FA80479E1ACBE92B_StaticFields*)il2cpp_codegen_static_fields_for(TouchManager_tE3A4C5ABD05B3B6C984CDBB1FA80479E1ACBE92B_il2cpp_TypeInfo_var))->get_INVALID_POSITION_6();
		return L_3;
	}

IL_0010:
	{
		// if (count == 1) return pointers[0].PreviousPosition;
		int32_t L_4 = V_0;
		if ((!(((uint32_t)L_4) == ((uint32_t)1))))
		{
			goto IL_0021;
		}
	}
	{
		// if (count == 1) return pointers[0].PreviousPosition;
		RuntimeObject* L_5 = ___pointers0;
		NullCheck(L_5);
		Pointer_tBF6873B314D4CC8534B6CD8A8BA54B92D8CFDE98 * L_6 = InterfaceFuncInvoker1< Pointer_tBF6873B314D4CC8534B6CD8A8BA54B92D8CFDE98 *, int32_t >::Invoke(0 /* !0 System.Collections.Generic.IList`1<TouchScript.Pointers.Pointer>::get_Item(System.Int32) */, IList_1_t3C3D2AB1202DCF5C5B777ABDD60717732726F73F_il2cpp_TypeInfo_var, L_5, 0);
		NullCheck(L_6);
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_7 = Pointer_get_PreviousPosition_m18CBA71CDAE159A709A4F88D96F40B81B333118E_inline(L_6, /*hidden argument*/NULL);
		return L_7;
	}

IL_0021:
	{
		// var position = new Vector2();
		il2cpp_codegen_initobj((&V_1), sizeof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D ));
		// for (var i = 0; i < count; i++) position += pointers[i].PreviousPosition;
		V_2 = 0;
		goto IL_0044;
	}

IL_002d:
	{
		// for (var i = 0; i < count; i++) position += pointers[i].PreviousPosition;
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_8 = V_1;
		RuntimeObject* L_9 = ___pointers0;
		int32_t L_10 = V_2;
		NullCheck(L_9);
		Pointer_tBF6873B314D4CC8534B6CD8A8BA54B92D8CFDE98 * L_11 = InterfaceFuncInvoker1< Pointer_tBF6873B314D4CC8534B6CD8A8BA54B92D8CFDE98 *, int32_t >::Invoke(0 /* !0 System.Collections.Generic.IList`1<TouchScript.Pointers.Pointer>::get_Item(System.Int32) */, IList_1_t3C3D2AB1202DCF5C5B777ABDD60717732726F73F_il2cpp_TypeInfo_var, L_9, L_10);
		NullCheck(L_11);
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_12 = Pointer_get_PreviousPosition_m18CBA71CDAE159A709A4F88D96F40B81B333118E_inline(L_11, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_il2cpp_TypeInfo_var);
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_13 = Vector2_op_Addition_m81A4D928B8E399DA3A4E3ACD8937EDFDCB014682(L_8, L_12, /*hidden argument*/NULL);
		V_1 = L_13;
		// for (var i = 0; i < count; i++) position += pointers[i].PreviousPosition;
		int32_t L_14 = V_2;
		V_2 = ((int32_t)il2cpp_codegen_add((int32_t)L_14, (int32_t)1));
	}

IL_0044:
	{
		// for (var i = 0; i < count; i++) position += pointers[i].PreviousPosition;
		int32_t L_15 = V_2;
		int32_t L_16 = V_0;
		if ((((int32_t)L_15) < ((int32_t)L_16)))
		{
			goto IL_002d;
		}
	}
	{
		// return position / count;
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_17 = V_1;
		int32_t L_18 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_il2cpp_TypeInfo_var);
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_19 = Vector2_op_Division_m0961A935168EE6701E098E2B37013DFFF46A5077(L_17, (((float)((float)L_18))), /*hidden argument*/NULL);
		return L_19;
	}
}
// System.String TouchScript.Utils.ClusterUtils::GetPointsHash(System.Collections.Generic.IList`1<TouchScript.Pointers.Pointer>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* ClusterUtils_GetPointsHash_m31E06AECDB1ACD7975B9B98CA151E538E1EFA823 (RuntimeObject* ___pointers0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ClusterUtils_GetPointsHash_m31E06AECDB1ACD7975B9B98CA151E538E1EFA823_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		// hashString.Remove(0, hashString.Length);
		IL2CPP_RUNTIME_CLASS_INIT(ClusterUtils_t2CD9CE77DEAE30CFAFE820A605093A2A27A86696_il2cpp_TypeInfo_var);
		StringBuilder_t * L_0 = ((ClusterUtils_t2CD9CE77DEAE30CFAFE820A605093A2A27A86696_StaticFields*)il2cpp_codegen_static_fields_for(ClusterUtils_t2CD9CE77DEAE30CFAFE820A605093A2A27A86696_il2cpp_TypeInfo_var))->get_hashString_0();
		StringBuilder_t * L_1 = ((ClusterUtils_t2CD9CE77DEAE30CFAFE820A605093A2A27A86696_StaticFields*)il2cpp_codegen_static_fields_for(ClusterUtils_t2CD9CE77DEAE30CFAFE820A605093A2A27A86696_il2cpp_TypeInfo_var))->get_hashString_0();
		NullCheck(L_1);
		int32_t L_2 = StringBuilder_get_Length_m44BCD2BF32D45E9376761FF33AA429BFBD902F07(L_1, /*hidden argument*/NULL);
		NullCheck(L_0);
		StringBuilder_Remove_m5DA9C1C4D056FA61B8923BE85E6BFF44B14A24F9(L_0, 0, L_2, /*hidden argument*/NULL);
		// for (var i = 0; i < pointers.Count; i++)
		V_0 = 0;
		goto IL_0045;
	}

IL_001a:
	{
		// hashString.Append("#");
		IL2CPP_RUNTIME_CLASS_INIT(ClusterUtils_t2CD9CE77DEAE30CFAFE820A605093A2A27A86696_il2cpp_TypeInfo_var);
		StringBuilder_t * L_3 = ((ClusterUtils_t2CD9CE77DEAE30CFAFE820A605093A2A27A86696_StaticFields*)il2cpp_codegen_static_fields_for(ClusterUtils_t2CD9CE77DEAE30CFAFE820A605093A2A27A86696_il2cpp_TypeInfo_var))->get_hashString_0();
		NullCheck(L_3);
		StringBuilder_Append_mDBB8CCBB7750C67BE2F2D92F47E6C0FA42793260(L_3, _stringLiteralD08F88DF745FA7950B104E4A707A31CFCE7B5841, /*hidden argument*/NULL);
		// hashString.Append(pointers[i].Id);
		StringBuilder_t * L_4 = ((ClusterUtils_t2CD9CE77DEAE30CFAFE820A605093A2A27A86696_StaticFields*)il2cpp_codegen_static_fields_for(ClusterUtils_t2CD9CE77DEAE30CFAFE820A605093A2A27A86696_il2cpp_TypeInfo_var))->get_hashString_0();
		RuntimeObject* L_5 = ___pointers0;
		int32_t L_6 = V_0;
		NullCheck(L_5);
		Pointer_tBF6873B314D4CC8534B6CD8A8BA54B92D8CFDE98 * L_7 = InterfaceFuncInvoker1< Pointer_tBF6873B314D4CC8534B6CD8A8BA54B92D8CFDE98 *, int32_t >::Invoke(0 /* !0 System.Collections.Generic.IList`1<TouchScript.Pointers.Pointer>::get_Item(System.Int32) */, IList_1_t3C3D2AB1202DCF5C5B777ABDD60717732726F73F_il2cpp_TypeInfo_var, L_5, L_6);
		NullCheck(L_7);
		int32_t L_8 = Pointer_get_Id_mFF112114C234DC146CA93A7719C3F316E8DACCAD_inline(L_7, /*hidden argument*/NULL);
		NullCheck(L_4);
		StringBuilder_Append_m85874CFF9E4B152DB2A91936C6F2CA3E9B1EFF84(L_4, L_8, /*hidden argument*/NULL);
		// for (var i = 0; i < pointers.Count; i++)
		int32_t L_9 = V_0;
		V_0 = ((int32_t)il2cpp_codegen_add((int32_t)L_9, (int32_t)1));
	}

IL_0045:
	{
		// for (var i = 0; i < pointers.Count; i++)
		int32_t L_10 = V_0;
		RuntimeObject* L_11 = ___pointers0;
		NullCheck(L_11);
		int32_t L_12 = InterfaceFuncInvoker0< int32_t >::Invoke(0 /* System.Int32 System.Collections.Generic.ICollection`1<TouchScript.Pointers.Pointer>::get_Count() */, ICollection_1_tF3AE5A349C10D1F7E8F454D6EE9D5E63A06DD52C_il2cpp_TypeInfo_var, L_11);
		if ((((int32_t)L_10) < ((int32_t)L_12)))
		{
			goto IL_001a;
		}
	}
	{
		// return hashString.ToString();
		IL2CPP_RUNTIME_CLASS_INIT(ClusterUtils_t2CD9CE77DEAE30CFAFE820A605093A2A27A86696_il2cpp_TypeInfo_var);
		StringBuilder_t * L_13 = ((ClusterUtils_t2CD9CE77DEAE30CFAFE820A605093A2A27A86696_StaticFields*)il2cpp_codegen_static_fields_for(ClusterUtils_t2CD9CE77DEAE30CFAFE820A605093A2A27A86696_il2cpp_TypeInfo_var))->get_hashString_0();
		NullCheck(L_13);
		String_t* L_14 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_13);
		return L_14;
	}
}
// System.Void TouchScript.Utils.ClusterUtils::.cctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ClusterUtils__cctor_m2E71E8524F593F6D8BABC472814BC6A3BC5176A6 (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ClusterUtils__cctor_m2E71E8524F593F6D8BABC472814BC6A3BC5176A6_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// private static StringBuilder hashString = new StringBuilder();
		StringBuilder_t * L_0 = (StringBuilder_t *)il2cpp_codegen_object_new(StringBuilder_t_il2cpp_TypeInfo_var);
		StringBuilder__ctor_mF928376F82E8C8FF3C11842C562DB8CF28B2735E(L_0, /*hidden argument*/NULL);
		((ClusterUtils_t2CD9CE77DEAE30CFAFE820A605093A2A27A86696_StaticFields*)il2cpp_codegen_static_fields_for(ClusterUtils_t2CD9CE77DEAE30CFAFE820A605093A2A27A86696_il2cpp_TypeInfo_var))->set_hashString_0(L_0);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Exception TouchScript.Utils.EventHandlerExtensions::InvokeHandleExceptions(System.EventHandler,System.Object,System.EventArgs)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Exception_t * EventHandlerExtensions_InvokeHandleExceptions_m8E6D5FEFA89D62212811606B5639EA40526C8509 (EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C * ___handler0, RuntimeObject * ___sender1, EventArgs_t8E6CA180BE0E56674C6407011A94BAF7C757352E * ___args2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (EventHandlerExtensions_InvokeHandleExceptions_m8E6D5FEFA89D62212811606B5639EA40526C8509_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Exception_t * V_0 = NULL;
	Exception_t * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	void* __leave_targets_storage = alloca(sizeof(int32_t) * 2);
	il2cpp::utils::LeaveTargetStack __leave_targets(__leave_targets_storage);
	NO_UNUSED_WARNING (__leave_targets);

IL_0000:
	try
	{ // begin try (depth: 1)
		// handler(sender, args);
		EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C * L_0 = ___handler0;
		RuntimeObject * L_1 = ___sender1;
		EventArgs_t8E6CA180BE0E56674C6407011A94BAF7C757352E * L_2 = ___args2;
		NullCheck(L_0);
		EventHandler_Invoke_mD23D5EFEA562A05C5EACDD3E91EEDD2BF6C22800(L_0, L_1, L_2, /*hidden argument*/NULL);
		// }
		goto IL_0013;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Exception_t_il2cpp_TypeInfo_var, il2cpp_codegen_object_class(e.ex)))
			goto CATCH_000a;
		throw e;
	}

CATCH_000a:
	{ // begin catch(System.Exception)
		// Debug.LogException(ex);
		Exception_t * L_3 = ((Exception_t *)__exception_local);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t7B5FCB117E2FD63B6838BC52821B252E2BFB61C4_il2cpp_TypeInfo_var);
		Debug_LogException_mBAA6702C240E37B2A834AA74E4FDC15A3A5589A9(L_3, /*hidden argument*/NULL);
		// return ex;
		V_0 = L_3;
		goto IL_0015;
	} // end catch (depth: 1)

IL_0013:
	{
		// return null;
		return (Exception_t *)NULL;
	}

IL_0015:
	{
		// }
		Exception_t * L_4 = V_0;
		return L_4;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// UnityEngine.Vector3 TouchScript.Utils.Geom.ProjectionUtils::CameraToPlaneProjection(UnityEngine.Vector2,UnityEngine.Camera,UnityEngine.Plane)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ProjectionUtils_CameraToPlaneProjection_m49C90C2A65AB392F106E1B043BB38EC1D603376A (Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___position0, Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * ___camera1, Plane_t0903921088DEEDE1BCDEA5BF279EDBCFC9679AED  ___projectionPlane2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ProjectionUtils_CameraToPlaneProjection_m49C90C2A65AB392F106E1B043BB38EC1D603376A_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	Ray_tE2163D4CB3E6B267E29F8ABE41684490E4A614B2  V_1;
	memset((&V_1), 0, sizeof(V_1));
	{
		// var distance = 0f;
		V_0 = (0.0f);
		// var ray = camera.ScreenPointToRay(position);
		Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * L_0 = ___camera1;
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_1 = ___position0;
		IL2CPP_RUNTIME_CLASS_INIT(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_il2cpp_TypeInfo_var);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_2 = Vector2_op_Implicit_mD152B6A34B4DB7FFECC2844D74718568FE867D6F(L_1, /*hidden argument*/NULL);
		NullCheck(L_0);
		Ray_tE2163D4CB3E6B267E29F8ABE41684490E4A614B2  L_3 = Camera_ScreenPointToRay_m27638E78502DB6D6D7113F81AF7C210773B828F3(L_0, L_2, /*hidden argument*/NULL);
		V_1 = L_3;
		// var result = projectionPlane.Raycast(ray, out distance);
		Ray_tE2163D4CB3E6B267E29F8ABE41684490E4A614B2  L_4 = V_1;
		bool L_5 = Plane_Raycast_m04E61D7C78A5DA70F4F73F9805ABB54177B799A9((Plane_t0903921088DEEDE1BCDEA5BF279EDBCFC9679AED *)(&___projectionPlane2), L_4, (float*)(&V_0), /*hidden argument*/NULL);
		// if (!result && Mathf.Approximately(distance, 0f)) return -projectionPlane.normal * projectionPlane.GetDistanceToPoint(Vector3.zero); // perpendicular to the screen
		if (L_5)
		{
			goto IL_004a;
		}
	}
	{
		float L_6 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_tFBDE6467D269BFE410605C7D806FD9991D4A89CB_il2cpp_TypeInfo_var);
		bool L_7 = Mathf_Approximately_m91AF00403E0D2DEA1AAE68601AD218CFAD70DF7E(L_6, (0.0f), /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_004a;
		}
	}
	{
		// if (!result && Mathf.Approximately(distance, 0f)) return -projectionPlane.normal * projectionPlane.GetDistanceToPoint(Vector3.zero); // perpendicular to the screen
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_8 = Plane_get_normal_m203D43F51C449990214D04F332E8261295162E84((Plane_t0903921088DEEDE1BCDEA5BF279EDBCFC9679AED *)(&___projectionPlane2), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_il2cpp_TypeInfo_var);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_9 = Vector3_op_UnaryNegation_m2AFBBF22801F9BCA5A4EBE642A29F433FE1339C2(L_8, /*hidden argument*/NULL);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_10 = Vector3_get_zero_m3CDDCAE94581DF3BB16C4B40A100E28E9C6649C2(/*hidden argument*/NULL);
		float L_11 = Plane_GetDistanceToPoint_mF59C65D80AEE969807A8D78AB6B5012B07BF113A((Plane_t0903921088DEEDE1BCDEA5BF279EDBCFC9679AED *)(&___projectionPlane2), L_10, /*hidden argument*/NULL);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_12 = Vector3_op_Multiply_m1C5F07723615156ACF035D88A1280A9E8F35A04E(L_9, L_11, /*hidden argument*/NULL);
		return L_12;
	}

IL_004a:
	{
		// return ray.origin + ray.direction * distance;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_13 = Ray_get_origin_m3773CA7B1E2F26F6F1447652B485D86C0BEC5187((Ray_tE2163D4CB3E6B267E29F8ABE41684490E4A614B2 *)(&V_1), /*hidden argument*/NULL);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_14 = Ray_get_direction_m9E6468CD87844B437FC4B93491E63D388322F76E((Ray_tE2163D4CB3E6B267E29F8ABE41684490E4A614B2 *)(&V_1), /*hidden argument*/NULL);
		float L_15 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_il2cpp_TypeInfo_var);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_16 = Vector3_op_Multiply_m1C5F07723615156ACF035D88A1280A9E8F35A04E(L_14, L_15, /*hidden argument*/NULL);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_17 = Vector3_op_Addition_m929F9C17E5D11B94D50B4AFF1D730B70CB59B50E(L_13, L_16, /*hidden argument*/NULL);
		return L_17;
	}
}
// UnityEngine.Vector3 TouchScript.Utils.Geom.ProjectionUtils::ScreenToPlaneProjection(UnityEngine.Vector2,UnityEngine.Plane)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ProjectionUtils_ScreenToPlaneProjection_m5ED3FA8D18B088F44849EC32C475DE400C7B63D5 (Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___position0, Plane_t0903921088DEEDE1BCDEA5BF279EDBCFC9679AED  ___projectionPlane1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ProjectionUtils_ScreenToPlaneProjection_m5ED3FA8D18B088F44849EC32C475DE400C7B63D5_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	Ray_tE2163D4CB3E6B267E29F8ABE41684490E4A614B2  V_1;
	memset((&V_1), 0, sizeof(V_1));
	{
		// var distance = 0f;
		V_0 = (0.0f);
		// var ray = new Ray(position, Vector3.forward);
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_0 = ___position0;
		IL2CPP_RUNTIME_CLASS_INIT(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_il2cpp_TypeInfo_var);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_1 = Vector2_op_Implicit_mD152B6A34B4DB7FFECC2844D74718568FE867D6F(L_0, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_il2cpp_TypeInfo_var);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_2 = Vector3_get_forward_m3E2E192B3302130098738C308FA1EE1439449D0D(/*hidden argument*/NULL);
		Ray__ctor_m695D219349B8AA4C82F96C55A27D384C07736F6B((Ray_tE2163D4CB3E6B267E29F8ABE41684490E4A614B2 *)(&V_1), L_1, L_2, /*hidden argument*/NULL);
		// var result = projectionPlane.Raycast(ray, out distance);
		Ray_tE2163D4CB3E6B267E29F8ABE41684490E4A614B2  L_3 = V_1;
		bool L_4 = Plane_Raycast_m04E61D7C78A5DA70F4F73F9805ABB54177B799A9((Plane_t0903921088DEEDE1BCDEA5BF279EDBCFC9679AED *)(&___projectionPlane1), L_3, (float*)(&V_0), /*hidden argument*/NULL);
		// if (!result && Mathf.Approximately(distance, 0f)) return -projectionPlane.normal * projectionPlane.GetDistanceToPoint(Vector3.zero); // perpendicular to the screen
		if (L_4)
		{
			goto IL_004f;
		}
	}
	{
		float L_5 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_tFBDE6467D269BFE410605C7D806FD9991D4A89CB_il2cpp_TypeInfo_var);
		bool L_6 = Mathf_Approximately_m91AF00403E0D2DEA1AAE68601AD218CFAD70DF7E(L_5, (0.0f), /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_004f;
		}
	}
	{
		// if (!result && Mathf.Approximately(distance, 0f)) return -projectionPlane.normal * projectionPlane.GetDistanceToPoint(Vector3.zero); // perpendicular to the screen
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_7 = Plane_get_normal_m203D43F51C449990214D04F332E8261295162E84((Plane_t0903921088DEEDE1BCDEA5BF279EDBCFC9679AED *)(&___projectionPlane1), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_il2cpp_TypeInfo_var);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_8 = Vector3_op_UnaryNegation_m2AFBBF22801F9BCA5A4EBE642A29F433FE1339C2(L_7, /*hidden argument*/NULL);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_9 = Vector3_get_zero_m3CDDCAE94581DF3BB16C4B40A100E28E9C6649C2(/*hidden argument*/NULL);
		float L_10 = Plane_GetDistanceToPoint_mF59C65D80AEE969807A8D78AB6B5012B07BF113A((Plane_t0903921088DEEDE1BCDEA5BF279EDBCFC9679AED *)(&___projectionPlane1), L_9, /*hidden argument*/NULL);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_11 = Vector3_op_Multiply_m1C5F07723615156ACF035D88A1280A9E8F35A04E(L_8, L_10, /*hidden argument*/NULL);
		return L_11;
	}

IL_004f:
	{
		// return ray.origin + new Vector3(0, 0, distance);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_12 = Ray_get_origin_m3773CA7B1E2F26F6F1447652B485D86C0BEC5187((Ray_tE2163D4CB3E6B267E29F8ABE41684490E4A614B2 *)(&V_1), /*hidden argument*/NULL);
		float L_13 = V_0;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_14;
		memset((&L_14), 0, sizeof(L_14));
		Vector3__ctor_m08F61F548AA5836D8789843ACB4A81E4963D2EE1((&L_14), (0.0f), (0.0f), L_13, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_il2cpp_TypeInfo_var);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_15 = Vector3_op_Addition_m929F9C17E5D11B94D50B4AFF1D730B70CB59B50E(L_12, L_14, /*hidden argument*/NULL);
		return L_15;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Single TouchScript.Utils.Geom.TwoD::PointToLineDistance(UnityEngine.Vector2,UnityEngine.Vector2,UnityEngine.Vector2)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float TwoD_PointToLineDistance_m423B16A5A7384D020178775D8019DE019E7C52B0 (Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___lineStart0, Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___lineEnd1, Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___point2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TwoD_PointToLineDistance_m423B16A5A7384D020178775D8019DE019E7C52B0_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	{
		// var dx = lineEnd.x - lineStart.x;
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_0 = ___lineEnd1;
		float L_1 = L_0.get_x_0();
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_2 = ___lineStart0;
		float L_3 = L_2.get_x_0();
		V_0 = ((float)il2cpp_codegen_subtract((float)L_1, (float)L_3));
		// var dy = lineEnd.y - lineStart.y;
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_4 = ___lineEnd1;
		float L_5 = L_4.get_y_1();
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_6 = ___lineStart0;
		float L_7 = L_6.get_y_1();
		V_1 = ((float)il2cpp_codegen_subtract((float)L_5, (float)L_7));
		// return (dy * point.x - dx * point.y + lineEnd.x * lineStart.y - lineEnd.y * lineStart.x) / Mathf.Sqrt(dx * dx + dy * dy);
		float L_8 = V_1;
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_9 = ___point2;
		float L_10 = L_9.get_x_0();
		float L_11 = V_0;
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_12 = ___point2;
		float L_13 = L_12.get_y_1();
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_14 = ___lineEnd1;
		float L_15 = L_14.get_x_0();
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_16 = ___lineStart0;
		float L_17 = L_16.get_y_1();
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_18 = ___lineEnd1;
		float L_19 = L_18.get_y_1();
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_20 = ___lineStart0;
		float L_21 = L_20.get_x_0();
		float L_22 = V_0;
		float L_23 = V_0;
		float L_24 = V_1;
		float L_25 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_tFBDE6467D269BFE410605C7D806FD9991D4A89CB_il2cpp_TypeInfo_var);
		float L_26 = sqrtf(((float)il2cpp_codegen_add((float)((float)il2cpp_codegen_multiply((float)L_22, (float)L_23)), (float)((float)il2cpp_codegen_multiply((float)L_24, (float)L_25)))));
		return ((float)((float)((float)il2cpp_codegen_subtract((float)((float)il2cpp_codegen_add((float)((float)il2cpp_codegen_subtract((float)((float)il2cpp_codegen_multiply((float)L_8, (float)L_10)), (float)((float)il2cpp_codegen_multiply((float)L_11, (float)L_13)))), (float)((float)il2cpp_codegen_multiply((float)L_15, (float)L_17)))), (float)((float)il2cpp_codegen_multiply((float)L_19, (float)L_21))))/(float)L_26));
	}
}
// System.Void TouchScript.Utils.Geom.TwoD::PointToLineDistance2(UnityEngine.Vector2,UnityEngine.Vector2,UnityEngine.Vector2,UnityEngine.Vector2,System.Single&,System.Single&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TwoD_PointToLineDistance2_mA904184ADD13C33E7951C56BB2B8A16C167686F9 (Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___lineStart0, Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___lineEnd1, Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___point12, Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___point23, float* ___dist14, float* ___dist25, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TwoD_PointToLineDistance2_mA904184ADD13C33E7951C56BB2B8A16C167686F9_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	float V_2 = 0.0f;
	float V_3 = 0.0f;
	{
		// var dx = lineEnd.x - lineStart.x;
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_0 = ___lineEnd1;
		float L_1 = L_0.get_x_0();
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_2 = ___lineStart0;
		float L_3 = L_2.get_x_0();
		V_0 = ((float)il2cpp_codegen_subtract((float)L_1, (float)L_3));
		// var dy = lineEnd.y - lineStart.y;
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_4 = ___lineEnd1;
		float L_5 = L_4.get_y_1();
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_6 = ___lineStart0;
		float L_7 = L_6.get_y_1();
		V_1 = ((float)il2cpp_codegen_subtract((float)L_5, (float)L_7));
		// var c = lineEnd.x * lineStart.y - lineEnd.y * lineStart.x;
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_8 = ___lineEnd1;
		float L_9 = L_8.get_x_0();
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_10 = ___lineStart0;
		float L_11 = L_10.get_y_1();
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_12 = ___lineEnd1;
		float L_13 = L_12.get_y_1();
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_14 = ___lineStart0;
		float L_15 = L_14.get_x_0();
		V_2 = ((float)il2cpp_codegen_subtract((float)((float)il2cpp_codegen_multiply((float)L_9, (float)L_11)), (float)((float)il2cpp_codegen_multiply((float)L_13, (float)L_15))));
		// var length = Mathf.Sqrt(dx * dx + dy * dy);
		float L_16 = V_0;
		float L_17 = V_0;
		float L_18 = V_1;
		float L_19 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_tFBDE6467D269BFE410605C7D806FD9991D4A89CB_il2cpp_TypeInfo_var);
		float L_20 = sqrtf(((float)il2cpp_codegen_add((float)((float)il2cpp_codegen_multiply((float)L_16, (float)L_17)), (float)((float)il2cpp_codegen_multiply((float)L_18, (float)L_19)))));
		V_3 = L_20;
		// dist1 = (dy * point1.x - dx * point1.y + c) / length;
		float* L_21 = ___dist14;
		float L_22 = V_1;
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_23 = ___point12;
		float L_24 = L_23.get_x_0();
		float L_25 = V_0;
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_26 = ___point12;
		float L_27 = L_26.get_y_1();
		float L_28 = V_2;
		float L_29 = V_3;
		*((float*)L_21) = (float)((float)((float)((float)il2cpp_codegen_add((float)((float)il2cpp_codegen_subtract((float)((float)il2cpp_codegen_multiply((float)L_22, (float)L_24)), (float)((float)il2cpp_codegen_multiply((float)L_25, (float)L_27)))), (float)L_28))/(float)L_29));
		// dist2 = (dy * point2.x - dx * point2.y + c) / length;
		float* L_30 = ___dist25;
		float L_31 = V_1;
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_32 = ___point23;
		float L_33 = L_32.get_x_0();
		float L_34 = V_0;
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_35 = ___point23;
		float L_36 = L_35.get_y_1();
		float L_37 = V_2;
		float L_38 = V_3;
		*((float*)L_30) = (float)((float)((float)((float)il2cpp_codegen_add((float)((float)il2cpp_codegen_subtract((float)((float)il2cpp_codegen_multiply((float)L_31, (float)L_33)), (float)((float)il2cpp_codegen_multiply((float)L_34, (float)L_36)))), (float)L_37))/(float)L_38));
		// }
		return;
	}
}
// UnityEngine.Vector2 TouchScript.Utils.Geom.TwoD::Rotate(UnityEngine.Vector2,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  TwoD_Rotate_m5077CC8359AAB1CE9AFD09F8F8A10E6FB2E6BF42 (Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___point0, float ___angle1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TwoD_Rotate_m5077CC8359AAB1CE9AFD09F8F8A10E6FB2E6BF42_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	{
		// var rad = angle * Mathf.Deg2Rad;
		float L_0 = ___angle1;
		// var cos = Mathf.Cos(rad);
		float L_1 = ((float)il2cpp_codegen_multiply((float)L_0, (float)(0.0174532924f)));
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_tFBDE6467D269BFE410605C7D806FD9991D4A89CB_il2cpp_TypeInfo_var);
		float L_2 = cosf(L_1);
		V_0 = L_2;
		// var sin = Mathf.Sin(rad);
		float L_3 = sinf(L_1);
		V_1 = L_3;
		// return new Vector2(point.x * cos - point.y * sin, point.x * sin + point.y * cos);
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_4 = ___point0;
		float L_5 = L_4.get_x_0();
		float L_6 = V_0;
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_7 = ___point0;
		float L_8 = L_7.get_y_1();
		float L_9 = V_1;
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_10 = ___point0;
		float L_11 = L_10.get_x_0();
		float L_12 = V_1;
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_13 = ___point0;
		float L_14 = L_13.get_y_1();
		float L_15 = V_0;
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_16;
		memset((&L_16), 0, sizeof(L_16));
		Vector2__ctor_mEE8FB117AB1F8DB746FB8B3EB4C0DA3BF2A230D0((&L_16), ((float)il2cpp_codegen_subtract((float)((float)il2cpp_codegen_multiply((float)L_5, (float)L_6)), (float)((float)il2cpp_codegen_multiply((float)L_8, (float)L_9)))), ((float)il2cpp_codegen_add((float)((float)il2cpp_codegen_multiply((float)L_11, (float)L_12)), (float)((float)il2cpp_codegen_multiply((float)L_14, (float)L_15)))), /*hidden argument*/NULL);
		return L_16;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Boolean TouchScript.Utils.PointerUtils::IsPointerOnTarget(TouchScript.Pointers.Pointer)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool PointerUtils_IsPointerOnTarget_mC471A1AE57B2F02F0CB977FF51AB0BCD7706C6DE (Pointer_tBF6873B314D4CC8534B6CD8A8BA54B92D8CFDE98 * ___pointer0, const RuntimeMethod* method)
{
	HitData_t3022C0DF180FDE3CF86F713B78A2056D02E78980  V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		// if (pointer == null) return false;
		Pointer_tBF6873B314D4CC8534B6CD8A8BA54B92D8CFDE98 * L_0 = ___pointer0;
		if (L_0)
		{
			goto IL_0005;
		}
	}
	{
		// if (pointer == null) return false;
		return (bool)0;
	}

IL_0005:
	{
		// return IsPointerOnTarget(pointer, pointer.GetPressData().Target);
		Pointer_tBF6873B314D4CC8534B6CD8A8BA54B92D8CFDE98 * L_1 = ___pointer0;
		Pointer_tBF6873B314D4CC8534B6CD8A8BA54B92D8CFDE98 * L_2 = ___pointer0;
		NullCheck(L_2);
		HitData_t3022C0DF180FDE3CF86F713B78A2056D02E78980  L_3 = Pointer_GetPressData_m806849D3319D7AF0EB6469AC978F593B1AD6A3B4_inline(L_2, /*hidden argument*/NULL);
		V_0 = L_3;
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_4 = HitData_get_Target_mD8CF226FD6A5401997C2F6F554C2BA26155F7A05_inline((HitData_t3022C0DF180FDE3CF86F713B78A2056D02E78980 *)(&V_0), /*hidden argument*/NULL);
		bool L_5 = PointerUtils_IsPointerOnTarget_m73191DDEDDAA92FC5B63E82BE8C94DB49948E576(L_1, L_4, /*hidden argument*/NULL);
		return L_5;
	}
}
// System.Boolean TouchScript.Utils.PointerUtils::IsPointerOnTarget(TouchScript.Pointers.IPointer,UnityEngine.Transform)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool PointerUtils_IsPointerOnTarget_m73191DDEDDAA92FC5B63E82BE8C94DB49948E576 (RuntimeObject* ___pointer0, Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ___target1, const RuntimeMethod* method)
{
	HitData_t3022C0DF180FDE3CF86F713B78A2056D02E78980  V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		// return IsPointerOnTarget(pointer, target, out hit);
		RuntimeObject* L_0 = ___pointer0;
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_1 = ___target1;
		bool L_2 = PointerUtils_IsPointerOnTarget_mFA6B5C217481FFAC6208A08D8564B02C464C2DBF(L_0, L_1, (HitData_t3022C0DF180FDE3CF86F713B78A2056D02E78980 *)(&V_0), /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Boolean TouchScript.Utils.PointerUtils::IsPointerOnTarget(TouchScript.Pointers.IPointer,UnityEngine.Transform,TouchScript.Hit.HitData&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool PointerUtils_IsPointerOnTarget_mFA6B5C217481FFAC6208A08D8564B02C464C2DBF (RuntimeObject* ___pointer0, Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ___target1, HitData_t3022C0DF180FDE3CF86F713B78A2056D02E78980 * ___hit2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PointerUtils_IsPointerOnTarget_mFA6B5C217481FFAC6208A08D8564B02C464C2DBF_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// hit = default(HitData);
		HitData_t3022C0DF180FDE3CF86F713B78A2056D02E78980 * L_0 = ___hit2;
		il2cpp_codegen_initobj(L_0, sizeof(HitData_t3022C0DF180FDE3CF86F713B78A2056D02E78980 ));
		// if (pointer == null || target == null) return false;
		RuntimeObject* L_1 = ___pointer0;
		if (!L_1)
		{
			goto IL_0013;
		}
	}
	{
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_2 = ___target1;
		IL2CPP_RUNTIME_CLASS_INIT(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_il2cpp_TypeInfo_var);
		bool L_3 = Object_op_Equality_mBC2401774F3BE33E8CF6F0A8148E66C95D6CFF1C(L_2, (Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 *)NULL, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0015;
		}
	}

IL_0013:
	{
		// if (pointer == null || target == null) return false;
		return (bool)0;
	}

IL_0015:
	{
		// hit = pointer.GetOverData();
		HitData_t3022C0DF180FDE3CF86F713B78A2056D02E78980 * L_4 = ___hit2;
		RuntimeObject* L_5 = ___pointer0;
		NullCheck(L_5);
		HitData_t3022C0DF180FDE3CF86F713B78A2056D02E78980  L_6 = InterfaceFuncInvoker1< HitData_t3022C0DF180FDE3CF86F713B78A2056D02E78980 , bool >::Invoke(8 /* TouchScript.Hit.HitData TouchScript.Pointers.IPointer::GetOverData(System.Boolean) */, IPointer_t769E910CC2226CA89B07F95CCC6C526380C4BE13_il2cpp_TypeInfo_var, L_5, (bool)0);
		*(HitData_t3022C0DF180FDE3CF86F713B78A2056D02E78980 *)L_4 = L_6;
		Il2CppCodeGenWriteBarrier((void**)&(((HitData_t3022C0DF180FDE3CF86F713B78A2056D02E78980 *)L_4)->___target_1), (void*)NULL);
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&(((HitData_t3022C0DF180FDE3CF86F713B78A2056D02E78980 *)L_4)->___layer_3), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&(((HitData_t3022C0DF180FDE3CF86F713B78A2056D02E78980 *)L_4)->___raycastHitUI_6))->___Target_0), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&(((HitData_t3022C0DF180FDE3CF86F713B78A2056D02E78980 *)L_4)->___raycastHitUI_6))->___Raycaster_1), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&(((HitData_t3022C0DF180FDE3CF86F713B78A2056D02E78980 *)L_4)->___raycastHitUI_6))->___Graphic_6), (void*)NULL);
		#endif
		// if (hit.Target == null) return false;
		HitData_t3022C0DF180FDE3CF86F713B78A2056D02E78980 * L_7 = ___hit2;
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_8 = HitData_get_Target_mD8CF226FD6A5401997C2F6F554C2BA26155F7A05_inline((HitData_t3022C0DF180FDE3CF86F713B78A2056D02E78980 *)L_7, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_il2cpp_TypeInfo_var);
		bool L_9 = Object_op_Equality_mBC2401774F3BE33E8CF6F0A8148E66C95D6CFF1C(L_8, (Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 *)NULL, /*hidden argument*/NULL);
		if (!L_9)
		{
			goto IL_0032;
		}
	}
	{
		// if (hit.Target == null) return false;
		return (bool)0;
	}

IL_0032:
	{
		// return hit.Target.IsChildOf(target);
		HitData_t3022C0DF180FDE3CF86F713B78A2056D02E78980 * L_10 = ___hit2;
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_11 = HitData_get_Target_mD8CF226FD6A5401997C2F6F554C2BA26155F7A05_inline((HitData_t3022C0DF180FDE3CF86F713B78A2056D02E78980 *)L_10, /*hidden argument*/NULL);
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_12 = ___target1;
		NullCheck(L_11);
		bool L_13 = Transform_IsChildOf_mCB98BA14F7FB82B6AF6AE961E84C47AE1D99AA80(L_11, L_12, /*hidden argument*/NULL);
		return L_13;
	}
}
// System.String TouchScript.Utils.PointerUtils::PressedButtonsToString(TouchScript.Pointers.Pointer_PointerButtonState)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* PointerUtils_PressedButtonsToString_m57F1173F8613DA8D378DB09AA0FDB41285C639A5 (int32_t ___buttons0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PointerUtils_PressedButtonsToString_m57F1173F8613DA8D378DB09AA0FDB41285C639A5_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// initStringBuilder();
		PointerUtils_initStringBuilder_mEB6698C4E28B8F6D3E4E40FE5CA452C8415C179C(/*hidden argument*/NULL);
		// PressedButtonsToString(buttons, sb);
		int32_t L_0 = ___buttons0;
		StringBuilder_t * L_1 = ((PointerUtils_tEB27F0FDEFA565D24AC180439CAE78E49CF1D235_StaticFields*)il2cpp_codegen_static_fields_for(PointerUtils_tEB27F0FDEFA565D24AC180439CAE78E49CF1D235_il2cpp_TypeInfo_var))->get_sb_0();
		PointerUtils_PressedButtonsToString_mAF4CD9F932292EE068D39024DDD1E70F7DC26D9F(L_0, L_1, /*hidden argument*/NULL);
		// return sb.ToString();
		StringBuilder_t * L_2 = ((PointerUtils_tEB27F0FDEFA565D24AC180439CAE78E49CF1D235_StaticFields*)il2cpp_codegen_static_fields_for(PointerUtils_tEB27F0FDEFA565D24AC180439CAE78E49CF1D235_il2cpp_TypeInfo_var))->get_sb_0();
		NullCheck(L_2);
		String_t* L_3 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_2);
		return L_3;
	}
}
// System.Void TouchScript.Utils.PointerUtils::PressedButtonsToString(TouchScript.Pointers.Pointer_PointerButtonState,System.Text.StringBuilder)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PointerUtils_PressedButtonsToString_mAF4CD9F932292EE068D39024DDD1E70F7DC26D9F (int32_t ___buttons0, StringBuilder_t * ___builder1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PointerUtils_PressedButtonsToString_mAF4CD9F932292EE068D39024DDD1E70F7DC26D9F_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// if ((buttons & Pointer.PointerButtonState.FirstButtonPressed) != 0) builder.Append("1");
		int32_t L_0 = ___buttons0;
		if (!((int32_t)((int32_t)L_0&(int32_t)1)))
		{
			goto IL_0013;
		}
	}
	{
		// if ((buttons & Pointer.PointerButtonState.FirstButtonPressed) != 0) builder.Append("1");
		StringBuilder_t * L_1 = ___builder1;
		NullCheck(L_1);
		StringBuilder_Append_mDBB8CCBB7750C67BE2F2D92F47E6C0FA42793260(L_1, _stringLiteral356A192B7913B04C54574D18C28D46E6395428AB, /*hidden argument*/NULL);
		goto IL_001f;
	}

IL_0013:
	{
		// else builder.Append("_");
		StringBuilder_t * L_2 = ___builder1;
		NullCheck(L_2);
		StringBuilder_Append_mDBB8CCBB7750C67BE2F2D92F47E6C0FA42793260(L_2, _stringLiteral53A0ACFAD59379B3E050338BF9F23CFC172EE787, /*hidden argument*/NULL);
	}

IL_001f:
	{
		// if ((buttons & Pointer.PointerButtonState.SecondButtonPressed) != 0) builder.Append("2");
		int32_t L_3 = ___buttons0;
		if (!((int32_t)((int32_t)L_3&(int32_t)2)))
		{
			goto IL_0032;
		}
	}
	{
		// if ((buttons & Pointer.PointerButtonState.SecondButtonPressed) != 0) builder.Append("2");
		StringBuilder_t * L_4 = ___builder1;
		NullCheck(L_4);
		StringBuilder_Append_mDBB8CCBB7750C67BE2F2D92F47E6C0FA42793260(L_4, _stringLiteralDA4B9237BACCCDF19C0760CAB7AEC4A8359010B0, /*hidden argument*/NULL);
		goto IL_003e;
	}

IL_0032:
	{
		// else builder.Append("_");
		StringBuilder_t * L_5 = ___builder1;
		NullCheck(L_5);
		StringBuilder_Append_mDBB8CCBB7750C67BE2F2D92F47E6C0FA42793260(L_5, _stringLiteral53A0ACFAD59379B3E050338BF9F23CFC172EE787, /*hidden argument*/NULL);
	}

IL_003e:
	{
		// if ((buttons & Pointer.PointerButtonState.ThirdButtonPressed) != 0) builder.Append("3");
		int32_t L_6 = ___buttons0;
		if (!((int32_t)((int32_t)L_6&(int32_t)4)))
		{
			goto IL_0051;
		}
	}
	{
		// if ((buttons & Pointer.PointerButtonState.ThirdButtonPressed) != 0) builder.Append("3");
		StringBuilder_t * L_7 = ___builder1;
		NullCheck(L_7);
		StringBuilder_Append_mDBB8CCBB7750C67BE2F2D92F47E6C0FA42793260(L_7, _stringLiteral77DE68DAECD823BABBB58EDB1C8E14D7106E83BB, /*hidden argument*/NULL);
		goto IL_005d;
	}

IL_0051:
	{
		// else builder.Append("_");
		StringBuilder_t * L_8 = ___builder1;
		NullCheck(L_8);
		StringBuilder_Append_mDBB8CCBB7750C67BE2F2D92F47E6C0FA42793260(L_8, _stringLiteral53A0ACFAD59379B3E050338BF9F23CFC172EE787, /*hidden argument*/NULL);
	}

IL_005d:
	{
		// if ((buttons & Pointer.PointerButtonState.FourthButtonPressed) != 0) builder.Append("4");
		int32_t L_9 = ___buttons0;
		if (!((int32_t)((int32_t)L_9&(int32_t)8)))
		{
			goto IL_0070;
		}
	}
	{
		// if ((buttons & Pointer.PointerButtonState.FourthButtonPressed) != 0) builder.Append("4");
		StringBuilder_t * L_10 = ___builder1;
		NullCheck(L_10);
		StringBuilder_Append_mDBB8CCBB7750C67BE2F2D92F47E6C0FA42793260(L_10, _stringLiteral1B6453892473A467D07372D45EB05ABC2031647A, /*hidden argument*/NULL);
		goto IL_007c;
	}

IL_0070:
	{
		// else builder.Append("_");
		StringBuilder_t * L_11 = ___builder1;
		NullCheck(L_11);
		StringBuilder_Append_mDBB8CCBB7750C67BE2F2D92F47E6C0FA42793260(L_11, _stringLiteral53A0ACFAD59379B3E050338BF9F23CFC172EE787, /*hidden argument*/NULL);
	}

IL_007c:
	{
		// if ((buttons & Pointer.PointerButtonState.FifthButtonPressed) != 0) builder.Append("5");
		int32_t L_12 = ___buttons0;
		if (!((int32_t)((int32_t)L_12&(int32_t)((int32_t)16))))
		{
			goto IL_008f;
		}
	}
	{
		// if ((buttons & Pointer.PointerButtonState.FifthButtonPressed) != 0) builder.Append("5");
		StringBuilder_t * L_13 = ___builder1;
		NullCheck(L_13);
		StringBuilder_Append_mDBB8CCBB7750C67BE2F2D92F47E6C0FA42793260(L_13, _stringLiteralAC3478D69A3C81FA62E60F5C3696165A4E5E6AC4, /*hidden argument*/NULL);
		return;
	}

IL_008f:
	{
		// else builder.Append("_");
		StringBuilder_t * L_14 = ___builder1;
		NullCheck(L_14);
		StringBuilder_Append_mDBB8CCBB7750C67BE2F2D92F47E6C0FA42793260(L_14, _stringLiteral53A0ACFAD59379B3E050338BF9F23CFC172EE787, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.String TouchScript.Utils.PointerUtils::ButtonsToString(TouchScript.Pointers.Pointer_PointerButtonState)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* PointerUtils_ButtonsToString_mEC2274B4CDCC3BC17F12957876CA4760A47815B7 (int32_t ___buttons0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PointerUtils_ButtonsToString_mEC2274B4CDCC3BC17F12957876CA4760A47815B7_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// initStringBuilder();
		PointerUtils_initStringBuilder_mEB6698C4E28B8F6D3E4E40FE5CA452C8415C179C(/*hidden argument*/NULL);
		// ButtonsToString(buttons, sb);
		int32_t L_0 = ___buttons0;
		StringBuilder_t * L_1 = ((PointerUtils_tEB27F0FDEFA565D24AC180439CAE78E49CF1D235_StaticFields*)il2cpp_codegen_static_fields_for(PointerUtils_tEB27F0FDEFA565D24AC180439CAE78E49CF1D235_il2cpp_TypeInfo_var))->get_sb_0();
		PointerUtils_ButtonsToString_m90AE750BDB47BF3343173B2DC737E10AE1BB0C21(L_0, L_1, /*hidden argument*/NULL);
		// return sb.ToString();
		StringBuilder_t * L_2 = ((PointerUtils_tEB27F0FDEFA565D24AC180439CAE78E49CF1D235_StaticFields*)il2cpp_codegen_static_fields_for(PointerUtils_tEB27F0FDEFA565D24AC180439CAE78E49CF1D235_il2cpp_TypeInfo_var))->get_sb_0();
		NullCheck(L_2);
		String_t* L_3 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_2);
		return L_3;
	}
}
// System.Void TouchScript.Utils.PointerUtils::ButtonsToString(TouchScript.Pointers.Pointer_PointerButtonState,System.Text.StringBuilder)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PointerUtils_ButtonsToString_m90AE750BDB47BF3343173B2DC737E10AE1BB0C21 (int32_t ___buttons0, StringBuilder_t * ___builder1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PointerUtils_ButtonsToString_m90AE750BDB47BF3343173B2DC737E10AE1BB0C21_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// if ((buttons & Pointer.PointerButtonState.FirstButtonDown) != 0) builder.Append("v");
		int32_t L_0 = ___buttons0;
		if (!((int32_t)((int32_t)L_0&(int32_t)((int32_t)2048))))
		{
			goto IL_0017;
		}
	}
	{
		// if ((buttons & Pointer.PointerButtonState.FirstButtonDown) != 0) builder.Append("v");
		StringBuilder_t * L_1 = ___builder1;
		NullCheck(L_1);
		StringBuilder_Append_mDBB8CCBB7750C67BE2F2D92F47E6C0FA42793260(L_1, _stringLiteral7A38D8CBD20D9932BA948EFAA364BB62651D5AD4, /*hidden argument*/NULL);
		goto IL_004d;
	}

IL_0017:
	{
		// else if ((buttons & Pointer.PointerButtonState.FirstButtonUp) != 0) builder.Append("^");
		int32_t L_2 = ___buttons0;
		if (!((int32_t)((int32_t)L_2&(int32_t)((int32_t)4096))))
		{
			goto IL_002e;
		}
	}
	{
		// else if ((buttons & Pointer.PointerButtonState.FirstButtonUp) != 0) builder.Append("^");
		StringBuilder_t * L_3 = ___builder1;
		NullCheck(L_3);
		StringBuilder_Append_mDBB8CCBB7750C67BE2F2D92F47E6C0FA42793260(L_3, _stringLiteral5E6F80A34A9798CAFC6A5DB96CC57BA4C4DB59C2, /*hidden argument*/NULL);
		goto IL_004d;
	}

IL_002e:
	{
		// else if ((buttons & Pointer.PointerButtonState.FirstButtonPressed) != 0) builder.Append("1");
		int32_t L_4 = ___buttons0;
		if (!((int32_t)((int32_t)L_4&(int32_t)1)))
		{
			goto IL_0041;
		}
	}
	{
		// else if ((buttons & Pointer.PointerButtonState.FirstButtonPressed) != 0) builder.Append("1");
		StringBuilder_t * L_5 = ___builder1;
		NullCheck(L_5);
		StringBuilder_Append_mDBB8CCBB7750C67BE2F2D92F47E6C0FA42793260(L_5, _stringLiteral356A192B7913B04C54574D18C28D46E6395428AB, /*hidden argument*/NULL);
		goto IL_004d;
	}

IL_0041:
	{
		// else builder.Append("_");
		StringBuilder_t * L_6 = ___builder1;
		NullCheck(L_6);
		StringBuilder_Append_mDBB8CCBB7750C67BE2F2D92F47E6C0FA42793260(L_6, _stringLiteral53A0ACFAD59379B3E050338BF9F23CFC172EE787, /*hidden argument*/NULL);
	}

IL_004d:
	{
		// if ((buttons & Pointer.PointerButtonState.SecondButtonDown) != 0) builder.Append("v");
		int32_t L_7 = ___buttons0;
		if (!((int32_t)((int32_t)L_7&(int32_t)((int32_t)8192))))
		{
			goto IL_0064;
		}
	}
	{
		// if ((buttons & Pointer.PointerButtonState.SecondButtonDown) != 0) builder.Append("v");
		StringBuilder_t * L_8 = ___builder1;
		NullCheck(L_8);
		StringBuilder_Append_mDBB8CCBB7750C67BE2F2D92F47E6C0FA42793260(L_8, _stringLiteral7A38D8CBD20D9932BA948EFAA364BB62651D5AD4, /*hidden argument*/NULL);
		goto IL_009a;
	}

IL_0064:
	{
		// else if ((buttons & Pointer.PointerButtonState.SecondButtonUp) != 0) builder.Append("^");
		int32_t L_9 = ___buttons0;
		if (!((int32_t)((int32_t)L_9&(int32_t)((int32_t)16384))))
		{
			goto IL_007b;
		}
	}
	{
		// else if ((buttons & Pointer.PointerButtonState.SecondButtonUp) != 0) builder.Append("^");
		StringBuilder_t * L_10 = ___builder1;
		NullCheck(L_10);
		StringBuilder_Append_mDBB8CCBB7750C67BE2F2D92F47E6C0FA42793260(L_10, _stringLiteral5E6F80A34A9798CAFC6A5DB96CC57BA4C4DB59C2, /*hidden argument*/NULL);
		goto IL_009a;
	}

IL_007b:
	{
		// else if ((buttons & Pointer.PointerButtonState.SecondButtonPressed) != 0) builder.Append("2");
		int32_t L_11 = ___buttons0;
		if (!((int32_t)((int32_t)L_11&(int32_t)2)))
		{
			goto IL_008e;
		}
	}
	{
		// else if ((buttons & Pointer.PointerButtonState.SecondButtonPressed) != 0) builder.Append("2");
		StringBuilder_t * L_12 = ___builder1;
		NullCheck(L_12);
		StringBuilder_Append_mDBB8CCBB7750C67BE2F2D92F47E6C0FA42793260(L_12, _stringLiteralDA4B9237BACCCDF19C0760CAB7AEC4A8359010B0, /*hidden argument*/NULL);
		goto IL_009a;
	}

IL_008e:
	{
		// else builder.Append("_");
		StringBuilder_t * L_13 = ___builder1;
		NullCheck(L_13);
		StringBuilder_Append_mDBB8CCBB7750C67BE2F2D92F47E6C0FA42793260(L_13, _stringLiteral53A0ACFAD59379B3E050338BF9F23CFC172EE787, /*hidden argument*/NULL);
	}

IL_009a:
	{
		// if ((buttons & Pointer.PointerButtonState.ThirdButtonDown) != 0) builder.Append("v");
		int32_t L_14 = ___buttons0;
		if (!((int32_t)((int32_t)L_14&(int32_t)((int32_t)32768))))
		{
			goto IL_00b1;
		}
	}
	{
		// if ((buttons & Pointer.PointerButtonState.ThirdButtonDown) != 0) builder.Append("v");
		StringBuilder_t * L_15 = ___builder1;
		NullCheck(L_15);
		StringBuilder_Append_mDBB8CCBB7750C67BE2F2D92F47E6C0FA42793260(L_15, _stringLiteral7A38D8CBD20D9932BA948EFAA364BB62651D5AD4, /*hidden argument*/NULL);
		goto IL_00e7;
	}

IL_00b1:
	{
		// else if ((buttons & Pointer.PointerButtonState.ThirdButtonUp) != 0) builder.Append("^");
		int32_t L_16 = ___buttons0;
		if (!((int32_t)((int32_t)L_16&(int32_t)((int32_t)65536))))
		{
			goto IL_00c8;
		}
	}
	{
		// else if ((buttons & Pointer.PointerButtonState.ThirdButtonUp) != 0) builder.Append("^");
		StringBuilder_t * L_17 = ___builder1;
		NullCheck(L_17);
		StringBuilder_Append_mDBB8CCBB7750C67BE2F2D92F47E6C0FA42793260(L_17, _stringLiteral5E6F80A34A9798CAFC6A5DB96CC57BA4C4DB59C2, /*hidden argument*/NULL);
		goto IL_00e7;
	}

IL_00c8:
	{
		// else if ((buttons & Pointer.PointerButtonState.ThirdButtonPressed) != 0) builder.Append("3");
		int32_t L_18 = ___buttons0;
		if (!((int32_t)((int32_t)L_18&(int32_t)4)))
		{
			goto IL_00db;
		}
	}
	{
		// else if ((buttons & Pointer.PointerButtonState.ThirdButtonPressed) != 0) builder.Append("3");
		StringBuilder_t * L_19 = ___builder1;
		NullCheck(L_19);
		StringBuilder_Append_mDBB8CCBB7750C67BE2F2D92F47E6C0FA42793260(L_19, _stringLiteral77DE68DAECD823BABBB58EDB1C8E14D7106E83BB, /*hidden argument*/NULL);
		goto IL_00e7;
	}

IL_00db:
	{
		// else builder.Append("_");
		StringBuilder_t * L_20 = ___builder1;
		NullCheck(L_20);
		StringBuilder_Append_mDBB8CCBB7750C67BE2F2D92F47E6C0FA42793260(L_20, _stringLiteral53A0ACFAD59379B3E050338BF9F23CFC172EE787, /*hidden argument*/NULL);
	}

IL_00e7:
	{
		// if ((buttons & Pointer.PointerButtonState.FourthButtonDown) != 0) builder.Append("v");
		int32_t L_21 = ___buttons0;
		if (!((int32_t)((int32_t)L_21&(int32_t)((int32_t)131072))))
		{
			goto IL_00fe;
		}
	}
	{
		// if ((buttons & Pointer.PointerButtonState.FourthButtonDown) != 0) builder.Append("v");
		StringBuilder_t * L_22 = ___builder1;
		NullCheck(L_22);
		StringBuilder_Append_mDBB8CCBB7750C67BE2F2D92F47E6C0FA42793260(L_22, _stringLiteral7A38D8CBD20D9932BA948EFAA364BB62651D5AD4, /*hidden argument*/NULL);
		goto IL_0134;
	}

IL_00fe:
	{
		// else if ((buttons & Pointer.PointerButtonState.FourthButtonUp) != 0) builder.Append("^");
		int32_t L_23 = ___buttons0;
		if (!((int32_t)((int32_t)L_23&(int32_t)((int32_t)262144))))
		{
			goto IL_0115;
		}
	}
	{
		// else if ((buttons & Pointer.PointerButtonState.FourthButtonUp) != 0) builder.Append("^");
		StringBuilder_t * L_24 = ___builder1;
		NullCheck(L_24);
		StringBuilder_Append_mDBB8CCBB7750C67BE2F2D92F47E6C0FA42793260(L_24, _stringLiteral5E6F80A34A9798CAFC6A5DB96CC57BA4C4DB59C2, /*hidden argument*/NULL);
		goto IL_0134;
	}

IL_0115:
	{
		// else if ((buttons & Pointer.PointerButtonState.FourthButtonPressed) != 0) builder.Append("4");
		int32_t L_25 = ___buttons0;
		if (!((int32_t)((int32_t)L_25&(int32_t)8)))
		{
			goto IL_0128;
		}
	}
	{
		// else if ((buttons & Pointer.PointerButtonState.FourthButtonPressed) != 0) builder.Append("4");
		StringBuilder_t * L_26 = ___builder1;
		NullCheck(L_26);
		StringBuilder_Append_mDBB8CCBB7750C67BE2F2D92F47E6C0FA42793260(L_26, _stringLiteral1B6453892473A467D07372D45EB05ABC2031647A, /*hidden argument*/NULL);
		goto IL_0134;
	}

IL_0128:
	{
		// else builder.Append("_");
		StringBuilder_t * L_27 = ___builder1;
		NullCheck(L_27);
		StringBuilder_Append_mDBB8CCBB7750C67BE2F2D92F47E6C0FA42793260(L_27, _stringLiteral53A0ACFAD59379B3E050338BF9F23CFC172EE787, /*hidden argument*/NULL);
	}

IL_0134:
	{
		// if ((buttons & Pointer.PointerButtonState.FifthButtonDown) != 0) builder.Append("v");
		int32_t L_28 = ___buttons0;
		if (!((int32_t)((int32_t)L_28&(int32_t)((int32_t)524288))))
		{
			goto IL_014a;
		}
	}
	{
		// if ((buttons & Pointer.PointerButtonState.FifthButtonDown) != 0) builder.Append("v");
		StringBuilder_t * L_29 = ___builder1;
		NullCheck(L_29);
		StringBuilder_Append_mDBB8CCBB7750C67BE2F2D92F47E6C0FA42793260(L_29, _stringLiteral7A38D8CBD20D9932BA948EFAA364BB62651D5AD4, /*hidden argument*/NULL);
		return;
	}

IL_014a:
	{
		// else if ((buttons & Pointer.PointerButtonState.FifthButtonUp) != 0) builder.Append("^");
		int32_t L_30 = ___buttons0;
		if (!((int32_t)((int32_t)L_30&(int32_t)((int32_t)1048576))))
		{
			goto IL_0160;
		}
	}
	{
		// else if ((buttons & Pointer.PointerButtonState.FifthButtonUp) != 0) builder.Append("^");
		StringBuilder_t * L_31 = ___builder1;
		NullCheck(L_31);
		StringBuilder_Append_mDBB8CCBB7750C67BE2F2D92F47E6C0FA42793260(L_31, _stringLiteral5E6F80A34A9798CAFC6A5DB96CC57BA4C4DB59C2, /*hidden argument*/NULL);
		return;
	}

IL_0160:
	{
		// else if ((buttons & Pointer.PointerButtonState.FifthButtonPressed) != 0) builder.Append("5");
		int32_t L_32 = ___buttons0;
		if (!((int32_t)((int32_t)L_32&(int32_t)((int32_t)16))))
		{
			goto IL_0173;
		}
	}
	{
		// else if ((buttons & Pointer.PointerButtonState.FifthButtonPressed) != 0) builder.Append("5");
		StringBuilder_t * L_33 = ___builder1;
		NullCheck(L_33);
		StringBuilder_Append_mDBB8CCBB7750C67BE2F2D92F47E6C0FA42793260(L_33, _stringLiteralAC3478D69A3C81FA62E60F5C3696165A4E5E6AC4, /*hidden argument*/NULL);
		return;
	}

IL_0173:
	{
		// else builder.Append("_");
		StringBuilder_t * L_34 = ___builder1;
		NullCheck(L_34);
		StringBuilder_Append_mDBB8CCBB7750C67BE2F2D92F47E6C0FA42793260(L_34, _stringLiteral53A0ACFAD59379B3E050338BF9F23CFC172EE787, /*hidden argument*/NULL);
		// }
		return;
	}
}
// TouchScript.Pointers.Pointer_PointerButtonState TouchScript.Utils.PointerUtils::DownPressedButtons(TouchScript.Pointers.Pointer_PointerButtonState)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t PointerUtils_DownPressedButtons_mA925780101477141AEADCBD36F73DBA3EF36816E (int32_t ___buttons0, const RuntimeMethod* method)
{
	{
		// if ((buttons & Pointer.PointerButtonState.FirstButtonPressed) != 0)
		int32_t L_0 = ___buttons0;
		if (!((int32_t)((int32_t)L_0&(int32_t)1)))
		{
			goto IL_000e;
		}
	}
	{
		// buttons |= Pointer.PointerButtonState.FirstButtonDown;
		int32_t L_1 = ___buttons0;
		___buttons0 = ((int32_t)((int32_t)L_1|(int32_t)((int32_t)2048)));
	}

IL_000e:
	{
		// if ((buttons & Pointer.PointerButtonState.SecondButtonPressed) != 0)
		int32_t L_2 = ___buttons0;
		if (!((int32_t)((int32_t)L_2&(int32_t)2)))
		{
			goto IL_001c;
		}
	}
	{
		// buttons |= Pointer.PointerButtonState.SecondButtonDown;
		int32_t L_3 = ___buttons0;
		___buttons0 = ((int32_t)((int32_t)L_3|(int32_t)((int32_t)8192)));
	}

IL_001c:
	{
		// if ((buttons & Pointer.PointerButtonState.ThirdButtonPressed) != 0)
		int32_t L_4 = ___buttons0;
		if (!((int32_t)((int32_t)L_4&(int32_t)4)))
		{
			goto IL_002a;
		}
	}
	{
		// buttons |= Pointer.PointerButtonState.ThirdButtonDown;
		int32_t L_5 = ___buttons0;
		___buttons0 = ((int32_t)((int32_t)L_5|(int32_t)((int32_t)32768)));
	}

IL_002a:
	{
		// if ((buttons & Pointer.PointerButtonState.FourthButtonPressed) != 0)
		int32_t L_6 = ___buttons0;
		if (!((int32_t)((int32_t)L_6&(int32_t)8)))
		{
			goto IL_0038;
		}
	}
	{
		// buttons |= Pointer.PointerButtonState.FourthButtonDown;
		int32_t L_7 = ___buttons0;
		___buttons0 = ((int32_t)((int32_t)L_7|(int32_t)((int32_t)131072)));
	}

IL_0038:
	{
		// if ((buttons & Pointer.PointerButtonState.FifthButtonPressed) != 0)
		int32_t L_8 = ___buttons0;
		if (!((int32_t)((int32_t)L_8&(int32_t)((int32_t)16))))
		{
			goto IL_0047;
		}
	}
	{
		// buttons |= Pointer.PointerButtonState.FifthButtonDown;
		int32_t L_9 = ___buttons0;
		___buttons0 = ((int32_t)((int32_t)L_9|(int32_t)((int32_t)524288)));
	}

IL_0047:
	{
		// return buttons;
		int32_t L_10 = ___buttons0;
		return L_10;
	}
}
// TouchScript.Pointers.Pointer_PointerButtonState TouchScript.Utils.PointerUtils::PressDownButtons(TouchScript.Pointers.Pointer_PointerButtonState)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t PointerUtils_PressDownButtons_mA71150EFD6A6F014898A56D35B72049AC7E569DD (int32_t ___buttons0, const RuntimeMethod* method)
{
	{
		// if ((buttons & Pointer.PointerButtonState.FirstButtonDown) != 0)
		int32_t L_0 = ___buttons0;
		if (!((int32_t)((int32_t)L_0&(int32_t)((int32_t)2048))))
		{
			goto IL_000e;
		}
	}
	{
		// buttons |= Pointer.PointerButtonState.FirstButtonPressed;
		int32_t L_1 = ___buttons0;
		___buttons0 = ((int32_t)((int32_t)L_1|(int32_t)1));
	}

IL_000e:
	{
		// if ((buttons & Pointer.PointerButtonState.SecondButtonDown) != 0)
		int32_t L_2 = ___buttons0;
		if (!((int32_t)((int32_t)L_2&(int32_t)((int32_t)8192))))
		{
			goto IL_001c;
		}
	}
	{
		// buttons |= Pointer.PointerButtonState.SecondButtonPressed;
		int32_t L_3 = ___buttons0;
		___buttons0 = ((int32_t)((int32_t)L_3|(int32_t)2));
	}

IL_001c:
	{
		// if ((buttons & Pointer.PointerButtonState.ThirdButtonDown) != 0)
		int32_t L_4 = ___buttons0;
		if (!((int32_t)((int32_t)L_4&(int32_t)((int32_t)32768))))
		{
			goto IL_002a;
		}
	}
	{
		// buttons |= Pointer.PointerButtonState.ThirdButtonPressed;
		int32_t L_5 = ___buttons0;
		___buttons0 = ((int32_t)((int32_t)L_5|(int32_t)4));
	}

IL_002a:
	{
		// if ((buttons & Pointer.PointerButtonState.FourthButtonDown) != 0)
		int32_t L_6 = ___buttons0;
		if (!((int32_t)((int32_t)L_6&(int32_t)((int32_t)131072))))
		{
			goto IL_0038;
		}
	}
	{
		// buttons |= Pointer.PointerButtonState.FourthButtonPressed;
		int32_t L_7 = ___buttons0;
		___buttons0 = ((int32_t)((int32_t)L_7|(int32_t)8));
	}

IL_0038:
	{
		// if ((buttons & Pointer.PointerButtonState.FifthButtonDown) != 0)
		int32_t L_8 = ___buttons0;
		if (!((int32_t)((int32_t)L_8&(int32_t)((int32_t)524288))))
		{
			goto IL_0047;
		}
	}
	{
		// buttons |= Pointer.PointerButtonState.FifthButtonPressed;
		int32_t L_9 = ___buttons0;
		___buttons0 = ((int32_t)((int32_t)L_9|(int32_t)((int32_t)16)));
	}

IL_0047:
	{
		// return buttons;
		int32_t L_10 = ___buttons0;
		return L_10;
	}
}
// TouchScript.Pointers.Pointer_PointerButtonState TouchScript.Utils.PointerUtils::UpPressedButtons(TouchScript.Pointers.Pointer_PointerButtonState)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t PointerUtils_UpPressedButtons_mE54D033069BCF06BA90CBE76507B340ACC4F58DC (int32_t ___buttons0, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	{
		// var btns = Pointer.PointerButtonState.Nothing;
		V_0 = 0;
		// if ((buttons & Pointer.PointerButtonState.FirstButtonPressed) != 0)
		int32_t L_0 = ___buttons0;
		if (!((int32_t)((int32_t)L_0&(int32_t)1)))
		{
			goto IL_000f;
		}
	}
	{
		// btns |= Pointer.PointerButtonState.FirstButtonUp;
		int32_t L_1 = V_0;
		V_0 = ((int32_t)((int32_t)L_1|(int32_t)((int32_t)4096)));
	}

IL_000f:
	{
		// if ((buttons & Pointer.PointerButtonState.SecondButtonPressed) != 0)
		int32_t L_2 = ___buttons0;
		if (!((int32_t)((int32_t)L_2&(int32_t)2)))
		{
			goto IL_001c;
		}
	}
	{
		// btns |= Pointer.PointerButtonState.SecondButtonUp;
		int32_t L_3 = V_0;
		V_0 = ((int32_t)((int32_t)L_3|(int32_t)((int32_t)16384)));
	}

IL_001c:
	{
		// if ((buttons & Pointer.PointerButtonState.ThirdButtonPressed) != 0)
		int32_t L_4 = ___buttons0;
		if (!((int32_t)((int32_t)L_4&(int32_t)4)))
		{
			goto IL_0029;
		}
	}
	{
		// btns |= Pointer.PointerButtonState.ThirdButtonUp;
		int32_t L_5 = V_0;
		V_0 = ((int32_t)((int32_t)L_5|(int32_t)((int32_t)65536)));
	}

IL_0029:
	{
		// if ((buttons & Pointer.PointerButtonState.FourthButtonPressed) != 0)
		int32_t L_6 = ___buttons0;
		if (!((int32_t)((int32_t)L_6&(int32_t)8)))
		{
			goto IL_0036;
		}
	}
	{
		// btns |= Pointer.PointerButtonState.FourthButtonUp;
		int32_t L_7 = V_0;
		V_0 = ((int32_t)((int32_t)L_7|(int32_t)((int32_t)262144)));
	}

IL_0036:
	{
		// if ((buttons & Pointer.PointerButtonState.FifthButtonPressed) != 0)
		int32_t L_8 = ___buttons0;
		if (!((int32_t)((int32_t)L_8&(int32_t)((int32_t)16))))
		{
			goto IL_0044;
		}
	}
	{
		// btns |= Pointer.PointerButtonState.FifthButtonUp;
		int32_t L_9 = V_0;
		V_0 = ((int32_t)((int32_t)L_9|(int32_t)((int32_t)1048576)));
	}

IL_0044:
	{
		// return btns;
		int32_t L_10 = V_0;
		return L_10;
	}
}
// System.Void TouchScript.Utils.PointerUtils::initStringBuilder()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PointerUtils_initStringBuilder_mEB6698C4E28B8F6D3E4E40FE5CA452C8415C179C (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PointerUtils_initStringBuilder_mEB6698C4E28B8F6D3E4E40FE5CA452C8415C179C_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// if (sb == null) sb = new StringBuilder();
		StringBuilder_t * L_0 = ((PointerUtils_tEB27F0FDEFA565D24AC180439CAE78E49CF1D235_StaticFields*)il2cpp_codegen_static_fields_for(PointerUtils_tEB27F0FDEFA565D24AC180439CAE78E49CF1D235_il2cpp_TypeInfo_var))->get_sb_0();
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		// if (sb == null) sb = new StringBuilder();
		StringBuilder_t * L_1 = (StringBuilder_t *)il2cpp_codegen_object_new(StringBuilder_t_il2cpp_TypeInfo_var);
		StringBuilder__ctor_mF928376F82E8C8FF3C11842C562DB8CF28B2735E(L_1, /*hidden argument*/NULL);
		((PointerUtils_tEB27F0FDEFA565D24AC180439CAE78E49CF1D235_StaticFields*)il2cpp_codegen_static_fields_for(PointerUtils_tEB27F0FDEFA565D24AC180439CAE78E49CF1D235_il2cpp_TypeInfo_var))->set_sb_0(L_1);
	}

IL_0011:
	{
		// sb.Length = 0;
		StringBuilder_t * L_2 = ((PointerUtils_tEB27F0FDEFA565D24AC180439CAE78E49CF1D235_StaticFields*)il2cpp_codegen_static_fields_for(PointerUtils_tEB27F0FDEFA565D24AC180439CAE78E49CF1D235_il2cpp_TypeInfo_var))->get_sb_0();
		NullCheck(L_2);
		StringBuilder_set_Length_m84AF318230AE5C3D0D48F1CE7C2170F6F5C19F5B(L_2, 0, /*hidden argument*/NULL);
		// }
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// UnityEngine.Vector3 TouchScript.Utils.TransformUtils::GlobalToLocalPosition(UnityEngine.Transform,UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  TransformUtils_GlobalToLocalPosition_m83D494F04656DBAD08C6443DEDE9A014171743FD (Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ___transform0, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___global1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TransformUtils_GlobalToLocalPosition_m83D494F04656DBAD08C6443DEDE9A014171743FD_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// if (transform.parent == null) return global;
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_0 = ___transform0;
		NullCheck(L_0);
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_1 = Transform_get_parent_m8FA24E38A1FA29D90CBF3CDC9F9F017C65BB3403(L_0, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_il2cpp_TypeInfo_var);
		bool L_2 = Object_op_Equality_mBC2401774F3BE33E8CF6F0A8148E66C95D6CFF1C(L_1, (Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 *)NULL, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0010;
		}
	}
	{
		// if (transform.parent == null) return global;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_3 = ___global1;
		return L_3;
	}

IL_0010:
	{
		// return transform.parent.InverseTransformPoint(global);
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_4 = ___transform0;
		NullCheck(L_4);
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_5 = Transform_get_parent_m8FA24E38A1FA29D90CBF3CDC9F9F017C65BB3403(L_4, /*hidden argument*/NULL);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_6 = ___global1;
		NullCheck(L_5);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_7 = Transform_InverseTransformPoint_mB6E3145F20B531B4A781C194BAC43A8255C96C47(L_5, L_6, /*hidden argument*/NULL);
		return L_7;
	}
}
// UnityEngine.Vector3 TouchScript.Utils.TransformUtils::GlobalToLocalDirection(UnityEngine.Transform,UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  TransformUtils_GlobalToLocalDirection_m51021C8A547001E01BEFD522426645789F7F7C76 (Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ___transform0, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___global1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TransformUtils_GlobalToLocalDirection_m51021C8A547001E01BEFD522426645789F7F7C76_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// if (transform.parent == null) return global;
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_0 = ___transform0;
		NullCheck(L_0);
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_1 = Transform_get_parent_m8FA24E38A1FA29D90CBF3CDC9F9F017C65BB3403(L_0, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_il2cpp_TypeInfo_var);
		bool L_2 = Object_op_Equality_mBC2401774F3BE33E8CF6F0A8148E66C95D6CFF1C(L_1, (Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 *)NULL, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0010;
		}
	}
	{
		// if (transform.parent == null) return global;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_3 = ___global1;
		return L_3;
	}

IL_0010:
	{
		// return transform.parent.InverseTransformDirection(global);
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_4 = ___transform0;
		NullCheck(L_4);
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_5 = Transform_get_parent_m8FA24E38A1FA29D90CBF3CDC9F9F017C65BB3403(L_4, /*hidden argument*/NULL);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_6 = ___global1;
		NullCheck(L_5);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_7 = Transform_InverseTransformDirection_m6F0513F2EC19C204F2077E3C68DD1D45317CB5F2(L_5, L_6, /*hidden argument*/NULL);
		return L_7;
	}
}
// UnityEngine.Vector3 TouchScript.Utils.TransformUtils::GlobalToLocalVector(UnityEngine.Transform,UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  TransformUtils_GlobalToLocalVector_m57DE02E94AFA43DD446DD9C16B46140FADCF1003 (Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ___transform0, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___global1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TransformUtils_GlobalToLocalVector_m57DE02E94AFA43DD446DD9C16B46140FADCF1003_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * V_0 = NULL;
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  V_1;
	memset((&V_1), 0, sizeof(V_1));
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  V_2;
	memset((&V_2), 0, sizeof(V_2));
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * G_B4_0 = NULL;
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * G_B3_0 = NULL;
	float G_B5_0 = 0.0f;
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * G_B5_1 = NULL;
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * G_B7_0 = NULL;
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * G_B6_0 = NULL;
	float G_B8_0 = 0.0f;
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * G_B8_1 = NULL;
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * G_B10_0 = NULL;
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * G_B9_0 = NULL;
	float G_B11_0 = 0.0f;
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * G_B11_1 = NULL;
	{
		// var parent = transform.parent;
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_0 = ___transform0;
		NullCheck(L_0);
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_1 = Transform_get_parent_m8FA24E38A1FA29D90CBF3CDC9F9F017C65BB3403(L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		// if (parent == null) return global;
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_2 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_il2cpp_TypeInfo_var);
		bool L_3 = Object_op_Equality_mBC2401774F3BE33E8CF6F0A8148E66C95D6CFF1C(L_2, (Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 *)NULL, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0012;
		}
	}
	{
		// if (parent == null) return global;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_4 = ___global1;
		return L_4;
	}

IL_0012:
	{
		// var scale = parent.localScale;
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_5 = V_0;
		NullCheck(L_5);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_6 = Transform_get_localScale_mD8F631021C2D62B7C341B1A17FA75491F64E13DA(L_5, /*hidden argument*/NULL);
		V_1 = L_6;
		// var vector = GlobalToLocalVector(parent, global);
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_7 = V_0;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_8 = ___global1;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_9 = TransformUtils_GlobalToLocalVector_m57DE02E94AFA43DD446DD9C16B46140FADCF1003(L_7, L_8, /*hidden argument*/NULL);
		V_2 = L_9;
		// vector = Quaternion.Inverse(parent.localRotation) * vector;
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_10 = V_0;
		NullCheck(L_10);
		Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  L_11 = Transform_get_localRotation_mEDA319E1B42EF12A19A95AC0824345B6574863FE(L_10, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357_il2cpp_TypeInfo_var);
		Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  L_12 = Quaternion_Inverse_mC3A78571A826F05CE179637E675BD25F8B203E0C(L_11, /*hidden argument*/NULL);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_13 = V_2;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_14 = Quaternion_op_Multiply_mD5999DE317D808808B72E58E7A978C4C0995879C(L_12, L_13, /*hidden argument*/NULL);
		V_2 = L_14;
		// vector.x = Mathf.Approximately(scale.x, 0) ? 0 : vector.x / scale.x;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_15 = V_1;
		float L_16 = L_15.get_x_2();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_tFBDE6467D269BFE410605C7D806FD9991D4A89CB_il2cpp_TypeInfo_var);
		bool L_17 = Mathf_Approximately_m91AF00403E0D2DEA1AAE68601AD218CFAD70DF7E(L_16, (0.0f), /*hidden argument*/NULL);
		G_B3_0 = (&V_2);
		if (L_17)
		{
			G_B4_0 = (&V_2);
			goto IL_0056;
		}
	}
	{
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_18 = V_2;
		float L_19 = L_18.get_x_2();
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_20 = V_1;
		float L_21 = L_20.get_x_2();
		G_B5_0 = ((float)((float)L_19/(float)L_21));
		G_B5_1 = G_B3_0;
		goto IL_005b;
	}

IL_0056:
	{
		G_B5_0 = (0.0f);
		G_B5_1 = G_B4_0;
	}

IL_005b:
	{
		G_B5_1->set_x_2(G_B5_0);
		// vector.y = Mathf.Approximately(scale.y, 0) ? 0 : vector.y / scale.y;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_22 = V_1;
		float L_23 = L_22.get_y_3();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_tFBDE6467D269BFE410605C7D806FD9991D4A89CB_il2cpp_TypeInfo_var);
		bool L_24 = Mathf_Approximately_m91AF00403E0D2DEA1AAE68601AD218CFAD70DF7E(L_23, (0.0f), /*hidden argument*/NULL);
		G_B6_0 = (&V_2);
		if (L_24)
		{
			G_B7_0 = (&V_2);
			goto IL_0083;
		}
	}
	{
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_25 = V_2;
		float L_26 = L_25.get_y_3();
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_27 = V_1;
		float L_28 = L_27.get_y_3();
		G_B8_0 = ((float)((float)L_26/(float)L_28));
		G_B8_1 = G_B6_0;
		goto IL_0088;
	}

IL_0083:
	{
		G_B8_0 = (0.0f);
		G_B8_1 = G_B7_0;
	}

IL_0088:
	{
		G_B8_1->set_y_3(G_B8_0);
		// vector.z = Mathf.Approximately(scale.z, 0) ? 0 : vector.z / scale.z;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_29 = V_1;
		float L_30 = L_29.get_z_4();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_tFBDE6467D269BFE410605C7D806FD9991D4A89CB_il2cpp_TypeInfo_var);
		bool L_31 = Mathf_Approximately_m91AF00403E0D2DEA1AAE68601AD218CFAD70DF7E(L_30, (0.0f), /*hidden argument*/NULL);
		G_B9_0 = (&V_2);
		if (L_31)
		{
			G_B10_0 = (&V_2);
			goto IL_00b0;
		}
	}
	{
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_32 = V_2;
		float L_33 = L_32.get_z_4();
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_34 = V_1;
		float L_35 = L_34.get_z_4();
		G_B11_0 = ((float)((float)L_33/(float)L_35));
		G_B11_1 = G_B9_0;
		goto IL_00b5;
	}

IL_00b0:
	{
		G_B11_0 = (0.0f);
		G_B11_1 = G_B10_0;
	}

IL_00b5:
	{
		G_B11_1->set_z_4(G_B11_0);
		// return vector;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_36 = V_2;
		return L_36;
	}
}
// System.String TouchScript.Utils.TransformUtils::GetHeirarchyPath(UnityEngine.Transform)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* TransformUtils_GetHeirarchyPath_m0AEF337A0A5E5D3DD2686702E34749D3AC63B0E5 (Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ___transform0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TransformUtils_GetHeirarchyPath_m0AEF337A0A5E5D3DD2686702E34749D3AC63B0E5_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// initStringBuilder();
		TransformUtils_initStringBuilder_m6846E6CB8306F423AC924826D4D12667ED674C99(/*hidden argument*/NULL);
		// if (transform == null) return null;
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_0 = ___transform0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Equality_mBC2401774F3BE33E8CF6F0A8148E66C95D6CFF1C(L_0, (Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_003b;
		}
	}
	{
		// if (transform == null) return null;
		return (String_t*)NULL;
	}

IL_0010:
	{
		// sb.Insert(0, transform.name);
		StringBuilder_t * L_2 = ((TransformUtils_tEC6DF78714A4FEB9B1F27B298B574CF82DE1C680_StaticFields*)il2cpp_codegen_static_fields_for(TransformUtils_tEC6DF78714A4FEB9B1F27B298B574CF82DE1C680_il2cpp_TypeInfo_var))->get_sb_0();
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_3 = ___transform0;
		NullCheck(L_3);
		String_t* L_4 = Object_get_name_mA2D400141CB3C991C87A2556429781DE961A83CE(L_3, /*hidden argument*/NULL);
		NullCheck(L_2);
		StringBuilder_Insert_m38829D9C9FE52ACD6541ED735D4435FB2A831A2C(L_2, 0, L_4, /*hidden argument*/NULL);
		// sb.Insert(0, "/");
		StringBuilder_t * L_5 = ((TransformUtils_tEC6DF78714A4FEB9B1F27B298B574CF82DE1C680_StaticFields*)il2cpp_codegen_static_fields_for(TransformUtils_tEC6DF78714A4FEB9B1F27B298B574CF82DE1C680_il2cpp_TypeInfo_var))->get_sb_0();
		NullCheck(L_5);
		StringBuilder_Insert_m38829D9C9FE52ACD6541ED735D4435FB2A831A2C(L_5, 0, _stringLiteral42099B4AF021E53FD8FD4E056C2568D7C2E3FFA8, /*hidden argument*/NULL);
		// transform = transform.parent;
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_6 = ___transform0;
		NullCheck(L_6);
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_7 = Transform_get_parent_m8FA24E38A1FA29D90CBF3CDC9F9F017C65BB3403(L_6, /*hidden argument*/NULL);
		___transform0 = L_7;
	}

IL_003b:
	{
		// while (transform != null)
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_8 = ___transform0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_il2cpp_TypeInfo_var);
		bool L_9 = Object_op_Inequality_m31EF58E217E8F4BDD3E409DEF79E1AEE95874FC1(L_8, (Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 *)NULL, /*hidden argument*/NULL);
		if (L_9)
		{
			goto IL_0010;
		}
	}
	{
		// return sb.ToString();
		StringBuilder_t * L_10 = ((TransformUtils_tEC6DF78714A4FEB9B1F27B298B574CF82DE1C680_StaticFields*)il2cpp_codegen_static_fields_for(TransformUtils_tEC6DF78714A4FEB9B1F27B298B574CF82DE1C680_il2cpp_TypeInfo_var))->get_sb_0();
		NullCheck(L_10);
		String_t* L_11 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_10);
		return L_11;
	}
}
// System.Void TouchScript.Utils.TransformUtils::initStringBuilder()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TransformUtils_initStringBuilder_m6846E6CB8306F423AC924826D4D12667ED674C99 (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TransformUtils_initStringBuilder_m6846E6CB8306F423AC924826D4D12667ED674C99_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// if (sb == null) sb = new StringBuilder();
		StringBuilder_t * L_0 = ((TransformUtils_tEC6DF78714A4FEB9B1F27B298B574CF82DE1C680_StaticFields*)il2cpp_codegen_static_fields_for(TransformUtils_tEC6DF78714A4FEB9B1F27B298B574CF82DE1C680_il2cpp_TypeInfo_var))->get_sb_0();
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		// if (sb == null) sb = new StringBuilder();
		StringBuilder_t * L_1 = (StringBuilder_t *)il2cpp_codegen_object_new(StringBuilder_t_il2cpp_TypeInfo_var);
		StringBuilder__ctor_mF928376F82E8C8FF3C11842C562DB8CF28B2735E(L_1, /*hidden argument*/NULL);
		((TransformUtils_tEC6DF78714A4FEB9B1F27B298B574CF82DE1C680_StaticFields*)il2cpp_codegen_static_fields_for(TransformUtils_tEC6DF78714A4FEB9B1F27B298B574CF82DE1C680_il2cpp_TypeInfo_var))->set_sb_0(L_1);
	}

IL_0011:
	{
		// sb.Length = 0;
		StringBuilder_t * L_2 = ((TransformUtils_tEC6DF78714A4FEB9B1F27B298B574CF82DE1C680_StaticFields*)il2cpp_codegen_static_fields_for(TransformUtils_tEC6DF78714A4FEB9B1F27B298B574CF82DE1C680_il2cpp_TypeInfo_var))->get_sb_0();
		NullCheck(L_2);
		StringBuilder_set_Length_m84AF318230AE5C3D0D48F1CE7C2170F6F5C19F5B(L_2, 0, /*hidden argument*/NULL);
		// }
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void imageBank::Start()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void imageBank_Start_m0FA24C2C5260EBC0A56D06CD47E23E7EC25E8764 (imageBank_t5EEBC6CE9144A46186F0114E6CE8B23ACDC4CCE3 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (imageBank_Start_m0FA24C2C5260EBC0A56D06CD47E23E7EC25E8764_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// oldQuad = GameObject.FindGameObjectWithTag("NewBDQuad");
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_0 = GameObject_FindGameObjectWithTag_m9F2877F52346B973DE3023209D73852E96ECC10D(_stringLiteral3C0D5845A80675A696F495CA84A88CFD41556CBA, /*hidden argument*/NULL);
		__this->set_oldQuad_4(L_0);
		// }
		return;
	}
}
// System.Void imageBank::Save()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void imageBank_Save_mD64E958D323A334B2ED50F5261B037EDAA1B6F9E (imageBank_t5EEBC6CE9144A46186F0114E6CE8B23ACDC4CCE3 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (imageBank_Save_mD64E958D323A334B2ED50F5261B037EDAA1B6F9E_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * V_0 = NULL;
	int32_t V_1 = 0;
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* V_2 = NULL;
	{
		// Texture2D texture = (Texture2D)oldQuad.GetComponent<MeshRenderer>().material.mainTexture;
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_0 = __this->get_oldQuad_4();
		NullCheck(L_0);
		MeshRenderer_t9D67CA54E83315F743623BDE8EADCD5074659EED * L_1 = GameObject_GetComponent_TisMeshRenderer_t9D67CA54E83315F743623BDE8EADCD5074659EED_m91C1D9637A332988C72E62D52DFCFE89A6DDAB72(L_0, /*hidden argument*/GameObject_GetComponent_TisMeshRenderer_t9D67CA54E83315F743623BDE8EADCD5074659EED_m91C1D9637A332988C72E62D52DFCFE89A6DDAB72_RuntimeMethod_var);
		NullCheck(L_1);
		Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * L_2 = Renderer_get_material_m4434513446B652652CE9FD766B0E3D1D34C4A617(L_1, /*hidden argument*/NULL);
		NullCheck(L_2);
		Texture_t387FE83BB848001FD06B14707AEA6D5A0F6A95F4 * L_3 = Material_get_mainTexture_mE85CF647728AD145D7E03A172EFD5930773E514E(L_2, /*hidden argument*/NULL);
		V_0 = ((Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C *)CastclassSealed((RuntimeObject*)L_3, Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C_il2cpp_TypeInfo_var));
		// int num = 1;
		V_1 = 1;
		// if (texture)
		Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * L_4 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_il2cpp_TypeInfo_var);
		bool L_5 = Object_op_Implicit_m8B2A44B4B1406ED346D1AE6D962294FD58D0D534(L_4, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_00aa;
		}
	}
	{
		goto IL_002e;
	}

IL_002a:
	{
		// num++;
		int32_t L_6 = V_1;
		V_1 = ((int32_t)il2cpp_codegen_add((int32_t)L_6, (int32_t)1));
	}

IL_002e:
	{
		// while(File.Exists(Application.persistentDataPath + "/image" + num.ToString() + ".png")){
		String_t* L_7 = Application_get_persistentDataPath_m82E34156D8BD0A55CAC258CDFE8317FAD6945F5B(/*hidden argument*/NULL);
		String_t* L_8 = Int32_ToString_m1863896DE712BF97C031D55B12E1583F1982DC02((int32_t*)(&V_1), /*hidden argument*/NULL);
		String_t* L_9 = String_Concat_mDD2E38332DED3A8C088D38D78A0E0BEB5091DA64(L_7, _stringLiteralB8D68D9750E37D43B20BDCF6675828CDB80E7B77, L_8, _stringLiteral339F5868588CFC8FDA4A1EC94EAD8F737C5FCFC0, /*hidden argument*/NULL);
		bool L_10 = File_Exists_m6B9BDD8EEB33D744EB0590DD27BC0152FAFBD1FB(L_9, /*hidden argument*/NULL);
		if (L_10)
		{
			goto IL_002a;
		}
	}
	{
		// byte[] imbytes = texture.EncodeToPNG();
		Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * L_11 = V_0;
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_12 = ImageConversion_EncodeToPNG_m8D67A36A7D81F436CDA108CC5293E15A9CFD5617(L_11, /*hidden argument*/NULL);
		V_2 = L_12;
		// File.WriteAllBytes(Application.persistentDataPath + "/image" + num.ToString() + ".png", imbytes);
		String_t* L_13 = Application_get_persistentDataPath_m82E34156D8BD0A55CAC258CDFE8317FAD6945F5B(/*hidden argument*/NULL);
		String_t* L_14 = Int32_ToString_m1863896DE712BF97C031D55B12E1583F1982DC02((int32_t*)(&V_1), /*hidden argument*/NULL);
		String_t* L_15 = String_Concat_mDD2E38332DED3A8C088D38D78A0E0BEB5091DA64(L_13, _stringLiteralB8D68D9750E37D43B20BDCF6675828CDB80E7B77, L_14, _stringLiteral339F5868588CFC8FDA4A1EC94EAD8F737C5FCFC0, /*hidden argument*/NULL);
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_16 = V_2;
		File_WriteAllBytes_m07F13C1CA0BD0960392C78AB99E0F19564F9B594(L_15, L_16, /*hidden argument*/NULL);
		// Destroy(texture);
		Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * L_17 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_il2cpp_TypeInfo_var);
		Object_Destroy_m23B4562495BA35A74266D4372D45368F8C05109A(L_17, /*hidden argument*/NULL);
		// Debug.Log(Application.persistentDataPath + "/image" + num.ToString() + ".png");
		String_t* L_18 = Application_get_persistentDataPath_m82E34156D8BD0A55CAC258CDFE8317FAD6945F5B(/*hidden argument*/NULL);
		String_t* L_19 = Int32_ToString_m1863896DE712BF97C031D55B12E1583F1982DC02((int32_t*)(&V_1), /*hidden argument*/NULL);
		String_t* L_20 = String_Concat_mDD2E38332DED3A8C088D38D78A0E0BEB5091DA64(L_18, _stringLiteralB8D68D9750E37D43B20BDCF6675828CDB80E7B77, L_19, _stringLiteral339F5868588CFC8FDA4A1EC94EAD8F737C5FCFC0, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t7B5FCB117E2FD63B6838BC52821B252E2BFB61C4_il2cpp_TypeInfo_var);
		Debug_Log_m4B7C70BAFD477C6BDB59C88A0934F0B018D03708(L_20, /*hidden argument*/NULL);
		// Debug.Log("Sauvegarde effectu?e");
		Debug_Log_m4B7C70BAFD477C6BDB59C88A0934F0B018D03708(_stringLiteral1C4040CDF99C7F061E8046B8048A2E7E291BFB63, /*hidden argument*/NULL);
		// }
		goto IL_00b4;
	}

IL_00aa:
	{
		// Debug.Log("Sauvegarde NON effectu?e");
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t7B5FCB117E2FD63B6838BC52821B252E2BFB61C4_il2cpp_TypeInfo_var);
		Debug_Log_m4B7C70BAFD477C6BDB59C88A0934F0B018D03708(_stringLiteralC973956BBC57FD0B45330CCC233A90EB5DFDEA13, /*hidden argument*/NULL);
	}

IL_00b4:
	{
		// SceneManager.LoadScene("Menu");
		SceneManager_LoadScene_mFC850AC783E5EA05D6154976385DFECC251CDFB9(_stringLiteral57F5F5EFBC5990F5230AA95359042338B856707B, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void imageBank::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void imageBank__ctor_m385D1C985E5F8091FC8632A695615E916902848E (imageBank_t5EEBC6CE9144A46186F0114E6CE8B23ACDC4CCE3 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_mEAEC84B222C60319D593E456D769B3311DFCEF97(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void test::Start()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void test_Start_m199A97ADA195BDC507FFD0450ABB3E3ADF58073A (test_tBCA4AC3C1D45BA0DD176A21EC1F45D70CE6F1246 * __this, const RuntimeMethod* method)
{
	{
		// }
		return;
	}
}
// System.Void test::Update()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void test_Update_m8F447A6A478967D47638F11275101245FF2314DA (test_tBCA4AC3C1D45BA0DD176A21EC1F45D70CE6F1246 * __this, const RuntimeMethod* method)
{
	{
		// }
		return;
	}
}
// System.Void test::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void test__ctor_m150D737B2A1D3D58D1BD576A85C27FA05B275670 (test_tBCA4AC3C1D45BA0DD176A21EC1F45D70CE6F1246 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_mEAEC84B222C60319D593E456D769B3311DFCEF97(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void test_nativegallery::TryPickImage()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void test_nativegallery_TryPickImage_mD9DA2AAE5585AFE7E98CB7BF7498366BCE3DED39 (test_nativegallery_tAC993D2146BA62036BC7ADD8AFF36FC4CD783627 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (test_nativegallery_TryPickImage_mD9DA2AAE5585AFE7E98CB7BF7498366BCE3DED39_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// if (NativeGallery.IsMediaPickerBusy())
		IL2CPP_RUNTIME_CLASS_INIT(NativeGallery_tD21DE6A0C6A5FF1B267058E3B2C00F0007F4C84C_il2cpp_TypeInfo_var);
		bool L_0 = NativeGallery_IsMediaPickerBusy_m1AB7029F286FAB7CD4FCE7A4145DBEEB2E9946AB(/*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0008;
		}
	}
	{
		// return;
		return;
	}

IL_0008:
	{
		// PickImage(16384);
		test_nativegallery_PickImage_m5836EF2B5F137638F5B23C1186F18096E8A8AF7E(__this, ((int32_t)16384), /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void test_nativegallery::PickImage(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void test_nativegallery_PickImage_m5836EF2B5F137638F5B23C1186F18096E8A8AF7E (test_nativegallery_tAC993D2146BA62036BC7ADD8AFF36FC4CD783627 * __this, int32_t ___maxSize0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (test_nativegallery_PickImage_m5836EF2B5F137638F5B23C1186F18096E8A8AF7E_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		U3CU3Ec__DisplayClass2_0_t6812C288DD4EA8357CAFD0805FDB4C76A5DC6CB3 * L_0 = (U3CU3Ec__DisplayClass2_0_t6812C288DD4EA8357CAFD0805FDB4C76A5DC6CB3 *)il2cpp_codegen_object_new(U3CU3Ec__DisplayClass2_0_t6812C288DD4EA8357CAFD0805FDB4C76A5DC6CB3_il2cpp_TypeInfo_var);
		U3CU3Ec__DisplayClass2_0__ctor_mF95464CCBB9EDFA86D0785A8482181854DEFF131(L_0, /*hidden argument*/NULL);
		U3CU3Ec__DisplayClass2_0_t6812C288DD4EA8357CAFD0805FDB4C76A5DC6CB3 * L_1 = L_0;
		int32_t L_2 = ___maxSize0;
		NullCheck(L_1);
		L_1->set_maxSize_1(L_2);
		U3CU3Ec__DisplayClass2_0_t6812C288DD4EA8357CAFD0805FDB4C76A5DC6CB3 * L_3 = L_1;
		NullCheck(L_3);
		L_3->set_U3CU3E4__this_2(__this);
		// Texture2D texture = new Texture2D(2, 2);
		U3CU3Ec__DisplayClass2_0_t6812C288DD4EA8357CAFD0805FDB4C76A5DC6CB3 * L_4 = L_3;
		Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * L_5 = (Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C *)il2cpp_codegen_object_new(Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C_il2cpp_TypeInfo_var);
		Texture2D__ctor_m0C86A87871AA8075791EF98499D34DA95ACB0E35(L_5, 2, 2, /*hidden argument*/NULL);
		NullCheck(L_4);
		L_4->set_texture_0(L_5);
		// NativeGallery.Permission permission = NativeGallery.GetImageFromGallery((path) =>
		// {
		//     Debug.Log("Image path: " + path);
		//     if (path != null)
		//     {
		//         // Create Texture from selected image
		//          texture = NativeGallery.LoadImageAtPath(path, maxSize, false);
		//         if (texture == null)
		//         {
		//             Debug.Log("Couldn't load texture from " + path);
		//             return;
		//         }
		// 
		// 
		// 
		// 
		//         // Assign texture to a temporary quad and destroy it after 5 seconds
		// 
		//         quad.transform.position = Camera.main.transform.position + Camera.main.transform.forward * 2.5f;
		//         quad.transform.forward = Camera.main.transform.forward;
		//         quad.transform.localScale = new Vector3(5f, 5f * (texture.height / (float)texture.width), 1f);
		// 
		//         Material material = quad.GetComponent<Renderer>().material;
		//         if (!material.shader.isSupported) // happens when Standard shader is not included in the build
		//             material.shader = Shader.Find("Legacy Shaders/Diffuse");
		// 
		//         material.mainTexture = texture;
		// 
		// 
		// 
		// 
		// 
		//     }
		// }, "Select a PNG image", "image/png");
		MediaPickCallback_t2F83A1D5E2DFEE08EECAE29F980B5BEDD1EEE77D * L_6 = (MediaPickCallback_t2F83A1D5E2DFEE08EECAE29F980B5BEDD1EEE77D *)il2cpp_codegen_object_new(MediaPickCallback_t2F83A1D5E2DFEE08EECAE29F980B5BEDD1EEE77D_il2cpp_TypeInfo_var);
		MediaPickCallback__ctor_m82EF4DC0AEBE82B93D0132A4205B6A28DD958979(L_6, L_4, (intptr_t)((intptr_t)U3CU3Ec__DisplayClass2_0_U3CPickImageU3Eb__0_m9A5755815823DE9D168D4EB0EA78CE86C35B1ED3_RuntimeMethod_var), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(NativeGallery_tD21DE6A0C6A5FF1B267058E3B2C00F0007F4C84C_il2cpp_TypeInfo_var);
		int32_t L_7 = NativeGallery_GetImageFromGallery_mD6D6BF52470E918BF06AB31BAB5EB0DD320DA7C8(L_6, _stringLiteral9036D06C54315FC50FA8E31B45FAFA8B898DA2F3, _stringLiteral34D71611DCE9C30419B3C3EA42DC9426E931CDDE, /*hidden argument*/NULL);
		V_0 = L_7;
		// Debug.Log("Permission result: " + permission);
		int32_t L_8 = V_0;
		int32_t L_9 = L_8;
		RuntimeObject * L_10 = Box(Permission_t578711508834B1BD13C99D50DAD7CD563AAADEC5_il2cpp_TypeInfo_var, &L_9);
		String_t* L_11 = String_Concat_mBB19C73816BDD1C3519F248E1ADC8E11A6FDB495(_stringLiteral760D542CE497A54CB36ACFB2A671DD229132CF03, L_10, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t7B5FCB117E2FD63B6838BC52821B252E2BFB61C4_il2cpp_TypeInfo_var);
		Debug_Log_m4B7C70BAFD477C6BDB59C88A0934F0B018D03708(L_11, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void test_nativegallery::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void test_nativegallery__ctor_mAE0FE4727CE600AC1F35A864605E36BA18EFE427 (test_nativegallery_tAC993D2146BA62036BC7ADD8AFF36FC4CD783627 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_mEAEC84B222C60319D593E456D769B3311DFCEF97(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void test_nativegallery_<>c__DisplayClass2_0::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__DisplayClass2_0__ctor_mF95464CCBB9EDFA86D0785A8482181854DEFF131 (U3CU3Ec__DisplayClass2_0_t6812C288DD4EA8357CAFD0805FDB4C76A5DC6CB3 * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m925ECA5E85CA100E3FB86A4F9E15C120E9A184C0(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void test_nativegallery_<>c__DisplayClass2_0::<PickImage>b__0(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__DisplayClass2_0_U3CPickImageU3Eb__0_m9A5755815823DE9D168D4EB0EA78CE86C35B1ED3 (U3CU3Ec__DisplayClass2_0_t6812C288DD4EA8357CAFD0805FDB4C76A5DC6CB3 * __this, String_t* ___path0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CU3Ec__DisplayClass2_0_U3CPickImageU3Eb__0_m9A5755815823DE9D168D4EB0EA78CE86C35B1ED3_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * V_0 = NULL;
	{
		// Debug.Log("Image path: " + path);
		String_t* L_0 = ___path0;
		String_t* L_1 = String_Concat_mB78D0094592718DA6D5DB6C712A9C225631666BE(_stringLiteral6FB6DCA24A95A61457676DB99D1A9C5D11E0B5B8, L_0, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t7B5FCB117E2FD63B6838BC52821B252E2BFB61C4_il2cpp_TypeInfo_var);
		Debug_Log_m4B7C70BAFD477C6BDB59C88A0934F0B018D03708(L_1, /*hidden argument*/NULL);
		// if (path != null)
		String_t* L_2 = ___path0;
		if (!L_2)
		{
			goto IL_0132;
		}
	}
	{
		// texture = NativeGallery.LoadImageAtPath(path, maxSize, false);
		String_t* L_3 = ___path0;
		int32_t L_4 = __this->get_maxSize_1();
		IL2CPP_RUNTIME_CLASS_INIT(NativeGallery_tD21DE6A0C6A5FF1B267058E3B2C00F0007F4C84C_il2cpp_TypeInfo_var);
		Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * L_5 = NativeGallery_LoadImageAtPath_m7E2D04E8514667DB5A7BBC450D46FDC971F2F0C7(L_3, L_4, (bool)0, (bool)1, (bool)0, /*hidden argument*/NULL);
		__this->set_texture_0(L_5);
		// if (texture == null)
		Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * L_6 = __this->get_texture_0();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_il2cpp_TypeInfo_var);
		bool L_7 = Object_op_Equality_mBC2401774F3BE33E8CF6F0A8148E66C95D6CFF1C(L_6, (Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 *)NULL, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_004a;
		}
	}
	{
		// Debug.Log("Couldn't load texture from " + path);
		String_t* L_8 = ___path0;
		String_t* L_9 = String_Concat_mB78D0094592718DA6D5DB6C712A9C225631666BE(_stringLiteral424F5635CBDACD59C5F967E319D15FC0CFCB0637, L_8, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t7B5FCB117E2FD63B6838BC52821B252E2BFB61C4_il2cpp_TypeInfo_var);
		Debug_Log_m4B7C70BAFD477C6BDB59C88A0934F0B018D03708(L_9, /*hidden argument*/NULL);
		// return;
		return;
	}

IL_004a:
	{
		// quad.transform.position = Camera.main.transform.position + Camera.main.transform.forward * 2.5f;
		test_nativegallery_tAC993D2146BA62036BC7ADD8AFF36FC4CD783627 * L_10 = __this->get_U3CU3E4__this_2();
		NullCheck(L_10);
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_11 = L_10->get_quad_4();
		NullCheck(L_11);
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_12 = GameObject_get_transform_mA5C38857137F137CB96C69FAA624199EB1C2FB2C(L_11, /*hidden argument*/NULL);
		Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * L_13 = Camera_get_main_m9256A9F84F92D7ED73F3E6C4E2694030AD8B61FA(/*hidden argument*/NULL);
		NullCheck(L_13);
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_14 = Component_get_transform_m00F05BD782F920C301A7EBA480F3B7A904C07EC9(L_13, /*hidden argument*/NULL);
		NullCheck(L_14);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_15 = Transform_get_position_mF54C3A064F7C8E24F1C56EE128728B2E4485E294(L_14, /*hidden argument*/NULL);
		Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * L_16 = Camera_get_main_m9256A9F84F92D7ED73F3E6C4E2694030AD8B61FA(/*hidden argument*/NULL);
		NullCheck(L_16);
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_17 = Component_get_transform_m00F05BD782F920C301A7EBA480F3B7A904C07EC9(L_16, /*hidden argument*/NULL);
		NullCheck(L_17);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_18 = Transform_get_forward_m0BE1E88B86049ADA39391C3ACED2314A624BC67F(L_17, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_il2cpp_TypeInfo_var);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_19 = Vector3_op_Multiply_m1C5F07723615156ACF035D88A1280A9E8F35A04E(L_18, (2.5f), /*hidden argument*/NULL);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_20 = Vector3_op_Addition_m929F9C17E5D11B94D50B4AFF1D730B70CB59B50E(L_15, L_19, /*hidden argument*/NULL);
		NullCheck(L_12);
		Transform_set_position_mDA89E4893F14ECA5CBEEE7FB80A5BF7C1B8EA6DC(L_12, L_20, /*hidden argument*/NULL);
		// quad.transform.forward = Camera.main.transform.forward;
		test_nativegallery_tAC993D2146BA62036BC7ADD8AFF36FC4CD783627 * L_21 = __this->get_U3CU3E4__this_2();
		NullCheck(L_21);
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_22 = L_21->get_quad_4();
		NullCheck(L_22);
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_23 = GameObject_get_transform_mA5C38857137F137CB96C69FAA624199EB1C2FB2C(L_22, /*hidden argument*/NULL);
		Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * L_24 = Camera_get_main_m9256A9F84F92D7ED73F3E6C4E2694030AD8B61FA(/*hidden argument*/NULL);
		NullCheck(L_24);
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_25 = Component_get_transform_m00F05BD782F920C301A7EBA480F3B7A904C07EC9(L_24, /*hidden argument*/NULL);
		NullCheck(L_25);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_26 = Transform_get_forward_m0BE1E88B86049ADA39391C3ACED2314A624BC67F(L_25, /*hidden argument*/NULL);
		NullCheck(L_23);
		Transform_set_forward_m02858E8B3313B27174B19E9113F24EF25FBCEC7F(L_23, L_26, /*hidden argument*/NULL);
		// quad.transform.localScale = new Vector3(5f, 5f * (texture.height / (float)texture.width), 1f);
		test_nativegallery_tAC993D2146BA62036BC7ADD8AFF36FC4CD783627 * L_27 = __this->get_U3CU3E4__this_2();
		NullCheck(L_27);
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_28 = L_27->get_quad_4();
		NullCheck(L_28);
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_29 = GameObject_get_transform_mA5C38857137F137CB96C69FAA624199EB1C2FB2C(L_28, /*hidden argument*/NULL);
		Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * L_30 = __this->get_texture_0();
		NullCheck(L_30);
		int32_t L_31 = VirtFuncInvoker0< int32_t >::Invoke(6 /* System.Int32 UnityEngine.Texture::get_height() */, L_30);
		Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * L_32 = __this->get_texture_0();
		NullCheck(L_32);
		int32_t L_33 = VirtFuncInvoker0< int32_t >::Invoke(4 /* System.Int32 UnityEngine.Texture::get_width() */, L_32);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_34;
		memset((&L_34), 0, sizeof(L_34));
		Vector3__ctor_m08F61F548AA5836D8789843ACB4A81E4963D2EE1((&L_34), (5.0f), ((float)il2cpp_codegen_multiply((float)(5.0f), (float)((float)((float)(((float)((float)L_31)))/(float)(((float)((float)L_33))))))), (1.0f), /*hidden argument*/NULL);
		NullCheck(L_29);
		Transform_set_localScale_m7ED1A6E5A87CD1D483515B99D6D3121FB92B0556(L_29, L_34, /*hidden argument*/NULL);
		// Material material = quad.GetComponent<Renderer>().material;
		test_nativegallery_tAC993D2146BA62036BC7ADD8AFF36FC4CD783627 * L_35 = __this->get_U3CU3E4__this_2();
		NullCheck(L_35);
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_36 = L_35->get_quad_4();
		NullCheck(L_36);
		Renderer_t0556D67DD582620D1F495627EDE30D03284151F4 * L_37 = GameObject_GetComponent_TisRenderer_t0556D67DD582620D1F495627EDE30D03284151F4_mD65E2552CCFED4D0EC506EE90DE51215D90AEF85(L_36, /*hidden argument*/GameObject_GetComponent_TisRenderer_t0556D67DD582620D1F495627EDE30D03284151F4_mD65E2552CCFED4D0EC506EE90DE51215D90AEF85_RuntimeMethod_var);
		NullCheck(L_37);
		Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * L_38 = Renderer_get_material_m4434513446B652652CE9FD766B0E3D1D34C4A617(L_37, /*hidden argument*/NULL);
		V_0 = L_38;
		// if (!material.shader.isSupported) // happens when Standard shader is not included in the build
		Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * L_39 = V_0;
		NullCheck(L_39);
		Shader_tE2731FF351B74AB4186897484FB01E000C1160CA * L_40 = Material_get_shader_m9CEDCA4D97D42588C6B827400E364E4A8EC55FF0(L_39, /*hidden argument*/NULL);
		NullCheck(L_40);
		bool L_41 = Shader_get_isSupported_m3660681289CDFE742D399C3030A6CF5C4D8B030D(L_40, /*hidden argument*/NULL);
		if (L_41)
		{
			goto IL_0126;
		}
	}
	{
		// material.shader = Shader.Find("Legacy Shaders/Diffuse");
		Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * L_42 = V_0;
		Shader_tE2731FF351B74AB4186897484FB01E000C1160CA * L_43 = Shader_Find_m755654AA68D1C663A3E20A10E00CDC10F96C962B(_stringLiteral53DE552696C19F8EF3C3CB30E2C3F162ED1483CB, /*hidden argument*/NULL);
		NullCheck(L_42);
		Material_set_shader_m689F997F888E3C2A0FF9E6F399AA5D8204B454B1(L_42, L_43, /*hidden argument*/NULL);
	}

IL_0126:
	{
		// material.mainTexture = texture;
		Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * L_44 = V_0;
		Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * L_45 = __this->get_texture_0();
		NullCheck(L_44);
		Material_set_mainTexture_m0742CFF768E9701618DA07C71F009239AB31EB41(L_44, L_45, /*hidden argument*/NULL);
	}

IL_0132:
	{
		// }, "Select a PNG image", "image/png");
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR void PointerEventArgs_set_Pointers_m80547B23A481F55D60DEE830E6340F85555B550A_inline (PointerEventArgs_t60C9291910F961213770A1302F3CE7CC5A580106 * __this, RuntimeObject* ___value0, const RuntimeMethod* method)
{
	{
		// public IList<Pointer> Pointers { get; private set; }
		RuntimeObject* L_0 = ___value0;
		__this->set_U3CPointersU3Ek__BackingField_1(L_0);
		return;
	}
}
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR void FakePointer_set_Position_mCA620D1E36778D9DD9DB87D24F6899D5EFEBBF34_inline (FakePointer_t8C1AEEBB63CF9BCC8170772F4E4601CD4298BC47 * __this, Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___value0, const RuntimeMethod* method)
{
	{
		// public Vector2 Position { get; set; }
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_0 = ___value0;
		__this->set_U3CPositionU3Ek__BackingField_3(L_0);
		return;
	}
}
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR void FakePointer_set_Id_mC0D41C4688A1B33B826E36B909C6A0E3983EFEB5_inline (FakePointer_t8C1AEEBB63CF9BCC8170772F4E4601CD4298BC47 * __this, int32_t ___value0, const RuntimeMethod* method)
{
	{
		// public int Id { get; private set; }
		int32_t L_0 = ___value0;
		__this->set_U3CIdU3Ek__BackingField_0(L_0);
		return;
	}
}
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR void FakePointer_set_Type_m79DF4E0AF1EEF7C1BD223A21042D27D9E12327EC_inline (FakePointer_t8C1AEEBB63CF9BCC8170772F4E4601CD4298BC47 * __this, int32_t ___value0, const RuntimeMethod* method)
{
	{
		// public Pointer.PointerType Type { get; private set; }
		int32_t L_0 = ___value0;
		__this->set_U3CTypeU3Ek__BackingField_1(L_0);
		return;
	}
}
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR void FakePointer_set_Flags_m64E3B23758D21054F7CB36BB6033AA7088169EB8_inline (FakePointer_t8C1AEEBB63CF9BCC8170772F4E4601CD4298BC47 * __this, uint32_t ___value0, const RuntimeMethod* method)
{
	{
		// public uint Flags { get; private set; }
		uint32_t L_0 = ___value0;
		__this->set_U3CFlagsU3Ek__BackingField_4(L_0);
		return;
	}
}
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR void Pointer_set_Type_mC0D513634112590ABB8EB877E8BA70416BDF2625_inline (Pointer_tBF6873B314D4CC8534B6CD8A8BA54B92D8CFDE98 * __this, int32_t ___value0, const RuntimeMethod* method)
{
	{
		// public PointerType Type { get; protected set; }
		int32_t L_0 = ___value0;
		__this->set_U3CTypeU3Ek__BackingField_5(L_0);
		return;
	}
}
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  MousePointer_get_ScrollDelta_mC8F97C22EFBD42A56861A7B1EF1907185F0746A3_inline (MousePointer_tEFB01FDD2340977461DEB8153349F62DFDE0E100 * __this, const RuntimeMethod* method)
{
	{
		// public Vector2 ScrollDelta { get; set; }
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_0 = __this->get_U3CScrollDeltaU3Ek__BackingField_18();
		return L_0;
	}
}
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR void MousePointer_set_ScrollDelta_m352D328338CF5FF4420BEA90891189D57C517C1F_inline (MousePointer_tEFB01FDD2340977461DEB8153349F62DFDE0E100 * __this, Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___value0, const RuntimeMethod* method)
{
	{
		// public Vector2 ScrollDelta { get; set; }
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_0 = ___value0;
		__this->set_U3CScrollDeltaU3Ek__BackingField_18(L_0);
		return;
	}
}
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR int32_t ObjectPointer_get_ObjectId_m19757D9C574D591D4DD3043EEFBDDCE1064B2581_inline (ObjectPointer_tD190FCA2463DDBBD72FBA988696DFC51EFF46C36 * __this, const RuntimeMethod* method)
{
	{
		// public int ObjectId { get; internal set; }
		int32_t L_0 = __this->get_U3CObjectIdU3Ek__BackingField_22();
		return L_0;
	}
}
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR void ObjectPointer_set_ObjectId_m4951549B2096276F8533B2D025F744CE8E5D4FDD_inline (ObjectPointer_tD190FCA2463DDBBD72FBA988696DFC51EFF46C36 * __this, int32_t ___value0, const RuntimeMethod* method)
{
	{
		// public int ObjectId { get; internal set; }
		int32_t L_0 = ___value0;
		__this->set_U3CObjectIdU3Ek__BackingField_22(L_0);
		return;
	}
}
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR float ObjectPointer_get_Width_m92F855D6202D7ECC31204F7BFB1924B26DA5F007_inline (ObjectPointer_tD190FCA2463DDBBD72FBA988696DFC51EFF46C36 * __this, const RuntimeMethod* method)
{
	{
		// public float Width { get; internal set; }
		float L_0 = __this->get_U3CWidthU3Ek__BackingField_23();
		return L_0;
	}
}
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR void ObjectPointer_set_Width_mFCDC006E0026F249E35C88362BD748EE5A14AFD7_inline (ObjectPointer_tD190FCA2463DDBBD72FBA988696DFC51EFF46C36 * __this, float ___value0, const RuntimeMethod* method)
{
	{
		// public float Width { get; internal set; }
		float L_0 = ___value0;
		__this->set_U3CWidthU3Ek__BackingField_23(L_0);
		return;
	}
}
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR float ObjectPointer_get_Height_m30D7BD97E5CD11924D9111F3011E0E453CAA6DA0_inline (ObjectPointer_tD190FCA2463DDBBD72FBA988696DFC51EFF46C36 * __this, const RuntimeMethod* method)
{
	{
		// public float Height { get; internal set; }
		float L_0 = __this->get_U3CHeightU3Ek__BackingField_24();
		return L_0;
	}
}
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR void ObjectPointer_set_Height_m793FAF0083E3C0DC47E6825EE6ECCA7F342CE66A_inline (ObjectPointer_tD190FCA2463DDBBD72FBA988696DFC51EFF46C36 * __this, float ___value0, const RuntimeMethod* method)
{
	{
		// public float Height { get; internal set; }
		float L_0 = ___value0;
		__this->set_U3CHeightU3Ek__BackingField_24(L_0);
		return;
	}
}
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR float ObjectPointer_get_Angle_mF6824CF5CA2316D34523B893EE50BEF01DF9D1E9_inline (ObjectPointer_tD190FCA2463DDBBD72FBA988696DFC51EFF46C36 * __this, const RuntimeMethod* method)
{
	{
		// public float Angle { get; internal set; }
		float L_0 = __this->get_U3CAngleU3Ek__BackingField_25();
		return L_0;
	}
}
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR void ObjectPointer_set_Angle_m3710F36254EBE84ACBC00123055FFB1CE8D8D108_inline (ObjectPointer_tD190FCA2463DDBBD72FBA988696DFC51EFF46C36 * __this, float ___value0, const RuntimeMethod* method)
{
	{
		// public float Angle { get; internal set; }
		float L_0 = ___value0;
		__this->set_U3CAngleU3Ek__BackingField_25(L_0);
		return;
	}
}
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR void PenPointer_set_Rotation_m74FF90FEA33FF1621F9792CBDDE3A31569246E08_inline (PenPointer_tAF65E3C674E3160B823CFBB7D82046A3FFD60340 * __this, float ___value0, const RuntimeMethod* method)
{
	{
		// public float Rotation { get; set; }
		float L_0 = ___value0;
		__this->set_U3CRotationU3Ek__BackingField_20(L_0);
		return;
	}
}
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR void PenPointer_set_Pressure_m33A397E626F343E987248BEC63FAB5F18621BD61_inline (PenPointer_tAF65E3C674E3160B823CFBB7D82046A3FFD60340 * __this, float ___value0, const RuntimeMethod* method)
{
	{
		// public float Pressure { get; set; }
		float L_0 = ___value0;
		__this->set_U3CPressureU3Ek__BackingField_21(L_0);
		return;
	}
}
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR TouchLayer_t484C3424D422E6073B3FE4DD21EFE05686F5AC54 * HitData_get_Layer_m80AD3383FA69E902C6C6E7B29769771F24EAEA51_inline (HitData_t3022C0DF180FDE3CF86F713B78A2056D02E78980 * __this, const RuntimeMethod* method)
{
	{
		// get { return layer; }
		TouchLayer_t484C3424D422E6073B3FE4DD21EFE05686F5AC54 * L_0 = __this->get_layer_3();
		return L_0;
	}
}
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR int32_t Pointer_get_Type_m8733FBC6C16FE2C6FBA301C50E8E26266720B9EB_inline (Pointer_tBF6873B314D4CC8534B6CD8A8BA54B92D8CFDE98 * __this, const RuntimeMethod* method)
{
	{
		// public PointerType Type { get; protected set; }
		int32_t L_0 = __this->get_U3CTypeU3Ek__BackingField_5();
		return L_0;
	}
}
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR uint32_t Pointer_get_Flags_m941FB1843C3FD979278B353B040264E56F68A883_inline (Pointer_tBF6873B314D4CC8534B6CD8A8BA54B92D8CFDE98 * __this, const RuntimeMethod* method)
{
	{
		// public uint Flags { get; set; }
		uint32_t L_0 = __this->get_U3CFlagsU3Ek__BackingField_9();
		return L_0;
	}
}
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR void Pointer_set_Flags_mF6A08BE8419E9E7DFD4CD15AF3A4E811734E9293_inline (Pointer_tBF6873B314D4CC8534B6CD8A8BA54B92D8CFDE98 * __this, uint32_t ___value0, const RuntimeMethod* method)
{
	{
		// public uint Flags { get; set; }
		uint32_t L_0 = ___value0;
		__this->set_U3CFlagsU3Ek__BackingField_9(L_0);
		return;
	}
}
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR int32_t Pointer_get_Buttons_mDD144F267D37B07E930E44E9B7D9357246D5839A_inline (Pointer_tBF6873B314D4CC8534B6CD8A8BA54B92D8CFDE98 * __this, const RuntimeMethod* method)
{
	{
		// public PointerButtonState Buttons { get; set; }
		int32_t L_0 = __this->get_U3CButtonsU3Ek__BackingField_6();
		return L_0;
	}
}
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR void Pointer_set_Buttons_mAE901843226D346730CAD2CAD512793E4E4BEAC8_inline (Pointer_tBF6873B314D4CC8534B6CD8A8BA54B92D8CFDE98 * __this, int32_t ___value0, const RuntimeMethod* method)
{
	{
		// public PointerButtonState Buttons { get; set; }
		int32_t L_0 = ___value0;
		__this->set_U3CButtonsU3Ek__BackingField_6(L_0);
		return;
	}
}
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  Pointer_get_PreviousPosition_m18CBA71CDAE159A709A4F88D96F40B81B333118E_inline (Pointer_tBF6873B314D4CC8534B6CD8A8BA54B92D8CFDE98 * __this, const RuntimeMethod* method)
{
	{
		// public Vector2 PreviousPosition { get; private set; }
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_0 = __this->get_U3CPreviousPositionU3Ek__BackingField_8();
		return L_0;
	}
}
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR void Pointer_set_PreviousPosition_m9825F629338D0DF6160DCB6DAADF3488860FA703_inline (Pointer_tBF6873B314D4CC8534B6CD8A8BA54B92D8CFDE98 * __this, Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___value0, const RuntimeMethod* method)
{
	{
		// public Vector2 PreviousPosition { get; private set; }
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_0 = ___value0;
		__this->set_U3CPreviousPositionU3Ek__BackingField_8(L_0);
		return;
	}
}
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR int32_t Pointer_get_Id_mFF112114C234DC146CA93A7719C3F316E8DACCAD_inline (Pointer_tBF6873B314D4CC8534B6CD8A8BA54B92D8CFDE98 * __this, const RuntimeMethod* method)
{
	{
		// public int Id { get; private set; }
		int32_t L_0 = __this->get_U3CIdU3Ek__BackingField_4();
		return L_0;
	}
}
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  Pointer_get_Position_mCE0B33AF8B7D86A8BA6824E7817A3DFCB29BE4BC_inline (Pointer_tBF6873B314D4CC8534B6CD8A8BA54B92D8CFDE98 * __this, const RuntimeMethod* method)
{
	{
		// get { return position; }
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_0 = __this->get_position_13();
		return L_0;
	}
}
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR void Pointer_set_InputSource_m646284A86B72DC008C111B607C12E38F4A4EA385_inline (Pointer_tBF6873B314D4CC8534B6CD8A8BA54B92D8CFDE98 * __this, RuntimeObject* ___value0, const RuntimeMethod* method)
{
	{
		// public IInputSource InputSource { get; private set; }
		RuntimeObject* L_0 = ___value0;
		__this->set_U3CInputSourceU3Ek__BackingField_7(L_0);
		return;
	}
}
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR void Pointer_set_Id_mEB0B14E2905D84B287EABA142EEAFF7737D63886_inline (Pointer_tBF6873B314D4CC8534B6CD8A8BA54B92D8CFDE98 * __this, int32_t ___value0, const RuntimeMethod* method)
{
	{
		// public int Id { get; private set; }
		int32_t L_0 = ___value0;
		__this->set_U3CIdU3Ek__BackingField_4(L_0);
		return;
	}
}
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR void TouchPointer_set_Rotation_m87442B9A19EB126F7E42949A8E0FB1FC2CB88FF9_inline (TouchPointer_t35FC788E12AC007D8F90D6D28131910B61316942 * __this, float ___value0, const RuntimeMethod* method)
{
	{
		// public float Rotation { get; set; }
		float L_0 = ___value0;
		__this->set_U3CRotationU3Ek__BackingField_20(L_0);
		return;
	}
}
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR void TouchPointer_set_Pressure_m9DAD082334050E94FCD81D8887D56CAA8D548BD0_inline (TouchPointer_t35FC788E12AC007D8F90D6D28131910B61316942 * __this, float ___value0, const RuntimeMethod* method)
{
	{
		// public float Pressure { get; set; }
		float L_0 = ___value0;
		__this->set_U3CPressureU3Ek__BackingField_21(L_0);
		return;
	}
}
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR bool TouchManager_get_ShouldCreateCameraLayer_m9555EEF395EA3D416B86CD071695F48749CEA641_inline (TouchManager_tE3A4C5ABD05B3B6C984CDBB1FA80479E1ACBE92B * __this, const RuntimeMethod* method)
{
	{
		// get { return shouldCreateCameraLayer; }
		bool L_0 = __this->get_shouldCreateCameraLayer_19();
		return L_0;
	}
}
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR bool TouchManager_get_ShouldCreateStandardInput_mE33919A0FE9C7D46BA910362D807179381DFAAE1_inline (TouchManager_tE3A4C5ABD05B3B6C984CDBB1FA80479E1ACBE92B * __this, const RuntimeMethod* method)
{
	{
		// get { return shouldCreateStandardInput; }
		bool L_0 = __this->get_shouldCreateStandardInput_20();
		return L_0;
	}
}
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR int32_t TouchManager_get_SendMessageEvents_m807C4A591F1C10FB759D7470C7D1F54484C478E0_inline (TouchManager_tE3A4C5ABD05B3B6C984CDBB1FA80479E1ACBE92B * __this, const RuntimeMethod* method)
{
	{
		// get { return sendMessageEvents; }
		int32_t L_0 = __this->get_sendMessageEvents_22();
		return L_0;
	}
}
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR RuntimeObject* PointerEventArgs_get_Pointers_m16B91F1D5BE47012CF6C4BC0BC3F53691525FC41_inline (PointerEventArgs_t60C9291910F961213770A1302F3CE7CC5A580106 * __this, const RuntimeMethod* method)
{
	{
		// public IList<Pointer> Pointers { get; private set; }
		RuntimeObject* L_0 = __this->get_U3CPointersU3Ek__BackingField_1();
		return L_0;
	}
}
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR HitData_t3022C0DF180FDE3CF86F713B78A2056D02E78980  Pointer_GetPressData_m806849D3319D7AF0EB6469AC978F593B1AD6A3B4_inline (Pointer_tBF6873B314D4CC8534B6CD8A8BA54B92D8CFDE98 * __this, const RuntimeMethod* method)
{
	{
		// return pressData;
		HitData_t3022C0DF180FDE3CF86F713B78A2056D02E78980  L_0 = __this->get_pressData_15();
		return L_0;
	}
}
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * HitData_get_Target_mD8CF226FD6A5401997C2F6F554C2BA26155F7A05_inline (HitData_t3022C0DF180FDE3CF86F713B78A2056D02E78980 * __this, const RuntimeMethod* method)
{
	{
		// get { return target; }
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_0 = __this->get_target_1();
		return L_0;
	}
}
IL2CPP_EXTERN_C inline IL2CPP_METHOD_ATTR RuntimeObject * List_1_get_Item_mFDB8AD680C600072736579BBF5F38F7416396588_gshared_inline (List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * __this, int32_t ___index0, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___index0;
		int32_t L_1 = (int32_t)__this->get__size_2();
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_000e;
		}
	}
	{
		ThrowHelper_ThrowArgumentOutOfRangeException_mBA2AF20A35144E0C43CD721A22EAC9FCA15D6550(/*hidden argument*/NULL);
	}

IL_000e:
	{
		ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* L_2 = (ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A*)__this->get__items_1();
		int32_t L_3 = ___index0;
		RuntimeObject * L_4 = IL2CPP_ARRAY_UNSAFE_LOAD((ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A*)L_2, (int32_t)L_3);
		return L_4;
	}
}
IL2CPP_EXTERN_C inline IL2CPP_METHOD_ATTR int32_t List_1_get_Count_m507C9149FF7F83AAC72C29091E745D557DA47D22_gshared_inline (List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * __this, const RuntimeMethod* method)
{
	{
		int32_t L_0 = (int32_t)__this->get__size_2();
		return L_0;
	}
}

﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif



#include "codegen/il2cpp-codegen-metadata.h"





IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END




// 0x00000001 System.Void BD_UI::GoMenu()
extern void BD_UI_GoMenu_mBEF5A8424B3AB7B28932326F4E35ECFF359427D2 ();
// 0x00000002 System.Void BD_UI::.ctor()
extern void BD_UI__ctor_m2BE5A2FA5C2667494B901327CB429B8A25B89589 ();
// 0x00000003 System.Void DestroyQuad::Start()
extern void DestroyQuad_Start_m0589642E465B3F5E7F9C5352E6ED0A2985D1B505 ();
// 0x00000004 System.Void DestroyQuad::.ctor()
extern void DestroyQuad__ctor_mEC0FA2DE0994881DF2FDA6BF68C76B016F0A5ED9 ();
// 0x00000005 System.Void DontDestroy::Start()
extern void DontDestroy_Start_m28B8114DACB37F25CC4D1D43CBD5DF1729E8F140 ();
// 0x00000006 System.Void DontDestroy::.ctor()
extern void DontDestroy__ctor_m086F2780B84C7DF0A1ECF34920B93A7799C13719 ();
// 0x00000007 System.Void GoNewBD::LoadSceneNewBD()
extern void GoNewBD_LoadSceneNewBD_mE20EF90678D2E3A1FA3DBD12042C37AC65721317 ();
// 0x00000008 System.Void GoNewBD::.ctor()
extern void GoNewBD__ctor_mCBC14A7D5D64DC8316A83EEBC3EE8D00DED4B0B2 ();
// 0x00000009 System.Void LoadBD::Start()
extern void LoadBD_Start_m1F586ACB9C9DEACF197254338A96BDCE84AB9F4A ();
// 0x0000000A System.Void LoadBD::.ctor()
extern void LoadBD__ctor_m536E1797CCB2C0F3E63B1276288355259C76B089 ();
// 0x0000000B System.Void LoadImgNum::Start()
extern void LoadImgNum_Start_m72C28B461B2F04D97A5FDA5368139D966A5C516A ();
// 0x0000000C System.Void LoadImgNum::.ctor()
extern void LoadImgNum__ctor_m503DAFB21604EF8828D34CD8A91E9DDF80FD6387 ();
// 0x0000000D System.Void MainMenu::Start()
extern void MainMenu_Start_m0E4D98D402AC049948D1CA7E386641DCF5652821 ();
// 0x0000000E System.Void MainMenu::.ctor()
extern void MainMenu__ctor_mF17B753D99BD98B88E949A5B9CA53892E19A6CD5 ();
// 0x0000000F System.Void NumDuQuad::OnDestroy()
extern void NumDuQuad_OnDestroy_mB7C1E04BFD761FC38F99199CAC67E8E5FA9711EE ();
// 0x00000010 System.Void NumDuQuad::.ctor()
extern void NumDuQuad__ctor_m8D2C782C0A5A865946654C06C1B33388022025D1 ();
// 0x00000011 System.Void NumImage::LoadImgNum()
extern void NumImage_LoadImgNum_mA1AEAC2EE3F2339CE37C7898C7B20EA5C5823CFD ();
// 0x00000012 System.Void NumImage::LoadBDScene()
extern void NumImage_LoadBDScene_m9B7A34E3A1971A8E3C7BFCAF023B098D64A27316 ();
// 0x00000013 System.Void NumImage::.ctor()
extern void NumImage__ctor_m304FD3286218BE2B7EEF3ED98E27F4B86B1BDC6D ();
// 0x00000014 System.Void test::Start()
extern void test_Start_m199A97ADA195BDC507FFD0450ABB3E3ADF58073A ();
// 0x00000015 System.Void test::Update()
extern void test_Update_m8F447A6A478967D47638F11275101245FF2314DA ();
// 0x00000016 System.Void test::.ctor()
extern void test__ctor_m150D737B2A1D3D58D1BD576A85C27FA05B275670 ();
// 0x00000017 System.Void ExamplesList::Start()
extern void ExamplesList_Start_mCC53E9F4158B463A7057D5EA20E3B45233E806E2 ();
// 0x00000018 System.Void ExamplesList::ShowHide()
extern void ExamplesList_ShowHide_m7B4C8DD1B532DF0211D2831428733B82431D9110 ();
// 0x00000019 System.Void ExamplesList::.ctor()
extern void ExamplesList__ctor_m582B2C57F0BB2DB961F8E084A02D62A0DD94168C ();
// 0x0000001A System.Void Highlight::OnEnable()
extern void Highlight_OnEnable_mCE146025FC5903AF0792098A90FEF8D0E816124D ();
// 0x0000001B System.Void Highlight::OnDisable()
extern void Highlight_OnDisable_mEF152AE18B878639C4A499B31783C7EEDAD69799 ();
// 0x0000001C System.Void Highlight::overHandler(System.Object,System.EventArgs)
extern void Highlight_overHandler_m7BE07AEC1D848E075622C48DDB1E1597E8495242 ();
// 0x0000001D System.Void Highlight::outHandler(System.Object,System.EventArgs)
extern void Highlight_outHandler_m08281D04161918D998187DB9FD9FBBC44F7C8CEC ();
// 0x0000001E System.Void Highlight::.ctor()
extern void Highlight__ctor_mE02F969053F8F5B433A4047AF6DA9660E59B5AB5 ();
// 0x0000001F System.Collections.IEnumerator ShowMe::Start()
extern void ShowMe_Start_m1DD7DF2D5267CA544EC0B5A1688C03833932269D ();
// 0x00000020 System.Void ShowMe::.ctor()
extern void ShowMe__ctor_mED6AEBE0CF18FF46EB7C28FC95570A58FD15D6EA ();
// 0x00000021 System.Void imageBank::Start()
extern void imageBank_Start_m0FA24C2C5260EBC0A56D06CD47E23E7EC25E8764 ();
// 0x00000022 System.Void imageBank::Save()
extern void imageBank_Save_mD64E958D323A334B2ED50F5261B037EDAA1B6F9E ();
// 0x00000023 System.Void imageBank::.ctor()
extern void imageBank__ctor_m385D1C985E5F8091FC8632A695615E916902848E ();
// 0x00000024 System.Void test_nativegallery::TryPickImage()
extern void test_nativegallery_TryPickImage_mD9DA2AAE5585AFE7E98CB7BF7498366BCE3DED39 ();
// 0x00000025 System.Void test_nativegallery::PickImage(System.Int32)
extern void test_nativegallery_PickImage_m5836EF2B5F137638F5B23C1186F18096E8A8AF7E ();
// 0x00000026 System.Void test_nativegallery::.ctor()
extern void test_nativegallery__ctor_mAE0FE4727CE600AC1F35A864605E36BA18EFE427 ();
// 0x00000027 TouchScript.IGestureManager TouchScript.GestureManager::get_Instance()
extern void GestureManager_get_Instance_m3A10987EB55D0F84D701535F286BEE0C81C14E3E ();
// 0x00000028 System.Void TouchScript.GestureManager::.ctor()
extern void GestureManager__ctor_m3777A9BDB9285195D9CD102715094310E80DD992 ();
// 0x00000029 System.Boolean TouchScript.IGestureDelegate::ShouldReceivePointer(TouchScript.Gestures.Gesture,TouchScript.Pointers.Pointer)
// 0x0000002A System.Boolean TouchScript.IGestureDelegate::ShouldBegin(TouchScript.Gestures.Gesture)
// 0x0000002B System.Boolean TouchScript.IGestureDelegate::ShouldRecognizeSimultaneously(TouchScript.Gestures.Gesture,TouchScript.Gestures.Gesture)
// 0x0000002C System.Boolean TouchScript.IDebuggable::get_DebugMode()
// 0x0000002D System.Void TouchScript.IDebuggable::set_DebugMode(System.Boolean)
// 0x0000002E TouchScript.IGestureDelegate TouchScript.IGestureManager::get_GlobalGestureDelegate()
// 0x0000002F System.Void TouchScript.IGestureManager::set_GlobalGestureDelegate(TouchScript.IGestureDelegate)
// 0x00000030 System.Collections.Generic.IList`1<TouchScript.Layers.TouchLayer> TouchScript.ILayerManager::get_Layers()
// 0x00000031 System.Int32 TouchScript.ILayerManager::get_LayerCount()
// 0x00000032 System.Boolean TouchScript.ILayerManager::get_HasExclusive()
// 0x00000033 System.Boolean TouchScript.ILayerManager::AddLayer(TouchScript.Layers.TouchLayer,System.Int32,System.Boolean)
// 0x00000034 System.Boolean TouchScript.ILayerManager::RemoveLayer(TouchScript.Layers.TouchLayer)
// 0x00000035 System.Void TouchScript.ILayerManager::ChangeLayerIndex(System.Int32,System.Int32)
// 0x00000036 System.Void TouchScript.ILayerManager::ForEach(System.Func`2<TouchScript.Layers.TouchLayer,System.Boolean>)
// 0x00000037 System.Boolean TouchScript.ILayerManager::GetHitTarget(TouchScript.Pointers.IPointer,TouchScript.Hit.HitData&)
// 0x00000038 System.Void TouchScript.ILayerManager::SetExclusive(UnityEngine.Transform,System.Boolean)
// 0x00000039 System.Void TouchScript.ILayerManager::SetExclusive(System.Collections.Generic.IEnumerable`1<UnityEngine.Transform>)
// 0x0000003A System.Boolean TouchScript.ILayerManager::IsExclusive(UnityEngine.Transform)
// 0x0000003B System.Void TouchScript.ILayerManager::ClearExclusive()
// 0x0000003C System.Void TouchScript.ITouchManager::add_FrameStarted(System.EventHandler)
// 0x0000003D System.Void TouchScript.ITouchManager::remove_FrameStarted(System.EventHandler)
// 0x0000003E System.Void TouchScript.ITouchManager::add_FrameFinished(System.EventHandler)
// 0x0000003F System.Void TouchScript.ITouchManager::remove_FrameFinished(System.EventHandler)
// 0x00000040 System.Void TouchScript.ITouchManager::add_PointersAdded(System.EventHandler`1<TouchScript.PointerEventArgs>)
// 0x00000041 System.Void TouchScript.ITouchManager::remove_PointersAdded(System.EventHandler`1<TouchScript.PointerEventArgs>)
// 0x00000042 System.Void TouchScript.ITouchManager::add_PointersUpdated(System.EventHandler`1<TouchScript.PointerEventArgs>)
// 0x00000043 System.Void TouchScript.ITouchManager::remove_PointersUpdated(System.EventHandler`1<TouchScript.PointerEventArgs>)
// 0x00000044 System.Void TouchScript.ITouchManager::add_PointersPressed(System.EventHandler`1<TouchScript.PointerEventArgs>)
// 0x00000045 System.Void TouchScript.ITouchManager::remove_PointersPressed(System.EventHandler`1<TouchScript.PointerEventArgs>)
// 0x00000046 System.Void TouchScript.ITouchManager::add_PointersReleased(System.EventHandler`1<TouchScript.PointerEventArgs>)
// 0x00000047 System.Void TouchScript.ITouchManager::remove_PointersReleased(System.EventHandler`1<TouchScript.PointerEventArgs>)
// 0x00000048 System.Void TouchScript.ITouchManager::add_PointersRemoved(System.EventHandler`1<TouchScript.PointerEventArgs>)
// 0x00000049 System.Void TouchScript.ITouchManager::remove_PointersRemoved(System.EventHandler`1<TouchScript.PointerEventArgs>)
// 0x0000004A System.Void TouchScript.ITouchManager::add_PointersCancelled(System.EventHandler`1<TouchScript.PointerEventArgs>)
// 0x0000004B System.Void TouchScript.ITouchManager::remove_PointersCancelled(System.EventHandler`1<TouchScript.PointerEventArgs>)
// 0x0000004C TouchScript.Devices.Display.IDisplayDevice TouchScript.ITouchManager::get_DisplayDevice()
// 0x0000004D System.Void TouchScript.ITouchManager::set_DisplayDevice(TouchScript.Devices.Display.IDisplayDevice)
// 0x0000004E System.Single TouchScript.ITouchManager::get_DPI()
// 0x0000004F System.Boolean TouchScript.ITouchManager::get_ShouldCreateCameraLayer()
// 0x00000050 System.Void TouchScript.ITouchManager::set_ShouldCreateCameraLayer(System.Boolean)
// 0x00000051 System.Boolean TouchScript.ITouchManager::get_ShouldCreateStandardInput()
// 0x00000052 System.Void TouchScript.ITouchManager::set_ShouldCreateStandardInput(System.Boolean)
// 0x00000053 System.Collections.Generic.IList`1<TouchScript.InputSources.IInputSource> TouchScript.ITouchManager::get_Inputs()
// 0x00000054 System.Single TouchScript.ITouchManager::get_DotsPerCentimeter()
// 0x00000055 System.Int32 TouchScript.ITouchManager::get_PointersCount()
// 0x00000056 System.Int32 TouchScript.ITouchManager::get_PressedPointersCount()
// 0x00000057 System.Collections.Generic.IList`1<TouchScript.Pointers.Pointer> TouchScript.ITouchManager::get_Pointers()
// 0x00000058 System.Collections.Generic.IList`1<TouchScript.Pointers.Pointer> TouchScript.ITouchManager::get_PressedPointers()
// 0x00000059 System.Boolean TouchScript.ITouchManager::get_IsInsidePointerFrame()
// 0x0000005A System.Boolean TouchScript.ITouchManager::AddInput(TouchScript.InputSources.IInputSource)
// 0x0000005B System.Boolean TouchScript.ITouchManager::RemoveInput(TouchScript.InputSources.IInputSource)
// 0x0000005C System.Void TouchScript.ITouchManager::CancelPointer(System.Int32,System.Boolean)
// 0x0000005D System.Void TouchScript.ITouchManager::CancelPointer(System.Int32)
// 0x0000005E System.Void TouchScript.ITouchManager::UpdateResolution()
// 0x0000005F System.Collections.Generic.IList`1<TouchScript.Pointers.Pointer> TouchScript.PointerEventArgs::get_Pointers()
extern void PointerEventArgs_get_Pointers_m16B91F1D5BE47012CF6C4BC0BC3F53691525FC41 ();
// 0x00000060 System.Void TouchScript.PointerEventArgs::set_Pointers(System.Collections.Generic.IList`1<TouchScript.Pointers.Pointer>)
extern void PointerEventArgs_set_Pointers_m80547B23A481F55D60DEE830E6340F85555B550A ();
// 0x00000061 System.Void TouchScript.PointerEventArgs::.ctor()
extern void PointerEventArgs__ctor_m2184ED9B05EDC50EC544304A7B0EEE2BE31DA2F0 ();
// 0x00000062 TouchScript.PointerEventArgs TouchScript.PointerEventArgs::GetCachedEventArgs(System.Collections.Generic.IList`1<TouchScript.Pointers.Pointer>)
extern void PointerEventArgs_GetCachedEventArgs_m32D6BD4E2C26738D2BB82F24904DBBB59587ED5F ();
// 0x00000063 TouchScript.ILayerManager TouchScript.LayerManager::get_Instance()
extern void LayerManager_get_Instance_mA50DB5F8ACCAF0DCDE4BEEA78843142A752CF9F7 ();
// 0x00000064 System.Void TouchScript.LayerManager::.ctor()
extern void LayerManager__ctor_m4FCB94F38604B9609E6E6A67A11C13464FCC2661 ();
// 0x00000065 TouchScript.ITouchManager TouchScript.TouchManager::get_Instance()
extern void TouchManager_get_Instance_m355FC1AAF7F91B86F174A4429E8A44ABE454E8E3 ();
// 0x00000066 TouchScript.Devices.Display.IDisplayDevice TouchScript.TouchManager::get_DisplayDevice()
extern void TouchManager_get_DisplayDevice_m342110501378ACD88AFE29BCE3D2F1DB58D41D00 ();
// 0x00000067 System.Void TouchScript.TouchManager::set_DisplayDevice(TouchScript.Devices.Display.IDisplayDevice)
extern void TouchManager_set_DisplayDevice_m473676E71E3A26D1B447AC97A228687CA1FE9C71 ();
// 0x00000068 System.Boolean TouchScript.TouchManager::get_ShouldCreateCameraLayer()
extern void TouchManager_get_ShouldCreateCameraLayer_m9555EEF395EA3D416B86CD071695F48749CEA641 ();
// 0x00000069 System.Void TouchScript.TouchManager::set_ShouldCreateCameraLayer(System.Boolean)
extern void TouchManager_set_ShouldCreateCameraLayer_m321C5C1830A0EF4AE5FEB0FB5A33A5E32B62D938 ();
// 0x0000006A System.Boolean TouchScript.TouchManager::get_ShouldCreateStandardInput()
extern void TouchManager_get_ShouldCreateStandardInput_mE33919A0FE9C7D46BA910362D807179381DFAAE1 ();
// 0x0000006B System.Void TouchScript.TouchManager::set_ShouldCreateStandardInput(System.Boolean)
extern void TouchManager_set_ShouldCreateStandardInput_m06BB13C8B96C2A1040F4EB85BAEC51BDF4FAFEDA ();
// 0x0000006C System.Boolean TouchScript.TouchManager::get_UseSendMessage()
extern void TouchManager_get_UseSendMessage_m6174E37C90F89BBF01BDD07A57AA79D8153E85A9 ();
// 0x0000006D System.Void TouchScript.TouchManager::set_UseSendMessage(System.Boolean)
extern void TouchManager_set_UseSendMessage_m7885229E5BA8439B403C3BFFEEF0E1C7315D2009 ();
// 0x0000006E TouchScript.TouchManager_MessageType TouchScript.TouchManager::get_SendMessageEvents()
extern void TouchManager_get_SendMessageEvents_m807C4A591F1C10FB759D7470C7D1F54484C478E0 ();
// 0x0000006F System.Void TouchScript.TouchManager::set_SendMessageEvents(TouchScript.TouchManager_MessageType)
extern void TouchManager_set_SendMessageEvents_m83F740F8FC4AE4DC5FC8BC5349C2964F8E3325FA ();
// 0x00000070 UnityEngine.GameObject TouchScript.TouchManager::get_SendMessageTarget()
extern void TouchManager_get_SendMessageTarget_m0410C0052F476BB4C6B06ACAF535FFCBDFD21863 ();
// 0x00000071 System.Void TouchScript.TouchManager::set_SendMessageTarget(UnityEngine.GameObject)
extern void TouchManager_set_SendMessageTarget_m887C577DCA1779ABB748FF27E07892D9FDBAA1BA ();
// 0x00000072 System.Boolean TouchScript.TouchManager::get_UseUnityEvents()
extern void TouchManager_get_UseUnityEvents_m7AD3163F983E7429A268638E986C75EEC6620ECE ();
// 0x00000073 System.Void TouchScript.TouchManager::set_UseUnityEvents(System.Boolean)
extern void TouchManager_set_UseUnityEvents_m330D1F5971F4A6D8C5BEE0D2F36A6F721CEC56F7 ();
// 0x00000074 System.Boolean TouchScript.TouchManager::IsInvalidPosition(UnityEngine.Vector2)
extern void TouchManager_IsInvalidPosition_m80842AEBF1770FB52A67BC73957A1B1956D2716F ();
// 0x00000075 System.Void TouchScript.TouchManager::Awake()
extern void TouchManager_Awake_m9417FD558FC15583C25CEB2135A9A8F9053A0773 ();
// 0x00000076 System.Void TouchScript.TouchManager::OnEnable()
extern void TouchManager_OnEnable_m9C0ECA70F64AACDA06C0B83E848F5F3030AC3549 ();
// 0x00000077 System.Void TouchScript.TouchManager::OnDisable()
extern void TouchManager_OnDisable_m9039E96AFD2AFA352E2EE0D14007F3779E130C8D ();
// 0x00000078 System.Void TouchScript.TouchManager::switchToBasicEditor()
extern void TouchManager_switchToBasicEditor_m3A1C84E7DDBA54ED96FAC435729BC58C5F3B636E ();
// 0x00000079 System.Void TouchScript.TouchManager::updateSendMessageSubscription()
extern void TouchManager_updateSendMessageSubscription_mAA916B61823E409F702F20238AFA50536FBABADA ();
// 0x0000007A System.Void TouchScript.TouchManager::removeSendMessageSubscriptions()
extern void TouchManager_removeSendMessageSubscriptions_mDA19EA010E735C99BF9CEC546274DAC4694CF57C ();
// 0x0000007B System.Void TouchScript.TouchManager::pointersAddedSendMessageHandler(System.Object,TouchScript.PointerEventArgs)
extern void TouchManager_pointersAddedSendMessageHandler_m63C06829F976D339666BFF3FAB8530E101355FDD ();
// 0x0000007C System.Void TouchScript.TouchManager::pointersUpdatedSendMessageHandler(System.Object,TouchScript.PointerEventArgs)
extern void TouchManager_pointersUpdatedSendMessageHandler_m58381274AF569B5230025CFB6ABCDE92318C4606 ();
// 0x0000007D System.Void TouchScript.TouchManager::pointersPressedSendMessageHandler(System.Object,TouchScript.PointerEventArgs)
extern void TouchManager_pointersPressedSendMessageHandler_m93132F18416FB55153153A1F9F4D53C46255CF99 ();
// 0x0000007E System.Void TouchScript.TouchManager::pointersReleasedSendMessageHandler(System.Object,TouchScript.PointerEventArgs)
extern void TouchManager_pointersReleasedSendMessageHandler_mBA8E495E7939B9A1F66F81C7179514F1F9EAE3FF ();
// 0x0000007F System.Void TouchScript.TouchManager::pointersRemovedSendMessageHandler(System.Object,TouchScript.PointerEventArgs)
extern void TouchManager_pointersRemovedSendMessageHandler_m57EA76B182D2BAD64AA62E1141EBC21CD80AE940 ();
// 0x00000080 System.Void TouchScript.TouchManager::pointersCancelledSendMessageHandler(System.Object,TouchScript.PointerEventArgs)
extern void TouchManager_pointersCancelledSendMessageHandler_m4317C08AB3D25607D0B1544FBB0AEBE063597BFE ();
// 0x00000081 System.Void TouchScript.TouchManager::frameStartedSendMessageHandler(System.Object,System.EventArgs)
extern void TouchManager_frameStartedSendMessageHandler_mAE3F3B2767D5FA1521A68F0275B31A7A9B3B2E04 ();
// 0x00000082 System.Void TouchScript.TouchManager::frameFinishedSendMessageHandler(System.Object,System.EventArgs)
extern void TouchManager_frameFinishedSendMessageHandler_m06643F8EE650407C385EF6648920F3A211E0D5E6 ();
// 0x00000083 System.Void TouchScript.TouchManager::updateUnityEventsSubscription()
extern void TouchManager_updateUnityEventsSubscription_m6A5CA7A27D10F83B2A5CA578C3446A140E2FF3B4 ();
// 0x00000084 System.Void TouchScript.TouchManager::removeUnityEventsSubscriptions()
extern void TouchManager_removeUnityEventsSubscriptions_m16B73F9CF5C8D19E6FB2776C07439A1F6A93F3FA ();
// 0x00000085 System.Void TouchScript.TouchManager::pointersAddedUnityEventsHandler(System.Object,TouchScript.PointerEventArgs)
extern void TouchManager_pointersAddedUnityEventsHandler_m05BDEF56AC60141C69CFECD9324EAF9DCCBA3859 ();
// 0x00000086 System.Void TouchScript.TouchManager::pointersUpdatedUnityEventsHandler(System.Object,TouchScript.PointerEventArgs)
extern void TouchManager_pointersUpdatedUnityEventsHandler_mDD4D2BBAA16EB45E6473E25CAB802E6B1C02C719 ();
// 0x00000087 System.Void TouchScript.TouchManager::pointersPressedUnityEventsHandler(System.Object,TouchScript.PointerEventArgs)
extern void TouchManager_pointersPressedUnityEventsHandler_m82F19BBA6DFDA4EA2767F2F51EA100083F5C2F84 ();
// 0x00000088 System.Void TouchScript.TouchManager::pointersReleasedUnityEventsHandler(System.Object,TouchScript.PointerEventArgs)
extern void TouchManager_pointersReleasedUnityEventsHandler_m79811722367588FAA1AE65FB9B6F5250BCC92382 ();
// 0x00000089 System.Void TouchScript.TouchManager::pointersRemovedUnityEventsHandler(System.Object,TouchScript.PointerEventArgs)
extern void TouchManager_pointersRemovedUnityEventsHandler_mD8CB51C6894922EBAED67BD56F9920B78DA208FB ();
// 0x0000008A System.Void TouchScript.TouchManager::pointersCancelledUnityEventsHandler(System.Object,TouchScript.PointerEventArgs)
extern void TouchManager_pointersCancelledUnityEventsHandler_mFEB2D2BA0CA023BC04A9ADA27B2CFED543EA6A26 ();
// 0x0000008B System.Void TouchScript.TouchManager::frameStartedUnityEventsHandler(System.Object,System.EventArgs)
extern void TouchManager_frameStartedUnityEventsHandler_m89C3B5C487CEB8878A3B71B698F8BB814713ECF0 ();
// 0x0000008C System.Void TouchScript.TouchManager::frameFinishedUnityEventsHandler(System.Object,System.EventArgs)
extern void TouchManager_frameFinishedUnityEventsHandler_mBFE2FF2587FEE191DB70D58242692DC0690191ED ();
// 0x0000008D System.Void TouchScript.TouchManager::.ctor()
extern void TouchManager__ctor_m54F25692CE236EEBEEA41BD3E445AB6042BA35E9 ();
// 0x0000008E System.Void TouchScript.TouchManager::.cctor()
extern void TouchManager__cctor_mA1F542857C7735E0DC46820A360BF8B2A9E48366 ();
// 0x0000008F System.Void TouchScript.Utils.BinaryUtils::ToBinaryString(System.UInt32,System.Text.StringBuilder,System.Int32)
extern void BinaryUtils_ToBinaryString_mCB55E824E5042B4C35CD8EF55C3090C73D1B3FEC ();
// 0x00000090 System.String TouchScript.Utils.BinaryUtils::ToBinaryString(System.UInt32,System.Int32)
extern void BinaryUtils_ToBinaryString_m841313E6549570892EDB2B9C9421BB3C54F64F03 ();
// 0x00000091 System.UInt32 TouchScript.Utils.BinaryUtils::ToBinaryMask(System.Collections.Generic.IEnumerable`1<System.Boolean>)
extern void BinaryUtils_ToBinaryMask_mB486631A929B8755B239F3BD07EBD208F3F7E8AE ();
// 0x00000092 UnityEngine.Vector2 TouchScript.Utils.ClusterUtils::Get2DCenterPosition(System.Collections.Generic.IList`1<TouchScript.Pointers.Pointer>)
extern void ClusterUtils_Get2DCenterPosition_m85D3BC7F9ABF6B24256FC4F7EEC887B8EDC25293 ();
// 0x00000093 UnityEngine.Vector2 TouchScript.Utils.ClusterUtils::GetPrevious2DCenterPosition(System.Collections.Generic.IList`1<TouchScript.Pointers.Pointer>)
extern void ClusterUtils_GetPrevious2DCenterPosition_mE261E1BF0B86549ED5FE9DA1945C874585EF60BB ();
// 0x00000094 System.String TouchScript.Utils.ClusterUtils::GetPointsHash(System.Collections.Generic.IList`1<TouchScript.Pointers.Pointer>)
extern void ClusterUtils_GetPointsHash_m31E06AECDB1ACD7975B9B98CA151E538E1EFA823 ();
// 0x00000095 System.Void TouchScript.Utils.ClusterUtils::.cctor()
extern void ClusterUtils__cctor_m2E71E8524F593F6D8BABC472814BC6A3BC5176A6 ();
// 0x00000096 System.Exception TouchScript.Utils.EventHandlerExtensions::InvokeHandleExceptions(System.EventHandler`1<T>,System.Object,T)
// 0x00000097 System.Exception TouchScript.Utils.EventHandlerExtensions::InvokeHandleExceptions(System.EventHandler,System.Object,System.EventArgs)
extern void EventHandlerExtensions_InvokeHandleExceptions_m8E6D5FEFA89D62212811606B5639EA40526C8509 ();
// 0x00000098 System.String TouchScript.Utils.ObjectPool`1::get_Name()
// 0x00000099 System.Void TouchScript.Utils.ObjectPool`1::set_Name(System.String)
// 0x0000009A System.Int32 TouchScript.Utils.ObjectPool`1::get_CountAll()
// 0x0000009B System.Void TouchScript.Utils.ObjectPool`1::set_CountAll(System.Int32)
// 0x0000009C System.Int32 TouchScript.Utils.ObjectPool`1::get_CountActive()
// 0x0000009D System.Int32 TouchScript.Utils.ObjectPool`1::get_CountInactive()
// 0x0000009E System.Void TouchScript.Utils.ObjectPool`1::.ctor(System.Int32,TouchScript.Utils.ObjectPool`1_UnityFunc`1<T,T>,UnityEngine.Events.UnityAction`1<T>,UnityEngine.Events.UnityAction`1<T>,System.String)
// 0x0000009F System.Void TouchScript.Utils.ObjectPool`1::WarmUp(System.Int32)
// 0x000000A0 T TouchScript.Utils.ObjectPool`1::Get()
// 0x000000A1 System.Void TouchScript.Utils.ObjectPool`1::Release(T)
// 0x000000A2 System.Void TouchScript.Utils.ObjectPool`1::Release(System.Object)
// 0x000000A3 System.Boolean TouchScript.Utils.PointerUtils::IsPointerOnTarget(TouchScript.Pointers.Pointer)
extern void PointerUtils_IsPointerOnTarget_mC471A1AE57B2F02F0CB977FF51AB0BCD7706C6DE ();
// 0x000000A4 System.Boolean TouchScript.Utils.PointerUtils::IsPointerOnTarget(TouchScript.Pointers.IPointer,UnityEngine.Transform)
extern void PointerUtils_IsPointerOnTarget_m73191DDEDDAA92FC5B63E82BE8C94DB49948E576 ();
// 0x000000A5 System.Boolean TouchScript.Utils.PointerUtils::IsPointerOnTarget(TouchScript.Pointers.IPointer,UnityEngine.Transform,TouchScript.Hit.HitData&)
extern void PointerUtils_IsPointerOnTarget_mFA6B5C217481FFAC6208A08D8564B02C464C2DBF ();
// 0x000000A6 System.String TouchScript.Utils.PointerUtils::PressedButtonsToString(TouchScript.Pointers.Pointer_PointerButtonState)
extern void PointerUtils_PressedButtonsToString_m57F1173F8613DA8D378DB09AA0FDB41285C639A5 ();
// 0x000000A7 System.Void TouchScript.Utils.PointerUtils::PressedButtonsToString(TouchScript.Pointers.Pointer_PointerButtonState,System.Text.StringBuilder)
extern void PointerUtils_PressedButtonsToString_mAF4CD9F932292EE068D39024DDD1E70F7DC26D9F ();
// 0x000000A8 System.String TouchScript.Utils.PointerUtils::ButtonsToString(TouchScript.Pointers.Pointer_PointerButtonState)
extern void PointerUtils_ButtonsToString_mEC2274B4CDCC3BC17F12957876CA4760A47815B7 ();
// 0x000000A9 System.Void TouchScript.Utils.PointerUtils::ButtonsToString(TouchScript.Pointers.Pointer_PointerButtonState,System.Text.StringBuilder)
extern void PointerUtils_ButtonsToString_m90AE750BDB47BF3343173B2DC737E10AE1BB0C21 ();
// 0x000000AA TouchScript.Pointers.Pointer_PointerButtonState TouchScript.Utils.PointerUtils::DownPressedButtons(TouchScript.Pointers.Pointer_PointerButtonState)
extern void PointerUtils_DownPressedButtons_mA925780101477141AEADCBD36F73DBA3EF36816E ();
// 0x000000AB TouchScript.Pointers.Pointer_PointerButtonState TouchScript.Utils.PointerUtils::PressDownButtons(TouchScript.Pointers.Pointer_PointerButtonState)
extern void PointerUtils_PressDownButtons_mA71150EFD6A6F014898A56D35B72049AC7E569DD ();
// 0x000000AC TouchScript.Pointers.Pointer_PointerButtonState TouchScript.Utils.PointerUtils::UpPressedButtons(TouchScript.Pointers.Pointer_PointerButtonState)
extern void PointerUtils_UpPressedButtons_mE54D033069BCF06BA90CBE76507B340ACC4F58DC ();
// 0x000000AD System.Void TouchScript.Utils.PointerUtils::initStringBuilder()
extern void PointerUtils_initStringBuilder_mEB6698C4E28B8F6D3E4E40FE5CA452C8415C179C ();
// 0x000000AE System.Void TouchScript.Utils.TimedSequence`1::Add(T)
// 0x000000AF System.Void TouchScript.Utils.TimedSequence`1::Add(T,System.Single)
// 0x000000B0 System.Void TouchScript.Utils.TimedSequence`1::Clear()
// 0x000000B1 System.Collections.Generic.IList`1<T> TouchScript.Utils.TimedSequence`1::FindElementsLaterThan(System.Single)
// 0x000000B2 System.Collections.Generic.IList`1<T> TouchScript.Utils.TimedSequence`1::FindElementsLaterThan(System.Single,System.Single&)
// 0x000000B3 System.Collections.Generic.IList`1<T> TouchScript.Utils.TimedSequence`1::FindElementsLaterThan(System.Single,System.Predicate`1<T>)
// 0x000000B4 System.Void TouchScript.Utils.TimedSequence`1::.ctor()
// 0x000000B5 UnityEngine.Vector3 TouchScript.Utils.TransformUtils::GlobalToLocalPosition(UnityEngine.Transform,UnityEngine.Vector3)
extern void TransformUtils_GlobalToLocalPosition_m83D494F04656DBAD08C6443DEDE9A014171743FD ();
// 0x000000B6 UnityEngine.Vector3 TouchScript.Utils.TransformUtils::GlobalToLocalDirection(UnityEngine.Transform,UnityEngine.Vector3)
extern void TransformUtils_GlobalToLocalDirection_m51021C8A547001E01BEFD522426645789F7F7C76 ();
// 0x000000B7 UnityEngine.Vector3 TouchScript.Utils.TransformUtils::GlobalToLocalVector(UnityEngine.Transform,UnityEngine.Vector3)
extern void TransformUtils_GlobalToLocalVector_m57DE02E94AFA43DD446DD9C16B46140FADCF1003 ();
// 0x000000B8 System.String TouchScript.Utils.TransformUtils::GetHeirarchyPath(UnityEngine.Transform)
extern void TransformUtils_GetHeirarchyPath_m0AEF337A0A5E5D3DD2686702E34749D3AC63B0E5 ();
// 0x000000B9 System.Void TouchScript.Utils.TransformUtils::initStringBuilder()
extern void TransformUtils_initStringBuilder_m6846E6CB8306F423AC924826D4D12667ED674C99 ();
// 0x000000BA UnityEngine.Vector3 TouchScript.Utils.Geom.ProjectionUtils::CameraToPlaneProjection(UnityEngine.Vector2,UnityEngine.Camera,UnityEngine.Plane)
extern void ProjectionUtils_CameraToPlaneProjection_m49C90C2A65AB392F106E1B043BB38EC1D603376A ();
// 0x000000BB UnityEngine.Vector3 TouchScript.Utils.Geom.ProjectionUtils::ScreenToPlaneProjection(UnityEngine.Vector2,UnityEngine.Plane)
extern void ProjectionUtils_ScreenToPlaneProjection_m5ED3FA8D18B088F44849EC32C475DE400C7B63D5 ();
// 0x000000BC System.Single TouchScript.Utils.Geom.TwoD::PointToLineDistance(UnityEngine.Vector2,UnityEngine.Vector2,UnityEngine.Vector2)
extern void TwoD_PointToLineDistance_m423B16A5A7384D020178775D8019DE019E7C52B0 ();
// 0x000000BD System.Void TouchScript.Utils.Geom.TwoD::PointToLineDistance2(UnityEngine.Vector2,UnityEngine.Vector2,UnityEngine.Vector2,UnityEngine.Vector2,System.Single&,System.Single&)
extern void TwoD_PointToLineDistance2_mA904184ADD13C33E7951C56BB2B8A16C167686F9 ();
// 0x000000BE UnityEngine.Vector2 TouchScript.Utils.Geom.TwoD::Rotate(UnityEngine.Vector2,System.Single)
extern void TwoD_Rotate_m5077CC8359AAB1CE9AFD09F8F8A10E6FB2E6BF42 ();
// 0x000000BF System.Void TouchScript.Utils.Attributes.NullToggleAttribute::.ctor()
extern void NullToggleAttribute__ctor_m35A491114899DA79F9BE73EA8B91B70FFC5041F0 ();
// 0x000000C0 System.Void TouchScript.Utils.Attributes.ToggleLeftAttribute::.ctor()
extern void ToggleLeftAttribute__ctor_mF0FF749A04925D5C49276E1DEE37922A4087A4E3 ();
// 0x000000C1 System.Int32 TouchScript.Pointers.FakePointer::get_Id()
extern void FakePointer_get_Id_m7F37A5013C88FA454666B85294990FD3C5BE5EE3 ();
// 0x000000C2 System.Void TouchScript.Pointers.FakePointer::set_Id(System.Int32)
extern void FakePointer_set_Id_mC0D41C4688A1B33B826E36B909C6A0E3983EFEB5 ();
// 0x000000C3 TouchScript.Pointers.Pointer_PointerType TouchScript.Pointers.FakePointer::get_Type()
extern void FakePointer_get_Type_m0273E1B87410A58AF1941B97AE4547FE7F22B588 ();
// 0x000000C4 System.Void TouchScript.Pointers.FakePointer::set_Type(TouchScript.Pointers.Pointer_PointerType)
extern void FakePointer_set_Type_m79DF4E0AF1EEF7C1BD223A21042D27D9E12327EC ();
// 0x000000C5 TouchScript.InputSources.IInputSource TouchScript.Pointers.FakePointer::get_InputSource()
extern void FakePointer_get_InputSource_m43787A4A83F64C058A0E58DEA93B595FA05C0670 ();
// 0x000000C6 System.Void TouchScript.Pointers.FakePointer::set_InputSource(TouchScript.InputSources.IInputSource)
extern void FakePointer_set_InputSource_mB5DEBE9F1174C7584540B72CD9DE5FF267967CB5 ();
// 0x000000C7 UnityEngine.Vector2 TouchScript.Pointers.FakePointer::get_Position()
extern void FakePointer_get_Position_m9FD582FD15FF725FF49EF261D11455ABAF2F6BB7 ();
// 0x000000C8 System.Void TouchScript.Pointers.FakePointer::set_Position(UnityEngine.Vector2)
extern void FakePointer_set_Position_mCA620D1E36778D9DD9DB87D24F6899D5EFEBBF34 ();
// 0x000000C9 System.UInt32 TouchScript.Pointers.FakePointer::get_Flags()
extern void FakePointer_get_Flags_m85F8668433273916F920F9C20331964A5C0BB71A ();
// 0x000000CA System.Void TouchScript.Pointers.FakePointer::set_Flags(System.UInt32)
extern void FakePointer_set_Flags_m64E3B23758D21054F7CB36BB6033AA7088169EB8 ();
// 0x000000CB TouchScript.Pointers.Pointer_PointerButtonState TouchScript.Pointers.FakePointer::get_Buttons()
extern void FakePointer_get_Buttons_mFB0294AF9118DD3A20720111DD9BB5349873D224 ();
// 0x000000CC System.Void TouchScript.Pointers.FakePointer::set_Buttons(TouchScript.Pointers.Pointer_PointerButtonState)
extern void FakePointer_set_Buttons_m85F4C76E07206AAFFB0F917283159DCDC4261280 ();
// 0x000000CD UnityEngine.Vector2 TouchScript.Pointers.FakePointer::get_PreviousPosition()
extern void FakePointer_get_PreviousPosition_m2F7D67485F3BBA05DB7CC1E4A07AC85743450ECB ();
// 0x000000CE System.Void TouchScript.Pointers.FakePointer::set_PreviousPosition(UnityEngine.Vector2)
extern void FakePointer_set_PreviousPosition_mCA34B1330CCAD003F7A6033C7F3EB24C712C3608 ();
// 0x000000CF System.Void TouchScript.Pointers.FakePointer::.ctor(UnityEngine.Vector2)
extern void FakePointer__ctor_m296C5E4936AD664F9CA7065525B7AC8CEF1D5F0E ();
// 0x000000D0 System.Void TouchScript.Pointers.FakePointer::.ctor()
extern void FakePointer__ctor_mAB218D43DC15CD0732B9995C7F5EF5D08FFA2F3C ();
// 0x000000D1 TouchScript.Hit.HitData TouchScript.Pointers.FakePointer::GetOverData(System.Boolean)
extern void FakePointer_GetOverData_mAF9904063454D323B0D8E6758558C7F892680D31 ();
// 0x000000D2 System.Int32 TouchScript.Pointers.IPointer::get_Id()
// 0x000000D3 TouchScript.Pointers.Pointer_PointerType TouchScript.Pointers.IPointer::get_Type()
// 0x000000D4 TouchScript.Pointers.Pointer_PointerButtonState TouchScript.Pointers.IPointer::get_Buttons()
// 0x000000D5 TouchScript.InputSources.IInputSource TouchScript.Pointers.IPointer::get_InputSource()
// 0x000000D6 UnityEngine.Vector2 TouchScript.Pointers.IPointer::get_Position()
// 0x000000D7 System.Void TouchScript.Pointers.IPointer::set_Position(UnityEngine.Vector2)
// 0x000000D8 UnityEngine.Vector2 TouchScript.Pointers.IPointer::get_PreviousPosition()
// 0x000000D9 System.UInt32 TouchScript.Pointers.IPointer::get_Flags()
// 0x000000DA TouchScript.Hit.HitData TouchScript.Pointers.IPointer::GetOverData(System.Boolean)
// 0x000000DB UnityEngine.Vector2 TouchScript.Pointers.MousePointer::get_ScrollDelta()
extern void MousePointer_get_ScrollDelta_mC8F97C22EFBD42A56861A7B1EF1907185F0746A3 ();
// 0x000000DC System.Void TouchScript.Pointers.MousePointer::set_ScrollDelta(UnityEngine.Vector2)
extern void MousePointer_set_ScrollDelta_m352D328338CF5FF4420BEA90891189D57C517C1F ();
// 0x000000DD System.Void TouchScript.Pointers.MousePointer::.ctor(TouchScript.InputSources.IInputSource)
extern void MousePointer__ctor_m406338CE653654229558A5BCC38F97692A452158 ();
// 0x000000DE System.Void TouchScript.Pointers.MousePointer::CopyFrom(TouchScript.Pointers.Pointer)
extern void MousePointer_CopyFrom_m9568581ECA2A9BB14241FB2E292E4FE15591EA2F ();
// 0x000000DF System.Int32 TouchScript.Pointers.ObjectPointer::get_ObjectId()
extern void ObjectPointer_get_ObjectId_m19757D9C574D591D4DD3043EEFBDDCE1064B2581 ();
// 0x000000E0 System.Void TouchScript.Pointers.ObjectPointer::set_ObjectId(System.Int32)
extern void ObjectPointer_set_ObjectId_m4951549B2096276F8533B2D025F744CE8E5D4FDD ();
// 0x000000E1 System.Single TouchScript.Pointers.ObjectPointer::get_Width()
extern void ObjectPointer_get_Width_m92F855D6202D7ECC31204F7BFB1924B26DA5F007 ();
// 0x000000E2 System.Void TouchScript.Pointers.ObjectPointer::set_Width(System.Single)
extern void ObjectPointer_set_Width_mFCDC006E0026F249E35C88362BD748EE5A14AFD7 ();
// 0x000000E3 System.Single TouchScript.Pointers.ObjectPointer::get_Height()
extern void ObjectPointer_get_Height_m30D7BD97E5CD11924D9111F3011E0E453CAA6DA0 ();
// 0x000000E4 System.Void TouchScript.Pointers.ObjectPointer::set_Height(System.Single)
extern void ObjectPointer_set_Height_m793FAF0083E3C0DC47E6825EE6ECCA7F342CE66A ();
// 0x000000E5 System.Single TouchScript.Pointers.ObjectPointer::get_Angle()
extern void ObjectPointer_get_Angle_mF6824CF5CA2316D34523B893EE50BEF01DF9D1E9 ();
// 0x000000E6 System.Void TouchScript.Pointers.ObjectPointer::set_Angle(System.Single)
extern void ObjectPointer_set_Angle_m3710F36254EBE84ACBC00123055FFB1CE8D8D108 ();
// 0x000000E7 System.Void TouchScript.Pointers.ObjectPointer::.ctor(TouchScript.InputSources.IInputSource)
extern void ObjectPointer__ctor_m24E6EC51EA50C4F5BF815D88CF8F84A552D98A41 ();
// 0x000000E8 System.Void TouchScript.Pointers.ObjectPointer::CopyFrom(TouchScript.Pointers.Pointer)
extern void ObjectPointer_CopyFrom_mC615B4489D31D9AF0AEC3C4992CC93C7A562A14A ();
// 0x000000E9 System.Void TouchScript.Pointers.ObjectPointer::INTERNAL_Reset()
extern void ObjectPointer_INTERNAL_Reset_mAFD389FA014F7C7EE0FEC7A4F81CDB9D5CE410A4 ();
// 0x000000EA System.Single TouchScript.Pointers.PenPointer::get_Rotation()
extern void PenPointer_get_Rotation_m83B389E7974ED8C633C93AE5CC7811D3F8980BEA ();
// 0x000000EB System.Void TouchScript.Pointers.PenPointer::set_Rotation(System.Single)
extern void PenPointer_set_Rotation_m74FF90FEA33FF1621F9792CBDDE3A31569246E08 ();
// 0x000000EC System.Single TouchScript.Pointers.PenPointer::get_Pressure()
extern void PenPointer_get_Pressure_m7594304DCC404FB3D58E741002C56E92B44BFF30 ();
// 0x000000ED System.Void TouchScript.Pointers.PenPointer::set_Pressure(System.Single)
extern void PenPointer_set_Pressure_m33A397E626F343E987248BEC63FAB5F18621BD61 ();
// 0x000000EE System.Void TouchScript.Pointers.PenPointer::.ctor(TouchScript.InputSources.IInputSource)
extern void PenPointer__ctor_m6D8A7F282AAE1FA58ECC119BF00ECF3C534AE5E2 ();
// 0x000000EF System.Void TouchScript.Pointers.PenPointer::INTERNAL_Reset()
extern void PenPointer_INTERNAL_Reset_mB10622BC68374DB2B92C6111E0D3BAA2BF38A718 ();
// 0x000000F0 System.Int32 TouchScript.Pointers.Pointer::get_Id()
extern void Pointer_get_Id_mFF112114C234DC146CA93A7719C3F316E8DACCAD ();
// 0x000000F1 System.Void TouchScript.Pointers.Pointer::set_Id(System.Int32)
extern void Pointer_set_Id_mEB0B14E2905D84B287EABA142EEAFF7737D63886 ();
// 0x000000F2 TouchScript.Pointers.Pointer_PointerType TouchScript.Pointers.Pointer::get_Type()
extern void Pointer_get_Type_m8733FBC6C16FE2C6FBA301C50E8E26266720B9EB ();
// 0x000000F3 System.Void TouchScript.Pointers.Pointer::set_Type(TouchScript.Pointers.Pointer_PointerType)
extern void Pointer_set_Type_mC0D513634112590ABB8EB877E8BA70416BDF2625 ();
// 0x000000F4 TouchScript.Pointers.Pointer_PointerButtonState TouchScript.Pointers.Pointer::get_Buttons()
extern void Pointer_get_Buttons_mDD144F267D37B07E930E44E9B7D9357246D5839A ();
// 0x000000F5 System.Void TouchScript.Pointers.Pointer::set_Buttons(TouchScript.Pointers.Pointer_PointerButtonState)
extern void Pointer_set_Buttons_mAE901843226D346730CAD2CAD512793E4E4BEAC8 ();
// 0x000000F6 TouchScript.InputSources.IInputSource TouchScript.Pointers.Pointer::get_InputSource()
extern void Pointer_get_InputSource_m39065F33E59F38803F3CB3DB02DF22DF17332149 ();
// 0x000000F7 System.Void TouchScript.Pointers.Pointer::set_InputSource(TouchScript.InputSources.IInputSource)
extern void Pointer_set_InputSource_m646284A86B72DC008C111B607C12E38F4A4EA385 ();
// 0x000000F8 UnityEngine.Vector2 TouchScript.Pointers.Pointer::get_Position()
extern void Pointer_get_Position_mCE0B33AF8B7D86A8BA6824E7817A3DFCB29BE4BC ();
// 0x000000F9 System.Void TouchScript.Pointers.Pointer::set_Position(UnityEngine.Vector2)
extern void Pointer_set_Position_m00CDB115C2F9FE3EDDB493FCCF6016D09AD52C4A ();
// 0x000000FA UnityEngine.Vector2 TouchScript.Pointers.Pointer::get_PreviousPosition()
extern void Pointer_get_PreviousPosition_m18CBA71CDAE159A709A4F88D96F40B81B333118E ();
// 0x000000FB System.Void TouchScript.Pointers.Pointer::set_PreviousPosition(UnityEngine.Vector2)
extern void Pointer_set_PreviousPosition_m9825F629338D0DF6160DCB6DAADF3488860FA703 ();
// 0x000000FC System.UInt32 TouchScript.Pointers.Pointer::get_Flags()
extern void Pointer_get_Flags_m941FB1843C3FD979278B353B040264E56F68A883 ();
// 0x000000FD System.Void TouchScript.Pointers.Pointer::set_Flags(System.UInt32)
extern void Pointer_set_Flags_mF6A08BE8419E9E7DFD4CD15AF3A4E811734E9293 ();
// 0x000000FE TouchScript.Layers.ProjectionParams TouchScript.Pointers.Pointer::get_ProjectionParams()
extern void Pointer_get_ProjectionParams_mECA4594E2279D92F8731317FA2BA2F3CD7904C38 ();
// 0x000000FF TouchScript.Hit.HitData TouchScript.Pointers.Pointer::GetOverData(System.Boolean)
extern void Pointer_GetOverData_m3E939E2B28388235F9797C61CBBDABC0905DD195 ();
// 0x00000100 TouchScript.Hit.HitData TouchScript.Pointers.Pointer::GetPressData()
extern void Pointer_GetPressData_m806849D3319D7AF0EB6469AC978F593B1AD6A3B4 ();
// 0x00000101 System.Void TouchScript.Pointers.Pointer::CopyFrom(TouchScript.Pointers.Pointer)
extern void Pointer_CopyFrom_m323BE52F7920E028E626E6C42E1C5A230CAB1494 ();
// 0x00000102 System.Boolean TouchScript.Pointers.Pointer::Equals(System.Object)
extern void Pointer_Equals_mB22D2809944C62A89A29FB04C2FC45DA69F38D9A ();
// 0x00000103 System.Boolean TouchScript.Pointers.Pointer::Equals(TouchScript.Pointers.Pointer)
extern void Pointer_Equals_mBB1ABA52EFE71C48513257DDD931073295A2EE1C ();
// 0x00000104 System.Int32 TouchScript.Pointers.Pointer::GetHashCode()
extern void Pointer_GetHashCode_m408BF0466B46E542E764170915D38FDB1EA60B5B ();
// 0x00000105 System.String TouchScript.Pointers.Pointer::ToString()
extern void Pointer_ToString_mFFA8218E27F99EC02496ABECBB46E8D4A5AE37B6 ();
// 0x00000106 System.Void TouchScript.Pointers.Pointer::.ctor(TouchScript.InputSources.IInputSource)
extern void Pointer__ctor_mE7828F1485349BF2A610843ABB9A7944396F2D6C ();
// 0x00000107 System.Void TouchScript.Pointers.Pointer::INTERNAL_Init(System.Int32)
extern void Pointer_INTERNAL_Init_m2E683FC93E6121AAC8BF03B4B10E2DE3274A2C5C ();
// 0x00000108 System.Void TouchScript.Pointers.Pointer::INTERNAL_Reset()
extern void Pointer_INTERNAL_Reset_m851FB3F6C82DD723EB5A78235BC0B45865FE8897 ();
// 0x00000109 System.Void TouchScript.Pointers.Pointer::INTERNAL_FrameStarted()
extern void Pointer_INTERNAL_FrameStarted_mC677279C07B7D49D25BFC21580F50C74D037FAA0 ();
// 0x0000010A System.Void TouchScript.Pointers.Pointer::INTERNAL_UpdatePosition()
extern void Pointer_INTERNAL_UpdatePosition_mB4AE54E8D1CFC6EFDBD36FAA6A3DFD04E98E0716 ();
// 0x0000010B System.Void TouchScript.Pointers.Pointer::INTERNAL_Retain()
extern void Pointer_INTERNAL_Retain_m4B131382F3A12FF21FB907EEE0769FEA6704A0BF ();
// 0x0000010C System.Int32 TouchScript.Pointers.Pointer::INTERNAL_Release()
extern void Pointer_INTERNAL_Release_m15DC4D63F05CB4001750D188BC00502BF017927B ();
// 0x0000010D System.Void TouchScript.Pointers.Pointer::INTERNAL_SetPressData(TouchScript.Hit.HitData)
extern void Pointer_INTERNAL_SetPressData_m922C64806B13717F734D42422561C0FE31DA8B26 ();
// 0x0000010E System.Void TouchScript.Pointers.Pointer::INTERNAL_ClearPressData()
extern void Pointer_INTERNAL_ClearPressData_mBF3AD7F3B5D26FC7D9D50A7753BFA63FB3BFFF39 ();
// 0x0000010F TouchScript.Pointers.Pointer TouchScript.Pointers.PointerFactory::Create(TouchScript.Pointers.Pointer_PointerType,TouchScript.InputSources.IInputSource)
extern void PointerFactory_Create_m7D6B0A6806D107FFCF0D3BA8B4C3EB31B4E6055F ();
// 0x00000110 System.Single TouchScript.Pointers.TouchPointer::get_Rotation()
extern void TouchPointer_get_Rotation_m3ECB502F6BA93D13E74646A81460AAACF27B75AF ();
// 0x00000111 System.Void TouchScript.Pointers.TouchPointer::set_Rotation(System.Single)
extern void TouchPointer_set_Rotation_m87442B9A19EB126F7E42949A8E0FB1FC2CB88FF9 ();
// 0x00000112 System.Single TouchScript.Pointers.TouchPointer::get_Pressure()
extern void TouchPointer_get_Pressure_m894BA5476887A5547C328C11BE33BB03E2267462 ();
// 0x00000113 System.Void TouchScript.Pointers.TouchPointer::set_Pressure(System.Single)
extern void TouchPointer_set_Pressure_m9DAD082334050E94FCD81D8887D56CAA8D548BD0 ();
// 0x00000114 System.Void TouchScript.Pointers.TouchPointer::.ctor(TouchScript.InputSources.IInputSource)
extern void TouchPointer__ctor_m45AB2CE7C1661672F231AFA9D718A1F877FBF276 ();
// 0x00000115 System.Void TouchScript.Pointers.TouchPointer::INTERNAL_Reset()
extern void TouchPointer_INTERNAL_Reset_mAA8BF6032219E321A1CC86AEF61EF74A4ADAF0A2 ();
// 0x00000116 TouchScript.Layers.FullscreenLayer_LayerType TouchScript.Layers.FullscreenLayer::get_Type()
extern void FullscreenLayer_get_Type_m4B04610F987D3F71BDBEF7D19FEEC1A4D872F417 ();
// 0x00000117 System.Void TouchScript.Layers.FullscreenLayer::set_Type(TouchScript.Layers.FullscreenLayer_LayerType)
extern void FullscreenLayer_set_Type_mED3CD065A8EE3DB019329068C94E6F781C6E4B5C ();
// 0x00000118 UnityEngine.Camera TouchScript.Layers.FullscreenLayer::get_Camera()
extern void FullscreenLayer_get_Camera_m7DFE60A334FD4157B8B5609C8DF432E21FC9D658 ();
// 0x00000119 System.Void TouchScript.Layers.FullscreenLayer::set_Camera(UnityEngine.Camera)
extern void FullscreenLayer_set_Camera_m001B1EF63AA742960956B13A7DF3CAC5AB7FB9FF ();
// 0x0000011A UnityEngine.Vector3 TouchScript.Layers.FullscreenLayer::get_WorldProjectionNormal()
extern void FullscreenLayer_get_WorldProjectionNormal_mE1518BCAB865E2C9B459C17ABD601D784BCDE076 ();
// 0x0000011B TouchScript.Hit.HitResult TouchScript.Layers.FullscreenLayer::Hit(TouchScript.Pointers.IPointer,TouchScript.Hit.HitData&)
extern void FullscreenLayer_Hit_mF0461594D6E277C208EAF8C0A8C1968BF645F30C ();
// 0x0000011C System.Void TouchScript.Layers.FullscreenLayer::Awake()
extern void FullscreenLayer_Awake_m1FDED2FB2DD29062C6E2376B59C8C4A3A171A3B8 ();
// 0x0000011D System.Void TouchScript.Layers.FullscreenLayer::setName()
extern void FullscreenLayer_setName_m48D33B282BA78251E3A8EA17C87A600A62160C62 ();
// 0x0000011E TouchScript.Layers.ProjectionParams TouchScript.Layers.FullscreenLayer::createProjectionParams()
extern void FullscreenLayer_createProjectionParams_m524FE182CC2876BEA489FDBE48438318FF3F340C ();
// 0x0000011F System.Void TouchScript.Layers.FullscreenLayer::updateCamera()
extern void FullscreenLayer_updateCamera_mE8C5D7A045638E21BF482F1B68AF2077E5D35A75 ();
// 0x00000120 System.Void TouchScript.Layers.FullscreenLayer::cacheCameraTransform()
extern void FullscreenLayer_cacheCameraTransform_m7E488A981ABFE988101D410D0D24F877AABDD2C2 ();
// 0x00000121 System.Void TouchScript.Layers.FullscreenLayer::.ctor()
extern void FullscreenLayer__ctor_m28D29EB32FEE880687FF110654BA16227038D27D ();
// 0x00000122 System.Boolean TouchScript.Layers.ILayerDelegate::ShouldReceivePointer(TouchScript.Layers.TouchLayer,TouchScript.Pointers.IPointer)
// 0x00000123 UnityEngine.Vector3 TouchScript.Layers.ProjectionParams::ProjectTo(UnityEngine.Vector2,UnityEngine.Plane)
extern void ProjectionParams_ProjectTo_m7F72FAB69700D8EBFA9E8BE93880E8F4BEFA769B ();
// 0x00000124 UnityEngine.Vector2 TouchScript.Layers.ProjectionParams::ProjectFrom(UnityEngine.Vector3)
extern void ProjectionParams_ProjectFrom_m04E72D41FC2DAB6A14CAC3EBDB46A76D991314B1 ();
// 0x00000125 System.Void TouchScript.Layers.ProjectionParams::.ctor()
extern void ProjectionParams__ctor_m6A4C0C8062A36F504724741380C7479CCD9AB348 ();
// 0x00000126 System.Void TouchScript.Layers.CameraProjectionParams::.ctor(UnityEngine.Camera)
extern void CameraProjectionParams__ctor_mC62D0B363F564F5F93A329C693E8994D4A0FDC7B ();
// 0x00000127 UnityEngine.Vector3 TouchScript.Layers.CameraProjectionParams::ProjectTo(UnityEngine.Vector2,UnityEngine.Plane)
extern void CameraProjectionParams_ProjectTo_mD45439E04067FF83436D8C1EA9BF965FB77B4FE1 ();
// 0x00000128 UnityEngine.Vector2 TouchScript.Layers.CameraProjectionParams::ProjectFrom(UnityEngine.Vector3)
extern void CameraProjectionParams_ProjectFrom_mAC8A159F143B27D3F6B0651842BC763A8ACDEEE6 ();
// 0x00000129 System.Void TouchScript.Layers.WorldSpaceCanvasProjectionParams::.ctor(UnityEngine.Canvas)
extern void WorldSpaceCanvasProjectionParams__ctor_mDDE0E638C385DBFF93B8CB5802CF8D3D8B39BC4C ();
// 0x0000012A UnityEngine.Vector3 TouchScript.Layers.WorldSpaceCanvasProjectionParams::ProjectTo(UnityEngine.Vector2,UnityEngine.Plane)
extern void WorldSpaceCanvasProjectionParams_ProjectTo_m1D26196396B95BA4A98BAD0D3366D951DA12F030 ();
// 0x0000012B UnityEngine.Vector2 TouchScript.Layers.WorldSpaceCanvasProjectionParams::ProjectFrom(UnityEngine.Vector3)
extern void WorldSpaceCanvasProjectionParams_ProjectFrom_mBD552841734A8E42D1978277AF584B317C84A321 ();
// 0x0000012C System.Boolean TouchScript.Layers.StandardLayer::get_Hit3DObjects()
extern void StandardLayer_get_Hit3DObjects_m9DD3E44FAFE7E1FC43A8F044F0C7501FC568DF13 ();
// 0x0000012D System.Void TouchScript.Layers.StandardLayer::set_Hit3DObjects(System.Boolean)
extern void StandardLayer_set_Hit3DObjects_m330D2E44BFFB3838AB7CDD1C01BDB510451092C0 ();
// 0x0000012E System.Boolean TouchScript.Layers.StandardLayer::get_Hit2DObjects()
extern void StandardLayer_get_Hit2DObjects_mFC5C5EC8783A40FCF438BAE393B1D951A2A30503 ();
// 0x0000012F System.Void TouchScript.Layers.StandardLayer::set_Hit2DObjects(System.Boolean)
extern void StandardLayer_set_Hit2DObjects_m25653BAB030E5CD7BFBDEE55D6A46552A237F387 ();
// 0x00000130 System.Boolean TouchScript.Layers.StandardLayer::get_HitWorldSpaceUI()
extern void StandardLayer_get_HitWorldSpaceUI_mD600F9AC5657B229FE7A720EE452A1A4348B827E ();
// 0x00000131 System.Void TouchScript.Layers.StandardLayer::set_HitWorldSpaceUI(System.Boolean)
extern void StandardLayer_set_HitWorldSpaceUI_m1729EA20A511DDEEDD8EB9E0EA205DBEB4238618 ();
// 0x00000132 System.Boolean TouchScript.Layers.StandardLayer::get_HitScreenSpaceUI()
extern void StandardLayer_get_HitScreenSpaceUI_m473D8F8DA59E07AE14489FBB2A863747F54A00A8 ();
// 0x00000133 System.Void TouchScript.Layers.StandardLayer::set_HitScreenSpaceUI(System.Boolean)
extern void StandardLayer_set_HitScreenSpaceUI_m05D72794DCC58218035D08247E0B5099CD49DF7A ();
// 0x00000134 System.Boolean TouchScript.Layers.StandardLayer::get_UseHitFilters()
extern void StandardLayer_get_UseHitFilters_mAF2683CDA6FF64739E4F2807FDFBB8FA3FA7D1E8 ();
// 0x00000135 System.Void TouchScript.Layers.StandardLayer::set_UseHitFilters(System.Boolean)
extern void StandardLayer_set_UseHitFilters_m756B5FBB37682A62031832F616CD073784B10E28 ();
// 0x00000136 UnityEngine.LayerMask TouchScript.Layers.StandardLayer::get_LayerMask()
extern void StandardLayer_get_LayerMask_mF42BFB57C8F5C4057727E24D9C4996DA5F9B9401 ();
// 0x00000137 System.Void TouchScript.Layers.StandardLayer::set_LayerMask(UnityEngine.LayerMask)
extern void StandardLayer_set_LayerMask_mB3BA71C86E249584BDF082F20EC4651326D61795 ();
// 0x00000138 UnityEngine.Vector3 TouchScript.Layers.StandardLayer::get_WorldProjectionNormal()
extern void StandardLayer_get_WorldProjectionNormal_mA4A17DC1ABFA447F17CF3FC0442E4336FDF010DD ();
// 0x00000139 TouchScript.Hit.HitResult TouchScript.Layers.StandardLayer::Hit(TouchScript.Pointers.IPointer,TouchScript.Hit.HitData&)
extern void StandardLayer_Hit_mCB72499B9BB6BE4CAB26E6DE872345D82664F238 ();
// 0x0000013A TouchScript.Layers.ProjectionParams TouchScript.Layers.StandardLayer::GetProjectionParams(TouchScript.Pointers.Pointer)
extern void StandardLayer_GetProjectionParams_m659030799A5F67B4A1BEF9720C19F05C4070D9A1 ();
// 0x0000013B System.Void TouchScript.Layers.StandardLayer::Awake()
extern void StandardLayer_Awake_mF0FAEDCEDF303325290E141D6E04E603A92D4598 ();
// 0x0000013C System.Void TouchScript.Layers.StandardLayer::OnEnable()
extern void StandardLayer_OnEnable_m905342AECBFB079F2041AE5495FC6896B35D5099 ();
// 0x0000013D System.Collections.IEnumerator TouchScript.Layers.StandardLayer::lateEnable()
extern void StandardLayer_lateEnable_m0298E9000E4131E237A1EB027FACC04C6C6C64EF ();
// 0x0000013E System.Void TouchScript.Layers.StandardLayer::OnDisable()
extern void StandardLayer_OnDisable_m61CC0FB31E8DE1B69A6378B9144E8E8A0B5A3580 ();
// 0x0000013F System.Void TouchScript.Layers.StandardLayer::switchToBasicEditor()
extern void StandardLayer_switchToBasicEditor_mA853669EA719E3B95A615967699948BC59D1E6F0 ();
// 0x00000140 System.Void TouchScript.Layers.StandardLayer::updateCamera()
extern void StandardLayer_updateCamera_m378E206684CB41C6EAD2457AE2F5BF44447F84A4 ();
// 0x00000141 TouchScript.Layers.ProjectionParams TouchScript.Layers.StandardLayer::createProjectionParams()
extern void StandardLayer_createProjectionParams_mF728C6CFAD5AFF3C3EF7B2C1045530B33A22FFDD ();
// 0x00000142 System.Void TouchScript.Layers.StandardLayer::setName()
extern void StandardLayer_setName_m144092F07C29458533807D54A8EC081E6AE2F3C3 ();
// 0x00000143 System.Void TouchScript.Layers.StandardLayer::setupInputModule()
extern void StandardLayer_setupInputModule_mEF1427D87433C3EE20D055D4322EC53A19923859 ();
// 0x00000144 TouchScript.Hit.HitResult TouchScript.Layers.StandardLayer::performWorldSearch(TouchScript.Pointers.IPointer,TouchScript.Hit.HitData&)
extern void StandardLayer_performWorldSearch_m968AFCB9739F46BF9587DEEA5E0474C21E574593 ();
// 0x00000145 TouchScript.Hit.HitResult TouchScript.Layers.StandardLayer::performSSUISearch(TouchScript.Pointers.IPointer,TouchScript.Hit.HitData&)
extern void StandardLayer_performSSUISearch_m4B4D9E8334ACABBD763118078AEE5B7E4CCC156D ();
// 0x00000146 System.Void TouchScript.Layers.StandardLayer::performUISearchForCanvas(TouchScript.Pointers.IPointer,UnityEngine.Canvas,UnityEngine.UI.GraphicRaycaster,UnityEngine.Camera,System.Single,UnityEngine.Ray)
extern void StandardLayer_performUISearchForCanvas_m6801B1E5A376C25A9F1CEE232FE38B12E5771E81 ();
// 0x00000147 TouchScript.Hit.HitResult TouchScript.Layers.StandardLayer::doHit(TouchScript.Pointers.IPointer,TouchScript.Hit.RaycastHitUI,TouchScript.Hit.HitData&)
extern void StandardLayer_doHit_m93790EAA1BC87D9D1CEA17EC8AEA80E165F40F41 ();
// 0x00000148 TouchScript.Hit.HitResult TouchScript.Layers.StandardLayer::doHit(TouchScript.Pointers.IPointer,UnityEngine.RaycastHit,TouchScript.Hit.HitData&)
extern void StandardLayer_doHit_m0B3FFCFC5B953A6B66CDAC5CECBEFAA5DCCEF7FD ();
// 0x00000149 System.Void TouchScript.Layers.StandardLayer::updateVariants()
extern void StandardLayer_updateVariants_mFCB2F7B60AFDB2CD667715F8E9E3FDE412D01A4B ();
// 0x0000014A System.Int32 TouchScript.Layers.StandardLayer::raycastHitUIComparerFunc(TouchScript.Hit.RaycastHitUI,TouchScript.Hit.RaycastHitUI)
extern void StandardLayer_raycastHitUIComparerFunc_m24D15006D692933868D38BD870A0213CA168E90D ();
// 0x0000014B System.Int32 TouchScript.Layers.StandardLayer::raycastHitComparerFunc(UnityEngine.RaycastHit,UnityEngine.RaycastHit)
extern void StandardLayer_raycastHitComparerFunc_m494605C8BE0B7F67F5AFB10171F80FD39AD47295 ();
// 0x0000014C System.Int32 TouchScript.Layers.StandardLayer::hitDataComparerFunc(TouchScript.Hit.HitData,TouchScript.Hit.HitData)
extern void StandardLayer_hitDataComparerFunc_m7975F815112BB81FFCCA64AF47B610FE27DBC021 ();
// 0x0000014D System.Void TouchScript.Layers.StandardLayer::frameStartedHandler(System.Object,System.EventArgs)
extern void StandardLayer_frameStartedHandler_m60F3C8FB1A05528735DA6A7BC7FA00F3CD8C0986 ();
// 0x0000014E System.Void TouchScript.Layers.StandardLayer::.ctor()
extern void StandardLayer__ctor_m64661AD4B7455D67DD5C06FAA965E26E44E6738D ();
// 0x0000014F System.Void TouchScript.Layers.StandardLayer::.cctor()
extern void StandardLayer__cctor_mD654515376308B21FA82AFC8D3B207081E3ACE24 ();
// 0x00000150 System.Void TouchScript.Layers.TouchLayer::add_PointerBegan(System.EventHandler`1<TouchScript.Layers.TouchLayerEventArgs>)
extern void TouchLayer_add_PointerBegan_mD859EB0CD2D987A07BF5097EA01150E650B1C046 ();
// 0x00000151 System.Void TouchScript.Layers.TouchLayer::remove_PointerBegan(System.EventHandler`1<TouchScript.Layers.TouchLayerEventArgs>)
extern void TouchLayer_remove_PointerBegan_m439868A981A009DBD00BF07AD484DC5AE4BCA1C1 ();
// 0x00000152 UnityEngine.Vector3 TouchScript.Layers.TouchLayer::get_WorldProjectionNormal()
extern void TouchLayer_get_WorldProjectionNormal_m6A9114EEDAF3C043013DC105CB4A59EC99E4F46B ();
// 0x00000153 TouchScript.Layers.ILayerDelegate TouchScript.Layers.TouchLayer::get_Delegate()
extern void TouchLayer_get_Delegate_m323557CA7B99D9BF8610CBF3133192C2D214D7EE ();
// 0x00000154 System.Void TouchScript.Layers.TouchLayer::set_Delegate(TouchScript.Layers.ILayerDelegate)
extern void TouchLayer_set_Delegate_mF73EB797666FE4C3805F348DF2ACCF41F096308F ();
// 0x00000155 TouchScript.Layers.ProjectionParams TouchScript.Layers.TouchLayer::GetProjectionParams(TouchScript.Pointers.Pointer)
extern void TouchLayer_GetProjectionParams_m32695242FBEB2D7AF07CD670CA6850EA99C1A174 ();
// 0x00000156 TouchScript.Hit.HitResult TouchScript.Layers.TouchLayer::Hit(TouchScript.Pointers.IPointer,TouchScript.Hit.HitData&)
extern void TouchLayer_Hit_mF4746A5BE8F136AA19C580A754573F97F359C2CB ();
// 0x00000157 System.Void TouchScript.Layers.TouchLayer::Awake()
extern void TouchLayer_Awake_mCE25E28C3D1CBBC8767DBCD39EDF92A890F310CD ();
// 0x00000158 System.Collections.IEnumerator TouchScript.Layers.TouchLayer::lateAwake()
extern void TouchLayer_lateAwake_mB75570F442C3A16F263FEC0AD64FC83A103035D7 ();
// 0x00000159 System.Void TouchScript.Layers.TouchLayer::Start()
extern void TouchLayer_Start_m892EE66C1CA8E8382594A14FC83546CA0AA5572A ();
// 0x0000015A System.Void TouchScript.Layers.TouchLayer::OnDestroy()
extern void TouchLayer_OnDestroy_mA32E6957ECEDDBF720584AC3AD2533542C7BF499 ();
// 0x0000015B System.Void TouchScript.Layers.TouchLayer::INTERNAL_AddPointer(TouchScript.Pointers.Pointer)
extern void TouchLayer_INTERNAL_AddPointer_m92F774F1100FC9E763FA5ABE5AB44BF56E9EF533 ();
// 0x0000015C System.Void TouchScript.Layers.TouchLayer::INTERNAL_UpdatePointer(TouchScript.Pointers.Pointer)
extern void TouchLayer_INTERNAL_UpdatePointer_m950657B6D8931E2AF4D8FC92007D656EC4663EC3 ();
// 0x0000015D System.Boolean TouchScript.Layers.TouchLayer::INTERNAL_PressPointer(TouchScript.Pointers.Pointer)
extern void TouchLayer_INTERNAL_PressPointer_m1065338DB1DCAFE907587896FE76178EFF81E399 ();
// 0x0000015E System.Void TouchScript.Layers.TouchLayer::INTERNAL_ReleasePointer(TouchScript.Pointers.Pointer)
extern void TouchLayer_INTERNAL_ReleasePointer_m2F12AFBCD1A3F4B155D6F0199FEBB7F916D7E84F ();
// 0x0000015F System.Void TouchScript.Layers.TouchLayer::INTERNAL_RemovePointer(TouchScript.Pointers.Pointer)
extern void TouchLayer_INTERNAL_RemovePointer_m995AE661DD81080C9A840DFA8B189EE4CDB36902 ();
// 0x00000160 System.Void TouchScript.Layers.TouchLayer::INTERNAL_CancelPointer(TouchScript.Pointers.Pointer)
extern void TouchLayer_INTERNAL_CancelPointer_m32453B34AF6F491BCB5151F306E757E9CC20B4E4 ();
// 0x00000161 TouchScript.Hit.HitResult TouchScript.Layers.TouchLayer::checkHitFilters(TouchScript.Pointers.IPointer,TouchScript.Hit.HitData)
extern void TouchLayer_checkHitFilters_mEF3E87A8533094812A2AAA3AB40FBE6784CFD23E ();
// 0x00000162 System.Void TouchScript.Layers.TouchLayer::setName()
extern void TouchLayer_setName_mA8E3FB6ABAFBE2A3307768F8CAFC139471B640C1 ();
// 0x00000163 System.Void TouchScript.Layers.TouchLayer::addPointer(TouchScript.Pointers.Pointer)
extern void TouchLayer_addPointer_m2550CC60DF813CA5829FB386F78F8186C4FDCFA8 ();
// 0x00000164 System.Void TouchScript.Layers.TouchLayer::pressPointer(TouchScript.Pointers.Pointer)
extern void TouchLayer_pressPointer_m0AA646E9C4F275E51AC49733B141851509BEAFCD ();
// 0x00000165 System.Void TouchScript.Layers.TouchLayer::updatePointer(TouchScript.Pointers.Pointer)
extern void TouchLayer_updatePointer_m17938E18F21292DBA91368615D6D8BB1F6173019 ();
// 0x00000166 System.Void TouchScript.Layers.TouchLayer::releasePointer(TouchScript.Pointers.Pointer)
extern void TouchLayer_releasePointer_m6D4E3B31D7906D12859F894F30F5E40BE1324EA1 ();
// 0x00000167 System.Void TouchScript.Layers.TouchLayer::removePointer(TouchScript.Pointers.Pointer)
extern void TouchLayer_removePointer_mFB20AF0FD8BF9612D6B87D5E509071E9D3D3FF55 ();
// 0x00000168 System.Void TouchScript.Layers.TouchLayer::cancelPointer(TouchScript.Pointers.Pointer)
extern void TouchLayer_cancelPointer_mC637FBD7A983B2DEDE789537F7C28B8E8C0A3259 ();
// 0x00000169 TouchScript.Layers.ProjectionParams TouchScript.Layers.TouchLayer::createProjectionParams()
extern void TouchLayer_createProjectionParams_m517619874A42BCA26A8DA756CD37BDAE2EDA3315 ();
// 0x0000016A System.Void TouchScript.Layers.TouchLayer::.ctor()
extern void TouchLayer__ctor_mCC19DA14C5A7CDA9AB1A21C4F131FF84CD32A5A3 ();
// 0x0000016B TouchScript.Pointers.Pointer TouchScript.Layers.TouchLayerEventArgs::get_Pointer()
extern void TouchLayerEventArgs_get_Pointer_m6995F780D2D3EC5E2A40D4A87CB535EC5C40F228 ();
// 0x0000016C System.Void TouchScript.Layers.TouchLayerEventArgs::set_Pointer(TouchScript.Pointers.Pointer)
extern void TouchLayerEventArgs_set_Pointer_m31C50E02225B5A42D3802AB9DB0A55A9CF021266 ();
// 0x0000016D System.Void TouchScript.Layers.TouchLayerEventArgs::.ctor(TouchScript.Pointers.Pointer)
extern void TouchLayerEventArgs__ctor_mC51EB0A7AEC1F354BC6DA580359969C25D061E40 ();
// 0x0000016E TouchScript.Layers.UI.TouchScriptInputModule TouchScript.Layers.UI.TouchScriptInputModule::get_Instance()
extern void TouchScriptInputModule_get_Instance_m1C3312EE86FB0C6645698CE44E98EEF980716E8F ();
// 0x0000016F System.Void TouchScript.Layers.UI.TouchScriptInputModule::.ctor()
extern void TouchScriptInputModule__ctor_m597AA785F41E7C3563E337716CEA1BBCF21EC4F4 ();
// 0x00000170 System.Void TouchScript.Layers.UI.TouchScriptInputModule::OnEnable()
extern void TouchScriptInputModule_OnEnable_mD6149E783F7A1FB7E4DFCD19DC1F685211BF8E99 ();
// 0x00000171 System.Void TouchScript.Layers.UI.TouchScriptInputModule::OnDisable()
extern void TouchScriptInputModule_OnDisable_m7840DABE792EE8069831F67A07D5838927B080A4 ();
// 0x00000172 System.Void TouchScript.Layers.UI.TouchScriptInputModule::OnApplicationQuit()
extern void TouchScriptInputModule_OnApplicationQuit_m8B7D96693D1DB33653DEC030536F727AD95EB1C1 ();
// 0x00000173 System.Collections.Generic.List`1<UnityEngine.EventSystems.BaseRaycaster> TouchScript.Layers.UI.TouchScriptInputModule::GetRaycasters()
extern void TouchScriptInputModule_GetRaycasters_m4E5453F68A6C7837C997DCFD2006D356785087D6 ();
// 0x00000174 UnityEngine.Canvas TouchScript.Layers.UI.TouchScriptInputModule::GetCanvasForRaycaster(UnityEngine.EventSystems.BaseRaycaster)
extern void TouchScriptInputModule_GetCanvasForRaycaster_m48C9D00D72301BAD605954B38A4EEF4E9BA1802B ();
// 0x00000175 System.Void TouchScript.Layers.UI.TouchScriptInputModule::Process()
extern void TouchScriptInputModule_Process_m11C287676F1C042747AA7266E6209F3F2179790D ();
// 0x00000176 System.Boolean TouchScript.Layers.UI.TouchScriptInputModule::IsPointerOverGameObject(System.Int32)
extern void TouchScriptInputModule_IsPointerOverGameObject_mC5B728CCFBD02DC689CFF35C4CB8BEF23343B345 ();
// 0x00000177 System.Boolean TouchScript.Layers.UI.TouchScriptInputModule::ShouldActivateModule()
extern void TouchScriptInputModule_ShouldActivateModule_mB14F07685FC4E158EF7114CC4564C539BD3D162C ();
// 0x00000178 System.Boolean TouchScript.Layers.UI.TouchScriptInputModule::IsModuleSupported()
extern void TouchScriptInputModule_IsModuleSupported_m3C7917468B42EB31DAC4BEAA14ACE3B85913BD6E ();
// 0x00000179 System.Void TouchScript.Layers.UI.TouchScriptInputModule::DeactivateModule()
extern void TouchScriptInputModule_DeactivateModule_m05A709DD7D02D7F1FD9F5523A9BCF58910C13AE4 ();
// 0x0000017A System.Void TouchScript.Layers.UI.TouchScriptInputModule::ActivateModule()
extern void TouchScriptInputModule_ActivateModule_m1E2219AF468BBCCE90A4421848841A2F52238BD9 ();
// 0x0000017B System.Void TouchScript.Layers.UI.TouchScriptInputModule::UpdateModule()
extern void TouchScriptInputModule_UpdateModule_mB135DC7CE5BDFC57CFE0F5CF7960C5DB18201DBD ();
// 0x0000017C System.Void TouchScript.Layers.UI.TouchScriptInputModule::INTERNAL_Retain()
extern void TouchScriptInputModule_INTERNAL_Retain_m3FCF80941B11F539CF37F7BB7021B00DEBF59E60 ();
// 0x0000017D System.Int32 TouchScript.Layers.UI.TouchScriptInputModule::INTERNAL_Release()
extern void TouchScriptInputModule_INTERNAL_Release_mAA3C7480D2AD4E5245F994884694CF7DB7228772 ();
// 0x0000017E System.Void TouchScript.Layers.UI.TouchScriptInputModule::enable()
extern void TouchScriptInputModule_enable_m0396B6BABA3185094B191774CB754CB4EF2F8C1F ();
// 0x0000017F System.Void TouchScript.Layers.UI.TouchScriptInputModule::disable()
extern void TouchScriptInputModule_disable_m76B01999ACFACE8032A3D3451C61D67A8DBB43F8 ();
// 0x00000180 System.Void TouchScript.Layers.UI.TouchScriptInputModule::.cctor()
extern void TouchScriptInputModule__cctor_mEBC46F354326BFC74D470157CE08E91F8D0ABBB1 ();
// 0x00000181 UnityEngine.Vector2 TouchScript.InputSources.ICoordinatesRemapper::Remap(UnityEngine.Vector2)
// 0x00000182 TouchScript.InputSources.ICoordinatesRemapper TouchScript.InputSources.IInputSource::get_CoordinatesRemapper()
// 0x00000183 System.Void TouchScript.InputSources.IInputSource::set_CoordinatesRemapper(TouchScript.InputSources.ICoordinatesRemapper)
// 0x00000184 System.Boolean TouchScript.InputSources.IInputSource::UpdateInput()
// 0x00000185 System.Void TouchScript.InputSources.IInputSource::UpdateResolution()
// 0x00000186 System.Boolean TouchScript.InputSources.IInputSource::CancelPointer(TouchScript.Pointers.Pointer,System.Boolean)
// 0x00000187 System.Void TouchScript.InputSources.INTERNAL_IInputSource::INTERNAL_DiscardPointer(TouchScript.Pointers.Pointer)
// 0x00000188 System.Void TouchScript.InputSources.PointerDelegate::.ctor(System.Object,System.IntPtr)
extern void PointerDelegate__ctor_m3502BA1ED39792501732A5A60B3AA262C6D14091 ();
// 0x00000189 System.Void TouchScript.InputSources.PointerDelegate::Invoke(TouchScript.Pointers.Pointer)
extern void PointerDelegate_Invoke_m0ADEBCF43488019F853BE6F396A814E6EC211039 ();
// 0x0000018A System.IAsyncResult TouchScript.InputSources.PointerDelegate::BeginInvoke(TouchScript.Pointers.Pointer,System.AsyncCallback,System.Object)
extern void PointerDelegate_BeginInvoke_m833BFBD5734AF35CBD8C13832B8181905A0C6A57 ();
// 0x0000018B System.Void TouchScript.InputSources.PointerDelegate::EndInvoke(System.IAsyncResult)
extern void PointerDelegate_EndInvoke_m2A5324F23D69E176109763FBBC1F388A13D740ED ();
// 0x0000018C TouchScript.InputSources.ICoordinatesRemapper TouchScript.InputSources.InputSource::get_CoordinatesRemapper()
extern void InputSource_get_CoordinatesRemapper_mAA23A9CE379397789FDBB3E2B17E6F4C0D8107FC ();
// 0x0000018D System.Void TouchScript.InputSources.InputSource::set_CoordinatesRemapper(TouchScript.InputSources.ICoordinatesRemapper)
extern void InputSource_set_CoordinatesRemapper_m8E914274363528B6448E325E4501EECEBEC2DBED ();
// 0x0000018E System.Boolean TouchScript.InputSources.InputSource::UpdateInput()
extern void InputSource_UpdateInput_mDAB230C1DE8EC83289D7CE490BF1F089DE58F55B ();
// 0x0000018F System.Void TouchScript.InputSources.InputSource::UpdateResolution()
extern void InputSource_UpdateResolution_mDD670279D16306D6E87CCFB989E7BB747F59A5AC ();
// 0x00000190 System.Boolean TouchScript.InputSources.InputSource::CancelPointer(TouchScript.Pointers.Pointer,System.Boolean)
extern void InputSource_CancelPointer_m53ED3921AD3A1F95124B856F6CF70DDAC8CD4532 ();
// 0x00000191 System.Void TouchScript.InputSources.InputSource::INTERNAL_DiscardPointer(TouchScript.Pointers.Pointer)
extern void InputSource_INTERNAL_DiscardPointer_m23C8EF4C18A2175AA9DA621575F0464A3752FAAE ();
// 0x00000192 System.Void TouchScript.InputSources.InputSource::OnEnable()
extern void InputSource_OnEnable_m4B6C0093FEB241D287FB964602FB10E69E918ED8 ();
// 0x00000193 System.Void TouchScript.InputSources.InputSource::OnDisable()
extern void InputSource_OnDisable_mFA43FC2B1744C02CE41A67E9A26FD9AD511441A2 ();
// 0x00000194 System.Void TouchScript.InputSources.InputSource::addPointer(TouchScript.Pointers.Pointer)
extern void InputSource_addPointer_m62F5CEEDE9B66E0C8B414B89D2A6E4C94E5EA62E ();
// 0x00000195 System.Void TouchScript.InputSources.InputSource::updatePointer(TouchScript.Pointers.Pointer)
extern void InputSource_updatePointer_m22B7FB283FC97011FC9A0A4A9FCFD13B170D6BEB ();
// 0x00000196 System.Void TouchScript.InputSources.InputSource::pressPointer(TouchScript.Pointers.Pointer)
extern void InputSource_pressPointer_mAC97DC87D8EE3E127706867B5488C23FC7047390 ();
// 0x00000197 System.Void TouchScript.InputSources.InputSource::releasePointer(TouchScript.Pointers.Pointer)
extern void InputSource_releasePointer_m2C76F24F508F7E5ED533C649C74F475C52AEF275 ();
// 0x00000198 System.Void TouchScript.InputSources.InputSource::removePointer(TouchScript.Pointers.Pointer)
extern void InputSource_removePointer_mA105E39F5363F55607F9D5A99D317F71D079F275 ();
// 0x00000199 System.Void TouchScript.InputSources.InputSource::cancelPointer(TouchScript.Pointers.Pointer)
extern void InputSource_cancelPointer_mCB02D2D11B42AB28B0D972B2E50F8CF9F16186D7 ();
// 0x0000019A System.Void TouchScript.InputSources.InputSource::updateCoordinatesRemapper(TouchScript.InputSources.ICoordinatesRemapper)
extern void InputSource_updateCoordinatesRemapper_m763B13ABA17FB93200643775EBB5CECDF712FA19 ();
// 0x0000019B UnityEngine.Vector2 TouchScript.InputSources.InputSource::remapCoordinates(UnityEngine.Vector2)
extern void InputSource_remapCoordinates_mD99943B29864A81DB9AD7F868A5BC4D3BCCC424C ();
// 0x0000019C System.Void TouchScript.InputSources.InputSource::.ctor()
extern void InputSource__ctor_m10AB4CED3BFBB14FFB677A71F044CE922D70C17E ();
// 0x0000019D TouchScript.InputSources.StandardInput_Windows8APIType TouchScript.InputSources.StandardInput::get_Windows8API()
extern void StandardInput_get_Windows8API_mAFEBEAB1026F308B47C9E4853BD2E2C4E70ABC50 ();
// 0x0000019E TouchScript.InputSources.StandardInput_Windows7APIType TouchScript.InputSources.StandardInput::get_Windows7API()
extern void StandardInput_get_Windows7API_m233E2974BAC1C3B74861E2C38CE233E0EE37C1BA ();
// 0x0000019F System.Boolean TouchScript.InputSources.StandardInput::get_WebGLTouch()
extern void StandardInput_get_WebGLTouch_mC9FF250F9DD94606CB80E831A44678419AAD975D ();
// 0x000001A0 System.Boolean TouchScript.InputSources.StandardInput::get_Windows8Mouse()
extern void StandardInput_get_Windows8Mouse_m029133A48E9E4B1BD6DE207ADECD9B698E50CFCF ();
// 0x000001A1 System.Boolean TouchScript.InputSources.StandardInput::get_Windows7Mouse()
extern void StandardInput_get_Windows7Mouse_m93D3822EF6C9824C58D2EFE03ECA304B50B558AE ();
// 0x000001A2 System.Boolean TouchScript.InputSources.StandardInput::get_UniversalWindowsMouse()
extern void StandardInput_get_UniversalWindowsMouse_m44D4DB4CC21141AD05616725A723D9C9E1B744AD ();
// 0x000001A3 System.Boolean TouchScript.InputSources.StandardInput::get_EmulateSecondMousePointer()
extern void StandardInput_get_EmulateSecondMousePointer_m292B69D30FE63C395451D797FE527FD23D75CA12 ();
// 0x000001A4 System.Void TouchScript.InputSources.StandardInput::set_EmulateSecondMousePointer(System.Boolean)
extern void StandardInput_set_EmulateSecondMousePointer_m89A1BC7F029124D75A679643E534201C78E3BAE2 ();
// 0x000001A5 System.Boolean TouchScript.InputSources.StandardInput::UpdateInput()
extern void StandardInput_UpdateInput_m317AF408D8C388369A18526A3D385C1180CD6E5B ();
// 0x000001A6 System.Void TouchScript.InputSources.StandardInput::UpdateResolution()
extern void StandardInput_UpdateResolution_mAECB2F1C4DEFFD8FC9DD308BA6545CE71FB1D461 ();
// 0x000001A7 System.Boolean TouchScript.InputSources.StandardInput::CancelPointer(TouchScript.Pointers.Pointer,System.Boolean)
extern void StandardInput_CancelPointer_m5352E5DB187E610681EA78E475EF546BACCFCD5B ();
// 0x000001A8 System.Void TouchScript.InputSources.StandardInput::OnEnable()
extern void StandardInput_OnEnable_m9EE713D7441F02540B1ADF11572A2C29ABF678BA ();
// 0x000001A9 System.Void TouchScript.InputSources.StandardInput::OnDisable()
extern void StandardInput_OnDisable_mBF9B175DD91AEDC45A75AEEF55308525349FFBC0 ();
// 0x000001AA System.Void TouchScript.InputSources.StandardInput::switchToBasicEditor()
extern void StandardInput_switchToBasicEditor_m1F74B05D7A423DF04A8DA5E531D2CBEAA43E00A7 ();
// 0x000001AB System.Void TouchScript.InputSources.StandardInput::updateCoordinatesRemapper(TouchScript.InputSources.ICoordinatesRemapper)
extern void StandardInput_updateCoordinatesRemapper_mDF1319DD1D08164DD5C3F40790D132A2E0385E00 ();
// 0x000001AC System.Void TouchScript.InputSources.StandardInput::enableMouse()
extern void StandardInput_enableMouse_m6E303BE93A3DE7A7DF3D40D604D7528281696DAA ();
// 0x000001AD System.Void TouchScript.InputSources.StandardInput::disableMouse()
extern void StandardInput_disableMouse_m47B567E3C02B2EAE4237BF720F2EB8DB4AC4D9D9 ();
// 0x000001AE System.Void TouchScript.InputSources.StandardInput::enableTouch()
extern void StandardInput_enableTouch_mFF487146C665A986C30A49DDBE4B06C1DD4AB88B ();
// 0x000001AF System.Void TouchScript.InputSources.StandardInput::disableTouch()
extern void StandardInput_disableTouch_m2B0AAF063D444218ADF3A7A3BA3F79AECDC0B84C ();
// 0x000001B0 System.Void TouchScript.InputSources.StandardInput::.ctor()
extern void StandardInput__ctor_m721B12256B47845AD09A6DD820594180DB32BA2E ();
// 0x000001B1 TouchScript.InputSources.ICoordinatesRemapper TouchScript.InputSources.InputHandlers.MouseHandler::get_CoordinatesRemapper()
extern void MouseHandler_get_CoordinatesRemapper_mA9A749023AF1989DC3414E70A063DACD8AF9FB57 ();
// 0x000001B2 System.Void TouchScript.InputSources.InputHandlers.MouseHandler::set_CoordinatesRemapper(TouchScript.InputSources.ICoordinatesRemapper)
extern void MouseHandler_set_CoordinatesRemapper_mD89E131D24C9D9ED35FBFCF9206F4930DF42E11E ();
// 0x000001B3 System.Boolean TouchScript.InputSources.InputHandlers.MouseHandler::get_EmulateSecondMousePointer()
extern void MouseHandler_get_EmulateSecondMousePointer_m5A09DEBE36AE0D7F042D2A2E49C3EDDAD073E92E ();
// 0x000001B4 System.Void TouchScript.InputSources.InputHandlers.MouseHandler::set_EmulateSecondMousePointer(System.Boolean)
extern void MouseHandler_set_EmulateSecondMousePointer_mEF7505BE8AD19391D0C96101781E6691092CADA1 ();
// 0x000001B5 System.Void TouchScript.InputSources.InputHandlers.MouseHandler::.ctor(TouchScript.InputSources.PointerDelegate,TouchScript.InputSources.PointerDelegate,TouchScript.InputSources.PointerDelegate,TouchScript.InputSources.PointerDelegate,TouchScript.InputSources.PointerDelegate,TouchScript.InputSources.PointerDelegate)
extern void MouseHandler__ctor_m3FE081371395430265013FC2C5D3E8819EDC4525 ();
// 0x000001B6 System.Void TouchScript.InputSources.InputHandlers.MouseHandler::CancelMousePointer()
extern void MouseHandler_CancelMousePointer_m49E3E6844F1306C76D2DF2F971FC601A5AA43938 ();
// 0x000001B7 System.Boolean TouchScript.InputSources.InputHandlers.MouseHandler::UpdateInput()
extern void MouseHandler_UpdateInput_m7167FA18AE3882EFC6DA9303BA1630C17834E485 ();
// 0x000001B8 System.Void TouchScript.InputSources.InputHandlers.MouseHandler::UpdateResolution()
extern void MouseHandler_UpdateResolution_m7FEDE2B5C1AF34CCADB0CE7503CB41C3FF81390B ();
// 0x000001B9 System.Boolean TouchScript.InputSources.InputHandlers.MouseHandler::CancelPointer(TouchScript.Pointers.Pointer,System.Boolean)
extern void MouseHandler_CancelPointer_mA14AB0680D24B0137AD7B2730D6B2D7366E0B443 ();
// 0x000001BA System.Void TouchScript.InputSources.InputHandlers.MouseHandler::Dispose()
extern void MouseHandler_Dispose_m2B54073CF866C0BE982F039F41CA2BAF76F13ED6 ();
// 0x000001BB System.Void TouchScript.InputSources.InputHandlers.MouseHandler::INTERNAL_DiscardPointer(TouchScript.Pointers.Pointer)
extern void MouseHandler_INTERNAL_DiscardPointer_m84CAE6D62DF3CA4C70D23F613F1084EA10A746EB ();
// 0x000001BC TouchScript.Pointers.Pointer_PointerButtonState TouchScript.InputSources.InputHandlers.MouseHandler::getMouseButtons()
extern void MouseHandler_getMouseButtons_m46AB36FB309193AE4318F4CBE2334AF467F09859 ();
// 0x000001BD System.Void TouchScript.InputSources.InputHandlers.MouseHandler::updateButtons(TouchScript.Pointers.Pointer_PointerButtonState,TouchScript.Pointers.Pointer_PointerButtonState)
extern void MouseHandler_updateButtons_m3398D9DFE9D02F589681C6C435B46E3DE197ED0E ();
// 0x000001BE System.Boolean TouchScript.InputSources.InputHandlers.MouseHandler::fakeTouchReleased()
extern void MouseHandler_fakeTouchReleased_m62D6E481257ADB79BCC19B697A2824E6DA8567EB ();
// 0x000001BF TouchScript.Pointers.MousePointer TouchScript.InputSources.InputHandlers.MouseHandler::internalAddPointer(UnityEngine.Vector2,TouchScript.Pointers.Pointer_PointerButtonState,System.UInt32)
extern void MouseHandler_internalAddPointer_mEA2AFAA7DF2DC27DA88827C8B482E3905B53DA79 ();
// 0x000001C0 System.Void TouchScript.InputSources.InputHandlers.MouseHandler::internalReleaseMousePointer(TouchScript.Pointers.Pointer_PointerButtonState)
extern void MouseHandler_internalReleaseMousePointer_m47BC131A7AE6BC79C516FB0D973ED1459C102C3B ();
// 0x000001C1 TouchScript.Pointers.MousePointer TouchScript.InputSources.InputHandlers.MouseHandler::internalReturnPointer(TouchScript.Pointers.MousePointer)
extern void MouseHandler_internalReturnPointer_m458B4DE568C86106A63EC772E2045723DCA09B48 ();
// 0x000001C2 UnityEngine.Vector2 TouchScript.InputSources.InputHandlers.MouseHandler::remapCoordinates(UnityEngine.Vector2)
extern void MouseHandler_remapCoordinates_m1ABF7ED10957794C6B3E3BE9C86520378830367F ();
// 0x000001C3 System.Void TouchScript.InputSources.InputHandlers.MouseHandler::resetPointer(TouchScript.Pointers.Pointer)
extern void MouseHandler_resetPointer_m0DB4DA2A30B42EAF0A287BD463A51F403A65CE66 ();
// 0x000001C4 System.Void TouchScript.InputSources.InputHandlers.MouseHandler::stateMouse()
extern void MouseHandler_stateMouse_mEA19230557329FE4066A3FC201EEBB80DDF3FE16 ();
// 0x000001C5 System.Void TouchScript.InputSources.InputHandlers.MouseHandler::stateWaitingForFake()
extern void MouseHandler_stateWaitingForFake_m1FF8AE61E9B77390F9C8A832559E83B83EA8B102 ();
// 0x000001C6 System.Void TouchScript.InputSources.InputHandlers.MouseHandler::stateMouseAndFake()
extern void MouseHandler_stateMouseAndFake_m17831B829E59E29E0EA40ED17A16CDC35C9C436C ();
// 0x000001C7 System.Void TouchScript.InputSources.InputHandlers.MouseHandler::stateStationaryFake()
extern void MouseHandler_stateStationaryFake_mA91F5F60DAD980EB67D7B24D840EE1BF6757B4FD ();
// 0x000001C8 System.Void TouchScript.InputSources.InputHandlers.MouseHandler::setState(TouchScript.InputSources.InputHandlers.MouseHandler_State)
extern void MouseHandler_setState_mDD389D573EC073E142D94A13B382D4C5F63171CA ();
// 0x000001C9 TouchScript.Pointers.MousePointer TouchScript.InputSources.InputHandlers.MouseHandler::<.ctor>b__20_0()
extern void MouseHandler_U3C_ctorU3Eb__20_0_mFFC8E33D6492E1C435E05EF724856F6D44EAD689 ();
// 0x000001CA TouchScript.InputSources.ICoordinatesRemapper TouchScript.InputSources.InputHandlers.TouchHandler::get_CoordinatesRemapper()
extern void TouchHandler_get_CoordinatesRemapper_mE1F66B8CEA2952E84CB0585E40E23D8FC39E5957 ();
// 0x000001CB System.Void TouchScript.InputSources.InputHandlers.TouchHandler::set_CoordinatesRemapper(TouchScript.InputSources.ICoordinatesRemapper)
extern void TouchHandler_set_CoordinatesRemapper_mC8243F4495D92E4D5C1C6BCA921B23A3EA07347C ();
// 0x000001CC System.Boolean TouchScript.InputSources.InputHandlers.TouchHandler::get_HasPointers()
extern void TouchHandler_get_HasPointers_m23704054B25FEEB86E3042CFCE583CBE8705B076 ();
// 0x000001CD System.Void TouchScript.InputSources.InputHandlers.TouchHandler::.ctor(TouchScript.InputSources.PointerDelegate,TouchScript.InputSources.PointerDelegate,TouchScript.InputSources.PointerDelegate,TouchScript.InputSources.PointerDelegate,TouchScript.InputSources.PointerDelegate,TouchScript.InputSources.PointerDelegate)
extern void TouchHandler__ctor_m44934BEE553CC11DC6C8DF48D0AC82FD2474AD5C ();
// 0x000001CE System.Boolean TouchScript.InputSources.InputHandlers.TouchHandler::UpdateInput()
extern void TouchHandler_UpdateInput_m7A9D719FDC9C7280DF53B12481E3ACE3C1AB921B ();
// 0x000001CF System.Void TouchScript.InputSources.InputHandlers.TouchHandler::UpdateResolution()
extern void TouchHandler_UpdateResolution_m73E09088A33EE0527CBFDC3B006CFCCD4E1227E0 ();
// 0x000001D0 System.Boolean TouchScript.InputSources.InputHandlers.TouchHandler::CancelPointer(TouchScript.Pointers.Pointer,System.Boolean)
extern void TouchHandler_CancelPointer_m0352D2E599DFAFCC574C2FDCA757DAB4CAF2C7B1 ();
// 0x000001D1 System.Void TouchScript.InputSources.InputHandlers.TouchHandler::Dispose()
extern void TouchHandler_Dispose_m764D6585A1D1EEC17799AF85333AB73A782C5BB1 ();
// 0x000001D2 System.Void TouchScript.InputSources.InputHandlers.TouchHandler::INTERNAL_DiscardPointer(TouchScript.Pointers.Pointer)
extern void TouchHandler_INTERNAL_DiscardPointer_mCCAD128F15D006D87E9C7091B5BCA0671E7F55B7 ();
// 0x000001D3 TouchScript.Pointers.Pointer TouchScript.InputSources.InputHandlers.TouchHandler::internalAddPointer(UnityEngine.Vector2)
extern void TouchHandler_internalAddPointer_m6C8BEE080B5F96D93382D780CAF7D4A228AAF361 ();
// 0x000001D4 TouchScript.Pointers.TouchPointer TouchScript.InputSources.InputHandlers.TouchHandler::internalReturnPointer(TouchScript.Pointers.TouchPointer)
extern void TouchHandler_internalReturnPointer_m1CFA9EAD8E0C8C3F41134DDAEDE020FC3557C53B ();
// 0x000001D5 System.Void TouchScript.InputSources.InputHandlers.TouchHandler::internalRemovePointer(TouchScript.Pointers.Pointer)
extern void TouchHandler_internalRemovePointer_m3C3B29C1CF186668C54CD32EE5D830C9041892AE ();
// 0x000001D6 System.Void TouchScript.InputSources.InputHandlers.TouchHandler::internalCancelPointer(TouchScript.Pointers.Pointer)
extern void TouchHandler_internalCancelPointer_mE1B9DE2B4D31112A2F4704C1195996885BC55273 ();
// 0x000001D7 UnityEngine.Vector2 TouchScript.InputSources.InputHandlers.TouchHandler::remapCoordinates(UnityEngine.Vector2)
extern void TouchHandler_remapCoordinates_m90AF579F604F825834CE570B4EACC95790B67FE3 ();
// 0x000001D8 System.Void TouchScript.InputSources.InputHandlers.TouchHandler::resetPointer(TouchScript.Pointers.Pointer)
extern void TouchHandler_resetPointer_m0113909AAD3568FFDA7EF957DAC9824CB92F91FF ();
// 0x000001D9 TouchScript.Pointers.TouchPointer TouchScript.InputSources.InputHandlers.TouchHandler::<.ctor>b__16_0()
extern void TouchHandler_U3C_ctorU3Eb__16_0_m288B739A7E91AC84C9B96437256382C2105C7C2A ();
// 0x000001DA TouchScript.Hit.HitData_HitType TouchScript.Hit.HitData::get_Type()
extern void HitData_get_Type_m72692ED1AEB9B5DB43169456255E7B81D5BFFE54_AdjustorThunk ();
// 0x000001DB UnityEngine.Transform TouchScript.Hit.HitData::get_Target()
extern void HitData_get_Target_mD8CF226FD6A5401997C2F6F554C2BA26155F7A05_AdjustorThunk ();
// 0x000001DC TouchScript.Layers.TouchLayer TouchScript.Hit.HitData::get_Layer()
extern void HitData_get_Layer_m80AD3383FA69E902C6C6E7B29769771F24EAEA51_AdjustorThunk ();
// 0x000001DD UnityEngine.RaycastHit TouchScript.Hit.HitData::get_RaycastHit()
extern void HitData_get_RaycastHit_m3E212799996C2759827EC25ECEC4861E7A0FF9F2_AdjustorThunk ();
// 0x000001DE UnityEngine.RaycastHit2D TouchScript.Hit.HitData::get_RaycastHit2D()
extern void HitData_get_RaycastHit2D_m1A8ECB492182FF3B9173BFDC61F958D4EF0353F0_AdjustorThunk ();
// 0x000001DF TouchScript.Hit.RaycastHitUI TouchScript.Hit.HitData::get_RaycastHitUI()
extern void HitData_get_RaycastHitUI_m869818385A3B8348A5F5CDCDAA3F798A52BA6AB4_AdjustorThunk ();
// 0x000001E0 System.Boolean TouchScript.Hit.HitData::get_ScreenSpace()
extern void HitData_get_ScreenSpace_m42D38D84E89E1AAF2BFF256515216391314F2673_AdjustorThunk ();
// 0x000001E1 UnityEngine.Vector3 TouchScript.Hit.HitData::get_Point()
extern void HitData_get_Point_mE05135C35317136F666766096410B03CC4B2A3F5_AdjustorThunk ();
// 0x000001E2 UnityEngine.Vector3 TouchScript.Hit.HitData::get_Normal()
extern void HitData_get_Normal_m8D28B6400EE26784998ECF9C9E11ECC898736694_AdjustorThunk ();
// 0x000001E3 System.Single TouchScript.Hit.HitData::get_Distance()
extern void HitData_get_Distance_m3FD9927E15997EBAE1F6BC4874CBA1BDB9DDADA5_AdjustorThunk ();
// 0x000001E4 System.Int32 TouchScript.Hit.HitData::get_SortingLayer()
extern void HitData_get_SortingLayer_mC1E6AD3168188865B0906FBD69328BD6EEB201B1_AdjustorThunk ();
// 0x000001E5 System.Int32 TouchScript.Hit.HitData::get_SortingOrder()
extern void HitData_get_SortingOrder_m8E8A408700F4481599B43098F197D1E56D60D2B6_AdjustorThunk ();
// 0x000001E6 System.Void TouchScript.Hit.HitData::.ctor(UnityEngine.Transform,TouchScript.Layers.TouchLayer,System.Boolean)
extern void HitData__ctor_m5E8312FD3DDD4F5323F744FF247510CE454AF15A_AdjustorThunk ();
// 0x000001E7 System.Void TouchScript.Hit.HitData::.ctor(UnityEngine.RaycastHit,TouchScript.Layers.TouchLayer,System.Boolean)
extern void HitData__ctor_m31CED98457E52ADFD68C498CFDEBA662C99DFCDD_AdjustorThunk ();
// 0x000001E8 System.Void TouchScript.Hit.HitData::.ctor(UnityEngine.RaycastHit2D,TouchScript.Layers.TouchLayer,System.Boolean)
extern void HitData__ctor_mBC74645ACB5450B9CA6DBBF8B551C1AB1635FA73_AdjustorThunk ();
// 0x000001E9 System.Void TouchScript.Hit.HitData::.ctor(TouchScript.Hit.RaycastHitUI,TouchScript.Layers.TouchLayer,System.Boolean)
extern void HitData__ctor_m76EA363D8D703942F812B954CA35AB9E0F9152FE_AdjustorThunk ();
// 0x000001EA System.Void TouchScript.Hit.HitData::updateSortingValues()
extern void HitData_updateSortingValues_m83995CF9B001FA94DA5A87AA350BF40A0687012A_AdjustorThunk ();
// 0x000001EB TouchScript.Hit.HitResult TouchScript.Hit.HitTest::IsHit(TouchScript.Pointers.IPointer,TouchScript.Hit.HitData)
extern void HitTest_IsHit_m24CBD5CE26C34C8C715DBA39C141006EAFF4222E ();
// 0x000001EC System.Void TouchScript.Hit.HitTest::OnEnable()
extern void HitTest_OnEnable_m2870E2D3CCCA2276C415F57B73127BB37752F537 ();
// 0x000001ED System.Void TouchScript.Hit.HitTest::.ctor()
extern void HitTest__ctor_m70957C00CC9BFB35FE18EEFB0D3B560F20C1872F ();
// 0x000001EE TouchScript.Hit.HitResult TouchScript.Hit.Untouchable::IsHit(TouchScript.Pointers.IPointer,TouchScript.Hit.HitData)
extern void Untouchable_IsHit_m4B38CC5DA7FC55389BD8097CE066CAD93ECA923B ();
// 0x000001EF System.Void TouchScript.Hit.Untouchable::.ctor()
extern void Untouchable__ctor_mCE3C4B8158F85CC030FC7B5BB93E169641E8382C ();
// 0x000001F0 System.Void TouchScript.Gestures.FlickGesture::add_Flicked(System.EventHandler`1<System.EventArgs>)
extern void FlickGesture_add_Flicked_m25DC9931033826699D50DA7D349CAC6F17BAA985 ();
// 0x000001F1 System.Void TouchScript.Gestures.FlickGesture::remove_Flicked(System.EventHandler`1<System.EventArgs>)
extern void FlickGesture_remove_Flicked_m4BA5DCCCC6C46D8DF2B4794F0A8772A80B128738 ();
// 0x000001F2 System.Single TouchScript.Gestures.FlickGesture::get_FlickTime()
extern void FlickGesture_get_FlickTime_m215BEE64D528CDB9C8F68917095A7A7BD0B295CC ();
// 0x000001F3 System.Void TouchScript.Gestures.FlickGesture::set_FlickTime(System.Single)
extern void FlickGesture_set_FlickTime_m2BF8570CF57C9BAF4DEB3FD940C719471F1235A9 ();
// 0x000001F4 System.Single TouchScript.Gestures.FlickGesture::get_MinDistance()
extern void FlickGesture_get_MinDistance_m4B1BDE629056EC030B2094FB909A4C7FCE54B389 ();
// 0x000001F5 System.Void TouchScript.Gestures.FlickGesture::set_MinDistance(System.Single)
extern void FlickGesture_set_MinDistance_m768E1EC2A0C12856DF491A757C55A3B5F540FAD1 ();
// 0x000001F6 System.Single TouchScript.Gestures.FlickGesture::get_MovementThreshold()
extern void FlickGesture_get_MovementThreshold_m611FC9C1847F17CCDDD395C7BEB7D1951FC9E8F6 ();
// 0x000001F7 System.Void TouchScript.Gestures.FlickGesture::set_MovementThreshold(System.Single)
extern void FlickGesture_set_MovementThreshold_mEB25245068FAA667698AA456C02E7C38171588A3 ();
// 0x000001F8 TouchScript.Gestures.FlickGesture_GestureDirection TouchScript.Gestures.FlickGesture::get_Direction()
extern void FlickGesture_get_Direction_m5E86A6551551247A416300357BC3AEF1D21EC374 ();
// 0x000001F9 System.Void TouchScript.Gestures.FlickGesture::set_Direction(TouchScript.Gestures.FlickGesture_GestureDirection)
extern void FlickGesture_set_Direction_mC2B4619328F9ED4A770E8834FAFD62565592AF57 ();
// 0x000001FA UnityEngine.Vector2 TouchScript.Gestures.FlickGesture::get_ScreenFlickVector()
extern void FlickGesture_get_ScreenFlickVector_m45978D06D8A07EF930FA4F40598B28D2B522DDED ();
// 0x000001FB System.Void TouchScript.Gestures.FlickGesture::set_ScreenFlickVector(UnityEngine.Vector2)
extern void FlickGesture_set_ScreenFlickVector_mD894B27E22DC8F9C26D9C1FA67AFAD7668155DBD ();
// 0x000001FC System.Single TouchScript.Gestures.FlickGesture::get_ScreenFlickTime()
extern void FlickGesture_get_ScreenFlickTime_m85159730FE776C6BD03CCD88FF9D9AF7201E2A11 ();
// 0x000001FD System.Void TouchScript.Gestures.FlickGesture::set_ScreenFlickTime(System.Single)
extern void FlickGesture_set_ScreenFlickTime_m34E593B24DC48C53214F2774767F746F6EC281E7 ();
// 0x000001FE System.Void TouchScript.Gestures.FlickGesture::Awake()
extern void FlickGesture_Awake_m14707660D6AECC0DB4F3A54A9CA2E7045A627C93 ();
// 0x000001FF System.Void TouchScript.Gestures.FlickGesture::LateUpdate()
extern void FlickGesture_LateUpdate_mF819EDDAD9C47CEBF737E8F7AC6E6D7C079311CF ();
// 0x00000200 System.Void TouchScript.Gestures.FlickGesture::switchToBasicEditor()
extern void FlickGesture_switchToBasicEditor_m39EA45D74BBBBAF6B6B91DEEF795E79806F2E90F ();
// 0x00000201 System.Void TouchScript.Gestures.FlickGesture::pointersPressed(System.Collections.Generic.IList`1<TouchScript.Pointers.Pointer>)
extern void FlickGesture_pointersPressed_m621B8B6A45C9E05E1661594A0D6FC79FA991093D ();
// 0x00000202 System.Void TouchScript.Gestures.FlickGesture::pointersUpdated(System.Collections.Generic.IList`1<TouchScript.Pointers.Pointer>)
extern void FlickGesture_pointersUpdated_mE200896F5F9C5A9D9296F8C6C730BC3D52724C07 ();
// 0x00000203 System.Void TouchScript.Gestures.FlickGesture::pointersReleased(System.Collections.Generic.IList`1<TouchScript.Pointers.Pointer>)
extern void FlickGesture_pointersReleased_m4EA4731BD56EFF6723500B60C38A1C43BADDDB9E ();
// 0x00000204 System.Void TouchScript.Gestures.FlickGesture::onRecognized()
extern void FlickGesture_onRecognized_m9190FC5AF6465C19CC73CF4C2D5616B3AD7C5A03 ();
// 0x00000205 System.Void TouchScript.Gestures.FlickGesture::reset()
extern void FlickGesture_reset_mF6D48937FFC8391F0A9D5A882B5D82E40B29B79E ();
// 0x00000206 System.Void TouchScript.Gestures.FlickGesture::.ctor()
extern void FlickGesture__ctor_mC347B1523AA37AB48B291576A58BBC9EF51C2306 ();
// 0x00000207 System.Void TouchScript.Gestures.Gesture::add_StateChanged(System.EventHandler`1<TouchScript.Gestures.GestureStateChangeEventArgs>)
extern void Gesture_add_StateChanged_m1BCAF2D266C1AD01E7B3BD0B81844144800EDE12 ();
// 0x00000208 System.Void TouchScript.Gestures.Gesture::remove_StateChanged(System.EventHandler`1<TouchScript.Gestures.GestureStateChangeEventArgs>)
extern void Gesture_remove_StateChanged_m112B45165CDF6471EDBFEB4670211ABDEF08AEFE ();
// 0x00000209 System.Void TouchScript.Gestures.Gesture::add_Cancelled(System.EventHandler`1<System.EventArgs>)
extern void Gesture_add_Cancelled_mEFC45E21916CBB7EEA6B30762E82F33900BD7979 ();
// 0x0000020A System.Void TouchScript.Gestures.Gesture::remove_Cancelled(System.EventHandler`1<System.EventArgs>)
extern void Gesture_remove_Cancelled_m18F0A3A2D1D150D662A0832D87448CE33A65C5E8 ();
// 0x0000020B System.Int32 TouchScript.Gestures.Gesture::get_MinPointers()
extern void Gesture_get_MinPointers_mCEC72B25F58BBEB7F3DBCACDD5CFBAB8CE9C627E ();
// 0x0000020C System.Void TouchScript.Gestures.Gesture::set_MinPointers(System.Int32)
extern void Gesture_set_MinPointers_mAAFA65A244E5B47017F26E9B41DE4158A03578E6 ();
// 0x0000020D System.Int32 TouchScript.Gestures.Gesture::get_MaxPointers()
extern void Gesture_get_MaxPointers_m43485ED37836BA5C1F1A3D1792ED9F8117CA1365 ();
// 0x0000020E System.Void TouchScript.Gestures.Gesture::set_MaxPointers(System.Int32)
extern void Gesture_set_MaxPointers_mE1F8C7696DB473AC8298089B44A411B3D0495E0C ();
// 0x0000020F TouchScript.Gestures.Gesture TouchScript.Gestures.Gesture::get_RequireGestureToFail()
extern void Gesture_get_RequireGestureToFail_mDF307189244CA4A9BDF25FFF97BBAED111E2096E ();
// 0x00000210 System.Void TouchScript.Gestures.Gesture::set_RequireGestureToFail(TouchScript.Gestures.Gesture)
extern void Gesture_set_RequireGestureToFail_m9159F9B75D878F4EC9D8851D304FA77C6CB2AA47 ();
// 0x00000211 System.Boolean TouchScript.Gestures.Gesture::get_UseSendMessage()
extern void Gesture_get_UseSendMessage_mFC0CC0A4B4ED478240FB7E74D91403C24B5207D4 ();
// 0x00000212 System.Void TouchScript.Gestures.Gesture::set_UseSendMessage(System.Boolean)
extern void Gesture_set_UseSendMessage_m5AAD7EB2DED01C096D68C56D9AC0BF11A0912CAD ();
// 0x00000213 System.Boolean TouchScript.Gestures.Gesture::get_SendStateChangeMessages()
extern void Gesture_get_SendStateChangeMessages_m1FD7F2330D182C8EE803A54A19D40B729B175F26 ();
// 0x00000214 System.Void TouchScript.Gestures.Gesture::set_SendStateChangeMessages(System.Boolean)
extern void Gesture_set_SendStateChangeMessages_m36A68209D5DE7DFB8B441C37D414C3D866899EAD ();
// 0x00000215 UnityEngine.GameObject TouchScript.Gestures.Gesture::get_SendMessageTarget()
extern void Gesture_get_SendMessageTarget_m7185B42B012F3F599A09561EA43284FC932609E3 ();
// 0x00000216 System.Void TouchScript.Gestures.Gesture::set_SendMessageTarget(UnityEngine.GameObject)
extern void Gesture_set_SendMessageTarget_m44394AC4BEB88D3BCB660E2907BEE6A1DFD73F05 ();
// 0x00000217 System.Boolean TouchScript.Gestures.Gesture::get_UseUnityEvents()
extern void Gesture_get_UseUnityEvents_m4CEDD4AF4C5996901DC37F6A1C9496B65235E0D4 ();
// 0x00000218 System.Void TouchScript.Gestures.Gesture::set_UseUnityEvents(System.Boolean)
extern void Gesture_set_UseUnityEvents_m726AEC4B54A363C072350A32E49DC515D50C0581 ();
// 0x00000219 System.Boolean TouchScript.Gestures.Gesture::get_SendStateChangeEvents()
extern void Gesture_get_SendStateChangeEvents_m8F28A6B70A4A673E35FB7F45EBE44A4C85401255 ();
// 0x0000021A System.Void TouchScript.Gestures.Gesture::set_SendStateChangeEvents(System.Boolean)
extern void Gesture_set_SendStateChangeEvents_m0A109722474680D7AFB0023B084C1C61B923DBF9 ();
// 0x0000021B TouchScript.Gestures.Gesture_GestureState TouchScript.Gestures.Gesture::get_State()
extern void Gesture_get_State_m0D84F23AC52A6740479C86C64E800627C3AE4932 ();
// 0x0000021C System.Void TouchScript.Gestures.Gesture::set_State(TouchScript.Gestures.Gesture_GestureState)
extern void Gesture_set_State_m5FC1A99C59CC058997EC32F1947E05D3E0E3B909 ();
// 0x0000021D TouchScript.Gestures.Gesture_GestureState TouchScript.Gestures.Gesture::get_PreviousState()
extern void Gesture_get_PreviousState_m45BD994576B1B3734E494C2DE797364396149ECF ();
// 0x0000021E System.Void TouchScript.Gestures.Gesture::set_PreviousState(TouchScript.Gestures.Gesture_GestureState)
extern void Gesture_set_PreviousState_m4B719D37BD834F63D486E7B74E9FF750DA9C2AD1 ();
// 0x0000021F UnityEngine.Vector2 TouchScript.Gestures.Gesture::get_ScreenPosition()
extern void Gesture_get_ScreenPosition_m94AFB3991D358396A6A8BFA81E97D570BCCCE385 ();
// 0x00000220 UnityEngine.Vector2 TouchScript.Gestures.Gesture::get_PreviousScreenPosition()
extern void Gesture_get_PreviousScreenPosition_m29915BBAD0AF5F3FDD359C7C98C8347B567102F9 ();
// 0x00000221 UnityEngine.Vector2 TouchScript.Gestures.Gesture::get_NormalizedScreenPosition()
extern void Gesture_get_NormalizedScreenPosition_m01A99749863CBAAEC3C80E9BC2AC173C4B629C29 ();
// 0x00000222 UnityEngine.Vector2 TouchScript.Gestures.Gesture::get_PreviousNormalizedScreenPosition()
extern void Gesture_get_PreviousNormalizedScreenPosition_m8356D2596C45CE831BAD1180B0B9431B993679D8 ();
// 0x00000223 System.Collections.Generic.IList`1<TouchScript.Pointers.Pointer> TouchScript.Gestures.Gesture::get_ActivePointers()
extern void Gesture_get_ActivePointers_mFDB15216DCB336A42687B31F9ED1EB9880F6C09D ();
// 0x00000224 System.Int32 TouchScript.Gestures.Gesture::get_NumPointers()
extern void Gesture_get_NumPointers_m0F38BE67443C4798476B8049F6ACD1F668910B62 ();
// 0x00000225 TouchScript.IGestureDelegate TouchScript.Gestures.Gesture::get_Delegate()
extern void Gesture_get_Delegate_m5FB26ADAA6F5E9E31BC0BC24B72A142837740CAF ();
// 0x00000226 System.Void TouchScript.Gestures.Gesture::set_Delegate(TouchScript.IGestureDelegate)
extern void Gesture_set_Delegate_m7768C92C41F47FFE8C55D2FF9BA71598CB6801AF ();
// 0x00000227 TouchScript.IGestureManager TouchScript.Gestures.Gesture::get_gestureManager()
extern void Gesture_get_gestureManager_mF4E49D625D490BE21BD14CE9F443FA0C50F5E3D2 ();
// 0x00000228 TouchScript.Core.TouchManagerInstance TouchScript.Gestures.Gesture::get_touchManager()
extern void Gesture_get_touchManager_m0F322BA03472D79D25C0CDD90E4A712304A31FE2 ();
// 0x00000229 System.Void TouchScript.Gestures.Gesture::set_touchManager(TouchScript.Core.TouchManagerInstance)
extern void Gesture_set_touchManager_m3E48247E09C6B82F21696B4180C176E3F9AC1429 ();
// 0x0000022A TouchScript.Gestures.Gesture_PointersNumState TouchScript.Gestures.Gesture::get_pointersNumState()
extern void Gesture_get_pointersNumState_mF9306D62AA0E8CA56224D2B268F0A9284FE8F295 ();
// 0x0000022B System.Void TouchScript.Gestures.Gesture::set_pointersNumState(TouchScript.Gestures.Gesture_PointersNumState)
extern void Gesture_set_pointersNumState_m930CD5744B26B49F33C7B296CD3A1544AA042EEC ();
// 0x0000022C System.Void TouchScript.Gestures.Gesture::AddFriendlyGesture(TouchScript.Gestures.Gesture)
extern void Gesture_AddFriendlyGesture_m8D3E9186679834BA76E164EE88F82AB834E4E0A3 ();
// 0x0000022D System.Boolean TouchScript.Gestures.Gesture::IsFriendly(TouchScript.Gestures.Gesture)
extern void Gesture_IsFriendly_mCD9AD811FD29116864F63442611961B84A0524A1 ();
// 0x0000022E System.Boolean TouchScript.Gestures.Gesture::HasPointer(TouchScript.Pointers.Pointer)
extern void Gesture_HasPointer_m273A1D2D7CE4EA36785CA1FC6DA0583BD81E5F7F ();
// 0x0000022F System.Boolean TouchScript.Gestures.Gesture::CanPreventGesture(TouchScript.Gestures.Gesture)
extern void Gesture_CanPreventGesture_m1571AE65E916FBD991FFEC8E5E1E43116E53001E ();
// 0x00000230 System.Boolean TouchScript.Gestures.Gesture::CanBePreventedByGesture(TouchScript.Gestures.Gesture)
extern void Gesture_CanBePreventedByGesture_m0D3028D97030EA7246DFABB0FE617A18790A4983 ();
// 0x00000231 System.Boolean TouchScript.Gestures.Gesture::ShouldReceivePointer(TouchScript.Pointers.Pointer)
extern void Gesture_ShouldReceivePointer_mF853BA9BAF9AAA6B1099E8379C6DC3206FC155EF ();
// 0x00000232 System.Boolean TouchScript.Gestures.Gesture::ShouldBegin()
extern void Gesture_ShouldBegin_m4F1FCF546A904D434A9A47C44FB007A872516F31 ();
// 0x00000233 System.Void TouchScript.Gestures.Gesture::Cancel(System.Boolean,System.Boolean)
extern void Gesture_Cancel_mADAD69AD0365AD677BCE8E71BEA4AF3EAA9BF7A3 ();
// 0x00000234 System.Void TouchScript.Gestures.Gesture::Cancel()
extern void Gesture_Cancel_m1216C968046C72D027F0FCC69F6D998E922FEE9B ();
// 0x00000235 TouchScript.Hit.HitData TouchScript.Gestures.Gesture::GetScreenPositionHitData()
extern void Gesture_GetScreenPositionHitData_mF922F6A0FCB47F4B27A875D027F15B7DC7C65008 ();
// 0x00000236 System.Void TouchScript.Gestures.Gesture::Awake()
extern void Gesture_Awake_m7B0102253D74F10003FAC561B60780BDC3EE73DD ();
// 0x00000237 System.Void TouchScript.Gestures.Gesture::OnEnable()
extern void Gesture_OnEnable_mB9F38A24E250F505C806E28D91CE9A46F9519BA9 ();
// 0x00000238 System.Void TouchScript.Gestures.Gesture::OnDisable()
extern void Gesture_OnDisable_mB44F68A8CC3FBD3FD137ACE267C87D484B950AA4 ();
// 0x00000239 System.Void TouchScript.Gestures.Gesture::OnDestroy()
extern void Gesture_OnDestroy_mEC27FB48B392517CF2B9E0D8FC875B702A0A25BA ();
// 0x0000023A System.Void TouchScript.Gestures.Gesture::INTERNAL_SetState(TouchScript.Gestures.Gesture_GestureState)
extern void Gesture_INTERNAL_SetState_mBCA7FBC10C327E4785C6734A698C3CBD7DCEAB4E ();
// 0x0000023B System.Void TouchScript.Gestures.Gesture::INTERNAL_Reset()
extern void Gesture_INTERNAL_Reset_m05AABC9DD5A39DF1A83168607C8F3DF58DE1DD6B ();
// 0x0000023C System.Void TouchScript.Gestures.Gesture::INTERNAL_PointersPressed(System.Collections.Generic.IList`1<TouchScript.Pointers.Pointer>)
extern void Gesture_INTERNAL_PointersPressed_mB5E7041D74FA932E4F88B349F6EDE25D907ED897 ();
// 0x0000023D System.Void TouchScript.Gestures.Gesture::INTERNAL_PointersUpdated(System.Collections.Generic.IList`1<TouchScript.Pointers.Pointer>)
extern void Gesture_INTERNAL_PointersUpdated_mB5756EAF7CE48E8FAD19F236A78552EBE827FBDF ();
// 0x0000023E System.Void TouchScript.Gestures.Gesture::INTERNAL_PointersReleased(System.Collections.Generic.IList`1<TouchScript.Pointers.Pointer>)
extern void Gesture_INTERNAL_PointersReleased_m74440EA14EE70040408453E951D35302831F1774 ();
// 0x0000023F System.Void TouchScript.Gestures.Gesture::INTERNAL_PointersCancelled(System.Collections.Generic.IList`1<TouchScript.Pointers.Pointer>)
extern void Gesture_INTERNAL_PointersCancelled_mA8A367344AE15EE109E4DFE41E6048C148727ECC ();
// 0x00000240 System.Void TouchScript.Gestures.Gesture::INTERNAL_RemoveFriendlyGesture(TouchScript.Gestures.Gesture)
extern void Gesture_INTERNAL_RemoveFriendlyGesture_mE54CC45493BEE6D99F565960061A951A4754933B ();
// 0x00000241 System.Boolean TouchScript.Gestures.Gesture::shouldCachePointerPosition(TouchScript.Pointers.Pointer)
extern void Gesture_shouldCachePointerPosition_m11D2EEC7D30F8BB59FD9667CC1AFC4C9637E1CB3 ();
// 0x00000242 System.Boolean TouchScript.Gestures.Gesture::setState(TouchScript.Gestures.Gesture_GestureState)
extern void Gesture_setState_m65E096FD83407A65F2C1E9A338D269C7E3975631 ();
// 0x00000243 System.Void TouchScript.Gestures.Gesture::pointersPressed(System.Collections.Generic.IList`1<TouchScript.Pointers.Pointer>)
extern void Gesture_pointersPressed_mE351C2D0EF6FF70D2BDEA0B23F3DD8878809AA41 ();
// 0x00000244 System.Void TouchScript.Gestures.Gesture::pointersUpdated(System.Collections.Generic.IList`1<TouchScript.Pointers.Pointer>)
extern void Gesture_pointersUpdated_m5002285C2C3500505C9FA6CDEDD0098AEC5441CC ();
// 0x00000245 System.Void TouchScript.Gestures.Gesture::pointersReleased(System.Collections.Generic.IList`1<TouchScript.Pointers.Pointer>)
extern void Gesture_pointersReleased_mF20DF89E4F5B96754807D1964F69FC3CECFEC2C7 ();
// 0x00000246 System.Void TouchScript.Gestures.Gesture::pointersCancelled(System.Collections.Generic.IList`1<TouchScript.Pointers.Pointer>)
extern void Gesture_pointersCancelled_m8420CFF19089E06B9C6953A4E3285B7537A97E7B ();
// 0x00000247 System.Void TouchScript.Gestures.Gesture::reset()
extern void Gesture_reset_m1877C9E7D1C1EE6850A00785F6FB9012196C362E ();
// 0x00000248 System.Void TouchScript.Gestures.Gesture::onIdle()
extern void Gesture_onIdle_mD95A7535FC8D1784F6627785680597B19D546788 ();
// 0x00000249 System.Void TouchScript.Gestures.Gesture::onPossible()
extern void Gesture_onPossible_mB2F2C913DDD161D4391A920A235819DB575ABEEE ();
// 0x0000024A System.Void TouchScript.Gestures.Gesture::onBegan()
extern void Gesture_onBegan_m3A7F927ED4FC4BC1231B9EB73744242A50D3E588 ();
// 0x0000024B System.Void TouchScript.Gestures.Gesture::onChanged()
extern void Gesture_onChanged_mFCDA69921ED330AF1E607273442DB7A725306568 ();
// 0x0000024C System.Void TouchScript.Gestures.Gesture::onRecognized()
extern void Gesture_onRecognized_m2EC7E369EC4E9DC87DCDBA2BEC50C2B67023204D ();
// 0x0000024D System.Void TouchScript.Gestures.Gesture::onFailed()
extern void Gesture_onFailed_m1F03D1EA870EE52C4DE171EBABDAE7A2D203CE2C ();
// 0x0000024E System.Void TouchScript.Gestures.Gesture::onCancelled()
extern void Gesture_onCancelled_m83D6C0B7DD77A449F3B5DBC8B65F40FD6A982385 ();
// 0x0000024F System.Void TouchScript.Gestures.Gesture::retainPointers()
extern void Gesture_retainPointers_m5C712BA2471ECB9BB5808ED4CDA989AA6088F0B1 ();
// 0x00000250 System.Void TouchScript.Gestures.Gesture::releasePointers(System.Boolean)
extern void Gesture_releasePointers_m16E842498B3C77FD4CD7CC1AAA3BAF6C61DB36C2 ();
// 0x00000251 System.Void TouchScript.Gestures.Gesture::registerFriendlyGesture(TouchScript.Gestures.Gesture)
extern void Gesture_registerFriendlyGesture_m5402E6D46A79ABE77536ECFD27868EC6745DD4DD ();
// 0x00000252 System.Void TouchScript.Gestures.Gesture::unregisterFriendlyGesture(TouchScript.Gestures.Gesture)
extern void Gesture_unregisterFriendlyGesture_m0FF702A86CE10153C4E95A50E344559F3AE3F3D5 ();
// 0x00000253 System.Void TouchScript.Gestures.Gesture::requiredToFailGestureStateChangedHandler(System.Object,TouchScript.Gestures.GestureStateChangeEventArgs)
extern void Gesture_requiredToFailGestureStateChangedHandler_mB15CB1A9E6127FCCD9A4AF492C47811909DB55E5 ();
// 0x00000254 System.Void TouchScript.Gestures.Gesture::.ctor()
extern void Gesture__ctor_m3193815F0F71D3DD154CAAF8ED6BD6CE7DF1D3AA ();
// 0x00000255 TouchScript.Gestures.Gesture_GestureState TouchScript.Gestures.GestureStateChangeEventArgs::get_PreviousState()
extern void GestureStateChangeEventArgs_get_PreviousState_mC8F6C4E5F98A62F6AEC8534F41A2744A5FBCECD1 ();
// 0x00000256 System.Void TouchScript.Gestures.GestureStateChangeEventArgs::set_PreviousState(TouchScript.Gestures.Gesture_GestureState)
extern void GestureStateChangeEventArgs_set_PreviousState_mA70EEDA3FA1A2464A96BA2D5C594467E4D2E57DC ();
// 0x00000257 TouchScript.Gestures.Gesture_GestureState TouchScript.Gestures.GestureStateChangeEventArgs::get_State()
extern void GestureStateChangeEventArgs_get_State_mA634588E846F16B0C631A9CF3EF67C7CD8C4E47C ();
// 0x00000258 System.Void TouchScript.Gestures.GestureStateChangeEventArgs::set_State(TouchScript.Gestures.Gesture_GestureState)
extern void GestureStateChangeEventArgs_set_State_mFADFCF11FEAEBD70A279C3D56099E10F43170322 ();
// 0x00000259 System.Void TouchScript.Gestures.GestureStateChangeEventArgs::.ctor()
extern void GestureStateChangeEventArgs__ctor_m00A1F3CB76D570409E14E5A9923FBA738D15C902 ();
// 0x0000025A TouchScript.Gestures.GestureStateChangeEventArgs TouchScript.Gestures.GestureStateChangeEventArgs::GetCachedEventArgs(TouchScript.Gestures.Gesture_GestureState,TouchScript.Gestures.Gesture_GestureState)
extern void GestureStateChangeEventArgs_GetCachedEventArgs_mC9EA1DCA6C5F6E357EAA24384EB26065CF40015C ();
// 0x0000025B System.Void TouchScript.Gestures.LongPressGesture::add_LongPressed(System.EventHandler`1<System.EventArgs>)
extern void LongPressGesture_add_LongPressed_m4B45E6ED6B7973F1C1FDDB7F09D31187EBF5F10F ();
// 0x0000025C System.Void TouchScript.Gestures.LongPressGesture::remove_LongPressed(System.EventHandler`1<System.EventArgs>)
extern void LongPressGesture_remove_LongPressed_mDAB3DD929F7073C371213304D189437528C8DFD0 ();
// 0x0000025D System.Single TouchScript.Gestures.LongPressGesture::get_TimeToPress()
extern void LongPressGesture_get_TimeToPress_mF8C5BB1CBC2ED7043D66C4DD0FCCD7BAF7394778 ();
// 0x0000025E System.Void TouchScript.Gestures.LongPressGesture::set_TimeToPress(System.Single)
extern void LongPressGesture_set_TimeToPress_m18DA7B12F4583FAD79397BA8BCF21A375D0D8892 ();
// 0x0000025F System.Single TouchScript.Gestures.LongPressGesture::get_DistanceLimit()
extern void LongPressGesture_get_DistanceLimit_m0F395C22DF93ED07EF4265D651797B0911CC2032 ();
// 0x00000260 System.Void TouchScript.Gestures.LongPressGesture::set_DistanceLimit(System.Single)
extern void LongPressGesture_set_DistanceLimit_m9DA9721518E6DD5F0FE955049C1A1BCCEF397F39 ();
// 0x00000261 System.Void TouchScript.Gestures.LongPressGesture::Awake()
extern void LongPressGesture_Awake_m4F2A7E0B63A4E5B81A12F71202D9A261C83E755B ();
// 0x00000262 System.Void TouchScript.Gestures.LongPressGesture::OnEnable()
extern void LongPressGesture_OnEnable_mD365C46D4CA7F6BD84CA4DBC0A53A901907C15E6 ();
// 0x00000263 System.Void TouchScript.Gestures.LongPressGesture::switchToBasicEditor()
extern void LongPressGesture_switchToBasicEditor_m371D46E470650C066CC25CB988BC1EC740133CAC ();
// 0x00000264 System.Void TouchScript.Gestures.LongPressGesture::pointersPressed(System.Collections.Generic.IList`1<TouchScript.Pointers.Pointer>)
extern void LongPressGesture_pointersPressed_m361AF3751035D72FA517B6E83DB9DF7ACA08231D ();
// 0x00000265 System.Void TouchScript.Gestures.LongPressGesture::pointersUpdated(System.Collections.Generic.IList`1<TouchScript.Pointers.Pointer>)
extern void LongPressGesture_pointersUpdated_mC4B1C839368730B4E031F37E523B8729C7D05B96 ();
// 0x00000266 System.Void TouchScript.Gestures.LongPressGesture::pointersReleased(System.Collections.Generic.IList`1<TouchScript.Pointers.Pointer>)
extern void LongPressGesture_pointersReleased_mAAB8D542E75EE92E5081BC0CAEFADE4CB6BBA659 ();
// 0x00000267 System.Void TouchScript.Gestures.LongPressGesture::onRecognized()
extern void LongPressGesture_onRecognized_m18F8F27661C36A970657B6A2E9A2EE20B5D24107 ();
// 0x00000268 System.Void TouchScript.Gestures.LongPressGesture::reset()
extern void LongPressGesture_reset_m124D31C79F9E7FE0D7A044B61140AD8227F9EA4E ();
// 0x00000269 System.Collections.IEnumerator TouchScript.Gestures.LongPressGesture::wait()
extern void LongPressGesture_wait_m035EA290BA50196DD1522C755646A00EB7E3F9E2 ();
// 0x0000026A System.Void TouchScript.Gestures.LongPressGesture::.ctor()
extern void LongPressGesture__ctor_m6CD790DCCBA99DF053E13CD9DF594CE9566C2A11 ();
// 0x0000026B System.Void TouchScript.Gestures.MetaGesture::add_PointerPressed(System.EventHandler`1<TouchScript.Gestures.MetaGestureEventArgs>)
extern void MetaGesture_add_PointerPressed_mCA3510DA9CFEA858C7D2208309D45D45BA6E54CA ();
// 0x0000026C System.Void TouchScript.Gestures.MetaGesture::remove_PointerPressed(System.EventHandler`1<TouchScript.Gestures.MetaGestureEventArgs>)
extern void MetaGesture_remove_PointerPressed_mD979507A2AE07F5FED1F8ED4660941E8BF3D3F1A ();
// 0x0000026D System.Void TouchScript.Gestures.MetaGesture::add_PointerUpdated(System.EventHandler`1<TouchScript.Gestures.MetaGestureEventArgs>)
extern void MetaGesture_add_PointerUpdated_m16EB208373340961AC7ECD7F5D3E56A6C007B19D ();
// 0x0000026E System.Void TouchScript.Gestures.MetaGesture::remove_PointerUpdated(System.EventHandler`1<TouchScript.Gestures.MetaGestureEventArgs>)
extern void MetaGesture_remove_PointerUpdated_m5780B8BA04B9140B2AB0E45F9C8C698CED706DDF ();
// 0x0000026F System.Void TouchScript.Gestures.MetaGesture::add_PointerReleased(System.EventHandler`1<TouchScript.Gestures.MetaGestureEventArgs>)
extern void MetaGesture_add_PointerReleased_m349D749D6D9B1589B23B99C2AFE5DBF493BF4285 ();
// 0x00000270 System.Void TouchScript.Gestures.MetaGesture::remove_PointerReleased(System.EventHandler`1<TouchScript.Gestures.MetaGestureEventArgs>)
extern void MetaGesture_remove_PointerReleased_m5CF40F31C74C22C1B60724D95845D5D05E1AC628 ();
// 0x00000271 System.Void TouchScript.Gestures.MetaGesture::add_PointerCancelled(System.EventHandler`1<TouchScript.Gestures.MetaGestureEventArgs>)
extern void MetaGesture_add_PointerCancelled_mDBD446F95A464C9600389BFA04060ECE486DFFB0 ();
// 0x00000272 System.Void TouchScript.Gestures.MetaGesture::remove_PointerCancelled(System.EventHandler`1<TouchScript.Gestures.MetaGestureEventArgs>)
extern void MetaGesture_remove_PointerCancelled_m72C785E59A802C9A510B7301774F5F42C73076C2 ();
// 0x00000273 System.Void TouchScript.Gestures.MetaGesture::Awake()
extern void MetaGesture_Awake_m260A11DE49B1ACCE1EFAA92FCB69DF6C87D8E48E ();
// 0x00000274 System.Void TouchScript.Gestures.MetaGesture::switchToBasicEditor()
extern void MetaGesture_switchToBasicEditor_mF32681D33EC3AA453F195DAAE300D4C5F220DB8D ();
// 0x00000275 System.Void TouchScript.Gestures.MetaGesture::pointersPressed(System.Collections.Generic.IList`1<TouchScript.Pointers.Pointer>)
extern void MetaGesture_pointersPressed_m174571D9EDA6C2DC1BB37DE18E635E9C8DA37216 ();
// 0x00000276 System.Void TouchScript.Gestures.MetaGesture::pointersUpdated(System.Collections.Generic.IList`1<TouchScript.Pointers.Pointer>)
extern void MetaGesture_pointersUpdated_mBA996B7F2B35384BC2E97004C363637461127C82 ();
// 0x00000277 System.Void TouchScript.Gestures.MetaGesture::pointersReleased(System.Collections.Generic.IList`1<TouchScript.Pointers.Pointer>)
extern void MetaGesture_pointersReleased_mE0A9391573C45D4BD3E64D72815C957A92D784D8 ();
// 0x00000278 System.Void TouchScript.Gestures.MetaGesture::pointersCancelled(System.Collections.Generic.IList`1<TouchScript.Pointers.Pointer>)
extern void MetaGesture_pointersCancelled_m75699B19966A5B671DBCD64644088942B60967D4 ();
// 0x00000279 System.Void TouchScript.Gestures.MetaGesture::.ctor()
extern void MetaGesture__ctor_m5AFE49DAD3936F3A185B2FB374252216787758A5 ();
// 0x0000027A TouchScript.Pointers.Pointer TouchScript.Gestures.MetaGestureEventArgs::get_Pointer()
extern void MetaGestureEventArgs_get_Pointer_m5FC69D23F4C4D7E1B0BB6261319A31AEFF003E99 ();
// 0x0000027B System.Void TouchScript.Gestures.MetaGestureEventArgs::set_Pointer(TouchScript.Pointers.Pointer)
extern void MetaGestureEventArgs_set_Pointer_m044D11B147DE05937085A1700F85BDBD994B77E5 ();
// 0x0000027C System.Void TouchScript.Gestures.MetaGestureEventArgs::.ctor(TouchScript.Pointers.Pointer)
extern void MetaGestureEventArgs__ctor_mBCFC9E0CD820143A730F9093AB2A7C315A2CDF31 ();
// 0x0000027D System.Void TouchScript.Gestures.PressGesture::add_Pressed(System.EventHandler`1<System.EventArgs>)
extern void PressGesture_add_Pressed_mE8A33C4F8D0B7A3BD09B120392BEAEB30F6671D5 ();
// 0x0000027E System.Void TouchScript.Gestures.PressGesture::remove_Pressed(System.EventHandler`1<System.EventArgs>)
extern void PressGesture_remove_Pressed_m17AC24C997D01943360F20CD82F8B25C2FFB8AF1 ();
// 0x0000027F System.Boolean TouchScript.Gestures.PressGesture::get_IgnoreChildren()
extern void PressGesture_get_IgnoreChildren_m4E570945B8FB179E91375B41C5815E98E061F0DF ();
// 0x00000280 System.Void TouchScript.Gestures.PressGesture::set_IgnoreChildren(System.Boolean)
extern void PressGesture_set_IgnoreChildren_mA13F046D944484E60955D73D77059AE54F2A0D42 ();
// 0x00000281 System.Void TouchScript.Gestures.PressGesture::Awake()
extern void PressGesture_Awake_m7CA2183ED9CE9BA43A960A096371D53BCEDACA8C ();
// 0x00000282 System.Void TouchScript.Gestures.PressGesture::switchToBasicEditor()
extern void PressGesture_switchToBasicEditor_m352B5C7CC6D91651CFE06C58314CDAFFD8B8C927 ();
// 0x00000283 System.Boolean TouchScript.Gestures.PressGesture::ShouldReceivePointer(TouchScript.Pointers.Pointer)
extern void PressGesture_ShouldReceivePointer_m8E0B1A9C4D2EC3E00013752077D5D4B8BE17AF0A ();
// 0x00000284 System.Boolean TouchScript.Gestures.PressGesture::CanPreventGesture(TouchScript.Gestures.Gesture)
extern void PressGesture_CanPreventGesture_m1B8726CD6EDB3632FE1513CDB3F5B169A39E2BB9 ();
// 0x00000285 System.Boolean TouchScript.Gestures.PressGesture::CanBePreventedByGesture(TouchScript.Gestures.Gesture)
extern void PressGesture_CanBePreventedByGesture_m1FE96F8DA722247E9FE37922AD7AC65C5A1C3328 ();
// 0x00000286 System.Void TouchScript.Gestures.PressGesture::pointersPressed(System.Collections.Generic.IList`1<TouchScript.Pointers.Pointer>)
extern void PressGesture_pointersPressed_m5DA6103F48DEDBA1D3D8E42356580432F21CDA76 ();
// 0x00000287 System.Void TouchScript.Gestures.PressGesture::onRecognized()
extern void PressGesture_onRecognized_m3284AE0DC5163EED04198B24285C3DB99464BF1F ();
// 0x00000288 System.Void TouchScript.Gestures.PressGesture::.ctor()
extern void PressGesture__ctor_mFA59BDC8DA4332F2002B5021E8B8A4199932CA4C ();
// 0x00000289 System.Void TouchScript.Gestures.ReleaseGesture::add_Released(System.EventHandler`1<System.EventArgs>)
extern void ReleaseGesture_add_Released_m9FAF03CFBD0EF46DD75A2F6A0323CD90492CB6EF ();
// 0x0000028A System.Void TouchScript.Gestures.ReleaseGesture::remove_Released(System.EventHandler`1<System.EventArgs>)
extern void ReleaseGesture_remove_Released_mA9A9644038AF96D6569C933E85682051499FEF3C ();
// 0x0000028B System.Boolean TouchScript.Gestures.ReleaseGesture::get_IgnoreChildren()
extern void ReleaseGesture_get_IgnoreChildren_m2EEBF77AE7AD9D837F48B636C22035A5166C7CCA ();
// 0x0000028C System.Void TouchScript.Gestures.ReleaseGesture::set_IgnoreChildren(System.Boolean)
extern void ReleaseGesture_set_IgnoreChildren_m0B3851DA48E59F4E3F52F5111A86E92B77142CF9 ();
// 0x0000028D System.Void TouchScript.Gestures.ReleaseGesture::Awake()
extern void ReleaseGesture_Awake_m4A5F22806710E26562EAB68B46A2AD80B5F3AC21 ();
// 0x0000028E System.Void TouchScript.Gestures.ReleaseGesture::switchToBasicEditor()
extern void ReleaseGesture_switchToBasicEditor_m8C7FFFE3FABB907252FD858AC4193477E7D23268 ();
// 0x0000028F System.Boolean TouchScript.Gestures.ReleaseGesture::ShouldReceivePointer(TouchScript.Pointers.Pointer)
extern void ReleaseGesture_ShouldReceivePointer_m924D5C58C2ABDED44E1CFAD99F6B19D405F386C2 ();
// 0x00000290 System.Boolean TouchScript.Gestures.ReleaseGesture::CanPreventGesture(TouchScript.Gestures.Gesture)
extern void ReleaseGesture_CanPreventGesture_m597D9B130C72D69BE5E229393AF63F884CDF21ED ();
// 0x00000291 System.Boolean TouchScript.Gestures.ReleaseGesture::CanBePreventedByGesture(TouchScript.Gestures.Gesture)
extern void ReleaseGesture_CanBePreventedByGesture_m40F83EE1FFB6716B672E42F4E0DA49A79C22A7D8 ();
// 0x00000292 System.Void TouchScript.Gestures.ReleaseGesture::pointersPressed(System.Collections.Generic.IList`1<TouchScript.Pointers.Pointer>)
extern void ReleaseGesture_pointersPressed_m917F2E470ECDF34AFAE73DD4322155ECB3B15A95 ();
// 0x00000293 System.Void TouchScript.Gestures.ReleaseGesture::pointersReleased(System.Collections.Generic.IList`1<TouchScript.Pointers.Pointer>)
extern void ReleaseGesture_pointersReleased_m9D4810D08CEECEB49D3C859EF7697B26D6B2E500 ();
// 0x00000294 System.Void TouchScript.Gestures.ReleaseGesture::onRecognized()
extern void ReleaseGesture_onRecognized_mD65FAFB5B3F6615FA08647FFF1A9A7374B0AD096 ();
// 0x00000295 System.Void TouchScript.Gestures.ReleaseGesture::.ctor()
extern void ReleaseGesture__ctor_m54E19C1EE9C1AA15371FC64AB79D86B0F767DE88 ();
// 0x00000296 System.Void TouchScript.Gestures.TapGesture::add_Tapped(System.EventHandler`1<System.EventArgs>)
extern void TapGesture_add_Tapped_mF661A59B8DC9305017A63DB2B36924CC60EF2084 ();
// 0x00000297 System.Void TouchScript.Gestures.TapGesture::remove_Tapped(System.EventHandler`1<System.EventArgs>)
extern void TapGesture_remove_Tapped_mC85E95CD41880860BA7CEE1E61D50F6F6B7AD6D6 ();
// 0x00000298 System.Int32 TouchScript.Gestures.TapGesture::get_NumberOfTapsRequired()
extern void TapGesture_get_NumberOfTapsRequired_m3A7DEE0DE012DA405CEFAC5A411B3816E7A28AD3 ();
// 0x00000299 System.Void TouchScript.Gestures.TapGesture::set_NumberOfTapsRequired(System.Int32)
extern void TapGesture_set_NumberOfTapsRequired_m43C3FF739D39C91F2E88477423ED1BF60C2F1444 ();
// 0x0000029A System.Single TouchScript.Gestures.TapGesture::get_TimeLimit()
extern void TapGesture_get_TimeLimit_m2922A5F19DD5AE3EBDB67EC0499EC2E385A26497 ();
// 0x0000029B System.Void TouchScript.Gestures.TapGesture::set_TimeLimit(System.Single)
extern void TapGesture_set_TimeLimit_m70FB17044FD91D34BA2D0A51A9AE10762FB24836 ();
// 0x0000029C System.Single TouchScript.Gestures.TapGesture::get_DistanceLimit()
extern void TapGesture_get_DistanceLimit_m5B29383686D9B40390AC83F44DC97677929D5059 ();
// 0x0000029D System.Void TouchScript.Gestures.TapGesture::set_DistanceLimit(System.Single)
extern void TapGesture_set_DistanceLimit_m08D4C4F6407B855F1DD21158AF1C69C46D23D036 ();
// 0x0000029E System.Boolean TouchScript.Gestures.TapGesture::get_CombinePointers()
extern void TapGesture_get_CombinePointers_mEFBCCA33FA7E9B50E4CEB85EB7A4AD2917B73F87 ();
// 0x0000029F System.Void TouchScript.Gestures.TapGesture::set_CombinePointers(System.Boolean)
extern void TapGesture_set_CombinePointers_mDCF54CFDE2AB4E71951544030EE5259CF4078325 ();
// 0x000002A0 System.Single TouchScript.Gestures.TapGesture::get_CombinePointersInterval()
extern void TapGesture_get_CombinePointersInterval_m1E1C8C6686EAAE2DE8D4EBF07B4C1D3AECEE6DC8 ();
// 0x000002A1 System.Void TouchScript.Gestures.TapGesture::set_CombinePointersInterval(System.Single)
extern void TapGesture_set_CombinePointersInterval_m63212174909D07D0D098FC81EA8C26B22DFDA968 ();
// 0x000002A2 System.Boolean TouchScript.Gestures.TapGesture::ShouldReceivePointer(TouchScript.Pointers.Pointer)
extern void TapGesture_ShouldReceivePointer_mAD70FAEAED4B2DB9A08D36B6E01E39DD9D588AA1 ();
// 0x000002A3 System.Void TouchScript.Gestures.TapGesture::Awake()
extern void TapGesture_Awake_m3ADEB3D00F2DC16F2B4683D37164E0B80B827478 ();
// 0x000002A4 System.Void TouchScript.Gestures.TapGesture::OnEnable()
extern void TapGesture_OnEnable_mD38A2473AB9E02106737C7370C97D5F30FB61E47 ();
// 0x000002A5 System.Void TouchScript.Gestures.TapGesture::switchToBasicEditor()
extern void TapGesture_switchToBasicEditor_m2E5A3D4E13198C90C4423FD33959047B11FD4D14 ();
// 0x000002A6 System.Void TouchScript.Gestures.TapGesture::pointersPressed(System.Collections.Generic.IList`1<TouchScript.Pointers.Pointer>)
extern void TapGesture_pointersPressed_m856CEC40B06BC33E6C4B285AE87DFC1A9E38867D ();
// 0x000002A7 System.Void TouchScript.Gestures.TapGesture::pointersUpdated(System.Collections.Generic.IList`1<TouchScript.Pointers.Pointer>)
extern void TapGesture_pointersUpdated_mF45F4F8BD348A3335679650323924311F82CE46C ();
// 0x000002A8 System.Void TouchScript.Gestures.TapGesture::pointersReleased(System.Collections.Generic.IList`1<TouchScript.Pointers.Pointer>)
extern void TapGesture_pointersReleased_m376DA436237A94B1E7AA0DCF9D7120D72482DBEC ();
// 0x000002A9 System.Void TouchScript.Gestures.TapGesture::onRecognized()
extern void TapGesture_onRecognized_m8CCF554F27443CBA96EB1059258270851C826796 ();
// 0x000002AA System.Void TouchScript.Gestures.TapGesture::reset()
extern void TapGesture_reset_mD066D1B207DA42D38A18C6B4F09A98A2D21DD98E ();
// 0x000002AB System.Boolean TouchScript.Gestures.TapGesture::shouldCachePointerPosition(TouchScript.Pointers.Pointer)
extern void TapGesture_shouldCachePointerPosition_m1646205532C69142A5893AB67D58F4A62EC7B2B0 ();
// 0x000002AC System.Collections.IEnumerator TouchScript.Gestures.TapGesture::wait()
extern void TapGesture_wait_m0D00DEA2E4E2D13109939092381264E87669E75C ();
// 0x000002AD System.Void TouchScript.Gestures.TapGesture::.ctor()
extern void TapGesture__ctor_mEA50242D0359C196E1F42112C614019CF85E24F3 ();
// 0x000002AE System.Void TouchScript.Gestures.TransformGestures.ITransformGesture::add_TransformStarted(System.EventHandler`1<System.EventArgs>)
// 0x000002AF System.Void TouchScript.Gestures.TransformGestures.ITransformGesture::remove_TransformStarted(System.EventHandler`1<System.EventArgs>)
// 0x000002B0 System.Void TouchScript.Gestures.TransformGestures.ITransformGesture::add_Transformed(System.EventHandler`1<System.EventArgs>)
// 0x000002B1 System.Void TouchScript.Gestures.TransformGestures.ITransformGesture::remove_Transformed(System.EventHandler`1<System.EventArgs>)
// 0x000002B2 System.Void TouchScript.Gestures.TransformGestures.ITransformGesture::add_TransformCompleted(System.EventHandler`1<System.EventArgs>)
// 0x000002B3 System.Void TouchScript.Gestures.TransformGestures.ITransformGesture::remove_TransformCompleted(System.EventHandler`1<System.EventArgs>)
// 0x000002B4 TouchScript.Gestures.TransformGestures.TransformGesture_TransformType TouchScript.Gestures.TransformGestures.ITransformGesture::get_TransformMask()
// 0x000002B5 UnityEngine.Vector3 TouchScript.Gestures.TransformGestures.ITransformGesture::get_DeltaPosition()
// 0x000002B6 System.Single TouchScript.Gestures.TransformGestures.ITransformGesture::get_DeltaRotation()
// 0x000002B7 System.Single TouchScript.Gestures.TransformGestures.ITransformGesture::get_DeltaScale()
// 0x000002B8 UnityEngine.Vector3 TouchScript.Gestures.TransformGestures.ITransformGesture::get_RotationAxis()
// 0x000002B9 TouchScript.Gestures.TransformGestures.TransformGesture_ProjectionType TouchScript.Gestures.TransformGestures.PinnedTransformGesture::get_Projection()
extern void PinnedTransformGesture_get_Projection_m144D5A9CD9CA71432B0B287FAC4F6146559A6089 ();
// 0x000002BA System.Void TouchScript.Gestures.TransformGestures.PinnedTransformGesture::set_Projection(TouchScript.Gestures.TransformGestures.TransformGesture_ProjectionType)
extern void PinnedTransformGesture_set_Projection_mB26E70F3EE37D51656AF825CA95B4B1759945B93 ();
// 0x000002BB UnityEngine.Vector3 TouchScript.Gestures.TransformGestures.PinnedTransformGesture::get_ProjectionPlaneNormal()
extern void PinnedTransformGesture_get_ProjectionPlaneNormal_mB2B39FE5B4E893CCA9145300682720A2FD37A764 ();
// 0x000002BC System.Void TouchScript.Gestures.TransformGestures.PinnedTransformGesture::set_ProjectionPlaneNormal(UnityEngine.Vector3)
extern void PinnedTransformGesture_set_ProjectionPlaneNormal_mB41D10CFCB39EEA5398E94B4303EC1A610A7C4CC ();
// 0x000002BD UnityEngine.Plane TouchScript.Gestures.TransformGestures.PinnedTransformGesture::get_TransformPlane()
extern void PinnedTransformGesture_get_TransformPlane_m9A7AAF95D6DA97886F35B5C70B2EA0B0956A6BDF ();
// 0x000002BE System.Void TouchScript.Gestures.TransformGestures.PinnedTransformGesture::Awake()
extern void PinnedTransformGesture_Awake_m78C2AFCA4DC848DC4EB09BC6066DE7AF0EBB2322 ();
// 0x000002BF System.Void TouchScript.Gestures.TransformGestures.PinnedTransformGesture::OnEnable()
extern void PinnedTransformGesture_OnEnable_mA76F7192E1E2E9DF1ABCB657CFC7D80082C3E70D ();
// 0x000002C0 System.Void TouchScript.Gestures.TransformGestures.PinnedTransformGesture::switchToBasicEditor()
extern void PinnedTransformGesture_switchToBasicEditor_m26C73E2FB53A9C3C386F8ACBDA3B9A0EB388AEFA ();
// 0x000002C1 System.Void TouchScript.Gestures.TransformGestures.PinnedTransformGesture::pointersPressed(System.Collections.Generic.IList`1<TouchScript.Pointers.Pointer>)
extern void PinnedTransformGesture_pointersPressed_mCE6475C8D7AE361A411532DAFFCF1FF51FBAA5AD ();
// 0x000002C2 System.Void TouchScript.Gestures.TransformGestures.PinnedTransformGesture::pointersUpdated(System.Collections.Generic.IList`1<TouchScript.Pointers.Pointer>)
extern void PinnedTransformGesture_pointersUpdated_m172FA4313437EA22DEDA34B3FF7D3E7BF861EF84 ();
// 0x000002C3 System.Void TouchScript.Gestures.TransformGestures.PinnedTransformGesture::pointersReleased(System.Collections.Generic.IList`1<TouchScript.Pointers.Pointer>)
extern void PinnedTransformGesture_pointersReleased_m5C37F010A2E7DE155FE286EBB3F5304D1B890C6D ();
// 0x000002C4 System.Single TouchScript.Gestures.TransformGestures.PinnedTransformGesture::doRotation(UnityEngine.Vector3,UnityEngine.Vector2,UnityEngine.Vector2,TouchScript.Layers.ProjectionParams)
extern void PinnedTransformGesture_doRotation_mF20E021207EC336F04CA723EB446EA89D1697D6A ();
// 0x000002C5 System.Single TouchScript.Gestures.TransformGestures.PinnedTransformGesture::doScaling(UnityEngine.Vector3,UnityEngine.Vector2,UnityEngine.Vector2,TouchScript.Layers.ProjectionParams)
extern void PinnedTransformGesture_doScaling_m899C2062740436FF12516D413C119ADC7A55949E ();
// 0x000002C6 System.Void TouchScript.Gestures.TransformGestures.PinnedTransformGesture::updateProjectionPlane()
extern void PinnedTransformGesture_updateProjectionPlane_m50DB123DD037BAA9D1EB66B1A53145A252B73354 ();
// 0x000002C7 System.Void TouchScript.Gestures.TransformGestures.PinnedTransformGesture::.ctor()
extern void PinnedTransformGesture__ctor_mA92FF69FB4CC50AFE4FB3370743065B256B47F35 ();
// 0x000002C8 System.Void TouchScript.Gestures.TransformGestures.ScreenTransformGesture::Awake()
extern void ScreenTransformGesture_Awake_m2695BCFC0987431BE03FDA2262D6A841927CC4CD ();
// 0x000002C9 System.Void TouchScript.Gestures.TransformGestures.ScreenTransformGesture::switchToBasicEditor()
extern void ScreenTransformGesture_switchToBasicEditor_m322A5452DC6BFB94D72595FA985A423116BEBCF1 ();
// 0x000002CA System.Void TouchScript.Gestures.TransformGestures.ScreenTransformGesture::pointersPressed(System.Collections.Generic.IList`1<TouchScript.Pointers.Pointer>)
extern void ScreenTransformGesture_pointersPressed_m95A7C353AE47FF044ED696D44090EF99E72B8756 ();
// 0x000002CB System.Void TouchScript.Gestures.TransformGestures.ScreenTransformGesture::pointersUpdated(System.Collections.Generic.IList`1<TouchScript.Pointers.Pointer>)
extern void ScreenTransformGesture_pointersUpdated_mF69740ED4C2ABD57B10362E98F62C1F14EABD503 ();
// 0x000002CC System.Void TouchScript.Gestures.TransformGestures.ScreenTransformGesture::pointersReleased(System.Collections.Generic.IList`1<TouchScript.Pointers.Pointer>)
extern void ScreenTransformGesture_pointersReleased_mE4BD33BC4DF4ABD1687691CA98E55CBEBCB8010D ();
// 0x000002CD System.Single TouchScript.Gestures.TransformGestures.ScreenTransformGesture::doRotation(UnityEngine.Vector2,UnityEngine.Vector2,UnityEngine.Vector2,UnityEngine.Vector2,TouchScript.Layers.ProjectionParams)
extern void ScreenTransformGesture_doRotation_m8F3DE09FA1D91038239B00231139FA43D55CCA8F ();
// 0x000002CE System.Single TouchScript.Gestures.TransformGestures.ScreenTransformGesture::doScaling(UnityEngine.Vector2,UnityEngine.Vector2,UnityEngine.Vector2,UnityEngine.Vector2,TouchScript.Layers.ProjectionParams)
extern void ScreenTransformGesture_doScaling_mC69892BF67ACBED5B948707C9AF2CD51232F3572 ();
// 0x000002CF UnityEngine.Vector3 TouchScript.Gestures.TransformGestures.ScreenTransformGesture::doOnePointTranslation(UnityEngine.Vector2,UnityEngine.Vector2,TouchScript.Layers.ProjectionParams)
extern void ScreenTransformGesture_doOnePointTranslation_m43783CF266086DA047C4FADD9AF4AAD790A267B6 ();
// 0x000002D0 UnityEngine.Vector3 TouchScript.Gestures.TransformGestures.ScreenTransformGesture::doTwoPointTranslation(UnityEngine.Vector2,UnityEngine.Vector2,UnityEngine.Vector2,UnityEngine.Vector2,System.Single,System.Single,TouchScript.Layers.ProjectionParams)
extern void ScreenTransformGesture_doTwoPointTranslation_mD4C34DCEB79ADAB7BBAD41DD5801DC290598BC66 ();
// 0x000002D1 UnityEngine.Vector2 TouchScript.Gestures.TransformGestures.ScreenTransformGesture::scaleAndRotate(UnityEngine.Vector2,UnityEngine.Vector2,System.Single,System.Single)
extern void ScreenTransformGesture_scaleAndRotate_mF48A1C058F9CA2B0B5351153E3A2EFB405E7D6A2 ();
// 0x000002D2 System.Void TouchScript.Gestures.TransformGestures.ScreenTransformGesture::.ctor()
extern void ScreenTransformGesture__ctor_m7C0D01AC89C3FDDABD53E05CF1A5F344910009BA ();
// 0x000002D3 TouchScript.Gestures.TransformGestures.TransformGesture_ProjectionType TouchScript.Gestures.TransformGestures.TransformGesture::get_Projection()
extern void TransformGesture_get_Projection_m477905703CB42C1A39C61BB3A33693EDC18CE43C ();
// 0x000002D4 System.Void TouchScript.Gestures.TransformGestures.TransformGesture::set_Projection(TouchScript.Gestures.TransformGestures.TransformGesture_ProjectionType)
extern void TransformGesture_set_Projection_mC79E4ED53F4C9B77DB8836589EAD0A75356431AC ();
// 0x000002D5 UnityEngine.Vector3 TouchScript.Gestures.TransformGestures.TransformGesture::get_ProjectionPlaneNormal()
extern void TransformGesture_get_ProjectionPlaneNormal_mF91505FF99F0694BA68F6763C5783247715831FC ();
// 0x000002D6 System.Void TouchScript.Gestures.TransformGestures.TransformGesture::set_ProjectionPlaneNormal(UnityEngine.Vector3)
extern void TransformGesture_set_ProjectionPlaneNormal_m8BF0B251A756CB66C6F4D7043E606CEA0E765300 ();
// 0x000002D7 UnityEngine.Plane TouchScript.Gestures.TransformGestures.TransformGesture::get_TransformPlane()
extern void TransformGesture_get_TransformPlane_m6709A7E604E7FD46D9438A73AD8AB21D9A29403D ();
// 0x000002D8 UnityEngine.Vector3 TouchScript.Gestures.TransformGestures.TransformGesture::get_LocalDeltaPosition()
extern void TransformGesture_get_LocalDeltaPosition_mC526FF6DAFDBDC31CC21AE741A277ACEDEBA1F73 ();
// 0x000002D9 System.Void TouchScript.Gestures.TransformGestures.TransformGesture::Awake()
extern void TransformGesture_Awake_mF3E7793B89BFC6AB3E77074CD6D2D0CEDB95E2EE ();
// 0x000002DA System.Void TouchScript.Gestures.TransformGestures.TransformGesture::OnEnable()
extern void TransformGesture_OnEnable_m2406964A10313A0EEC20C5C6F7D423612078BA76 ();
// 0x000002DB System.Void TouchScript.Gestures.TransformGestures.TransformGesture::switchToBasicEditor()
extern void TransformGesture_switchToBasicEditor_m8C74480E9FA3C4A529F989567E287B7D4C6BD231 ();
// 0x000002DC System.Void TouchScript.Gestures.TransformGestures.TransformGesture::pointersPressed(System.Collections.Generic.IList`1<TouchScript.Pointers.Pointer>)
extern void TransformGesture_pointersPressed_m0A94E1827689D601E0A43CB9945DD58A7E6198DC ();
// 0x000002DD System.Void TouchScript.Gestures.TransformGestures.TransformGesture::pointersUpdated(System.Collections.Generic.IList`1<TouchScript.Pointers.Pointer>)
extern void TransformGesture_pointersUpdated_m783B60D663A26FDE6A7BE4BF9B5854BA065A73BA ();
// 0x000002DE System.Void TouchScript.Gestures.TransformGestures.TransformGesture::pointersReleased(System.Collections.Generic.IList`1<TouchScript.Pointers.Pointer>)
extern void TransformGesture_pointersReleased_m14930EC872330DB5509EFDB80F42A6CBB8CC9ACA ();
// 0x000002DF UnityEngine.Vector3 TouchScript.Gestures.TransformGestures.TransformGesture::projectScaledRotated(UnityEngine.Vector2,System.Single,System.Single,TouchScript.Layers.ProjectionParams)
extern void TransformGesture_projectScaledRotated_mA4F89105C1928C8EBF4823DD79DB5B80DB13AA73 ();
// 0x000002E0 System.Single TouchScript.Gestures.TransformGestures.TransformGesture::doRotation(UnityEngine.Vector2,UnityEngine.Vector2,UnityEngine.Vector2,UnityEngine.Vector2,TouchScript.Layers.ProjectionParams)
extern void TransformGesture_doRotation_mB981FE34A3482C8D16E880352A29F58A61837A67 ();
// 0x000002E1 System.Single TouchScript.Gestures.TransformGestures.TransformGesture::doScaling(UnityEngine.Vector2,UnityEngine.Vector2,UnityEngine.Vector2,UnityEngine.Vector2,TouchScript.Layers.ProjectionParams)
extern void TransformGesture_doScaling_m96CC632F2FE01A4A8A7211C013B260FA1684F1DB ();
// 0x000002E2 UnityEngine.Vector3 TouchScript.Gestures.TransformGestures.TransformGesture::doOnePointTranslation(UnityEngine.Vector2,UnityEngine.Vector2,TouchScript.Layers.ProjectionParams)
extern void TransformGesture_doOnePointTranslation_m45BFBDCABC861526639CD6BA387076F2C964FC2A ();
// 0x000002E3 UnityEngine.Vector3 TouchScript.Gestures.TransformGestures.TransformGesture::doTwoPointTranslation(UnityEngine.Vector2,UnityEngine.Vector2,UnityEngine.Vector2,UnityEngine.Vector2,System.Single,System.Single,TouchScript.Layers.ProjectionParams)
extern void TransformGesture_doTwoPointTranslation_m564FB1BDCCEBBF1CB1E768784CE0A292EA9D35D0 ();
// 0x000002E4 System.Void TouchScript.Gestures.TransformGestures.TransformGesture::updateProjectionPlane()
extern void TransformGesture_updateProjectionPlane_m4919B830642DAA3EE03352C530B6823042E221E4 ();
// 0x000002E5 System.Void TouchScript.Gestures.TransformGestures.TransformGesture::.ctor()
extern void TransformGesture__ctor_m356E940E38468ACA29EBBD47C143F6EAED5E6F49 ();
// 0x000002E6 System.Boolean TouchScript.Gestures.TransformGestures.Clustered.ClusteredPinnedTransformGesture::relevantPointers(System.Collections.Generic.IList`1<TouchScript.Pointers.Pointer>)
extern void ClusteredPinnedTransformGesture_relevantPointers_m5B1D72381AA391603882CFBB02D8C190D797647C ();
// 0x000002E7 UnityEngine.Vector2 TouchScript.Gestures.TransformGestures.Clustered.ClusteredPinnedTransformGesture::getPointScreenPosition()
extern void ClusteredPinnedTransformGesture_getPointScreenPosition_mB2935F824A1486CD544331CD45655FA0E3863101 ();
// 0x000002E8 UnityEngine.Vector2 TouchScript.Gestures.TransformGestures.Clustered.ClusteredPinnedTransformGesture::getPointPreviousScreenPosition()
extern void ClusteredPinnedTransformGesture_getPointPreviousScreenPosition_m61913E4FEB8C5038379DACB1292D90711E4AF782 ();
// 0x000002E9 System.Void TouchScript.Gestures.TransformGestures.Clustered.ClusteredPinnedTransformGesture::.ctor()
extern void ClusteredPinnedTransformGesture__ctor_m284181179FCA98C2407DB16683BC3F057EF112CE ();
// 0x000002EA System.Void TouchScript.Gestures.TransformGestures.Clustered.ClusteredScreenTransformGesture::pointersPressed(System.Collections.Generic.IList`1<TouchScript.Pointers.Pointer>)
extern void ClusteredScreenTransformGesture_pointersPressed_m322BE33EC05ACC998093BC25DA024BDB70FBF577 ();
// 0x000002EB System.Void TouchScript.Gestures.TransformGestures.Clustered.ClusteredScreenTransformGesture::pointersUpdated(System.Collections.Generic.IList`1<TouchScript.Pointers.Pointer>)
extern void ClusteredScreenTransformGesture_pointersUpdated_m74CF2EB3F313FF27241BEBAFCA330481417261E7 ();
// 0x000002EC System.Void TouchScript.Gestures.TransformGestures.Clustered.ClusteredScreenTransformGesture::pointersReleased(System.Collections.Generic.IList`1<TouchScript.Pointers.Pointer>)
extern void ClusteredScreenTransformGesture_pointersReleased_mCE27673BE69A01E93D8CC74F76F395F3C1D3B083 ();
// 0x000002ED System.Void TouchScript.Gestures.TransformGestures.Clustered.ClusteredScreenTransformGesture::reset()
extern void ClusteredScreenTransformGesture_reset_mE2AF3CB7EB213A0A994CE230FBE87B4093C68E61 ();
// 0x000002EE System.Int32 TouchScript.Gestures.TransformGestures.Clustered.ClusteredScreenTransformGesture::getNumPoints()
extern void ClusteredScreenTransformGesture_getNumPoints_m4597026802232E74459ECBD519953E62069B43E8 ();
// 0x000002EF System.Boolean TouchScript.Gestures.TransformGestures.Clustered.ClusteredScreenTransformGesture::relevantPointers1(System.Collections.Generic.IList`1<TouchScript.Pointers.Pointer>)
extern void ClusteredScreenTransformGesture_relevantPointers1_m3A0410D96AA0F659E430AC653B1F2F0570D97380 ();
// 0x000002F0 System.Boolean TouchScript.Gestures.TransformGestures.Clustered.ClusteredScreenTransformGesture::relevantPointers2(System.Collections.Generic.IList`1<TouchScript.Pointers.Pointer>)
extern void ClusteredScreenTransformGesture_relevantPointers2_m86EE3F98FB33F830AEC5040308C01C81E0280F5F ();
// 0x000002F1 UnityEngine.Vector2 TouchScript.Gestures.TransformGestures.Clustered.ClusteredScreenTransformGesture::getPointScreenPosition(System.Int32)
extern void ClusteredScreenTransformGesture_getPointScreenPosition_m8EC0B56F1A2E33CA0A2500E2A4A419BC4A6FB697 ();
// 0x000002F2 UnityEngine.Vector2 TouchScript.Gestures.TransformGestures.Clustered.ClusteredScreenTransformGesture::getPointPreviousScreenPosition(System.Int32)
extern void ClusteredScreenTransformGesture_getPointPreviousScreenPosition_mF647F6B1742070902C0878F2C51ABEF5D5809CBC ();
// 0x000002F3 System.Void TouchScript.Gestures.TransformGestures.Clustered.ClusteredScreenTransformGesture::.ctor()
extern void ClusteredScreenTransformGesture__ctor_m628C61571486E91996BC06F03C348B24D0DC544F ();
// 0x000002F4 System.Void TouchScript.Gestures.TransformGestures.Clustered.ClusteredTransformGesture::pointersPressed(System.Collections.Generic.IList`1<TouchScript.Pointers.Pointer>)
extern void ClusteredTransformGesture_pointersPressed_m62B5CD651DC2EC0FAAE91B843B8622592144F346 ();
// 0x000002F5 System.Void TouchScript.Gestures.TransformGestures.Clustered.ClusteredTransformGesture::pointersUpdated(System.Collections.Generic.IList`1<TouchScript.Pointers.Pointer>)
extern void ClusteredTransformGesture_pointersUpdated_mFB4AFF1A068F4E972D1457F9940C342ACE6F4CA4 ();
// 0x000002F6 System.Void TouchScript.Gestures.TransformGestures.Clustered.ClusteredTransformGesture::pointersReleased(System.Collections.Generic.IList`1<TouchScript.Pointers.Pointer>)
extern void ClusteredTransformGesture_pointersReleased_m127A9D0255133C6015DE9D8DD9464D524BB6DC5B ();
// 0x000002F7 System.Void TouchScript.Gestures.TransformGestures.Clustered.ClusteredTransformGesture::reset()
extern void ClusteredTransformGesture_reset_m6EFB94326F15EA87754DD473469E447F4D620BEA ();
// 0x000002F8 System.Int32 TouchScript.Gestures.TransformGestures.Clustered.ClusteredTransformGesture::getNumPoints()
extern void ClusteredTransformGesture_getNumPoints_m5F90023F68B027B3B79AFCA618787A645EEB749D ();
// 0x000002F9 System.Boolean TouchScript.Gestures.TransformGestures.Clustered.ClusteredTransformGesture::relevantPointers1(System.Collections.Generic.IList`1<TouchScript.Pointers.Pointer>)
extern void ClusteredTransformGesture_relevantPointers1_m48382D5561CA1AF43251790C47E9D5BCD26111A7 ();
// 0x000002FA System.Boolean TouchScript.Gestures.TransformGestures.Clustered.ClusteredTransformGesture::relevantPointers2(System.Collections.Generic.IList`1<TouchScript.Pointers.Pointer>)
extern void ClusteredTransformGesture_relevantPointers2_mBD9617B9F0F6F6731392FAB4223E0968B4285ADA ();
// 0x000002FB UnityEngine.Vector2 TouchScript.Gestures.TransformGestures.Clustered.ClusteredTransformGesture::getPointScreenPosition(System.Int32)
extern void ClusteredTransformGesture_getPointScreenPosition_m60EB5B28C37CC944270AE9669C7A3A68E4A131AB ();
// 0x000002FC UnityEngine.Vector2 TouchScript.Gestures.TransformGestures.Clustered.ClusteredTransformGesture::getPointPreviousScreenPosition(System.Int32)
extern void ClusteredTransformGesture_getPointPreviousScreenPosition_mDE398C1E6AABC00C4C1A34A9F2CA69D1CF5F881C ();
// 0x000002FD System.Void TouchScript.Gestures.TransformGestures.Clustered.ClusteredTransformGesture::.ctor()
extern void ClusteredTransformGesture__ctor_mD3F4BA0B04400EF3510D0B652B9DEAAA4D566006 ();
// 0x000002FE UnityEngine.Vector2 TouchScript.Gestures.TransformGestures.Base.OnePointTrasformGestureBase::get_ScreenPosition()
extern void OnePointTrasformGestureBase_get_ScreenPosition_mFE43C3CB8E66E411FF0737FB1A68F2027590D3BB ();
// 0x000002FF UnityEngine.Vector2 TouchScript.Gestures.TransformGestures.Base.OnePointTrasformGestureBase::get_PreviousScreenPosition()
extern void OnePointTrasformGestureBase_get_PreviousScreenPosition_m4FA6DD6CF83388FF79A8F05A22DC6780BB2AB21E ();
// 0x00000300 System.Void TouchScript.Gestures.TransformGestures.Base.OnePointTrasformGestureBase::pointersUpdated(System.Collections.Generic.IList`1<TouchScript.Pointers.Pointer>)
extern void OnePointTrasformGestureBase_pointersUpdated_m1B2BCBBEE6D6DFED6EECC6956547E11EEBF72758 ();
// 0x00000301 System.Void TouchScript.Gestures.TransformGestures.Base.OnePointTrasformGestureBase::reset()
extern void OnePointTrasformGestureBase_reset_m8CAB822CB55B4812EFDFC6C9C8965211ACA569A0 ();
// 0x00000302 System.Single TouchScript.Gestures.TransformGestures.Base.OnePointTrasformGestureBase::doRotation(UnityEngine.Vector3,UnityEngine.Vector2,UnityEngine.Vector2,TouchScript.Layers.ProjectionParams)
extern void OnePointTrasformGestureBase_doRotation_mACC51AAFA98D4D8697197B8DBEDFBE730A9D8689 ();
// 0x00000303 System.Single TouchScript.Gestures.TransformGestures.Base.OnePointTrasformGestureBase::doScaling(UnityEngine.Vector3,UnityEngine.Vector2,UnityEngine.Vector2,TouchScript.Layers.ProjectionParams)
extern void OnePointTrasformGestureBase_doScaling_mB77719083A920644994370B08CC9001A034520A8 ();
// 0x00000304 System.Boolean TouchScript.Gestures.TransformGestures.Base.OnePointTrasformGestureBase::relevantPointers(System.Collections.Generic.IList`1<TouchScript.Pointers.Pointer>)
extern void OnePointTrasformGestureBase_relevantPointers_m79CB15A1707716F82F59D362C9FBA13A15D74F4C ();
// 0x00000305 UnityEngine.Vector2 TouchScript.Gestures.TransformGestures.Base.OnePointTrasformGestureBase::getPointScreenPosition()
extern void OnePointTrasformGestureBase_getPointScreenPosition_m8D19342A1F8E02251F814949651F005DF8D951DA ();
// 0x00000306 UnityEngine.Vector2 TouchScript.Gestures.TransformGestures.Base.OnePointTrasformGestureBase::getPointPreviousScreenPosition()
extern void OnePointTrasformGestureBase_getPointPreviousScreenPosition_m5A67B366DE89C2158F36674B7380B45B4438AF80 ();
// 0x00000307 System.Void TouchScript.Gestures.TransformGestures.Base.OnePointTrasformGestureBase::updateType()
extern void OnePointTrasformGestureBase_updateType_mE287D7BBC6800B7518F1810AC08E82264A16E658 ();
// 0x00000308 System.Void TouchScript.Gestures.TransformGestures.Base.OnePointTrasformGestureBase::.ctor()
extern void OnePointTrasformGestureBase__ctor_mC1159D5A73AE549B718FEA59A3634643E8551764 ();
// 0x00000309 System.Void TouchScript.Gestures.TransformGestures.Base.TransformGestureBase::add_TransformStarted(System.EventHandler`1<System.EventArgs>)
extern void TransformGestureBase_add_TransformStarted_mF91D27FA97873A130853A7839890B247C4C4BB14 ();
// 0x0000030A System.Void TouchScript.Gestures.TransformGestures.Base.TransformGestureBase::remove_TransformStarted(System.EventHandler`1<System.EventArgs>)
extern void TransformGestureBase_remove_TransformStarted_m2687BA12F20384D1D4246B6952E89747AB12DEF1 ();
// 0x0000030B System.Void TouchScript.Gestures.TransformGestures.Base.TransformGestureBase::add_Transformed(System.EventHandler`1<System.EventArgs>)
extern void TransformGestureBase_add_Transformed_m9D3F8364C7BCD8ACA78C36F98256EA7C5F9B9DE4 ();
// 0x0000030C System.Void TouchScript.Gestures.TransformGestures.Base.TransformGestureBase::remove_Transformed(System.EventHandler`1<System.EventArgs>)
extern void TransformGestureBase_remove_Transformed_mFF853747145C63373B2A3BC0FD2A3A084C067026 ();
// 0x0000030D System.Void TouchScript.Gestures.TransformGestures.Base.TransformGestureBase::add_TransformCompleted(System.EventHandler`1<System.EventArgs>)
extern void TransformGestureBase_add_TransformCompleted_m3BF7A1D9F541C37B5D9D728AE444E1AD406A65FF ();
// 0x0000030E System.Void TouchScript.Gestures.TransformGestures.Base.TransformGestureBase::remove_TransformCompleted(System.EventHandler`1<System.EventArgs>)
extern void TransformGestureBase_remove_TransformCompleted_m304D26C65653C89F17CDCEEB2008CC5BC8A03ADE ();
// 0x0000030F TouchScript.Gestures.TransformGestures.TransformGesture_TransformType TouchScript.Gestures.TransformGestures.Base.TransformGestureBase::get_Type()
extern void TransformGestureBase_get_Type_mF0095E9F1203C71AE506C88CB5E9C2A06B960884 ();
// 0x00000310 System.Void TouchScript.Gestures.TransformGestures.Base.TransformGestureBase::set_Type(TouchScript.Gestures.TransformGestures.TransformGesture_TransformType)
extern void TransformGestureBase_set_Type_m92276E46B69A7116D45E86F06BBE5B4A7E8638F4 ();
// 0x00000311 System.Single TouchScript.Gestures.TransformGestures.Base.TransformGestureBase::get_ScreenTransformThreshold()
extern void TransformGestureBase_get_ScreenTransformThreshold_m8478C5E8681B1AC7E53F253C2D1E226ED5E3C5D6 ();
// 0x00000312 System.Void TouchScript.Gestures.TransformGestures.Base.TransformGestureBase::set_ScreenTransformThreshold(System.Single)
extern void TransformGestureBase_set_ScreenTransformThreshold_mF2E5CC587D28A5854F3D23027A407EA70A2CE2B7 ();
// 0x00000313 TouchScript.Gestures.TransformGestures.TransformGesture_TransformType TouchScript.Gestures.TransformGestures.Base.TransformGestureBase::get_TransformMask()
extern void TransformGestureBase_get_TransformMask_mD7AF3DB99B9F96728563601852A4F33D37CA1D2C ();
// 0x00000314 UnityEngine.Vector3 TouchScript.Gestures.TransformGestures.Base.TransformGestureBase::get_DeltaPosition()
extern void TransformGestureBase_get_DeltaPosition_m4EFF8A2C639EE3080E2E45C42D82BDEA7E0B6145 ();
// 0x00000315 System.Single TouchScript.Gestures.TransformGestures.Base.TransformGestureBase::get_DeltaRotation()
extern void TransformGestureBase_get_DeltaRotation_mF862E11D69D3B40CC672A12376A10AEAF8194E93 ();
// 0x00000316 System.Single TouchScript.Gestures.TransformGestures.Base.TransformGestureBase::get_DeltaScale()
extern void TransformGestureBase_get_DeltaScale_mF5A26CA4B1B761BB736A361650206CEA55332385 ();
// 0x00000317 UnityEngine.Vector3 TouchScript.Gestures.TransformGestures.Base.TransformGestureBase::get_RotationAxis()
extern void TransformGestureBase_get_RotationAxis_m16611C3E1F587DCEE6EB2FF728A354426F3BF5A4 ();
// 0x00000318 System.Void TouchScript.Gestures.TransformGestures.Base.TransformGestureBase::OverrideTargetPosition(UnityEngine.Vector3)
extern void TransformGestureBase_OverrideTargetPosition_m830A586C2CC9B39310975F8157C4F420E1FA1824 ();
// 0x00000319 System.Void TouchScript.Gestures.TransformGestures.Base.TransformGestureBase::OnEnable()
extern void TransformGestureBase_OnEnable_m980B3F064F91093F42959C24C2AFACC051B65059 ();
// 0x0000031A System.Void TouchScript.Gestures.TransformGestures.Base.TransformGestureBase::pointersPressed(System.Collections.Generic.IList`1<TouchScript.Pointers.Pointer>)
extern void TransformGestureBase_pointersPressed_m0F719C9148CAC1B065B631F1FB63C83F421624D1 ();
// 0x0000031B System.Void TouchScript.Gestures.TransformGestures.Base.TransformGestureBase::pointersReleased(System.Collections.Generic.IList`1<TouchScript.Pointers.Pointer>)
extern void TransformGestureBase_pointersReleased_m1F213A07C0E74B38C35D785F62684E9BECAFB97D ();
// 0x0000031C System.Void TouchScript.Gestures.TransformGestures.Base.TransformGestureBase::onBegan()
extern void TransformGestureBase_onBegan_mE3E03A3EBC745AAE7419CB8D848E462CF88D6E3A ();
// 0x0000031D System.Void TouchScript.Gestures.TransformGestures.Base.TransformGestureBase::onChanged()
extern void TransformGestureBase_onChanged_mF1204496E151E56CA282DE624B1C1504974250DD ();
// 0x0000031E System.Void TouchScript.Gestures.TransformGestures.Base.TransformGestureBase::onRecognized()
extern void TransformGestureBase_onRecognized_m21F74CD27877A0410B102020D881A5AABE83E3FE ();
// 0x0000031F System.Void TouchScript.Gestures.TransformGestures.Base.TransformGestureBase::reset()
extern void TransformGestureBase_reset_mA318E5BAFCD83EFB315825C034169FEE14268DFF ();
// 0x00000320 System.Void TouchScript.Gestures.TransformGestures.Base.TransformGestureBase::updateType()
extern void TransformGestureBase_updateType_mE6DF64E94F57A533B69DEE03219288514D908CC2 ();
// 0x00000321 System.Void TouchScript.Gestures.TransformGestures.Base.TransformGestureBase::resetValues()
extern void TransformGestureBase_resetValues_m247BBB5232D7FD38F976BA63F70102BB7CC0CF8D ();
// 0x00000322 System.Void TouchScript.Gestures.TransformGestures.Base.TransformGestureBase::updateScreenTransformThreshold()
extern void TransformGestureBase_updateScreenTransformThreshold_m9C9B722B168D3A100D02E63CD2C03994F902159B ();
// 0x00000323 System.Void TouchScript.Gestures.TransformGestures.Base.TransformGestureBase::.ctor()
extern void TransformGestureBase__ctor_m778AEED4BF0755E385B29562F1B031A2A46E1149 ();
// 0x00000324 System.Single TouchScript.Gestures.TransformGestures.Base.TwoPointTransformGestureBase::get_MinScreenPointsDistance()
extern void TwoPointTransformGestureBase_get_MinScreenPointsDistance_m7CA529C28C28A98F5ED0075A4607AA94095CB461 ();
// 0x00000325 System.Void TouchScript.Gestures.TransformGestures.Base.TwoPointTransformGestureBase::set_MinScreenPointsDistance(System.Single)
extern void TwoPointTransformGestureBase_set_MinScreenPointsDistance_mDB4D71ABB8DA3B3DE9304E62F4C2305F36280412 ();
// 0x00000326 System.Void TouchScript.Gestures.TransformGestures.Base.TwoPointTransformGestureBase::OnEnable()
extern void TwoPointTransformGestureBase_OnEnable_m808CB046127E7E7781F30052E390E9CB88A3B59A ();
// 0x00000327 System.Void TouchScript.Gestures.TransformGestures.Base.TwoPointTransformGestureBase::pointersUpdated(System.Collections.Generic.IList`1<TouchScript.Pointers.Pointer>)
extern void TwoPointTransformGestureBase_pointersUpdated_m79FE24D31050A9FCC9DBC19AFB829A46DAFAF326 ();
// 0x00000328 System.Void TouchScript.Gestures.TransformGestures.Base.TwoPointTransformGestureBase::reset()
extern void TwoPointTransformGestureBase_reset_m701D1952139F26E4C07249BACDD05E33987F0E39 ();
// 0x00000329 System.Single TouchScript.Gestures.TransformGestures.Base.TwoPointTransformGestureBase::doRotation(UnityEngine.Vector2,UnityEngine.Vector2,UnityEngine.Vector2,UnityEngine.Vector2,TouchScript.Layers.ProjectionParams)
extern void TwoPointTransformGestureBase_doRotation_m94729D0FF511B4D3F152BBF6AEACABFE9EF78A81 ();
// 0x0000032A System.Single TouchScript.Gestures.TransformGestures.Base.TwoPointTransformGestureBase::doScaling(UnityEngine.Vector2,UnityEngine.Vector2,UnityEngine.Vector2,UnityEngine.Vector2,TouchScript.Layers.ProjectionParams)
extern void TwoPointTransformGestureBase_doScaling_m962A191626D50E09283BF4D45A6C4B3AC37BBED0 ();
// 0x0000032B UnityEngine.Vector3 TouchScript.Gestures.TransformGestures.Base.TwoPointTransformGestureBase::doOnePointTranslation(UnityEngine.Vector2,UnityEngine.Vector2,TouchScript.Layers.ProjectionParams)
extern void TwoPointTransformGestureBase_doOnePointTranslation_mC67B5C085D2D78705EA2FCDD506064E16E78F2A5 ();
// 0x0000032C UnityEngine.Vector3 TouchScript.Gestures.TransformGestures.Base.TwoPointTransformGestureBase::doTwoPointTranslation(UnityEngine.Vector2,UnityEngine.Vector2,UnityEngine.Vector2,UnityEngine.Vector2,System.Single,System.Single,TouchScript.Layers.ProjectionParams)
extern void TwoPointTransformGestureBase_doTwoPointTranslation_m37C86C3F36A82A271958DD89E45BCDDB86067C34 ();
// 0x0000032D System.Int32 TouchScript.Gestures.TransformGestures.Base.TwoPointTransformGestureBase::getNumPoints()
extern void TwoPointTransformGestureBase_getNumPoints_mDECFD698337D2DAF63D9BC2B2A541C235246608F ();
// 0x0000032E System.Boolean TouchScript.Gestures.TransformGestures.Base.TwoPointTransformGestureBase::relevantPointers1(System.Collections.Generic.IList`1<TouchScript.Pointers.Pointer>)
extern void TwoPointTransformGestureBase_relevantPointers1_m73D2F78852CBF959907A6625A1AC461FC1BA0365 ();
// 0x0000032F System.Boolean TouchScript.Gestures.TransformGestures.Base.TwoPointTransformGestureBase::relevantPointers2(System.Collections.Generic.IList`1<TouchScript.Pointers.Pointer>)
extern void TwoPointTransformGestureBase_relevantPointers2_m822362A5B710E1135274D1A7360C586C7C68C62E ();
// 0x00000330 UnityEngine.Vector2 TouchScript.Gestures.TransformGestures.Base.TwoPointTransformGestureBase::getPointScreenPosition(System.Int32)
extern void TwoPointTransformGestureBase_getPointScreenPosition_m29BDCAEF1AAA791FE0DABCA511F9B86A65275820 ();
// 0x00000331 UnityEngine.Vector2 TouchScript.Gestures.TransformGestures.Base.TwoPointTransformGestureBase::getPointPreviousScreenPosition(System.Int32)
extern void TwoPointTransformGestureBase_getPointPreviousScreenPosition_m98393B0578EFEB59016EAD97E393E719BC44539D ();
// 0x00000332 System.Void TouchScript.Gestures.TransformGestures.Base.TwoPointTransformGestureBase::updateMinScreenPointsDistance()
extern void TwoPointTransformGestureBase_updateMinScreenPointsDistance_m8FA1D60E3A81F14C2414CCB27860ACEFF1140B5E ();
// 0x00000333 System.Void TouchScript.Gestures.TransformGestures.Base.TwoPointTransformGestureBase::.ctor()
extern void TwoPointTransformGestureBase__ctor_m71939D7EA22415A9631AEB1ACB8F161E03C8BB51 ();
// 0x00000334 System.String TouchScript.Devices.Display.DisplayDevice::get_Name()
extern void DisplayDevice_get_Name_mF4C36BD5A83E5824E0D1741A20016D200B6D72E5 ();
// 0x00000335 System.Void TouchScript.Devices.Display.DisplayDevice::set_Name(System.String)
extern void DisplayDevice_set_Name_m717E36608D14BC08B128C167F23F2EE0706C1E8E ();
// 0x00000336 System.Single TouchScript.Devices.Display.DisplayDevice::get_DPI()
extern void DisplayDevice_get_DPI_mE1EA8DF49A3970C014CEB81C223EF5BB3F54A7AF ();
// 0x00000337 System.Single TouchScript.Devices.Display.DisplayDevice::get_NativeDPI()
extern void DisplayDevice_get_NativeDPI_mA8A9EFC78A5D2ECBFA4C36C76BF67CDDEC5BF3BD ();
// 0x00000338 UnityEngine.Vector2 TouchScript.Devices.Display.DisplayDevice::get_NativeResolution()
extern void DisplayDevice_get_NativeResolution_m2EB79A5B8799302C34CC3B849B80D552C9DE541E ();
// 0x00000339 System.Void TouchScript.Devices.Display.DisplayDevice::UpdateDPI()
extern void DisplayDevice_UpdateDPI_m9597A7DDB4BBF8ACD23E1E4BC1170FD64D194E75 ();
// 0x0000033A System.Void TouchScript.Devices.Display.DisplayDevice::OnEnable()
extern void DisplayDevice_OnEnable_mF595B56C1DC7388FE0B54D1C07B86273F874C549 ();
// 0x0000033B System.Void TouchScript.Devices.Display.DisplayDevice::.ctor()
extern void DisplayDevice__ctor_m34300C4C8E6E9CA2E74617A6A0DB7A09E2E1FDC5 ();
// 0x0000033C System.Boolean TouchScript.Devices.Display.GenericDisplayDevice::get_IsLaptop()
extern void GenericDisplayDevice_get_IsLaptop_m0F20D18DE8E7D4FA3A736EA493935DC1006D7CAE ();
// 0x0000033D System.Void TouchScript.Devices.Display.GenericDisplayDevice::UpdateDPI()
extern void GenericDisplayDevice_UpdateDPI_m6CC88F0352E351A0F75A32513324E3413A9D87EC ();
// 0x0000033E System.Void TouchScript.Devices.Display.GenericDisplayDevice::OnEnable()
extern void GenericDisplayDevice_OnEnable_m34D4E9D03830441151108AB16FC0169844036DDF ();
// 0x0000033F System.Void TouchScript.Devices.Display.GenericDisplayDevice::updateNativeResulotion()
extern void GenericDisplayDevice_updateNativeResulotion_m253809FCC6F08CE19AF1D5583CF946BD4A9347C2 ();
// 0x00000340 System.Void TouchScript.Devices.Display.GenericDisplayDevice::updateNativeDPI()
extern void GenericDisplayDevice_updateNativeDPI_m75A4C52E35F1AD5EB3F28364FE92D4FD2C086CFA ();
// 0x00000341 System.Boolean TouchScript.Devices.Display.GenericDisplayDevice::getHighestResolution(UnityEngine.Vector2&)
extern void GenericDisplayDevice_getHighestResolution_m879363517A6886123D0C084C309FC7055BEB5C1F ();
// 0x00000342 System.Void TouchScript.Devices.Display.GenericDisplayDevice::.ctor()
extern void GenericDisplayDevice__ctor_mDE6666CCEB66F5ADF245C49DF484465C5153056A ();
// 0x00000343 System.Void TouchScript.Devices.Display.GenericDisplayDevice::.cctor()
extern void GenericDisplayDevice__cctor_m579BEC689F617986DBBBC6507506CE211526E0F4 ();
// 0x00000344 System.String TouchScript.Devices.Display.IDisplayDevice::get_Name()
// 0x00000345 System.Single TouchScript.Devices.Display.IDisplayDevice::get_DPI()
// 0x00000346 System.Single TouchScript.Devices.Display.IDisplayDevice::get_NativeDPI()
// 0x00000347 UnityEngine.Vector2 TouchScript.Devices.Display.IDisplayDevice::get_NativeResolution()
// 0x00000348 System.Void TouchScript.Devices.Display.IDisplayDevice::UpdateDPI()
// 0x00000349 System.Boolean TouchScript.Core.DebuggableMonoBehaviour::get_DebugMode()
extern void DebuggableMonoBehaviour_get_DebugMode_m1038AA3417292402A0CF31B7D83FBA6274991AF5 ();
// 0x0000034A System.Void TouchScript.Core.DebuggableMonoBehaviour::set_DebugMode(System.Boolean)
extern void DebuggableMonoBehaviour_set_DebugMode_mF68A4E8748195B9495B7B93559B751B6E382E4EC ();
// 0x0000034B System.Void TouchScript.Core.DebuggableMonoBehaviour::.ctor()
extern void DebuggableMonoBehaviour__ctor_m2721B121281EA6FCDA46C369E88D5A282D6FD065 ();
// 0x0000034C TouchScript.IGestureManager TouchScript.Core.GestureManagerInstance::get_Instance()
extern void GestureManagerInstance_get_Instance_mEDD8318DA11453F2645F22C99EDFF65BC27771B0 ();
// 0x0000034D TouchScript.IGestureDelegate TouchScript.Core.GestureManagerInstance::get_GlobalGestureDelegate()
extern void GestureManagerInstance_get_GlobalGestureDelegate_m52C9B3384BBA949F699BE46E2F12B0F3ABB74634 ();
// 0x0000034E System.Void TouchScript.Core.GestureManagerInstance::set_GlobalGestureDelegate(TouchScript.IGestureDelegate)
extern void GestureManagerInstance_set_GlobalGestureDelegate_m0E83EB33A9D36F0B0D81003C1F41C08DE3126548 ();
// 0x0000034F System.Void TouchScript.Core.GestureManagerInstance::Awake()
extern void GestureManagerInstance_Awake_mE0E518D1AF08A9EA9D7D0C7FA0D09837260A7AAF ();
// 0x00000350 System.Void TouchScript.Core.GestureManagerInstance::OnEnable()
extern void GestureManagerInstance_OnEnable_m67B621910568A052A256B147FCE2856B93096756 ();
// 0x00000351 System.Void TouchScript.Core.GestureManagerInstance::OnDisable()
extern void GestureManagerInstance_OnDisable_mEC1C7786C576C3FA550BF6006EA4A17D8F8F692E ();
// 0x00000352 System.Void TouchScript.Core.GestureManagerInstance::OnApplicationQuit()
extern void GestureManagerInstance_OnApplicationQuit_m638F1509803F2108184E956CF9A60E08B9D36AF3 ();
// 0x00000353 TouchScript.Gestures.Gesture_GestureState TouchScript.Core.GestureManagerInstance::INTERNAL_GestureChangeState(TouchScript.Gestures.Gesture,TouchScript.Gestures.Gesture_GestureState)
extern void GestureManagerInstance_INTERNAL_GestureChangeState_m7ADDA1CDE3BBA348832BAB067862C32F1046DD3C ();
// 0x00000354 System.Void TouchScript.Core.GestureManagerInstance::updatePressed(System.Collections.Generic.IList`1<TouchScript.Pointers.Pointer>)
extern void GestureManagerInstance_updatePressed_m4A3431B20DB4358635D4508B506289F6C524AF3C ();
// 0x00000355 System.Void TouchScript.Core.GestureManagerInstance::updateUpdated(System.Collections.Generic.IList`1<TouchScript.Pointers.Pointer>)
extern void GestureManagerInstance_updateUpdated_m88C014CE03F4DB43D30DA528D8B3C8A99238367B ();
// 0x00000356 System.Void TouchScript.Core.GestureManagerInstance::updateReleased(System.Collections.Generic.IList`1<TouchScript.Pointers.Pointer>)
extern void GestureManagerInstance_updateReleased_mDD5B76E9727664515ADD872378518BFC6E51816F ();
// 0x00000357 System.Void TouchScript.Core.GestureManagerInstance::updateCancelled(System.Collections.Generic.IList`1<TouchScript.Pointers.Pointer>)
extern void GestureManagerInstance_updateCancelled_mC1CA3E9AC5F162EE8BEDB32CF72AA99672553035 ();
// 0x00000358 System.Void TouchScript.Core.GestureManagerInstance::sortPointersForActiveGestures(System.Collections.Generic.IList`1<TouchScript.Pointers.Pointer>)
extern void GestureManagerInstance_sortPointersForActiveGestures_mB67FCF00E4ADA3BF91021BB726CF234A73256D88 ();
// 0x00000359 System.Void TouchScript.Core.GestureManagerInstance::removePointers(System.Collections.Generic.IList`1<TouchScript.Pointers.Pointer>)
extern void GestureManagerInstance_removePointers_mD9252F209D1A06F36C49C934DF66683F0AA92578 ();
// 0x0000035A System.Void TouchScript.Core.GestureManagerInstance::resetGestures()
extern void GestureManagerInstance_resetGestures_mBE260342D905BD4FDA4A7A3741CE38E387AB98E7 ();
// 0x0000035B System.Void TouchScript.Core.GestureManagerInstance::clearFrameCaches()
extern void GestureManagerInstance_clearFrameCaches_mD929F1FEE7D95EA900951A5BA2B384216262C593 ();
// 0x0000035C System.Collections.Generic.List`1<TouchScript.Gestures.Gesture> TouchScript.Core.GestureManagerInstance::getHierarchyEndingWith(UnityEngine.Transform)
extern void GestureManagerInstance_getHierarchyEndingWith_m720D31D11E2E8D951D262CC088B53001940935C8 ();
// 0x0000035D System.Collections.Generic.List`1<TouchScript.Gestures.Gesture> TouchScript.Core.GestureManagerInstance::getHierarchyBeginningWith(UnityEngine.Transform)
extern void GestureManagerInstance_getHierarchyBeginningWith_m3FC9C10E1B6AB16FB4A859986CD6094F47F53601 ();
// 0x0000035E System.Boolean TouchScript.Core.GestureManagerInstance::gestureIsActive(TouchScript.Gestures.Gesture)
extern void GestureManagerInstance_gestureIsActive_mA8D71708EE5A5EF9B87AF5D35D20C51AC92E41C2 ();
// 0x0000035F System.Boolean TouchScript.Core.GestureManagerInstance::recognizeGestureIfNotPrevented(TouchScript.Gestures.Gesture)
extern void GestureManagerInstance_recognizeGestureIfNotPrevented_m7E3B0270D4D9B0E2EFE16CAC40CCE0DDBCFAAF50 ();
// 0x00000360 System.Void TouchScript.Core.GestureManagerInstance::failGesture(TouchScript.Gestures.Gesture)
extern void GestureManagerInstance_failGesture_m4845EC2561CDD90131EF176865F58AEC08E749D6 ();
// 0x00000361 System.Boolean TouchScript.Core.GestureManagerInstance::shouldReceivePointer(TouchScript.Gestures.Gesture,TouchScript.Pointers.Pointer)
extern void GestureManagerInstance_shouldReceivePointer_m61DB3A5AB20FB50450605782CEEC7C86EA5660C6 ();
// 0x00000362 System.Boolean TouchScript.Core.GestureManagerInstance::shouldBegin(TouchScript.Gestures.Gesture)
extern void GestureManagerInstance_shouldBegin_m5E8CFD1280B6CA8E163FB0A21DEB583A15479ED6 ();
// 0x00000363 System.Boolean TouchScript.Core.GestureManagerInstance::canPreventGesture(TouchScript.Gestures.Gesture,TouchScript.Gestures.Gesture)
extern void GestureManagerInstance_canPreventGesture_m58E0B30C017877B734AE147C59C03B3410E6A000 ();
// 0x00000364 System.Void TouchScript.Core.GestureManagerInstance::frameFinishedHandler(System.Object,System.EventArgs)
extern void GestureManagerInstance_frameFinishedHandler_m2BFD2EDC70F97D68F10EB607ED96594C5C0B5394 ();
// 0x00000365 System.Void TouchScript.Core.GestureManagerInstance::frameStartedHandler(System.Object,System.EventArgs)
extern void GestureManagerInstance_frameStartedHandler_m4EE45D6825B3621FEA81AC19ED3BC3B41BD385BA ();
// 0x00000366 System.Void TouchScript.Core.GestureManagerInstance::pointersPressedHandler(System.Object,TouchScript.PointerEventArgs)
extern void GestureManagerInstance_pointersPressedHandler_m6D101D3CA86EBF1D04E37F178A90FC3DA65F640D ();
// 0x00000367 System.Void TouchScript.Core.GestureManagerInstance::pointersUpdatedHandler(System.Object,TouchScript.PointerEventArgs)
extern void GestureManagerInstance_pointersUpdatedHandler_mB5E6B57CA8A2F03A088470116D6447579CF31197 ();
// 0x00000368 System.Void TouchScript.Core.GestureManagerInstance::pointersReleasedHandler(System.Object,TouchScript.PointerEventArgs)
extern void GestureManagerInstance_pointersReleasedHandler_m9C04B98378F3D7612207E1F0EF4B5B35899F0227 ();
// 0x00000369 System.Void TouchScript.Core.GestureManagerInstance::pointersCancelledHandler(System.Object,TouchScript.PointerEventArgs)
extern void GestureManagerInstance_pointersCancelledHandler_m41C83BD7BC69A297F36875DBF94E3FEE2337F234 ();
// 0x0000036A System.Void TouchScript.Core.GestureManagerInstance::.ctor()
extern void GestureManagerInstance__ctor_mCF2E1DD8B9C68DFE7E60080E3A5789EA7CCF648D ();
// 0x0000036B System.Void TouchScript.Core.GestureManagerInstance::.cctor()
extern void GestureManagerInstance__cctor_m28237BD840950EDC962A2CDB3A9A0E175799B3CD ();
// 0x0000036C TouchScript.ILayerManager TouchScript.Core.LayerManagerInstance::get_Instance()
extern void LayerManagerInstance_get_Instance_m35136008FD919F7E1FAC5079A1DA8F04E8C8B2F9 ();
// 0x0000036D System.Collections.Generic.IList`1<TouchScript.Layers.TouchLayer> TouchScript.Core.LayerManagerInstance::get_Layers()
extern void LayerManagerInstance_get_Layers_m8B02793D5E8DE67DCE35DFD431F99B5EDD6022DC ();
// 0x0000036E System.Int32 TouchScript.Core.LayerManagerInstance::get_LayerCount()
extern void LayerManagerInstance_get_LayerCount_m48BF00EE2918595E551BF6CD2B8168C7252F509A ();
// 0x0000036F System.Boolean TouchScript.Core.LayerManagerInstance::get_HasExclusive()
extern void LayerManagerInstance_get_HasExclusive_m1D89A12F69CF7C13B050D7E54AB55E6A0A33B488 ();
// 0x00000370 System.Boolean TouchScript.Core.LayerManagerInstance::AddLayer(TouchScript.Layers.TouchLayer,System.Int32,System.Boolean)
extern void LayerManagerInstance_AddLayer_m1DB03A3C1C0826F3B443E556A0A8DA895DAA01F5 ();
// 0x00000371 System.Boolean TouchScript.Core.LayerManagerInstance::RemoveLayer(TouchScript.Layers.TouchLayer)
extern void LayerManagerInstance_RemoveLayer_m5DE2E8EB003BBC95B1E19CDDA207B2B9F67FCC12 ();
// 0x00000372 System.Void TouchScript.Core.LayerManagerInstance::ChangeLayerIndex(System.Int32,System.Int32)
extern void LayerManagerInstance_ChangeLayerIndex_m127265736EFEEFCACC99022F7106EA4132D0F1AB ();
// 0x00000373 System.Void TouchScript.Core.LayerManagerInstance::ForEach(System.Func`2<TouchScript.Layers.TouchLayer,System.Boolean>)
extern void LayerManagerInstance_ForEach_m18FF3CE3F49821AAD6151A0BD86B13C8E45ECBD8 ();
// 0x00000374 System.Boolean TouchScript.Core.LayerManagerInstance::GetHitTarget(TouchScript.Pointers.IPointer,TouchScript.Hit.HitData&)
extern void LayerManagerInstance_GetHitTarget_m9B935210E7FF3A5562AE24FA17AB0936252E72D9 ();
// 0x00000375 System.Void TouchScript.Core.LayerManagerInstance::SetExclusive(UnityEngine.Transform,System.Boolean)
extern void LayerManagerInstance_SetExclusive_mED6612DE2801C49E07D0BC1B1EE3FE494EA26522 ();
// 0x00000376 System.Void TouchScript.Core.LayerManagerInstance::SetExclusive(System.Collections.Generic.IEnumerable`1<UnityEngine.Transform>)
extern void LayerManagerInstance_SetExclusive_m037482E05A3F0D7E1E741A06FC4781A6E0FA2780 ();
// 0x00000377 System.Boolean TouchScript.Core.LayerManagerInstance::IsExclusive(UnityEngine.Transform)
extern void LayerManagerInstance_IsExclusive_mD2707FAF3BF644EE057F02CB42772A36118CABD3 ();
// 0x00000378 System.Void TouchScript.Core.LayerManagerInstance::ClearExclusive()
extern void LayerManagerInstance_ClearExclusive_mA4357C0DEBFBB335906377ACC26561ADA7A0ED32 ();
// 0x00000379 System.Void TouchScript.Core.LayerManagerInstance::Awake()
extern void LayerManagerInstance_Awake_m89CE105D333C64CEEA5DC7C6EDE79E412EEA14C8 ();
// 0x0000037A System.Void TouchScript.Core.LayerManagerInstance::OnEnable()
extern void LayerManagerInstance_OnEnable_m9554249E85F2F6906C3E27947C716D19F2F851C6 ();
// 0x0000037B System.Void TouchScript.Core.LayerManagerInstance::OnDisable()
extern void LayerManagerInstance_OnDisable_m20CF81C9D65F8AE325041BE09A1290AD1A461A35 ();
// 0x0000037C System.Void TouchScript.Core.LayerManagerInstance::OnApplicationQuit()
extern void LayerManagerInstance_OnApplicationQuit_mAF1CFC266BD99E9CBA5B3F9D5DEF86304F488786 ();
// 0x0000037D System.Void TouchScript.Core.LayerManagerInstance::frameFinishedHandler(System.Object,System.EventArgs)
extern void LayerManagerInstance_frameFinishedHandler_m500D8B4B5C494A1D2D7B003D1973A576E441A52E ();
// 0x0000037E System.Void TouchScript.Core.LayerManagerInstance::.ctor()
extern void LayerManagerInstance__ctor_m6614061C5C10EEDDC24EDAE5389EE7E4D2456C70 ();
// 0x0000037F System.Void TouchScript.Core.LayerManagerInstance::.cctor()
extern void LayerManagerInstance__cctor_m45F68C22366CBE3487F34CF434E541D4A67EF226 ();
// 0x00000380 System.Void TouchScript.Core.TouchManagerInstance::add_FrameStarted(System.EventHandler)
extern void TouchManagerInstance_add_FrameStarted_mC213BC4B71B1EAE89D8AC9A8DB2CA9857E2644D1 ();
// 0x00000381 System.Void TouchScript.Core.TouchManagerInstance::remove_FrameStarted(System.EventHandler)
extern void TouchManagerInstance_remove_FrameStarted_m32EA2BA1805899D2BE28EC7119E811765FF06A68 ();
// 0x00000382 System.Void TouchScript.Core.TouchManagerInstance::add_FrameFinished(System.EventHandler)
extern void TouchManagerInstance_add_FrameFinished_mE89E2CC43FFE489E8F53ECBBA32C9F8FC3EFC97E ();
// 0x00000383 System.Void TouchScript.Core.TouchManagerInstance::remove_FrameFinished(System.EventHandler)
extern void TouchManagerInstance_remove_FrameFinished_m88A4D8FBDAADF4D254872AA911EB47FE466DCA1E ();
// 0x00000384 System.Void TouchScript.Core.TouchManagerInstance::add_PointersAdded(System.EventHandler`1<TouchScript.PointerEventArgs>)
extern void TouchManagerInstance_add_PointersAdded_m11E8CE419CCD16A750E9F03F982AD2E504B24243 ();
// 0x00000385 System.Void TouchScript.Core.TouchManagerInstance::remove_PointersAdded(System.EventHandler`1<TouchScript.PointerEventArgs>)
extern void TouchManagerInstance_remove_PointersAdded_mEED459E21FB7973794253E4AD40701CAB7BD8B19 ();
// 0x00000386 System.Void TouchScript.Core.TouchManagerInstance::add_PointersUpdated(System.EventHandler`1<TouchScript.PointerEventArgs>)
extern void TouchManagerInstance_add_PointersUpdated_m07CF0C6D90BEBEA4810E116BB22AF1CED2B65986 ();
// 0x00000387 System.Void TouchScript.Core.TouchManagerInstance::remove_PointersUpdated(System.EventHandler`1<TouchScript.PointerEventArgs>)
extern void TouchManagerInstance_remove_PointersUpdated_m453B8C556169ED648CA1C4A43F95AB09D26E9EF5 ();
// 0x00000388 System.Void TouchScript.Core.TouchManagerInstance::add_PointersPressed(System.EventHandler`1<TouchScript.PointerEventArgs>)
extern void TouchManagerInstance_add_PointersPressed_m900D03923946A08FA829F054E1C5C7E5B49AB44B ();
// 0x00000389 System.Void TouchScript.Core.TouchManagerInstance::remove_PointersPressed(System.EventHandler`1<TouchScript.PointerEventArgs>)
extern void TouchManagerInstance_remove_PointersPressed_m8794B5A72E3FE24F534941E6CC95AE1BF823DD0A ();
// 0x0000038A System.Void TouchScript.Core.TouchManagerInstance::add_PointersReleased(System.EventHandler`1<TouchScript.PointerEventArgs>)
extern void TouchManagerInstance_add_PointersReleased_mA140ED9C8302817F291E34FB2B4921BD4D1CD52D ();
// 0x0000038B System.Void TouchScript.Core.TouchManagerInstance::remove_PointersReleased(System.EventHandler`1<TouchScript.PointerEventArgs>)
extern void TouchManagerInstance_remove_PointersReleased_m92065E3273A7D739A4B198A332BD94BB8E917151 ();
// 0x0000038C System.Void TouchScript.Core.TouchManagerInstance::add_PointersRemoved(System.EventHandler`1<TouchScript.PointerEventArgs>)
extern void TouchManagerInstance_add_PointersRemoved_mEC7E8C8F0F4F767435E55ADA36BACD5553FBF448 ();
// 0x0000038D System.Void TouchScript.Core.TouchManagerInstance::remove_PointersRemoved(System.EventHandler`1<TouchScript.PointerEventArgs>)
extern void TouchManagerInstance_remove_PointersRemoved_m083650C359DB15D34631A89A1CA8955D364C72D1 ();
// 0x0000038E System.Void TouchScript.Core.TouchManagerInstance::add_PointersCancelled(System.EventHandler`1<TouchScript.PointerEventArgs>)
extern void TouchManagerInstance_add_PointersCancelled_m4A4268F071D9D3AE4E4C680DF7D5566EE801DD19 ();
// 0x0000038F System.Void TouchScript.Core.TouchManagerInstance::remove_PointersCancelled(System.EventHandler`1<TouchScript.PointerEventArgs>)
extern void TouchManagerInstance_remove_PointersCancelled_mECECCC50EC0336C930EE458B4C231B251EB107B6 ();
// 0x00000390 TouchScript.Core.TouchManagerInstance TouchScript.Core.TouchManagerInstance::get_Instance()
extern void TouchManagerInstance_get_Instance_m78AE6F31FA1A28D96EEAA748AD95F344E1E219EA ();
// 0x00000391 TouchScript.Devices.Display.IDisplayDevice TouchScript.Core.TouchManagerInstance::get_DisplayDevice()
extern void TouchManagerInstance_get_DisplayDevice_mA12E14299FD2AE2DB893564EFF47B8BB5B56B9A3 ();
// 0x00000392 System.Void TouchScript.Core.TouchManagerInstance::set_DisplayDevice(TouchScript.Devices.Display.IDisplayDevice)
extern void TouchManagerInstance_set_DisplayDevice_mC949640D595EA41A6D1FAB8827C650FABD4A8C1D ();
// 0x00000393 System.Single TouchScript.Core.TouchManagerInstance::get_DPI()
extern void TouchManagerInstance_get_DPI_mC1001719F67DA7CDF3FA9F863C5E9219DA4616DA ();
// 0x00000394 System.Boolean TouchScript.Core.TouchManagerInstance::get_ShouldCreateCameraLayer()
extern void TouchManagerInstance_get_ShouldCreateCameraLayer_mA4052FF942E934603919F6461D46CC956A620B4F ();
// 0x00000395 System.Void TouchScript.Core.TouchManagerInstance::set_ShouldCreateCameraLayer(System.Boolean)
extern void TouchManagerInstance_set_ShouldCreateCameraLayer_m201621650A241F0AF2D42FE7630B3658EA9A8B23 ();
// 0x00000396 System.Boolean TouchScript.Core.TouchManagerInstance::get_ShouldCreateStandardInput()
extern void TouchManagerInstance_get_ShouldCreateStandardInput_m3FABC945C4AB65CEF870523E96A91C2A93282188 ();
// 0x00000397 System.Void TouchScript.Core.TouchManagerInstance::set_ShouldCreateStandardInput(System.Boolean)
extern void TouchManagerInstance_set_ShouldCreateStandardInput_m2ADF05D5AE0D86C009638F9D0A3F77CBCD0BDA4E ();
// 0x00000398 System.Collections.Generic.IList`1<TouchScript.InputSources.IInputSource> TouchScript.Core.TouchManagerInstance::get_Inputs()
extern void TouchManagerInstance_get_Inputs_mFD9D07E612C8CBBA3FEF056A7B658C4E53026024 ();
// 0x00000399 System.Single TouchScript.Core.TouchManagerInstance::get_DotsPerCentimeter()
extern void TouchManagerInstance_get_DotsPerCentimeter_m41295F72BE58FCB18E6EC43A888D396C7610FD14 ();
// 0x0000039A System.Int32 TouchScript.Core.TouchManagerInstance::get_PointersCount()
extern void TouchManagerInstance_get_PointersCount_mDD00330D2BBF5946B3D48C8515F0C1C39EA904BC ();
// 0x0000039B System.Collections.Generic.IList`1<TouchScript.Pointers.Pointer> TouchScript.Core.TouchManagerInstance::get_Pointers()
extern void TouchManagerInstance_get_Pointers_mEB333ED681ABA79523A95B323C0F988D9A70EEE5 ();
// 0x0000039C System.Int32 TouchScript.Core.TouchManagerInstance::get_PressedPointersCount()
extern void TouchManagerInstance_get_PressedPointersCount_mBD080B3F5D7365B0C5789C0AAAED353D32A3F148 ();
// 0x0000039D System.Collections.Generic.IList`1<TouchScript.Pointers.Pointer> TouchScript.Core.TouchManagerInstance::get_PressedPointers()
extern void TouchManagerInstance_get_PressedPointers_mDD4439DA292D9BD331ABE7976CA812D15260AA2E ();
// 0x0000039E System.Boolean TouchScript.Core.TouchManagerInstance::get_IsInsidePointerFrame()
extern void TouchManagerInstance_get_IsInsidePointerFrame_m95B0763088B71DA4ABBA5214290D240EA4D7804C ();
// 0x0000039F System.Void TouchScript.Core.TouchManagerInstance::set_IsInsidePointerFrame(System.Boolean)
extern void TouchManagerInstance_set_IsInsidePointerFrame_mD402343201F17BDA1C70FD0C0C51F4ACB6777597 ();
// 0x000003A0 System.Boolean TouchScript.Core.TouchManagerInstance::AddInput(TouchScript.InputSources.IInputSource)
extern void TouchManagerInstance_AddInput_m7D38585DACC14D463677F981C91928EE9DF9CB14 ();
// 0x000003A1 System.Boolean TouchScript.Core.TouchManagerInstance::RemoveInput(TouchScript.InputSources.IInputSource)
extern void TouchManagerInstance_RemoveInput_m18A8C0920BC0027D511AB9D6078227E1DD02CEF3 ();
// 0x000003A2 System.Void TouchScript.Core.TouchManagerInstance::CancelPointer(System.Int32,System.Boolean)
extern void TouchManagerInstance_CancelPointer_m1D5CA640901A10DC01FA2385D24C0E5CE269E7DE ();
// 0x000003A3 System.Void TouchScript.Core.TouchManagerInstance::CancelPointer(System.Int32)
extern void TouchManagerInstance_CancelPointer_m63CB5F0CF8B190EE4F5BDDF510ED184F91E8130E ();
// 0x000003A4 System.Void TouchScript.Core.TouchManagerInstance::UpdateResolution()
extern void TouchManagerInstance_UpdateResolution_m9611D1296A160E2F5F2DDCBBEF3ED1E0FE60547D ();
// 0x000003A5 System.Void TouchScript.Core.TouchManagerInstance::INTERNAL_AddPointer(TouchScript.Pointers.Pointer)
extern void TouchManagerInstance_INTERNAL_AddPointer_mC45CE29B4CBC6BCEF20BD816EBDD5C71A6A994EE ();
// 0x000003A6 System.Void TouchScript.Core.TouchManagerInstance::INTERNAL_UpdatePointer(System.Int32)
extern void TouchManagerInstance_INTERNAL_UpdatePointer_m659F74AE6DEE48C0CC51BFE47CFB38906C14499A ();
// 0x000003A7 System.Void TouchScript.Core.TouchManagerInstance::INTERNAL_PressPointer(System.Int32)
extern void TouchManagerInstance_INTERNAL_PressPointer_m87EF8DB01CBC2124AF9C7B456A4B7DAD59DA0B0D ();
// 0x000003A8 System.Void TouchScript.Core.TouchManagerInstance::INTERNAL_ReleasePointer(System.Int32)
extern void TouchManagerInstance_INTERNAL_ReleasePointer_m0E98FA271BEEDA11ADC8E0263EE09A6C17E3B4F9 ();
// 0x000003A9 System.Void TouchScript.Core.TouchManagerInstance::INTERNAL_RemovePointer(System.Int32)
extern void TouchManagerInstance_INTERNAL_RemovePointer_m9B0F26CA6B21D5BC92536AEF7E00621986021F51 ();
// 0x000003AA System.Void TouchScript.Core.TouchManagerInstance::INTERNAL_CancelPointer(System.Int32)
extern void TouchManagerInstance_INTERNAL_CancelPointer_m0877C5855D096E005F3BFBBDE6708744FF7B98F4 ();
// 0x000003AB System.Void TouchScript.Core.TouchManagerInstance::Awake()
extern void TouchManagerInstance_Awake_mB4198CB119E8897B1BFA02850BAD9593121EF0FF ();
// 0x000003AC System.Void TouchScript.Core.TouchManagerInstance::sceneLoadedHandler(UnityEngine.SceneManagement.Scene,UnityEngine.SceneManagement.LoadSceneMode)
extern void TouchManagerInstance_sceneLoadedHandler_m1708D4BAB85BA2C626DC4050DA76C591E706BC08 ();
// 0x000003AD System.Collections.IEnumerator TouchScript.Core.TouchManagerInstance::lateAwake()
extern void TouchManagerInstance_lateAwake_mE80F82728E3C818FE16DAEA042935D11BF352391 ();
// 0x000003AE System.Void TouchScript.Core.TouchManagerInstance::Update()
extern void TouchManagerInstance_Update_m27EB9A6464AD4089C567A079CBA7506B2512C781 ();
// 0x000003AF System.Void TouchScript.Core.TouchManagerInstance::OnApplicationQuit()
extern void TouchManagerInstance_OnApplicationQuit_m8DF3C8AA6DBA480A9DEF4F79891BDEAF1B781A98 ();
// 0x000003B0 System.Void TouchScript.Core.TouchManagerInstance::createCameraLayer()
extern void TouchManagerInstance_createCameraLayer_mBA06278792A4CD63885F5D276C1C68E57470D983 ();
// 0x000003B1 System.Void TouchScript.Core.TouchManagerInstance::createInput()
extern void TouchManagerInstance_createInput_mDC401897FB67C29D077EF349604D637CB324BC8A ();
// 0x000003B2 System.Void TouchScript.Core.TouchManagerInstance::updateInputs()
extern void TouchManagerInstance_updateInputs_m3DB2CED0E81090754F86E5C4C40C34E4C481D2FE ();
// 0x000003B3 System.Void TouchScript.Core.TouchManagerInstance::updateAdded(System.Collections.Generic.List`1<TouchScript.Pointers.Pointer>)
extern void TouchManagerInstance_updateAdded_m8AA0273B193026A8E62DB06E98920444384CFB9C ();
// 0x000003B4 System.Boolean TouchScript.Core.TouchManagerInstance::layerAddPointer(TouchScript.Layers.TouchLayer)
extern void TouchManagerInstance_layerAddPointer_m8074A3A42E67E73FF01502F7A2ED34DA6D8BAA04 ();
// 0x000003B5 System.Void TouchScript.Core.TouchManagerInstance::updateUpdated(System.Collections.Generic.List`1<System.Int32>)
extern void TouchManagerInstance_updateUpdated_m12FC23113E9F4EC5A47C832C6C49CC94F0F8F9BC ();
// 0x000003B6 System.Boolean TouchScript.Core.TouchManagerInstance::layerUpdatePointer(TouchScript.Layers.TouchLayer)
extern void TouchManagerInstance_layerUpdatePointer_m0A8CE629D01777E250E768EE74C9C1B5283F0949 ();
// 0x000003B7 System.Void TouchScript.Core.TouchManagerInstance::updatePressed(System.Collections.Generic.List`1<System.Int32>)
extern void TouchManagerInstance_updatePressed_m25A4DBEA34D8E88E6549AD57C1D33A94B8FB00FE ();
// 0x000003B8 System.Void TouchScript.Core.TouchManagerInstance::updateReleased(System.Collections.Generic.List`1<System.Int32>)
extern void TouchManagerInstance_updateReleased_m10F618711D1DD2869C1DE78393F088BF638E6FF8 ();
// 0x000003B9 System.Void TouchScript.Core.TouchManagerInstance::updateRemoved(System.Collections.Generic.List`1<System.Int32>)
extern void TouchManagerInstance_updateRemoved_m0B6CA106190B3470AAE6D0C814D4B1EA59C5483B ();
// 0x000003BA System.Boolean TouchScript.Core.TouchManagerInstance::layerRemovePointer(TouchScript.Layers.TouchLayer)
extern void TouchManagerInstance_layerRemovePointer_m7D0FAE663816EA8EBB12768F82B455264B2C3AF0 ();
// 0x000003BB System.Void TouchScript.Core.TouchManagerInstance::updateCancelled(System.Collections.Generic.List`1<System.Int32>)
extern void TouchManagerInstance_updateCancelled_m19D8B796225CEFA23AE31B5197BC49BB3C4FC062 ();
// 0x000003BC System.Boolean TouchScript.Core.TouchManagerInstance::layerCancelPointer(TouchScript.Layers.TouchLayer)
extern void TouchManagerInstance_layerCancelPointer_mA336D324314F476DAA9D0F87B279F07AF8FBBFF3 ();
// 0x000003BD System.Void TouchScript.Core.TouchManagerInstance::sendFrameStartedToPointers()
extern void TouchManagerInstance_sendFrameStartedToPointers_mE6BCC16FDDB562F77CE2B60E1291003D564A3D6D ();
// 0x000003BE System.Void TouchScript.Core.TouchManagerInstance::updatePointers()
extern void TouchManagerInstance_updatePointers_m826E60A9DEA74F4AC5D1AC901C234C88BA3595C3 ();
// 0x000003BF System.Boolean TouchScript.Core.TouchManagerInstance::wasPointerAddedThisFrame(System.Int32,TouchScript.Pointers.Pointer&)
extern void TouchManagerInstance_wasPointerAddedThisFrame_mFBD260568A0E31AF9640EA7B23E76B0D21F5AAA5 ();
// 0x000003C0 System.Void TouchScript.Core.TouchManagerInstance::.ctor()
extern void TouchManagerInstance__ctor_mC4BE5F6D1842F6EF526816ABCAD3CCB65E3679EA ();
// 0x000003C1 System.Void TouchScript.Core.TouchManagerInstance::.cctor()
extern void TouchManagerInstance__cctor_mFF054D19E0E20EB9A1E4D31AA030A2540ED1EB33 ();
// 0x000003C2 System.Int32 TouchScript.Clusters.Clusters2D::get_PointsCount()
extern void Clusters2D_get_PointsCount_mBF677C733DECDBC6962739E590389C2F025F55CC ();
// 0x000003C3 System.Single TouchScript.Clusters.Clusters2D::get_MinPointsDistance()
extern void Clusters2D_get_MinPointsDistance_mD26732D8478F77214D59FDECBF01EB4442929483 ();
// 0x000003C4 System.Void TouchScript.Clusters.Clusters2D::set_MinPointsDistance(System.Single)
extern void Clusters2D_set_MinPointsDistance_m4B86CB2CD3E0ACC4BB9234A4D7B11411876B48A0 ();
// 0x000003C5 System.Boolean TouchScript.Clusters.Clusters2D::get_HasClusters()
extern void Clusters2D_get_HasClusters_mBA702A02A3C3BE9A3FE4BBB8AF3D0621B60BEBDC ();
// 0x000003C6 System.Void TouchScript.Clusters.Clusters2D::.ctor()
extern void Clusters2D__ctor_mCADB6F800A1D4CA1BA4727E0BA81B16B730FF8BA ();
// 0x000003C7 UnityEngine.Vector2 TouchScript.Clusters.Clusters2D::GetCenterPosition(System.Int32)
extern void Clusters2D_GetCenterPosition_m03FBD8D278AE10F729DB7AE7199AFE28DF17594A ();
// 0x000003C8 UnityEngine.Vector2 TouchScript.Clusters.Clusters2D::GetPreviousCenterPosition(System.Int32)
extern void Clusters2D_GetPreviousCenterPosition_m30ABE6C4BD6D555A02DA4527E9DE51170B4BE39C ();
// 0x000003C9 System.Void TouchScript.Clusters.Clusters2D::AddPoint(TouchScript.Pointers.Pointer)
extern void Clusters2D_AddPoint_mA8A342BCACCE7512C68A2B992A240768540AA67C ();
// 0x000003CA System.Void TouchScript.Clusters.Clusters2D::AddPoints(System.Collections.Generic.IList`1<TouchScript.Pointers.Pointer>)
extern void Clusters2D_AddPoints_mFFC9AE302610192E482F60F924DEEDD94B3A4704 ();
// 0x000003CB System.Void TouchScript.Clusters.Clusters2D::RemovePoint(TouchScript.Pointers.Pointer)
extern void Clusters2D_RemovePoint_mE6EEA74F232CBFD4C8E1D9C937DBEA9C0CD873A8 ();
// 0x000003CC System.Void TouchScript.Clusters.Clusters2D::RemovePoints(System.Collections.Generic.IList`1<TouchScript.Pointers.Pointer>)
extern void Clusters2D_RemovePoints_mC7026EE918F6303C260B4E20A6B5B377E654DCD9 ();
// 0x000003CD System.Void TouchScript.Clusters.Clusters2D::RemoveAllPoints()
extern void Clusters2D_RemoveAllPoints_m3AA8421EF8952715882CB2BA7D22C8C814569D0F ();
// 0x000003CE System.Void TouchScript.Clusters.Clusters2D::Invalidate()
extern void Clusters2D_Invalidate_m73C187E405D1283490EF44A78D447899778D591B ();
// 0x000003CF System.Void TouchScript.Clusters.Clusters2D::distributePoints()
extern void Clusters2D_distributePoints_mBFC9CBD586FD066A05C468FC3DFA868E6FA82B5B ();
// 0x000003D0 System.Boolean TouchScript.Clusters.Clusters2D::checkClusters()
extern void Clusters2D_checkClusters_mAACC7B5ADB58AE0CF05CB5DD1CDE64647E32DB6D ();
// 0x000003D1 System.Void TouchScript.Clusters.Clusters2D::markDirty()
extern void Clusters2D_markDirty_m82F2FD009775A944CE669EA7E3E69518FC1EEC5D ();
// 0x000003D2 System.Void TouchScript.Clusters.Clusters2D::markClean()
extern void Clusters2D_markClean_mEFE319AAEEA2AD09D83B05FF337870DA03CA7A7C ();
// 0x000003D3 System.Boolean TouchScript.Behaviors.Transformer::get_EnableSmoothing()
extern void Transformer_get_EnableSmoothing_mC5702182753B8A5EA25E16A451220BBE5F1B21D2 ();
// 0x000003D4 System.Void TouchScript.Behaviors.Transformer::set_EnableSmoothing(System.Boolean)
extern void Transformer_set_EnableSmoothing_m28ABABE8AC86CCA60CAA56042E53E7FFEFE3E571 ();
// 0x000003D5 System.Single TouchScript.Behaviors.Transformer::get_SmoothingFactor()
extern void Transformer_get_SmoothingFactor_mD85F908B76497C4C80F12FE0989CC8A33050E973 ();
// 0x000003D6 System.Void TouchScript.Behaviors.Transformer::set_SmoothingFactor(System.Single)
extern void Transformer_set_SmoothingFactor_mA6951F93DFA491B0AEF059DD0B11C168FEC5C1D0 ();
// 0x000003D7 System.Single TouchScript.Behaviors.Transformer::get_PositionThreshold()
extern void Transformer_get_PositionThreshold_m767094602CB7040C65B8CD2543E8211714EFF583 ();
// 0x000003D8 System.Void TouchScript.Behaviors.Transformer::set_PositionThreshold(System.Single)
extern void Transformer_set_PositionThreshold_mC62D02B42E30773C5BA9152ECB49A1016407A169 ();
// 0x000003D9 System.Single TouchScript.Behaviors.Transformer::get_RotationThreshold()
extern void Transformer_get_RotationThreshold_mFACD9FCD08FA2B3BA7AA89F2D20129D5F9E920E5 ();
// 0x000003DA System.Void TouchScript.Behaviors.Transformer::set_RotationThreshold(System.Single)
extern void Transformer_set_RotationThreshold_m31F7BDFE97D88FACB97D9404AD45C6F1B868025C ();
// 0x000003DB System.Single TouchScript.Behaviors.Transformer::get_ScaleThreshold()
extern void Transformer_get_ScaleThreshold_m64AC23AD1A201F8878E9907152C8F40A78319E2D ();
// 0x000003DC System.Void TouchScript.Behaviors.Transformer::set_ScaleThreshold(System.Single)
extern void Transformer_set_ScaleThreshold_m1048B714954B61E89FAC09AC3C9F6EE84F8605FB ();
// 0x000003DD System.Boolean TouchScript.Behaviors.Transformer::get_AllowChangingFromOutside()
extern void Transformer_get_AllowChangingFromOutside_m6C449501D83568EF5BEA03992CCC01DCADC9C689 ();
// 0x000003DE System.Void TouchScript.Behaviors.Transformer::set_AllowChangingFromOutside(System.Boolean)
extern void Transformer_set_AllowChangingFromOutside_mA7AED5AE745D8262D654985F9D1931B8EFF0443B ();
// 0x000003DF System.Void TouchScript.Behaviors.Transformer::Awake()
extern void Transformer_Awake_mA1E34B533380507672643E50C7D06924C44CB621 ();
// 0x000003E0 System.Void TouchScript.Behaviors.Transformer::OnEnable()
extern void Transformer_OnEnable_mA83F58CA491C039C4415240550DEF105452E1B4F ();
// 0x000003E1 System.Void TouchScript.Behaviors.Transformer::OnDisable()
extern void Transformer_OnDisable_m970628CE73F498FEB5C1111968DE87F2AA857F27 ();
// 0x000003E2 System.Void TouchScript.Behaviors.Transformer::stateIdle()
extern void Transformer_stateIdle_m3CB811C714D52EA9F57772D1C96221B79A537344 ();
// 0x000003E3 System.Void TouchScript.Behaviors.Transformer::stateManual()
extern void Transformer_stateManual_mF243DBE2651C581CF1786A5CD8D2D3921EC89662 ();
// 0x000003E4 System.Void TouchScript.Behaviors.Transformer::stateAutomatic()
extern void Transformer_stateAutomatic_m6275809F62FCD3319C22095EAC02CE2BACBC87E7 ();
// 0x000003E5 System.Void TouchScript.Behaviors.Transformer::setState(TouchScript.Behaviors.Transformer_TransformerState)
extern void Transformer_setState_m5505F86414304369D59E4FE21C6D07C5C987C6F3 ();
// 0x000003E6 System.Void TouchScript.Behaviors.Transformer::update()
extern void Transformer_update_m10D5106985A3FDEB8C89B5C730DEDA146D447E82 ();
// 0x000003E7 System.Void TouchScript.Behaviors.Transformer::manualUpdate()
extern void Transformer_manualUpdate_mC4C86FE4B5C7917E19AAFA3B3C106B940AEC8FB6 ();
// 0x000003E8 System.Void TouchScript.Behaviors.Transformer::applyValues()
extern void Transformer_applyValues_mEBF52D62741932DDA891EB727881647ACC6C9635 ();
// 0x000003E9 System.Void TouchScript.Behaviors.Transformer::stateChangedHandler(System.Object,TouchScript.Gestures.GestureStateChangeEventArgs)
extern void Transformer_stateChangedHandler_mC8E4855CA01DFE753DE6C07C64EB8DB24E84141C ();
// 0x000003EA System.Void TouchScript.Behaviors.Transformer::frameFinishedHandler(System.Object,System.EventArgs)
extern void Transformer_frameFinishedHandler_m82CA35A51E7D4DDA0ADACEA29A7692A5984800E3 ();
// 0x000003EB System.Void TouchScript.Behaviors.Transformer::.ctor()
extern void Transformer__ctor_m3C8B68B03D81373779C2E797204147B768B054F6 ();
// 0x000003EC System.Void TouchScript.Behaviors.UI.OverHelper::add_Over(System.EventHandler)
extern void OverHelper_add_Over_m6AD9B0FAB0CF41F4E9C4F5E7CF2FC320492CAD9A ();
// 0x000003ED System.Void TouchScript.Behaviors.UI.OverHelper::remove_Over(System.EventHandler)
extern void OverHelper_remove_Over_m32887625DC3DC45A7352D81FF6D2242F6D511F10 ();
// 0x000003EE System.Void TouchScript.Behaviors.UI.OverHelper::add_Out(System.EventHandler)
extern void OverHelper_add_Out_m316642D4629206671DF5F65E948D7C640751762C ();
// 0x000003EF System.Void TouchScript.Behaviors.UI.OverHelper::remove_Out(System.EventHandler)
extern void OverHelper_remove_Out_mE066A1208350F5E4381D51326B41C15AEE02DE5C ();
// 0x000003F0 System.Void TouchScript.Behaviors.UI.OverHelper::OnEnable()
extern void OverHelper_OnEnable_m4D08AF12FC607F468D514C70DDB23C91130AD050 ();
// 0x000003F1 System.Void TouchScript.Behaviors.UI.OverHelper::OnDisable()
extern void OverHelper_OnDisable_m0016C981F9C458CABA450B6A4799310E98A53D28 ();
// 0x000003F2 System.Void TouchScript.Behaviors.UI.OverHelper::dispatchOver()
extern void OverHelper_dispatchOver_m499273C73319DD699BEAED6A2668FB6836CDA4F8 ();
// 0x000003F3 System.Void TouchScript.Behaviors.UI.OverHelper::dispatchOut()
extern void OverHelper_dispatchOut_m820D324CED5C47DC720634066F4098F419ED74B7 ();
// 0x000003F4 System.Void TouchScript.Behaviors.UI.OverHelper::pointersAddedHandler(System.Object,TouchScript.PointerEventArgs)
extern void OverHelper_pointersAddedHandler_mBBC6DF97BC2EE67231192F2A6A89E557BA8658CD ();
// 0x000003F5 System.Void TouchScript.Behaviors.UI.OverHelper::pointersUpdatedHandler(System.Object,TouchScript.PointerEventArgs)
extern void OverHelper_pointersUpdatedHandler_mA0CD6BEF32DB111E8F04450AEDCB86706B99397E ();
// 0x000003F6 System.Void TouchScript.Behaviors.UI.OverHelper::pointersReleasedHandler(System.Object,TouchScript.PointerEventArgs)
extern void OverHelper_pointersReleasedHandler_m80B3501AB1FF3597AA6DFFEF17240610AEB95A06 ();
// 0x000003F7 System.Void TouchScript.Behaviors.UI.OverHelper::pointersRemovedHandler(System.Object,TouchScript.PointerEventArgs)
extern void OverHelper_pointersRemovedHandler_m8DE8ACA874DE2DC36A0C01465B5912B72D910FCE ();
// 0x000003F8 System.Void TouchScript.Behaviors.UI.OverHelper::.ctor()
extern void OverHelper__ctor_m69A116634CB7056AB9F9CAA6444DAB6BB270ED6A ();
// 0x000003F9 TouchScript.Behaviors.Cursors.PointerCursor TouchScript.Behaviors.Cursors.CursorManager::get_MouseCursor()
extern void CursorManager_get_MouseCursor_m8197D6F0F1A6E72A63262674FD395F6FA2B77BBC ();
// 0x000003FA System.Void TouchScript.Behaviors.Cursors.CursorManager::set_MouseCursor(TouchScript.Behaviors.Cursors.PointerCursor)
extern void CursorManager_set_MouseCursor_m07AD66438486CC5BC2526BE468602169F2D191D5 ();
// 0x000003FB TouchScript.Behaviors.Cursors.PointerCursor TouchScript.Behaviors.Cursors.CursorManager::get_TouchCursor()
extern void CursorManager_get_TouchCursor_m301C6F2B7E25CA14A2B50F6443B92B049CC75B3E ();
// 0x000003FC System.Void TouchScript.Behaviors.Cursors.CursorManager::set_TouchCursor(TouchScript.Behaviors.Cursors.PointerCursor)
extern void CursorManager_set_TouchCursor_m614D34D7BF6080B0D6EB979AF0B0C60E800E1D9A ();
// 0x000003FD TouchScript.Behaviors.Cursors.PointerCursor TouchScript.Behaviors.Cursors.CursorManager::get_PenCursor()
extern void CursorManager_get_PenCursor_m70F62990BBD76FFD8678A6D2AEDEC75431B30C1C ();
// 0x000003FE System.Void TouchScript.Behaviors.Cursors.CursorManager::set_PenCursor(TouchScript.Behaviors.Cursors.PointerCursor)
extern void CursorManager_set_PenCursor_m81D030F43ECFC6CAA843A6DB4976BFB5F116AEC9 ();
// 0x000003FF TouchScript.Behaviors.Cursors.PointerCursor TouchScript.Behaviors.Cursors.CursorManager::get_ObjectCursor()
extern void CursorManager_get_ObjectCursor_m792339B60D9D3B5D348E2F5CE04F923E9C9E163B ();
// 0x00000400 System.Void TouchScript.Behaviors.Cursors.CursorManager::set_ObjectCursor(TouchScript.Behaviors.Cursors.PointerCursor)
extern void CursorManager_set_ObjectCursor_m13AACAF4147767DFF8EABECE1CD2051DF7A25E61 ();
// 0x00000401 System.Boolean TouchScript.Behaviors.Cursors.CursorManager::get_UseDPI()
extern void CursorManager_get_UseDPI_m535E943E58BD5C39F91F725587F613255CCB3229 ();
// 0x00000402 System.Void TouchScript.Behaviors.Cursors.CursorManager::set_UseDPI(System.Boolean)
extern void CursorManager_set_UseDPI_m35C93320BC0C06028DE5A5EA531E2F2F5623AFC2 ();
// 0x00000403 System.Single TouchScript.Behaviors.Cursors.CursorManager::get_CursorSize()
extern void CursorManager_get_CursorSize_m404BBE7772677EA68081E8416E631DE9AEB2CB82 ();
// 0x00000404 System.Void TouchScript.Behaviors.Cursors.CursorManager::set_CursorSize(System.Single)
extern void CursorManager_set_CursorSize_mC1FF2258219632E174E7BB4186B8E1DEEE496CA5 ();
// 0x00000405 System.UInt32 TouchScript.Behaviors.Cursors.CursorManager::get_CursorPixelSize()
extern void CursorManager_get_CursorPixelSize_m16A6B328FB3BD845977320F214ACC0DC0397311A ();
// 0x00000406 System.Void TouchScript.Behaviors.Cursors.CursorManager::set_CursorPixelSize(System.UInt32)
extern void CursorManager_set_CursorPixelSize_m0A1E252EB25265F71B60DDA484BC6C113ED5DD60 ();
// 0x00000407 System.Void TouchScript.Behaviors.Cursors.CursorManager::Awake()
extern void CursorManager_Awake_m4213D7E74CE31E03839FE06E32FEE4768ED999A3 ();
// 0x00000408 System.Void TouchScript.Behaviors.Cursors.CursorManager::OnEnable()
extern void CursorManager_OnEnable_m5BCCBB006C92293EB3EDF982C200AE00DE3D921F ();
// 0x00000409 System.Void TouchScript.Behaviors.Cursors.CursorManager::OnDisable()
extern void CursorManager_OnDisable_m321BA269EC2BB16E81431E2290C61A4B8323DAB3 ();
// 0x0000040A TouchScript.Behaviors.Cursors.PointerCursor TouchScript.Behaviors.Cursors.CursorManager::instantiateMouseProxy()
extern void CursorManager_instantiateMouseProxy_m21C2093B46BA2E40165D671CFD85ACA813168C92 ();
// 0x0000040B TouchScript.Behaviors.Cursors.PointerCursor TouchScript.Behaviors.Cursors.CursorManager::instantiateTouchProxy()
extern void CursorManager_instantiateTouchProxy_mCC18BB8A0E537E03CA30A7ADE0EF469DF76AF243 ();
// 0x0000040C TouchScript.Behaviors.Cursors.PointerCursor TouchScript.Behaviors.Cursors.CursorManager::instantiatePenProxy()
extern void CursorManager_instantiatePenProxy_m9DEDDDFAC3FF687907A9A856837D9F43E9CD0E4D ();
// 0x0000040D TouchScript.Behaviors.Cursors.PointerCursor TouchScript.Behaviors.Cursors.CursorManager::instantiateObjectProxy()
extern void CursorManager_instantiateObjectProxy_mEADCB9A2CEB82EF9EBF098B99CD72DBED91A7057 ();
// 0x0000040E System.Void TouchScript.Behaviors.Cursors.CursorManager::clearProxy(TouchScript.Behaviors.Cursors.PointerCursor)
extern void CursorManager_clearProxy_m328F4C863A6EF9EECBB4DDAD1F507AE344CF4903 ();
// 0x0000040F System.Void TouchScript.Behaviors.Cursors.CursorManager::updateCursorSize()
extern void CursorManager_updateCursorSize_mE00F038531F5B93C1D635108C1DA9293AD5169F4 ();
// 0x00000410 System.Void TouchScript.Behaviors.Cursors.CursorManager::pointersAddedHandler(System.Object,TouchScript.PointerEventArgs)
extern void CursorManager_pointersAddedHandler_m88A8C7BD8DFE2D5BD01C021442A3CDC99DC7648D ();
// 0x00000411 System.Void TouchScript.Behaviors.Cursors.CursorManager::pointersRemovedHandler(System.Object,TouchScript.PointerEventArgs)
extern void CursorManager_pointersRemovedHandler_m0BBB884BD33D081F42A85B803894C9198C8FFCCB ();
// 0x00000412 System.Void TouchScript.Behaviors.Cursors.CursorManager::pointersPressedHandler(System.Object,TouchScript.PointerEventArgs)
extern void CursorManager_pointersPressedHandler_mD1E032BFCA65F615739B87464A2D7CCDA2B677A5 ();
// 0x00000413 System.Void TouchScript.Behaviors.Cursors.CursorManager::PointersUpdatedHandler(System.Object,TouchScript.PointerEventArgs)
extern void CursorManager_PointersUpdatedHandler_m9A657FE7692B2CFE76C2FB879A09A1BCAA859E34 ();
// 0x00000414 System.Void TouchScript.Behaviors.Cursors.CursorManager::pointersReleasedHandler(System.Object,TouchScript.PointerEventArgs)
extern void CursorManager_pointersReleasedHandler_mA2B213F0BD020526492A306E48CCD867BD8A9163 ();
// 0x00000415 System.Void TouchScript.Behaviors.Cursors.CursorManager::pointersCancelledHandler(System.Object,TouchScript.PointerEventArgs)
extern void CursorManager_pointersCancelledHandler_m1605DF0439489565F47E836FF820DEADC20141A1 ();
// 0x00000416 System.Void TouchScript.Behaviors.Cursors.CursorManager::.ctor()
extern void CursorManager__ctor_mE7A21304B71D85DF9A1A0F6082C5DF7ABC56F92C ();
// 0x00000417 System.Void TouchScript.Behaviors.Cursors.MouseCursor::updateOnce(TouchScript.Pointers.IPointer)
extern void MouseCursor_updateOnce_m9FB8A17B1CAC8407A13863D8B94B4E97C583AC57 ();
// 0x00000418 System.Void TouchScript.Behaviors.Cursors.MouseCursor::generateText(TouchScript.Pointers.MousePointer,System.Text.StringBuilder)
extern void MouseCursor_generateText_mE93383EBFFAC5B95BF8D13DAF774895FA85EF2C6 ();
// 0x00000419 System.Boolean TouchScript.Behaviors.Cursors.MouseCursor::textIsVisible()
extern void MouseCursor_textIsVisible_m06D3CE5E28774B80CF6114EE67446A42F77AF253 ();
// 0x0000041A System.UInt32 TouchScript.Behaviors.Cursors.MouseCursor::gethash(TouchScript.Pointers.MousePointer)
extern void MouseCursor_gethash_m235E1B2A65842B5698D769283EDA8CF0E04B7472 ();
// 0x0000041B System.Void TouchScript.Behaviors.Cursors.MouseCursor::.ctor()
extern void MouseCursor__ctor_mD4B4DFF7C09F9DF65C1846A8D49348C66166DDC3 ();
// 0x0000041C System.Void TouchScript.Behaviors.Cursors.ObjectCursor::generateText(TouchScript.Pointers.ObjectPointer,System.Text.StringBuilder)
extern void ObjectCursor_generateText_m40ED348864D90A58BC80A57E581889630F0C7C6D ();
// 0x0000041D System.Boolean TouchScript.Behaviors.Cursors.ObjectCursor::textIsVisible()
extern void ObjectCursor_textIsVisible_m8AD670831940CF25D368AF0985C00C48FE178E4D ();
// 0x0000041E System.UInt32 TouchScript.Behaviors.Cursors.ObjectCursor::gethash(TouchScript.Pointers.ObjectPointer)
extern void ObjectCursor_gethash_m76BBC3809DEDB3C4FF02651F5DA02B8A08720A67 ();
// 0x0000041F System.Void TouchScript.Behaviors.Cursors.ObjectCursor::.ctor()
extern void ObjectCursor__ctor_m02244E0C1E2BDA048483A5F0DF33EE8F56F3D06A ();
// 0x00000420 System.Void TouchScript.Behaviors.Cursors.PenCursor::updateOnce(TouchScript.Pointers.IPointer)
extern void PenCursor_updateOnce_m0EC5F072AFEFFE5A81582569BBF4C9936770E7D1 ();
// 0x00000421 System.Void TouchScript.Behaviors.Cursors.PenCursor::generateText(TouchScript.Pointers.PenPointer,System.Text.StringBuilder)
extern void PenCursor_generateText_m6CAE123E463E4F05AA3D4D37E19E77E8FE106910 ();
// 0x00000422 System.Boolean TouchScript.Behaviors.Cursors.PenCursor::textIsVisible()
extern void PenCursor_textIsVisible_mF630AC59946E3183688D9D209CD97A0DA2E6B5E6 ();
// 0x00000423 System.UInt32 TouchScript.Behaviors.Cursors.PenCursor::gethash(TouchScript.Pointers.PenPointer)
extern void PenCursor_gethash_m2C1A2FA2292456E534D5367C279107DD7043D380 ();
// 0x00000424 System.Void TouchScript.Behaviors.Cursors.PenCursor::.ctor()
extern void PenCursor__ctor_m91E36D14FCA8D1DEBB56831E6E1E9EAB90DD5A47 ();
// 0x00000425 System.Void TouchScript.Behaviors.Cursors.TextPointerCursor`1::updateOnce(TouchScript.Pointers.IPointer)
// 0x00000426 System.Void TouchScript.Behaviors.Cursors.TextPointerCursor`1::generateText(T,System.Text.StringBuilder)
// 0x00000427 System.Boolean TouchScript.Behaviors.Cursors.TextPointerCursor`1::textIsVisible()
// 0x00000428 System.UInt32 TouchScript.Behaviors.Cursors.TextPointerCursor`1::gethash(T)
// 0x00000429 System.UInt32 TouchScript.Behaviors.Cursors.TextPointerCursor`1::getPointerHash(TouchScript.Pointers.IPointer)
// 0x0000042A System.Void TouchScript.Behaviors.Cursors.TextPointerCursor`1::.ctor()
// 0x0000042B System.Void TouchScript.Behaviors.Cursors.TextPointerCursor`1::.cctor()
// 0x0000042C System.Single TouchScript.Behaviors.Cursors.PointerCursor::get_Size()
extern void PointerCursor_get_Size_m3B277AE69754EE0E368C041CCA93C9DC948A85E8 ();
// 0x0000042D System.Void TouchScript.Behaviors.Cursors.PointerCursor::set_Size(System.Single)
extern void PointerCursor_set_Size_m4B0954262A8FCE424CFD5F6C30B49340927A8CF3 ();
// 0x0000042E System.Void TouchScript.Behaviors.Cursors.PointerCursor::Init(UnityEngine.RectTransform,TouchScript.Pointers.IPointer)
extern void PointerCursor_Init_mBD30377900DD1F0F88FBEBAACFDE3D1E21A513AE ();
// 0x0000042F System.Void TouchScript.Behaviors.Cursors.PointerCursor::UpdatePointer(TouchScript.Pointers.IPointer)
extern void PointerCursor_UpdatePointer_m4211256990695D8DA5C61B2AA7C63F56C210A456 ();
// 0x00000430 System.Void TouchScript.Behaviors.Cursors.PointerCursor::SetState(TouchScript.Pointers.IPointer,TouchScript.Behaviors.Cursors.PointerCursor_CursorState,System.Object)
extern void PointerCursor_SetState_mE7A83F1F53EE8B47766F51B4556D9B0115973FBA ();
// 0x00000431 System.Void TouchScript.Behaviors.Cursors.PointerCursor::Hide()
extern void PointerCursor_Hide_mAE891F87391F0872D8F0FF2D48BDDA5D12974FC3 ();
// 0x00000432 System.Void TouchScript.Behaviors.Cursors.PointerCursor::Awake()
extern void PointerCursor_Awake_mAF667615DA7BACEB16776728A5DED5E08359BF43 ();
// 0x00000433 System.Void TouchScript.Behaviors.Cursors.PointerCursor::hide()
extern void PointerCursor_hide_mF0934BF39377528582AE9A55A680A25D04002F45 ();
// 0x00000434 System.Void TouchScript.Behaviors.Cursors.PointerCursor::show()
extern void PointerCursor_show_m27D2618E805936B211771E79094A5647104EBDED ();
// 0x00000435 System.Void TouchScript.Behaviors.Cursors.PointerCursor::updateOnce(TouchScript.Pointers.IPointer)
extern void PointerCursor_updateOnce_mA34AF2C967654F70029DFCD3BE525AC391DE0965 ();
// 0x00000436 System.Void TouchScript.Behaviors.Cursors.PointerCursor::update(TouchScript.Pointers.IPointer)
extern void PointerCursor_update_m581222C6269ECB86C175D219A77B8AAD0CF700F5 ();
// 0x00000437 System.UInt32 TouchScript.Behaviors.Cursors.PointerCursor::getPointerHash(TouchScript.Pointers.IPointer)
extern void PointerCursor_getPointerHash_mE13CC1C1CDEBFF64CD28536B1A47773594EF3E2B ();
// 0x00000438 System.Void TouchScript.Behaviors.Cursors.PointerCursor::.ctor()
extern void PointerCursor__ctor_m43144A36F3CD04E7ECD4DA9480137FE927A09374 ();
// 0x00000439 System.Void TouchScript.Behaviors.Cursors.TouchCursor::generateText(TouchScript.Pointers.TouchPointer,System.Text.StringBuilder)
extern void TouchCursor_generateText_m8A065D5B018DA1F47A60A77B6C8E8C1B7D8C7D0D ();
// 0x0000043A System.Boolean TouchScript.Behaviors.Cursors.TouchCursor::textIsVisible()
extern void TouchCursor_textIsVisible_m62766D562F01A08F458ECDA5546D34D4F4B0484F ();
// 0x0000043B System.UInt32 TouchScript.Behaviors.Cursors.TouchCursor::gethash(TouchScript.Pointers.TouchPointer)
extern void TouchCursor_gethash_m2D2067E73C4E1D5D99FCA2B724935F4152D27528 ();
// 0x0000043C System.Void TouchScript.Behaviors.Cursors.TouchCursor::.ctor()
extern void TouchCursor__ctor_m17EC5DA47060D2D2766F27C896FA453D0168F595 ();
// 0x0000043D UnityEngine.Texture2D TouchScript.Behaviors.Cursors.UI.GradientTexture::Generate()
extern void GradientTexture_Generate_m3E692617E2E3A28883E1E6D81DD438009F7412CB ();
// 0x0000043E System.Void TouchScript.Behaviors.Cursors.UI.GradientTexture::Start()
extern void GradientTexture_Start_m536C9CC190293CD96F17F32D96381A9B32FB70F9 ();
// 0x0000043F System.Void TouchScript.Behaviors.Cursors.UI.GradientTexture::OnValidate()
extern void GradientTexture_OnValidate_m1B637E19835BE3EA384961E8F8F06261EB3DFFC2 ();
// 0x00000440 System.Void TouchScript.Behaviors.Cursors.UI.GradientTexture::refresh()
extern void GradientTexture_refresh_m743EFBCECF5A162E64857F1FE15B17622A5CE1DB ();
// 0x00000441 System.Void TouchScript.Behaviors.Cursors.UI.GradientTexture::apply()
extern void GradientTexture_apply_mE5D364BFE2A3384D15185561E7483D84D0DDD789 ();
// 0x00000442 System.Void TouchScript.Behaviors.Cursors.UI.GradientTexture::.ctor()
extern void GradientTexture__ctor_m7CCD2EEC1A548EF009DA2605ACB531F82D2362F9 ();
// 0x00000443 System.Void TouchScript.Behaviors.Cursors.UI.GradientTexture::.cctor()
extern void GradientTexture__cctor_m26F2D55F9E40D604BF9C3651F0F29D2BCE4F274B ();
// 0x00000444 System.Void TouchScript.Behaviors.Cursors.UI.TextureSwitch::Show()
extern void TextureSwitch_Show_m8E6A3566324E923E17118DA0CA8CE558B6B325DF ();
// 0x00000445 System.Void TouchScript.Behaviors.Cursors.UI.TextureSwitch::Hide()
extern void TextureSwitch_Hide_m65862F55643E025A89098AE6895D674CDBB6A15C ();
// 0x00000446 System.Void TouchScript.Behaviors.Cursors.UI.TextureSwitch::Awake()
extern void TextureSwitch_Awake_m83F9DD0928DC030F3B4A04B2E0EBC222F646A603 ();
// 0x00000447 System.Void TouchScript.Behaviors.Cursors.UI.TextureSwitch::.ctor()
extern void TextureSwitch__ctor_m8A4554509B623D3CCE933FD4D2BF7D9A4D8345B7 ();
// 0x00000448 System.Void TouchScript.Examples.Checker::OnEnable()
extern void Checker_OnEnable_m22244786833AE2A7ABBA4B74FB0E9DDD899816AC ();
// 0x00000449 System.Void TouchScript.Examples.Checker::OnDisable()
extern void Checker_OnDisable_mEBC4D1CF0936580B82587E0C46D2886B53C4ED70 ();
// 0x0000044A System.Void TouchScript.Examples.Checker::transformStartedHandler(System.Object,System.EventArgs)
extern void Checker_transformStartedHandler_mE62DA697B8E72515698C0CE8759C6F63FF181781 ();
// 0x0000044B System.Void TouchScript.Examples.Checker::transformCompletedHandler(System.Object,System.EventArgs)
extern void Checker_transformCompletedHandler_mD19A952A443B68D76A26FF04F6AD78E215B9A6DB ();
// 0x0000044C System.Void TouchScript.Examples.Checker::.ctor()
extern void Checker__ctor_m73ABEC26E306920C31F1675E7C22DAEECF6026EC ();
// 0x0000044D System.Collections.IEnumerator TouchScript.Examples.KillMe::Start()
extern void KillMe_Start_mE5BDF0FE7A7CBDD4EC3F919BB363CC26D002F6E8 ();
// 0x0000044E System.Void TouchScript.Examples.KillMe::.ctor()
extern void KillMe__ctor_m0CE80246905A917389D52D550F3592A61152388A ();
// 0x0000044F System.Void TouchScript.Examples.Runner::LoadLevel(System.String)
extern void Runner_LoadLevel_mFC4DDAD9020BF050811A7C7433874C8523F73298 ();
// 0x00000450 System.Void TouchScript.Examples.Runner::LoadNextLevel()
extern void Runner_LoadNextLevel_m47F81F83B6F0C42EC410EC06051512B9FA554D13 ();
// 0x00000451 System.Void TouchScript.Examples.Runner::LoadPreviousLevel()
extern void Runner_LoadPreviousLevel_mEB6B45834AFA837C925564A1F63BA621C52ED2F8 ();
// 0x00000452 System.Void TouchScript.Examples.Runner::Start()
extern void Runner_Start_mC5A19E3D1C9F440C596D0A7D25A12995AD97FE5A ();
// 0x00000453 System.Void TouchScript.Examples.Runner::OnDestroy()
extern void Runner_OnDestroy_m9B8B33DF5B75E285FC0212C76AAA94F14BA1346A ();
// 0x00000454 System.Void TouchScript.Examples.Runner::Update()
extern void Runner_Update_m6CB9E27B8BEA633F680A8882CA75EE37C5F1ADC4 ();
// 0x00000455 System.Void TouchScript.Examples.Runner::sceneLoadedHandler(UnityEngine.SceneManagement.Scene,UnityEngine.SceneManagement.LoadSceneMode)
extern void Runner_sceneLoadedHandler_m7BF24C8CBE8754581A2E7E0FAB3B07CD64E2866B ();
// 0x00000456 System.Collections.IEnumerator TouchScript.Examples.Runner::resetUILayer()
extern void Runner_resetUILayer_m859A7F3A0AFFC89A654424C1132CC804F815A92D ();
// 0x00000457 System.Void TouchScript.Examples.Runner::.ctor()
extern void Runner__ctor_m2246327898122ACAFD9AF8F6B199436213B1D290 ();
// 0x00000458 System.Void TouchScript.Examples.Tap.Break::OnEnable()
extern void Break_OnEnable_m006D3897652AED2D8986354CEC878A455E23C427 ();
// 0x00000459 System.Void TouchScript.Examples.Tap.Break::OnDisable()
extern void Break_OnDisable_mFCF3DEAC57CF47B70CEDB2A8EE5F5090F8DC6065 ();
// 0x0000045A System.Void TouchScript.Examples.Tap.Break::Update()
extern void Break_Update_mECFA71DC2C4E68D6F4B94BC8F4607BA945279311 ();
// 0x0000045B System.Void TouchScript.Examples.Tap.Break::startGrowing()
extern void Break_startGrowing_m95A6EE85B94F1EEFF104527692755C3ED3FF396B ();
// 0x0000045C System.Void TouchScript.Examples.Tap.Break::stopGrowing()
extern void Break_stopGrowing_m30D6ECA6195CC2B1536DE62111FE06617BEC7AD8 ();
// 0x0000045D System.Void TouchScript.Examples.Tap.Break::pressedHandler(System.Object,System.EventArgs)
extern void Break_pressedHandler_m83AA6C64261B26DC3625C5746DB9B7125D43A849 ();
// 0x0000045E System.Void TouchScript.Examples.Tap.Break::longPressedHandler(System.Object,TouchScript.Gestures.GestureStateChangeEventArgs)
extern void Break_longPressedHandler_mDE8E6E49E07AA36405267990A58666B39C3419EE ();
// 0x0000045F System.Void TouchScript.Examples.Tap.Break::.ctor()
extern void Break__ctor_mC9F442C861DAC9C10FE50298F6DF8590CB295490 ();
// 0x00000460 System.Void TouchScript.Examples.Tap.Kick::OnEnable()
extern void Kick_OnEnable_mAE00729C20D076A9CA61A9A95E105379BDB00150 ();
// 0x00000461 System.Void TouchScript.Examples.Tap.Kick::OnDisable()
extern void Kick_OnDisable_m1DAF77464C7E028F3085C610BA47826151AACF0B ();
// 0x00000462 System.Void TouchScript.Examples.Tap.Kick::tappedHandler(System.Object,System.EventArgs)
extern void Kick_tappedHandler_m2B3670F4602E00055F97B00A053E57B3173FD984 ();
// 0x00000463 System.Void TouchScript.Examples.Tap.Kick::.ctor()
extern void Kick__ctor_m0164DCC0528026FAB78D58DC58C4DA231D5DC485 ();
// 0x00000464 System.Void TouchScript.Examples.Tap.Spawn::OnEnable()
extern void Spawn_OnEnable_m26ED9352BC75E5C5A4CC294680E3D3199E3BB29F ();
// 0x00000465 System.Void TouchScript.Examples.Tap.Spawn::OnDisable()
extern void Spawn_OnDisable_mFAB9F4D9475CE486A2ECF336EC08B3DB7DD970AF ();
// 0x00000466 System.Void TouchScript.Examples.Tap.Spawn::tappedHandler(System.Object,System.EventArgs)
extern void Spawn_tappedHandler_m884F5683C7CD58149CCD19DDBD0508ED5A7A5C85 ();
// 0x00000467 System.Void TouchScript.Examples.Tap.Spawn::.ctor()
extern void Spawn__ctor_mFCEC573B10C9E8FE72C55D2F3798536A85E7D568 ();
// 0x00000468 System.Void TouchScript.Examples.RawInput.Ball::Update()
extern void Ball_Update_m2C132F2380CD745AA05CBAAF0FF2CDA469DB1D09 ();
// 0x00000469 System.Void TouchScript.Examples.RawInput.Ball::.ctor()
extern void Ball__ctor_mCD0981C29298D58AF723800AF665333B883C412C ();
// 0x0000046A System.Void TouchScript.Examples.RawInput.Spawner::OnEnable()
extern void Spawner_OnEnable_mB5217B5CB8CC86C7A417B37CC04D7A4C4A8E2014 ();
// 0x0000046B System.Void TouchScript.Examples.RawInput.Spawner::OnDisable()
extern void Spawner_OnDisable_mCB4976CB03B28BA37A14C0A024E384BB2C07BD19 ();
// 0x0000046C System.Void TouchScript.Examples.RawInput.Spawner::spawnPrefabAt(UnityEngine.Vector2)
extern void Spawner_spawnPrefabAt_mCAB9CA8B03F9E0F51273869275BF1DC50386D311 ();
// 0x0000046D System.Void TouchScript.Examples.RawInput.Spawner::pointersPressedHandler(System.Object,TouchScript.PointerEventArgs)
extern void Spawner_pointersPressedHandler_mFFE66D14DA544A9EB5C7C70F4CE66536489F611D ();
// 0x0000046E System.Void TouchScript.Examples.RawInput.Spawner::.ctor()
extern void Spawner__ctor_m8221222A41C7DDB7B392C6F651B99CA840299745 ();
// 0x0000046F System.Void TouchScript.Examples.Portal.Planet::Fall()
extern void Planet_Fall_m5034351FC0529F74E8171FE47B57AE6C7368BF96 ();
// 0x00000470 System.Void TouchScript.Examples.Portal.Planet::OnEnable()
extern void Planet_OnEnable_mB370964517A63ED0B88115DF1FA8FBDEC723E5ED ();
// 0x00000471 System.Void TouchScript.Examples.Portal.Planet::OnDisable()
extern void Planet_OnDisable_mCADBF0F8BD2D21392241E504DC1AC19A16D3869F ();
// 0x00000472 System.Void TouchScript.Examples.Portal.Planet::Update()
extern void Planet_Update_mF92E212D57BBCEA1EFCA7E3B777B0CBE36724140 ();
// 0x00000473 System.Void TouchScript.Examples.Portal.Planet::pressedhandler(System.Object,System.EventArgs)
extern void Planet_pressedhandler_m3E405FCAD05BA3A92F07CE6FE349089C248386F1 ();
// 0x00000474 System.Void TouchScript.Examples.Portal.Planet::releasedHandler(System.Object,System.EventArgs)
extern void Planet_releasedHandler_m90AF6C209A555501291DC95BAA0363175CF07C97 ();
// 0x00000475 System.Void TouchScript.Examples.Portal.Planet::.ctor()
extern void Planet__ctor_m6B8E568A83270B47C675EA8222206CE45F4CCF63 ();
// 0x00000476 System.Void TouchScript.Examples.Portal.Rotator::Update()
extern void Rotator_Update_m62C8E67861D2D19ABD3525DA159A8A46BF440D22 ();
// 0x00000477 System.Void TouchScript.Examples.Portal.Rotator::.ctor()
extern void Rotator__ctor_m293D13755A8E6CD52E5224C5E9ED1532A9322706 ();
// 0x00000478 System.Void TouchScript.Examples.Portal.Spawner::OnEnable()
extern void Spawner_OnEnable_m114A4C299CCF79650C49B98F8754DB02F9D502BF ();
// 0x00000479 System.Void TouchScript.Examples.Portal.Spawner::OnDisable()
extern void Spawner_OnDisable_m7F07B9BF09D3F2F5A85F4721576373C678D7779C ();
// 0x0000047A System.Void TouchScript.Examples.Portal.Spawner::pressHandler(System.Object,System.EventArgs)
extern void Spawner_pressHandler_mBBE08D2BE67D094A478BC2FA7B4810BD401ABB42 ();
// 0x0000047B System.Void TouchScript.Examples.Portal.Spawner::.ctor()
extern void Spawner__ctor_mD64033E133CAD768B0645FA11498F9D3BA9FF59E ();
// 0x0000047C System.Void TouchScript.Examples.Portal.Vortex::OnTriggerEnter(UnityEngine.Collider)
extern void Vortex_OnTriggerEnter_m87E6A0FDA5AE85A3D6A26531A5D28526034DE71B ();
// 0x0000047D System.Void TouchScript.Examples.Portal.Vortex::.ctor()
extern void Vortex__ctor_m072F81E0E39792BD61921C630BA65BDD0C0D58B2 ();
// 0x0000047E System.Void TouchScript.Examples.UI.SetColor::Set(System.Int32)
extern void SetColor_Set_m9A5D29AB7B5B3F91E1007480C84CBA2673A18952 ();
// 0x0000047F System.Void TouchScript.Examples.UI.SetColor::.ctor()
extern void SetColor__ctor_m74A69FEC184FE253F14F7D9947F8BF01F3DEEF7E ();
// 0x00000480 System.Void TouchScript.Examples.Photos.Container::Add()
extern void Container_Add_mA258161D0E40B99B1B62060B38B60CC17395C80E ();
// 0x00000481 System.Void TouchScript.Examples.Photos.Container::Start()
extern void Container_Start_mD82ECB6FE52E82882C1C82888AE880706E812772 ();
// 0x00000482 System.Void TouchScript.Examples.Photos.Container::initChild(UnityEngine.Transform)
extern void Container_initChild_m3945E71855569BDCAA8A93F93449E13B6E679CB7 ();
// 0x00000483 System.Void TouchScript.Examples.Photos.Container::pressedHandler(System.Object,System.EventArgs)
extern void Container_pressedHandler_m3FB594D9449A10DD2186E4C5B6F68DCF5D670554 ();
// 0x00000484 System.Void TouchScript.Examples.Photos.Container::.ctor()
extern void Container__ctor_m7CE40023605E7BD47BB0E4B97C0F31C5F5D179AA ();
// 0x00000485 System.Void TouchScript.Examples.Multiuser.Logo::OnEnable()
extern void Logo_OnEnable_m4856654646850CD5F2DB28078B1950F01B2CCD6F ();
// 0x00000486 System.Void TouchScript.Examples.Multiuser.Logo::OnDisable()
extern void Logo_OnDisable_m87005B514999E08C6BE533786E171A8CA70693BF ();
// 0x00000487 System.Void TouchScript.Examples.Multiuser.Logo::changeColor()
extern void Logo_changeColor_mA2B900924A48C17E55A5A7D37CE62BED2A248582 ();
// 0x00000488 System.Void TouchScript.Examples.Multiuser.Logo::tappedHandler(System.Object,System.EventArgs)
extern void Logo_tappedHandler_mD4CD4B9518C84A90D4A98003FF1FA0A4B1905239 ();
// 0x00000489 System.Void TouchScript.Examples.Multiuser.Logo::.ctor()
extern void Logo__ctor_m2EDE7CF8D140795394B85CA0CE1116D40CA39F5A ();
// 0x0000048A System.Void TouchScript.Examples.Multiuser.Logo::.cctor()
extern void Logo__cctor_mBE5C8AC1FAE5C2DEACE6518F70CAAA597579E1CE ();
// 0x0000048B System.Void TouchScript.Examples.Cube.CustomPointerProxy::updateOnce(TouchScript.Pointers.IPointer)
extern void CustomPointerProxy_updateOnce_m926AA073292C5F78E51B6D202E1B3CBF153E4346 ();
// 0x0000048C System.Void TouchScript.Examples.Cube.CustomPointerProxy::.ctor()
extern void CustomPointerProxy__ctor_m9F88E1950A33A574C22F4D1DCB72F9815DA9081A ();
// 0x0000048D System.Void TouchScript.Examples.Cube.Init::Start()
extern void Init_Start_m4C5A39CF798E77ECEE29D701D8A297F11690A3E8 ();
// 0x0000048E System.Void TouchScript.Examples.Cube.Init::.ctor()
extern void Init__ctor_m3D89B79C83258B26A693F00A1CF96E6FEE0AA634 ();
// 0x0000048F System.Boolean TouchScript.Examples.Cube.LayerDelegate::ShouldReceivePointer(TouchScript.Layers.TouchLayer,TouchScript.Pointers.IPointer)
extern void LayerDelegate_ShouldReceivePointer_mA445F4F58298632176766AC37C3C56F7CC534343 ();
// 0x00000490 System.Void TouchScript.Examples.Cube.LayerDelegate::.ctor()
extern void LayerDelegate__ctor_mA37893208937F1311649FC7FF23541B3D9856279 ();
// 0x00000491 System.Boolean TouchScript.Examples.Cube.RedirectInput::CancelPointer(TouchScript.Pointers.Pointer,System.Boolean)
extern void RedirectInput_CancelPointer_m6F551316AEC8F9A697EB0A5A135543F1D71A28BE ();
// 0x00000492 System.Void TouchScript.Examples.Cube.RedirectInput::OnEnable()
extern void RedirectInput_OnEnable_m07AF117B3678F212A9F235B3DD41F93C85F80C7F ();
// 0x00000493 System.Void TouchScript.Examples.Cube.RedirectInput::OnDisable()
extern void RedirectInput_OnDisable_m59CA7BD9E349507B5BD41DEC7F821900F71B28A5 ();
// 0x00000494 UnityEngine.Vector2 TouchScript.Examples.Cube.RedirectInput::processCoords(UnityEngine.Vector2)
extern void RedirectInput_processCoords_m894056053BEB2CD0A21E7DEE56B6A0701F65A991 ();
// 0x00000495 System.Void TouchScript.Examples.Cube.RedirectInput::pointerPressedHandler(System.Object,TouchScript.Gestures.MetaGestureEventArgs)
extern void RedirectInput_pointerPressedHandler_m049EE10FEDB2AAB088B74D13A3DD726A38C3915F ();
// 0x00000496 System.Void TouchScript.Examples.Cube.RedirectInput::pointerUpdatedHandler(System.Object,TouchScript.Gestures.MetaGestureEventArgs)
extern void RedirectInput_pointerUpdatedHandler_mD3110F1358718676C06DE530B920BDDF555C791C ();
// 0x00000497 System.Void TouchScript.Examples.Cube.RedirectInput::pointerReleasedHandler(System.Object,TouchScript.Gestures.MetaGestureEventArgs)
extern void RedirectInput_pointerReleasedHandler_mA24C87DAFF62E2F94ED6C9ECED7D426A5A204EA0 ();
// 0x00000498 System.Void TouchScript.Examples.Cube.RedirectInput::pointerCancelledhandler(System.Object,TouchScript.Gestures.MetaGestureEventArgs)
extern void RedirectInput_pointerCancelledhandler_m7077BC6BBEE5BAA3D5D3877E35D90D50FE0DAA65 ();
// 0x00000499 System.Void TouchScript.Examples.Cube.RedirectInput::.ctor()
extern void RedirectInput__ctor_mDCA7A3D46531FF97588A75EAA9C65677917B7777 ();
// 0x0000049A UnityEngine.Color TouchScript.Examples.Colors.Circle::Kill()
extern void Circle_Kill_m1616E2262DE24784E359F0DA0923E34323B72530 ();
// 0x0000049B System.Void TouchScript.Examples.Colors.Circle::OnTriggerEnter2D(UnityEngine.Collider2D)
extern void Circle_OnTriggerEnter2D_m01A2B25AEFC7C5A7E0BC559C667856AFF4A882BD ();
// 0x0000049C System.Void TouchScript.Examples.Colors.Circle::.ctor()
extern void Circle__ctor_m8649F15CDC90782DE8322CF712E21DCEC85C6904 ();
// 0x0000049D System.Void TouchScript.Examples.Colors.Colors::Start()
extern void Colors_Start_mA5A9477713B64D37F1BED03A0278B4A903D4C5B3 ();
// 0x0000049E System.Void TouchScript.Examples.Colors.Colors::.ctor()
extern void Colors__ctor_m1B959D35C1D96B3BC8E8C8483A5086593CF0F13D ();
// 0x0000049F System.Void TouchScript.Examples.Checkers.Board::OnEnable()
extern void Board_OnEnable_m0DF08F0E6B12C2D889C54F9DC39922D37711DD6E ();
// 0x000004A0 System.Void TouchScript.Examples.Checkers.Board::OnDisable()
extern void Board_OnDisable_m17A2EAE25E83D8B1A36FF26352B8C4783BE758F9 ();
// 0x000004A1 System.Void TouchScript.Examples.Checkers.Board::transformedHandler(System.Object,System.EventArgs)
extern void Board_transformedHandler_m842BA6FF27628E9B954AAA603250446D44FEF658 ();
// 0x000004A2 System.Void TouchScript.Examples.Checkers.Board::.ctor()
extern void Board__ctor_m1B1EA536B46C92AC6158470A0422946E1D15E9C6 ();
// 0x000004A3 System.Void TouchScript.Examples.Checkers.Exclusive::Awake()
extern void Exclusive_Awake_mAB9E78C0FD300DB544FF1BA587FA896216494F9E ();
// 0x000004A4 System.Void TouchScript.Examples.Checkers.Exclusive::Update()
extern void Exclusive_Update_mAE431335FEA9CB4797CABBF7C00BFC742AF45B76 ();
// 0x000004A5 System.Boolean TouchScript.Examples.Checkers.Exclusive::ShouldBegin(TouchScript.Gestures.Gesture)
extern void Exclusive_ShouldBegin_m9C153226AD0B01A8752A012D99A450283D26CFC6 ();
// 0x000004A6 System.Boolean TouchScript.Examples.Checkers.Exclusive::ShouldReceivePointer(TouchScript.Gestures.Gesture,TouchScript.Pointers.Pointer)
extern void Exclusive_ShouldReceivePointer_mD422EDB6D4B872BB89AB8B1208966526D3A0F619 ();
// 0x000004A7 System.Boolean TouchScript.Examples.Checkers.Exclusive::ShouldRecognizeSimultaneously(TouchScript.Gestures.Gesture,TouchScript.Gestures.Gesture)
extern void Exclusive_ShouldRecognizeSimultaneously_m53CCEDF7F16425239DA4C880647BE601EF12C62F ();
// 0x000004A8 System.Void TouchScript.Examples.Checkers.Exclusive::.ctor()
extern void Exclusive__ctor_m108DB8E55D4845DDD2B338748B2E488789084E20 ();
// 0x000004A9 System.Void TouchScript.Examples.CameraControl.CameraController::Awake()
extern void CameraController_Awake_m85AF24A3CCC6915C66FABEA503A2976E6BAD9449 ();
// 0x000004AA System.Void TouchScript.Examples.CameraControl.CameraController::OnEnable()
extern void CameraController_OnEnable_m1E89CBA6F598EB1DDCC6A905D269858151477CE2 ();
// 0x000004AB System.Void TouchScript.Examples.CameraControl.CameraController::OnDisable()
extern void CameraController_OnDisable_m16B61DEA3CB5A35FB2ECAFF2948CE748889F9666 ();
// 0x000004AC System.Void TouchScript.Examples.CameraControl.CameraController::manipulationTransformedHandler(System.Object,System.EventArgs)
extern void CameraController_manipulationTransformedHandler_mCD54321C80F6CB65A26B9D5A4FAC9FCAD46FADEA ();
// 0x000004AD System.Void TouchScript.Examples.CameraControl.CameraController::twoFingerTransformHandler(System.Object,System.EventArgs)
extern void CameraController_twoFingerTransformHandler_m153B637246A7E21731B9A64CA237E3E17D4A3162 ();
// 0x000004AE System.Void TouchScript.Examples.CameraControl.CameraController::.ctor()
extern void CameraController__ctor_m6D25D3B16C45FD6D2CE3D5A1E689AB0B6B10ECE9 ();
// 0x000004AF System.Void ShowMe_<Start>d__0::.ctor(System.Int32)
extern void U3CStartU3Ed__0__ctor_m68F3546D7663144C02866F962D17C9440A1920C4 ();
// 0x000004B0 System.Void ShowMe_<Start>d__0::System.IDisposable.Dispose()
extern void U3CStartU3Ed__0_System_IDisposable_Dispose_m6DBBCE6635B27796EE108B8237956FB1AB96F262 ();
// 0x000004B1 System.Boolean ShowMe_<Start>d__0::MoveNext()
extern void U3CStartU3Ed__0_MoveNext_mDE76B03C8B83AFD1798DDC310E89DAD2E668A2E3 ();
// 0x000004B2 System.Object ShowMe_<Start>d__0::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CStartU3Ed__0_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m593685FD537795C7D73B1B54681041E4210F3F34 ();
// 0x000004B3 System.Void ShowMe_<Start>d__0::System.Collections.IEnumerator.Reset()
extern void U3CStartU3Ed__0_System_Collections_IEnumerator_Reset_m67477E6B6C474BE023CE7D2B4F2B48BD44A42A09 ();
// 0x000004B4 System.Object ShowMe_<Start>d__0::System.Collections.IEnumerator.get_Current()
extern void U3CStartU3Ed__0_System_Collections_IEnumerator_get_Current_m8EB8E75B6E07249830AF0404BD1CE6D546BB49A3 ();
// 0x000004B5 System.Void test_nativegallery_<>c__DisplayClass2_0::.ctor()
extern void U3CU3Ec__DisplayClass2_0__ctor_mF95464CCBB9EDFA86D0785A8482181854DEFF131 ();
// 0x000004B6 System.Void test_nativegallery_<>c__DisplayClass2_0::<PickImage>b__0(System.String)
extern void U3CU3Ec__DisplayClass2_0_U3CPickImageU3Eb__0_m9A5755815823DE9D168D4EB0EA78CE86C35B1ED3 ();
// 0x000004B7 System.Void TouchScript.TouchManager_PointerEvent::.ctor()
extern void PointerEvent__ctor_m63D4E009DE7DACDA202FD81E36A40E7D04F387C2 ();
// 0x000004B8 System.Void TouchScript.TouchManager_FrameEvent::.ctor()
extern void FrameEvent__ctor_mC2B90B221D4FECA515E3401E5611A8E7510BF817 ();
// 0x000004B9 System.Void TouchScript.Utils.ObjectPool`1_UnityFunc`1::.ctor(System.Object,System.IntPtr)
// 0x000004BA T0 TouchScript.Utils.ObjectPool`1_UnityFunc`1::Invoke()
// 0x000004BB System.IAsyncResult TouchScript.Utils.ObjectPool`1_UnityFunc`1::BeginInvoke(System.AsyncCallback,System.Object)
// 0x000004BC T0 TouchScript.Utils.ObjectPool`1_UnityFunc`1::EndInvoke(System.IAsyncResult)
// 0x000004BD System.Void TouchScript.Layers.StandardLayer_<lateEnable>d__46::.ctor(System.Int32)
extern void U3ClateEnableU3Ed__46__ctor_m55FEB14D9838CA2B519B11268309CA1F33383899 ();
// 0x000004BE System.Void TouchScript.Layers.StandardLayer_<lateEnable>d__46::System.IDisposable.Dispose()
extern void U3ClateEnableU3Ed__46_System_IDisposable_Dispose_m6998592F6C19AD3776BA7FB3615E655FCE084510 ();
// 0x000004BF System.Boolean TouchScript.Layers.StandardLayer_<lateEnable>d__46::MoveNext()
extern void U3ClateEnableU3Ed__46_MoveNext_mF0C3227B9D5A81FB319B2319C0D63AFD1C57A5F8 ();
// 0x000004C0 System.Object TouchScript.Layers.StandardLayer_<lateEnable>d__46::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3ClateEnableU3Ed__46_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m0BE5322A66A3ECDD3715E38F1141D510C37F62E5 ();
// 0x000004C1 System.Void TouchScript.Layers.StandardLayer_<lateEnable>d__46::System.Collections.IEnumerator.Reset()
extern void U3ClateEnableU3Ed__46_System_Collections_IEnumerator_Reset_m50B1C834F8A2EC0DD9DF7DCFAC95CBEB8A0E6173 ();
// 0x000004C2 System.Object TouchScript.Layers.StandardLayer_<lateEnable>d__46::System.Collections.IEnumerator.get_Current()
extern void U3ClateEnableU3Ed__46_System_Collections_IEnumerator_get_Current_mC288F28B5D57A5627D273392A6972E1B01D1E3B4 ();
// 0x000004C3 System.Void TouchScript.Layers.TouchLayer_<lateAwake>d__17::.ctor(System.Int32)
extern void U3ClateAwakeU3Ed__17__ctor_m3CA22DCA2BA8FD972871A55AB88487703312C617 ();
// 0x000004C4 System.Void TouchScript.Layers.TouchLayer_<lateAwake>d__17::System.IDisposable.Dispose()
extern void U3ClateAwakeU3Ed__17_System_IDisposable_Dispose_m21F6AAFE695AF29F3D6AECD56746633607DA37F0 ();
// 0x000004C5 System.Boolean TouchScript.Layers.TouchLayer_<lateAwake>d__17::MoveNext()
extern void U3ClateAwakeU3Ed__17_MoveNext_mB1FEF8CC06BA2E431DB918BB6424AE2D3E247714 ();
// 0x000004C6 System.Object TouchScript.Layers.TouchLayer_<lateAwake>d__17::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3ClateAwakeU3Ed__17_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m83B1CE8547DD9E08C78D59FC3A477FFE5BF593AB ();
// 0x000004C7 System.Void TouchScript.Layers.TouchLayer_<lateAwake>d__17::System.Collections.IEnumerator.Reset()
extern void U3ClateAwakeU3Ed__17_System_Collections_IEnumerator_Reset_mDBC3B955C30B87DD05C779810F199A006E31DBF4 ();
// 0x000004C8 System.Object TouchScript.Layers.TouchLayer_<lateAwake>d__17::System.Collections.IEnumerator.get_Current()
extern void U3ClateAwakeU3Ed__17_System_Collections_IEnumerator_get_Current_mCEBC4FE9C1B9B12863F399BD4C05DBA1318C817C ();
// 0x000004C9 System.Void TouchScript.Layers.UI.TouchScriptInputModule_UIStandardInputModule::.ctor(TouchScript.Layers.UI.TouchScriptInputModule)
extern void UIStandardInputModule__ctor_mE74BA31E858A8B496B5768ABB4C61FAAB2374400 ();
// 0x000004CA System.Boolean TouchScript.Layers.UI.TouchScriptInputModule_UIStandardInputModule::IsPointerOverGameObject(System.Int32)
extern void UIStandardInputModule_IsPointerOverGameObject_m8B72650E3686B6327186B2EFFC8F6DF2FEBA13DA ();
// 0x000004CB System.Boolean TouchScript.Layers.UI.TouchScriptInputModule_UIStandardInputModule::GetPointerData(System.Int32,UnityEngine.EventSystems.PointerEventData&,System.Boolean)
extern void UIStandardInputModule_GetPointerData_m1C41F37A3CD0F6D326A88B9065A2707FF4B2C036 ();
// 0x000004CC System.Void TouchScript.Layers.UI.TouchScriptInputModule_UIStandardInputModule::DeselectIfSelectionChanged(UnityEngine.GameObject,UnityEngine.EventSystems.BaseEventData)
extern void UIStandardInputModule_DeselectIfSelectionChanged_mCC2D0B24E90A4C0BA6308A31589BCA6EBB500FD1 ();
// 0x000004CD UnityEngine.EventSystems.PointerEventData TouchScript.Layers.UI.TouchScriptInputModule_UIStandardInputModule::GetLastPointerEventData(System.Int32)
extern void UIStandardInputModule_GetLastPointerEventData_m609058CF2767C7FD89CADC1D8CDC0E8D7276531B ();
// 0x000004CE System.Boolean TouchScript.Layers.UI.TouchScriptInputModule_UIStandardInputModule::ShouldStartDrag(UnityEngine.Vector2,UnityEngine.Vector2,System.Single,System.Boolean)
extern void UIStandardInputModule_ShouldStartDrag_mA090464BF9C6574DBFFE079BBF935A58568CE8AE ();
// 0x000004CF System.Boolean TouchScript.Layers.UI.TouchScriptInputModule_UIStandardInputModule::SendUpdateEventToSelectedObject()
extern void UIStandardInputModule_SendUpdateEventToSelectedObject_m025CA5ADF75416F34BE357E25717456C6EB422DF ();
// 0x000004D0 System.Boolean TouchScript.Layers.UI.TouchScriptInputModule_UIStandardInputModule::SendMoveEventToSelectedObject()
extern void UIStandardInputModule_SendMoveEventToSelectedObject_m647E343743C16926B0878DE1A19726397077BF6A ();
// 0x000004D1 System.Boolean TouchScript.Layers.UI.TouchScriptInputModule_UIStandardInputModule::SendSubmitEventToSelectedObject()
extern void UIStandardInputModule_SendSubmitEventToSelectedObject_mF58B53B69185554E6FBC1BC2C914133CE69B3306 ();
// 0x000004D2 UnityEngine.Vector2 TouchScript.Layers.UI.TouchScriptInputModule_UIStandardInputModule::GetRawMoveVector()
extern void UIStandardInputModule_GetRawMoveVector_mB7233D69070FA8D68C131525FD38C225F3828457 ();
// 0x000004D3 System.Void TouchScript.Layers.UI.TouchScriptInputModule_UIStandardInputModule::Process()
extern void UIStandardInputModule_Process_m128F9F0293A64AFAF06134B0BEF393C2F39E6408 ();
// 0x000004D4 System.Void TouchScript.Layers.UI.TouchScriptInputModule_UIStandardInputModule::RemovePointerData(System.Int32)
extern void UIStandardInputModule_RemovePointerData_m52D87CFB8DF7B3D4A85838173A1A8A91B177D2CD ();
// 0x000004D5 System.Void TouchScript.Layers.UI.TouchScriptInputModule_UIStandardInputModule::convertRaycast(TouchScript.Hit.RaycastHitUI,UnityEngine.EventSystems.RaycastResult&)
extern void UIStandardInputModule_convertRaycast_m7F74BBC32629D9E6F40535B5E5D3557E3AFA236F ();
// 0x000004D6 System.Void TouchScript.Layers.UI.TouchScriptInputModule_UIStandardInputModule::ProcessUpdated(System.Object,TouchScript.PointerEventArgs)
extern void UIStandardInputModule_ProcessUpdated_m00F503B5AD3AAA332DEF7DFF1472C3CAB570D80E ();
// 0x000004D7 System.Void TouchScript.Layers.UI.TouchScriptInputModule_UIStandardInputModule::ProcessPressed(System.Object,TouchScript.PointerEventArgs)
extern void UIStandardInputModule_ProcessPressed_m1BF46C8AE5D9A4092C8191121663C584F877FC8F ();
// 0x000004D8 System.Void TouchScript.Layers.UI.TouchScriptInputModule_UIStandardInputModule::ProcessReleased(System.Object,TouchScript.PointerEventArgs)
extern void UIStandardInputModule_ProcessReleased_mC76C699EF03C973CF2EC48D52C2A04DDFEE7384E ();
// 0x000004D9 System.Void TouchScript.Layers.UI.TouchScriptInputModule_UIStandardInputModule::ProcessCancelled(System.Object,TouchScript.PointerEventArgs)
extern void UIStandardInputModule_ProcessCancelled_mD397CFF1043DFC2B9694E3713E02BA1092BCE5AC ();
// 0x000004DA System.Void TouchScript.Layers.UI.TouchScriptInputModule_UIStandardInputModule::ProcessRemoved(System.Object,TouchScript.PointerEventArgs)
extern void UIStandardInputModule_ProcessRemoved_m8CF9D7E0C005C4F8C8EC31A7232742953763C3D4 ();
// 0x000004DB System.Void TouchScript.InputSources.InputHandlers.TouchHandler_TouchState::.ctor(TouchScript.Pointers.Pointer,UnityEngine.TouchPhase)
extern void TouchState__ctor_m7B2C8ECC4F38BE22A4B43A8ECC48A149437BAFD3_AdjustorThunk ();
// 0x000004DC System.Void TouchScript.Gestures.Gesture_GestureEvent::.ctor()
extern void GestureEvent__ctor_m8D7187B46A674A2DA5DF24031F12A6A5A2F929C9 ();
// 0x000004DD System.Void TouchScript.Gestures.LongPressGesture_<wait>d__25::.ctor(System.Int32)
extern void U3CwaitU3Ed__25__ctor_m7A5B5C5EC945F5915EC705E8E6531221781A19F0 ();
// 0x000004DE System.Void TouchScript.Gestures.LongPressGesture_<wait>d__25::System.IDisposable.Dispose()
extern void U3CwaitU3Ed__25_System_IDisposable_Dispose_m8FA6757ED65AFC6DACBB523473AB71F2B904296C ();
// 0x000004DF System.Boolean TouchScript.Gestures.LongPressGesture_<wait>d__25::MoveNext()
extern void U3CwaitU3Ed__25_MoveNext_mD5DD73FFC3B85E65510EA13DB9AC39AD5850E5F6 ();
// 0x000004E0 System.Object TouchScript.Gestures.LongPressGesture_<wait>d__25::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CwaitU3Ed__25_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mF59DE5F31505D7C5AB985E99E729BDA37C4F3092 ();
// 0x000004E1 System.Void TouchScript.Gestures.LongPressGesture_<wait>d__25::System.Collections.IEnumerator.Reset()
extern void U3CwaitU3Ed__25_System_Collections_IEnumerator_Reset_m494375675C67808ADEF89DD497CED830A4E2BF06 ();
// 0x000004E2 System.Object TouchScript.Gestures.LongPressGesture_<wait>d__25::System.Collections.IEnumerator.get_Current()
extern void U3CwaitU3Ed__25_System_Collections_IEnumerator_get_Current_mE807BFD154B7C06744A3F99DA9FF66C2A9602FF0 ();
// 0x000004E3 System.Void TouchScript.Gestures.TapGesture_<wait>d__43::.ctor(System.Int32)
extern void U3CwaitU3Ed__43__ctor_m3AC19D8DCF890FFD7EC7B4C415DE37646FCB2A32 ();
// 0x000004E4 System.Void TouchScript.Gestures.TapGesture_<wait>d__43::System.IDisposable.Dispose()
extern void U3CwaitU3Ed__43_System_IDisposable_Dispose_m54744209E6CBB21D0E284C4AA4ED708D4BACEA1A ();
// 0x000004E5 System.Boolean TouchScript.Gestures.TapGesture_<wait>d__43::MoveNext()
extern void U3CwaitU3Ed__43_MoveNext_mC25D1C6FBE9122503A4E1ACC17727FD9B1E63119 ();
// 0x000004E6 System.Object TouchScript.Gestures.TapGesture_<wait>d__43::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CwaitU3Ed__43_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m4DF14E48448B4B8FE937D35C00D7C6327F7F80F2 ();
// 0x000004E7 System.Void TouchScript.Gestures.TapGesture_<wait>d__43::System.Collections.IEnumerator.Reset()
extern void U3CwaitU3Ed__43_System_Collections_IEnumerator_Reset_m7B58B8269C23985A4995C945488DC612620380AB ();
// 0x000004E8 System.Object TouchScript.Gestures.TapGesture_<wait>d__43::System.Collections.IEnumerator.get_Current()
extern void U3CwaitU3Ed__43_System_Collections_IEnumerator_get_Current_m13969DE0842296A196575525642B5D51220867DF ();
// 0x000004E9 System.Void TouchScript.Core.GestureManagerInstance_<>c::.cctor()
extern void U3CU3Ec__cctor_m4B53EBF4576A2CC52989254F02CFE09C561341C5 ();
// 0x000004EA System.Void TouchScript.Core.GestureManagerInstance_<>c::.ctor()
extern void U3CU3Ec__ctor_m02B3736DDBB2BEC654735154ADDCD39C8046FAFB ();
// 0x000004EB System.Collections.Generic.List`1<TouchScript.Gestures.Gesture> TouchScript.Core.GestureManagerInstance_<>c::<.cctor>b__47_0()
extern void U3CU3Ec_U3C_cctorU3Eb__47_0_m538E88BFC4DBCD52F255DA8DA5B9A78DC79CE72A ();
// 0x000004EC System.Void TouchScript.Core.GestureManagerInstance_<>c::<.cctor>b__47_1(System.Collections.Generic.List`1<TouchScript.Gestures.Gesture>)
extern void U3CU3Ec_U3C_cctorU3Eb__47_1_m10316F18556530CC8F06D6E0B8BAA5AD195E97DF ();
// 0x000004ED System.Collections.Generic.List`1<TouchScript.Pointers.Pointer> TouchScript.Core.GestureManagerInstance_<>c::<.cctor>b__47_2()
extern void U3CU3Ec_U3C_cctorU3Eb__47_2_mAB494D2605418B3543FBC3C3079A8A0492B348B2 ();
// 0x000004EE System.Void TouchScript.Core.GestureManagerInstance_<>c::<.cctor>b__47_3(System.Collections.Generic.List`1<TouchScript.Pointers.Pointer>)
extern void U3CU3Ec_U3C_cctorU3Eb__47_3_m4469E2A72AA6AEAF75402A165E2F640EF063F850 ();
// 0x000004EF System.Collections.Generic.List`1<UnityEngine.Transform> TouchScript.Core.GestureManagerInstance_<>c::<.cctor>b__47_4()
extern void U3CU3Ec_U3C_cctorU3Eb__47_4_mCE8F43A094B63ECB9F8CF078EECC814F7711104A ();
// 0x000004F0 System.Void TouchScript.Core.GestureManagerInstance_<>c::<.cctor>b__47_5(System.Collections.Generic.List`1<UnityEngine.Transform>)
extern void U3CU3Ec_U3C_cctorU3Eb__47_5_m7730ACB25741653A1051D0F89C91AFD93033D760 ();
// 0x000004F1 System.Void TouchScript.Core.TouchManagerInstance_<lateAwake>d__109::.ctor(System.Int32)
extern void U3ClateAwakeU3Ed__109__ctor_m56C8A851AB8559D6F2F3C181DA45187F9A778877 ();
// 0x000004F2 System.Void TouchScript.Core.TouchManagerInstance_<lateAwake>d__109::System.IDisposable.Dispose()
extern void U3ClateAwakeU3Ed__109_System_IDisposable_Dispose_mCF02BD52B18943D69FCADFB8B48D5C71540B7B3B ();
// 0x000004F3 System.Boolean TouchScript.Core.TouchManagerInstance_<lateAwake>d__109::MoveNext()
extern void U3ClateAwakeU3Ed__109_MoveNext_m92337C14936E2C8DF621A1750F545378D7110925 ();
// 0x000004F4 System.Object TouchScript.Core.TouchManagerInstance_<lateAwake>d__109::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3ClateAwakeU3Ed__109_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m00DA18E805C1CE6B014485D5343B6F33A9D5A89A ();
// 0x000004F5 System.Void TouchScript.Core.TouchManagerInstance_<lateAwake>d__109::System.Collections.IEnumerator.Reset()
extern void U3ClateAwakeU3Ed__109_System_Collections_IEnumerator_Reset_m55FEF147A2CE155F38178DE6FEE1FDC551160F39 ();
// 0x000004F6 System.Object TouchScript.Core.TouchManagerInstance_<lateAwake>d__109::System.Collections.IEnumerator.get_Current()
extern void U3ClateAwakeU3Ed__109_System_Collections_IEnumerator_get_Current_m3FD9527DD11F0D084E4B40B723E450C92E6BC0BE ();
// 0x000004F7 System.Void TouchScript.Core.TouchManagerInstance_<>c::.cctor()
extern void U3CU3Ec__cctor_m917EEE6C81BF77465A520CB4E0ACBF48776DBBD2 ();
// 0x000004F8 System.Void TouchScript.Core.TouchManagerInstance_<>c::.ctor()
extern void U3CU3Ec__ctor_mAFC6F44E151E15E5B6D763D6965F656B5E8A772A ();
// 0x000004F9 System.Collections.Generic.List`1<TouchScript.Pointers.Pointer> TouchScript.Core.TouchManagerInstance_<>c::<.cctor>b__129_0()
extern void U3CU3Ec_U3C_cctorU3Eb__129_0_m456F4C5BA26213741D14B1806E081F5AB187D60D ();
// 0x000004FA System.Void TouchScript.Core.TouchManagerInstance_<>c::<.cctor>b__129_1(System.Collections.Generic.List`1<TouchScript.Pointers.Pointer>)
extern void U3CU3Ec_U3C_cctorU3Eb__129_1_m6530EC0AAE2AC84973A5523F945D8C01A7E15596 ();
// 0x000004FB System.Collections.Generic.List`1<System.Int32> TouchScript.Core.TouchManagerInstance_<>c::<.cctor>b__129_2()
extern void U3CU3Ec_U3C_cctorU3Eb__129_2_mBBEA5FC6F007D1315A76A339CF70A349D4BDC3DD ();
// 0x000004FC System.Void TouchScript.Core.TouchManagerInstance_<>c::<.cctor>b__129_3(System.Collections.Generic.List`1<System.Int32>)
extern void U3CU3Ec_U3C_cctorU3Eb__129_3_m1EB1EF2D23CD9A9D08BB9F352E5136A64D0776F8 ();
// 0x000004FD System.Void TouchScript.Examples.KillMe_<Start>d__1::.ctor(System.Int32)
extern void U3CStartU3Ed__1__ctor_mCE687CDBE3CFDB38805FA2CA6DACBAD8181DD3DC ();
// 0x000004FE System.Void TouchScript.Examples.KillMe_<Start>d__1::System.IDisposable.Dispose()
extern void U3CStartU3Ed__1_System_IDisposable_Dispose_m7C1AB1E6884234D634C34D33EE15A7B29489D3F9 ();
// 0x000004FF System.Boolean TouchScript.Examples.KillMe_<Start>d__1::MoveNext()
extern void U3CStartU3Ed__1_MoveNext_m0DB6AFEA72301047F3DDEC67B4C8635A3E46427A ();
// 0x00000500 System.Object TouchScript.Examples.KillMe_<Start>d__1::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CStartU3Ed__1_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mD6CC52D4DC2B29320C17CFF90A51743038E00F07 ();
// 0x00000501 System.Void TouchScript.Examples.KillMe_<Start>d__1::System.Collections.IEnumerator.Reset()
extern void U3CStartU3Ed__1_System_Collections_IEnumerator_Reset_m03E1DED2743ACC02957FCDFC197611B4190ACA92 ();
// 0x00000502 System.Object TouchScript.Examples.KillMe_<Start>d__1::System.Collections.IEnumerator.get_Current()
extern void U3CStartU3Ed__1_System_Collections_IEnumerator_get_Current_mC483127391A2BA87191716AA0EEEC9611579054A ();
// 0x00000503 System.Void TouchScript.Examples.Runner_<resetUILayer>d__9::.ctor(System.Int32)
extern void U3CresetUILayerU3Ed__9__ctor_mEED30C36793D09E063FA5B04BA52D38B044023EF ();
// 0x00000504 System.Void TouchScript.Examples.Runner_<resetUILayer>d__9::System.IDisposable.Dispose()
extern void U3CresetUILayerU3Ed__9_System_IDisposable_Dispose_m644D1403AB20710FE544F4C55785DEDC2E3FE83D ();
// 0x00000505 System.Boolean TouchScript.Examples.Runner_<resetUILayer>d__9::MoveNext()
extern void U3CresetUILayerU3Ed__9_MoveNext_mD6C4AE86FD850BE887F4A855A3F10026FB6F6181 ();
// 0x00000506 System.Object TouchScript.Examples.Runner_<resetUILayer>d__9::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CresetUILayerU3Ed__9_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m0AC3371ACE4DBAD79D3E5FA8A35DC1D92BBDDAA4 ();
// 0x00000507 System.Void TouchScript.Examples.Runner_<resetUILayer>d__9::System.Collections.IEnumerator.Reset()
extern void U3CresetUILayerU3Ed__9_System_Collections_IEnumerator_Reset_m45AFFC063D71369E6EED1835F4FDA1E98C11E532 ();
// 0x00000508 System.Object TouchScript.Examples.Runner_<resetUILayer>d__9::System.Collections.IEnumerator.get_Current()
extern void U3CresetUILayerU3Ed__9_System_Collections_IEnumerator_get_Current_m4A1C4965A0D6E9524583CFEE2596A23819C0F06C ();
static Il2CppMethodPointer s_methodPointers[1288] = 
{
	BD_UI_GoMenu_mBEF5A8424B3AB7B28932326F4E35ECFF359427D2,
	BD_UI__ctor_m2BE5A2FA5C2667494B901327CB429B8A25B89589,
	DestroyQuad_Start_m0589642E465B3F5E7F9C5352E6ED0A2985D1B505,
	DestroyQuad__ctor_mEC0FA2DE0994881DF2FDA6BF68C76B016F0A5ED9,
	DontDestroy_Start_m28B8114DACB37F25CC4D1D43CBD5DF1729E8F140,
	DontDestroy__ctor_m086F2780B84C7DF0A1ECF34920B93A7799C13719,
	GoNewBD_LoadSceneNewBD_mE20EF90678D2E3A1FA3DBD12042C37AC65721317,
	GoNewBD__ctor_mCBC14A7D5D64DC8316A83EEBC3EE8D00DED4B0B2,
	LoadBD_Start_m1F586ACB9C9DEACF197254338A96BDCE84AB9F4A,
	LoadBD__ctor_m536E1797CCB2C0F3E63B1276288355259C76B089,
	LoadImgNum_Start_m72C28B461B2F04D97A5FDA5368139D966A5C516A,
	LoadImgNum__ctor_m503DAFB21604EF8828D34CD8A91E9DDF80FD6387,
	MainMenu_Start_m0E4D98D402AC049948D1CA7E386641DCF5652821,
	MainMenu__ctor_mF17B753D99BD98B88E949A5B9CA53892E19A6CD5,
	NumDuQuad_OnDestroy_mB7C1E04BFD761FC38F99199CAC67E8E5FA9711EE,
	NumDuQuad__ctor_m8D2C782C0A5A865946654C06C1B33388022025D1,
	NumImage_LoadImgNum_mA1AEAC2EE3F2339CE37C7898C7B20EA5C5823CFD,
	NumImage_LoadBDScene_m9B7A34E3A1971A8E3C7BFCAF023B098D64A27316,
	NumImage__ctor_m304FD3286218BE2B7EEF3ED98E27F4B86B1BDC6D,
	test_Start_m199A97ADA195BDC507FFD0450ABB3E3ADF58073A,
	test_Update_m8F447A6A478967D47638F11275101245FF2314DA,
	test__ctor_m150D737B2A1D3D58D1BD576A85C27FA05B275670,
	ExamplesList_Start_mCC53E9F4158B463A7057D5EA20E3B45233E806E2,
	ExamplesList_ShowHide_m7B4C8DD1B532DF0211D2831428733B82431D9110,
	ExamplesList__ctor_m582B2C57F0BB2DB961F8E084A02D62A0DD94168C,
	Highlight_OnEnable_mCE146025FC5903AF0792098A90FEF8D0E816124D,
	Highlight_OnDisable_mEF152AE18B878639C4A499B31783C7EEDAD69799,
	Highlight_overHandler_m7BE07AEC1D848E075622C48DDB1E1597E8495242,
	Highlight_outHandler_m08281D04161918D998187DB9FD9FBBC44F7C8CEC,
	Highlight__ctor_mE02F969053F8F5B433A4047AF6DA9660E59B5AB5,
	ShowMe_Start_m1DD7DF2D5267CA544EC0B5A1688C03833932269D,
	ShowMe__ctor_mED6AEBE0CF18FF46EB7C28FC95570A58FD15D6EA,
	imageBank_Start_m0FA24C2C5260EBC0A56D06CD47E23E7EC25E8764,
	imageBank_Save_mD64E958D323A334B2ED50F5261B037EDAA1B6F9E,
	imageBank__ctor_m385D1C985E5F8091FC8632A695615E916902848E,
	test_nativegallery_TryPickImage_mD9DA2AAE5585AFE7E98CB7BF7498366BCE3DED39,
	test_nativegallery_PickImage_m5836EF2B5F137638F5B23C1186F18096E8A8AF7E,
	test_nativegallery__ctor_mAE0FE4727CE600AC1F35A864605E36BA18EFE427,
	GestureManager_get_Instance_m3A10987EB55D0F84D701535F286BEE0C81C14E3E,
	GestureManager__ctor_m3777A9BDB9285195D9CD102715094310E80DD992,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	PointerEventArgs_get_Pointers_m16B91F1D5BE47012CF6C4BC0BC3F53691525FC41,
	PointerEventArgs_set_Pointers_m80547B23A481F55D60DEE830E6340F85555B550A,
	PointerEventArgs__ctor_m2184ED9B05EDC50EC544304A7B0EEE2BE31DA2F0,
	PointerEventArgs_GetCachedEventArgs_m32D6BD4E2C26738D2BB82F24904DBBB59587ED5F,
	LayerManager_get_Instance_mA50DB5F8ACCAF0DCDE4BEEA78843142A752CF9F7,
	LayerManager__ctor_m4FCB94F38604B9609E6E6A67A11C13464FCC2661,
	TouchManager_get_Instance_m355FC1AAF7F91B86F174A4429E8A44ABE454E8E3,
	TouchManager_get_DisplayDevice_m342110501378ACD88AFE29BCE3D2F1DB58D41D00,
	TouchManager_set_DisplayDevice_m473676E71E3A26D1B447AC97A228687CA1FE9C71,
	TouchManager_get_ShouldCreateCameraLayer_m9555EEF395EA3D416B86CD071695F48749CEA641,
	TouchManager_set_ShouldCreateCameraLayer_m321C5C1830A0EF4AE5FEB0FB5A33A5E32B62D938,
	TouchManager_get_ShouldCreateStandardInput_mE33919A0FE9C7D46BA910362D807179381DFAAE1,
	TouchManager_set_ShouldCreateStandardInput_m06BB13C8B96C2A1040F4EB85BAEC51BDF4FAFEDA,
	TouchManager_get_UseSendMessage_m6174E37C90F89BBF01BDD07A57AA79D8153E85A9,
	TouchManager_set_UseSendMessage_m7885229E5BA8439B403C3BFFEEF0E1C7315D2009,
	TouchManager_get_SendMessageEvents_m807C4A591F1C10FB759D7470C7D1F54484C478E0,
	TouchManager_set_SendMessageEvents_m83F740F8FC4AE4DC5FC8BC5349C2964F8E3325FA,
	TouchManager_get_SendMessageTarget_m0410C0052F476BB4C6B06ACAF535FFCBDFD21863,
	TouchManager_set_SendMessageTarget_m887C577DCA1779ABB748FF27E07892D9FDBAA1BA,
	TouchManager_get_UseUnityEvents_m7AD3163F983E7429A268638E986C75EEC6620ECE,
	TouchManager_set_UseUnityEvents_m330D1F5971F4A6D8C5BEE0D2F36A6F721CEC56F7,
	TouchManager_IsInvalidPosition_m80842AEBF1770FB52A67BC73957A1B1956D2716F,
	TouchManager_Awake_m9417FD558FC15583C25CEB2135A9A8F9053A0773,
	TouchManager_OnEnable_m9C0ECA70F64AACDA06C0B83E848F5F3030AC3549,
	TouchManager_OnDisable_m9039E96AFD2AFA352E2EE0D14007F3779E130C8D,
	TouchManager_switchToBasicEditor_m3A1C84E7DDBA54ED96FAC435729BC58C5F3B636E,
	TouchManager_updateSendMessageSubscription_mAA916B61823E409F702F20238AFA50536FBABADA,
	TouchManager_removeSendMessageSubscriptions_mDA19EA010E735C99BF9CEC546274DAC4694CF57C,
	TouchManager_pointersAddedSendMessageHandler_m63C06829F976D339666BFF3FAB8530E101355FDD,
	TouchManager_pointersUpdatedSendMessageHandler_m58381274AF569B5230025CFB6ABCDE92318C4606,
	TouchManager_pointersPressedSendMessageHandler_m93132F18416FB55153153A1F9F4D53C46255CF99,
	TouchManager_pointersReleasedSendMessageHandler_mBA8E495E7939B9A1F66F81C7179514F1F9EAE3FF,
	TouchManager_pointersRemovedSendMessageHandler_m57EA76B182D2BAD64AA62E1141EBC21CD80AE940,
	TouchManager_pointersCancelledSendMessageHandler_m4317C08AB3D25607D0B1544FBB0AEBE063597BFE,
	TouchManager_frameStartedSendMessageHandler_mAE3F3B2767D5FA1521A68F0275B31A7A9B3B2E04,
	TouchManager_frameFinishedSendMessageHandler_m06643F8EE650407C385EF6648920F3A211E0D5E6,
	TouchManager_updateUnityEventsSubscription_m6A5CA7A27D10F83B2A5CA578C3446A140E2FF3B4,
	TouchManager_removeUnityEventsSubscriptions_m16B73F9CF5C8D19E6FB2776C07439A1F6A93F3FA,
	TouchManager_pointersAddedUnityEventsHandler_m05BDEF56AC60141C69CFECD9324EAF9DCCBA3859,
	TouchManager_pointersUpdatedUnityEventsHandler_mDD4D2BBAA16EB45E6473E25CAB802E6B1C02C719,
	TouchManager_pointersPressedUnityEventsHandler_m82F19BBA6DFDA4EA2767F2F51EA100083F5C2F84,
	TouchManager_pointersReleasedUnityEventsHandler_m79811722367588FAA1AE65FB9B6F5250BCC92382,
	TouchManager_pointersRemovedUnityEventsHandler_mD8CB51C6894922EBAED67BD56F9920B78DA208FB,
	TouchManager_pointersCancelledUnityEventsHandler_mFEB2D2BA0CA023BC04A9ADA27B2CFED543EA6A26,
	TouchManager_frameStartedUnityEventsHandler_m89C3B5C487CEB8878A3B71B698F8BB814713ECF0,
	TouchManager_frameFinishedUnityEventsHandler_mBFE2FF2587FEE191DB70D58242692DC0690191ED,
	TouchManager__ctor_m54F25692CE236EEBEEA41BD3E445AB6042BA35E9,
	TouchManager__cctor_mA1F542857C7735E0DC46820A360BF8B2A9E48366,
	BinaryUtils_ToBinaryString_mCB55E824E5042B4C35CD8EF55C3090C73D1B3FEC,
	BinaryUtils_ToBinaryString_m841313E6549570892EDB2B9C9421BB3C54F64F03,
	BinaryUtils_ToBinaryMask_mB486631A929B8755B239F3BD07EBD208F3F7E8AE,
	ClusterUtils_Get2DCenterPosition_m85D3BC7F9ABF6B24256FC4F7EEC887B8EDC25293,
	ClusterUtils_GetPrevious2DCenterPosition_mE261E1BF0B86549ED5FE9DA1945C874585EF60BB,
	ClusterUtils_GetPointsHash_m31E06AECDB1ACD7975B9B98CA151E538E1EFA823,
	ClusterUtils__cctor_m2E71E8524F593F6D8BABC472814BC6A3BC5176A6,
	NULL,
	EventHandlerExtensions_InvokeHandleExceptions_m8E6D5FEFA89D62212811606B5639EA40526C8509,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	PointerUtils_IsPointerOnTarget_mC471A1AE57B2F02F0CB977FF51AB0BCD7706C6DE,
	PointerUtils_IsPointerOnTarget_m73191DDEDDAA92FC5B63E82BE8C94DB49948E576,
	PointerUtils_IsPointerOnTarget_mFA6B5C217481FFAC6208A08D8564B02C464C2DBF,
	PointerUtils_PressedButtonsToString_m57F1173F8613DA8D378DB09AA0FDB41285C639A5,
	PointerUtils_PressedButtonsToString_mAF4CD9F932292EE068D39024DDD1E70F7DC26D9F,
	PointerUtils_ButtonsToString_mEC2274B4CDCC3BC17F12957876CA4760A47815B7,
	PointerUtils_ButtonsToString_m90AE750BDB47BF3343173B2DC737E10AE1BB0C21,
	PointerUtils_DownPressedButtons_mA925780101477141AEADCBD36F73DBA3EF36816E,
	PointerUtils_PressDownButtons_mA71150EFD6A6F014898A56D35B72049AC7E569DD,
	PointerUtils_UpPressedButtons_mE54D033069BCF06BA90CBE76507B340ACC4F58DC,
	PointerUtils_initStringBuilder_mEB6698C4E28B8F6D3E4E40FE5CA452C8415C179C,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	TransformUtils_GlobalToLocalPosition_m83D494F04656DBAD08C6443DEDE9A014171743FD,
	TransformUtils_GlobalToLocalDirection_m51021C8A547001E01BEFD522426645789F7F7C76,
	TransformUtils_GlobalToLocalVector_m57DE02E94AFA43DD446DD9C16B46140FADCF1003,
	TransformUtils_GetHeirarchyPath_m0AEF337A0A5E5D3DD2686702E34749D3AC63B0E5,
	TransformUtils_initStringBuilder_m6846E6CB8306F423AC924826D4D12667ED674C99,
	ProjectionUtils_CameraToPlaneProjection_m49C90C2A65AB392F106E1B043BB38EC1D603376A,
	ProjectionUtils_ScreenToPlaneProjection_m5ED3FA8D18B088F44849EC32C475DE400C7B63D5,
	TwoD_PointToLineDistance_m423B16A5A7384D020178775D8019DE019E7C52B0,
	TwoD_PointToLineDistance2_mA904184ADD13C33E7951C56BB2B8A16C167686F9,
	TwoD_Rotate_m5077CC8359AAB1CE9AFD09F8F8A10E6FB2E6BF42,
	NullToggleAttribute__ctor_m35A491114899DA79F9BE73EA8B91B70FFC5041F0,
	ToggleLeftAttribute__ctor_mF0FF749A04925D5C49276E1DEE37922A4087A4E3,
	FakePointer_get_Id_m7F37A5013C88FA454666B85294990FD3C5BE5EE3,
	FakePointer_set_Id_mC0D41C4688A1B33B826E36B909C6A0E3983EFEB5,
	FakePointer_get_Type_m0273E1B87410A58AF1941B97AE4547FE7F22B588,
	FakePointer_set_Type_m79DF4E0AF1EEF7C1BD223A21042D27D9E12327EC,
	FakePointer_get_InputSource_m43787A4A83F64C058A0E58DEA93B595FA05C0670,
	FakePointer_set_InputSource_mB5DEBE9F1174C7584540B72CD9DE5FF267967CB5,
	FakePointer_get_Position_m9FD582FD15FF725FF49EF261D11455ABAF2F6BB7,
	FakePointer_set_Position_mCA620D1E36778D9DD9DB87D24F6899D5EFEBBF34,
	FakePointer_get_Flags_m85F8668433273916F920F9C20331964A5C0BB71A,
	FakePointer_set_Flags_m64E3B23758D21054F7CB36BB6033AA7088169EB8,
	FakePointer_get_Buttons_mFB0294AF9118DD3A20720111DD9BB5349873D224,
	FakePointer_set_Buttons_m85F4C76E07206AAFFB0F917283159DCDC4261280,
	FakePointer_get_PreviousPosition_m2F7D67485F3BBA05DB7CC1E4A07AC85743450ECB,
	FakePointer_set_PreviousPosition_mCA34B1330CCAD003F7A6033C7F3EB24C712C3608,
	FakePointer__ctor_m296C5E4936AD664F9CA7065525B7AC8CEF1D5F0E,
	FakePointer__ctor_mAB218D43DC15CD0732B9995C7F5EF5D08FFA2F3C,
	FakePointer_GetOverData_mAF9904063454D323B0D8E6758558C7F892680D31,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	MousePointer_get_ScrollDelta_mC8F97C22EFBD42A56861A7B1EF1907185F0746A3,
	MousePointer_set_ScrollDelta_m352D328338CF5FF4420BEA90891189D57C517C1F,
	MousePointer__ctor_m406338CE653654229558A5BCC38F97692A452158,
	MousePointer_CopyFrom_m9568581ECA2A9BB14241FB2E292E4FE15591EA2F,
	ObjectPointer_get_ObjectId_m19757D9C574D591D4DD3043EEFBDDCE1064B2581,
	ObjectPointer_set_ObjectId_m4951549B2096276F8533B2D025F744CE8E5D4FDD,
	ObjectPointer_get_Width_m92F855D6202D7ECC31204F7BFB1924B26DA5F007,
	ObjectPointer_set_Width_mFCDC006E0026F249E35C88362BD748EE5A14AFD7,
	ObjectPointer_get_Height_m30D7BD97E5CD11924D9111F3011E0E453CAA6DA0,
	ObjectPointer_set_Height_m793FAF0083E3C0DC47E6825EE6ECCA7F342CE66A,
	ObjectPointer_get_Angle_mF6824CF5CA2316D34523B893EE50BEF01DF9D1E9,
	ObjectPointer_set_Angle_m3710F36254EBE84ACBC00123055FFB1CE8D8D108,
	ObjectPointer__ctor_m24E6EC51EA50C4F5BF815D88CF8F84A552D98A41,
	ObjectPointer_CopyFrom_mC615B4489D31D9AF0AEC3C4992CC93C7A562A14A,
	ObjectPointer_INTERNAL_Reset_mAFD389FA014F7C7EE0FEC7A4F81CDB9D5CE410A4,
	PenPointer_get_Rotation_m83B389E7974ED8C633C93AE5CC7811D3F8980BEA,
	PenPointer_set_Rotation_m74FF90FEA33FF1621F9792CBDDE3A31569246E08,
	PenPointer_get_Pressure_m7594304DCC404FB3D58E741002C56E92B44BFF30,
	PenPointer_set_Pressure_m33A397E626F343E987248BEC63FAB5F18621BD61,
	PenPointer__ctor_m6D8A7F282AAE1FA58ECC119BF00ECF3C534AE5E2,
	PenPointer_INTERNAL_Reset_mB10622BC68374DB2B92C6111E0D3BAA2BF38A718,
	Pointer_get_Id_mFF112114C234DC146CA93A7719C3F316E8DACCAD,
	Pointer_set_Id_mEB0B14E2905D84B287EABA142EEAFF7737D63886,
	Pointer_get_Type_m8733FBC6C16FE2C6FBA301C50E8E26266720B9EB,
	Pointer_set_Type_mC0D513634112590ABB8EB877E8BA70416BDF2625,
	Pointer_get_Buttons_mDD144F267D37B07E930E44E9B7D9357246D5839A,
	Pointer_set_Buttons_mAE901843226D346730CAD2CAD512793E4E4BEAC8,
	Pointer_get_InputSource_m39065F33E59F38803F3CB3DB02DF22DF17332149,
	Pointer_set_InputSource_m646284A86B72DC008C111B607C12E38F4A4EA385,
	Pointer_get_Position_mCE0B33AF8B7D86A8BA6824E7817A3DFCB29BE4BC,
	Pointer_set_Position_m00CDB115C2F9FE3EDDB493FCCF6016D09AD52C4A,
	Pointer_get_PreviousPosition_m18CBA71CDAE159A709A4F88D96F40B81B333118E,
	Pointer_set_PreviousPosition_m9825F629338D0DF6160DCB6DAADF3488860FA703,
	Pointer_get_Flags_m941FB1843C3FD979278B353B040264E56F68A883,
	Pointer_set_Flags_mF6A08BE8419E9E7DFD4CD15AF3A4E811734E9293,
	Pointer_get_ProjectionParams_mECA4594E2279D92F8731317FA2BA2F3CD7904C38,
	Pointer_GetOverData_m3E939E2B28388235F9797C61CBBDABC0905DD195,
	Pointer_GetPressData_m806849D3319D7AF0EB6469AC978F593B1AD6A3B4,
	Pointer_CopyFrom_m323BE52F7920E028E626E6C42E1C5A230CAB1494,
	Pointer_Equals_mB22D2809944C62A89A29FB04C2FC45DA69F38D9A,
	Pointer_Equals_mBB1ABA52EFE71C48513257DDD931073295A2EE1C,
	Pointer_GetHashCode_m408BF0466B46E542E764170915D38FDB1EA60B5B,
	Pointer_ToString_mFFA8218E27F99EC02496ABECBB46E8D4A5AE37B6,
	Pointer__ctor_mE7828F1485349BF2A610843ABB9A7944396F2D6C,
	Pointer_INTERNAL_Init_m2E683FC93E6121AAC8BF03B4B10E2DE3274A2C5C,
	Pointer_INTERNAL_Reset_m851FB3F6C82DD723EB5A78235BC0B45865FE8897,
	Pointer_INTERNAL_FrameStarted_mC677279C07B7D49D25BFC21580F50C74D037FAA0,
	Pointer_INTERNAL_UpdatePosition_mB4AE54E8D1CFC6EFDBD36FAA6A3DFD04E98E0716,
	Pointer_INTERNAL_Retain_m4B131382F3A12FF21FB907EEE0769FEA6704A0BF,
	Pointer_INTERNAL_Release_m15DC4D63F05CB4001750D188BC00502BF017927B,
	Pointer_INTERNAL_SetPressData_m922C64806B13717F734D42422561C0FE31DA8B26,
	Pointer_INTERNAL_ClearPressData_mBF3AD7F3B5D26FC7D9D50A7753BFA63FB3BFFF39,
	PointerFactory_Create_m7D6B0A6806D107FFCF0D3BA8B4C3EB31B4E6055F,
	TouchPointer_get_Rotation_m3ECB502F6BA93D13E74646A81460AAACF27B75AF,
	TouchPointer_set_Rotation_m87442B9A19EB126F7E42949A8E0FB1FC2CB88FF9,
	TouchPointer_get_Pressure_m894BA5476887A5547C328C11BE33BB03E2267462,
	TouchPointer_set_Pressure_m9DAD082334050E94FCD81D8887D56CAA8D548BD0,
	TouchPointer__ctor_m45AB2CE7C1661672F231AFA9D718A1F877FBF276,
	TouchPointer_INTERNAL_Reset_mAA8BF6032219E321A1CC86AEF61EF74A4ADAF0A2,
	FullscreenLayer_get_Type_m4B04610F987D3F71BDBEF7D19FEEC1A4D872F417,
	FullscreenLayer_set_Type_mED3CD065A8EE3DB019329068C94E6F781C6E4B5C,
	FullscreenLayer_get_Camera_m7DFE60A334FD4157B8B5609C8DF432E21FC9D658,
	FullscreenLayer_set_Camera_m001B1EF63AA742960956B13A7DF3CAC5AB7FB9FF,
	FullscreenLayer_get_WorldProjectionNormal_mE1518BCAB865E2C9B459C17ABD601D784BCDE076,
	FullscreenLayer_Hit_mF0461594D6E277C208EAF8C0A8C1968BF645F30C,
	FullscreenLayer_Awake_m1FDED2FB2DD29062C6E2376B59C8C4A3A171A3B8,
	FullscreenLayer_setName_m48D33B282BA78251E3A8EA17C87A600A62160C62,
	FullscreenLayer_createProjectionParams_m524FE182CC2876BEA489FDBE48438318FF3F340C,
	FullscreenLayer_updateCamera_mE8C5D7A045638E21BF482F1B68AF2077E5D35A75,
	FullscreenLayer_cacheCameraTransform_m7E488A981ABFE988101D410D0D24F877AABDD2C2,
	FullscreenLayer__ctor_m28D29EB32FEE880687FF110654BA16227038D27D,
	NULL,
	ProjectionParams_ProjectTo_m7F72FAB69700D8EBFA9E8BE93880E8F4BEFA769B,
	ProjectionParams_ProjectFrom_m04E72D41FC2DAB6A14CAC3EBDB46A76D991314B1,
	ProjectionParams__ctor_m6A4C0C8062A36F504724741380C7479CCD9AB348,
	CameraProjectionParams__ctor_mC62D0B363F564F5F93A329C693E8994D4A0FDC7B,
	CameraProjectionParams_ProjectTo_mD45439E04067FF83436D8C1EA9BF965FB77B4FE1,
	CameraProjectionParams_ProjectFrom_mAC8A159F143B27D3F6B0651842BC763A8ACDEEE6,
	WorldSpaceCanvasProjectionParams__ctor_mDDE0E638C385DBFF93B8CB5802CF8D3D8B39BC4C,
	WorldSpaceCanvasProjectionParams_ProjectTo_m1D26196396B95BA4A98BAD0D3366D951DA12F030,
	WorldSpaceCanvasProjectionParams_ProjectFrom_mBD552841734A8E42D1978277AF584B317C84A321,
	StandardLayer_get_Hit3DObjects_m9DD3E44FAFE7E1FC43A8F044F0C7501FC568DF13,
	StandardLayer_set_Hit3DObjects_m330D2E44BFFB3838AB7CDD1C01BDB510451092C0,
	StandardLayer_get_Hit2DObjects_mFC5C5EC8783A40FCF438BAE393B1D951A2A30503,
	StandardLayer_set_Hit2DObjects_m25653BAB030E5CD7BFBDEE55D6A46552A237F387,
	StandardLayer_get_HitWorldSpaceUI_mD600F9AC5657B229FE7A720EE452A1A4348B827E,
	StandardLayer_set_HitWorldSpaceUI_m1729EA20A511DDEEDD8EB9E0EA205DBEB4238618,
	StandardLayer_get_HitScreenSpaceUI_m473D8F8DA59E07AE14489FBB2A863747F54A00A8,
	StandardLayer_set_HitScreenSpaceUI_m05D72794DCC58218035D08247E0B5099CD49DF7A,
	StandardLayer_get_UseHitFilters_mAF2683CDA6FF64739E4F2807FDFBB8FA3FA7D1E8,
	StandardLayer_set_UseHitFilters_m756B5FBB37682A62031832F616CD073784B10E28,
	StandardLayer_get_LayerMask_mF42BFB57C8F5C4057727E24D9C4996DA5F9B9401,
	StandardLayer_set_LayerMask_mB3BA71C86E249584BDF082F20EC4651326D61795,
	StandardLayer_get_WorldProjectionNormal_mA4A17DC1ABFA447F17CF3FC0442E4336FDF010DD,
	StandardLayer_Hit_mCB72499B9BB6BE4CAB26E6DE872345D82664F238,
	StandardLayer_GetProjectionParams_m659030799A5F67B4A1BEF9720C19F05C4070D9A1,
	StandardLayer_Awake_mF0FAEDCEDF303325290E141D6E04E603A92D4598,
	StandardLayer_OnEnable_m905342AECBFB079F2041AE5495FC6896B35D5099,
	StandardLayer_lateEnable_m0298E9000E4131E237A1EB027FACC04C6C6C64EF,
	StandardLayer_OnDisable_m61CC0FB31E8DE1B69A6378B9144E8E8A0B5A3580,
	StandardLayer_switchToBasicEditor_mA853669EA719E3B95A615967699948BC59D1E6F0,
	StandardLayer_updateCamera_m378E206684CB41C6EAD2457AE2F5BF44447F84A4,
	StandardLayer_createProjectionParams_mF728C6CFAD5AFF3C3EF7B2C1045530B33A22FFDD,
	StandardLayer_setName_m144092F07C29458533807D54A8EC081E6AE2F3C3,
	StandardLayer_setupInputModule_mEF1427D87433C3EE20D055D4322EC53A19923859,
	StandardLayer_performWorldSearch_m968AFCB9739F46BF9587DEEA5E0474C21E574593,
	StandardLayer_performSSUISearch_m4B4D9E8334ACABBD763118078AEE5B7E4CCC156D,
	StandardLayer_performUISearchForCanvas_m6801B1E5A376C25A9F1CEE232FE38B12E5771E81,
	StandardLayer_doHit_m93790EAA1BC87D9D1CEA17EC8AEA80E165F40F41,
	StandardLayer_doHit_m0B3FFCFC5B953A6B66CDAC5CECBEFAA5DCCEF7FD,
	StandardLayer_updateVariants_mFCB2F7B60AFDB2CD667715F8E9E3FDE412D01A4B,
	StandardLayer_raycastHitUIComparerFunc_m24D15006D692933868D38BD870A0213CA168E90D,
	StandardLayer_raycastHitComparerFunc_m494605C8BE0B7F67F5AFB10171F80FD39AD47295,
	StandardLayer_hitDataComparerFunc_m7975F815112BB81FFCCA64AF47B610FE27DBC021,
	StandardLayer_frameStartedHandler_m60F3C8FB1A05528735DA6A7BC7FA00F3CD8C0986,
	StandardLayer__ctor_m64661AD4B7455D67DD5C06FAA965E26E44E6738D,
	StandardLayer__cctor_mD654515376308B21FA82AFC8D3B207081E3ACE24,
	TouchLayer_add_PointerBegan_mD859EB0CD2D987A07BF5097EA01150E650B1C046,
	TouchLayer_remove_PointerBegan_m439868A981A009DBD00BF07AD484DC5AE4BCA1C1,
	TouchLayer_get_WorldProjectionNormal_m6A9114EEDAF3C043013DC105CB4A59EC99E4F46B,
	TouchLayer_get_Delegate_m323557CA7B99D9BF8610CBF3133192C2D214D7EE,
	TouchLayer_set_Delegate_mF73EB797666FE4C3805F348DF2ACCF41F096308F,
	TouchLayer_GetProjectionParams_m32695242FBEB2D7AF07CD670CA6850EA99C1A174,
	TouchLayer_Hit_mF4746A5BE8F136AA19C580A754573F97F359C2CB,
	TouchLayer_Awake_mCE25E28C3D1CBBC8767DBCD39EDF92A890F310CD,
	TouchLayer_lateAwake_mB75570F442C3A16F263FEC0AD64FC83A103035D7,
	TouchLayer_Start_m892EE66C1CA8E8382594A14FC83546CA0AA5572A,
	TouchLayer_OnDestroy_mA32E6957ECEDDBF720584AC3AD2533542C7BF499,
	TouchLayer_INTERNAL_AddPointer_m92F774F1100FC9E763FA5ABE5AB44BF56E9EF533,
	TouchLayer_INTERNAL_UpdatePointer_m950657B6D8931E2AF4D8FC92007D656EC4663EC3,
	TouchLayer_INTERNAL_PressPointer_m1065338DB1DCAFE907587896FE76178EFF81E399,
	TouchLayer_INTERNAL_ReleasePointer_m2F12AFBCD1A3F4B155D6F0199FEBB7F916D7E84F,
	TouchLayer_INTERNAL_RemovePointer_m995AE661DD81080C9A840DFA8B189EE4CDB36902,
	TouchLayer_INTERNAL_CancelPointer_m32453B34AF6F491BCB5151F306E757E9CC20B4E4,
	TouchLayer_checkHitFilters_mEF3E87A8533094812A2AAA3AB40FBE6784CFD23E,
	TouchLayer_setName_mA8E3FB6ABAFBE2A3307768F8CAFC139471B640C1,
	TouchLayer_addPointer_m2550CC60DF813CA5829FB386F78F8186C4FDCFA8,
	TouchLayer_pressPointer_m0AA646E9C4F275E51AC49733B141851509BEAFCD,
	TouchLayer_updatePointer_m17938E18F21292DBA91368615D6D8BB1F6173019,
	TouchLayer_releasePointer_m6D4E3B31D7906D12859F894F30F5E40BE1324EA1,
	TouchLayer_removePointer_mFB20AF0FD8BF9612D6B87D5E509071E9D3D3FF55,
	TouchLayer_cancelPointer_mC637FBD7A983B2DEDE789537F7C28B8E8C0A3259,
	TouchLayer_createProjectionParams_m517619874A42BCA26A8DA756CD37BDAE2EDA3315,
	TouchLayer__ctor_mCC19DA14C5A7CDA9AB1A21C4F131FF84CD32A5A3,
	TouchLayerEventArgs_get_Pointer_m6995F780D2D3EC5E2A40D4A87CB535EC5C40F228,
	TouchLayerEventArgs_set_Pointer_m31C50E02225B5A42D3802AB9DB0A55A9CF021266,
	TouchLayerEventArgs__ctor_mC51EB0A7AEC1F354BC6DA580359969C25D061E40,
	TouchScriptInputModule_get_Instance_m1C3312EE86FB0C6645698CE44E98EEF980716E8F,
	TouchScriptInputModule__ctor_m597AA785F41E7C3563E337716CEA1BBCF21EC4F4,
	TouchScriptInputModule_OnEnable_mD6149E783F7A1FB7E4DFCD19DC1F685211BF8E99,
	TouchScriptInputModule_OnDisable_m7840DABE792EE8069831F67A07D5838927B080A4,
	TouchScriptInputModule_OnApplicationQuit_m8B7D96693D1DB33653DEC030536F727AD95EB1C1,
	TouchScriptInputModule_GetRaycasters_m4E5453F68A6C7837C997DCFD2006D356785087D6,
	TouchScriptInputModule_GetCanvasForRaycaster_m48C9D00D72301BAD605954B38A4EEF4E9BA1802B,
	TouchScriptInputModule_Process_m11C287676F1C042747AA7266E6209F3F2179790D,
	TouchScriptInputModule_IsPointerOverGameObject_mC5B728CCFBD02DC689CFF35C4CB8BEF23343B345,
	TouchScriptInputModule_ShouldActivateModule_mB14F07685FC4E158EF7114CC4564C539BD3D162C,
	TouchScriptInputModule_IsModuleSupported_m3C7917468B42EB31DAC4BEAA14ACE3B85913BD6E,
	TouchScriptInputModule_DeactivateModule_m05A709DD7D02D7F1FD9F5523A9BCF58910C13AE4,
	TouchScriptInputModule_ActivateModule_m1E2219AF468BBCCE90A4421848841A2F52238BD9,
	TouchScriptInputModule_UpdateModule_mB135DC7CE5BDFC57CFE0F5CF7960C5DB18201DBD,
	TouchScriptInputModule_INTERNAL_Retain_m3FCF80941B11F539CF37F7BB7021B00DEBF59E60,
	TouchScriptInputModule_INTERNAL_Release_mAA3C7480D2AD4E5245F994884694CF7DB7228772,
	TouchScriptInputModule_enable_m0396B6BABA3185094B191774CB754CB4EF2F8C1F,
	TouchScriptInputModule_disable_m76B01999ACFACE8032A3D3451C61D67A8DBB43F8,
	TouchScriptInputModule__cctor_mEBC46F354326BFC74D470157CE08E91F8D0ABBB1,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	PointerDelegate__ctor_m3502BA1ED39792501732A5A60B3AA262C6D14091,
	PointerDelegate_Invoke_m0ADEBCF43488019F853BE6F396A814E6EC211039,
	PointerDelegate_BeginInvoke_m833BFBD5734AF35CBD8C13832B8181905A0C6A57,
	PointerDelegate_EndInvoke_m2A5324F23D69E176109763FBBC1F388A13D740ED,
	InputSource_get_CoordinatesRemapper_mAA23A9CE379397789FDBB3E2B17E6F4C0D8107FC,
	InputSource_set_CoordinatesRemapper_m8E914274363528B6448E325E4501EECEBEC2DBED,
	InputSource_UpdateInput_mDAB230C1DE8EC83289D7CE490BF1F089DE58F55B,
	InputSource_UpdateResolution_mDD670279D16306D6E87CCFB989E7BB747F59A5AC,
	InputSource_CancelPointer_m53ED3921AD3A1F95124B856F6CF70DDAC8CD4532,
	InputSource_INTERNAL_DiscardPointer_m23C8EF4C18A2175AA9DA621575F0464A3752FAAE,
	InputSource_OnEnable_m4B6C0093FEB241D287FB964602FB10E69E918ED8,
	InputSource_OnDisable_mFA43FC2B1744C02CE41A67E9A26FD9AD511441A2,
	InputSource_addPointer_m62F5CEEDE9B66E0C8B414B89D2A6E4C94E5EA62E,
	InputSource_updatePointer_m22B7FB283FC97011FC9A0A4A9FCFD13B170D6BEB,
	InputSource_pressPointer_mAC97DC87D8EE3E127706867B5488C23FC7047390,
	InputSource_releasePointer_m2C76F24F508F7E5ED533C649C74F475C52AEF275,
	InputSource_removePointer_mA105E39F5363F55607F9D5A99D317F71D079F275,
	InputSource_cancelPointer_mCB02D2D11B42AB28B0D972B2E50F8CF9F16186D7,
	InputSource_updateCoordinatesRemapper_m763B13ABA17FB93200643775EBB5CECDF712FA19,
	InputSource_remapCoordinates_mD99943B29864A81DB9AD7F868A5BC4D3BCCC424C,
	InputSource__ctor_m10AB4CED3BFBB14FFB677A71F044CE922D70C17E,
	StandardInput_get_Windows8API_mAFEBEAB1026F308B47C9E4853BD2E2C4E70ABC50,
	StandardInput_get_Windows7API_m233E2974BAC1C3B74861E2C38CE233E0EE37C1BA,
	StandardInput_get_WebGLTouch_mC9FF250F9DD94606CB80E831A44678419AAD975D,
	StandardInput_get_Windows8Mouse_m029133A48E9E4B1BD6DE207ADECD9B698E50CFCF,
	StandardInput_get_Windows7Mouse_m93D3822EF6C9824C58D2EFE03ECA304B50B558AE,
	StandardInput_get_UniversalWindowsMouse_m44D4DB4CC21141AD05616725A723D9C9E1B744AD,
	StandardInput_get_EmulateSecondMousePointer_m292B69D30FE63C395451D797FE527FD23D75CA12,
	StandardInput_set_EmulateSecondMousePointer_m89A1BC7F029124D75A679643E534201C78E3BAE2,
	StandardInput_UpdateInput_m317AF408D8C388369A18526A3D385C1180CD6E5B,
	StandardInput_UpdateResolution_mAECB2F1C4DEFFD8FC9DD308BA6545CE71FB1D461,
	StandardInput_CancelPointer_m5352E5DB187E610681EA78E475EF546BACCFCD5B,
	StandardInput_OnEnable_m9EE713D7441F02540B1ADF11572A2C29ABF678BA,
	StandardInput_OnDisable_mBF9B175DD91AEDC45A75AEEF55308525349FFBC0,
	StandardInput_switchToBasicEditor_m1F74B05D7A423DF04A8DA5E531D2CBEAA43E00A7,
	StandardInput_updateCoordinatesRemapper_mDF1319DD1D08164DD5C3F40790D132A2E0385E00,
	StandardInput_enableMouse_m6E303BE93A3DE7A7DF3D40D604D7528281696DAA,
	StandardInput_disableMouse_m47B567E3C02B2EAE4237BF720F2EB8DB4AC4D9D9,
	StandardInput_enableTouch_mFF487146C665A986C30A49DDBE4B06C1DD4AB88B,
	StandardInput_disableTouch_m2B0AAF063D444218ADF3A7A3BA3F79AECDC0B84C,
	StandardInput__ctor_m721B12256B47845AD09A6DD820594180DB32BA2E,
	MouseHandler_get_CoordinatesRemapper_mA9A749023AF1989DC3414E70A063DACD8AF9FB57,
	MouseHandler_set_CoordinatesRemapper_mD89E131D24C9D9ED35FBFCF9206F4930DF42E11E,
	MouseHandler_get_EmulateSecondMousePointer_m5A09DEBE36AE0D7F042D2A2E49C3EDDAD073E92E,
	MouseHandler_set_EmulateSecondMousePointer_mEF7505BE8AD19391D0C96101781E6691092CADA1,
	MouseHandler__ctor_m3FE081371395430265013FC2C5D3E8819EDC4525,
	MouseHandler_CancelMousePointer_m49E3E6844F1306C76D2DF2F971FC601A5AA43938,
	MouseHandler_UpdateInput_m7167FA18AE3882EFC6DA9303BA1630C17834E485,
	MouseHandler_UpdateResolution_m7FEDE2B5C1AF34CCADB0CE7503CB41C3FF81390B,
	MouseHandler_CancelPointer_mA14AB0680D24B0137AD7B2730D6B2D7366E0B443,
	MouseHandler_Dispose_m2B54073CF866C0BE982F039F41CA2BAF76F13ED6,
	MouseHandler_INTERNAL_DiscardPointer_m84CAE6D62DF3CA4C70D23F613F1084EA10A746EB,
	MouseHandler_getMouseButtons_m46AB36FB309193AE4318F4CBE2334AF467F09859,
	MouseHandler_updateButtons_m3398D9DFE9D02F589681C6C435B46E3DE197ED0E,
	MouseHandler_fakeTouchReleased_m62D6E481257ADB79BCC19B697A2824E6DA8567EB,
	MouseHandler_internalAddPointer_mEA2AFAA7DF2DC27DA88827C8B482E3905B53DA79,
	MouseHandler_internalReleaseMousePointer_m47BC131A7AE6BC79C516FB0D973ED1459C102C3B,
	MouseHandler_internalReturnPointer_m458B4DE568C86106A63EC772E2045723DCA09B48,
	MouseHandler_remapCoordinates_m1ABF7ED10957794C6B3E3BE9C86520378830367F,
	MouseHandler_resetPointer_m0DB4DA2A30B42EAF0A287BD463A51F403A65CE66,
	MouseHandler_stateMouse_mEA19230557329FE4066A3FC201EEBB80DDF3FE16,
	MouseHandler_stateWaitingForFake_m1FF8AE61E9B77390F9C8A832559E83B83EA8B102,
	MouseHandler_stateMouseAndFake_m17831B829E59E29E0EA40ED17A16CDC35C9C436C,
	MouseHandler_stateStationaryFake_mA91F5F60DAD980EB67D7B24D840EE1BF6757B4FD,
	MouseHandler_setState_mDD389D573EC073E142D94A13B382D4C5F63171CA,
	MouseHandler_U3C_ctorU3Eb__20_0_mFFC8E33D6492E1C435E05EF724856F6D44EAD689,
	TouchHandler_get_CoordinatesRemapper_mE1F66B8CEA2952E84CB0585E40E23D8FC39E5957,
	TouchHandler_set_CoordinatesRemapper_mC8243F4495D92E4D5C1C6BCA921B23A3EA07347C,
	TouchHandler_get_HasPointers_m23704054B25FEEB86E3042CFCE583CBE8705B076,
	TouchHandler__ctor_m44934BEE553CC11DC6C8DF48D0AC82FD2474AD5C,
	TouchHandler_UpdateInput_m7A9D719FDC9C7280DF53B12481E3ACE3C1AB921B,
	TouchHandler_UpdateResolution_m73E09088A33EE0527CBFDC3B006CFCCD4E1227E0,
	TouchHandler_CancelPointer_m0352D2E599DFAFCC574C2FDCA757DAB4CAF2C7B1,
	TouchHandler_Dispose_m764D6585A1D1EEC17799AF85333AB73A782C5BB1,
	TouchHandler_INTERNAL_DiscardPointer_mCCAD128F15D006D87E9C7091B5BCA0671E7F55B7,
	TouchHandler_internalAddPointer_m6C8BEE080B5F96D93382D780CAF7D4A228AAF361,
	TouchHandler_internalReturnPointer_m1CFA9EAD8E0C8C3F41134DDAEDE020FC3557C53B,
	TouchHandler_internalRemovePointer_m3C3B29C1CF186668C54CD32EE5D830C9041892AE,
	TouchHandler_internalCancelPointer_mE1B9DE2B4D31112A2F4704C1195996885BC55273,
	TouchHandler_remapCoordinates_m90AF579F604F825834CE570B4EACC95790B67FE3,
	TouchHandler_resetPointer_m0113909AAD3568FFDA7EF957DAC9824CB92F91FF,
	TouchHandler_U3C_ctorU3Eb__16_0_m288B739A7E91AC84C9B96437256382C2105C7C2A,
	HitData_get_Type_m72692ED1AEB9B5DB43169456255E7B81D5BFFE54_AdjustorThunk,
	HitData_get_Target_mD8CF226FD6A5401997C2F6F554C2BA26155F7A05_AdjustorThunk,
	HitData_get_Layer_m80AD3383FA69E902C6C6E7B29769771F24EAEA51_AdjustorThunk,
	HitData_get_RaycastHit_m3E212799996C2759827EC25ECEC4861E7A0FF9F2_AdjustorThunk,
	HitData_get_RaycastHit2D_m1A8ECB492182FF3B9173BFDC61F958D4EF0353F0_AdjustorThunk,
	HitData_get_RaycastHitUI_m869818385A3B8348A5F5CDCDAA3F798A52BA6AB4_AdjustorThunk,
	HitData_get_ScreenSpace_m42D38D84E89E1AAF2BFF256515216391314F2673_AdjustorThunk,
	HitData_get_Point_mE05135C35317136F666766096410B03CC4B2A3F5_AdjustorThunk,
	HitData_get_Normal_m8D28B6400EE26784998ECF9C9E11ECC898736694_AdjustorThunk,
	HitData_get_Distance_m3FD9927E15997EBAE1F6BC4874CBA1BDB9DDADA5_AdjustorThunk,
	HitData_get_SortingLayer_mC1E6AD3168188865B0906FBD69328BD6EEB201B1_AdjustorThunk,
	HitData_get_SortingOrder_m8E8A408700F4481599B43098F197D1E56D60D2B6_AdjustorThunk,
	HitData__ctor_m5E8312FD3DDD4F5323F744FF247510CE454AF15A_AdjustorThunk,
	HitData__ctor_m31CED98457E52ADFD68C498CFDEBA662C99DFCDD_AdjustorThunk,
	HitData__ctor_mBC74645ACB5450B9CA6DBBF8B551C1AB1635FA73_AdjustorThunk,
	HitData__ctor_m76EA363D8D703942F812B954CA35AB9E0F9152FE_AdjustorThunk,
	HitData_updateSortingValues_m83995CF9B001FA94DA5A87AA350BF40A0687012A_AdjustorThunk,
	HitTest_IsHit_m24CBD5CE26C34C8C715DBA39C141006EAFF4222E,
	HitTest_OnEnable_m2870E2D3CCCA2276C415F57B73127BB37752F537,
	HitTest__ctor_m70957C00CC9BFB35FE18EEFB0D3B560F20C1872F,
	Untouchable_IsHit_m4B38CC5DA7FC55389BD8097CE066CAD93ECA923B,
	Untouchable__ctor_mCE3C4B8158F85CC030FC7B5BB93E169641E8382C,
	FlickGesture_add_Flicked_m25DC9931033826699D50DA7D349CAC6F17BAA985,
	FlickGesture_remove_Flicked_m4BA5DCCCC6C46D8DF2B4794F0A8772A80B128738,
	FlickGesture_get_FlickTime_m215BEE64D528CDB9C8F68917095A7A7BD0B295CC,
	FlickGesture_set_FlickTime_m2BF8570CF57C9BAF4DEB3FD940C719471F1235A9,
	FlickGesture_get_MinDistance_m4B1BDE629056EC030B2094FB909A4C7FCE54B389,
	FlickGesture_set_MinDistance_m768E1EC2A0C12856DF491A757C55A3B5F540FAD1,
	FlickGesture_get_MovementThreshold_m611FC9C1847F17CCDDD395C7BEB7D1951FC9E8F6,
	FlickGesture_set_MovementThreshold_mEB25245068FAA667698AA456C02E7C38171588A3,
	FlickGesture_get_Direction_m5E86A6551551247A416300357BC3AEF1D21EC374,
	FlickGesture_set_Direction_mC2B4619328F9ED4A770E8834FAFD62565592AF57,
	FlickGesture_get_ScreenFlickVector_m45978D06D8A07EF930FA4F40598B28D2B522DDED,
	FlickGesture_set_ScreenFlickVector_mD894B27E22DC8F9C26D9C1FA67AFAD7668155DBD,
	FlickGesture_get_ScreenFlickTime_m85159730FE776C6BD03CCD88FF9D9AF7201E2A11,
	FlickGesture_set_ScreenFlickTime_m34E593B24DC48C53214F2774767F746F6EC281E7,
	FlickGesture_Awake_m14707660D6AECC0DB4F3A54A9CA2E7045A627C93,
	FlickGesture_LateUpdate_mF819EDDAD9C47CEBF737E8F7AC6E6D7C079311CF,
	FlickGesture_switchToBasicEditor_m39EA45D74BBBBAF6B6B91DEEF795E79806F2E90F,
	FlickGesture_pointersPressed_m621B8B6A45C9E05E1661594A0D6FC79FA991093D,
	FlickGesture_pointersUpdated_mE200896F5F9C5A9D9296F8C6C730BC3D52724C07,
	FlickGesture_pointersReleased_m4EA4731BD56EFF6723500B60C38A1C43BADDDB9E,
	FlickGesture_onRecognized_m9190FC5AF6465C19CC73CF4C2D5616B3AD7C5A03,
	FlickGesture_reset_mF6D48937FFC8391F0A9D5A882B5D82E40B29B79E,
	FlickGesture__ctor_mC347B1523AA37AB48B291576A58BBC9EF51C2306,
	Gesture_add_StateChanged_m1BCAF2D266C1AD01E7B3BD0B81844144800EDE12,
	Gesture_remove_StateChanged_m112B45165CDF6471EDBFEB4670211ABDEF08AEFE,
	Gesture_add_Cancelled_mEFC45E21916CBB7EEA6B30762E82F33900BD7979,
	Gesture_remove_Cancelled_m18F0A3A2D1D150D662A0832D87448CE33A65C5E8,
	Gesture_get_MinPointers_mCEC72B25F58BBEB7F3DBCACDD5CFBAB8CE9C627E,
	Gesture_set_MinPointers_mAAFA65A244E5B47017F26E9B41DE4158A03578E6,
	Gesture_get_MaxPointers_m43485ED37836BA5C1F1A3D1792ED9F8117CA1365,
	Gesture_set_MaxPointers_mE1F8C7696DB473AC8298089B44A411B3D0495E0C,
	Gesture_get_RequireGestureToFail_mDF307189244CA4A9BDF25FFF97BBAED111E2096E,
	Gesture_set_RequireGestureToFail_m9159F9B75D878F4EC9D8851D304FA77C6CB2AA47,
	Gesture_get_UseSendMessage_mFC0CC0A4B4ED478240FB7E74D91403C24B5207D4,
	Gesture_set_UseSendMessage_m5AAD7EB2DED01C096D68C56D9AC0BF11A0912CAD,
	Gesture_get_SendStateChangeMessages_m1FD7F2330D182C8EE803A54A19D40B729B175F26,
	Gesture_set_SendStateChangeMessages_m36A68209D5DE7DFB8B441C37D414C3D866899EAD,
	Gesture_get_SendMessageTarget_m7185B42B012F3F599A09561EA43284FC932609E3,
	Gesture_set_SendMessageTarget_m44394AC4BEB88D3BCB660E2907BEE6A1DFD73F05,
	Gesture_get_UseUnityEvents_m4CEDD4AF4C5996901DC37F6A1C9496B65235E0D4,
	Gesture_set_UseUnityEvents_m726AEC4B54A363C072350A32E49DC515D50C0581,
	Gesture_get_SendStateChangeEvents_m8F28A6B70A4A673E35FB7F45EBE44A4C85401255,
	Gesture_set_SendStateChangeEvents_m0A109722474680D7AFB0023B084C1C61B923DBF9,
	Gesture_get_State_m0D84F23AC52A6740479C86C64E800627C3AE4932,
	Gesture_set_State_m5FC1A99C59CC058997EC32F1947E05D3E0E3B909,
	Gesture_get_PreviousState_m45BD994576B1B3734E494C2DE797364396149ECF,
	Gesture_set_PreviousState_m4B719D37BD834F63D486E7B74E9FF750DA9C2AD1,
	Gesture_get_ScreenPosition_m94AFB3991D358396A6A8BFA81E97D570BCCCE385,
	Gesture_get_PreviousScreenPosition_m29915BBAD0AF5F3FDD359C7C98C8347B567102F9,
	Gesture_get_NormalizedScreenPosition_m01A99749863CBAAEC3C80E9BC2AC173C4B629C29,
	Gesture_get_PreviousNormalizedScreenPosition_m8356D2596C45CE831BAD1180B0B9431B993679D8,
	Gesture_get_ActivePointers_mFDB15216DCB336A42687B31F9ED1EB9880F6C09D,
	Gesture_get_NumPointers_m0F38BE67443C4798476B8049F6ACD1F668910B62,
	Gesture_get_Delegate_m5FB26ADAA6F5E9E31BC0BC24B72A142837740CAF,
	Gesture_set_Delegate_m7768C92C41F47FFE8C55D2FF9BA71598CB6801AF,
	Gesture_get_gestureManager_mF4E49D625D490BE21BD14CE9F443FA0C50F5E3D2,
	Gesture_get_touchManager_m0F322BA03472D79D25C0CDD90E4A712304A31FE2,
	Gesture_set_touchManager_m3E48247E09C6B82F21696B4180C176E3F9AC1429,
	Gesture_get_pointersNumState_mF9306D62AA0E8CA56224D2B268F0A9284FE8F295,
	Gesture_set_pointersNumState_m930CD5744B26B49F33C7B296CD3A1544AA042EEC,
	Gesture_AddFriendlyGesture_m8D3E9186679834BA76E164EE88F82AB834E4E0A3,
	Gesture_IsFriendly_mCD9AD811FD29116864F63442611961B84A0524A1,
	Gesture_HasPointer_m273A1D2D7CE4EA36785CA1FC6DA0583BD81E5F7F,
	Gesture_CanPreventGesture_m1571AE65E916FBD991FFEC8E5E1E43116E53001E,
	Gesture_CanBePreventedByGesture_m0D3028D97030EA7246DFABB0FE617A18790A4983,
	Gesture_ShouldReceivePointer_mF853BA9BAF9AAA6B1099E8379C6DC3206FC155EF,
	Gesture_ShouldBegin_m4F1FCF546A904D434A9A47C44FB007A872516F31,
	Gesture_Cancel_mADAD69AD0365AD677BCE8E71BEA4AF3EAA9BF7A3,
	Gesture_Cancel_m1216C968046C72D027F0FCC69F6D998E922FEE9B,
	Gesture_GetScreenPositionHitData_mF922F6A0FCB47F4B27A875D027F15B7DC7C65008,
	Gesture_Awake_m7B0102253D74F10003FAC561B60780BDC3EE73DD,
	Gesture_OnEnable_mB9F38A24E250F505C806E28D91CE9A46F9519BA9,
	Gesture_OnDisable_mB44F68A8CC3FBD3FD137ACE267C87D484B950AA4,
	Gesture_OnDestroy_mEC27FB48B392517CF2B9E0D8FC875B702A0A25BA,
	Gesture_INTERNAL_SetState_mBCA7FBC10C327E4785C6734A698C3CBD7DCEAB4E,
	Gesture_INTERNAL_Reset_m05AABC9DD5A39DF1A83168607C8F3DF58DE1DD6B,
	Gesture_INTERNAL_PointersPressed_mB5E7041D74FA932E4F88B349F6EDE25D907ED897,
	Gesture_INTERNAL_PointersUpdated_mB5756EAF7CE48E8FAD19F236A78552EBE827FBDF,
	Gesture_INTERNAL_PointersReleased_m74440EA14EE70040408453E951D35302831F1774,
	Gesture_INTERNAL_PointersCancelled_mA8A367344AE15EE109E4DFE41E6048C148727ECC,
	Gesture_INTERNAL_RemoveFriendlyGesture_mE54CC45493BEE6D99F565960061A951A4754933B,
	Gesture_shouldCachePointerPosition_m11D2EEC7D30F8BB59FD9667CC1AFC4C9637E1CB3,
	Gesture_setState_m65E096FD83407A65F2C1E9A338D269C7E3975631,
	Gesture_pointersPressed_mE351C2D0EF6FF70D2BDEA0B23F3DD8878809AA41,
	Gesture_pointersUpdated_m5002285C2C3500505C9FA6CDEDD0098AEC5441CC,
	Gesture_pointersReleased_mF20DF89E4F5B96754807D1964F69FC3CECFEC2C7,
	Gesture_pointersCancelled_m8420CFF19089E06B9C6953A4E3285B7537A97E7B,
	Gesture_reset_m1877C9E7D1C1EE6850A00785F6FB9012196C362E,
	Gesture_onIdle_mD95A7535FC8D1784F6627785680597B19D546788,
	Gesture_onPossible_mB2F2C913DDD161D4391A920A235819DB575ABEEE,
	Gesture_onBegan_m3A7F927ED4FC4BC1231B9EB73744242A50D3E588,
	Gesture_onChanged_mFCDA69921ED330AF1E607273442DB7A725306568,
	Gesture_onRecognized_m2EC7E369EC4E9DC87DCDBA2BEC50C2B67023204D,
	Gesture_onFailed_m1F03D1EA870EE52C4DE171EBABDAE7A2D203CE2C,
	Gesture_onCancelled_m83D6C0B7DD77A449F3B5DBC8B65F40FD6A982385,
	Gesture_retainPointers_m5C712BA2471ECB9BB5808ED4CDA989AA6088F0B1,
	Gesture_releasePointers_m16E842498B3C77FD4CD7CC1AAA3BAF6C61DB36C2,
	Gesture_registerFriendlyGesture_m5402E6D46A79ABE77536ECFD27868EC6745DD4DD,
	Gesture_unregisterFriendlyGesture_m0FF702A86CE10153C4E95A50E344559F3AE3F3D5,
	Gesture_requiredToFailGestureStateChangedHandler_mB15CB1A9E6127FCCD9A4AF492C47811909DB55E5,
	Gesture__ctor_m3193815F0F71D3DD154CAAF8ED6BD6CE7DF1D3AA,
	GestureStateChangeEventArgs_get_PreviousState_mC8F6C4E5F98A62F6AEC8534F41A2744A5FBCECD1,
	GestureStateChangeEventArgs_set_PreviousState_mA70EEDA3FA1A2464A96BA2D5C594467E4D2E57DC,
	GestureStateChangeEventArgs_get_State_mA634588E846F16B0C631A9CF3EF67C7CD8C4E47C,
	GestureStateChangeEventArgs_set_State_mFADFCF11FEAEBD70A279C3D56099E10F43170322,
	GestureStateChangeEventArgs__ctor_m00A1F3CB76D570409E14E5A9923FBA738D15C902,
	GestureStateChangeEventArgs_GetCachedEventArgs_mC9EA1DCA6C5F6E357EAA24384EB26065CF40015C,
	LongPressGesture_add_LongPressed_m4B45E6ED6B7973F1C1FDDB7F09D31187EBF5F10F,
	LongPressGesture_remove_LongPressed_mDAB3DD929F7073C371213304D189437528C8DFD0,
	LongPressGesture_get_TimeToPress_mF8C5BB1CBC2ED7043D66C4DD0FCCD7BAF7394778,
	LongPressGesture_set_TimeToPress_m18DA7B12F4583FAD79397BA8BCF21A375D0D8892,
	LongPressGesture_get_DistanceLimit_m0F395C22DF93ED07EF4265D651797B0911CC2032,
	LongPressGesture_set_DistanceLimit_m9DA9721518E6DD5F0FE955049C1A1BCCEF397F39,
	LongPressGesture_Awake_m4F2A7E0B63A4E5B81A12F71202D9A261C83E755B,
	LongPressGesture_OnEnable_mD365C46D4CA7F6BD84CA4DBC0A53A901907C15E6,
	LongPressGesture_switchToBasicEditor_m371D46E470650C066CC25CB988BC1EC740133CAC,
	LongPressGesture_pointersPressed_m361AF3751035D72FA517B6E83DB9DF7ACA08231D,
	LongPressGesture_pointersUpdated_mC4B1C839368730B4E031F37E523B8729C7D05B96,
	LongPressGesture_pointersReleased_mAAB8D542E75EE92E5081BC0CAEFADE4CB6BBA659,
	LongPressGesture_onRecognized_m18F8F27661C36A970657B6A2E9A2EE20B5D24107,
	LongPressGesture_reset_m124D31C79F9E7FE0D7A044B61140AD8227F9EA4E,
	LongPressGesture_wait_m035EA290BA50196DD1522C755646A00EB7E3F9E2,
	LongPressGesture__ctor_m6CD790DCCBA99DF053E13CD9DF594CE9566C2A11,
	MetaGesture_add_PointerPressed_mCA3510DA9CFEA858C7D2208309D45D45BA6E54CA,
	MetaGesture_remove_PointerPressed_mD979507A2AE07F5FED1F8ED4660941E8BF3D3F1A,
	MetaGesture_add_PointerUpdated_m16EB208373340961AC7ECD7F5D3E56A6C007B19D,
	MetaGesture_remove_PointerUpdated_m5780B8BA04B9140B2AB0E45F9C8C698CED706DDF,
	MetaGesture_add_PointerReleased_m349D749D6D9B1589B23B99C2AFE5DBF493BF4285,
	MetaGesture_remove_PointerReleased_m5CF40F31C74C22C1B60724D95845D5D05E1AC628,
	MetaGesture_add_PointerCancelled_mDBD446F95A464C9600389BFA04060ECE486DFFB0,
	MetaGesture_remove_PointerCancelled_m72C785E59A802C9A510B7301774F5F42C73076C2,
	MetaGesture_Awake_m260A11DE49B1ACCE1EFAA92FCB69DF6C87D8E48E,
	MetaGesture_switchToBasicEditor_mF32681D33EC3AA453F195DAAE300D4C5F220DB8D,
	MetaGesture_pointersPressed_m174571D9EDA6C2DC1BB37DE18E635E9C8DA37216,
	MetaGesture_pointersUpdated_mBA996B7F2B35384BC2E97004C363637461127C82,
	MetaGesture_pointersReleased_mE0A9391573C45D4BD3E64D72815C957A92D784D8,
	MetaGesture_pointersCancelled_m75699B19966A5B671DBCD64644088942B60967D4,
	MetaGesture__ctor_m5AFE49DAD3936F3A185B2FB374252216787758A5,
	MetaGestureEventArgs_get_Pointer_m5FC69D23F4C4D7E1B0BB6261319A31AEFF003E99,
	MetaGestureEventArgs_set_Pointer_m044D11B147DE05937085A1700F85BDBD994B77E5,
	MetaGestureEventArgs__ctor_mBCFC9E0CD820143A730F9093AB2A7C315A2CDF31,
	PressGesture_add_Pressed_mE8A33C4F8D0B7A3BD09B120392BEAEB30F6671D5,
	PressGesture_remove_Pressed_m17AC24C997D01943360F20CD82F8B25C2FFB8AF1,
	PressGesture_get_IgnoreChildren_m4E570945B8FB179E91375B41C5815E98E061F0DF,
	PressGesture_set_IgnoreChildren_mA13F046D944484E60955D73D77059AE54F2A0D42,
	PressGesture_Awake_m7CA2183ED9CE9BA43A960A096371D53BCEDACA8C,
	PressGesture_switchToBasicEditor_m352B5C7CC6D91651CFE06C58314CDAFFD8B8C927,
	PressGesture_ShouldReceivePointer_m8E0B1A9C4D2EC3E00013752077D5D4B8BE17AF0A,
	PressGesture_CanPreventGesture_m1B8726CD6EDB3632FE1513CDB3F5B169A39E2BB9,
	PressGesture_CanBePreventedByGesture_m1FE96F8DA722247E9FE37922AD7AC65C5A1C3328,
	PressGesture_pointersPressed_m5DA6103F48DEDBA1D3D8E42356580432F21CDA76,
	PressGesture_onRecognized_m3284AE0DC5163EED04198B24285C3DB99464BF1F,
	PressGesture__ctor_mFA59BDC8DA4332F2002B5021E8B8A4199932CA4C,
	ReleaseGesture_add_Released_m9FAF03CFBD0EF46DD75A2F6A0323CD90492CB6EF,
	ReleaseGesture_remove_Released_mA9A9644038AF96D6569C933E85682051499FEF3C,
	ReleaseGesture_get_IgnoreChildren_m2EEBF77AE7AD9D837F48B636C22035A5166C7CCA,
	ReleaseGesture_set_IgnoreChildren_m0B3851DA48E59F4E3F52F5111A86E92B77142CF9,
	ReleaseGesture_Awake_m4A5F22806710E26562EAB68B46A2AD80B5F3AC21,
	ReleaseGesture_switchToBasicEditor_m8C7FFFE3FABB907252FD858AC4193477E7D23268,
	ReleaseGesture_ShouldReceivePointer_m924D5C58C2ABDED44E1CFAD99F6B19D405F386C2,
	ReleaseGesture_CanPreventGesture_m597D9B130C72D69BE5E229393AF63F884CDF21ED,
	ReleaseGesture_CanBePreventedByGesture_m40F83EE1FFB6716B672E42F4E0DA49A79C22A7D8,
	ReleaseGesture_pointersPressed_m917F2E470ECDF34AFAE73DD4322155ECB3B15A95,
	ReleaseGesture_pointersReleased_m9D4810D08CEECEB49D3C859EF7697B26D6B2E500,
	ReleaseGesture_onRecognized_mD65FAFB5B3F6615FA08647FFF1A9A7374B0AD096,
	ReleaseGesture__ctor_m54E19C1EE9C1AA15371FC64AB79D86B0F767DE88,
	TapGesture_add_Tapped_mF661A59B8DC9305017A63DB2B36924CC60EF2084,
	TapGesture_remove_Tapped_mC85E95CD41880860BA7CEE1E61D50F6F6B7AD6D6,
	TapGesture_get_NumberOfTapsRequired_m3A7DEE0DE012DA405CEFAC5A411B3816E7A28AD3,
	TapGesture_set_NumberOfTapsRequired_m43C3FF739D39C91F2E88477423ED1BF60C2F1444,
	TapGesture_get_TimeLimit_m2922A5F19DD5AE3EBDB67EC0499EC2E385A26497,
	TapGesture_set_TimeLimit_m70FB17044FD91D34BA2D0A51A9AE10762FB24836,
	TapGesture_get_DistanceLimit_m5B29383686D9B40390AC83F44DC97677929D5059,
	TapGesture_set_DistanceLimit_m08D4C4F6407B855F1DD21158AF1C69C46D23D036,
	TapGesture_get_CombinePointers_mEFBCCA33FA7E9B50E4CEB85EB7A4AD2917B73F87,
	TapGesture_set_CombinePointers_mDCF54CFDE2AB4E71951544030EE5259CF4078325,
	TapGesture_get_CombinePointersInterval_m1E1C8C6686EAAE2DE8D4EBF07B4C1D3AECEE6DC8,
	TapGesture_set_CombinePointersInterval_m63212174909D07D0D098FC81EA8C26B22DFDA968,
	TapGesture_ShouldReceivePointer_mAD70FAEAED4B2DB9A08D36B6E01E39DD9D588AA1,
	TapGesture_Awake_m3ADEB3D00F2DC16F2B4683D37164E0B80B827478,
	TapGesture_OnEnable_mD38A2473AB9E02106737C7370C97D5F30FB61E47,
	TapGesture_switchToBasicEditor_m2E5A3D4E13198C90C4423FD33959047B11FD4D14,
	TapGesture_pointersPressed_m856CEC40B06BC33E6C4B285AE87DFC1A9E38867D,
	TapGesture_pointersUpdated_mF45F4F8BD348A3335679650323924311F82CE46C,
	TapGesture_pointersReleased_m376DA436237A94B1E7AA0DCF9D7120D72482DBEC,
	TapGesture_onRecognized_m8CCF554F27443CBA96EB1059258270851C826796,
	TapGesture_reset_mD066D1B207DA42D38A18C6B4F09A98A2D21DD98E,
	TapGesture_shouldCachePointerPosition_m1646205532C69142A5893AB67D58F4A62EC7B2B0,
	TapGesture_wait_m0D00DEA2E4E2D13109939092381264E87669E75C,
	TapGesture__ctor_mEA50242D0359C196E1F42112C614019CF85E24F3,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	PinnedTransformGesture_get_Projection_m144D5A9CD9CA71432B0B287FAC4F6146559A6089,
	PinnedTransformGesture_set_Projection_mB26E70F3EE37D51656AF825CA95B4B1759945B93,
	PinnedTransformGesture_get_ProjectionPlaneNormal_mB2B39FE5B4E893CCA9145300682720A2FD37A764,
	PinnedTransformGesture_set_ProjectionPlaneNormal_mB41D10CFCB39EEA5398E94B4303EC1A610A7C4CC,
	PinnedTransformGesture_get_TransformPlane_m9A7AAF95D6DA97886F35B5C70B2EA0B0956A6BDF,
	PinnedTransformGesture_Awake_m78C2AFCA4DC848DC4EB09BC6066DE7AF0EBB2322,
	PinnedTransformGesture_OnEnable_mA76F7192E1E2E9DF1ABCB657CFC7D80082C3E70D,
	PinnedTransformGesture_switchToBasicEditor_m26C73E2FB53A9C3C386F8ACBDA3B9A0EB388AEFA,
	PinnedTransformGesture_pointersPressed_mCE6475C8D7AE361A411532DAFFCF1FF51FBAA5AD,
	PinnedTransformGesture_pointersUpdated_m172FA4313437EA22DEDA34B3FF7D3E7BF861EF84,
	PinnedTransformGesture_pointersReleased_m5C37F010A2E7DE155FE286EBB3F5304D1B890C6D,
	PinnedTransformGesture_doRotation_mF20E021207EC336F04CA723EB446EA89D1697D6A,
	PinnedTransformGesture_doScaling_m899C2062740436FF12516D413C119ADC7A55949E,
	PinnedTransformGesture_updateProjectionPlane_m50DB123DD037BAA9D1EB66B1A53145A252B73354,
	PinnedTransformGesture__ctor_mA92FF69FB4CC50AFE4FB3370743065B256B47F35,
	ScreenTransformGesture_Awake_m2695BCFC0987431BE03FDA2262D6A841927CC4CD,
	ScreenTransformGesture_switchToBasicEditor_m322A5452DC6BFB94D72595FA985A423116BEBCF1,
	ScreenTransformGesture_pointersPressed_m95A7C353AE47FF044ED696D44090EF99E72B8756,
	ScreenTransformGesture_pointersUpdated_mF69740ED4C2ABD57B10362E98F62C1F14EABD503,
	ScreenTransformGesture_pointersReleased_mE4BD33BC4DF4ABD1687691CA98E55CBEBCB8010D,
	ScreenTransformGesture_doRotation_m8F3DE09FA1D91038239B00231139FA43D55CCA8F,
	ScreenTransformGesture_doScaling_mC69892BF67ACBED5B948707C9AF2CD51232F3572,
	ScreenTransformGesture_doOnePointTranslation_m43783CF266086DA047C4FADD9AF4AAD790A267B6,
	ScreenTransformGesture_doTwoPointTranslation_mD4C34DCEB79ADAB7BBAD41DD5801DC290598BC66,
	ScreenTransformGesture_scaleAndRotate_mF48A1C058F9CA2B0B5351153E3A2EFB405E7D6A2,
	ScreenTransformGesture__ctor_m7C0D01AC89C3FDDABD53E05CF1A5F344910009BA,
	TransformGesture_get_Projection_m477905703CB42C1A39C61BB3A33693EDC18CE43C,
	TransformGesture_set_Projection_mC79E4ED53F4C9B77DB8836589EAD0A75356431AC,
	TransformGesture_get_ProjectionPlaneNormal_mF91505FF99F0694BA68F6763C5783247715831FC,
	TransformGesture_set_ProjectionPlaneNormal_m8BF0B251A756CB66C6F4D7043E606CEA0E765300,
	TransformGesture_get_TransformPlane_m6709A7E604E7FD46D9438A73AD8AB21D9A29403D,
	TransformGesture_get_LocalDeltaPosition_mC526FF6DAFDBDC31CC21AE741A277ACEDEBA1F73,
	TransformGesture_Awake_mF3E7793B89BFC6AB3E77074CD6D2D0CEDB95E2EE,
	TransformGesture_OnEnable_m2406964A10313A0EEC20C5C6F7D423612078BA76,
	TransformGesture_switchToBasicEditor_m8C74480E9FA3C4A529F989567E287B7D4C6BD231,
	TransformGesture_pointersPressed_m0A94E1827689D601E0A43CB9945DD58A7E6198DC,
	TransformGesture_pointersUpdated_m783B60D663A26FDE6A7BE4BF9B5854BA065A73BA,
	TransformGesture_pointersReleased_m14930EC872330DB5509EFDB80F42A6CBB8CC9ACA,
	TransformGesture_projectScaledRotated_mA4F89105C1928C8EBF4823DD79DB5B80DB13AA73,
	TransformGesture_doRotation_mB981FE34A3482C8D16E880352A29F58A61837A67,
	TransformGesture_doScaling_m96CC632F2FE01A4A8A7211C013B260FA1684F1DB,
	TransformGesture_doOnePointTranslation_m45BFBDCABC861526639CD6BA387076F2C964FC2A,
	TransformGesture_doTwoPointTranslation_m564FB1BDCCEBBF1CB1E768784CE0A292EA9D35D0,
	TransformGesture_updateProjectionPlane_m4919B830642DAA3EE03352C530B6823042E221E4,
	TransformGesture__ctor_m356E940E38468ACA29EBBD47C143F6EAED5E6F49,
	ClusteredPinnedTransformGesture_relevantPointers_m5B1D72381AA391603882CFBB02D8C190D797647C,
	ClusteredPinnedTransformGesture_getPointScreenPosition_mB2935F824A1486CD544331CD45655FA0E3863101,
	ClusteredPinnedTransformGesture_getPointPreviousScreenPosition_m61913E4FEB8C5038379DACB1292D90711E4AF782,
	ClusteredPinnedTransformGesture__ctor_m284181179FCA98C2407DB16683BC3F057EF112CE,
	ClusteredScreenTransformGesture_pointersPressed_m322BE33EC05ACC998093BC25DA024BDB70FBF577,
	ClusteredScreenTransformGesture_pointersUpdated_m74CF2EB3F313FF27241BEBAFCA330481417261E7,
	ClusteredScreenTransformGesture_pointersReleased_mCE27673BE69A01E93D8CC74F76F395F3C1D3B083,
	ClusteredScreenTransformGesture_reset_mE2AF3CB7EB213A0A994CE230FBE87B4093C68E61,
	ClusteredScreenTransformGesture_getNumPoints_m4597026802232E74459ECBD519953E62069B43E8,
	ClusteredScreenTransformGesture_relevantPointers1_m3A0410D96AA0F659E430AC653B1F2F0570D97380,
	ClusteredScreenTransformGesture_relevantPointers2_m86EE3F98FB33F830AEC5040308C01C81E0280F5F,
	ClusteredScreenTransformGesture_getPointScreenPosition_m8EC0B56F1A2E33CA0A2500E2A4A419BC4A6FB697,
	ClusteredScreenTransformGesture_getPointPreviousScreenPosition_mF647F6B1742070902C0878F2C51ABEF5D5809CBC,
	ClusteredScreenTransformGesture__ctor_m628C61571486E91996BC06F03C348B24D0DC544F,
	ClusteredTransformGesture_pointersPressed_m62B5CD651DC2EC0FAAE91B843B8622592144F346,
	ClusteredTransformGesture_pointersUpdated_mFB4AFF1A068F4E972D1457F9940C342ACE6F4CA4,
	ClusteredTransformGesture_pointersReleased_m127A9D0255133C6015DE9D8DD9464D524BB6DC5B,
	ClusteredTransformGesture_reset_m6EFB94326F15EA87754DD473469E447F4D620BEA,
	ClusteredTransformGesture_getNumPoints_m5F90023F68B027B3B79AFCA618787A645EEB749D,
	ClusteredTransformGesture_relevantPointers1_m48382D5561CA1AF43251790C47E9D5BCD26111A7,
	ClusteredTransformGesture_relevantPointers2_mBD9617B9F0F6F6731392FAB4223E0968B4285ADA,
	ClusteredTransformGesture_getPointScreenPosition_m60EB5B28C37CC944270AE9669C7A3A68E4A131AB,
	ClusteredTransformGesture_getPointPreviousScreenPosition_mDE398C1E6AABC00C4C1A34A9F2CA69D1CF5F881C,
	ClusteredTransformGesture__ctor_mD3F4BA0B04400EF3510D0B652B9DEAAA4D566006,
	OnePointTrasformGestureBase_get_ScreenPosition_mFE43C3CB8E66E411FF0737FB1A68F2027590D3BB,
	OnePointTrasformGestureBase_get_PreviousScreenPosition_m4FA6DD6CF83388FF79A8F05A22DC6780BB2AB21E,
	OnePointTrasformGestureBase_pointersUpdated_m1B2BCBBEE6D6DFED6EECC6956547E11EEBF72758,
	OnePointTrasformGestureBase_reset_m8CAB822CB55B4812EFDFC6C9C8965211ACA569A0,
	OnePointTrasformGestureBase_doRotation_mACC51AAFA98D4D8697197B8DBEDFBE730A9D8689,
	OnePointTrasformGestureBase_doScaling_mB77719083A920644994370B08CC9001A034520A8,
	OnePointTrasformGestureBase_relevantPointers_m79CB15A1707716F82F59D362C9FBA13A15D74F4C,
	OnePointTrasformGestureBase_getPointScreenPosition_m8D19342A1F8E02251F814949651F005DF8D951DA,
	OnePointTrasformGestureBase_getPointPreviousScreenPosition_m5A67B366DE89C2158F36674B7380B45B4438AF80,
	OnePointTrasformGestureBase_updateType_mE287D7BBC6800B7518F1810AC08E82264A16E658,
	OnePointTrasformGestureBase__ctor_mC1159D5A73AE549B718FEA59A3634643E8551764,
	TransformGestureBase_add_TransformStarted_mF91D27FA97873A130853A7839890B247C4C4BB14,
	TransformGestureBase_remove_TransformStarted_m2687BA12F20384D1D4246B6952E89747AB12DEF1,
	TransformGestureBase_add_Transformed_m9D3F8364C7BCD8ACA78C36F98256EA7C5F9B9DE4,
	TransformGestureBase_remove_Transformed_mFF853747145C63373B2A3BC0FD2A3A084C067026,
	TransformGestureBase_add_TransformCompleted_m3BF7A1D9F541C37B5D9D728AE444E1AD406A65FF,
	TransformGestureBase_remove_TransformCompleted_m304D26C65653C89F17CDCEEB2008CC5BC8A03ADE,
	TransformGestureBase_get_Type_mF0095E9F1203C71AE506C88CB5E9C2A06B960884,
	TransformGestureBase_set_Type_m92276E46B69A7116D45E86F06BBE5B4A7E8638F4,
	TransformGestureBase_get_ScreenTransformThreshold_m8478C5E8681B1AC7E53F253C2D1E226ED5E3C5D6,
	TransformGestureBase_set_ScreenTransformThreshold_mF2E5CC587D28A5854F3D23027A407EA70A2CE2B7,
	TransformGestureBase_get_TransformMask_mD7AF3DB99B9F96728563601852A4F33D37CA1D2C,
	TransformGestureBase_get_DeltaPosition_m4EFF8A2C639EE3080E2E45C42D82BDEA7E0B6145,
	TransformGestureBase_get_DeltaRotation_mF862E11D69D3B40CC672A12376A10AEAF8194E93,
	TransformGestureBase_get_DeltaScale_mF5A26CA4B1B761BB736A361650206CEA55332385,
	TransformGestureBase_get_RotationAxis_m16611C3E1F587DCEE6EB2FF728A354426F3BF5A4,
	TransformGestureBase_OverrideTargetPosition_m830A586C2CC9B39310975F8157C4F420E1FA1824,
	TransformGestureBase_OnEnable_m980B3F064F91093F42959C24C2AFACC051B65059,
	TransformGestureBase_pointersPressed_m0F719C9148CAC1B065B631F1FB63C83F421624D1,
	TransformGestureBase_pointersReleased_m1F213A07C0E74B38C35D785F62684E9BECAFB97D,
	TransformGestureBase_onBegan_mE3E03A3EBC745AAE7419CB8D848E462CF88D6E3A,
	TransformGestureBase_onChanged_mF1204496E151E56CA282DE624B1C1504974250DD,
	TransformGestureBase_onRecognized_m21F74CD27877A0410B102020D881A5AABE83E3FE,
	TransformGestureBase_reset_mA318E5BAFCD83EFB315825C034169FEE14268DFF,
	TransformGestureBase_updateType_mE6DF64E94F57A533B69DEE03219288514D908CC2,
	TransformGestureBase_resetValues_m247BBB5232D7FD38F976BA63F70102BB7CC0CF8D,
	TransformGestureBase_updateScreenTransformThreshold_m9C9B722B168D3A100D02E63CD2C03994F902159B,
	TransformGestureBase__ctor_m778AEED4BF0755E385B29562F1B031A2A46E1149,
	TwoPointTransformGestureBase_get_MinScreenPointsDistance_m7CA529C28C28A98F5ED0075A4607AA94095CB461,
	TwoPointTransformGestureBase_set_MinScreenPointsDistance_mDB4D71ABB8DA3B3DE9304E62F4C2305F36280412,
	TwoPointTransformGestureBase_OnEnable_m808CB046127E7E7781F30052E390E9CB88A3B59A,
	TwoPointTransformGestureBase_pointersUpdated_m79FE24D31050A9FCC9DBC19AFB829A46DAFAF326,
	TwoPointTransformGestureBase_reset_m701D1952139F26E4C07249BACDD05E33987F0E39,
	TwoPointTransformGestureBase_doRotation_m94729D0FF511B4D3F152BBF6AEACABFE9EF78A81,
	TwoPointTransformGestureBase_doScaling_m962A191626D50E09283BF4D45A6C4B3AC37BBED0,
	TwoPointTransformGestureBase_doOnePointTranslation_mC67B5C085D2D78705EA2FCDD506064E16E78F2A5,
	TwoPointTransformGestureBase_doTwoPointTranslation_m37C86C3F36A82A271958DD89E45BCDDB86067C34,
	TwoPointTransformGestureBase_getNumPoints_mDECFD698337D2DAF63D9BC2B2A541C235246608F,
	TwoPointTransformGestureBase_relevantPointers1_m73D2F78852CBF959907A6625A1AC461FC1BA0365,
	TwoPointTransformGestureBase_relevantPointers2_m822362A5B710E1135274D1A7360C586C7C68C62E,
	TwoPointTransformGestureBase_getPointScreenPosition_m29BDCAEF1AAA791FE0DABCA511F9B86A65275820,
	TwoPointTransformGestureBase_getPointPreviousScreenPosition_m98393B0578EFEB59016EAD97E393E719BC44539D,
	TwoPointTransformGestureBase_updateMinScreenPointsDistance_m8FA1D60E3A81F14C2414CCB27860ACEFF1140B5E,
	TwoPointTransformGestureBase__ctor_m71939D7EA22415A9631AEB1ACB8F161E03C8BB51,
	DisplayDevice_get_Name_mF4C36BD5A83E5824E0D1741A20016D200B6D72E5,
	DisplayDevice_set_Name_m717E36608D14BC08B128C167F23F2EE0706C1E8E,
	DisplayDevice_get_DPI_mE1EA8DF49A3970C014CEB81C223EF5BB3F54A7AF,
	DisplayDevice_get_NativeDPI_mA8A9EFC78A5D2ECBFA4C36C76BF67CDDEC5BF3BD,
	DisplayDevice_get_NativeResolution_m2EB79A5B8799302C34CC3B849B80D552C9DE541E,
	DisplayDevice_UpdateDPI_m9597A7DDB4BBF8ACD23E1E4BC1170FD64D194E75,
	DisplayDevice_OnEnable_mF595B56C1DC7388FE0B54D1C07B86273F874C549,
	DisplayDevice__ctor_m34300C4C8E6E9CA2E74617A6A0DB7A09E2E1FDC5,
	GenericDisplayDevice_get_IsLaptop_m0F20D18DE8E7D4FA3A736EA493935DC1006D7CAE,
	GenericDisplayDevice_UpdateDPI_m6CC88F0352E351A0F75A32513324E3413A9D87EC,
	GenericDisplayDevice_OnEnable_m34D4E9D03830441151108AB16FC0169844036DDF,
	GenericDisplayDevice_updateNativeResulotion_m253809FCC6F08CE19AF1D5583CF946BD4A9347C2,
	GenericDisplayDevice_updateNativeDPI_m75A4C52E35F1AD5EB3F28364FE92D4FD2C086CFA,
	GenericDisplayDevice_getHighestResolution_m879363517A6886123D0C084C309FC7055BEB5C1F,
	GenericDisplayDevice__ctor_mDE6666CCEB66F5ADF245C49DF484465C5153056A,
	GenericDisplayDevice__cctor_m579BEC689F617986DBBBC6507506CE211526E0F4,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	DebuggableMonoBehaviour_get_DebugMode_m1038AA3417292402A0CF31B7D83FBA6274991AF5,
	DebuggableMonoBehaviour_set_DebugMode_mF68A4E8748195B9495B7B93559B751B6E382E4EC,
	DebuggableMonoBehaviour__ctor_m2721B121281EA6FCDA46C369E88D5A282D6FD065,
	GestureManagerInstance_get_Instance_mEDD8318DA11453F2645F22C99EDFF65BC27771B0,
	GestureManagerInstance_get_GlobalGestureDelegate_m52C9B3384BBA949F699BE46E2F12B0F3ABB74634,
	GestureManagerInstance_set_GlobalGestureDelegate_m0E83EB33A9D36F0B0D81003C1F41C08DE3126548,
	GestureManagerInstance_Awake_mE0E518D1AF08A9EA9D7D0C7FA0D09837260A7AAF,
	GestureManagerInstance_OnEnable_m67B621910568A052A256B147FCE2856B93096756,
	GestureManagerInstance_OnDisable_mEC1C7786C576C3FA550BF6006EA4A17D8F8F692E,
	GestureManagerInstance_OnApplicationQuit_m638F1509803F2108184E956CF9A60E08B9D36AF3,
	GestureManagerInstance_INTERNAL_GestureChangeState_m7ADDA1CDE3BBA348832BAB067862C32F1046DD3C,
	GestureManagerInstance_updatePressed_m4A3431B20DB4358635D4508B506289F6C524AF3C,
	GestureManagerInstance_updateUpdated_m88C014CE03F4DB43D30DA528D8B3C8A99238367B,
	GestureManagerInstance_updateReleased_mDD5B76E9727664515ADD872378518BFC6E51816F,
	GestureManagerInstance_updateCancelled_mC1CA3E9AC5F162EE8BEDB32CF72AA99672553035,
	GestureManagerInstance_sortPointersForActiveGestures_mB67FCF00E4ADA3BF91021BB726CF234A73256D88,
	GestureManagerInstance_removePointers_mD9252F209D1A06F36C49C934DF66683F0AA92578,
	GestureManagerInstance_resetGestures_mBE260342D905BD4FDA4A7A3741CE38E387AB98E7,
	GestureManagerInstance_clearFrameCaches_mD929F1FEE7D95EA900951A5BA2B384216262C593,
	GestureManagerInstance_getHierarchyEndingWith_m720D31D11E2E8D951D262CC088B53001940935C8,
	GestureManagerInstance_getHierarchyBeginningWith_m3FC9C10E1B6AB16FB4A859986CD6094F47F53601,
	GestureManagerInstance_gestureIsActive_mA8D71708EE5A5EF9B87AF5D35D20C51AC92E41C2,
	GestureManagerInstance_recognizeGestureIfNotPrevented_m7E3B0270D4D9B0E2EFE16CAC40CCE0DDBCFAAF50,
	GestureManagerInstance_failGesture_m4845EC2561CDD90131EF176865F58AEC08E749D6,
	GestureManagerInstance_shouldReceivePointer_m61DB3A5AB20FB50450605782CEEC7C86EA5660C6,
	GestureManagerInstance_shouldBegin_m5E8CFD1280B6CA8E163FB0A21DEB583A15479ED6,
	GestureManagerInstance_canPreventGesture_m58E0B30C017877B734AE147C59C03B3410E6A000,
	GestureManagerInstance_frameFinishedHandler_m2BFD2EDC70F97D68F10EB607ED96594C5C0B5394,
	GestureManagerInstance_frameStartedHandler_m4EE45D6825B3621FEA81AC19ED3BC3B41BD385BA,
	GestureManagerInstance_pointersPressedHandler_m6D101D3CA86EBF1D04E37F178A90FC3DA65F640D,
	GestureManagerInstance_pointersUpdatedHandler_mB5E6B57CA8A2F03A088470116D6447579CF31197,
	GestureManagerInstance_pointersReleasedHandler_m9C04B98378F3D7612207E1F0EF4B5B35899F0227,
	GestureManagerInstance_pointersCancelledHandler_m41C83BD7BC69A297F36875DBF94E3FEE2337F234,
	GestureManagerInstance__ctor_mCF2E1DD8B9C68DFE7E60080E3A5789EA7CCF648D,
	GestureManagerInstance__cctor_m28237BD840950EDC962A2CDB3A9A0E175799B3CD,
	LayerManagerInstance_get_Instance_m35136008FD919F7E1FAC5079A1DA8F04E8C8B2F9,
	LayerManagerInstance_get_Layers_m8B02793D5E8DE67DCE35DFD431F99B5EDD6022DC,
	LayerManagerInstance_get_LayerCount_m48BF00EE2918595E551BF6CD2B8168C7252F509A,
	LayerManagerInstance_get_HasExclusive_m1D89A12F69CF7C13B050D7E54AB55E6A0A33B488,
	LayerManagerInstance_AddLayer_m1DB03A3C1C0826F3B443E556A0A8DA895DAA01F5,
	LayerManagerInstance_RemoveLayer_m5DE2E8EB003BBC95B1E19CDDA207B2B9F67FCC12,
	LayerManagerInstance_ChangeLayerIndex_m127265736EFEEFCACC99022F7106EA4132D0F1AB,
	LayerManagerInstance_ForEach_m18FF3CE3F49821AAD6151A0BD86B13C8E45ECBD8,
	LayerManagerInstance_GetHitTarget_m9B935210E7FF3A5562AE24FA17AB0936252E72D9,
	LayerManagerInstance_SetExclusive_mED6612DE2801C49E07D0BC1B1EE3FE494EA26522,
	LayerManagerInstance_SetExclusive_m037482E05A3F0D7E1E741A06FC4781A6E0FA2780,
	LayerManagerInstance_IsExclusive_mD2707FAF3BF644EE057F02CB42772A36118CABD3,
	LayerManagerInstance_ClearExclusive_mA4357C0DEBFBB335906377ACC26561ADA7A0ED32,
	LayerManagerInstance_Awake_m89CE105D333C64CEEA5DC7C6EDE79E412EEA14C8,
	LayerManagerInstance_OnEnable_m9554249E85F2F6906C3E27947C716D19F2F851C6,
	LayerManagerInstance_OnDisable_m20CF81C9D65F8AE325041BE09A1290AD1A461A35,
	LayerManagerInstance_OnApplicationQuit_mAF1CFC266BD99E9CBA5B3F9D5DEF86304F488786,
	LayerManagerInstance_frameFinishedHandler_m500D8B4B5C494A1D2D7B003D1973A576E441A52E,
	LayerManagerInstance__ctor_m6614061C5C10EEDDC24EDAE5389EE7E4D2456C70,
	LayerManagerInstance__cctor_m45F68C22366CBE3487F34CF434E541D4A67EF226,
	TouchManagerInstance_add_FrameStarted_mC213BC4B71B1EAE89D8AC9A8DB2CA9857E2644D1,
	TouchManagerInstance_remove_FrameStarted_m32EA2BA1805899D2BE28EC7119E811765FF06A68,
	TouchManagerInstance_add_FrameFinished_mE89E2CC43FFE489E8F53ECBBA32C9F8FC3EFC97E,
	TouchManagerInstance_remove_FrameFinished_m88A4D8FBDAADF4D254872AA911EB47FE466DCA1E,
	TouchManagerInstance_add_PointersAdded_m11E8CE419CCD16A750E9F03F982AD2E504B24243,
	TouchManagerInstance_remove_PointersAdded_mEED459E21FB7973794253E4AD40701CAB7BD8B19,
	TouchManagerInstance_add_PointersUpdated_m07CF0C6D90BEBEA4810E116BB22AF1CED2B65986,
	TouchManagerInstance_remove_PointersUpdated_m453B8C556169ED648CA1C4A43F95AB09D26E9EF5,
	TouchManagerInstance_add_PointersPressed_m900D03923946A08FA829F054E1C5C7E5B49AB44B,
	TouchManagerInstance_remove_PointersPressed_m8794B5A72E3FE24F534941E6CC95AE1BF823DD0A,
	TouchManagerInstance_add_PointersReleased_mA140ED9C8302817F291E34FB2B4921BD4D1CD52D,
	TouchManagerInstance_remove_PointersReleased_m92065E3273A7D739A4B198A332BD94BB8E917151,
	TouchManagerInstance_add_PointersRemoved_mEC7E8C8F0F4F767435E55ADA36BACD5553FBF448,
	TouchManagerInstance_remove_PointersRemoved_m083650C359DB15D34631A89A1CA8955D364C72D1,
	TouchManagerInstance_add_PointersCancelled_m4A4268F071D9D3AE4E4C680DF7D5566EE801DD19,
	TouchManagerInstance_remove_PointersCancelled_mECECCC50EC0336C930EE458B4C231B251EB107B6,
	TouchManagerInstance_get_Instance_m78AE6F31FA1A28D96EEAA748AD95F344E1E219EA,
	TouchManagerInstance_get_DisplayDevice_mA12E14299FD2AE2DB893564EFF47B8BB5B56B9A3,
	TouchManagerInstance_set_DisplayDevice_mC949640D595EA41A6D1FAB8827C650FABD4A8C1D,
	TouchManagerInstance_get_DPI_mC1001719F67DA7CDF3FA9F863C5E9219DA4616DA,
	TouchManagerInstance_get_ShouldCreateCameraLayer_mA4052FF942E934603919F6461D46CC956A620B4F,
	TouchManagerInstance_set_ShouldCreateCameraLayer_m201621650A241F0AF2D42FE7630B3658EA9A8B23,
	TouchManagerInstance_get_ShouldCreateStandardInput_m3FABC945C4AB65CEF870523E96A91C2A93282188,
	TouchManagerInstance_set_ShouldCreateStandardInput_m2ADF05D5AE0D86C009638F9D0A3F77CBCD0BDA4E,
	TouchManagerInstance_get_Inputs_mFD9D07E612C8CBBA3FEF056A7B658C4E53026024,
	TouchManagerInstance_get_DotsPerCentimeter_m41295F72BE58FCB18E6EC43A888D396C7610FD14,
	TouchManagerInstance_get_PointersCount_mDD00330D2BBF5946B3D48C8515F0C1C39EA904BC,
	TouchManagerInstance_get_Pointers_mEB333ED681ABA79523A95B323C0F988D9A70EEE5,
	TouchManagerInstance_get_PressedPointersCount_mBD080B3F5D7365B0C5789C0AAAED353D32A3F148,
	TouchManagerInstance_get_PressedPointers_mDD4439DA292D9BD331ABE7976CA812D15260AA2E,
	TouchManagerInstance_get_IsInsidePointerFrame_m95B0763088B71DA4ABBA5214290D240EA4D7804C,
	TouchManagerInstance_set_IsInsidePointerFrame_mD402343201F17BDA1C70FD0C0C51F4ACB6777597,
	TouchManagerInstance_AddInput_m7D38585DACC14D463677F981C91928EE9DF9CB14,
	TouchManagerInstance_RemoveInput_m18A8C0920BC0027D511AB9D6078227E1DD02CEF3,
	TouchManagerInstance_CancelPointer_m1D5CA640901A10DC01FA2385D24C0E5CE269E7DE,
	TouchManagerInstance_CancelPointer_m63CB5F0CF8B190EE4F5BDDF510ED184F91E8130E,
	TouchManagerInstance_UpdateResolution_m9611D1296A160E2F5F2DDCBBEF3ED1E0FE60547D,
	TouchManagerInstance_INTERNAL_AddPointer_mC45CE29B4CBC6BCEF20BD816EBDD5C71A6A994EE,
	TouchManagerInstance_INTERNAL_UpdatePointer_m659F74AE6DEE48C0CC51BFE47CFB38906C14499A,
	TouchManagerInstance_INTERNAL_PressPointer_m87EF8DB01CBC2124AF9C7B456A4B7DAD59DA0B0D,
	TouchManagerInstance_INTERNAL_ReleasePointer_m0E98FA271BEEDA11ADC8E0263EE09A6C17E3B4F9,
	TouchManagerInstance_INTERNAL_RemovePointer_m9B0F26CA6B21D5BC92536AEF7E00621986021F51,
	TouchManagerInstance_INTERNAL_CancelPointer_m0877C5855D096E005F3BFBBDE6708744FF7B98F4,
	TouchManagerInstance_Awake_mB4198CB119E8897B1BFA02850BAD9593121EF0FF,
	TouchManagerInstance_sceneLoadedHandler_m1708D4BAB85BA2C626DC4050DA76C591E706BC08,
	TouchManagerInstance_lateAwake_mE80F82728E3C818FE16DAEA042935D11BF352391,
	TouchManagerInstance_Update_m27EB9A6464AD4089C567A079CBA7506B2512C781,
	TouchManagerInstance_OnApplicationQuit_m8DF3C8AA6DBA480A9DEF4F79891BDEAF1B781A98,
	TouchManagerInstance_createCameraLayer_mBA06278792A4CD63885F5D276C1C68E57470D983,
	TouchManagerInstance_createInput_mDC401897FB67C29D077EF349604D637CB324BC8A,
	TouchManagerInstance_updateInputs_m3DB2CED0E81090754F86E5C4C40C34E4C481D2FE,
	TouchManagerInstance_updateAdded_m8AA0273B193026A8E62DB06E98920444384CFB9C,
	TouchManagerInstance_layerAddPointer_m8074A3A42E67E73FF01502F7A2ED34DA6D8BAA04,
	TouchManagerInstance_updateUpdated_m12FC23113E9F4EC5A47C832C6C49CC94F0F8F9BC,
	TouchManagerInstance_layerUpdatePointer_m0A8CE629D01777E250E768EE74C9C1B5283F0949,
	TouchManagerInstance_updatePressed_m25A4DBEA34D8E88E6549AD57C1D33A94B8FB00FE,
	TouchManagerInstance_updateReleased_m10F618711D1DD2869C1DE78393F088BF638E6FF8,
	TouchManagerInstance_updateRemoved_m0B6CA106190B3470AAE6D0C814D4B1EA59C5483B,
	TouchManagerInstance_layerRemovePointer_m7D0FAE663816EA8EBB12768F82B455264B2C3AF0,
	TouchManagerInstance_updateCancelled_m19D8B796225CEFA23AE31B5197BC49BB3C4FC062,
	TouchManagerInstance_layerCancelPointer_mA336D324314F476DAA9D0F87B279F07AF8FBBFF3,
	TouchManagerInstance_sendFrameStartedToPointers_mE6BCC16FDDB562F77CE2B60E1291003D564A3D6D,
	TouchManagerInstance_updatePointers_m826E60A9DEA74F4AC5D1AC901C234C88BA3595C3,
	TouchManagerInstance_wasPointerAddedThisFrame_mFBD260568A0E31AF9640EA7B23E76B0D21F5AAA5,
	TouchManagerInstance__ctor_mC4BE5F6D1842F6EF526816ABCAD3CCB65E3679EA,
	TouchManagerInstance__cctor_mFF054D19E0E20EB9A1E4D31AA030A2540ED1EB33,
	Clusters2D_get_PointsCount_mBF677C733DECDBC6962739E590389C2F025F55CC,
	Clusters2D_get_MinPointsDistance_mD26732D8478F77214D59FDECBF01EB4442929483,
	Clusters2D_set_MinPointsDistance_m4B86CB2CD3E0ACC4BB9234A4D7B11411876B48A0,
	Clusters2D_get_HasClusters_mBA702A02A3C3BE9A3FE4BBB8AF3D0621B60BEBDC,
	Clusters2D__ctor_mCADB6F800A1D4CA1BA4727E0BA81B16B730FF8BA,
	Clusters2D_GetCenterPosition_m03FBD8D278AE10F729DB7AE7199AFE28DF17594A,
	Clusters2D_GetPreviousCenterPosition_m30ABE6C4BD6D555A02DA4527E9DE51170B4BE39C,
	Clusters2D_AddPoint_mA8A342BCACCE7512C68A2B992A240768540AA67C,
	Clusters2D_AddPoints_mFFC9AE302610192E482F60F924DEEDD94B3A4704,
	Clusters2D_RemovePoint_mE6EEA74F232CBFD4C8E1D9C937DBEA9C0CD873A8,
	Clusters2D_RemovePoints_mC7026EE918F6303C260B4E20A6B5B377E654DCD9,
	Clusters2D_RemoveAllPoints_m3AA8421EF8952715882CB2BA7D22C8C814569D0F,
	Clusters2D_Invalidate_m73C187E405D1283490EF44A78D447899778D591B,
	Clusters2D_distributePoints_mBFC9CBD586FD066A05C468FC3DFA868E6FA82B5B,
	Clusters2D_checkClusters_mAACC7B5ADB58AE0CF05CB5DD1CDE64647E32DB6D,
	Clusters2D_markDirty_m82F2FD009775A944CE669EA7E3E69518FC1EEC5D,
	Clusters2D_markClean_mEFE319AAEEA2AD09D83B05FF337870DA03CA7A7C,
	Transformer_get_EnableSmoothing_mC5702182753B8A5EA25E16A451220BBE5F1B21D2,
	Transformer_set_EnableSmoothing_m28ABABE8AC86CCA60CAA56042E53E7FFEFE3E571,
	Transformer_get_SmoothingFactor_mD85F908B76497C4C80F12FE0989CC8A33050E973,
	Transformer_set_SmoothingFactor_mA6951F93DFA491B0AEF059DD0B11C168FEC5C1D0,
	Transformer_get_PositionThreshold_m767094602CB7040C65B8CD2543E8211714EFF583,
	Transformer_set_PositionThreshold_mC62D02B42E30773C5BA9152ECB49A1016407A169,
	Transformer_get_RotationThreshold_mFACD9FCD08FA2B3BA7AA89F2D20129D5F9E920E5,
	Transformer_set_RotationThreshold_m31F7BDFE97D88FACB97D9404AD45C6F1B868025C,
	Transformer_get_ScaleThreshold_m64AC23AD1A201F8878E9907152C8F40A78319E2D,
	Transformer_set_ScaleThreshold_m1048B714954B61E89FAC09AC3C9F6EE84F8605FB,
	Transformer_get_AllowChangingFromOutside_m6C449501D83568EF5BEA03992CCC01DCADC9C689,
	Transformer_set_AllowChangingFromOutside_mA7AED5AE745D8262D654985F9D1931B8EFF0443B,
	Transformer_Awake_mA1E34B533380507672643E50C7D06924C44CB621,
	Transformer_OnEnable_mA83F58CA491C039C4415240550DEF105452E1B4F,
	Transformer_OnDisable_m970628CE73F498FEB5C1111968DE87F2AA857F27,
	Transformer_stateIdle_m3CB811C714D52EA9F57772D1C96221B79A537344,
	Transformer_stateManual_mF243DBE2651C581CF1786A5CD8D2D3921EC89662,
	Transformer_stateAutomatic_m6275809F62FCD3319C22095EAC02CE2BACBC87E7,
	Transformer_setState_m5505F86414304369D59E4FE21C6D07C5C987C6F3,
	Transformer_update_m10D5106985A3FDEB8C89B5C730DEDA146D447E82,
	Transformer_manualUpdate_mC4C86FE4B5C7917E19AAFA3B3C106B940AEC8FB6,
	Transformer_applyValues_mEBF52D62741932DDA891EB727881647ACC6C9635,
	Transformer_stateChangedHandler_mC8E4855CA01DFE753DE6C07C64EB8DB24E84141C,
	Transformer_frameFinishedHandler_m82CA35A51E7D4DDA0ADACEA29A7692A5984800E3,
	Transformer__ctor_m3C8B68B03D81373779C2E797204147B768B054F6,
	OverHelper_add_Over_m6AD9B0FAB0CF41F4E9C4F5E7CF2FC320492CAD9A,
	OverHelper_remove_Over_m32887625DC3DC45A7352D81FF6D2242F6D511F10,
	OverHelper_add_Out_m316642D4629206671DF5F65E948D7C640751762C,
	OverHelper_remove_Out_mE066A1208350F5E4381D51326B41C15AEE02DE5C,
	OverHelper_OnEnable_m4D08AF12FC607F468D514C70DDB23C91130AD050,
	OverHelper_OnDisable_m0016C981F9C458CABA450B6A4799310E98A53D28,
	OverHelper_dispatchOver_m499273C73319DD699BEAED6A2668FB6836CDA4F8,
	OverHelper_dispatchOut_m820D324CED5C47DC720634066F4098F419ED74B7,
	OverHelper_pointersAddedHandler_mBBC6DF97BC2EE67231192F2A6A89E557BA8658CD,
	OverHelper_pointersUpdatedHandler_mA0CD6BEF32DB111E8F04450AEDCB86706B99397E,
	OverHelper_pointersReleasedHandler_m80B3501AB1FF3597AA6DFFEF17240610AEB95A06,
	OverHelper_pointersRemovedHandler_m8DE8ACA874DE2DC36A0C01465B5912B72D910FCE,
	OverHelper__ctor_m69A116634CB7056AB9F9CAA6444DAB6BB270ED6A,
	CursorManager_get_MouseCursor_m8197D6F0F1A6E72A63262674FD395F6FA2B77BBC,
	CursorManager_set_MouseCursor_m07AD66438486CC5BC2526BE468602169F2D191D5,
	CursorManager_get_TouchCursor_m301C6F2B7E25CA14A2B50F6443B92B049CC75B3E,
	CursorManager_set_TouchCursor_m614D34D7BF6080B0D6EB979AF0B0C60E800E1D9A,
	CursorManager_get_PenCursor_m70F62990BBD76FFD8678A6D2AEDEC75431B30C1C,
	CursorManager_set_PenCursor_m81D030F43ECFC6CAA843A6DB4976BFB5F116AEC9,
	CursorManager_get_ObjectCursor_m792339B60D9D3B5D348E2F5CE04F923E9C9E163B,
	CursorManager_set_ObjectCursor_m13AACAF4147767DFF8EABECE1CD2051DF7A25E61,
	CursorManager_get_UseDPI_m535E943E58BD5C39F91F725587F613255CCB3229,
	CursorManager_set_UseDPI_m35C93320BC0C06028DE5A5EA531E2F2F5623AFC2,
	CursorManager_get_CursorSize_m404BBE7772677EA68081E8416E631DE9AEB2CB82,
	CursorManager_set_CursorSize_mC1FF2258219632E174E7BB4186B8E1DEEE496CA5,
	CursorManager_get_CursorPixelSize_m16A6B328FB3BD845977320F214ACC0DC0397311A,
	CursorManager_set_CursorPixelSize_m0A1E252EB25265F71B60DDA484BC6C113ED5DD60,
	CursorManager_Awake_m4213D7E74CE31E03839FE06E32FEE4768ED999A3,
	CursorManager_OnEnable_m5BCCBB006C92293EB3EDF982C200AE00DE3D921F,
	CursorManager_OnDisable_m321BA269EC2BB16E81431E2290C61A4B8323DAB3,
	CursorManager_instantiateMouseProxy_m21C2093B46BA2E40165D671CFD85ACA813168C92,
	CursorManager_instantiateTouchProxy_mCC18BB8A0E537E03CA30A7ADE0EF469DF76AF243,
	CursorManager_instantiatePenProxy_m9DEDDDFAC3FF687907A9A856837D9F43E9CD0E4D,
	CursorManager_instantiateObjectProxy_mEADCB9A2CEB82EF9EBF098B99CD72DBED91A7057,
	CursorManager_clearProxy_m328F4C863A6EF9EECBB4DDAD1F507AE344CF4903,
	CursorManager_updateCursorSize_mE00F038531F5B93C1D635108C1DA9293AD5169F4,
	CursorManager_pointersAddedHandler_m88A8C7BD8DFE2D5BD01C021442A3CDC99DC7648D,
	CursorManager_pointersRemovedHandler_m0BBB884BD33D081F42A85B803894C9198C8FFCCB,
	CursorManager_pointersPressedHandler_mD1E032BFCA65F615739B87464A2D7CCDA2B677A5,
	CursorManager_PointersUpdatedHandler_m9A657FE7692B2CFE76C2FB879A09A1BCAA859E34,
	CursorManager_pointersReleasedHandler_mA2B213F0BD020526492A306E48CCD867BD8A9163,
	CursorManager_pointersCancelledHandler_m1605DF0439489565F47E836FF820DEADC20141A1,
	CursorManager__ctor_mE7A21304B71D85DF9A1A0F6082C5DF7ABC56F92C,
	MouseCursor_updateOnce_m9FB8A17B1CAC8407A13863D8B94B4E97C583AC57,
	MouseCursor_generateText_mE93383EBFFAC5B95BF8D13DAF774895FA85EF2C6,
	MouseCursor_textIsVisible_m06D3CE5E28774B80CF6114EE67446A42F77AF253,
	MouseCursor_gethash_m235E1B2A65842B5698D769283EDA8CF0E04B7472,
	MouseCursor__ctor_mD4B4DFF7C09F9DF65C1846A8D49348C66166DDC3,
	ObjectCursor_generateText_m40ED348864D90A58BC80A57E581889630F0C7C6D,
	ObjectCursor_textIsVisible_m8AD670831940CF25D368AF0985C00C48FE178E4D,
	ObjectCursor_gethash_m76BBC3809DEDB3C4FF02651F5DA02B8A08720A67,
	ObjectCursor__ctor_m02244E0C1E2BDA048483A5F0DF33EE8F56F3D06A,
	PenCursor_updateOnce_m0EC5F072AFEFFE5A81582569BBF4C9936770E7D1,
	PenCursor_generateText_m6CAE123E463E4F05AA3D4D37E19E77E8FE106910,
	PenCursor_textIsVisible_mF630AC59946E3183688D9D209CD97A0DA2E6B5E6,
	PenCursor_gethash_m2C1A2FA2292456E534D5367C279107DD7043D380,
	PenCursor__ctor_m91E36D14FCA8D1DEBB56831E6E1E9EAB90DD5A47,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	PointerCursor_get_Size_m3B277AE69754EE0E368C041CCA93C9DC948A85E8,
	PointerCursor_set_Size_m4B0954262A8FCE424CFD5F6C30B49340927A8CF3,
	PointerCursor_Init_mBD30377900DD1F0F88FBEBAACFDE3D1E21A513AE,
	PointerCursor_UpdatePointer_m4211256990695D8DA5C61B2AA7C63F56C210A456,
	PointerCursor_SetState_mE7A83F1F53EE8B47766F51B4556D9B0115973FBA,
	PointerCursor_Hide_mAE891F87391F0872D8F0FF2D48BDDA5D12974FC3,
	PointerCursor_Awake_mAF667615DA7BACEB16776728A5DED5E08359BF43,
	PointerCursor_hide_mF0934BF39377528582AE9A55A680A25D04002F45,
	PointerCursor_show_m27D2618E805936B211771E79094A5647104EBDED,
	PointerCursor_updateOnce_mA34AF2C967654F70029DFCD3BE525AC391DE0965,
	PointerCursor_update_m581222C6269ECB86C175D219A77B8AAD0CF700F5,
	PointerCursor_getPointerHash_mE13CC1C1CDEBFF64CD28536B1A47773594EF3E2B,
	PointerCursor__ctor_m43144A36F3CD04E7ECD4DA9480137FE927A09374,
	TouchCursor_generateText_m8A065D5B018DA1F47A60A77B6C8E8C1B7D8C7D0D,
	TouchCursor_textIsVisible_m62766D562F01A08F458ECDA5546D34D4F4B0484F,
	TouchCursor_gethash_m2D2067E73C4E1D5D99FCA2B724935F4152D27528,
	TouchCursor__ctor_m17EC5DA47060D2D2766F27C896FA453D0168F595,
	GradientTexture_Generate_m3E692617E2E3A28883E1E6D81DD438009F7412CB,
	GradientTexture_Start_m536C9CC190293CD96F17F32D96381A9B32FB70F9,
	GradientTexture_OnValidate_m1B637E19835BE3EA384961E8F8F06261EB3DFFC2,
	GradientTexture_refresh_m743EFBCECF5A162E64857F1FE15B17622A5CE1DB,
	GradientTexture_apply_mE5D364BFE2A3384D15185561E7483D84D0DDD789,
	GradientTexture__ctor_m7CCD2EEC1A548EF009DA2605ACB531F82D2362F9,
	GradientTexture__cctor_m26F2D55F9E40D604BF9C3651F0F29D2BCE4F274B,
	TextureSwitch_Show_m8E6A3566324E923E17118DA0CA8CE558B6B325DF,
	TextureSwitch_Hide_m65862F55643E025A89098AE6895D674CDBB6A15C,
	TextureSwitch_Awake_m83F9DD0928DC030F3B4A04B2E0EBC222F646A603,
	TextureSwitch__ctor_m8A4554509B623D3CCE933FD4D2BF7D9A4D8345B7,
	Checker_OnEnable_m22244786833AE2A7ABBA4B74FB0E9DDD899816AC,
	Checker_OnDisable_mEBC4D1CF0936580B82587E0C46D2886B53C4ED70,
	Checker_transformStartedHandler_mE62DA697B8E72515698C0CE8759C6F63FF181781,
	Checker_transformCompletedHandler_mD19A952A443B68D76A26FF04F6AD78E215B9A6DB,
	Checker__ctor_m73ABEC26E306920C31F1675E7C22DAEECF6026EC,
	KillMe_Start_mE5BDF0FE7A7CBDD4EC3F919BB363CC26D002F6E8,
	KillMe__ctor_m0CE80246905A917389D52D550F3592A61152388A,
	Runner_LoadLevel_mFC4DDAD9020BF050811A7C7433874C8523F73298,
	Runner_LoadNextLevel_m47F81F83B6F0C42EC410EC06051512B9FA554D13,
	Runner_LoadPreviousLevel_mEB6B45834AFA837C925564A1F63BA621C52ED2F8,
	Runner_Start_mC5A19E3D1C9F440C596D0A7D25A12995AD97FE5A,
	Runner_OnDestroy_m9B8B33DF5B75E285FC0212C76AAA94F14BA1346A,
	Runner_Update_m6CB9E27B8BEA633F680A8882CA75EE37C5F1ADC4,
	Runner_sceneLoadedHandler_m7BF24C8CBE8754581A2E7E0FAB3B07CD64E2866B,
	Runner_resetUILayer_m859A7F3A0AFFC89A654424C1132CC804F815A92D,
	Runner__ctor_m2246327898122ACAFD9AF8F6B199436213B1D290,
	Break_OnEnable_m006D3897652AED2D8986354CEC878A455E23C427,
	Break_OnDisable_mFCF3DEAC57CF47B70CEDB2A8EE5F5090F8DC6065,
	Break_Update_mECFA71DC2C4E68D6F4B94BC8F4607BA945279311,
	Break_startGrowing_m95A6EE85B94F1EEFF104527692755C3ED3FF396B,
	Break_stopGrowing_m30D6ECA6195CC2B1536DE62111FE06617BEC7AD8,
	Break_pressedHandler_m83AA6C64261B26DC3625C5746DB9B7125D43A849,
	Break_longPressedHandler_mDE8E6E49E07AA36405267990A58666B39C3419EE,
	Break__ctor_mC9F442C861DAC9C10FE50298F6DF8590CB295490,
	Kick_OnEnable_mAE00729C20D076A9CA61A9A95E105379BDB00150,
	Kick_OnDisable_m1DAF77464C7E028F3085C610BA47826151AACF0B,
	Kick_tappedHandler_m2B3670F4602E00055F97B00A053E57B3173FD984,
	Kick__ctor_m0164DCC0528026FAB78D58DC58C4DA231D5DC485,
	Spawn_OnEnable_m26ED9352BC75E5C5A4CC294680E3D3199E3BB29F,
	Spawn_OnDisable_mFAB9F4D9475CE486A2ECF336EC08B3DB7DD970AF,
	Spawn_tappedHandler_m884F5683C7CD58149CCD19DDBD0508ED5A7A5C85,
	Spawn__ctor_mFCEC573B10C9E8FE72C55D2F3798536A85E7D568,
	Ball_Update_m2C132F2380CD745AA05CBAAF0FF2CDA469DB1D09,
	Ball__ctor_mCD0981C29298D58AF723800AF665333B883C412C,
	Spawner_OnEnable_mB5217B5CB8CC86C7A417B37CC04D7A4C4A8E2014,
	Spawner_OnDisable_mCB4976CB03B28BA37A14C0A024E384BB2C07BD19,
	Spawner_spawnPrefabAt_mCAB9CA8B03F9E0F51273869275BF1DC50386D311,
	Spawner_pointersPressedHandler_mFFE66D14DA544A9EB5C7C70F4CE66536489F611D,
	Spawner__ctor_m8221222A41C7DDB7B392C6F651B99CA840299745,
	Planet_Fall_m5034351FC0529F74E8171FE47B57AE6C7368BF96,
	Planet_OnEnable_mB370964517A63ED0B88115DF1FA8FBDEC723E5ED,
	Planet_OnDisable_mCADBF0F8BD2D21392241E504DC1AC19A16D3869F,
	Planet_Update_mF92E212D57BBCEA1EFCA7E3B777B0CBE36724140,
	Planet_pressedhandler_m3E405FCAD05BA3A92F07CE6FE349089C248386F1,
	Planet_releasedHandler_m90AF6C209A555501291DC95BAA0363175CF07C97,
	Planet__ctor_m6B8E568A83270B47C675EA8222206CE45F4CCF63,
	Rotator_Update_m62C8E67861D2D19ABD3525DA159A8A46BF440D22,
	Rotator__ctor_m293D13755A8E6CD52E5224C5E9ED1532A9322706,
	Spawner_OnEnable_m114A4C299CCF79650C49B98F8754DB02F9D502BF,
	Spawner_OnDisable_m7F07B9BF09D3F2F5A85F4721576373C678D7779C,
	Spawner_pressHandler_mBBE08D2BE67D094A478BC2FA7B4810BD401ABB42,
	Spawner__ctor_mD64033E133CAD768B0645FA11498F9D3BA9FF59E,
	Vortex_OnTriggerEnter_m87E6A0FDA5AE85A3D6A26531A5D28526034DE71B,
	Vortex__ctor_m072F81E0E39792BD61921C630BA65BDD0C0D58B2,
	SetColor_Set_m9A5D29AB7B5B3F91E1007480C84CBA2673A18952,
	SetColor__ctor_m74A69FEC184FE253F14F7D9947F8BF01F3DEEF7E,
	Container_Add_mA258161D0E40B99B1B62060B38B60CC17395C80E,
	Container_Start_mD82ECB6FE52E82882C1C82888AE880706E812772,
	Container_initChild_m3945E71855569BDCAA8A93F93449E13B6E679CB7,
	Container_pressedHandler_m3FB594D9449A10DD2186E4C5B6F68DCF5D670554,
	Container__ctor_m7CE40023605E7BD47BB0E4B97C0F31C5F5D179AA,
	Logo_OnEnable_m4856654646850CD5F2DB28078B1950F01B2CCD6F,
	Logo_OnDisable_m87005B514999E08C6BE533786E171A8CA70693BF,
	Logo_changeColor_mA2B900924A48C17E55A5A7D37CE62BED2A248582,
	Logo_tappedHandler_mD4CD4B9518C84A90D4A98003FF1FA0A4B1905239,
	Logo__ctor_m2EDE7CF8D140795394B85CA0CE1116D40CA39F5A,
	Logo__cctor_mBE5C8AC1FAE5C2DEACE6518F70CAAA597579E1CE,
	CustomPointerProxy_updateOnce_m926AA073292C5F78E51B6D202E1B3CBF153E4346,
	CustomPointerProxy__ctor_m9F88E1950A33A574C22F4D1DCB72F9815DA9081A,
	Init_Start_m4C5A39CF798E77ECEE29D701D8A297F11690A3E8,
	Init__ctor_m3D89B79C83258B26A693F00A1CF96E6FEE0AA634,
	LayerDelegate_ShouldReceivePointer_mA445F4F58298632176766AC37C3C56F7CC534343,
	LayerDelegate__ctor_mA37893208937F1311649FC7FF23541B3D9856279,
	RedirectInput_CancelPointer_m6F551316AEC8F9A697EB0A5A135543F1D71A28BE,
	RedirectInput_OnEnable_m07AF117B3678F212A9F235B3DD41F93C85F80C7F,
	RedirectInput_OnDisable_m59CA7BD9E349507B5BD41DEC7F821900F71B28A5,
	RedirectInput_processCoords_m894056053BEB2CD0A21E7DEE56B6A0701F65A991,
	RedirectInput_pointerPressedHandler_m049EE10FEDB2AAB088B74D13A3DD726A38C3915F,
	RedirectInput_pointerUpdatedHandler_mD3110F1358718676C06DE530B920BDDF555C791C,
	RedirectInput_pointerReleasedHandler_mA24C87DAFF62E2F94ED6C9ECED7D426A5A204EA0,
	RedirectInput_pointerCancelledhandler_m7077BC6BBEE5BAA3D5D3877E35D90D50FE0DAA65,
	RedirectInput__ctor_mDCA7A3D46531FF97588A75EAA9C65677917B7777,
	Circle_Kill_m1616E2262DE24784E359F0DA0923E34323B72530,
	Circle_OnTriggerEnter2D_m01A2B25AEFC7C5A7E0BC559C667856AFF4A882BD,
	Circle__ctor_m8649F15CDC90782DE8322CF712E21DCEC85C6904,
	Colors_Start_mA5A9477713B64D37F1BED03A0278B4A903D4C5B3,
	Colors__ctor_m1B959D35C1D96B3BC8E8C8483A5086593CF0F13D,
	Board_OnEnable_m0DF08F0E6B12C2D889C54F9DC39922D37711DD6E,
	Board_OnDisable_m17A2EAE25E83D8B1A36FF26352B8C4783BE758F9,
	Board_transformedHandler_m842BA6FF27628E9B954AAA603250446D44FEF658,
	Board__ctor_m1B1EA536B46C92AC6158470A0422946E1D15E9C6,
	Exclusive_Awake_mAB9E78C0FD300DB544FF1BA587FA896216494F9E,
	Exclusive_Update_mAE431335FEA9CB4797CABBF7C00BFC742AF45B76,
	Exclusive_ShouldBegin_m9C153226AD0B01A8752A012D99A450283D26CFC6,
	Exclusive_ShouldReceivePointer_mD422EDB6D4B872BB89AB8B1208966526D3A0F619,
	Exclusive_ShouldRecognizeSimultaneously_m53CCEDF7F16425239DA4C880647BE601EF12C62F,
	Exclusive__ctor_m108DB8E55D4845DDD2B338748B2E488789084E20,
	CameraController_Awake_m85AF24A3CCC6915C66FABEA503A2976E6BAD9449,
	CameraController_OnEnable_m1E89CBA6F598EB1DDCC6A905D269858151477CE2,
	CameraController_OnDisable_m16B61DEA3CB5A35FB2ECAFF2948CE748889F9666,
	CameraController_manipulationTransformedHandler_mCD54321C80F6CB65A26B9D5A4FAC9FCAD46FADEA,
	CameraController_twoFingerTransformHandler_m153B637246A7E21731B9A64CA237E3E17D4A3162,
	CameraController__ctor_m6D25D3B16C45FD6D2CE3D5A1E689AB0B6B10ECE9,
	U3CStartU3Ed__0__ctor_m68F3546D7663144C02866F962D17C9440A1920C4,
	U3CStartU3Ed__0_System_IDisposable_Dispose_m6DBBCE6635B27796EE108B8237956FB1AB96F262,
	U3CStartU3Ed__0_MoveNext_mDE76B03C8B83AFD1798DDC310E89DAD2E668A2E3,
	U3CStartU3Ed__0_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m593685FD537795C7D73B1B54681041E4210F3F34,
	U3CStartU3Ed__0_System_Collections_IEnumerator_Reset_m67477E6B6C474BE023CE7D2B4F2B48BD44A42A09,
	U3CStartU3Ed__0_System_Collections_IEnumerator_get_Current_m8EB8E75B6E07249830AF0404BD1CE6D546BB49A3,
	U3CU3Ec__DisplayClass2_0__ctor_mF95464CCBB9EDFA86D0785A8482181854DEFF131,
	U3CU3Ec__DisplayClass2_0_U3CPickImageU3Eb__0_m9A5755815823DE9D168D4EB0EA78CE86C35B1ED3,
	PointerEvent__ctor_m63D4E009DE7DACDA202FD81E36A40E7D04F387C2,
	FrameEvent__ctor_mC2B90B221D4FECA515E3401E5611A8E7510BF817,
	NULL,
	NULL,
	NULL,
	NULL,
	U3ClateEnableU3Ed__46__ctor_m55FEB14D9838CA2B519B11268309CA1F33383899,
	U3ClateEnableU3Ed__46_System_IDisposable_Dispose_m6998592F6C19AD3776BA7FB3615E655FCE084510,
	U3ClateEnableU3Ed__46_MoveNext_mF0C3227B9D5A81FB319B2319C0D63AFD1C57A5F8,
	U3ClateEnableU3Ed__46_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m0BE5322A66A3ECDD3715E38F1141D510C37F62E5,
	U3ClateEnableU3Ed__46_System_Collections_IEnumerator_Reset_m50B1C834F8A2EC0DD9DF7DCFAC95CBEB8A0E6173,
	U3ClateEnableU3Ed__46_System_Collections_IEnumerator_get_Current_mC288F28B5D57A5627D273392A6972E1B01D1E3B4,
	U3ClateAwakeU3Ed__17__ctor_m3CA22DCA2BA8FD972871A55AB88487703312C617,
	U3ClateAwakeU3Ed__17_System_IDisposable_Dispose_m21F6AAFE695AF29F3D6AECD56746633607DA37F0,
	U3ClateAwakeU3Ed__17_MoveNext_mB1FEF8CC06BA2E431DB918BB6424AE2D3E247714,
	U3ClateAwakeU3Ed__17_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m83B1CE8547DD9E08C78D59FC3A477FFE5BF593AB,
	U3ClateAwakeU3Ed__17_System_Collections_IEnumerator_Reset_mDBC3B955C30B87DD05C779810F199A006E31DBF4,
	U3ClateAwakeU3Ed__17_System_Collections_IEnumerator_get_Current_mCEBC4FE9C1B9B12863F399BD4C05DBA1318C817C,
	UIStandardInputModule__ctor_mE74BA31E858A8B496B5768ABB4C61FAAB2374400,
	UIStandardInputModule_IsPointerOverGameObject_m8B72650E3686B6327186B2EFFC8F6DF2FEBA13DA,
	UIStandardInputModule_GetPointerData_m1C41F37A3CD0F6D326A88B9065A2707FF4B2C036,
	UIStandardInputModule_DeselectIfSelectionChanged_mCC2D0B24E90A4C0BA6308A31589BCA6EBB500FD1,
	UIStandardInputModule_GetLastPointerEventData_m609058CF2767C7FD89CADC1D8CDC0E8D7276531B,
	UIStandardInputModule_ShouldStartDrag_mA090464BF9C6574DBFFE079BBF935A58568CE8AE,
	UIStandardInputModule_SendUpdateEventToSelectedObject_m025CA5ADF75416F34BE357E25717456C6EB422DF,
	UIStandardInputModule_SendMoveEventToSelectedObject_m647E343743C16926B0878DE1A19726397077BF6A,
	UIStandardInputModule_SendSubmitEventToSelectedObject_mF58B53B69185554E6FBC1BC2C914133CE69B3306,
	UIStandardInputModule_GetRawMoveVector_mB7233D69070FA8D68C131525FD38C225F3828457,
	UIStandardInputModule_Process_m128F9F0293A64AFAF06134B0BEF393C2F39E6408,
	UIStandardInputModule_RemovePointerData_m52D87CFB8DF7B3D4A85838173A1A8A91B177D2CD,
	UIStandardInputModule_convertRaycast_m7F74BBC32629D9E6F40535B5E5D3557E3AFA236F,
	UIStandardInputModule_ProcessUpdated_m00F503B5AD3AAA332DEF7DFF1472C3CAB570D80E,
	UIStandardInputModule_ProcessPressed_m1BF46C8AE5D9A4092C8191121663C584F877FC8F,
	UIStandardInputModule_ProcessReleased_mC76C699EF03C973CF2EC48D52C2A04DDFEE7384E,
	UIStandardInputModule_ProcessCancelled_mD397CFF1043DFC2B9694E3713E02BA1092BCE5AC,
	UIStandardInputModule_ProcessRemoved_m8CF9D7E0C005C4F8C8EC31A7232742953763C3D4,
	TouchState__ctor_m7B2C8ECC4F38BE22A4B43A8ECC48A149437BAFD3_AdjustorThunk,
	GestureEvent__ctor_m8D7187B46A674A2DA5DF24031F12A6A5A2F929C9,
	U3CwaitU3Ed__25__ctor_m7A5B5C5EC945F5915EC705E8E6531221781A19F0,
	U3CwaitU3Ed__25_System_IDisposable_Dispose_m8FA6757ED65AFC6DACBB523473AB71F2B904296C,
	U3CwaitU3Ed__25_MoveNext_mD5DD73FFC3B85E65510EA13DB9AC39AD5850E5F6,
	U3CwaitU3Ed__25_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mF59DE5F31505D7C5AB985E99E729BDA37C4F3092,
	U3CwaitU3Ed__25_System_Collections_IEnumerator_Reset_m494375675C67808ADEF89DD497CED830A4E2BF06,
	U3CwaitU3Ed__25_System_Collections_IEnumerator_get_Current_mE807BFD154B7C06744A3F99DA9FF66C2A9602FF0,
	U3CwaitU3Ed__43__ctor_m3AC19D8DCF890FFD7EC7B4C415DE37646FCB2A32,
	U3CwaitU3Ed__43_System_IDisposable_Dispose_m54744209E6CBB21D0E284C4AA4ED708D4BACEA1A,
	U3CwaitU3Ed__43_MoveNext_mC25D1C6FBE9122503A4E1ACC17727FD9B1E63119,
	U3CwaitU3Ed__43_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m4DF14E48448B4B8FE937D35C00D7C6327F7F80F2,
	U3CwaitU3Ed__43_System_Collections_IEnumerator_Reset_m7B58B8269C23985A4995C945488DC612620380AB,
	U3CwaitU3Ed__43_System_Collections_IEnumerator_get_Current_m13969DE0842296A196575525642B5D51220867DF,
	U3CU3Ec__cctor_m4B53EBF4576A2CC52989254F02CFE09C561341C5,
	U3CU3Ec__ctor_m02B3736DDBB2BEC654735154ADDCD39C8046FAFB,
	U3CU3Ec_U3C_cctorU3Eb__47_0_m538E88BFC4DBCD52F255DA8DA5B9A78DC79CE72A,
	U3CU3Ec_U3C_cctorU3Eb__47_1_m10316F18556530CC8F06D6E0B8BAA5AD195E97DF,
	U3CU3Ec_U3C_cctorU3Eb__47_2_mAB494D2605418B3543FBC3C3079A8A0492B348B2,
	U3CU3Ec_U3C_cctorU3Eb__47_3_m4469E2A72AA6AEAF75402A165E2F640EF063F850,
	U3CU3Ec_U3C_cctorU3Eb__47_4_mCE8F43A094B63ECB9F8CF078EECC814F7711104A,
	U3CU3Ec_U3C_cctorU3Eb__47_5_m7730ACB25741653A1051D0F89C91AFD93033D760,
	U3ClateAwakeU3Ed__109__ctor_m56C8A851AB8559D6F2F3C181DA45187F9A778877,
	U3ClateAwakeU3Ed__109_System_IDisposable_Dispose_mCF02BD52B18943D69FCADFB8B48D5C71540B7B3B,
	U3ClateAwakeU3Ed__109_MoveNext_m92337C14936E2C8DF621A1750F545378D7110925,
	U3ClateAwakeU3Ed__109_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m00DA18E805C1CE6B014485D5343B6F33A9D5A89A,
	U3ClateAwakeU3Ed__109_System_Collections_IEnumerator_Reset_m55FEF147A2CE155F38178DE6FEE1FDC551160F39,
	U3ClateAwakeU3Ed__109_System_Collections_IEnumerator_get_Current_m3FD9527DD11F0D084E4B40B723E450C92E6BC0BE,
	U3CU3Ec__cctor_m917EEE6C81BF77465A520CB4E0ACBF48776DBBD2,
	U3CU3Ec__ctor_mAFC6F44E151E15E5B6D763D6965F656B5E8A772A,
	U3CU3Ec_U3C_cctorU3Eb__129_0_m456F4C5BA26213741D14B1806E081F5AB187D60D,
	U3CU3Ec_U3C_cctorU3Eb__129_1_m6530EC0AAE2AC84973A5523F945D8C01A7E15596,
	U3CU3Ec_U3C_cctorU3Eb__129_2_mBBEA5FC6F007D1315A76A339CF70A349D4BDC3DD,
	U3CU3Ec_U3C_cctorU3Eb__129_3_m1EB1EF2D23CD9A9D08BB9F352E5136A64D0776F8,
	U3CStartU3Ed__1__ctor_mCE687CDBE3CFDB38805FA2CA6DACBAD8181DD3DC,
	U3CStartU3Ed__1_System_IDisposable_Dispose_m7C1AB1E6884234D634C34D33EE15A7B29489D3F9,
	U3CStartU3Ed__1_MoveNext_m0DB6AFEA72301047F3DDEC67B4C8635A3E46427A,
	U3CStartU3Ed__1_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mD6CC52D4DC2B29320C17CFF90A51743038E00F07,
	U3CStartU3Ed__1_System_Collections_IEnumerator_Reset_m03E1DED2743ACC02957FCDFC197611B4190ACA92,
	U3CStartU3Ed__1_System_Collections_IEnumerator_get_Current_mC483127391A2BA87191716AA0EEEC9611579054A,
	U3CresetUILayerU3Ed__9__ctor_mEED30C36793D09E063FA5B04BA52D38B044023EF,
	U3CresetUILayerU3Ed__9_System_IDisposable_Dispose_m644D1403AB20710FE544F4C55785DEDC2E3FE83D,
	U3CresetUILayerU3Ed__9_MoveNext_mD6C4AE86FD850BE887F4A855A3F10026FB6F6181,
	U3CresetUILayerU3Ed__9_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m0AC3371ACE4DBAD79D3E5FA8A35DC1D92BBDDAA4,
	U3CresetUILayerU3Ed__9_System_Collections_IEnumerator_Reset_m45AFFC063D71369E6EED1835F4FDA1E98C11E532,
	U3CresetUILayerU3Ed__9_System_Collections_IEnumerator_get_Current_m4A1C4965A0D6E9524583CFEE2596A23819C0F06C,
};
static const int32_t s_InvokerIndices[1288] = 
{
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	27,
	27,
	23,
	14,
	23,
	23,
	23,
	23,
	23,
	32,
	23,
	4,
	23,
	103,
	9,
	103,
	102,
	31,
	14,
	26,
	14,
	10,
	102,
	1460,
	9,
	157,
	26,
	1461,
	382,
	26,
	9,
	23,
	26,
	26,
	26,
	26,
	26,
	26,
	26,
	26,
	26,
	26,
	26,
	26,
	26,
	26,
	26,
	26,
	14,
	26,
	654,
	102,
	31,
	102,
	31,
	14,
	654,
	10,
	10,
	14,
	14,
	102,
	9,
	9,
	568,
	32,
	23,
	14,
	26,
	23,
	0,
	4,
	23,
	4,
	14,
	26,
	102,
	31,
	102,
	31,
	102,
	31,
	10,
	32,
	14,
	26,
	102,
	31,
	1462,
	23,
	23,
	23,
	23,
	23,
	23,
	27,
	27,
	27,
	27,
	27,
	27,
	27,
	27,
	23,
	23,
	27,
	27,
	27,
	27,
	27,
	27,
	27,
	27,
	23,
	3,
	1463,
	514,
	186,
	1154,
	1154,
	0,
	3,
	-1,
	2,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	109,
	99,
	320,
	43,
	510,
	43,
	510,
	21,
	21,
	21,
	3,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	1464,
	1464,
	1464,
	0,
	3,
	1465,
	1466,
	1467,
	1468,
	1105,
	23,
	23,
	10,
	32,
	10,
	32,
	14,
	26,
	1010,
	1135,
	10,
	32,
	10,
	32,
	1010,
	1135,
	1135,
	23,
	1469,
	10,
	10,
	10,
	14,
	1010,
	1135,
	1010,
	10,
	1469,
	1010,
	1135,
	26,
	26,
	10,
	32,
	654,
	275,
	654,
	275,
	654,
	275,
	26,
	26,
	23,
	654,
	275,
	654,
	275,
	26,
	23,
	10,
	32,
	10,
	32,
	10,
	32,
	14,
	26,
	1010,
	1135,
	1010,
	1135,
	10,
	32,
	14,
	1469,
	1470,
	26,
	9,
	9,
	10,
	14,
	26,
	32,
	23,
	23,
	23,
	23,
	10,
	1471,
	23,
	250,
	654,
	275,
	654,
	275,
	26,
	23,
	10,
	32,
	14,
	26,
	1001,
	420,
	23,
	23,
	14,
	23,
	23,
	23,
	103,
	1472,
	1473,
	23,
	26,
	1472,
	1473,
	26,
	1472,
	1473,
	102,
	31,
	102,
	31,
	102,
	31,
	102,
	31,
	102,
	31,
	1450,
	1277,
	1001,
	420,
	28,
	23,
	23,
	14,
	23,
	23,
	23,
	14,
	23,
	23,
	420,
	420,
	1474,
	1475,
	1476,
	23,
	1477,
	1478,
	1479,
	27,
	23,
	3,
	26,
	26,
	1001,
	14,
	26,
	28,
	420,
	23,
	14,
	23,
	23,
	26,
	26,
	9,
	26,
	26,
	26,
	1480,
	23,
	26,
	26,
	26,
	26,
	26,
	26,
	14,
	23,
	14,
	26,
	26,
	4,
	23,
	23,
	23,
	23,
	14,
	28,
	23,
	30,
	102,
	102,
	23,
	23,
	23,
	23,
	10,
	23,
	23,
	3,
	1368,
	14,
	26,
	102,
	23,
	404,
	26,
	163,
	26,
	166,
	26,
	14,
	26,
	102,
	23,
	404,
	26,
	23,
	23,
	26,
	26,
	26,
	26,
	26,
	26,
	26,
	1368,
	23,
	10,
	10,
	102,
	102,
	102,
	102,
	102,
	31,
	102,
	23,
	404,
	23,
	23,
	23,
	26,
	23,
	23,
	23,
	23,
	23,
	14,
	26,
	102,
	31,
	869,
	23,
	102,
	23,
	404,
	23,
	26,
	10,
	157,
	102,
	1482,
	32,
	28,
	1368,
	26,
	23,
	23,
	23,
	23,
	32,
	14,
	14,
	26,
	102,
	869,
	102,
	23,
	404,
	23,
	26,
	1483,
	28,
	26,
	26,
	1368,
	26,
	14,
	10,
	14,
	14,
	1484,
	1485,
	1486,
	102,
	1001,
	1001,
	654,
	10,
	10,
	98,
	1487,
	1488,
	1489,
	23,
	1480,
	23,
	23,
	1480,
	23,
	26,
	26,
	654,
	275,
	654,
	275,
	654,
	275,
	10,
	32,
	1010,
	1135,
	654,
	275,
	23,
	23,
	23,
	26,
	26,
	26,
	23,
	23,
	23,
	26,
	26,
	26,
	26,
	10,
	32,
	10,
	32,
	14,
	26,
	102,
	31,
	102,
	31,
	14,
	26,
	102,
	31,
	102,
	31,
	10,
	32,
	10,
	32,
	1010,
	1010,
	1010,
	1010,
	14,
	10,
	14,
	26,
	14,
	14,
	26,
	10,
	32,
	26,
	9,
	9,
	9,
	9,
	9,
	102,
	42,
	23,
	1470,
	23,
	23,
	23,
	23,
	32,
	23,
	26,
	26,
	26,
	26,
	26,
	9,
	30,
	26,
	26,
	26,
	26,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	31,
	26,
	26,
	27,
	23,
	10,
	32,
	10,
	32,
	23,
	514,
	26,
	26,
	654,
	275,
	654,
	275,
	23,
	23,
	23,
	26,
	26,
	26,
	23,
	23,
	14,
	23,
	26,
	26,
	26,
	26,
	26,
	26,
	26,
	26,
	23,
	23,
	26,
	26,
	26,
	26,
	23,
	14,
	26,
	26,
	26,
	26,
	102,
	31,
	23,
	23,
	9,
	9,
	9,
	26,
	23,
	23,
	26,
	26,
	102,
	31,
	23,
	23,
	9,
	9,
	9,
	26,
	26,
	23,
	23,
	26,
	26,
	10,
	32,
	654,
	275,
	654,
	275,
	102,
	31,
	654,
	275,
	9,
	23,
	23,
	23,
	26,
	26,
	26,
	23,
	23,
	9,
	14,
	23,
	26,
	26,
	26,
	26,
	26,
	26,
	10,
	1001,
	654,
	654,
	1001,
	10,
	32,
	1001,
	1002,
	1490,
	23,
	23,
	23,
	26,
	26,
	26,
	1491,
	1491,
	23,
	23,
	23,
	23,
	26,
	26,
	26,
	1492,
	1492,
	1493,
	1494,
	1495,
	23,
	10,
	32,
	1001,
	1002,
	1490,
	1001,
	23,
	23,
	23,
	26,
	26,
	26,
	1496,
	1492,
	1492,
	1493,
	1494,
	23,
	23,
	9,
	1010,
	1010,
	23,
	26,
	26,
	26,
	23,
	10,
	9,
	9,
	1497,
	1497,
	23,
	26,
	26,
	26,
	23,
	10,
	9,
	9,
	1497,
	1497,
	23,
	1010,
	1010,
	26,
	23,
	1491,
	1491,
	9,
	1010,
	1010,
	23,
	23,
	26,
	26,
	26,
	26,
	26,
	26,
	10,
	32,
	654,
	275,
	10,
	1001,
	654,
	654,
	1001,
	1002,
	23,
	26,
	26,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	654,
	275,
	23,
	26,
	23,
	1492,
	1492,
	1493,
	1494,
	10,
	9,
	9,
	1497,
	1497,
	23,
	23,
	14,
	26,
	654,
	654,
	1010,
	23,
	23,
	23,
	49,
	23,
	23,
	23,
	23,
	760,
	23,
	3,
	14,
	654,
	654,
	1010,
	23,
	102,
	31,
	23,
	4,
	14,
	26,
	23,
	23,
	23,
	23,
	434,
	26,
	26,
	26,
	26,
	26,
	26,
	23,
	23,
	28,
	28,
	9,
	9,
	26,
	103,
	9,
	103,
	27,
	27,
	27,
	27,
	27,
	27,
	23,
	3,
	4,
	14,
	10,
	102,
	1460,
	9,
	157,
	26,
	1461,
	382,
	26,
	9,
	23,
	23,
	23,
	23,
	23,
	27,
	23,
	3,
	26,
	26,
	26,
	26,
	26,
	26,
	26,
	26,
	26,
	26,
	26,
	26,
	26,
	26,
	26,
	26,
	4,
	14,
	26,
	654,
	102,
	31,
	102,
	31,
	14,
	654,
	10,
	14,
	10,
	14,
	102,
	31,
	9,
	9,
	568,
	32,
	23,
	26,
	32,
	32,
	32,
	32,
	32,
	23,
	1498,
	14,
	23,
	23,
	23,
	23,
	23,
	26,
	9,
	26,
	9,
	26,
	26,
	26,
	9,
	26,
	9,
	23,
	23,
	1499,
	23,
	3,
	10,
	654,
	275,
	102,
	23,
	1497,
	1497,
	26,
	26,
	26,
	26,
	23,
	23,
	23,
	102,
	23,
	23,
	102,
	31,
	654,
	275,
	654,
	275,
	654,
	275,
	654,
	275,
	102,
	31,
	23,
	23,
	23,
	23,
	23,
	23,
	32,
	23,
	23,
	23,
	27,
	27,
	23,
	26,
	26,
	26,
	26,
	23,
	23,
	23,
	23,
	27,
	27,
	27,
	27,
	23,
	14,
	26,
	14,
	26,
	14,
	26,
	14,
	26,
	102,
	31,
	654,
	275,
	10,
	32,
	23,
	23,
	23,
	14,
	14,
	14,
	14,
	26,
	23,
	27,
	27,
	27,
	27,
	27,
	27,
	23,
	26,
	27,
	102,
	104,
	23,
	27,
	102,
	104,
	23,
	26,
	27,
	102,
	104,
	23,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	654,
	275,
	27,
	26,
	657,
	23,
	23,
	23,
	23,
	26,
	26,
	104,
	23,
	27,
	102,
	104,
	23,
	14,
	23,
	23,
	23,
	23,
	23,
	3,
	23,
	23,
	23,
	23,
	23,
	23,
	27,
	27,
	23,
	14,
	23,
	26,
	23,
	23,
	23,
	23,
	23,
	1498,
	14,
	23,
	23,
	23,
	23,
	23,
	23,
	27,
	27,
	23,
	23,
	23,
	27,
	23,
	23,
	23,
	27,
	23,
	23,
	23,
	23,
	23,
	1135,
	27,
	23,
	23,
	23,
	23,
	23,
	27,
	27,
	23,
	23,
	23,
	23,
	23,
	27,
	23,
	26,
	23,
	32,
	23,
	23,
	23,
	26,
	27,
	23,
	23,
	23,
	23,
	27,
	23,
	3,
	26,
	23,
	23,
	23,
	103,
	23,
	404,
	23,
	23,
	1368,
	27,
	27,
	27,
	27,
	23,
	1022,
	26,
	23,
	23,
	23,
	23,
	23,
	27,
	23,
	23,
	23,
	9,
	103,
	103,
	23,
	23,
	23,
	23,
	27,
	27,
	23,
	32,
	23,
	102,
	14,
	23,
	14,
	23,
	26,
	23,
	23,
	-1,
	-1,
	-1,
	-1,
	32,
	23,
	102,
	14,
	23,
	14,
	32,
	23,
	102,
	14,
	23,
	14,
	26,
	30,
	1447,
	27,
	34,
	1449,
	102,
	102,
	102,
	1010,
	23,
	32,
	1481,
	27,
	27,
	27,
	27,
	27,
	124,
	23,
	32,
	23,
	102,
	14,
	23,
	14,
	32,
	23,
	102,
	14,
	23,
	14,
	3,
	23,
	14,
	26,
	14,
	26,
	14,
	26,
	32,
	23,
	102,
	14,
	23,
	14,
	3,
	23,
	14,
	26,
	14,
	26,
	32,
	23,
	102,
	14,
	23,
	14,
	32,
	23,
	102,
	14,
	23,
	14,
};
static const Il2CppTokenRangePair s_rgctxIndices[4] = 
{
	{ 0x0200001D, { 1, 13 } },
	{ 0x0200001F, { 14, 9 } },
	{ 0x02000064, { 23, 5 } },
	{ 0x06000096, { 0, 1 } },
};
static const Il2CppRGCTXDefinition s_rgctxValues[28] = 
{
	{ (Il2CppRGCTXDataType)3, 7078 },
	{ (Il2CppRGCTXDataType)3, 7079 },
	{ (Il2CppRGCTXDataType)3, 7080 },
	{ (Il2CppRGCTXDataType)3, 7081 },
	{ (Il2CppRGCTXDataType)2, 12734 },
	{ (Il2CppRGCTXDataType)3, 7082 },
	{ (Il2CppRGCTXDataType)3, 7083 },
	{ (Il2CppRGCTXDataType)3, 7084 },
	{ (Il2CppRGCTXDataType)3, 7085 },
	{ (Il2CppRGCTXDataType)3, 7086 },
	{ (Il2CppRGCTXDataType)3, 7087 },
	{ (Il2CppRGCTXDataType)3, 7088 },
	{ (Il2CppRGCTXDataType)2, 12179 },
	{ (Il2CppRGCTXDataType)3, 7089 },
	{ (Il2CppRGCTXDataType)3, 7090 },
	{ (Il2CppRGCTXDataType)3, 7091 },
	{ (Il2CppRGCTXDataType)3, 7092 },
	{ (Il2CppRGCTXDataType)2, 12735 },
	{ (Il2CppRGCTXDataType)3, 7093 },
	{ (Il2CppRGCTXDataType)3, 7094 },
	{ (Il2CppRGCTXDataType)3, 7095 },
	{ (Il2CppRGCTXDataType)3, 7096 },
	{ (Il2CppRGCTXDataType)3, 7097 },
	{ (Il2CppRGCTXDataType)3, 7098 },
	{ (Il2CppRGCTXDataType)2, 12736 },
	{ (Il2CppRGCTXDataType)2, 12484 },
	{ (Il2CppRGCTXDataType)3, 7099 },
	{ (Il2CppRGCTXDataType)3, 7100 },
};
extern const Il2CppCodeGenModule g_AssemblyU2DCSharpCodeGenModule;
const Il2CppCodeGenModule g_AssemblyU2DCSharpCodeGenModule = 
{
	"Assembly-CSharp.dll",
	1288,
	s_methodPointers,
	s_InvokerIndices,
	0,
	NULL,
	4,
	s_rgctxIndices,
	28,
	s_rgctxValues,
	NULL,
};

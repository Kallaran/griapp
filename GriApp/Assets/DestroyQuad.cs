﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyQuad : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        GameObject quad =GameObject.FindGameObjectWithTag("QuadToDel");
        Destroy(quad);
    }

    
}

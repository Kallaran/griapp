﻿using UnityEngine;

public class JumpTowardsTap : MonoBehaviour
{

    public GameObject TopPanel;

    int TapCount;
    public float MaxDubbleTapTime;
    float NewTime;

    void Start()
    {
        TapCount = 0;
    }

    void Update()
    {
        if (Input.touchCount == 1)
        {
            Touch touch = Input.GetTouch(0);

            if (touch.phase == TouchPhase.Ended)
            {
                TapCount += 1;
            }

            if (TapCount == 1)
            {

                NewTime = Time.time + MaxDubbleTapTime;
            }
            else if (TapCount == 2 && Time.time <= NewTime)
            {

                //Whatever you want after a dubble tap 

                if (TopPanel.activeSelf)
                {
                    TopPanel.SetActive(false);
                }
                else
                {
                    TopPanel.SetActive(true);
                }

                TapCount = 0;
            }

        }
        if (Time.time > NewTime)
        {
            TapCount = 0;
        }
    }
}
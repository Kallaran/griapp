﻿using System.IO;
using UnityEngine;

public class LoadImgNum : MonoBehaviour
{

    private void Start()
    {
        Texture OLDtexture = gameObject.GetComponent<MeshRenderer>().material.mainTexture;
        Destroy(OLDtexture);

        GameObject olQuad = GameObject.FindGameObjectWithTag("quad");

                

        int i = olQuad.GetComponent<NumDuQuad>().num;



        byte[] bytes = File.ReadAllBytes(Application.persistentDataPath + "/image" + i.ToString() + ".png");

        Texture2D tex = new Texture2D(2, 2);
        tex.LoadImage(bytes);


        gameObject.transform.position = Camera.main.transform.position + Camera.main.transform.forward * 2.5f;
        gameObject.transform.forward = Camera.main.transform.forward;
        gameObject.transform.localScale = new Vector3(5f, 5f * (tex.height / (float)tex.width), 1f);

        Material material = gameObject.GetComponent<MeshRenderer>().material;
        if (!material.shader.isSupported) // happens when Standard shader is not included in the build
            material.shader = Shader.Find("Legacy Shaders/Diffuse");

        material.mainTexture = tex;
        


    }

    
}

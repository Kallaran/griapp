﻿using UnityEngine;
using System.IO;
using UnityEngine.UI;


public class MainMenu : MonoBehaviour
{
    public Button prefabButton;
    public GameObject Bparent;
    public Button BD_Plus;
    public GameObject Content;






    private Button CreateButtonBD(int yPos, int num)
    {
        // create a button
        Button iButton = Instantiate(prefabButton, transform.position, Quaternion.identity, Bparent.transform.parent); 

        //on positionne le bouton où on le souhaite
        RectTransform BRectTransform = Bparent.GetComponent<RectTransform>();
        Vector2 temp = BRectTransform.anchoredPosition;
        temp.y = yPos;
        iButton.GetComponent<RectTransform>().anchoredPosition = temp;

        //on lui donnne le numero qui correspond à la bd
        iButton.GetComponent<NumImage>().ImgNum = num;

        return iButton;
    }

    

    void Awake()
    {

        int num = 1; // numero courant de la bd
        int yPos = -200;     



        //cherche si il existe des images, si oui pour chaque image il créé un bouton qui permet de s'y rendre
        while (File.Exists(Application.persistentDataPath + "/image" + num.ToString() + ".png"))
        {

            // on cree les boutons
            Button ButtonBD = CreateButtonBD(yPos,num);


            //on incremente le numero et la position pour les prochains boutons
            num++;
            yPos = yPos - 120;
        }

        //on repositionne le bouton '+' pour qu'il soit en dessous des boutons créés
        RectTransform BRectTransform2 = BD_Plus.GetComponent<RectTransform>();
        Vector2 temp2 = BRectTransform2.anchoredPosition;
        temp2.y = yPos;
        BD_Plus.GetComponent<RectTransform>().anchoredPosition = temp2;


        //On modifie la hauteur du content pour qu'il contienne bien tous les boutons
        RectTransform rt = Content.GetComponent<RectTransform>();
        rt.sizeDelta = new Vector2(Screen.width, ((-1)*yPos)+300);
    }


}

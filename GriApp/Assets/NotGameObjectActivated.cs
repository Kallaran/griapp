﻿using UnityEngine;

public class NotGameObjectActivated : MonoBehaviour
{

    public GameObject gamObj;

    public void ChangeActiveState()
    {
        if (gamObj.activeSelf)
        {
            gamObj.SetActive(false);
        }
        else
        {
            gamObj.SetActive(true);
        }
    }
}

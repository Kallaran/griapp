﻿
using System.IO;
using UnityEngine;
using UnityEngine.UI;


//script associé au quad qui doit survivre à la scene du menu avec le numero qui doit permettre d'afficher la bonne image
public class NumDuQuad : MonoBehaviour
{
    public int num;

    private Sprite mySprite;



    //quand le quad est "repéré" par le script de l'image de l'UI, ce dernier le détruit
    //à sa destruction ce script trouve l'image de l'UI et il lui donne le donne le bon sprite grâce au numero 'num' qu'il transporte
    void OnDestroy()    
    {
      
        GameObject image = GameObject.FindGameObjectWithTag("Image");


        byte[] bytes = File.ReadAllBytes(Application.persistentDataPath + "/image" + num.ToString() + ".png");

        Texture2D tex = new Texture2D(2, 2);
        tex.LoadImage(bytes);


        mySprite = Sprite.Create(tex, new Rect(0.0f, 0.0f, tex.width, tex.height), new Vector2(0.5f, 0.5f));

        image.GetComponent<Image>().sprite = mySprite;


        //on redimensionne l'image
        RectTransform rt = image.GetComponent<RectTransform>();
        rt.sizeDelta = new Vector2(Screen.width, tex.height);


    }
}

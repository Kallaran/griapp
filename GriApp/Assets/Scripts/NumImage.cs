﻿using UnityEngine;
using UnityEngine.SceneManagement;

//script associé au bouton qui doit charger la bonne bd
public class NumImage : MonoBehaviour
{
    public int ImgNum; // pour que le bouton sache quelle image il doit charger
    public GameObject PrefabQuad;



    //fonction qui load l'image numero 'imagNum'
    public void LoadImgNum()
    {

        // on instancie le quad indestructible qui va survivre au changement de scene
        GameObject quad = Instantiate(PrefabQuad, transform.position, Quaternion.identity);
        //on lui donne le numero qu'il doit conserver
        quad.GetComponent<NumDuQuad>().num = ImgNum;


        LoadBDScene();
    }

    private void LoadBDScene()
    {
        SceneManager.LoadScene("BD");
    }

  
}

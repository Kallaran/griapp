﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TakeScreenWidth : MonoBehaviour
{
    public float Height;

    void Start()
    {
        RectTransform rt = gameObject.GetComponent<RectTransform>();
        rt.sizeDelta = new Vector2(Screen.width, Height);
    }

}

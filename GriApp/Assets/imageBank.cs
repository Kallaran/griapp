﻿using System.IO;
using UnityEngine;
using UnityEngine.SceneManagement;

public class imageBank : MonoBehaviour
{

    public GameObject oldQuad;

    // Start is called before the first frame update
    void Start()
    {
        oldQuad = GameObject.FindGameObjectWithTag("NewBDQuad");
    }

    

    /* On sauvegarde la texture dans image.png*/
    public void Save()
    {
                                 
        Texture2D texture = (Texture2D)oldQuad.GetComponent<MeshRenderer>().material.mainTexture;

        int num = 1;
        
        if (texture)
        {
            while(File.Exists(Application.persistentDataPath + "/image" + num.ToString() + ".png")){
                num++;
            }


            byte[] imbytes = texture.EncodeToPNG();
            File.WriteAllBytes(Application.persistentDataPath + "/image" + num.ToString() + ".png", imbytes);
            Destroy(texture);

            Debug.Log(Application.persistentDataPath + "/image" + num.ToString() + ".png");
            ///storage/emulated/0/Android/data/com.Kallaran.GriApp/files/image1.png
            Debug.Log("Sauvegarde effectuée");
        }
        else
        {
            Debug.Log("Sauvegarde NON effectuée");

        }

        SceneManager.LoadScene("Menu");


    }



}

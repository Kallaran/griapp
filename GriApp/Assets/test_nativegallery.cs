﻿using UnityEngine;

public class test_nativegallery : MonoBehaviour
{

    public GameObject quad;

    public void TryPickImage()
    {
       
        // Don't attempt to pick media from Gallery/Photos if
        // another media pick operation is already in progress
        if (NativeGallery.IsMediaPickerBusy())
        {
            return;
        }

        else
        {
            // Pick a PNG image from Gallery/Photos
            // If the selected image's width and/or height is greater than 32768 px, down-scale the image
            PickImage(32768);
    
        }
     
                
               
        
    }

  

    private void PickImage (int maxSize)
    {

        Texture2D texture = new Texture2D(2, 2);

        NativeGallery.Permission permission = NativeGallery.GetImageFromGallery((path) =>
        {
            Debug.Log("Image path: " + path);
            if (path != null)
            {
                // Create Texture from selected image
                 texture = NativeGallery.LoadImageAtPath(path, maxSize, false);
                if (texture == null)
                {
                    Debug.Log("Couldn't load texture from " + path);
                    return;
                }

                


                // Assign texture to a temporary quad and destroy it after 5 seconds

                quad.transform.position = Camera.main.transform.position + Camera.main.transform.forward * 2.5f;
                quad.transform.forward = Camera.main.transform.forward;
                quad.transform.localScale = new Vector3(5f, 5f * (texture.height / (float)texture.width), 1f);

                Material material = quad.GetComponent<Renderer>().material;
                if (!material.shader.isSupported) // happens when Standard shader is not included in the build
                    material.shader = Shader.Find("Legacy Shaders/Diffuse");

                material.mainTexture = texture;





            }
        }, "Select a PNG image", "image/png");







        // If a procedural texture is not destroyed manually, 
        // it will only be freed after a scene change
        //Destroy(texture);


        Debug.Log("Permission result: " + permission);

    }

   
}



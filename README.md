# GriApp

### Unity assets used 

- Touchscript
- Native Gallery


## Obtenir l'application sur Android 

- Sur votre smartphone Android, téléchargez le .apk du dossier "BUILD_Android"
- Allez dans les paramètres de votre smartphone Android
- Trouvez "Sécurité"
- Autorisez les "Sources inconnues"
- Lancez le .apk que vous avez téléchargé précédemment
- Pensez à désautoriser les "sources inconnues" une fois l'application installée

## Obtenir l'application sur iOS 

- Sur votre mac, téléchargez le dossier "BUILD_iOS"
- Téléchargez Xcode que vous trouverez sur l'App Store
- Ouvrez le terminal (Launchpad > Rechercher > Terminal)
- Donnez le droit d'exécution au fichier MapFileParser.sh avec la commande ci-dessous par exemple
- chmod +x Downloads/griapp-master-BUILD_iOS/BUILD_iOS/MapFileParser.sh
- Ouvrez Unity-iPhone.xcodeproj (il se trouve dans le dossier que vous avez téléchargé précédemment)
- Xcode se lance, vérifiez que votre iPhone est correctement branché et cliquez sur le bouton "play"